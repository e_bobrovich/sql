select * from pos_dk_scores a where a.id_dk = '0000021540141' order by dates, id_sale,dk_sum;

select itog.id_dk, plus.val, used.val, burn.val, itog.val from (
  select a.id_dk, sum(dk_sum) val  from pos_dk_scores a 
  where a.id_dk = '0000021540141'
  and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
  and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
  group by a.id_dk
) itog
left join
(
  select a.id_dk, sum(dk_sum) val from pos_dk_scores a 
  where a.id_dk = '0000021540141'
  and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
  and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
  and a.dk_sum > 0
  group by a.id_dk
) plus on itog.id_dk = plus.id_dk
left join
(
  select a.id_dk, sum(dk_sum) val from pos_dk_scores a 
  where a.id_dk = '0000021540141'
  and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
  and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
  and a.dk_sum <= 0
  and a.id_shop != 'FIRM'
  group by a.id_dk
) used on itog.id_dk = used.id_dk
left join
(
  select a.id_dk, sum(dk_sum) val from pos_dk_scores a 
  where a.id_dk = '0000021540141'
  and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
  and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
  and a.dk_sum <= 0
  and a.id_shop = 'FIRM'
  group by a.id_dk
) burn on itog.id_dk = burn.id_dk
;



select a.id_dk, sum(dk_sum) from pos_dk_scores a 
where a.id_dk = '0000021540141'
and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
;

select a.id_dk, sum(dk_sum) from pos_dk_scores a 
where a.id_dk = '0000021540141'
and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
and a.dk_sum > 0
;

select a.id_dk, sum(dk_sum) from pos_dk_scores a 
where a.id_dk = '0000021540141'
and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
and a.dk_sum <= 0
and a.id_shop != 'FIRM'
;

select a.id_dk, sum(dk_sum) from pos_dk_scores a 
where a.id_dk = '0000021540141'
and trunc(a.dates) >= trunc(to_date('01.01.2018', 'dd.mm.yyyy'))
and trunc(a.dates) <= trunc(to_date('31.12.2018', 'dd.mm.yyyy'))
and a.dk_sum <= 0
and a.id_shop = 'FIRM'
;