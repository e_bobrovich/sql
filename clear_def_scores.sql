-- проход по дк
select pds.id_dk from pos_dk_scores pds group by pds.id_dk
;

-- количество обычных баллов начисленных за день год назад и дата\время первого чека
-- durr_date = 06.04.2017
-- into v_day_scores, v_day_first_chek
select sum(dk_sum), min(dates) from pos_dk_scores
where id_dk = '0000014741630'
and dk_sum > 0
and doc_type = 'ck'
and case when to_char(last_day(to_date('01/02/'||extract (year from to_date('06.04.2017', 'DD.MM.YYYY')), 'dd/mm/yyyy')), 'DD') = 29 
         then trunc(to_date('06.04.2017', 'DD.MM.YYYY')-366, 'ddd')
         else trunc(to_date('06.04.2017', 'DD.MM.YYYY')-365, 'ddd')
         end = (trunc(dates, 'ddd'))
;

-- (FALSE)сумма баллов в день год назад без вычетов в этот день
-- durr_date = 08.03.2018 a.*, a.rowid
select sum(dk_sum)  from pos_dk_scores a
where id_dk = '0000014741630'
and dk_sum <> 0
and doc_type = 'ck'
and case when to_char(last_day(to_date('01/02/'||extract (year from to_date('08.03.2018 ', 'DD.MM.YYYY')), 'dd/mm/yyyy')), 'DD') = 29 
         then trunc(to_date('08.03.2018 ', 'DD.MM.YYYY')-366, 'ddd')
         else trunc(to_date('08.03.2018 ', 'DD.MM.YYYY')-365, 'ddd')
         end >= (trunc(dates, 'ddd'))   
and a.rowid not in (
    select a.rowid  from pos_dk_scores a
    where id_dk = '0000014741630'
    and dk_sum < 0
    and doc_type = 'ck'
    and trunc(dates, 'ddd') in (
      select case when to_char(last_day(to_date('01/02/'||extract (year from to_date('08.03.2018 ', 'DD.MM.YYYY')), 'dd/mm/yyyy')), 'DD') = 29 
         then trunc(to_date('08.03.2018 ', 'DD.MM.YYYY')-366, 'ddd')
         else trunc(to_date('08.03.2018 ', 'DD.MM.YYYY')-365, 'ddd')
         end dates
      from dual
    )
)
order by dates
;

-- (TRUE)сумма баллов в день год назад без вычетов после первого чека
-- durr_date = 08.03.2018 a.*, a.rowid
-- first_chek_date = 22.10.16 10:12:07
select sum(dk_sum)  from pos_dk_scores a
where id_dk = '0000014741630'
and dk_sum <> 0
and doc_type = 'ck'
and case when to_char(last_day(to_date('01/02/'||extract (year from to_date('22.10.2017', 'DD.MM.YYYY')), 'dd/mm/yyyy')), 'DD') = 29 
         then trunc(to_date('22.10.2017', 'DD.MM.YYYY')-366, 'ddd')
         else trunc(to_date('22.10.2017', 'DD.MM.YYYY')-365, 'ddd')
         end >= (trunc(dates, 'ddd'))   
and a.rowid not in (
    select a.rowid  from pos_dk_scores a
    where id_dk = '0000014741630'
    and dk_sum < 0
    and doc_type = 'ck'
    and dates > to_timestamp('22.10.2016 10:12:07', 'DD.MM.YYYY HH24:MI:SS') 
    and dates <= to_timestamp('22.10.2016 10:12:07', 'DD.MM.YYYY HH24:MI:SS')+1
)
order by dates
;

-- сумма вычетов за год после первого чека

select sum(dk_sum) from pos_dk_scores a
where id_dk = '0000014741630'
and dk_sum < 0
and doc_type = 'ck'
and dates > to_timestamp('22.10.2016 10:12:07', 'DD.MM.YYYY HH24:MI:SS') 
and dates <= to_timestamp('22.10.2017 10:12:07', 'DD.MM.YYYY HH24:MI:SS')--текущая дата
order by dates
;