select * from s_seller_sap a
full join s_seller b on a.pernr = b.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
where to_char(dated,'yyyy') = '9999' and stat2 != 3 and lower(stext2) like '%продавец%'
order by begda;


select  a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, case when a.stat5 = 1 then 'Декрет' else ' ' end stat5 , c.* from s_seller_sap a
left join st_seller_massn b on a.massn = b.massn_id
full join s_seller c on a.pernr = c.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
--where pernr like '%20675'
--where pernr like '%00023058'
--where pernr like '%20584'
where pernr like '%23058'
order by begda;



select  * from s_seller_sap a
full join s_seller c on a.pernr = c.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
where pernr like '%00000514'
order by begda;

select  * from s_seller_sap a
full join s_seller c on a.pernr = c.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
where tabno in ('00020584', '00023058', '00020675')
or  pernr in ('00020584', '00023058', '00020675')
order by begda;

select  a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, c.* from s_seller_sap a
left join st_seller_massn b on a.massn = b.massn_id
full join s_seller c on a.pernr = c.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
where 
--to_char(dated,'yyyy') = '9999' and 
stat2 = 1
and tabno in ('00123058')
or  pernr in ('00123058')
order by a.pernr||tabno, begda, dateb;



select  a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name from s_seller_sap a
left join st_seller_massn b on a.massn = b.massn_id
where 
pernr in ('00023058')
order by a.pernr;


select b.tabno, b.fio, d.stat5, d.massn, d.massn_name, b.stext2, b.stext3, b.dateb, b.dated, c.tabno zavmag_tab, c.fio zavmag, a.* from bep_prem_seller a
left join (
    select tabno,fio,stext2,stext3,dateb,dated from s_seller 
    where  ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
       and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
) b on a.seller_tab = b.tabno
left join (
    select * from (
        select tabno, fio, orgno from s_seller 
        where 
        orgno = lpad(COALESCE('0046', orgno), 5, '0') and
        (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
        and ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        --and to_char(dated, 'yy') = '99'
        order by stext2 desc, dated desc
    ) where rownum = 1
) c on lpad(a.id_shop, 5, '0') = c.orgno
left join (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    where
        ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
) d on a.seller_tab = d.pernr
where a.year = COALESCE(2018, EXTRACT(YEAR FROM sysdate)) and a.month = COALESCE(8, EXTRACT(YEAR FROM sysdate)) and a.id_shop = COALESCE('0046', a.id_shop) and a.seller_tab != (
    select * from (
        select max(tabno) from s_seller 
        where orgno = lpad(COALESCE('0046', orgno), 5, '0') and
        (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
        and ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        --and to_char(dated, 'yy') = '99'
        order by stext2 desc, dated desc
    ) where rownum = 1
);