select idop,id_shop,MANUFACTOR, 0 STANDART_SUM,
  sum(sum_nds) SUM_NDS,
  0 SUM_NAC,
  sum(sum2) SUM_ROZN from (
    SELECT AA.*,ROUND(SUM2-SUM_NDS-NAC,2) AS SUM1,CASE WHEN KOL!=0 THEN ROUND(SUM2-SUM_NDS-NAC,2)/KOL ELSE 0 END AS CENA1 FROM (
      SELECT A.*,ROUND(SUM2/100.00*NDS,2) AS SUM_NDS
        ,ROUND((SUM2-ROUND(SUM2/100.00*NDS,2))/100*NACP,2) AS NAC
        FROM ( 
          SELECT ART,ASSORT,ASIZE,PROCENT,NEI,SUM(KOL) AS KOL,CENA2,SUM(KOL*CENA2) AS SUM2,SUM(TO_NUMBER(BRGEW,'99.999'))/1000  AS BRGEW
            ,FIRM.GET_NDSR(to_date(DATE_D,'yyyymmdd'),ID_SHOP)  AS NDS
            ,FIRM.GET_NACR(to_date(DATE_D,'yyyymmdd'),ID_SHOP)  AS NACP
            ,ID_SHOP,ID,MANUFACTOR,IDOP
            FROM (
              SELECT A."ID",A."ART",A."ASIZE",A."PROCENT",A."EAN",A."SCAN",A."ASSORT",A."KOL",A."CENA1",A."CENA2",A."CENA3",
                A."PORTION",A."NEI",A."VID_INS",A."PARTNO",A."KOROB",A."REL",A."NDS",A."TEXT",A."TABNO",A."SKIDKA"
                ,CASE VID_INS   WHEN 0 THEN 'РУЧНОЙ'   WHEN 1 THEN 'СКАНЕР'   WHEN 2 THEN 'ИМПОРТ'   ELSE ' '  END AS NAME_INS
                ,CAST(CENA1*KOL AS NUMBER(18,2)) AS SUM1,CAST(CENA3*KOL AS NUMBER(18,2)) AS SUM3,B.PRODH
                ,B.VOLUM,B.BRGEW,A.TABNO||' '||A.TEXT||' СКИДКА:'||TO_CHAR(A.SKIDKA,'99') AS TABNO_TEXT,A.ID_DK,C.IDOP
                ,A.ID_SHOP,to_char(C.DATED,'yyyymmdd') DATE_D, CASE WHEN B.MTART='ZFRT' THEN 'S' ELSE 'P' END AS MANUFACTOR
                FROM FIRM.D_RASXOD2 A
                left join FIRM.D_RASXOD1 C on a.id = c.id and a.id_shop = c.id_shop
                LEFT JOIN FIRM.S_ART B ON A.ART=B.ART
                where a.id =  '?1?'  and a.id_shop= '?2?'
            )
          GROUP BY ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS,DATE_D,ID_SHOP,ID,MANUFACTOR,IDOP
        ) A
      ) AA
    ) AAA
group by ID_SHOP,MANUFACTOR,idop
;


select id_op, id_shop, /*MANUFACTOR,*/ 
0 standart_sum,
a.*,
ROUND(SUM2/100.00*NDS,2) AS SUM_NDS
,SUM2-(ROUND(SUM2/100.00*NDS,2))-SUM1 AS NAC
from (
    select c.idop id_op, a.art, a.assort, a.asize, a.procent, a.nei, 
    sum(a.kol) as kol, 
    a.cena1,sum(a.kol*a.cena1) as sum1, 
    a.cena2,sum(a.kol*a.cena2) as sum2, 
    sum(to_number(a.brgew,'99.999'))/1000  as brgew,
    get_ndsr(c.dated, a.id_shop) as nds, 
    a.id_shop
     from FIRM.d_rasxod2_v a
     left join firm.d_rasxod1 c on a.id = c.id and a.id_shop = c.id_shop
    where a.rel=368 /*'?1?'*/ and a.id_shop= '0070' /*'?2?'*/
    group by c.idop, a.assort, a.art, a.asize, a.procent, a.cena1, a.cena2, a.cena3, a.nei, a.nds, c.dated, a.id_shop
) A
order by art,asize,procent,cena1,cena2,nei                 
;


select aa.*, round(sum2/100.00*nds,2) as sum_nds from (
    select c.idop, a.art, b.assort, a.asize, a.procent, a.nei, b.manufactor,
    sum(a.kol) as kol, 
    a.cena1,sum(a.kol*a.cena1) as sum1, 
    a.cena2,sum(a.kol*a.cena2) as sum2, 
    sum(to_number(b.brgew,'99.999'))/1000  as brgew,
    get_ndsr(c.dated, a.id_shop) as nds, 
    a.id_shop
     --from FIRM.d_rasxod2_v a
    from d_rasxod2 a
    left join s_art b on a.art=b.art
    left join firm.d_rasxod1 c on a.id = c.id and a.id_shop = c.id_shop
    where a./*rel*/id=173/*368*/ /*'?1?'*/ and a.id_shop= '0070' /*'?2?'*/
    group by c.idop, b.assort, a.art, a.asize, a.procent, a.cena1, a.cena2, a.cena3, a.nei, b.manufactor, a.nds, c.dated, a.id_shop
) aa
    ;
    
select idop,id_shop,manufactor, 
0 standart_sum,
sum(sum_nds) sum_nds,
0 sum_nac,
sum(sum2) sum_rozn from (     
    SELECT AA.*,ROUND(SUM2-SUM_NDS-NAC,2) AS SUM1,CASE WHEN KOL!=0 THEN ROUND(SUM2-SUM_NDS-NAC,2)/KOL ELSE 0 END AS CENA1 FROM (
        SELECT A.*,ROUND(SUM2/100.00*NDS,2) AS SUM_NDS
        ,round((sum2-round(sum2/100.00*nds,2))/100*nacp,2) as nac
        FROM ( 
            SELECT ART,ASSORT,ASIZE,PROCENT,NEI,SUM(KOL) AS KOL,CENA2,SUM(KOL*CENA2) AS SUM2,SUM(TO_NUMBER(BRGEW,'99.999'))/1000  AS BRGEW
            ,FIRM.GET_NDSR(to_date(DATE_D,'yyyymmdd'),ID_SHOP)  AS NDS
            ,FIRM.GET_NACR(to_date(DATE_D,'yyyymmdd'),ID_SHOP)  AS NACP
            ,id_shop,id,manufactor,idop
            FROM (
                SELECT A."ID",A."ART",A."ASIZE",A."PROCENT",A."EAN",A."SCAN",A."ASSORT",A."KOL",A."CENA1",A."CENA2",A."CENA3",
                A."PORTION",A."NEI",A."VID_INS",A."PARTNO",A."KOROB",A."REL",A."NDS",A."TEXT",A."TABNO",A."SKIDKA"
                ,CASE VID_INS   WHEN 0 THEN 'РУЧНОЙ'   WHEN 1 THEN 'СКАНЕР'   WHEN 2 THEN 'ИМПОРТ'   ELSE ' '  END AS NAME_INS
                ,CAST(CENA1*KOL AS NUMBER(18,2)) AS SUM1,CAST(CENA3*KOL AS NUMBER(18,2)) AS SUM3,B.PRODH
                ,B.VOLUM,B.BRGEW,A.TABNO||' '||A.TEXT||' СКИДКА:'||TO_CHAR(A.SKIDKA,'99') AS TABNO_TEXT,A.ID_DK,C.IDOP
                ,A.ID_SHOP,to_char(C.DATED,'yyyymmdd') DATE_D, CASE WHEN B.MTART='ZFRT' THEN 'S' ELSE 'P' END AS MANUFACTOR
                FROM FIRM.D_RASXOD2 A
                left join FIRM.D_RASXOD1 C on a.id = c.id and a.id_shop = c.id_shop
                left join firm.s_art b on a.art=b.art
                where a.id =  173  and a.id_shop= '0070'
            ) group by assort,art,asize,procent,cena1,cena2,cena3,nei,nds,date_d,id_shop,id,manufactor,idop
        ) a
    ) aa
) aaa group by ID_SHOP,MANUFACTOR,idop