select distinct 
  case 
    when x.id_shop in (select shopid from FIRM.s_shop where Landid = 'BY')
      then 
      ( case when x.idop = '21'  then '99' else x.idop end)
    else ( case when x.idop = '36'  then '96' else x.idop end) end idop , 
  case 
    when INSTR(x.ID_SHOP,'W') = 3 and INSTR(x.kkl,'W') = 0 
      THEN SUBSTR(x.kkl,0,2)||'W'||SUBSTR(x.ID_SHOP,4,1) 
    else x.id_shop end id_shop,
  x.ID, 
  IDOCS.Get_Date_D(x.DATED) DATED,
  replace(x.NDOC,'<','Б'), 
  case 
    WHEN INSTR(x.kkl,'W') = 3 
      THEN SUBSTR(replace(x.id_shop,'W',''),0,2)||'W'||SUBSTR(x.kkl,4,1) 
    ELSE x.kkl END kkl, 
  regexp_replace(x.TEXT,'<|>','') TEXT,
  IDOCS.Get_Date_D(x.DATE_S) DATE_S, 
  x.BIT_CLOSE, 
  x.NAMEOP, 
   x.nvl_postno POSTNO, 
   ' ' IMPNUM,
  (select kval from FIRM.ST_SHOP where shopid = x.ID_SHOP ) WAERS
from 
  (select 
    a.ID ,
    CAST (a.DATED AS TIMESTAMP(6)) DATED,
    CAST (a.DATE_CLOSE AS TIMESTAMP(6)) DATE_CLOSE,
    a.NDOC,
    a.kkl, 
    a.NKL ,
    a.TEXT ,
    a.DATE_S ,
    a.BIT_CLOSE ,
    a.IDOP ,
    a.NAMEOP ,
    a.POSTNO ,
    a.id_shop,
    nvl(c.POSTNO,' ') nvl_postno, 
    case 
      when a.TIMED is not null 
        then a.TIMED 
      else CAST (a.DATED AS TIMESTAMP(6)) end TIMED 
  from FIRM.d_rasxod1 a 
  inner join FIRM.d_rasxod2 b 
    on a.ID = b.ID and a.ID_SHOP = b.ID_SHOP 
  left join 
    (select 
      postvid,
      max(postno) postno,
      shopid,
      skladid,
      prdate,
      partnerid,
      namepart,
      postno_lifex,
      postno_traid 
    from FIRM.D_SAP_ODGRUZ1 
    where Pr_Ud != '1' 
    group by 
      postvid,
      shopid,
      skladid,
      prdate,
      partnerid,
      namepart,
      postno_lifex,
      postno_traid) c 
    on a.ID_SHOP = c.SKLADID and a.ID = c.POSTNO_LIFEX and c.POSTVID='TD' and a.KKL = c.shopid 
  group by 
    a.ID ,
    a.DATED ,
    a.DATE_CLOSE ,
    a.NDOC ,
    a.kkl,
    a.NKL ,
    a.TEXT ,
    a.DATE_S ,
    a.BIT_CLOSE ,
    a.IDOP ,
    a.NAMEOP ,
    a.POSTNO ,
    a.id_shop ,
    c.POSTNO, 
    case 
      when a.TIMED is not null 
        then a.TIMED 
      else CAST (a.DATED AS TIMESTAMP(6)) end) x 
where 
  x.ID_SHOP!='S777' and 
  x.ID_SHOP!='S888' and 
  x.ID_SHOP not like '%U%' and 
  x.kkl not like '%U%' and
  x.BIT_CLOSE = 'T' and 
  x.id_shop in (select shopid from firm.s_shop where landid = 'BY') and 
  x.IDOP in ('36')