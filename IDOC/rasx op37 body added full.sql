select idop,id_shop,MANUFACTOR, 
  case 
    when idop in ('36','21', '34') then
      0 
    else
      sum(sum1)
  end standart_sum,
  case 
    when idop in ('34') then
      0 
    else
      sum(sum_nds)
  end SUM_NDS,
  case 
    when idop in ('36','21', '34') then
      0 
    else
      sum(nac)
  end SUM_NAC,
  sum(sum2) SUM_ROZN from (
    SELECT AA.*,ROUND(SUM2-SUM_NDS-NAC,2) AS SUM1,CASE WHEN KOL!=0 THEN ROUND(SUM2-SUM_NDS-NAC,2)/KOL ELSE 0 END AS CENA1 FROM (
      SELECT A.*,ROUND(SUM2/100.00*NDS,2) AS SUM_NDS
        ,ROUND((SUM2-ROUND(SUM2/100.00*NDS,2))/100*NACP,2) AS NAC
        FROM ( 
          SELECT ART,ASSORT,ASIZE,PROCENT,NEI,SUM(KOL) AS KOL,CENA2,SUM(KOL*CENA2) AS SUM2,SUM(TO_NUMBER(BRGEW,'99.999'))/1000  AS BRGEW
            ,FIRM.GET_NDSR(to_date(DATE_D,'yyyymmdd'),ID_SHOP)  AS NDS
            ,FIRM.GET_NACR(to_date(DATE_D,'yyyymmdd'),ID_SHOP)  AS NACP
            ,ID_SHOP,ID,MANUFACTOR,IDOP
            FROM (
              SELECT A."ID",A."ART",A."ASIZE",A."PROCENT",A."EAN",A."SCAN",A."ASSORT",A."KOL",A."CENA1",A."CENA2",A."CENA3",
                A."PORTION",A."NEI",A."VID_INS",A."PARTNO",A."KOROB",A."REL",A."NDS",A."TEXT",A."TABNO",A."SKIDKA"
                ,CASE VID_INS   WHEN 0 THEN 'РУЧНОЙ'   WHEN 1 THEN 'СКАНЕР'   WHEN 2 THEN 'ИМПОРТ'   ELSE ' '  END AS NAME_INS
                ,CAST(CENA1*KOL AS NUMBER(18,2)) AS SUM1,CAST(CENA3*KOL AS NUMBER(18,2)) AS SUM3,B.PRODH
                ,B.VOLUM,B.BRGEW,A.TABNO||' '||A.TEXT||' СКИДКА:'||TO_CHAR(A.SKIDKA,'99') AS TABNO_TEXT,A.ID_DK,C.IDOP
                ,A.ID_SHOP,to_char(C.DATED,'yyyymmdd') DATE_D, CASE WHEN B.MTART='ZFRT' THEN 'S' ELSE 'P' END AS MANUFACTOR
                FROM FIRM.D_RASXOD2 A
                left join FIRM.D_RASXOD1 C on a.id = c.id and a.id_shop = c.id_shop
                left join firm.s_art b on a.art=b.art
                where a.id =  '1848'  and a.id_shop= '0004' --and c.idop  not in ('37')
            )
          GROUP BY ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS,DATE_D,ID_SHOP,ID,MANUFACTOR,IDOP
        ) A
      ) aa
    ) aaa 
where idop not in ('37')
group by id_shop,manufactor,idop

union all

select idop,id_shop,manufactor, 
  0 standart_sum,
  sum(sum2) sum_nds,
  sum(dop_sum) sum_nac,
  sum(sum2-sum3) SUM_ROZN from 
(
    select a.idop, a.id_shop,  b.kol, b.cena1, cena2, cena3,
    (kol*cena1) as sum1,(kol*cena2) as sum2, (kol*cena3) as sum3,
    case 
        when b.rowid in (select min(z.rowid) from d_rasxod2 z where z.id = a.id and z.id_shop = a.id_shop group by z.id) 
        then (select nvl(sum(d.sum),0) from d_rasxod3 d where d.id = a.id and d.id_shop = a.id_shop) else 0
        end dop_sum,
    case when c.mtart='ZFRT' then 'S' else 'P' end as manufactor
        
    from d_rasxod1 a
    inner join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop and a.bit_close = 'T'
    
    and a.id = '1848' and a.id_shop = '0004' and a.idop in ('37')
    inner join s_art c on b.art = c.art
)
where idop in ('37')
group by id_shop,manufactor,idop
;

  