select idop,id_shop,manufactor, 
case when idop in ('07') then sum(sum01) else sum(sum_0) end standart_sum,
--sum(SUM_0) STANDART_SUM,
sum(SUM_NDS) SUM_NDS,
sum(SUM_NAC) SUM_NAC,sum(SUM3) SUM_ROZN
from (
  SELECT D.SUM_0,D.SUM01,D.SUM_NDS,D.SUM_NAC,D.SUM3,D.MANUFACTOR,D.idop,D.id_shop
   FROM (
    SELECT C.*
    ,CASE WHEN C.idop = '01' THEN SUM0 ELSE SUM1 END AS SUM01
    ,CASE WHEN C.idop = '04' THEN ROUND(SUM2-SUM_NDS-NAC,2) else SUM0 end as SUM_0
    ,CASE WHEN B1.MTART='ZFRT' THEN 'S' ELSE 'P' END AS MANUFACTOR
    from  (
    
      SELECT B.*,
        case 
          when B.idop  in ('01','15') then SUM3-SUM_NDS-SUM0 
          when B.idop = '04' then ROUND((SUM2-ROUND(SUM2/100.00*NDS,2))/100*NACP,2)
            else SUM3-SUM_NDS-SUM1 end AS SUM_NAC
      FROM (
        SELECT A.*,
          CASE 
            WHEN IDOP = '04' THEN ROUND(SUM2/100.00*NDS,2)
            ELSE ROUND(SUM3/(100.00+NDS)*NDS,2) END AS SUM_NDS
        ,SUM3/(100.00+PROCENT)*PROCENT  AS SUM_PROCENT
        ,ROUND((SUM2-ROUND(SUM2/100.00*NDS,2))/100*NACP,2) AS NAC
         FROM (
          SELECT ART,ASIZE,PROCENT,NEI,SUM(kol_new) AS KOL
          ,CENA0,SUM(kol_new*CENA0) AS SUM0,CENA1,SUM(kol_new*CENA1) AS SUM1
          ,CENA2,SUM(kol_new*CENA2) AS SUM2,CENA3,SUM(kol_new*CENA3) AS SUM3
          ,CASE
            WHEN IDOP = '04' THEN FIRM.GET_NDSR(to_date(date_d,'yyyymmdd'),KKl)
            WHEN IDOP != '04' and NDS0=0 THEN FIRM.GET_NDS(ART,ASIZE) 
            ELSE NDS0 END AS NDS,
          idop,id_shop,FIRM.GET_NACR(to_date(date_d,'yyyymmdd'),KKl)  AS NACP,date_d
          from 
            (
              SELECT 
                g.PRICE2 CENA0, a.*, B.NDS as NDS0, e.idop, to_char(x.dated,'yyyymmdd') as date_d,x.id_shop kkl
                FROM (
                  select a.kol kol_new,a.* from firm.d_prixod2 a
                  inner join firm.d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
                  where a.kol != 0 and a.koli != 0 and c.idop != '07'
                  union
                  select b.kol kol_new,a.* from FIRM.D_PRIXOD2 a
                  left join FIRM.D_PRIXOD2 b 
                    on a.id =  b.id and 
                      a.id_shop = b.id_shop and 
                      A.ART = b.art and 
                      A.ASIZE = B.ASIZE and 
                      b.koli = 0
                  inner join firm.d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
                  where a.kol = 0 and c.idop != '07'
                  union
                  select a.kol kol_new,a.* from firm.d_prixod2 a
                  inner join firm.d_prixod1 c on a.ID = c.ID and a.ID_SHOP = c.ID_SHOP 
                  where c.idop = '07'
                ) a
                left join FIRM.D_PRIXOD1 e on a.id = e.id and a.id_shop = e.id_shop
                left join FIRM.S_NDS b on a.art=b.art and a.asize = b.asize
                left join (select postno,art,asize,scan,partno,price2,sum(kol) kol
		from firm.d_sap_odgruz2
		group by postno,art,asize,scan,partno,price2) g on g.postno = e.postno and g.art = a.art and g.asize = a.asize
                  and (case when length(G.SCAN) < 3 then ' ' else G.SCAN end) = 
                      (case when length(a.SCAN) < 3 then ' ' else FIRM.get_sap_scancode(a.SCAN) end)
                  and G.PARTNO = A.PARTNO
	left join firm.d_sap_odgruz1 h on h.postno = g.postno
                 left join FIRM.D_RASXOD1 x on 
	to_char(x.id) = h.postno_lifex and
	x.id_shop = h.skladid
--                  x.postno = e.postno and 
--                  e.kkl = x.id_shop
                 where a.id=  '383'  and a.id_shop= '0070'
                 --WHERE a.ID=  '1497'  and a.id_shop= '0034'
              )
           GROUP BY ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS0,CENA0,idop,id_shop,date_d,kkl
          ) A
        ) B
    ) C LEFT JOIN FIRM.S_ART B1 ON C.ART=B1.ART
  )D
) group by idop,id_shop,MANUFACTOR