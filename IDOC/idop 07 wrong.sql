select idop,id_shop,manufactor, 
sum(sum_0) standart_sum, --st
sum(sum_nds) sum_nds, --nds
sum(sum_nac) sum_nac --tr
,sum(SUM3) SUM_ROZN --rz
from (
  SELECT D.SUM_0,D.SUM_NDS,D.SUM_NAC,D.SUM3,D.MANUFACTOR,D.idop,D.id_shop
   FROM (
    SELECT C.*
    ,CASE 
      when c.idop = '01' then sum0 
        else sum1 end as sum01 -- +
        
    ,CASE --st
      when c.idop = '04' then round(sum2-sum_nds-nac,2)
      when c.idop = '07' then sum1
        else sum0 end as sum_0 -- +
        
    ,CASE WHEN B1.MTART='ZFRT' THEN 'S' ELSE 'P' END AS MANUFACTOR
    FROM  (
      SELECT B.*,
        case 
          when B.idop  in ('01','15') then SUM3-SUM_NDS-SUM0 
          when b.idop = '04' then round((sum2-round(sum2/100.00*nds,2))/100*nacp,2)
            else SUM3-SUM_NDS-SUM1 end AS SUM_NAC -- +
      FROM (
        SELECT A.*,
          CASE 
            WHEN IDOP = '04' THEN ROUND(SUM2/100.00*NDS,2)
            ELSE ROUND(SUM3/(100.00+NDS)*NDS,2) END AS SUM_NDS
        ,SUM3/(100.00+PROCENT)*PROCENT  AS SUM_PROCENT
        ,ROUND((SUM2-ROUND(SUM2/100.00*NDS,2))/100*NACP,2) AS NAC
         FROM (
          select art,asize,procent,nei,sum(kol_new) as kol
          ,nvl(cena0, 0) as cena0,sum(kol_new*nvl(cena0,0)) as sum0 --st
          ,CENA1,SUM(kol_new*CENA1) AS SUM1
          ,cena2,sum(kol_new*cena2) as sum2
          ,CENA3,SUM(kol_new*CENA3) AS SUM3
          ,CASE
            WHEN IDOP = '04' THEN FIRM.GET_NDSR(to_date(date_d,'yyyymmdd'),KKl)
            WHEN IDOP != '04' and NDS0=0 THEN FIRM.GET_NDS(ART,ASIZE) 
            ELSE NDS0 END AS NDS,
          idop,id_shop
          ,firm.get_nacr(to_date(date_d,'yyyymmdd'),kkl)  as nacp
          ,date_d
          from 
            (
              SELECT 
                g.PRICE2 CENA0, a.*, B.NDS as NDS0, e.idop, to_char(x.dated,'yyyymmdd') as date_d,x.id_shop kkl
                FROM (
                  select a.kol kol_new,a.* from FIRM.D_PRIXOD2 a
                  where a.kol != 0 and a.koli != 0
                  union
                  select /*b.kol*/ case when b.kol = 0 then 1 else b.kol end kol_new,a.* from FIRM.D_PRIXOD2 a
                  left join FIRM.D_PRIXOD2 b 
                    on a.id =  b.id and 
                      a.id_shop = b.id_shop and 
                      A.ART = b.art and 
                      A.ASIZE = B.ASIZE and 
                      b.koli = 0
                  where a.kol = 0
                ) a
                left join FIRM.D_PRIXOD1 e on a.id = e.id and a.id_shop = e.id_shop
                left join FIRM.S_NDS b on a.art=b.art and a.asize = b.asize
                left join (select postno,art,asize,scan,partno,price2,sum(kol) kol
                            from firm.d_sap_odgruz2
                            group by postno,art,asize,scan,partno,price2
                ) g on g.postno = e.postno and g.art = a.art and g.asize = a.asize
                      and (case when length(G.SCAN) < 3 then ' ' else G.SCAN end) = 
                          (case when length(a.SCAN) < 3 then ' ' else FIRM.get_sap_scancode(a.SCAN) end)
                      and G.PARTNO = A.PARTNO
                left join firm.d_sap_odgruz1 h on h.postno = g.postno
                left join FIRM.D_RASXOD1 x on to_char(x.id) = h.postno_lifex and x.id_shop = h.skladid
--                  x.postno = e.postno and 
--                  e.kkl = x.id_shop
                 where a.id=  /*'?1?'*/ 390 and a.id_shop= /*'?2?'*/ '0070'
              )
           GROUP BY ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS0,CENA0,idop,id_shop,date_d,kkl
          ) A
        ) B
    ) C LEFT JOIN FIRM.S_ART B1 ON C.ART=B1.ART
  )d
) group by idop,id_shop,manufactor
