SELECT D.*,CASE WHEN SUM01!=0 THEN SUM_NAC/SUM01*100.00 ELSE 0 END AS NAC
 FROM (
    SELECT C.*
    ,CASE WHEN $P!{PR_CENA}=0 THEN SUM_NAC0 ELSE SUM_NAC1 END AS SUM_NAC
    ,CASE WHEN $P!{PR_CENA}=0 THEN SUM0 ELSE SUM1 END AS SUM01
    ,CASE WHEN B1.MTART='ZFRT' THEN 'СОБСТВЕННОГО ПРОИЗВОДСТВА' ELSE 'ПОКУПНЫЕ ТМЦ' END AS MANUFACTOR
    FROM  (
    
        SELECT B.*,SUM3-SUM_NDS-SUM0 AS SUM_NAC0,SUM3-SUM_NDS-SUM1 AS SUM_NAC1
        FROM (
        
            SELECT A.*,ROUND(SUM3/(100.00+NDS)*NDS,2) AS SUM_NDS
            ,SUM3/(100.00+PROCENT)*PROCENT  AS SUM_PROCENT
             FROM (
            
                SELECT ART,ASIZE,PROCENT,NEI,SUM(kol_new) AS KOL
                  ,CENA0,SUM(kol_new*CENA0) AS SUM0,CENA1,SUM(kol_new*CENA1) AS SUM1
                  ,CENA2,SUM(kol_new*CENA2) AS SUM2,CENA3,SUM(kol_new*CENA3) AS SUM3
                  ,CASE
                    WHEN IDOP = '04' THEN FIRM.GET_NDSR(to_date(date_d,'yyyymmdd'),KKl)
                    WHEN IDOP != '04' and NDS0=0 THEN FIRM.GET_NDS(ART,ASIZE) 
                    ELSE NDS0 END AS NDS,
                  idop,id_shop,FIRM.GET_NACR(to_date(date_d,'yyyymmdd'),KKl)  AS NACP,date_d
                  from 
                    (
                      SELECT 
                        g.PRICE2 CENA0, a.*, B.NDS as NDS0, e.idop, to_char(x.dated,'yyyymmdd') as date_d,x.id_shop kkl
                        FROM (
                          select a.kol kol_new,a.* from FIRM.D_PRIXOD2 a
                          where a.kol != 0 and a.koli != 0
                          union
                          select b.kol kol_new,a.* from FIRM.D_PRIXOD2 a
                          left join FIRM.D_PRIXOD2 b 
                            on a.id =  b.id and 
                              a.id_shop = b.id_shop and 
                              A.ART = b.art and 
                              A.ASIZE = B.ASIZE and 
                              b.koli = 0
                          where a.kol = 0
                        ) a
                        left join FIRM.D_PRIXOD1 e on a.id = e.id and a.id_shop = e.id_shop
                        left join FIRM.S_NDS b on a.art=b.art and a.asize = b.asize
                        left join (select postno,art,asize,scan,partno,price2,sum(kol) kol
                        from firm.d_sap_odgruz2
                        group by postno,art,asize,scan,partno,price2) g on g.postno = e.postno and g.art = a.art and g.asize = a.asize
                                  and (case when length(G.SCAN) < 3 then ' ' else G.SCAN end) = 
                                      (case when length(a.SCAN) < 3 then ' ' else FIRM.get_sap_scancode(a.SCAN) end)
                                  and G.PARTNO = A.PARTNO
                    left join firm.d_sap_odgruz1 h on h.postno = g.postno
                                 left join FIRM.D_RASXOD1 x on 
                    to_char(x.id) = h.postno_lifex and
                    x.id_shop = h.skladid
        --                  x.postno = e.postno and 
        --                  e.kkl = x.id_shop
                         where a.id=  /*'?1?'*/ 383 and a.id_shop= /*'?2?'*/ '0070'
                      )
                   GROUP BY ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS0,CENA0,idop,id_shop,date_d,kkl
            ) A
            
        ) B
        
    ) C LEFT JOIN S_ART B1 ON C.ART=B1.ART

)d
ORDER BY MANUFACTOR, ART,ASIZE,PROCENT