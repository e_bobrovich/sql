select d.*,
CASE WHEN SUM01!=0 THEN SUM_NAC/SUM01*100.00 ELSE 0 END AS NAC
 FROM (
    select c.*
    ,case when 1 /*2*/ /*$P!{PR_CENA}*/=0 then sum_nac0 else sum_nac1 end as sum_nac
    ,CASE WHEN 1 /*2*/ /*$P!{PR_CENA}*/=0 THEN SUM0 ELSE SUM1 END AS SUM01
    ,CASE WHEN B1.MTART='ZFRT' THEN 'СОБСТВЕННОГО ПРОИЗВОДСТВА' ELSE 'ПОКУПНЫЕ ТМЦ' END AS MANUFACTOR
    FROM  (
    
        select b.*,sum3-sum_nds-sum0 as sum_nac0,
        
        SUM3-SUM_NDS-SUM1 AS SUM_NAC1
        FROM (
        
            SELECT A.*,ROUND(SUM3/(100.00+NDS)*NDS,2) AS SUM_NDS
            ,SUM3/(100.00+PROCENT)*PROCENT  AS SUM_PROCENT
             FROM (
            
                SELECT ART,ASSORT,ASIZE,PROCENT,NEI,SUM(KOL) AS KOL
                ,cena0,sum(kol*cena0) as sum0
                ,cena1,sum(kol*cena1) as sum1
                ,cena2,sum(kol*cena2) as sum2
                ,CENA3,SUM(KOL*CENA3) AS SUM3
                ,case when nds=0 then get_nds(art,asize)  else nds end as nds
                 from d_prixod2_v@s0070
                WHERE /*REL=383*/ id = 383 AND KOL!=0
                 GROUP BY ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS,CENA0
            ) A
        
        ) B
    
    ) C LEFT JOIN S_ART B1 ON C.ART=B1.ART

)d
ORDER BY MANUFACTOR, ART,ASIZE,PROCENT;


select 
art,assort,asize,procent,nei,
SUM(KOL) AS KOL
,cena0
,sum(kol*cena0) as sum0
,cena1,sum(kol*cena1) as sum1
,cena2
,sum(kol*cena2) as sum2
,cena3
,SUM(KOL*CENA3) AS SUM3
,case when nds=0 then get_nds(art,asize)  else nds end as nds
 FROM D_PRIXOD2_V@s8
where 
--rel=$p!{rel} and 

kol!=0
 GROUP BY ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS,CENA0;
 
 select * from d_prixod2_v@s0070;