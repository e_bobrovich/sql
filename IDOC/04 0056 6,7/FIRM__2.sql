select sum(SUM_NDS), sum(SUM01), sum(sum3) from (
SELECT D.*,CASE WHEN SUM01!=0 THEN SUM_NAC/SUM01*100.00 ELSE 0 END AS NAC
 FROM (
SELECT C.*
,CASE WHEN 1=0 THEN SUM_NAC0 ELSE SUM_NAC1 END AS SUM_NAC
,CASE WHEN 1=0 THEN SUM0 ELSE SUM1 END AS SUM01
,CASE WHEN B1.MTART='ZFRT' THEN 'СОБСТВЕННОГО ПРОИЗВОДСТВА' ELSE 'ПОКУПНЫЕ ТМЦ' END AS MANUFACTOR
FROM  (

    SELECT B.*,SUM3-SUM_NDS-SUM0 AS SUM_NAC0,SUM3-SUM_NDS-SUM1 AS SUM_NAC1
    FROM (
    
        SELECT A.*,ROUND(SUM3/(100.00+NDS)*NDS,2) AS SUM_NDS
        ,SUM3/(100.00+PROCENT)*PROCENT  AS SUM_PROCENT
         FROM (
        
            SELECT ART,ASSORT,ASIZE,PROCENT,NEI,SUM(KOL) AS KOL
            ,CENA0,SUM(KOL*CENA0) AS SUM0,CENA1,SUM(KOL*CENA1) AS SUM1,CENA2,SUM(KOL*CENA2) AS SUM2,CENA3,SUM(KOL*CENA3) AS SUM3
            ,case when nds=0 then get_nds(art,asize)  else nds end as nds
             from 
             d_prixod2_v@s0076
--             select a.kol kol_new,a.* from firm.d_prixod2 a
--                  	inner join firm.d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
--                  	where c.idop in('07', '04')
            WHERE id=6 AND KOL!=0
             GROUP BY ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS,CENA0
            ) A
        
        ) B
    
    ) C LEFT JOIN S_ART B1 ON C.ART=B1.ART

)d
order by manufactor, art,asize,procent
);

--select sum(cena3*kol) from (
select ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI from firm.d_prixod2 a
inner join firm.d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
where c.id_shop = '0076' and c.id = '6'
group by ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI
--)
;

select * from d_prixod2 a
inner join d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
where c.id_shop = '0076' and c.id = '6'


;
select * from d_prixod2@s0076 a
inner join d_prixod1@s0076 c on a.id = c.id
where c.id = '6'
;

select sum(a.kol*cena3) sc from d_prixod2@s0076 a
inner join d_prixod1@s0076 c on a.id = c.id
where c.id = '6';


select sum(a.kol*cena3) from firm.d_prixod2 a
inner join firm.d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
where a.ID=  '6'  and a.id_shop= '0076';



select a.scan, b.scan, a.cena1, b.cena1, a.cena2, b.cena2, a.cena3, b.cena3 from 
(
    select * from d_prixod2@s0076 a
    inner join d_prixod1@s0076 c on a.id = c.id
    where c.id = '6'
) a
full join
(
    select * from firm.d_prixod2 a
    inner join firm.d_prixod1 c on a.id = c.id and a.id_shop = c.id_shop 
    where a.id=  '6'  and a.id_shop= '0076'
)b on a.scan = b.scan
where a.cena1 != b.cena1 or a.cena2 != b.cena2 or a.cena3 != b.cena3
;