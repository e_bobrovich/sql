select distinct 
  x.IDOP IDOP,
  case  when INSTR(x.ID_SHOP,'W') = 3 and INSTR(x.kkl,'W') = 0 THEN SUBSTR(x.kkl,0,2)||'W'||SUBSTR(x.ID_SHOP,4,1)
        when INSTR(x.kkl,'W') = 1 and INSTR(x.id_shop,'W') != 1 THEN SUBSTR(x.kkl,2,2)||SUBSTR(x.ID_SHOP,3,2) else x.id_shop end id_shop,
  x.id,IDOCS.GET_DATE_D(x.DATED) DATED,
  replace(x.NDOC,'<','Б'),
  case  WHEN INSTR(x.kkl,'W') = 3 THEN SUBSTR(replace(x.id_shop,'W',''),0,2)||'W'||SUBSTR(x.kkl,4,1) 
        else x.kkl end kkl,
  x.TEXT,
  IDOCS.GET_DATE_D(x.DATE_S) DATE_S,
  x.BIT_CLOSE,
  x.NAMEOP,
  x.POSTNO_LIFEX,
  '' IMPNUM,
  (select kval from FIRM.ST_SHOP where shopid = x.ID_SHOP ) WAERS
from (
      select  a.ID ,
              CAST (a.DATED AS TIMESTAMP(6)) DATED,
              CAST (a.DATE_CLOSE AS TIMESTAMP(6)) DATE_CLOSE,
              NDOC , 
              case  when a.KKL = ' ' and c.postvid is not null then c.skladid 
                    else a.KKL end KKL, 
              NKL ,
              TEXT ,
              a.DATE_S ,
              BIT_CLOSE ,
              IDOP ,
              NAMEOP ,
              a.POSTNO ,
              case when c.POSTNO_LIFEX is null then a.POSTNO else c.POSTNO_LIFEX end POSTNO_LIFEX,
              a.id_shop, 
              case  when a.TIMED is not null then a.TIMED 
                    else CAST (a.DATED AS TIMESTAMP(6)) end TIMED 
      from FIRM.D_PRIXOD1 a 
        inner join FIRM.D_PRIXOD2 b on a.ID = b.ID and a.ID_SHOP = b.ID_SHOP 
        left join FIRM.d_sap_odgruz1 c 
          on a.id_shop = c.shopid and a.postno = c.postno and c.PR_UD = 0
        where (select count(*) from FIRM.D_PRIXOD2 where a.id = id and a.id_shop = id_shop) != 0
      group by  a.ID , 
                DATED ,
                DATE_CLOSE ,
                NDOC , 
                case  when a.KKL = ' ' and c.postvid is not null 
                      then c.skladid else a.KKL end , 
                NKL,
                TEXT,
                a.DATE_S,
                BIT_CLOSE,
                IDOP,
                NAMEOP,
                a.POSTNO,
                c.POSTNO_LIFEX,
                a.id_shop,
                case  when a.TIMED is not null then a.TIMED 
                      else CAST (a.DATED AS TIMESTAMP(6)) end
) x 
where 
      x.ID_SHOP!='S777' and 
      x.ID_SHOP!='S888' and 
      x.ID_SHOP not like '%U%' and
      x.kkl not like '%U%' and
      x.id_shop in (select shopid from firm.s_shop where landid = 'BY') and 
      x.bit_close = 'T' and x.idop in ('01','04','07','15') 
      and x.id_shop = '0076'
      ;