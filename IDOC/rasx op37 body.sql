select id,assort,kol,cena1,cena2,cena3,
case when nr = 1 then dop_sum else ' ' end dop_sum,
case when nr = 1 then sert_list else ' ' end sert_list
from (
    select 
    row_number() over(partition by x.assort,x.id order by x.id,x.dop_sum,x.assort) as nr,
    x.id,x.assort,1 kol,x.cena1,x.cena2,x.cena3,x.dop_sum,x.sert_list 
    from 
    (
        select a.id,'Арт. '||b.art||', '||b.assort||' '||c.groupmw||(case when c.mtart = 'ZHW3' then '' else ', '||b.asize end) assort,
        b.kol,b.cena1,cena2,b.cena3,b.asize,
        case 
            when b.rowid in (select min(z.rowid) from d_rasxod2 z where z.id = a.id and z.id_shop = a.id_shop group by z.id) 
            --then to_char((select nvl(sum(d.sum),0) from d_rasxod3 d where d.id = a.id and d.id_shop = a.id_shop)) else ' ' 
            then (select nvl(sum(d.sum),0) from d_rasxod3 d where d.id = a.id and d.id_shop = a.id_shop) else 0
            end dop_sum,
        (select nvl(sum(d.sum),0) from d_rasxod3 d where d.id = a.id and d.id_shop = a.id_shop) dop_sum_1, 
        (
            select nvl(max(e.sert_list),' ') sert_list from (select id, id_shop, listagg(sert_id,', ') within group (order by fr_id_chek) as sert_list from d_rasxod4 group by id, id_shop) e where e.id = a.id and e.id_shop = a.id_shop) sert_list
        from d_rasxod1 a
        inner join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop and a.bit_close = 'T'

        and a.idop = '37' and a.id = '1715' and a.id_shop = '0004'
        inner join s_art c on b.art = c.art
        order by b.asize
    ) x
    inner join (select rownum rn from dual connect by level <= (select max(u.kol) from d_rasxod1 t 
    inner join d_rasxod2 u on t.id = u.id and t.id_shop = u.id_shop
    where t.bit_close = 'T'

    and t.idop = '37' and t.id = '1715' and t.id_shop = '0004')) z on z.rn <= x.kol
)
order by id,dop_sum,assort;



select idop,id_shop,manufactor, 
  0 standart_sum,
  sum(sum2) sum_nds,
  sum(dop_sum) sum_nac,
  sum(sum2-sum3) SUM_ROZN from 
(
    select a.idop, a.id_shop,  b.kol, b.cena1, cena2, cena3,
    (kol*cena1) as sum1,(kol*cena2) as sum2, (kol*cena3) as sum3,
    case 
        when b.rowid in (select min(z.rowid) from d_rasxod2 z where z.id = a.id and z.id_shop = a.id_shop group by z.id) 
        then (select nvl(sum(d.sum),0) from d_rasxod3 d where d.id = a.id and d.id_shop = a.id_shop) else 0
        end dop_sum,
    case when c.mtart='ZFRT' then 'S' else 'P' end as manufactor
        
    from d_rasxod1 a
    inner join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop and a.bit_close = 'T'
    
    and a.id = '1848' and a.id_shop = '0004' and a.idop in ('37')
    inner join s_art c on b.art = c.art
)
group by id_shop,manufactor,idop
;