create or replace procedure tdv_refresh_one_dk(i_shop in varchar2, i_dk in varchar2) is
   ddl_ins VARCHAR2(3000);   
   v_link varchar2(20); 
   v_land varchar2(20);
   v_scores number(18,2)  default null;
   v_sum st_dk.summ%type;
   v_dk_level st_dk.dk_level%type;
   v_dk_discount st_dk.discount%type;
   
   V_SELE varchar2(2048);
   pole varchar2(500);
begin

  select case when substr(i_shop,1,2) = '00' then 'BY' else 'RU' end into v_land from dual;

/*  update st_dk a set a.dk_type = 'B', edit_date = sysdate
  where dk_type = 'S' and scores_date is not null and  a.id_dk =  i_dk; 
  commit;

--  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'pos_sale1');
--  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'pos_sale2');
--  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'pos_sale3');
--  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'pos_sale4');
--  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'pos_dk_scores');
--  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'st_dk');
  

  begin
    select sum(a.dk_sum), b.summ, b.dk_level
    into v_scores, v_sum, v_dk_level
    from (select s1.id_dk, sum(nvl(dk_sum,0)) dk_sum
            from  pos_dk_scores s1 
            left join pos_sale1 p1 on to_char(s1.id_chek) =to_char( p1.id_chek) and s1.id_shop = p1.id_shop                                                                                                     
            where doc_type not in ('ac01','ac03') and NOT(s1.dk_sum<0 and  to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv') )
                     and NOT(s1.dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
                     and  (p1.id_chek is null or p1.bit_close = 'T')
                     and  NOT (doc_type in ('voz', 'ckv') and (s1.id_shop,s1.id_chek,s1.doc_type) in (select id_shop, id_chek, doc_type from ST_DK_AC01_VOZV))
            group by  s1.id_dk
            union all
            select id_dk,  sum(nvl(dk_sum,0)) dk_sum
            from st_dk_hand2 b2
            inner join st_dk_hand1 b1 on b2.id = b1.id   
            where b1.dk_type = 'B'    
            group by  id_dk) a
    inner join st_dk b on a.id_dk = b.id_dk
    where a.id_dk =  i_dk group by b.summ, b.dk_level; 
    
   select v_scores - nvl(abs(max(ac1.scores)),0)
   into v_scores
   from ST_DK_AC01 ac1 where i_dk = ac1.id_dk   ; 
  exception when no_data_found then
     v_scores := null; 
  end   ;




  if v_scores is not null then
     v_dk_level := greatest(GET_DK_SCORES_LEVEL(v_sum), nvl(v_dk_level,3));    
     v_dk_discount := get_dk_sc_sys_int('PROC_LEVEL', v_dk_level,v_land);
     
     update st_dk a set dk_level = v_dk_level, discount = v_dk_discount, a.scores = nvl(v_scores,0)
     where id_dk = i_dk;
     commit;
  end if ;

  update st_dk a set scores = 0 where id_dk = i_dk and scores<0;

  update st_dk a set a.discount = 10, a.dk_level = 2 where inf2 = '2' and discount < 10;*/
  
  TDV_COPY_TABLE_FROM_SHOP(i_shop, 'pos_dk_scores');

  v_link := get_shop_link(i_shop); 
  
  V_SELE:=' DELETE FROM POS_DK_SCORES WHERE ID_SHOP=:1 AND ID_DK=:2 '; -- УДАЛЯЕМ ИЗ ЦЕНТРА ПО МАГАЗИНУ НОМЕРУ ДИСКОНТНОЙ И ЧЕКУ
   EXECUTE IMMEDIATE  V_SELE USING i_shop, i_dk;  --

   pole:=get_same_fields('POS_DK_SCORES',null,'POS_DK_SCORES',v_link);
   pole:=REPLACE(pole,',"ID_SHOP"',' ');

   V_SELE:='INSERT INTO POS_DK_SCORES ('||pole||',ID_SHOP) SELECT '||pole||',:1 FROM POS_DK_SCORES@'||v_link|| ' WHERE  ID_DK=:2  '  ;
   EXECUTE IMMEDIATE  V_SELE USING i_shop, i_dk;           

   commit;
  
   dk_sum_count3(i_dk);

   
   ddl_ins:= 'merge into st_dk@'||v_link||' using (SELECT * FROM ST_DK where id_dk = :1 ) ao on ( st_dk.id_dk = ao.id_dk )
      when matched then
            update set discount = ao.discount,
              dkvid_id = ao.dkvid_id, 
              summ = ao.summ,
              BIT_BLOCK = ao.BIT_BLOCK,
              SCORES = nvl(ao.SCORES,0),
              DK_TYPE = nvl(ao.DK_TYPE,''S''),
              DK_LEVEL =  nvl(ao.DK_LEVEL,''0''),
              INF1 = ao.inf1
        when not matched then
            insert ( id_dk, fio,
                     birthday, sex,
                     country, city,
                     street, phone_number,
                     email, discount, activation_date,
                     summ, id_shop ,dkvid_id, edit_date, BIT_BLOCK, SCORES, DK_TYPE, DK_LEVEL, inf1)
            values ( ao.id_dk, ao.fio,
                     ao.birthday, ao.sex,
                     ao.country, ao.city,
                     ao.street, ao.phone_number,
                     ao.email, ao.discount, ao.activation_date,
                     ao.summ, ao.id_shop, ao.dkvid_id, ao.edit_date, ao.BIT_BLOCK, 
                     nvl(ao.SCORES,0), nvl(ao.DK_TYPE,''S''), nvl(ao.DK_LEVEL,''0''), nvl(ao.inf1,''0''))' ;
                     
  execute immediate ddl_ins using  i_dk;        
  
  commit;       
  
 exception when others then
    dbms_output.put_line(sqlcode||sqlerrm);
    rollback;
    WRITE_ERROR_PROC('tdv_refresh_one_dk', sqlcode, sqlerrm, 'i_shop = '||i_shop||' i_dk = '||i_dk, 'ночной расчет', ' ');                   
end tdv_refresh_one_dk;