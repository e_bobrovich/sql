select * from (
  select a.id_shop, cast(a.id_chek as varchar2(50)) docno, case when bit_vozvr = 'T' then 'Док. возврата' else 'Док. продажи' end doctype,
    --to_char(sale_date,'dd.mm.yyyy hh24:mi:ss') dated,  
    sale_date dated,  
    case when bit_vozvr = 'T' then -cena3 else cena3 end cena3, assort, art, asize, seller_fio
    from pos_sale1 a 
    inner join pos_sale2 b on a.id_chek = b.id_chek and a.shop_id = b.id_shop 
    where id_dk = '0000010743973' and a.bit_close = 'T' and a.shop_id!='3716'
    union all
    -- продажи по чекам в магазине ,который вызвал запрос
    select shop_id id_shop, cast(a.id_chek as varchar2(50)) docno, case when bit_vozvr = 'T' then 'Док. возврата' else 'Док. продажи' end doctype,
    sale_date dated,  
    case when bit_vozvr = 'T' then -cena3 else cena3 end cena3, assort, art, asize, seller_fio
    from pos_sale1 a 
    inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
    where id_dk = '0000010743973' and a.bit_close = 'T'  and a.shop_id='3716'
    union all
    -- документальные возвраты
    select c.id_shop, cast(c.id as varchar2(50)) docno, 'Док. возврат' doctype,
    dated, -cena3, assort, art, asize, seller_fio
    from d_prixod1 c inner join 
    d_prixod2 d on c.id = d.id and c.id_shop = d.id_shop
    where d.id_dk='0000010743973' and idop = '03' and bit_close = 'T' 
    union all
    -- продажи из 1С
    select ' ' id_shop, ' ' docno, 'Накопления из 1С' doctype,
    cast(to_date('01/01/2000','dd.mm.yyyy') as date) dated, sum(a.dk_sum) dk_sum, ' ' assort, ' ' art, 0 asize, ''
    from st_dk_1c_sale a
    where a.id_dk='0000010743973'
    union all
    -- начисление скидок вручную
    select ' ' id_shop, a.id docno, a.text doctype,
    a.dated, b.dk_sum, ' ' assort, ' ' art, 0 asize, ''
    from st_dk_hand1 a
    inner join st_dk_hand2 b on a.id = b.id
    where b.id_dk='0000010743973'
) x 
--left join pos_dk_scores y on '0000010743973' = y.id_dk  and x.id_shop = y.id_shop and x.docno = y.id_chek
;
  select * from pos_dk_scores where id_Dk = '0000010743973' and dk_sum > 0;