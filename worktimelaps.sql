select b.surname, a.date_work,a.spend_time, c.note, d.title,a.small_comment,   a.comments,  a.fk_initiator, f.responsible from WORKTIMELAPSE.TIME_SHEET a
--select b.surname,c.note, d.title,a.small_comment  from WORKTIMELAPSE.TIME_SHEET a
left join worktimelapse.users b on a.fk_user = b.tub_num
left join WORKTIMELAPSE.Contracts2 c on a.fk_contract = c.id_contract
left join worktimelapse.progress_tasks d on a.fk_progress_task = d.id_progress_task
left join WORKTIMELAPSE.initiators f on a.fk_initiator = f.id
--left join WORKTIMELAPSE.Counterparties e on c.fk_counterparty = e.title
where a.date_work >= to_date('01.10.2019', 'dd.mm.yyyy')
--group by b.surname, c.note, d.title,a.small_comment
order by b.surname, d.title,a.small_comment
--, a.date_work
;

select a.small_comment sk, a.*, b.* from worktimelapse.time_sheet a left join worktimelapse.users b on a.fk_user = b.tub_num or a.fk_user2 = b.id
where b.surname = 'Бобрович' 
--and 
--trunc(date_work, 'month') = trunc(sysdate, 'month') and instr(A.SMALL_COMMENT, 'Провед')>0
order by b.surname, a.date_work desc;

select c.note, d.title, 
NUMTODSINTERVAL(SUM(EXTRACT(DAY FROM spend_time)), 'DAY') +
NUMTODSINTERVAL(SUM(EXTRACT(HOUR FROM spend_time)), 'HOUR') +
NUMTODSINTERVAL(SUM(EXTRACT(MINUTE FROM spend_time)), 'MINUTE') +
NUMTODSINTERVAL(SUM(EXTRACT(SECOND FROM spend_time)), 'SECOND') AS "SUM(spend_time)",
listagg(small_comment||' | ') within group (order by small_comment)
from worktimelapse.time_sheet a 
left join worktimelapse.users b on a.fk_user = b.tub_num
left join WORKTIMELAPSE.Contracts2 c on a.fk_contract = c.id_contract
left join worktimelapse.progress_tasks d on a.fk_progress_task = d.id_progress_task
where b.surname = 'Бобрович' and trunc(date_work, 'month') = trunc(sysdate-30, 'month') 
and a.is_delete != 0
group by c.note, d.title
order by c.note, d.title;


select c.note, d.title, 
NUMTODSINTERVAL(SUM(EXTRACT(DAY FROM spend_time)), 'DAY') +
NUMTODSINTERVAL(SUM(EXTRACT(HOUR FROM spend_time)), 'HOUR') +
NUMTODSINTERVAL(SUM(EXTRACT(MINUTE FROM spend_time)), 'MINUTE') +
NUMTODSINTERVAL(SUM(EXTRACT(SECOND FROM spend_time)), 'SECOND') AS "SUM(spend_time)",
a.small_comment
from worktimelapse.time_sheet a 
left join worktimelapse.users b on a.fk_user = b.tub_num
left join WORKTIMELAPSE.Contracts2 c on a.fk_contract = c.id_contract
left join worktimelapse.progress_tasks d on a.fk_progress_task = d.id_progress_task
where b.surname = 'Бобрович' 
--and to_char(date_work, 'yyyymm') = '202010'
and a.is_delete != 0
group by c.note, d.title, a.small_comment
order by c.note, d.title, a.small_comment;

select * from worktimelapse.time_sheet where small_comment like ('%функционал%');

update  worktimelapse.time_sheet 
set fk_initiator = 101
where fk_user = 70033 and fk_initiator = 100;

select * from worktimelapse.users; --70024

select c.note, d.title, a.small_comment ,
NUMTODSINTERVAL(SUM(EXTRACT(DAY FROM spend_time)), 'DAY') +
NUMTODSINTERVAL(SUM(EXTRACT(HOUR FROM spend_time)), 'HOUR') +
NUMTODSINTERVAL(SUM(EXTRACT(MINUTE FROM spend_time)), 'MINUTE') +
NUMTODSINTERVAL(SUM(EXTRACT(SECOND FROM spend_time)), 'SECOND') AS "SUM(spend_time)"

from worktimelapse.time_sheet a 
left join WORKTIMELAPSE.Contracts2 c on a.fk_contract = c.id_contract
left join worktimelapse.progress_tasks d on a.fk_progress_task = d.id_progress_task
where a.fk_user2 = 70 
and to_char(date_work, 'yyyymm') = '202010' and is_delete = 1
group by c.note, d.title, a.small_comment
--order by date_work desc
;



select 
*
from worktimelapse.time_sheet a 
left join WORKTIMELAPSE.Contracts2 c on a.fk_contract = c.id_contract
left join worktimelapse.progress_tasks d on a.fk_progress_task = d.id_progress_task
where a.fk_user2 = 70 
and to_char(date_work, 'yyyymm') in ('202010','202009') and is_delete = 1;
;
