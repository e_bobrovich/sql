pos_sale2select std.id_del as id_del, std.name_del as name_del, c.count_mess as count_mess,d.count_hist as count_hist,
case when count_hist != 0 then ROUND((d.count_hist/c.count_mess)*100,2) else 0 end as count_prog,
td.start_date as date_start, sc.name_channel as channel, dp.status as status
from st_delivery std
inner join delivery_pool dp on std.id_del = dp.id_del 
left join tech_data td on std.id_del=td.id_del
left join s_channel sc on td.channel = sc.id_channel 
left join (select id_del, count(id_dk) as count_mess from delivery_dk where groupt != 'K' or groupt is null group by id_del) c on std.id_del = c.id_del
left join (select id_del, count(id_dk) as count_hist from delivery_history group by id_del) d on std.id_del = d.id_del
where std.flag_delete=0
order by std.id_del desc
