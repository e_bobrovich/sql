SELECT 
TO_DATE(TO_CHAR(CAST(A.SALE_DATE AS DATE))) AS DATED
,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
,CASE WHEN A.BIT_VOZVR='F' THEN 1 ELSE 2 END AS KOP
,CASE WHEN A.BIT_VOZVR='F' THEN  1 ELSE -1 END AS KOEF 
,A.ID_CHEK
,A.SHOP_ID
,A.SELLER_FIO
,A.SELLER_TAB
,B.ART
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
FROM POS_SALE2 B  
-- сумма чека без оплаты сертификатом
LEFT JOIN (
        select x.ID_CHEK,x.SHOP_ID,x.SALE_DATE,x.SELLER_FIO,x.SELLER_TAB,x.BIT_CLOSE,x.BIT_VOZVR
        from pos_sale1 x
        left join (select id_chek, id_shop, sum(sum) sum from pos_sale3 where vid_op_id != 3 group by id_chek, id_shop) C on x.id_chek = c.id_chek and x.shop_id = c.id_shop
) A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
WHERE A.BIT_CLOSE='T'
and a.sale_date>=to_date('01.09.2018','dd.MM.yyyy') 
and a.sale_date<=to_date('30.09.2018','dd.MM.yyyy')
;

SELECT 
GET_VOZVRAT(A2.ID, C.SHOP_ID) AS DATED
,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
,3 AS KOP
,-1 AS KOEF
,A2.ID AS ID_CHEK
,C.SHOP_ID
,B2.SELLER_FIO
,B2.SELLER_TAB
,B2.ART
,0 AS SUMP1
,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
, CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
, CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
, 0  AS KOLP 
, B2.KOL AS KOLV
FROM D_PRIXOD2 B2
LEFT JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
LEFT JOIN POS_SALE1 C ON A2.NDOC=TO_CHAR(C.ID_CHEK)  and a2.id_shop = c.id_shop
WHERE
((GET_VOZVRAT(A2.ID,C.SHOP_ID,3) IS not NULL and A2.IDOP in ('03','14','19','42')) or (GET_VOZVRAT(A2.ID,C.SHOP_ID,1) IS not NULL and A2.IDOP not in ('03','14','19','42')))  
AND A2.BIT_CLOSE='T' 
and C.sale_date>=to_date('01.08.2018','dd.MM.yyyy') 
and C.sale_date<=to_date('31.08.2018','dd.MM.yyyy')
;

select 
a.DATED
,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
,4 AS KOP
,1 AS KOEF
,a.ID AS ID_CHEK
,b.id_shop
,b.SELLER_FIO
,b.seller_tab
,b.art ART
,0 AS SUMP1
,0 AS SUMV1 
,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
,0 AS SUMV 
,0  AS KOLP
, 0 AS KOLV
from d_rasxod1 a

inner join (
        select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2
        from (
            select id, art, seller_fio, seller_tab, id_shop, sum(kol*cena3) sum from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop
        ) x
        left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
        ) y on x.id = y.id and x.id_shop = y.id_shop
) b on a.id = b.id and a.id_shop = b.id_shop

where a.bit_close = 'T' and a.idop in ('34','35','37','38','39','40')
and a.DATED>=to_date('01.08.2018','dd.MM.yyyy') 
and a.DATED<=to_date('31.08.2018','dd.MM.yyyy')
;

select * from pos_sale1 where id_shop = '0031' and id_chek = 1613;

select get_vozvrat(414, '0063') from dual;
SELECT MAX(NUMCONF) FROM CONFIG WHERE SHOPID = '0063';
SELECT *  FROM D_PRIXOD1 WHERE ID=621 and id_shop = '0063';
SELECT MAX(TIP) FROM ST_OP WHERE IDOP=V_IDOP AND (NUMCONF=V_NUMCONF OR NUMCONF=0);
SELECT * FROM BEP_D_PRIXOD4_PREM_V WHERE ID=621 and id_shop = '0063';

SELECT *  FROM D_PRIXOD1 WHERE id_shop = '0063' ;

