select * from s_channel;

select fil.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other, nvl(al.colvo,0) as all_prod from 
  (
  select b.shopparent from (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b
  where b.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  group by b.shopparent
  order by b.shopparent
) fil 
left join (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join delivery_history dh on dd.id_del = dh.id_del and dd.id_dk = dh.id_dk
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where b.prodh like '0001%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F'
 and (dd.groupt = 'O' or dd.groupt is null)
and (dh.id_status = 5 or dh.id_status = 6)
and (dh.id_channel = 1) --viber
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
) sh on fil.shopparent = sh.shopparent
left join 
(
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join delivery_history dh on dd.id_del = dh.id_del and dd.id_dk = dh.id_dk
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where  b.prodh like '00020038%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F' and (dd.groupt = 'O' or dd.groupt is null)
and (dh.id_status = 5 or dh.id_status = 6)
and (dh.id_channel = 1) --viber
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )bg on fil.shopparent = bg.shopparent
left join 
  (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join delivery_history dh on dd.id_del = dh.id_del and dd.id_dk = dh.id_dk
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where b.prodh not like '0001%' and b.prodh not like '00020038%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F' and (dd.groupt = 'O' or dd.groupt is null)
and (dh.id_status = 5 or dh.id_status = 6)
and (dh.id_channel = 1) --viber
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )sp on fil.shopparent = sp.shopparent  
left join 
  (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join delivery_history dh on dd.id_del = dh.id_del and dd.id_dk = dh.id_dk
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F' and (dd.groupt = 'O' or dd.groupt is null)
and (dh.id_status = 5 or dh.id_status = 6)
and (dh.id_channel = 1) --viber
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )al on fil.shopparent = al.shopparent  
  order by fil.shopparent
;





select fil.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other, nvl(al.colvo,0) as all_prod from 
  (
  select b.shopparent from (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b
  where b.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  group by b.shopparent
  order by b.shopparent
) fil 
left join (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join delivery_history dh on dd.id_del = dh.id_del and dd.id_dk = dh.id_dk
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where b.prodh like '0001%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F'
 and dd.groupt = 'K'
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
) sh on fil.shopparent = sh.shopparent
left join 
(
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where  b.prodh like '00020038%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F' and dd.groupt = 'K'
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )bg on fil.shopparent = bg.shopparent
left join 
  (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where b.prodh not like '0001%' and b.prodh not like '00020038%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F' and dd.groupt = 'K'
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )sp on fil.shopparent = sp.shopparent  
left join 
  (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (4544) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
       and to_char(d.sale_date,'yyyyMMdd') >= to_char(to_date('07.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and to_char(d.sale_date,'yyyyMMdd') <= to_char(to_date('13.02.2019','dd.MM.yyyy'),'yyyyMMdd')
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F' and dd.groupt = 'K'
       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )al on fil.shopparent = al.shopparent  
  order by fil.shopparent
;