--- FORMATE SQL IN CLIENT
select
    cast(regexp_substr('1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31','[^;]+', 1, level) as number(2)) days,
    cast(regexp_substr('1000,5;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;9500;2700;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63','[^;]+', 1, level) as number(18,2)) plan_sum,
    cast(regexp_substr('45;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;40;22;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54','[^;]+', 1, level) as number(18,2)) plan_pair
from dual
connect by cast(regexp_substr('1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31', '[^;]+', 1, level) as number(2)) is not null
       and cast(regexp_substr('1000,5;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;9500;2700;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63;2922,63', '[^;]+', 1, level) as number(18,2)) is not null
       and cast(regexp_substr('45;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;40;22;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54;21,54', '[^;]+', 1, level) as number(18,2)) is not null
;

-------

---RKV PARAM PARSE

select 
str temp,
instr(str,'~',1,1) ps1,
instr(str,'~',1,2) pe1,
instr(str,'~',1,2)-instr(str,'~',1,1) pl1,
substr(str, instr(str,'~',1,1)+1,instr(str,'~',1,2)-instr(str,'~',1,1)-1) p1,

instr(str,'~',1,2) ps2,
instr(str,'~',1,3) pe2,
instr(str,'~',1,3)-instr(str,'~',1,2) pl2,
substr(str, instr(str,'~',1,2)+1,instr(str,'~',1,3)-instr(str,'~',1,2)-1) p2,

instr(str,'~',1,3) ps3,
instr(str,'~',1,4) pe3,
instr(str,'~',1,4)-instr(str,'~',1,3) pl3,
substr(str, instr(str,'~',1,3)+1,instr(str,'~',1,4)-instr(str,'~',1,3)-1) p3,

instr(str,'~',1,4) ps4,
substr(str, instr(str,'~',1,4)+1) p4
from (
    select 'E02~1.1.2019~D~2000.55~10.2' str from dual
)
;


select 
str temp,
to_date(substr(str, instr(str,'~',1,1)+1,instr(str,'~',1,2)-instr(str,'~',1,1)-1),'dd.mm.yyyy') p1,
substr(str, instr(str,'~',1,2)+1,instr(str,'~',1,3)-instr(str,'~',1,2)-1) p2,
to_number(substr(str, instr(str,'~',1,3)+1,instr(str,'~',1,4)-instr(str,'~',1,3)-1)) p3,
to_number(substr(str, instr(str,'~',1,4)+1)) p4
from (
    select 'E02~1.1.2019~D~2000.55~10.2' str from dual
)
;

select 
str temp,
cast(substr(str, instr(str,'~',1,1)+1,instr(str,'~',1,2)-instr(str,'~',1,1)-1) as number(18,2)) p1,
cast(substr(str, instr(str,'~',1,2)+1) as number(18,2)) p2
from (
    select 'E01~1~2019' str from dual
)
;

select bep_prem_get_day_shop_plan(
            'S888',
            cast(substr('E01~1~2019', instr('E01~1~2019','~',1,1)+1,instr('E01~1~2019','~',1,2)-instr('E01~1~2019','~',1,1)-1) as number),
            cast(substr('E01~1~2019', instr('E01~1~2019','~',1,2)+1) as number)
        )
from dual;

call bep_prem_uid_day_shop_plan(
    '0001', 
    to_date(substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,1)+1,instr('E02~01.01.2019~I~1000.50~45','~',1,2)-instr('E02~01.01.2019~I~1000.50~45','~',1,1)-1),'dd.mm.yyyy'),
    substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,2)+1,instr('E02~01.01.2019~I~1000.50~45','~',1,3)-instr('E02~01.01.2019~I~1000.50~45','~',1,2)-1),
    cast(substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,3)+1,instr('E02~01.01.2019~I~1000.50~45','~',1,4)-instr('E02~01.01.2019~I~1000.50~45','~',1,3)-1) as number),
    cast(substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,4)+1) as number)
);

call bep_prem_uid_day_shop_plan('0001', to_date('01.01.2019', 'dd.mm.yyyy'), 'I', 1000.50,45);

select 
    '0001', 
    to_date(substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,1)+1,instr('E02~01.01.2019~I~1000.50~45','~',1,2)-instr('E02~01.01.2019~I~1000.50~45','~',1,1)-1),'dd.mm.yyyy'),
    substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,2)+1,instr('E02~01.01.2019~I~1000.50~45','~',1,3)-instr('E02~01.01.2019~I~1000.50~45','~',1,2)-1),
    to_number(substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,3)+1,instr('E02~01.01.2019~I~1000.50~45','~',1,4)-instr('E02~01.01.2019~I~1000.50~45','~',1,3)-1)),
    to_number(substr('E02~01.01.2019~I~1000.50~45', instr('E02~01.01.2019~I~1000.50~45','~',1,4)+1))
from dual;
-----------------------

select 
to_number('100.5') 
from dual;



select day||';'||plan_sum||';'||plan_pair data 
from (
    select  listagg(day, ',') within group (order by day) day,
            listagg(''''||plan_sum||'''', ',') within group (order by day) plan_sum,
            listagg(''''||plan_pair||'''', ',') within group (order by day) plan_pair
    from (select * from bep_prem_day_shop_plan where id_shop = 'S888' and year = 2019 and month = 2 order by date_n)
);



select date_n, b.id_shop shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
nvl(e.plan_sum, d.plan_sum) plan_sum,
nvl(e.plan_pair, d.plan_pair) plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||1||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 1 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date('1'||'.'||'2019','mm.yyyy')))
) a 
inner join bep_prem_shop_plan b on b.id_shop = b.id_shop and a.month = b.month and a.year = b.year
left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
order by shop, date_n
;






call bep_calc_def_day_shop_plan(2019,2,'S888');

-- вьювер со всеми датами и подставленными значениями плана
select date_n, b.id_shop shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
nvl(e.plan_sum, d.plan_sum) plan_sum,
nvl(e.plan_pair, d.plan_pair) plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||1||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 1 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date('1'||'.'||'2019','mm.yyyy')))
) a 
inner join bep_prem_shop_plan b on lpad('0001', 4, '0') = b.id_shop and a.month = b.month and a.year = b.year
left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
order by date_n
;


select  1 + level - 1 day, 1, 2019 from dual connect by level <= extract(day from last_day(to_date('1'||'.'||'2019','mm.yyyy')));

select date_n, 
d.plan_sum default_plan_sum, d.plan_pair default_plan_pair,
e.plan_sum edit_plan_sum, e.plan_pair edit_plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||1||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 1 month, 2019 year from dual connect by level <= extract(day from last_day(to_date('1'||'.'||'2019','mm.yyyy')))
) a 
left join bep_prem_def_day_shop_plan d on '0001' = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on '0001' = e.id_shop and a.date_n = trunc(e.edit_date)
;

-- перезаполнение дефолтной таблицы

select a.id_shop, a.year, a.month, 
cast((a.plan_sum-b.plan_sum)/(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-b.edit_days) as number(18,2)) as def_day_plan_sum, 
cast((a.plan_pair-b.plan_pair)/(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-b.edit_days) as number(18,2)) as def_day_plan_pair
from bep_prem_shop_plan a
left join 
(
    select id_shop, extract(year from edit_date) year, 
    extract(month from edit_date) month,
    sum(plan_sum) plan_sum,
    sum(plan_pair) plan_pair,
    count(1) as edit_days
    from bep_prem_edit_day_shop_plan group by id_shop,
    extract(year from edit_date), extract(month from edit_date)
)b on a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
where a.id_shop = '0001'
and a.year = 2019 and a.month = 1
;


select a.id_shop, a.year, a.month, 
(a.plan_sum-b.plan_sum) as rest_sum, 
(a.plan_pair-b.plan_pair) as rest_pair,
(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-b.edit_days) as rest_days
--,
--a.plan_sum, a.plan_pair, b.plan_sum as edit_days_sum, b.plan_pair as edit_days_pair, b.edit_days 
from bep_prem_shop_plan a
left join 
(
    select id_shop, extract(year from edit_date) year, 
    extract(month from edit_date) month,
    sum(plan_sum) plan_sum,
    sum(plan_pair) plan_pair,
    count(1) as edit_days
    from bep_prem_edit_day_shop_plan group by id_shop,
    extract(year from edit_date), extract(month from edit_date)
)b on a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
where a.id_shop = '0001'
and a.year = 2019 and a.month = 1
;

select id_shop, extract(year from edit_date) year, 
extract(month from edit_date) month,
sum(plan_sum) plan_sum,
sum(plan_pair) plan_pair,
count(1) as edit_days
from bep_prem_edit_day_shop_plan group by id_shop,
extract(year from edit_date), extract(month from edit_date)
;


select 
id_shop, 
cast(plan_sum/extract(day from last_day(sysdate)) as number(18,2)) day_plan_sale, 
cast(plan_pair/extract(day from last_day(sysdate)) as number(18,2)) day_plan_pair 
from bep_prem_shop_plan 
where id_shop = '0001' 
and year = 2019
and month = 1
;

insert into bep_prem_def_day_shop_plan
select a.id_shop, a.year, a.month, 
cast((nvl(a.plan_sum, 0)-nvl(b.plan_sum,0))/(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-nvl(b.edit_days,0)) as number(18,2)), 
cast((nvl(a.plan_pair,0)-nvl(b.plan_pair,0))/(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-nvl(b.edit_days,0)) as number(18,2))
from bep_prem_shop_plan a
left join 
(
    select id_shop, extract(year from edit_date) year, 
    extract(month from edit_date) month,
    sum(plan_sum) plan_sum,
    sum(plan_pair) plan_pair,
    count(1) as edit_days
    from bep_prem_edit_day_shop_plan group by id_shop,
    extract(year from edit_date), extract(month from edit_date)
)b on a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
where a.id_shop = '0001'
and a.year = 2019 and a.month = 1
;
--------------------------------------------------------------------------------
select date_n, b.id_shop id_shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
nvl(e.plan_sum, d.plan_sum) plan_sum,
nvl(e.plan_pair, d.plan_pair) plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||2||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, extract(month from sysdate) month, extract(year from sysdate) year 
    from dual connect by level <= extract(day from last_day(to_date(2||'.'||2019,'mm.yyyy')))
) a 
inner join bep_prem_shop_plan b on b.id_shop = b.id_shop and a.month = b.month and a.year = b.year
left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
where b.id_shop = 'S888'
order by b.id_shop, a.date_n
;

select day||':::'||plan_sum||':::'||plan_pair data 
    from (
        select  listagg(day, ';') within group (order by day) day,
                listagg(plan_sum, ';') within group (order by day) plan_sum,
                listagg(plan_pair, ';') within group (order by day) plan_pair
        from (
            select date_n, b.id_shop id_shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
            nvl(e.plan_sum, d.plan_sum) plan_sum,
            nvl(e.plan_pair, d.plan_pair) plan_pair
            from 
            (
                select to_date(( 1 + level - 1)||'.'||2||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, extract(month from sysdate) month, extract(year from sysdate) year 
                from dual connect by level <= extract(day from last_day(to_date(2||'.'||2019,'mm.yyyy')))
            ) a 
            inner join bep_prem_shop_plan b on b.id_shop = b.id_shop and a.month = b.month and a.year = b.year
            left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
            left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
            where b.id_shop =  'S888' and b.year = 2019 and b.month = 2
            order by a.date_n             
        )
    );