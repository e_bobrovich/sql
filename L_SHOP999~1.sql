SET SERVEROUTPUT ON;

select * from st_dk where id_dk = '0000000000126';
select * from pos_dk_scores where id_dk = '0000000000125' order by dates, id_sale, dk_sum;
select * from pos_sale1 where id_dk = '0000000000128' order by sale_date;
select * from ri_stored_proc_error order by er_time desc;

select * from ri_stored_proc_error order by er_time desc;

select a.*
from pos_dk_stock1 a 
where sysdate between trunc(a.dateb) and trunc(a.datee) and scores > 0
and when_add = 'DK'
and landid = case when 1 = 1 then 'RU' when 1 = 2 then 'BY' end       
and '0000000000125' = case when (select 1 from pos_dk_stock2 where id = a.id group by id) = 1 
                           then (select id_dk from pos_dk_stock2 where id = a.id and id_dk = '0000000000125')
                           else '0000000000125' end
;

select to_date(
    case when to_char(birthday,'dd.mm') = '29.02' then '28.02' else to_char(birthday,'dd.mm') end 
    ||'.'||to_char(sysdate,'yyyy'),'dd.mm.yyyy'
) as dr
from st_dk where id_dk = '0000000000125';

select firm_dev.get_scores('0000002203447') from dual;

select id_dk from pos_dk_stock2 where id = 'TEST0094' and id_dk = '0000000000125';
select 1 from pos_dk_stock2 where id = 'TEST0094' group by id;

select * from pos_dk_stock3 a where a.id = 'TEST0093'; 

select 
substr(a.field_name, 0, instr(a.field_name, '.')-1) table_name, 
a.field_name, a.value, a.relation
from pos_dk_stock3 a
where a.id = 'TEST0093';

select count(*) from st_dk where id_dk = '0000000000125'  and( ST_DK.SEX = 1  or ST_DK.SEX = 2  and ST_DK.SUMM = 1);

select sysdate, a.* from st_dk a where trunc(a.activation_date) = trunc(sysdate)-18;
select trunc(activation_date) - trunc(sysdate) from st_dk where id_dk = '0000000000124';


select case when sysdate between sysdate and sysdate then 1 else 0 end from dual;
select case when sysdate between trunc(sysdate) and trunc(sysdate) then 1 else 0 end from dual;

select case when trunc(sysdate) between trunc(sysdate) and trunc(sysdate) then 1 else 0 end from dual;

select listagg(' and '||field_name||' '||relation||' '||value||' ') within group (order by field_name)
from pos_dk_stock3 a
where a.id = 'TEST0093';

call bep_dk_scores_add_card();
--to_systimestamp('02.01.18','dd.mm.yy')

select 
--count(*)
birthday
from st_dk where id_dk = '0000000000546'  and trunc(to_date(
case when to_char(st_dk.birthday,'dd.mm') = '29.02' then '28.02' else to_char(birthday,'dd.mm') end 
||'.'||to_char(add_months(sysdate, case when to_char(st_dk.birthday,'mmdd') > '1225' then -12 else 0 end),'yyyy'),'dd.mm.yyyy'
)+6) >= trunc(sysdate)  and trunc(to_date(
case when to_char(st_dk.birthday,'dd.mm') = '29.02' then '28.02' else to_char(birthday,'dd.mm') end
||'.'||to_char(add_months(sysdate, case when to_char(st_dk.birthday,'mmdd') > '1225' then -12 else 0 end),'yyyy'),'dd.mm.yyyy'
)) <= trunc(sysdate) 

;

select 
--count(*)
birthday,
'     ',
trunc(to_date(
case when to_char(st_dk.birthday,'dd.mm') = '29.02' then '28.02' else to_char(birthday,'dd.mm') end 
||'.'||to_char(add_months(to_timestamp('01.01.18','dd.mm.yy'), case when to_char(st_dk.birthday,'mmdd') > '1225' then -12 else 0 end),'yyyy'),'dd.mm.yyyy')+6) as "vdate1",
' >= ',
trunc(to_timestamp('01.01.18','dd.mm.yy')) as "sysdate1",
'     ',
trunc(to_date(
case when to_char(st_dk.birthday,'dd.mm') = '29.02' then '28.02' else to_char(birthday,'dd.mm') end
||'.'||to_char(add_months(to_timestamp('01.01.18','dd.mm.yy'), case when to_char(st_dk.birthday,'mmdd') > '1225' then -12 else 0 end),'yyyy'),'dd.mm.yyyy' )) as "vdate2",
' <= ',
trunc(to_timestamp('01.01.18','dd.mm.yy')) as "sysdate1" 

from st_dk where id_dk = '0000000000125' 
;

select sysdate - 6 from dual;

call u_test.run_tests_bep_dk_scores_add();
dbms_output.enable;

select * from st_dk where id_dk in ('0000000000155','0000000000129','0000000000882','0000000000884','0000000000125','0000000000546','0000000000126');
select * from pos_dk_scores where id_dk in ('0000000000155','0000000000129','0000000000882','0000000000884','0000000000125','0000000000546') order by dates;

select * from pos_dk_scores a where a.id_dk = '0000000000129' 
                and a.id_chek in ('TEST0099','TEST0095','TEST0094') and trunc(a.dates) = trunc(sysdate);

select case when res = 3 then 'T' else 'F' end 

            from (
                select count(*) res  from pos_dk_scores a where a.id_dk = '0000000000129' 
                and a.id_chek in ('TEST0099','TEST0095','TEST0094') and trunc(a.dates) = trunc(sysdate)
            );

select a.*
from pos_dk_stock1 a 
where trunc(sysdate) between trunc(a.dateb) and trunc(a.datee) and a.scores > 0
and a.when_add = 'DK_SCORES_ADD_CARD'
and case when a.dkvid_id is null then '003' else a.dkvid_id end = '003'
and a.landid = case when 1 = 1 then 'RU' when 1 = 2 then 'BY' end           
and '0000000000126' = case when (select 1 from pos_dk_stock2 where id = a.id group by id) = 1 
                   then (select id_dk from pos_dk_stock2 where id = a.id and id_dk = '0000000000126')
                   else '0000000000126' end
;

delete from pos_dk_scores where id_chek in ('TEST0099','TEST0095','TEST0094') and id_dk = '0000000000129';

select * from pos_sale1 where id_dk != ' ' and id_chek in (select id_chek
      from pos_sale3 a
      where a.vid_op_id IN (4)) order by sale_date desc;

select get_dk_sc_sys_str('DOCID') from dual;
select action_pricelist('88265',sysdate) from dual;

select a.*, b.id_sale as minus_scores, y.docid from pos_sale2 a
                        left join (select * from pos_sale4 x where x.disc_type_id = 'A000000001' and x.discount = 0 and x.discount_sum !=0) b on a.id_sale = b.id_sale
                        left join (select art, max(docid) docid from pos_skidki2 y where y.docid in ('B200000037','B200000152') group by art) y on y.art = a.art
                        where a.id_chek = 32223  
                                 and a.art not in (select a1.art from st_sertif a1);
                                 
                                                                  
                                 