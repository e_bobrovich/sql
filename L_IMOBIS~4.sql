select count(*) from v_st_dk
where   
  v_st_dk.id_dk not in 
   (select id_dk from firm.pos_sale1
     inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
     inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
     where firm.pos_sale1.id_dk != ' ' and 
     FIRM.S_ART.SEASON in ( 'Утепленная','Зимняя') and 
     (firm.pos_sale1.sale_date >= to_date('01.01.2017', 'dd.mm.yyyy'))
   )
            
and v_st_dk.id_shop in (select a.value1 from delivery_filters a where a.id_del = 2441 and a.field_name = 'ID_SHOP');

select count(*) from v_st_dk_t
where   
  v_st_dk_t.id_dk not in 
   (select id_dk from firm.pos_sale1
     inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
     inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
     where firm.pos_sale1.id_dk != ' ' and 
     FIRM.S_ART.SEASON in ( 'Утепленная','Зимняя') and 
     (firm.pos_sale1.sale_date >= to_date('01.01.2017', 'dd.mm.yyyy'))
   )
and v_st_dk_T.id_shop in (select a.value1 from delivery_filters a where a.id_del = 2441 and a.field_name = 'ID_SHOP');


select * from delivery_history where trunc(date_sent) > trunc(sysdate)-60;

select * from firm.st_dk;


CREATE TABLE sales_t
( id_chek NUMBER
, id_dk varchar(20)
, sale_date DATE
, id_shop number
, sale_sum NUMBER
);

insert into sales
( id_chek
, id_dk
, sale_date
, id_shop
, sale_sum
)
values 
(1 ,'1',sysdate-10,1,1),
(2 ,'2',sysdate-30,1,2),
(3 ,'3',sysdate-150,3,3),
(4 ,'4',sysdate-250,4,4),
(5 ,'5',sysdate-380,5,5),
(6 ,'6',sysdate-400,6,6),
(7 ,'7',sysdate-630,7,7),
;

insert into sales values (15 ,'15',sysdate-790,15,1
);

select * from sales PARTITION  (p0);
select * from sales_t;

insert into sales_t
select * from sales
;

select to_char(sale_date, 'yyyymm') from firm.pos_sale1 group by to_char(sale_date, 'yyyymm') order by to_char(sale_date, 'yyyymm');

select * from sales where trunc(sale_date) = trunc(to_date('18.02.2017', 'dd.mm.yyyy'));
select * from sales_t where trunc(sale_date) = trunc(to_date('18.02.2017', 'dd.mm.yyyy'));
