create or replace procedure bep_dk_scores_add_card(i_id_dk in varchar2) is
    v_dateb date;
    v_datee date;
    v_date_per date;
    v_act_date st_dk.activation_date%type;
    v_sc_date st_dk.scores_date%type;
    v_count integer;
    v_dk_vid st_dk.dkvid_id%type;
    v_other_disc d_kassa_disc_cur.discount_sum%type;
    v_numconf config.numconf%type;
    v_birthday number;
    v_scores_start_date date;
    v_dk_type st_dk.dk_type%type;
    v_id_shop config.shopid%type;
    
    v_sql_st varchar2(4000) := '';
    v_sql_where varchar2(4000);
    v_table_name varchar2(200) := '';
    v_field_name varchar2(200) := '';
    v_value varchar2(200) := '';
    v_relation varchar2(20) := '';
begin

    begin 
        select  a.activation_date, a.scores_date, a.dkvid_id, a.dk_type
        into v_act_date, v_sc_date, v_dk_vid, v_dk_type
        from st_dk a
        where a.id_dk = i_id_dk;

        exception when no_data_found then
        return;
    end ;  

    v_scores_start_date := to_date(get_system_val('DK_START_SCORES_DATE'),'dd.mm.yyyy');

    if v_scores_start_date is null or trunc(v_scores_start_date) > trunc(sysdate) then
        return;
    end if;  

    select numconf, shopid 
    into v_numconf, v_id_shop
    from config;

    -- ДЕНЬ РОЖДЕНИЯ
    v_birthday := case when v_numconf = 2 then 15 else 500 end;
    
    select 
    to_date(
        case when to_char(birthday,'dd.mm') = '29.02' then '28.02' else to_char(birthday,'dd.mm') end 
        ||'.'||to_char(sysdate,'yyyy'),'dd.mm.yyyy'
    ) as dr 
    into v_dateb
    from st_dk where id_dk = i_id_dk;
    
    if to_char(v_dateb + 6, 'yyyy')  != to_char(v_dateb,'yyyy') then 
        v_dateb := add_months(v_dateb, -12);
    end if;  
    
    select count(*) 
    into v_count
    from POS_DK_SCORES a 
    where a.id_chek = 'SC100001' and a.doc_type = 'ac' and a.id_dk = i_id_dk and to_char(v_dateb,'yyyy') = to_char(a.dates,'yyyy');    
    
    if  trunc(sysdate) between trunc(v_dateb) and trunc(v_dateb)+6 and v_count = 0  then 
        insert into pos_dk_scores x (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, x.id_ac) 
        values (i_id_dk, 'ac', 'SC100001', to_char(sysdate,'yymmddhhmi'), v_birthday, v_dateb, 'SC100001');
          
        update st_dk a 
        set a.edit_date = sysdate,
            a.scores = a.scores + v_birthday
        where id_dk =  i_id_dk;            
        
        commit;       
    end if; 
    
    /*
    TODO: owner="tyshevichdv" category="Optimize" priority="2 - Medium" created="20.11.2017"
    text="если баллы могут начисляться в разных процедурах (я про  when_add = 'DK'), 
    то может быть стоит сделать отдельную процедуру для начисления. 
    А параметром в неё передавать в том числе и имя процедуры, которая её вызвала. 
    А в параметр when_add тоже записывать имя процедуры ""DK_SCORES_ADD_CARD"". 
    Тогда всё начисление баллов будет в одном месте"
    */
    
    -- АКЦИИ
    for r_ac in (
        -- выборка акций подходящих по dateb, datee, dkvid_id, v_numconf
        -- если акция есть в pos_dk_stock2,  то проверяется вхождение дк в список
        select a.*
        from pos_dk_stock1 a 
        where trunc(sysdate) between trunc(a.dateb) and trunc(a.datee) and scores > 0
        and when_add = 'DK'
        and case when a.dkvid_id is null then v_dk_vid else a.dkvid_id end = v_dk_vid
        and landid = case when v_numconf = 1 then 'RU' when v_numconf = 2 then 'BY' end           
        and i_id_dk = case when (select 1 from pos_dk_stock2 where id = a.id group by id) = 1 
                           then (select id_dk from pos_dk_stock2 where id = a.id and id_dk = i_id_dk)
                           else i_id_dk end
    ) loop
        --dbms_output.put_line('r_ac.id : '||r_ac.id);
        -- если дата попадает в период действия акции
        /*
        TODO: owner="tyshevichdv" category="Optimize" priority="2 - Medium" created="20.11.2017"
        text="зачем здесь снова проверка на дату , если она уже есть в запросе выше
              "
        */
        if to_char(sysdate, 'yyyymmdd') >= to_char(r_ac.dateb, 'yyyymmdd') and  to_char(sysdate, 'yyyymmdd') <= to_char(r_ac.datee, 'yyyymmdd')
        then
            -- то считаем количество начислений по текущей акции
            select count(*) into v_count from pos_dk_scores a 
            where a.id_chek = r_ac.id and a.doc_type = 'ac' and a.id_dk = i_id_dk;
            
        end if;

        -- если не было начислений по акции
        if v_count = 0 then      
                
                /*
                TODO: owner="tyshevichdv" category="Optimize" priority="2 - Medium" created="20.11.2017"
                text="зачем в запросе ниже
                      substr(a.field_name, 0, instr(a.field_name, '.')-1)
                      если оно не используется?"
                */
                
                /*
                TODO: owner="tyshevichdv" created="20.11.2017"
                text="цикл ниже можно заменить одним запросом
                      select listagg(' and '||field_name||' '||relation||' '||value||' ') within group (order by field_name)
                      into v_sql_where
                      from pos_dk_stock3 a
                      where a.id = r_ac.id;"
                */     
                select listagg(' and '||field_name||' '||relation||' '||value||' ') within group (order by field_name)
                into v_sql_where
                from pos_dk_stock3 a
                where a.id = r_ac.id;
        
                for r_where in (
                    -- выборка названия таблицы, название поля, значения и отношения
                    select 
                    --substr(a.field_name, 0, instr(a.field_name, '.')-1),
                    a.field_name, a.value, a.relation
                    from pos_dk_stock3 a
                    where a.id = r_ac.id
                )
                loop
                    -- формирование строки where
                    v_sql_where := v_sql_where ||' and '||r_where.field_name||' '||r_where.relation||' '||r_where.value||' ';   
                end loop;
                
                /*
                TODO: owner="tyshevichdv" category="Optimize" priority="2 - Medium" created="20.11.2017"
                text="Конструкция If содержит дублирующий код. Вынести его за IF"
                */
                
                -- если where не пустое
                if v_sql_where is not null
                then
                    /*
                    TODO: owner="tyshevichdv" category="Optimize" priority="2 - Medium" created="20.11.2017"
                    text="в эту систему же можно положить и дни рождения?"
                    */
                   
                    -- то формируем весь запрос (доп условия пока что только по таблице st_dk)
                    v_sql_st := 'select count(*) from st_dk where id_dk = '''||i_id_dk||''' '||v_sql_where;
                    --dbms_output.put_line(v_sql_st);
                    -- выполняем запрос на проверку дк по условиям
                    execute immediate v_sql_st into v_count;
                    -- если дк подходит
                    if  v_count != 0 then
                        --то вставляем а историю и плюсуем баллы
                        --dbms_output.put_line('ADD SCORE !!!');
                        insert into pos_dk_scores (id_dk, doc_type, id_chek, id_sale, dk_sum, id_ac) 
                        values (i_id_dk, 'ac', r_ac.id, ' ', r_ac.scores, r_ac.id);
                        
                        update st_dk a 
                        set a.edit_date = sysdate,
                            a.scores = a.scores + r_ac.scores
                        where id_dk = i_id_dk;
                    end if;
                else
                    -- когда нет доп условий
                    ----то вставляем а историю и плюсуем баллы
                    --dbms_output.put_line('simple');
                    insert into pos_dk_scores (id_dk, doc_type, id_chek, id_sale, dk_sum, id_ac) 
                    values (i_id_dk, 'ac', r_ac.id, ' ', r_ac.scores, r_ac.id);
                    
                    update st_dk a 
                    set a.edit_date = sysdate,
                        a.scores = a.scores + r_ac.scores
                    where id_dk = i_id_dk;
                end if;

            --dbms_output.put_line('r_ac.scores : '||r_ac.scores);
        end if;

    end loop;
    
    commit;

    exception when others then 
        dbms_output.put_line(sqlcode||sqlerrm);
        rollback;
        WRITE_ERROR_PROC('bep_dk_scores_add', sqlcode, sqlerrm, ' i_id_dk = '||i_id_dk, ' ', ' ');     
        raise;

end bep_dk_scores_add_card;
