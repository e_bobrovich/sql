 select sum(fact_sum) from FIRM_DEV.bep_seller_prem where id_shop = '0063' and year = 2018 and month = 8;
 
 update firm_dev.BEP_SHOP_PLAN_TO
 set fact_sum = (select sum(fact_sum) from FIRM_DEV.bep_seller_prem where id_shop = '0063' and year = 2018 and month = 8)
 where id_shop = '0063' and year = 2018 and month = 8;
 
 select * from firm_dev.BEP_SHOP_PLAN_TO where id_shop = '0063' and year = 2018 and month = 8;
 --------------------------------
 -- расчет индивидуадьной премии по группе товаров
 --------------------------------
update FIRM_DEV.bep_seller_prem a
set
a.prem_individ_group = a.fact_sum * (case when a.group_t = 'Обувь' then 1.3 when a.group_t = 'Сопутка' then 3 end) / 100 
--where id_shop = '0063' and year = 2018 and month = 8
;
 --------------------------------
 -- расчет полной индивидуальной премии
 --------------------------------
update FIRM_DEV.bep_seller_prem a
set
a.prem_individ_full = (
                        select sum(b.prem_individ_group)
                        from FIRM_DEV.bep_seller_prem  b
                        where a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
                        group by b.seller_tab, b.id_shop, b.year, b.month
                    )
--where id_shop = '0063' and year = 2018 and month = 8
;

 --------------------------------
 -- расчет полной премии по плану
 --------------------------------
update FIRM_DEV.bep_seller_prem a
set
a.prem_plan_full = a.fact_sum * (
                        select  max(case when 100*b.fact_sum/b.plan_sum >= 100 then 0.5/100
                            when 100*b.fact_sum/b.plan_sum >= 90 then 0.2/100      
                            else 0
                        end)
                        from firm_dev.BEP_SHOP_PLAN_TO b
                        where a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
                        group by b.id_shop, b.year, b.month
                    )
--where id_shop = '0063' and year = 2018 and month = 8
;
 
 -----------------------------------
 --from FIRM_DEV.bep_seller_prem b where a.seller_tab = b.seller_tab, a.id_shop = b.id_shop, a.year = b.year, a.month = b.month, a.group_t = b.group_t
 
 select seller_tab, seller_fio, group_t, fact_sum, 
 fact_sum * (case when group_t = 'Обувь' then 1.3 when group_t = 'Сопутка' then 3 end) / 100 as prem_individ_group
 from FIRM_DEV.bep_seller_prem a 
where id_shop = '0063' and year = 2018 and month = 8;


select fact_sum * (case when group_t = 'Обувь' then 1.3 when group_t = 'Сопутка' then 3 end) / 100 as prem_individ_group
from FIRM_DEV.bep_seller_prem 
group by seller_tab, id_shop,  year, month, group_t,
fact_sum * (case when group_t = 'Обувь' then 1.3 when group_t = 'Сопутка' then 3 end) / 100
;

select seller_tab, id_shop, year, month,
sum(prem_individ_group) as prem_individ_full
from FIRM_DEV.bep_seller_prem 
group by seller_tab, id_shop,  year, month
;


select 
max(case when 100*fact_sum/plan_sum >= 100 then 0.5/100
    when 100*fact_sum/plan_sum >= 90 then 0.2/100      
    else 0
    end) koef
from firm_dev.BEP_SHOP_PLAN_TO 
group by id_shop, year, month;