select a.id_dk as "КАРТА",
REGEXP_REPLACE(a.phone_number, '[^[:digit:]]+', '') as "ТЕЛЕФОН",
to_char(a.activation_date, 'dd.mm.yyyy') as "ДАТА ЗАВЕДЕНИЯ",
a.id_shop as "№ ОТДЕЛЕНИЯ",
e.shop_name as "ОТДЕЛЕНИЕ",
a.scores as "СУММА БАЛЛОВ",
b.cnt_check as "КОЛ-ВО ПОКУПОК",
imob.get_last_sale_date_new(a.id_dk) as "ПОСЛ. ПОКУПКА",
case when a.sex = '1' then 'М' else 'Ж' end as "ПОЛ",
round(months_between(trunc(sysdate),a.birthday)/12, 0) as "ВОЗРАСТ"
from st_dk a 
left join
(
    select id_dk, count(1) cnt_check from pos_sale1 ps1
    where id_dk != ' '
    group by id_dk     
) b on a.id_dk = b.id_dk
left join
(
    select sfs.shp id_shop, shp.shopname shop_name, fil.shopname fil_name  from spa_fil_shop sfs 
    left join (select shopid, shopname from st_shop) shp on sfs.shp = shp.shopid
    left join (select shopid, shopname from st_shop) fil on sfs.fil = fil.shopid
) e on a.id_shop = e.id_shop
where a.bit_block = 'F'
and dk_type = 'B'
;



select a.id_dk as "КАРТА",
case when a.sex = '1' then 'М' else 'Ж' end as "ПОЛ",
to_char(a.birthday, 'dd.mm.yyyy') as "ДАТА РОЖДЕНИЯ",
months_between(trunc(sysdate),a.birthday)/12 as "ВОЗРАСТ",
imob.get_last_sale_date_new(a.id_dk) as "ПОСЛ. ПОКУПКА",
nvl(b.cnt_check, 0) as "ПОКУПКИ С 1.1.16",
nvl(c.cnt_check, 0) as "ПОКУПКИ 1.8.18-20.12.18",
nvl(d.avg_check, 0) as "СРЕДНИЙ ЧЕК",
e.shop_name as "ОТДЕЛЕНИЕ",
e.fil_name as "ФИЛИАЛ"
from st_dk a 

left join
(
    select id_dk, count(1) cnt_check from pos_sale1 ps1
    where id_dk != ' '  and 
    ps1.sale_date >= to_date('01.01.2016', 'dd.mm.yyyy')
    group by id_dk     
) b on a.id_dk = b.id_dk

left join
(
    select id_dk, count(1) cnt_check from pos_sale1 ps1
    where id_dk != ' '  and 
    (ps1.sale_date >= to_date('01.08.2018', 'dd.mm.yyyy') and ps1.sale_date <= to_date('20.12.2018', 'dd.mm.yyyy'))
    group by id_dk
) c on c.id_dk = a.id_dk

left join 
(
    select id_dk, cast(avg(sale_sum) as number(18,2)) avg_check from pos_sale1 ps1
    where id_dk != ' ' 
    group by id_dk
) d on a.id_dk = d.id_dk

left join
(
    select sfs.shp id_shop, shp.shopname shop_name, fil.shopname fil_name  from spa_fil_shop sfs 
    left join (select shopid, shopname from st_shop) shp on sfs.shp = shp.shopid
    left join (select shopid, shopname from st_shop) fil on sfs.fil = fil.shopid
) e on a.id_shop = e.id_shop
where a.bit_block = 'F' and a.dk_country = 'RU' and imob.get_last_sale_date_new(a.id_dk) >= add_months(sysdate, -18);

select * from pos_sale1 ps1
inner join pos_sale2 ps2 on ps1.id_shop = ps2.id_shop and ps1.id_chek = ps2.id_chek; 

select id_dk, count(1) cnt_check from pos_sale1 ps1
where id_dk != ' '  and 
ps1.sale_date >= to_date('01.01.2016', 'dd.mm.yyyy')
group by id_dk;

select id_dk, count(1) cnt_check from pos_sale1 ps1
where id_dk != ' '  and 
(ps1.sale_date >= to_date('01.08.2018', 'dd.mm.yyyy') and ps1.sale_date <= to_date('20.12.2018', 'dd.mm.yyyy'))
group by id_dk;

select id_dk, cast(avg(sale_sum) as number(18,2)) avg_check from pos_sale1 ps1
where id_dk != ' ' 
group by id_dk;

select * from st_shop where shopid = '43F1';
select * from spa_fil_shop;

select sfs.shp id_shop, shp.shopname shop_name, fil.shopname fil_name  from spa_fil_shop sfs 
left join (select shopid, shopname from st_shop) shp on sfs.shp = shp.shopid
left join (select shopid, shopname from st_shop) fil on sfs.fil = fil.shopid
where shp = '4306'
