set serveroutput on;
--номер карты 0000016220294 номер телефона 89231283485 Галина Викторовна
select * from st_dk where id_dk in (select id_dk from st_dk_full_block);
update st_dk set bit_block = 'T' where id_dk in (select id_dk from st_dk_full_block) and bit_block != 'T';

select * from ri_stored_proc_error order by er_time desc;

select * from ri_stored_proc_error where sp_name like '%bep%' order by er_time desc;

select * from st_dk_vid;

select * from st_dk where lower(fio) like lower('%Астафьева%') ;

select REGEXP_REPLACE(phone_number, '[^[:digit:]]+', ''), a.* from st_dk a where REGEXP_REPLACE(phone_number, '[^[:digit:]]+', '') = REGEXP_REPLACE('7960 838 5750', '[^[:digit:]]+', '');
SELECT * FROM BACKUP_FIRM_TRADE_1.POS_DK_SCORES where id_dk = '0000016641754' order by dates, id_sale,dk_sum; 
  
select st_dk.*,
       UTL_MATCH.edit_distance_similarity(lower(fam), lower('Гончаров')) AS eds
FROM   ST_DK
where utl_match.edit_distance_similarity(lower(fam), lower('Гончаров')) > 80
and utl_match.edit_distance_similarity(lower(ima), lower('Сергей')) > 80
and UTL_MATCH.edit_distance_similarity(lower(otch), lower('Николаевич')) > 80;

select * from st_dk where birthday = to_date('22.12.1968', 'dd.mm.yyyy')
and ima = 'Наталья' ;

select id_dk, phone_number,
       UTL_MATCH.edit_distance_similarity(SUBSTR(regexp_replace(phone_number, '[^0-9]', ''), 2, 10), '7960633029') AS eds
FROM   ST_DK
WHERE UTL_MATCH.edit_distance_similarity(SUBSTR(regexp_replace(phone_number, '[^0-9]', ''), 2, 10), '7960633029') > 70;


select id_dk,
       UTL_MATCH.edit_distance_similarity(id_dk, lpad('0035664758', 13, '0')) AS eds
FROM   ST_DK
WHERE UTL_MATCH.edit_distance_similarity(id_dk, lpad('0035664758', 13, '0')) > 84;

select count(1) from imob.t_day_st_dk1 where id_shop in ('3402','3406','3428');

select * from st_shop_access where shopid = '2805';
select * from st_shop where shopid = '2736';
select * from st_shop where shopid in ('3523', '2722');
select * from s_shop where shopid in ('S333', 'S222');
select * from s_shop where shopnum = 'S07122';
select * from s_shop where substr(shopnum,2) in ( '17107', '17116');
select * from ST_RETAIL_HIERARCHY where shopparent = '25F1';
select * from spa_fil_shop where shp = '4322';

select * from dk_system;

--update st_dk a set a.scores = 0 where id_dk = '0000022562609' ;

--я фотку даже сбросила на хотлайн,но все по-прежнему:старая карта 0000007507229, новая 0000031926591
--Привет. Можешь глянуть, пожалуйста, по этим картам. Они почему-то обе заблокированы и новая карта не видна в магазинах. Я центре её вижу, в магазинах - нет.

select * from st_dk where id_dk in (select id_dk from st_dk_full_block);

select  * from st_dk where fio like '%Бобрович Евгений%';
select * from st_dk where id_dk like '0000016926028%';
select * from pos_sale1 where id_dk like '0000033317007';
select * from imob.black_list where id_dk like '%0000138960318%';

select * from pos_dk_stock1 order by id;
select * from tdv_scores_split2 where id_dk = '0000028999812' order by dates, id_sale,dk_sum;
select * from tdv_scores_split2 where id_dk = '0000022678478' order by dates, id_sale,dk_sum;

select * from st_dk where id_dk = '0000035704294'; --n
select * from st_dk@S0059 where id_dk = '0000039170712'; --s
select * from st_dk where id_dk in ('0000004251897','0000034889183'); --s
--select * from st_dk where id_dk = '0000002146546'; --s
select * from pos_dk_scores a where a.id_dk in ('0000016236646','0000031882781','0000004656050','0000036427550')order by dates, id_sale,dk_sum;
select * from pos_dk_scores_firm a where a.id_dk = '0000006049959' order by dates, id_sale,dk_sum;

select * from pos_dk_scores_firm where id_shop = '2633';

select * from st_dk_hand1;
select * from st_dk_hand2 where id_dk = '0000025197051';
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores a where a.id_dk = '0000038972218' order by dates, id_sale,dk_sum;
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores a where a.id_dk = '0000035704294' order by dates, id_sale,dk_sum;
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores a where a.id_dk = '0000038208799' order by dates, id_sale,dk_sum;
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from shop999.pos_dk_scores a where a.id_dk = '0000028936562' order by dates, id_sale,dk_sum;
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores_firm a where a.id_dk = '0000038972218' order by dates, id_sale,dk_sum;

select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores@s0020 a where a.id_dk = '0510000577032' order by dates, id_sale,dk_sum;
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores@S3452 a where a.id_dk = '0000007087660' order by dates, id_sale,dk_sum;

select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from tdv_scores_split2 a where a.id_dk = '0000039170712' order by dates, id_sale,dk_sum;

select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores a where  id_dk = '0000010479360'
and doc_type not in ('ac01','ac03') and NOT(a.dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
             and not(a.dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
             and  NOT (doc_type in ('voz', 'ckv') and (a.id_shop,a.id_chek,a.doc_type) in (select id_shop, id_chek, doc_type from firm.ST_DK_AC01_VOZV))
             and a.bit_open_check = 'F'
order by dates, id_sale,dk_sum;


--------------------------------------------------------------------------------
select firm_dev.get_scores('0000016236646') from dual;
call DK_SUM_COUNT3('0000020411008');
--------------------------------------------------------------------------------
call bep_dk_clear_scores_voz('2811', '0000000029674');
--------------------------------------------------------------------------------
call tdv_refresh_one_dk('0041', '0000000029674');
--------------------------------------------------------------------------------
select * from POS_DK_COPY_LOGS where id_dk_from = '0000033377230' or id_dk_to = '0000033377230' order by ddate;
select * from POS_DK_COPY_LOGS where id_dk_from in ('0000005436101','0000033304755') or id_dk_to in ('0000005436101','0000033304755') order by ddate;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


delete from pos_dk_scores_firm where id_dk = '0000036545780';

--update st_dk set phone_number = '' where id_dk = '0000011877332';
--
--update st_dk@s3334 set scores = 17046 where id_dk = '0000034138021';

select * from pos_dk_scores@s2506 a where a.id_dk = '0000000782302' order by dates, id_sale,dk_sum;

select * from pos_dk_scores a where a.id_dk = '0000007459177' order by dates, id_sale,dk_sum;
select * from pos_dk_scores_firm a where a.id_dk = '0000004749011' order by dates, id_sale,dk_sum;
select * from pos_dk_scores_firm a where a.id_dk = '0000022678478' order by dates, id_sale,dk_sum;
--pos_dk_sale_his d_prixod2

select * from pos_dk_sale_his a where a.id_dk = '0000014469701';
select * from d_prixod2 a where a.id_dk = '0000014469701';

select * from pos_sale1 where id_dk = '0000007456480' order by sale_date;
select * from pos_sale1 where id_dk = '0000031882781' order by sale_date;
select * from pos_sale1 where id_dk = '0000031882781' order by sale_date;
select * from pos_sale2 where id_chek = '38119' and id_shop = '3917';
select * from pos_sale2 where id_sale = '236887';
select * from pos_sale7 where id_chek = '38119' and id_shop = '3917';
select * from pos_sale1 where id_chek = '53718' and id_shop = '3914';
select * from pos_sale1 where id_shop = '0020';

select * from pos_sale3 where id_chek = '73162' and id_shop = '2633';

select * from pos_sale1 where id_dk like '925%';

update pos_sale1 set id_dk = '0000034889183' where id_dk = '0000004251897';

select * from s_art where art = '1735341';

select * from pos_sale2 where art = '1735341';
select * from pos_sale1 where discount_sum < 0 order by sale_date desc;

select * from pos_sale1 a
inner join pos_sale2 b on a.id_chek = b.id_chek and a.shop_id = b.id_shop
where a.id_dk = '0000012701667'
order by a.sale_date
;

select a.sale_date, c.shopnum, a.id_chek, b.id_sale, b.art, b.assort from pos_sale1 a
inner join pos_sale2 b on a.id_chek = b.id_chek and a.shop_id = b.id_shop
inner join s_shop c on a.shop_id = c.shopid
where a.id_dk = '0000012701667'
order by a.sale_date
;

select * from pos_sale1 where id_dk in ('0000005436101','0000005436101') order by sale_date desc;

select * from pos_sale1@S3440 where id_chek = '30614' ;

update st_dk@s2112 set activation_date = (select activation_date from st_dk where id_dk = '0000020670641') where id_dk = '0000020670641';


select * from st_dk where id_dk in ( '0000026268201', '0000026258201');

select count(1) from st_dk@s3334;
select * from st_dk@s3813 where fio like '%Любовь Викторовна Кузнецова%';
select * from st_dk@s0040 where id_dk = '0000007459177';
select * from st_dk@s0018 where id_dk = '0000002945651';
select * from pos_dk_scores@s2308 a where a.id_dk = '0000036545780' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s0033 a where a.id_dk in ('0000031926591','0000007507229', '0000030889965') order by dates, id_sale,dk_sum;

select sum(sale_sum) from pos_sale1 where id_dk in ('0000000077163', '0000026268201', '0000026258201') ;
select * from d_prixod2 where id_dk in ('0000000077163', '0000026268201', '0000026258201') ;
select * from pos_dk_sale_his where id_dk in ('0000000077163', '0000026268201', '0000026258201') ;
select sum(sale_sum) from pos_sale1@S2503 where id_dk in ('0000000077163', '0000026268201', '0000026258201') ;

--select * from pos_dk_scores where id_dk = '0000012672158' and doc_type = 'voz' and id_chek = '875' and id_shop = '4306'l

select * from pos_sale1@s2501 where id_dk in ('0000027354933', '0000027354933', '0000027354933') ;
select * from pos_sale2@s3619  where id_chek = 40522;

update pos_sale1 set id_dk = '0000034922613' where id_chek = '32143' and id_shop = '2424';

select * from pos_sale1@s0029 where id_dk = '0000003960394' order by sale_date;

select * from ri_stored_proc_error@s4310 order by er_time desc;
select * from ri_stored_proc_error  where sp_name != 'LKA_SET_IP' order by er_time desc;

select * from POS_DK_COPY_LOGS where id_dk_from = '0000003252581' or id_dk_to = '0000003252581';

--begin shop999.tdv_dk_scores_check@s2538('0000000078016', '47473' ); end;
--begin shop999.tdv_dk_scores_check@s3619 ('0000019624624', '40522' ); end;
--begin tdv_copy_table_from_shop('2106', 'POS_DK_SCORES');end;
call  shop999.tdv_dk_scores_check@s3615('0000016220294', '35601' );
call shop999.tdv_dk_scores_add_card@s4505('0000029294781');
call tdv_dk.scores_clear2('0000010586068');

call shop999.tdv_dk_scores_add_card@s3335('0000037979485');
call tdv_dk_copy_create('0000003900468', '0000016079380', '2904');
call tdv_refresh_one_dk('0075', '0000034475324');

call SPA_REFRESH_MANY_DK('2101', '''0000000000125'', ''0000000000126''');

call DK_SUM_COUNT3('0000022274298');

call TDV_EX_FIRM_TO_SHOP.copy_DK_ON_SHOP('0060');

call TDV_get_inf_from_ex_DK('0047');

select FIRM_DEV.get_scores('0000032819090') from dual;

SELECT * FROM BACKUP_FIRM_TRADE_1.POS_DK_SCORES;

update pos_dk_scores@s0074 set dk_sum = dk_sum + 77.88 where id_dk = '0000030640702' and id_chek = '187' and dk_sum = -80.29 and doc_type = 'voz';

--------------------------------------------------------------------------------
-- УМЕНЬШЕНИЕ СПИСАННЫХ БАЛЛОВ В МАГАЗИНЕ ЗА ВОЗВРАТ, ПО КОТОРОМУ БЫЛО СГОРАНИЕ
--------------------------------------------------------------------------------
select * from pos_dk_scores a where a.id_dk = '0000024435178' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s4510 a where a.id_dk = '0000024435178' order by dates, id_sale,dk_sum;

update pos_dk_scores@s4510 
set dk_sum = dk_sum + 3418 
where id_dk = '0000024435178' and doc_type = 'voz' --ckv
and id_chek = '1029' and dk_sum = -3598;

call tdv_refresh_one_dk('4510', '0000024435178');
------------------------------------------------------------------------------


------------------------------------------------------------------------------
--            УДАЛЕНИЕ ЗАДВОЕННЫХ ВОЗВРАТОВ
------------------------------------------------------------------------------
select * from st_dk where id_dk = '0000024435178';
select * from st_dk@s0003 where id_dk = '0000017151436';
select * from pos_dk_scores a where a.id_dk = '0000030640702' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s0074 a where a.id_dk = '0000030640702' order by dates, id_sale,dk_sum;

select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores a where a.id_dk = '0000016220294' order by dates, id_sale,dk_sum;

select min(rowid), id_dk, id_shop, id_chek, id_sale, dk_sum, count(1) cnt
from pos_dk_scores
where id_dk = '0000017151436' 
and doc_type in ('ckv','voz')
group by id_dk, id_chek, id_sale, dk_sum, id_shop having count(1) > 1;

call bep_dk_clear_scores_voz('3615', '0000016220294');
call tdv_refresh_one_dk('0003', '0000017151436');

delete 
--select * 
from pos_dk_scores@s0003
where 
rowid not in (
    select min(rowid)--, id_dk, id_chek, id_sale, dk_sum 
    from pos_dk_scores@s0003
    where id_dk = '0000017151436' 
    and doc_type in ('ckv','voz')
    group by id_dk, id_chek, id_sale, dk_sum having count(1) > 1
) 
and 
(id_dk, id_chek, nvl(id_sale,' '), dk_sum) in (
    select id_dk, id_chek, nvl(id_sale,' '), dk_sum from pos_dk_scores@s0003
    where id_dk = '0000017151436' 
    and doc_type in ('ckv','voz')
    group by id_dk, id_chek, id_sale, dk_sum having count(1) > 1
)
and id_dk = '0000017151436' 
and doc_type in ('ckv','voz')
;

call tdv_refresh_one_dk('0003', '0000017151436');



delete 
--select *
from pos_dk_scores--@s0034
where rowid in (
    select min(rowid) from pos_dk_scores--@s0034
    where id_dk = '0000006656065' 
    group by id_dk, id_chek, id_sale, dk_sum having count(1) > 1
) 
and id_dk = '0000006656065';

delete from pos_dk_scores_firm where id_dk = '0000022587282' and id_chek = 'STND0365';
-----------------------------------------------------
update st_dk@s2325 
set summ = 14196
where id_dk = '0000015549693';
commit;

insert into pos_dk_scores
select '0000026619316', doc_type, id_chek, id_sale, dk_sum, dates, id_shop, a.bit_open_check, id_ac
from pos_dk_scores a where a.id_dk = '0000002843940';

insert into pos_dk_scores@s2202
select '0000026619316', doc_type, id_chek, id_sale, dk_sum, dates, id_ac
from pos_dk_scores@s2202 a where a.id_dk = '0000002843940';
-------------------------------------------------------
select * from st_dk@s2218 where id_dk = '0000022957351';
select * from st_dk@S2218 where id_dk = '0000002402260';

update st_dk@s2218 
set discount = 15,
    scores = 34404,
    summ = 94892.1
where id_dk = '0000022957351';

---
--0000024692434
select * from pos_dk_copy_logs a where a.id_dk_from in( '0000007507229','0000031926591') or a.id_dk_to in( '0000007507229','0000031926591');



update pos_dk_scores
set id_dk = '0000026619316'
where id_dk = '0000002843940'; 

select FIRM_DEV.get_scores('0000006461560') from dual;

update pos_sale1
set id_dk = '0000034520130'
where id_dk = '0000006514303'
;

select * from pos_sale1 where id_dk = '0000006514303';

select  *
from  firm.pos_dk_scores a 
where doc_type not in ('ac01','ac03') and NOT(a.dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
         and not(a.dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
         and  NOT (doc_type in ('voz', 'ckv') and (a.id_shop,a.id_chek,a.doc_type) in (select id_shop, id_chek, doc_type from firm.ST_DK_AC01_VOZV))
         and a.id_dk = '0000016093812'
         and a.bit_open_check = 'F'
         order by dates, id_sale,dk_sum;
         
         select  *
from  firm.pos_dk_scores a 
where doc_type not in ('ac01','ac03') and NOT(a.dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
         and not(a.dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
         and  NOT (doc_type in ('voz', 'ckv') and (a.id_shop,a.id_chek,a.doc_type) in (select id_shop, id_chek, doc_type from firm.ST_DK_AC01_VOZV))
         and a.id_dk = '0000010701249'
         and a.bit_open_check = 'F'
         order by dates, id_sale,dk_sum
         ;

select nvl(max(sum(nvl(dk_sum,0))),0)
    from st_dk_hand2 b2
    inner join st_dk_hand1 b1 on b2.id = b1.id   
    where b1.dk_type = 'B'  and b2.id_dk = '0000016427013'
    group by   b2.id_dk;
         
select * from pos_dk_scores where (id_chek, id_shop) in 
    (select to_char(a.id_chek), to_char(id_shop)
      from pos_sale3 a
      where a.vid_op_id IN (4)
    ) and doc_type not in ('ac', 'ckv', 'voz')   and dk_sum > 0
    order by dates desc;
    
select *
from pos_sale3 a
where (to_char(a.id_chek), to_char(id_shop)) in (select id_chek, id_shop from pos_dk_scores) and a.vid_op_id IN (4);

select *
from pos_sale3 a
where id_chek = 26538 and id_shop = 2903;
 
 
 insert into st_dk
     select '0000026258201', fio, birthday, sex, country, city, street, phone_number, email, 0, a.activation_date, 
              0, '2503', dkvid_id, sysdate, 'F','F',
              DISC_SCORES_START, SCORES, DK_TYPE, DK_LEVEL, SCORES_DATE, Systimestamp - 365, a.dk_country,'0',' ',' ',' ',' ',' '
     from st_dk   a
     where id_dk = '0000000077163' ; 


delete
--select * 
from pos_dk_scores@S4321 a where a.id_dk = '0000022863874' 
and doc_type = 'voz' and id_chek = 140 and id_sale is null and dk_sum = -417
and rownum = 1
--order by dates, id_sale,dk_sum
;

select * from st_dk@s2501 a where id_dk = '0000021518959';

update st_dk@s2501
set birthday = birthday + 1
where id_dk = '0000021518959';

update st_dk@s2501
set activation_date = activation_date - 1
where id_dk = '0000021518959';

select os_user from history_session group by os_user order by os_user;


select 
   segment_name           table_name,	   
   sum(bytes)/(1024*1024) table_size_meg 
   
from   
   user_extents a 
where  
   segment_type='TABLE' 
group  by segment_name  
order by sum(bytes)/(1024*1024) desc;


select count(*) from st_dk_status;

select count(*) from (
select a.id_dk
from pos_sale1 a
inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop 
inner join st_dk c on a.id_dk = c.id_dk
inner join st_shop d on a.id_shop = d.shopid 
where C.dkvid_id not in ('002') 
and a.bit_close = 'T'
and b.scan != ' '
and substr(a.id_shop,1,1) != 'S'
and to_number(to_char(a.sale_date,'yyyyMMdd')) >= to_number(to_char(to_date('01.01.2016','dd.MM.yyyy'),'yyyyMMdd'))
and to_number(to_char(a.sale_date,'yyyyMMdd')) <= to_number(to_char(to_date('01.03.2018','dd.MM.yyyy'),'yyyyMMdd'))
group by a.id_dk
);

select * from pos_dk_Scores 
where id_shop = 'FIRM' 
and id_chek = 'DK100003' 
and id_dk in 
(
select id_dk from st_dk where id_shop = '0066' and (trunc(activation_date) = to_date('08.03.2018', 'dd.mm.yyyy') or trunc(activation_date) = to_date('09.03.2018', 'dd.mm.yyyy'))
);

delete from pos_dk_Scores 
where id_shop = 'FIRM' 
and id_chek = 'DK100003' 
and id_dk in 
(
select id_dk from st_dk where id_shop = '0066' and (trunc(activation_date) = to_date('08.03.2018', 'dd.mm.yyyy') or trunc(activation_date) = to_date('09.03.2018', 'dd.mm.yyyy')) and dk_sum <= 0
);

select * from pos_sale1 a
left join pos_sale2 b on a.id_chek = b.id_chek and a.shop_id = b.id_shop
where a.id_dk = '0000012672158'
order by a.sale_date;

call dk_scores_info('0000014483899');
select * from tem_dk_scores_info where id_dk = '0000014483899';

select a.id_dk, a.id_chek, b.id_sale, b.art, a.sale_sum, b.cena3, b.cena3/a.sale_sum   from pos_sale1 a
inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek

where 
to_char(a.sale_date, 'dd.mm.yy') = '16.08.18'
and
a.id_dk in (
'0000024904704',
'0000002832555',
'0000013930035',
'0000028027171',
'0000027442913',
'0000013952945',
'0000022816771',
'0000004838500',
'0000027472873',
'0000029694105',
'0000021763540'
)


;

select REGEXP_REPLACE(phone_number, '[^[:digit:]]+', '') phone_number from st_dk a 
where dk_country = 'BY'
and length(REGEXP_REPLACE(phone_number, '[^[:digit:]]+', '')) > 7
;

select * from st_dk@s2714 where id_dk = '0000030572010';

  select shopid from st_shop where 
  substr(shopid,1,2) in ('27')
  and bit_open = 'T' and org_kod = 'SHP';

update st_dk
set fio = ' ', phone_number = ' ', fam = ' ', ima = ' ', otch = ' ', bit_black_list = 'T', bit_block = 'T'
where id_dk = '0000030572010';


select distinct partno from pos_brak@s0031;