select orgno, shopnum from s_seller a
left join s_shop b on a.orgno = substr(b.shopnum,2,5)
where tabno = '01630085' ;
select * from s_shop where substr(shopnum, 2,5) = '16116';


select tabno, fio, orgno from s_seller a
left join s_shop b on a.orgno = b.shopnum
where 
orgno = COALESCE((select substr(shopnum,2,5) from s_shop where shopid = '3616'), orgno)--lpad(COALESCE('3616', orgno), 5, '0') 
and
(lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
and ((EXTRACT(MONTH FROM dateb) <= COALESCE(4, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
and ((EXTRACT(MONTH FROM dated) >= COALESCE(4, EXTRACT(MONTH FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
order by stext2 desc, dated desc
;

select * from s_seller where tabno = '01630085';


select 
*
--b.tabno, b.fio,  b.stext2, b.stext3, b.dateb, b.dated, d.massn, d.massn_name,d.stat2, d.stat5, d.begda, d.endda, 
--a.seller_tab, a.seller_fio, a.id_shop, a.year, a.month, a.group_t, a.fact_sum, a.prem_individ_group, a.prem_individ_full, a.prem_plan_group, a.prem_plan_full, a.id_combination, a.id_group, a.fact_pair_kol, a.dateb as group_dateb, a.datee as group_datee,
--c.tabno zavmag_tab, c.fio zavmag 
from bep_prem_seller a
left join (
    select a1.tabno,a1.fio,a1.stext2,a1.stext3,a1.dateb,a1.dated,a1.orgno from s_seller a1
    inner join (
        select tabno, min(dated) dated from s_seller
        where  ((EXTRACT(MONTH FROM dateb) <= COALESCE(4, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
        and    ((EXTRACT(MONTH FROM dated) >= COALESCE(4, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dated) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dated = b1.dated
) b on a.seller_tab = b.tabno 
----
and (select substr(shopnum,2,5) from s_shop where shopid = a.id_shop) =  b.orgno
----
left join (
    select * from (
        select tabno, fio, orgno from s_seller a
        left join s_shop b on a.orgno = b.shopnum
        where 
        orgno = COALESCE((select substr(shopnum,2,5) from s_shop where shopid = '3616'), orgno)--lpad(COALESCE('3616', orgno), 5, '0') 
        and (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
        and ((EXTRACT(MONTH FROM dateb) <= COALESCE(4, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM dated) >= COALESCE(4, EXTRACT(MONTH FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
        order by stext2 desc, dated desc
    ) where rownum = 1
) c on lpad(a.id_shop, 5, '0') = c.orgno
left join (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    inner join (
        select pernr, min(endda) endda from s_seller_sap a 
        where
            ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) <= COALESCE(4, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) < COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) >= COALESCE(4, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(2019, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) > COALESCE(2019, EXTRACT(YEAR FROM sysdate)))
        group by pernr
    ) c on a.pernr = c.pernr and a.endda = c.endda
) d on a.seller_tab = d.pernr
where a.year = COALESCE(2019, EXTRACT(YEAR FROM sysdate)) and a.month = COALESCE(4, EXTRACT(MONTH FROM sysdate)) 
and a.id_shop = COALESCE('3616', a.id_shop) 
and a.seller_tab != c.tabno