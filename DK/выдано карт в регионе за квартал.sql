select * from s_limmag;

select distinct shopid id_shop, brand from s_limmag where date_f <= sysdate and sysdate <= date_l;

select (select substr(shopnum,2,5)  from s_shop b where b.shopid = a.id_shop) id_Shop, extract(month from activation_date) month, count(*) count from st_dk a 
where trunc(activation_date) >= trunc(to_date('01.07.2019','dd.mm.yyyy'))
  and trunc(activation_date) < trunc(to_date('01.10.2019','dd.mm.yyyy'))
  and substr(id_shop, 1, 2) in ('36', '38', '39', '40')
group by id_shop, extract(month from activation_date) 
order by extract(month from activation_date), id_shop
;