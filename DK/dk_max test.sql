select numconf from config;

select * from s_art where art = '1737800';

select * from D_KASSA_DISC_CUR where kass_id = '1#lacit108-m';
select * from D_KASSA_CUR where kass_id = '1#lacit108-m';

select * from d_prixod1 ;
select * from d_prixod2 where id = 330 and art = '1737800';

select a.scan from pos_brak a
inner join e_osttek b on a.scan = b.scan
where a.bit_close = 'T' and a.bit_allow = 'T'
group by a.scan having sum(b.kol) > 0;

select a.scan from e_osttek a
where procent != 0 and scan not in (select scan from pos_brak)
group by a.scan having sum(a.kol) > 0;

select * from  pos_skidki2 b
inner join pos_skidki1 a on b.docid = a.docid
where 
--a.docid = '0200001115'
b.art = '1737800'
order by datee desc, priority desc
;

select * from pos_sale2;

select scan, art, asize from e_osttek where procent > 0 group by scan, art, asize having sum(kol) > 0;
select * from e_osttek where scan = '00000000001005138360';

select * from s_label where scan = '00000000001107129898';

select * from st_dk where id_dk = '0000000000125';

select * from pos_brak;

select DK_MAX_SCORES_CHECK('1#lacit108-m', '0000000000706', 'F') from dual;
select BEP_DK_MAX_SCORES_CHECK('1#lacit108-m', '0000000000125', 'F') from dual;
select ACTION_PRICELIST('1637920', sysdate, 20) from dual;
select is_belwest_prod('1737800') from dual;
select get_dk_sc_sys_int('USE_SCR', 'OB_BELWEST') from dual;
select get_dk_orange_discount('0000000000095') from dual;
select * from st_dk where id_dk = '0000000000706';

select 
cena2p(sysdate,'1737800',0,1) a1,
cena2s(sysdate,'1737800',0,1,20,'00',20) a, 
nvl((select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001'),0) b ,
cena2s(sysdate,'1737800',0,1,20,'00',20) -
nvl((select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001'),0) res,
(cena2s(sysdate,'1737800',0,1,20,'00',20) -
nvl((select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001'),0))/100*20 res_pr
from dual;

select 
cena2p(sysdate,'1737800',0,1) a1,
cena2s(sysdate,'1737800',0,1,0,'00',0) a, 
nvl((select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001'),0) b ,
cena2s(sysdate,'1737800',0,1,0,'00', 0) -
nvl((select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001'),0) res,
(cena2s(sysdate,'1737800',0,1,0,'00',0) -
nvl((select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001'),0))/100*20 res_pr
from dual;


select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001';

select 
cena2p(sysdate,'1737800',0,1) c2p,
cena2(sysdate,'1737800',0,1) c2,
cena2s(sysdate,'1737800',0,1,/*b.procent*/10,'00',/*noncond_proc*/0) c2s,
b.PRICE_SUM,
b.PRICE,
(b.PRICE_SUM + nvl(/*DISCOUNT_SUM*/0, 0) - b.PRICE * 0.7) res_def,
(b.PRICE_SUM + nvl(/*DISCOUNT_SUM*/0, 0) - cena2p(sysdate,'1737800',0,1) * 0.5) res_cena2p,

cena2p(sysdate,'1737800',0,1)*0.5 - (cena2p(sysdate,'1737800',0,1) - cena2s(sysdate,'1737800',0,1,/*b.procent*/0,'00',/*noncond_proc*/0)) res_noncon,
cena2s(sysdate,'1737800',0,1,/*b.procent*/0,'00',/*noncond_proc*/0) - cena2p(sysdate,'1737800',0,1)*0.5 res_noncon_t,
cena2(sysdate,'1737800',0,1)*0.8 - (cena2(sysdate,'1737800',0,1) - cena2s(sysdate,'1737800',0,1,/*b.procent*/40,'00',/*noncond_proc*/0)) res_brak,
cena2s(sysdate,'1737800',0,1,/*b.procent*/40,'00',/*noncond_proc*/0)-cena2(sysdate,'1737800',0,1)*0.8 res_brak_t
from D_KASSA_CUR b;


select 
cena2p(sysdate,b.art,0,1) c2p,
cena2(sysdate,b.art,0,1) c2,
cena2s(sysdate,b.art,0,1,b.procent,'00',b.noncond_proc) c2s,
cena2s(sysdate,b.art,0,1,0,'00',0) c2s0,
b.PRICE_SUM,
b.PRICE,
(b.PRICE_SUM + nvl(a.DISCOUNT_SUM, 0) - b.PRICE * 0.5) res_def,
((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end -nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001')
                                                  ,0))*0.03) res_res,
cena2p(sysdate,b.art,0,1)*0.5 - (cena2p(sysdate,b.art,0,1) - cena2s(sysdate,b.art,0,1,/*b.procent*/0,'00',/*noncond_proc*/0)) res_noncon,
cena2s(sysdate,b.art,0,1,/*b.procent*/0,'00',/*noncond_proc*/0) - cena2p(sysdate,b.art,0,1)*0.5 res_noncon_t
from D_KASSA_CUR b
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id and a.discount_type_id != 'A000000001'
left join pos_brak br on b.scan = br.scan 
where b.kass_id = '1#lacit108-m';


select sum(discount_sum) from d_kassa_disc_cur t1 where t1.posnr = 1  and t1.kass_id = '1#lacit108-m' and t1.discount_type_id!='A000000001';
              