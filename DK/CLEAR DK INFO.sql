select * from ST_DK;
select * from DK_CONFIRMATION_CODE;

select * from russian_surnames where surname like '%ё%';
select * from russian_names where name like '%ё%';

-- НЕ ПОДХОДИТ ДАТА РОЖДЕНИЯ
select 
--count(*)
  trunc(months_between(sysdate, birthday)/12) year, dk.* 
from st_dk dk
where (
  (trunc(months_between(sysdate, birthday)/12) < 16 or trunc(months_between(sysdate, birthday)/12) > 100)
);

update st_dk set birthday = null
where (trunc(months_between(sysdate, birthday)/12) < 16 
      or trunc(months_between(sysdate, birthday)/12) > 100);

select * from st_dk;


-- НЕ ПОДХОДИТ ФИО
select --* 
count(*) cnt_all, sum(case when name is null then 1 else 0 end) kol_bad
from (
  select a.id_dk, a.ima, b.name from st_dk a
  left join  russian_names b on lower(a.ima) = lower(b.name)
)
--group by case when name is null then  
;

select c.surname, b.name, b.sex, a.fio, a.fam, a.ima, a.otch, a.sex from st_dk a
  left join  russian_names b on lower(a.ima) = lower(b.name)
  left join russian_surnames c on lower(a.fam) = lower(c.surname)
--  where ima is null or ima = ' '
  ;
 
select sex, count(*) from st_dk group by sex; 

select rtrim('Иванов')||rtrim(' '||null)||rtrim(' '||null) from dual;
  
    merge into st_dk dk
    using (
      select a.id_dk, c.surname, b.name, decode(nvl(b.sex, ' '), 'М', '1', 'Ж', '2', ' ') sex 
      from st_dk a
      left join  russian_names b on lower(a.ima) = lower(b.name)
      left join russian_surnames c on lower(a.fam) = lower(c.surname)
    ) src on (dk.id_dk = src.id_dk)
    when matched then update
      set dk.fam = src.surname, dk.ima = src.name, dk.sex = src.sex,
          fio = rtrim(src.surname)||rtrim(' '||src.name)||rtrim(' '||dk.otch)
    ;

select --* 
count(*) cnt_all, sum(case when surname is null then 1 else 0 end) kol_bad
from (
  select a.id_dk, a.fam, b.surname from st_dk a
  left join  russian_surnames b on lower(a.fam) = lower(b.surname)
)
--group by case when name is null then  
;
-- НЕ ПОДХОДИТ ЕМАЙЛ
--regexp_like (upper(v_email), '[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}')
select distinct email from st_dk where email is not null and not regexp_like (upper(email), '[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}');
select distinct email from st_dk where email is null;

-- НЕ ПОДХОДИТ НОМЕР ТЕЛЕФОНА
select 
--count(*)
  regexp_replace(phone_number, '[^[:digit:]]') PHONE_NUM,
  case when substr(regexp_replace(phone_number, '[^[:digit:]]'),1,2) = '80' 
            then '375'||substr(regexp_replace(phone_number, '[^[:digit:]]'),3) 
            else regexp_replace(phone_number, '[^[:digit:]]') 
          end ph_upd,
  
  dk.* 
from st_dk dk
where 
dk.bit_block = 'F'
and dk.dk_country = 'BY'

and 
(
  regexp_replace(phone_number, '[^[:digit:]]') is null
  or
  (length(case when substr(regexp_replace(phone_number, '[^[:digit:]]'),1,2) = '80' 
            then '375'||substr(regexp_replace(phone_number, '[^[:digit:]]'),3) 
            else regexp_replace(phone_number, '[^[:digit:]]') 
          end) != 12 
          and dk_country = 'BY')
  or
  (substr(regexp_replace(phone_number, '[^[:digit:]]'),1,3) not in ('375') and substr(regexp_replace(phone_number, '[^[:digit:]]'),1,2) not in ('80') and dk_country = 'BY')
  or
  (length(regexp_replace(phone_number, '[^[:digit:]]')) != 11 and dk_country = 'RU')
  or
  (substr(regexp_replace(phone_number, '[^[:digit:]]'),1,1) not in ('8', '7') and dk_country = 'RU')
  or
  (select MAX(length(regexp_replace(phone_number, '[^[:digit:]]')) - length(replace(regexp_replace(phone_number, '[^[:digit:]]'), level-1,''))) FROM dual connect by 1=1 and level <= 10) > 7
) and  regexp_replace(phone_number, '[^[:digit:]]') like '375%' and dk_country = 'BY'
;

update st_dk set phone_number = ' '
where
(
  regexp_replace(phone_number, '[^[:digit:]]') is null
  or
  (length(case when substr(regexp_replace(phone_number, '[^[:digit:]]'),1,2) = '80' 
            then '375'||substr(regexp_replace(phone_number, '[^[:digit:]]'),3) 
            else regexp_replace(phone_number, '[^[:digit:]]') 
          end) != 12 
          and dk_country = 'BY')
  or
  (substr(regexp_replace(phone_number, '[^[:digit:]]'),1,3) not in ('375') and substr(regexp_replace(phone_number, '[^[:digit:]]'),1,2) not in ('80') and dk_country = 'BY')
  or
  (length(regexp_replace(phone_number, '[^[:digit:]]')) != 11 and dk_country = 'RU')
  or
  (substr(regexp_replace(phone_number, '[^[:digit:]]'),1,1) not in ('8', '7') and dk_country = 'RU')
  or
  (select MAX(length(regexp_replace(phone_number, '[^[:digit:]]')) - length(replace(regexp_replace(phone_number, '[^[:digit:]]'), level-1,''))) FROM dual connect by 1=1 and level <= 10) > 7
);