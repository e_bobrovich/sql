select * from config@s2909;
select * from pos_dk_scores where id_chek = 'DK100049' and substr(id_shop,1,2) not in ('36','38','39','40') order by id_shop;

select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  from pos_dk_scores a where a.id_dk = '0000026961231' order by dates, id_sale,dk_sum;

delete from pos_dk_scores@s2121 where id_dk = '0000009133341' and id_chek = 'DK100049';

select * from pos_dk_stock1 where id = 'SC100012';

select numconf, shopid
    from config@s2909;
    


    
    
select a.*, 
sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over
from pos_dk_scores a
where id_chek = 'DK100049' and id_shop = '2202' and substr(id_shop,1,2) not in ('36','38','39','40') order by id_shop;    
    
select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
from pos_dk_scores a where a.id_dk = '0000007886485' order by dates, id_sale,dk_sum;



-- ПРОХОД ПО МАГАЗИНАМ С ТАКИМИ НАЧИСЛЕНИЯМИ
select id_shop, count(*) from pos_dk_scores where id_chek = 'DK100049' and substr(id_shop,1,2) not in ('36','38','39','40') group by id_Shop order by id_shop;

-- ПРОХОД ПО КАРТАМ МАГАЗИНА С ТАКИМИ НАЧИСЛЕНИЯМИ
select id_dk, dates from pos_dk_scores where id_chek = 'DK100049' and id_shop = '2202' and substr(id_shop,1,2) not in ('36','38','39','40') order by id_dk;

-- ПОЛУЧЕНИЕ СУММЫ БАЛЛОВ НА МОМЕНТ НАЧИСЛЕНИЯ ПО ВСЕЙ СИСТЕМЕ
select * from ( 
  select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
  from pos_dk_scores a where a.id_dk = '0000016334243' order by dates, id_sale,dk_sum
) where id_chek = 'DK100049' and id_shop = '2202'  
;

-- ПОЛУЧЕНИЕ СУММЫ БАЛЛОВ ПО ВСЕЙ СИСТЕМЕ БЕЗ ЭТИХ
select sum(dk_sum) from (
  select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
  from pos_dk_scores a where a.id_dk = '0000002355726' and id_chek != 'DK100049' order by dates, id_sale,dk_sum
);


select * from (
  select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
  from pos_dk_scores a where a.id_dk = '0000016334243' and dates > to_date('17.09.20 14:30:42', 'dd.mm.yy hh24:mi:ss') and dk_sum < 0
  order by dates, id_sale,dk_sum 
) where rownum = 1;

select nvl(sum(dk_sum),0) from (
  select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
  from pos_dk_scores a where a.id_dk = '0000002355726' and dates > to_date('17.09.20 14:30:42', 'dd.mm.yy hh24:mi:ss') and dk_sum < 0
  order by dates, id_sale,dk_sum 
) ;

select dk_sum_over - 1000
from ( 
  select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
  from pos_dk_scores a where a.id_dk = '0000026961231' order by dates, id_sale,dk_sum
) where id_chek = 'DK100049';


SET serveroutput ON
declare 
v_id_Shop varchar2(4);
v_link varchar2(20);

v_date_ac date;
v_fritst_minus_after number := 0;
v_summ_without number := 0;
v_minus_after number := 0;
v_summ_before number := 0;
begin
  
  -- ПРОХОД ПО МАГАЗИНАМ С ТАКИМИ НАЧИСЛЕНИЯМИ
  for r_shop in (select id_shop, count(*) from pos_dk_scores where id_chek = 'DK100049' and substr(id_shop,1,2) not in ('36','38','39','40') group by id_Shop order by id_shop)
  loop
  
    v_id_Shop := r_shop.id_shop;
    dbms_output.put_line('ID_SHOP : '||v_id_Shop);
      
    SELECT NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=v_id_Shop;
  
    for r_dk in (select id_dk, dates from pos_dk_scores where id_chek = 'DK100049' and id_shop = v_id_Shop and substr(id_shop,1,2) not in ('36','38','39','40') order by id_dk)
    loop
  --    dbms_output.put_line(r_dk.id_dk||' : '||r_dk.dates);
  
  -- ПОЛУЧЕНИЕ СУММЫ БАЛЛОВ ПО ВСЕЙ СИСТЕМЕ БЕЗ ЭТИХ
      select sum(dk_sum)
      into v_summ_without 
      from (
        select dk_sum,  sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
        from pos_dk_scores a where a.id_dk = r_dk.id_dk and id_chek != 'DK100049' order by dates, id_sale,dk_sum
      );
      
      begin
        select dk_sum_over - 1000
        into v_summ_before
        from ( 
          select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
          from pos_dk_scores a where a.id_dk = r_dk.id_dk order by dates, id_sale,dk_sum
        ) where id_chek = 'DK100049';
      exception when no_data_found then
          v_summ_before := 0;
      end; 
      
      
      begin
        select dk_sum 
        into v_fritst_minus_after
        from (
          select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
          from pos_dk_scores a where a.id_dk = r_dk.id_dk and dates > r_dk.dates and dk_sum < 0
          order by dates, id_sale,dk_sum 
        ) where rownum = 1;
      exception when no_data_found then
          v_fritst_minus_after := 0;
      end;  
      
      begin
        select nvl(sum(dk_sum),0)
        into v_minus_after
        from (
          select a.*, sum(dk_sum) over (partition by id_dk order by dates, id_sale,dk_sum) dk_sum_over  
          from pos_dk_scores a where a.id_dk = r_dk.id_dk and dates > r_dk.dates and dk_sum < 0
          order by dates, id_sale,dk_sum 
        );
      exception when no_data_found then
          v_minus_after := 0;
      end; 
      
      dbms_output.put_line(r_dk.id_dk||' : '||r_dk.dates||'   summ_before : '||v_summ_before||'   summ_without : '|| v_summ_without ||'   fritst_minus_after : '||v_fritst_minus_after||'   minus_after : '||v_minus_after);
      
      if 
        v_summ_before < v_fritst_minus_after*-1
        or
        v_summ_without < 0
        or
        v_summ_without < v_minus_after*-1
      then
        dbms_output.put_line(r_dk.id_dk||' NOT DELETE');
      else
        dbms_output.put_line(r_dk.id_dk||' DELETE');
        
        delete from pos_dk_scores where id_dk = r_dk.id_dk and id_shop = v_id_shop and id_chek = 'DK100049';
        execute immediate 'delete from pos_dk_scores where id_dk = '''||r_dk.id_dk||''' and id_shop = '''||v_id_shop||''' and id_chek = ''DK100049''';
        
  --      dbms_output.put_line('    delete from pos_dk_scores@'||V_LINK||' where id_dk = '''||r_dk.id_dk||''' and id_shop = '''||v_id_shop||''' and id_chek = ''DK100049''');
      end if;
      
    end loop;
    commit;
  end loop;
end;