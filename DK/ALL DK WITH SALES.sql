select * from st_dk a
where a.dk_country = 'BY';

select id_dk from pos_sale1 
where id_dk != ' ' and extract(year from sale_Date) >= 2019 
group by id_dk;

select 
a.id_dk "НОМЕР КАРТЫ", 
a.fio "ФИО",
trunc(months_between(sysdate, a.birthday)/12) "ВОЗРАСТ", 
case when a.sex = '1' then 'М' when a.sex = '2' then 'Ж' else ' ' end "ПОЛ",
substr(c.shopnum,2) "МАГАЗИН"
from st_dk a
--inner join (
--  select id_dk from pos_sale1 
--  where id_dk != ' ' and extract(year from sale_Date) >= 2019 
--  group by id_dk
--) b on a.id_dk = b.id_dk
inner join s_shop c on a.id_shop = c.shopid
where a.dk_country = 'BY' and a.bit_block = 'F';


select 
a.id_dk "НОМЕР КАРТЫ", 
a.fio "ФИО",
trunc(months_between(sysdate, a.birthday)/12) "ВОЗРАСТ", 
case when a.sex = '1' then 'М' when a.sex = '2' then 'Ж' else ' ' end "ПОЛ",
to_char(b.sale_date, 'dd.mm.yyyy') "ДАТА",
d.art "АРТИКУЛ" ,
case when e.prodh in ('000200486301','000200386301') then 'СУМКА' when e.mtart not in ('ZROH','ZHW3') then 'ОБУВЬ' else 'СОПУТКА' end "ТИП"--,
-- e.prodh, e.mtart
from st_dk a
inner join pos_sale1 b on a.id_dk = b.id_dk and extract(year from sale_Date) >= 2019
inner join pos_sale2 d on b.id_shop = d.id_Shop and b.id_chek = d.id_chek
inner join s_shop c on a.id_shop = c.shopid
inner join s_art e on d.art = e.art
where a.dk_country = 'BY';


