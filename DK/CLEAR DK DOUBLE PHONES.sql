delete from delivery_dk a
where a.id_dk in( select a.id_dk from delivery_dk a
  inner join t_day_st_dk1 b on a.id_dk = b.id_dk
  where 
  a.id_dk not in (select id_dk from white_list) 
  and a.id_del = i_id_del 
  and substr(b.phone_number,2) in (
        select  substr(b.phone_number,2)
        from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk
        where id_del = i_id_del
        group by id_del, substr(b.phone_number,2) having count(*)>1 
  )
  and (substr(b.phone_number,2), b.activation_date) not in(
    select substr(b.phone_number,2), min(b.activation_date) as first_date from delivery_dk a
    inner join t_day_st_dk1 b on a.id_dk = b.id_dk
    where a.id_del = i_id_del
    group by substr(b.phone_number,2) having count(*)>1
  )
);

select * from st_dk 
where 
  substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'),1,2) = 80 
          then '375'||substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'), 3) 
          else REGEXP_REPLACE(phone_number, '[^[:digit:]]') 
         end, 2) 
  = '75447031020';
select * from pos_sale1 where id_dk in ('0000004343998','0000022526540');

--left join (select id_dk, max(sale_date) sale_dete from pos_sale1 where id_dk != ' ' group by id_dk) sale on dk.id_dk = sale.id_dk


select REGEXP_REPLACE("PHONE_NUMBER", '[^[:digit:]]') PHONE_NUM
from st_dk 
where bit_block = 'F' and dk_country = 'BY' and REGEXP_REPLACE("PHONE_NUMBER", '[^[:digit:]]') is not null
group by REGEXP_REPLACE("PHONE_NUMBER", '[^[:digit:]]')
having count(*) > 1
;


select phone_number, id_dk from (
  select 
  row_number() over (partition by b.phone_number order by nvl(c.sale_date, a.activation_date) desc) as rnum,
  b.*, c.sale_date, a.id_dk 
  from st_dk a
  inner join (
    select 
      substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'),1,2) = 80 
              then '375'||substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'), 3) 
              else REGEXP_REPLACE(phone_number, '[^[:digit:]]') 
             end, 2) phone_number,
      count(*) cnt
    from st_dk 
    where --bit_block = 'F' and dk_country = 'BY' and
      REGEXP_REPLACE(phone_number, '[^[:digit:]]') is not null 
    group by 
      substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'),1,2) = 80 
              then '375'||substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'), 3) 
              else REGEXP_REPLACE(phone_number, '[^[:digit:]]') 
             end, 2)
    having count(*) > 1
  ) b on substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(a.phone_number, '[^[:digit:]]'),1,2) = 80 
                  then '375'||substr(REGEXP_REPLACE(a.phone_number, '[^[:digit:]]'), 3) 
                  else REGEXP_REPLACE(a.phone_number, '[^[:digit:]]') 
                end, 2) = b.phone_number
  left join (select id_dk, max(sale_date) sale_date from pos_sale1 where id_dk != ' ' group by id_dk) c on a.id_dk = c.id_dk
--  where b.phone_number = '75296069881'
) where rnum != 1
;

-- ПОИСК КАРТ С ОДИНКОВЫМ НОМЕРОМ ТЕЛЕФОНА
-- ЗАЧИСТКА У ВСЕХ КРОМЕ КАРТЫ С САМОЙ ПОСЛЕДНЕЙ ПРОДАЖЕЙ
set serveroutput on;
declare
  v_id_dk varchar2(13);
begin
  dbms_output.put_line('Start');
  
  for r_row in (
    select phone_number, id_dk from (
      select 
      row_number() over (partition by b.phone_number order by nvl(c.sale_date, a.activation_date) desc) as rnum,
      b.*, c.sale_date, a.id_dk 
      from st_dk a
      inner join (
        select 
          substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'),1,2) = 80 
                  then '375'||substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'), 3) 
                  else REGEXP_REPLACE(phone_number, '[^[:digit:]]') 
                 end, 2) phone_number,
          count(*) cnt
        from st_dk 
        where REGEXP_REPLACE(phone_number, '[^[:digit:]]') is not null 
        group by 
          substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'),1,2) = 80 
                  then '375'||substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'), 3) 
                  else REGEXP_REPLACE(phone_number, '[^[:digit:]]') 
                 end, 2)
        having count(*) > 1
      ) b on substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(a.phone_number, '[^[:digit:]]'),1,2) = 80 
                      then '375'||substr(REGEXP_REPLACE(a.phone_number, '[^[:digit:]]'), 3) 
                      else REGEXP_REPLACE(a.phone_number, '[^[:digit:]]') 
                    end, 2) = b.phone_number
      left join (select id_dk, max(sale_date) sale_date from pos_sale1 where id_dk != ' ' group by id_dk) c on a.id_dk = c.id_dk
    ) where rnum != 1
  ) loop
      -- ЗАТЕРЕТЬ НОМЕР ТЕЛЕФОНА ТЕМ У КОГО ОН ТАКОЙ КАК У ВЫБРАННОЙ КАРТЫ, НО НЕ СОВПАДАЕТ НОМЕР КАРТЫ
      update st_dk set phone_number = ' '
      where id_dk = r_row.id_dk
--        and
--        substr(case when dk_country = 'BY' and substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'),1,2) = 80 
--                then '375'||substr(REGEXP_REPLACE(phone_number, '[^[:digit:]]'), 3) 
--                else REGEXP_REPLACE(phone_number, '[^[:digit:]]') 
--               end, 2) = r_row.phone_number 
--            and 
        ;
    
    dbms_output.put_line(r_row.phone_number); 
  end loop;
  
  
  
  dbms_output.put_line('End'); 
end;