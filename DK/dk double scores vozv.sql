select * from pos_dk_scores
where 
rowid not in (
    select min(rowid) from pos_dk_scores
    where id_dk = '0000000780124'  and doc_type in ('ckv','voz')
    group by id_dk, id_chek, id_sale, dk_sum having count(1) > 1
) 
and 
(id_dk, id_chek, nvl(id_sale,' '), dk_sum) in (
    select id_dk, id_chek, nvl(id_sale,' '), dk_sum from pos_dk_scores
    where id_dk = '0000000780124'  and doc_type in ('ckv','voz')
    group by id_dk, id_chek, id_sale, dk_sum having count(1) > 1
)
and id_dk = '0000000780124'  and doc_type in ('ckv','voz');
            
            
select * from pos_dk_scores order by dates, id_sale,dk_sum;


select id_dk, count(1) shop_cnt  from (
  select id_dk, id_shop from (
    select id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac,
    count(1) cnt, min(rowid) min_rowid
    from pos_dk_scores a
    where doc_type in ('ckv','voz')
    group by id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac
    having count(1) > 1
  ) group by id_dk, id_shop
) 
group by id_dk
having count(1) > 1
;


select id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac,
count(1) cnt, min(rowid) min_rowid
from pos_dk_scores a
where doc_type in ('ckv','voz')
group by id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac
having count(1) > 1
order by dates desc
;