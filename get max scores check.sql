--00000000001002986353
--00000000001104442851 315008-02
--00000000001002651452 416390

SELECT SCAN, CENA3, shop999.CENA2S(SYSDATE, ART, ASIZE, 0, 1, 0) 
FROM shop999.E_OSTTEK 
WHERE shop999.CENA2S(SYSDATE, ART, ASIZE, 0, 1, 0) > (CENA3 / 2) 
GROUP BY SCAN, CENA3, shop999.CENA2S(SYSDATE, ART, ASIZE, 0, 1, 0) HAVING SUM(KOL) > 0
;

SELECT a.SCAN FROM E_OSTTEK a INNER JOIN POS_BRAK b ON a.SCAN = b.SCAN WHERE substr(cena2s(sysdate,a.art,0,1),-2) = '99' AND b.BIT_ALLOW = 'T' GROUP BY a.SCAN HAVING SUM(a.KOL) > 0;

SELECT SCAN FROM E_OSTTEK WHERE substr(cena2s(sysdate,art,0,1),-2) = '99' GROUP BY SCAN HAVING SUM(KOL) > 0;
--00000000001004971400
--00000000001001030359

SELECT * FROM S_ART WHERE GET_DK_BAG_AS_SOP(ART) = 'T';
--195301

SELECT ART, ASIZE, PROCENT, PARTNO, SUM(KOL), CENA2P(SYSDATE, ART, 0, 1) - CENA2S(SYSDATE, ART, 0, 1) AS SKIDKA FROM E_OSTTEK WHERE BAGGINS(ART) = 'T' GROUP BY ART, ASIZE, PROCENT, PARTNO HAVING SUM(KOL) > 0;

select * from pos_dk_scores where id_dk = '0000000000125' order by dates, id_sale,dk_sum;
select * from st_dk where id_dk = '0000000000125';


select get_dk_orange_discount('0000000000125') from dual;

select  a.dk_level from st_dk a where id_dk = '0000000000125' ;
select get_dk_sc_sys_int('ORACNGE_DISCOUNT_LEVEL', '3') from dual;
select sysdate from dual;
select 
*
from DK_SYSTEM
where 
dk_group = 'ORACNGE_DISCOUNT_LEVEL' 
and 
str_val = '3'
and 
trunc(sysdate) between trunc(dateb) and trunc(datee);

select 
dk_max_scores_check_old('1#lacit010-m', '0000000000125') dk_max_scores_check_old, 
dk_max_scores_check('1#lacit010-m', '0000000000125') dk_max_scores_check, 
bep_dk_max_scores_check('1#lacit010-m', '0000000000125') bep_dk_max_scores_check, 
spa_dk_max_scores_check('1#lacit010-m', '0000000000125') spa_dk_max_scores_check 
from dual;


select action_pricelist('416390', sysdate) from dual;
select action_pricelist('315008-02', sysdate) from dual;
get_order_date(' ');



select 
sum(round_dig(LEAST(GREATEST(b.PRICE_SUM + nvl(a.DISCOUNT_SUM, 0) - b.PRICE * 0.5, 0),
(
          case when 1 = 1 and ACTION_PRICELIST(b.art, SYSDATE) = 'T' -- проверка на оранжевый ценник
                    and (baggins(b.art) = 'T' or substr(c.prodh, 0, 4) = '0001') -- проверка на сумку или обувь
          then 
                --1000
                kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID'))
                                  ,0))/100*get_dk_orange_discount('0000000000125'))
                                  
          when action_pricelist(b.art,get_order_date(b.enet_posnr)) = 'F' and 
               IS_ORDER_WITH_DISC(b.enet_posnr,discount_type_id) = 'F'
          then
            --5000
              case when  (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null ) -- проверка на сопутку
              then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                        where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                        get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
              else kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                        where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                        get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
              end 
          --else 
            --666
          end
          )
          ), get_dk_sc_sys_int( 'SCORES_ROUND'), 0)
)  scores
from d_kassa_cur b 
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
left join s_art c on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1))) d on d.docid=discount_type_id
where (discount_type_id = get_dk_sc_sys_str( 'DOCID') or (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                                                                                                                    from d_kassa_disc_cur a
                                                                                                                                    inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                                    where trunc (sysdate) between b.dates and b.datee
                                                                                                                                    and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                                            ))))
;

select get_dk_sc_sys_str( 'DOCID') from dual;

select ACTION_PRICELIST(b.art, SYSDATE), b.*
from d_kassa_cur b 
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
left join s_art c on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1))) d on d.docid=discount_type_id
--where (discount_type_id = 'A000000001' 
--or ((get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and 1 = 2 and b.posnr not in (select posnr 
--                                                                                                            from d_kassa_disc_cur a
--                                                                                                            inner join pos_skidki1 b on a.discount_type_id = b.docid
--                                                                                                            where trunc (sysdate) between b.dates and b.datee
--                                                                                                            and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
--                                                                                                            )
--) or (1 = 1 and discount_type_id = 'A000000001' ) )
where (discount_type_id = get_dk_sc_sys_str( 'DOCID') or (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                                                                                                                    from d_kassa_disc_cur a
                                                                                                                                    inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                                    where trunc (sysdate) between b.dates and b.datee
                                                                                                                                    and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                                            ))))
;
                                                                                                                    
select 
b.posnr, round_dig(LEAST(GREATEST(b.PRICE_SUM + nvl(a.DISCOUNT_SUM, 0) - b.PRICE * 0.5, 0),
(
          case when 1 = 1 and ACTION_PRICELIST(b.art, SYSDATE) = 'T' -- проверка на оранжевый ценник
                    and (baggins(b.art) = 'T' or substr(c.prodh, 0, 4) = '0001') -- проверка на сумку или обувь
          then 
                --1000
                kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID'))
                                  ,0))/100*get_dk_orange_discount('0000000000125'))
                                  
          when action_pricelist(b.art,get_order_date(b.enet_posnr)) = 'F' and 
               IS_ORDER_WITH_DISC(b.enet_posnr,discount_type_id) = 'F'
          then
            --5000
              case when  (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null ) -- проверка на сопутку
              then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                        where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                        get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
              else kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                        where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                        get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
              end 
          --else 
            --666
          end
          )
          ), get_dk_sc_sys_int( 'SCORES_ROUND'), 0)
 scores
from d_kassa_cur b 
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
left join s_art c on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1))) d on d.docid=discount_type_id
where (discount_type_id = get_dk_sc_sys_str( 'DOCID') or (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                                                                                                                    from d_kassa_disc_cur a
                                                                                                                                    inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                                    where trunc (sysdate) between b.dates and b.datee
                                                                                                                                    and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                                            ))))
;
                                                                                                                    
                                                                                                                    
                                                                     
                                                                     
                                                                     
                                                                                                                    
select sum(round_dig(case when  (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null ) 
                  then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                            where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                            get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
                  else kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                            where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                            get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
                  end, get_dk_sc_sys_int( 'SCORES_ROUND'), 0))  scores        
from d_kassa_cur b 
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
left join s_art c on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK19') or (x.disc_type_id in ('ZK29') and 1 = 1))) d on d.docid=discount_type_id
where 
action_pricelist(b.art,get_order_date(b.enet_posnr)) = 'F' and 
IS_ORDER_WITH_DISC(b.enet_posnr,discount_type_id) = 'F' and 
(discount_type_id = get_dk_sc_sys_str( 'DOCID') or (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                                                                                                                            from d_kassa_disc_cur a
                                                                                                                                            inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                                            where trunc (sysdate) between b.dates and b.datee
                                                                                                                                            and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                                                    ))))
                                                                                                                                    ;        
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
select distinct a.* from d_kassa_cur a
inner join d_kassa_disc_cur b on a.posnr = b.posnr and a.kass_id = b.kass_id
inner join pos_skidki1 c on c.docid = B.DISCOUNT_TYPE_ID
where a.kass_id = '1#lacit010-m'
and C.DISC_TYPE_ID in ('ZK02','ZK01')
and ((substr(to_char(a.price_sum),-1,1) = '7' and to_char(a.price_sum) not like '%.%' and C.DISC_TYPE_ID in ('ZK02')) or C.DOC_EXCEPTION = 'F')
;

select distinct a.* from d_kassa_cur a
inner join d_kassa_disc_cur b on a.posnr = b.posnr and a.kass_id = b.kass_id
inner join pos_skidki1 c on c.docid = B.DISCOUNT_TYPE_ID
inner join s_art d on a.art = d.art
left join pos_brak e on a.scan = e.scan
where a.kass_id = '1#lacit010-m' ;

select distinct a.* from d_kassa_cur a 
inner join s_art d on a.art = d.art
left join d_kassa_disc_cur b on a.posnr = b.posnr and a.kass_id = b.kass_id
left join pos_skidki1 c on c.docid = B.DISCOUNT_TYPE_ID
left join pos_brak e on a.scan = e.scan
where a.kass_id = '1#lacit010-m' 
and 
(
    (
        substr(cena2s(sysdate,a.art,0,1),-2) != '99' 
        and substr(d.prodh, 0, 4) = '0001'
        and e.art is null
    )
    or substr(d.prodh, 0, 4) != '0001'
)
and 
a.posnr not in (select posnr from d_kassa_disc_cur where kass_id = '1#lacit010-m' and discount_type_id = 'A000000001')
;

select POSNR, 0, 0 ,'A000000001', '1#lacit010-m'  from d_kassa_cur a
where a.kass_id = '1#lacit010-m' 
and a.posnr not in (select posnr from d_kassa_disc_cur where kass_id = '1#lacit010-m' and discount_type_id = 'A000000001')
;

select * from pos_brak;

select distinct a.* from d_kassa_cur a
inner join d_kassa_disc_cur b on a.posnr = b.posnr and a.kass_id = b.kass_id
inner join pos_skidki1 c on c.docid = B.DISCOUNT_TYPE_ID
where a.kass_id = '1#lacit010-m' 
and ((substr(cena2s(sysdate,a.art,0,1),-2) != '99' 
    and substr(d.prodh, 0, 4) = '0001'
    and e.art is null)
or substr(d.prodh, 0, 4) != '0001');