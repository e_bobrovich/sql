select fl.login as "user", fl.password as "password", dh.id_del||'-'||dh.id_dk as "userMessageId", dh.id_imobis as "messageId", dh.id_del
from delivery_history dh
left join (select id_dk, id_shop from firm.st_dk) sd on dh.id_dk = sd.id_dk
left join firm.SPA_FIL_SHOP sfs on sd.id_shop = sfs.shp
left join s_filial_logins fl on sfs.fil = fl.shopid
left join channel_status chs on dh.id_channel = chs.id_channel and dh.id_status = chs.id_status
where 
fl.login is not null and
fl.password is not null and
--dh.id_del = 2929
--and 
chs.final = 0;


--viber full
select a.id_del||'-'||a.id_dk as id_message, b.phone_number
from (select id_del,id_dk  from delivery_dk where id_del = 3186) a
left join t_day_st_dk1 b on a.id_dk = b.id_dk;

--sms
select a.id_del||'-'||a.id_dk as id_message, b.phone_number
from (select id_del,id_dk  from delivery_dk where id_del = 3186) a
left join t_day_st_dk1 b on a.id_dk = b.id_dk
where a.id_dk not in
(
    select id_dk from delivery_history k 
    where a.id_del = k.id_del and a.id_dk = k.id_dk
    and k.id_channel = 1
    and k.id_status in (21, 20, 22, 7,8,12)
);



--CREATE OR REPLACE FORCE VIEW "IMOB"."V_REPORTS_DEVINO_SMS" ("user", "password", "userMessageId", "messageId", "ID_DEL") AS 
  select fl.login as "user", fl.password as "password", dh.id_del||'-'||dh.id_dk as "userMessageId", dh.id_imobis as "messageId", dh.id_del
from delivery_history dh
left join (select id_dk, id_shop from firm.st_dk) sd on dh.id_dk = sd.id_dk
left join firm.SPA_FIL_SHOP sfs on sd.id_shop = sfs.shp
left join s_filial_logins fl on sfs.fil = fl.shopid
left join channel_status chs on dh.id_channel = chs.id_channel and dh.id_status = chs.id_status
where 
fl.login is not null and
fl.password is not null and
--dh.id_del = 3186 and
dh.id_channel = 2 and
chs.final = 0;

select * from v_reports_devino_sms where id_del = 3186;
select * from v_reports_devino_viber where id_del = 3186;



--id=333852655&uid=3205-0000019496467&status=DELIVRD&mess_status=DELIVRD&sent=1529587303&done=1529587305&operator=4&operator_name=TELE2&channel_final=viber&messenger_final=viber&count=1&price=1.11

select 
case when length('1529587303') = length(regexp_replace('1529587303','[^[[:digit:]]]*'))                
then to_number('1529587303') else 
    case when round((to_date('1529587303','yyyy-mm-dd hh24:mi:ss')  - date '1970-01-01') * 60 * 60 * 24) 
end as a,

case when length('2018-06-22 16:17:20') = length(regexp_replace('2018-06-22 16:17:20','[^[[:digit:]]]*'))                
then to_number('2018-06-22 16:17:20') else round((to_date('2018-06-22 16:17:20','yyyy-mm-dd hh24:mi:ss')  - date '1970-01-01') * 60 * 60 * 24) 
end as b

from dual;

select round((to_date('2018-06-22 16:17:20','yyyy-mm-dd hh24:mi:ss')  - date '1970-01-01') * 60 * 60 * 24)  from dual;