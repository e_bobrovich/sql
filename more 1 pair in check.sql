select count (1) from delivery_dk where id_Del = 4582;

select count (1) from delivery_dk where id_Del = 4582 and id_dk in (
    select distinct a.id_dk from firm.pos_sale1 a
    inner join firm.pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
    inner join firm.s_art c on b.art = c.art
    where a.sale_date >= to_date('01.01.2017','dd.mm.yyyy') and a.sale_date <= to_date('31.12.2018','dd.mm.yyyy')
    and a.id_dk != ' ' and c.mtart not in ('ZROH','ZHW3')
    group by a.id_dk, a.id_chek having count(1) > 1
);

delete from delivery_dk where id_Del = 4582 and id_dk not in (
    select distinct a.id_dk from firm.pos_sale1 a
    inner join firm.pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
    inner join firm.s_art c on b.art = c.art
    where a.sale_date >= to_date('01.01.2017','dd.mm.yyyy') and a.sale_date <= to_date('31.12.2018','dd.mm.yyyy')
    and a.id_dk != ' ' and c.mtart not in ('ZROH','ZHW3')
    group by a.id_dk, a.id_chek having count(1) > 1
);