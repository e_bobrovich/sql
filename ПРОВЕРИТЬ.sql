select sh.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
     left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1243) 
    and 
    c.shopparent in ('1UF1','25F2','44F1')
    and
    d.sale_date between
    to_date('07.09.2017', 'DD.MM.YYYY') and to_date('09.09.2017', 'DD.MM.YYYY')
    and b.season != ' '
     group by c.shopparent
    order by c.shopparent
  ) sh
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
     left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1243) 
    and 
    c.shopparent in ('1UF1','25F2','44F1')
    and
    d.sale_date between
    to_date('07.09.2017', 'DD.MM.YYYY') and to_date('09.09.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) >0
     group by c.shopparent
    order by c.shopparent
  )bg on sh.shopparent = bg.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
     left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1243) 
    and 
    c.shopparent in ('1UF1','25F2','44F1')
    and
    d.sale_date between
    to_date('07.09.2017', 'DD.MM.YYYY') and to_date('09.09.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) <=0
     group by c.shopparent
    order by c.shopparent
  )sp on sh.shopparent = sp.shopparent  
  order by sh.shopparent
