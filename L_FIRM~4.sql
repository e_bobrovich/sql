select a.id_dk, a.id_shop, a.dates
from pos_dk_scores a
inner join POS_DK_STOCK1 b on nvl(a.id_ac, a.id_chek) = b.id and a.dk_sum > 0
where --a.id_shop = 'FIRM' 
         sysdate between trunc(a.dates) and tdv_dk.get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count) + 90 
         and a.id_dk = case when '0000027525418' is null then a.id_dk else '0000027525418' end
         and b.id = case when 'DK100003' is null then b.id else 'DK100003' end
group by a.id_dk, a.id_shop, a.dates;

select burn_date, burn_period, burn_count 
    from pos_dk_stock3 
    where id = 'DK100003' and id_shop = '0066';
    
    
SET SERVEROUTPUT ON;
declare  
i_add_date date ; --дата, когда баллы были начислены
i_id_ac varchar2(20) := 'DK100003';
i_id_shop varchar2(20) := '0064';

i_burn_date date;   -- если баллы сгорают на дату
i_burn_period varchar2(20); -- период действия баллов
i_burn_count number; -- кол-во периодов действия баллов

  v_ret_date date;                                            
 begin
    i_add_date := to_date('08.03.18 13:32:59', 'dd.mm.yy hh24:mi:ss');
 
    begin
        select burn_date, burn_period, burn_count 
        into i_burn_date, i_burn_period, i_burn_count 
        from pos_dk_stock3 
        where id = i_id_ac and id_shop = i_id_shop;
        
        dbms_output.put_line('i_burn_date : '||i_burn_date||' i_burn_period : '||i_burn_period||' i_burn_count : '||i_burn_count);
       
        exception when no_data_found then 
            select burn_date, burn_period, burn_count 
            into i_burn_date, i_burn_period, i_burn_count 
            from pos_dk_stock3 
            where id = i_id_ac and id_shop is null ;
            
            dbms_output.put_line('i_burn_date : '||i_burn_date||' i_burn_period : '||i_burn_period||' i_burn_count : '||i_burn_count);
    end;
    
    dbms_output.put_line('final burn date : '||tdv_dk.get_scores_burn_date(i_add_date, i_burn_date, i_burn_period, i_burn_count));
        
   --return trunc(v_ret_date);
 end;  
 
 
 insert into pos_dk_stock3
 select id, null, burn_count, burn_date, burn_period from pos_dk_stock1 where id != 'DK100003';
