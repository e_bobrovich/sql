call tdv_dk_copy_create('0000000226295', '0000000386329', '2904');

select * from backup_firm_trade_10.st_dk where id_dk = '0000016104938';

select * from pos_dk_copy_logs order by ddate desc;
select * from pos_dk_copy_logs where id_dk_from = '0000027027899' or id_dk_to = '0000027027899' order by ddate desc;

select * from firm.pos_sale1 where id_dk = '0000016104938';
select * from firm.pos_sale1 where id_dk = '0000025846430';

select * from st_dk where id_dk in ('0000016104938', '0000025846430');
select * from st_dk@S0028 where id_dk in ('0000016104938', '0000025846430');

select * from pos_dk_scores where id_dk in ('0000016104938', '0000025846430');
select * from pos_dk_scores@S2222 where id_dk in ('0000016104938', '0000025846430');

select * from pos_dk_scores where id_dk in ('0000016104938', '0000025846430');
select * from pos_dk_scores where id_dk = '0000020913724';


select * from pos_dk_scores@s2330 where id_dk = '0000025777536';

select count(*) from pos_dk_scores@S3126 where id_dk = '0000024693851';
select count(*) from pos_dk_scores@S2904 where id_dk = '0000000386329';
select count(*) from pos_dk_scores where id_dk = '0000026223858' and id_shop = '3124';
select count(*) from pos_dk_scores where id_dk = '0000015173515' and id_shop = '3124';
      
SET SERVEROUTPUT ON;
  
declare
    i_shop varchar2(10) := '0010';
    i_dk varchar2(20) := '0000020913724';

   ddl_ins VARCHAR2(3000);   
   v_link varchar2(20); 
   v_land varchar2(20);
   v_scores number(18,2)  default null;
   v_sum st_dk.summ%type;
   v_dk_level st_dk.dk_level%type;
   v_dk_discount st_dk.discount%type;
  
    v_dk_new varchar2(20);
    v_dk_old varchar2(20);
    
    v_firm_cnt_old number;
    v_firm_cnt_new number;
    v_shop_cnt_old number;
    v_shop_cnt_new number;
      
   V_SELE varchar2(2048);
   pole varchar2(500);
begin

    select case when substr(i_shop,1,2) = '00' then 'BY' else 'RU' end into v_land from dual;

    v_link := firm.get_shop_link(i_shop); 
    dbms_output.put_line(v_link);
    
    begin
        select id_dk_from, id_dk_to into v_dk_old, v_dk_new from 
        (select id_dk_from, id_dk_to from firm.pos_dk_copy_logs a where a.id_dk_to = i_dk or a.id_dk_from = i_dk order by ddate desc) 
        where rownum = 1;
        dbms_output.put_line('v_dk_old :'||v_dk_old||' v_dk_new:'||v_dk_new);
        
        if (v_dk_old is not null and v_dk_new is not null) then
            select count(*) into v_firm_cnt_new from firm.pos_dk_scores where id_dk = v_dk_new and id_shop = i_shop;
            select count(*) into v_firm_cnt_old from firm.pos_dk_scores where id_dk = v_dk_old and id_shop in ('FIRM', i_shop);
            execute immediate 'select count(*) from pos_dk_scores@'||v_link||' where id_dk = '||v_dk_new into v_shop_cnt_new;
            execute immediate 'select count(*) from pos_dk_scores@'||v_link||' where id_dk = '||v_dk_old into v_shop_cnt_old;
            dbms_output.put_line('v_firm_cnt_new :'||v_firm_cnt_new||' v_firm_cnt_old:'||v_firm_cnt_old);
            dbms_output.put_line('v_shop_cnt_new :'||v_shop_cnt_new||' v_shop_cnt_old:'||v_shop_cnt_old);
    
            if (0 < v_shop_cnt_old) then
                execute immediate 'update pos_dk_scores@'||v_link||' set id_dk = '''||v_dk_new||''' where id_dk = '''||v_dk_old||''''; 
                dbms_output.put_line('v_shop_cnt_old replaced to v_shop_cnt_new');
                --commit;
            end if;
            if (0 < v_firm_cnt_old) then
                update pos_dk_scores set id_dk = v_dk_new where id_dk = v_dk_old and id_shop in ('FIRM', i_shop);          
                dbms_output.put_line('v_firm_cnt_old replaced to v_firm_cnt_new');
                --commit;
            end if;
        else
            dbms_output.put_line('replaced clearly');    
        end if;
        
        
    exception when no_data_found then 
        dbms_output.put_line('replace didn''t need');
    end;  

    --tdv_refresh_one_dk(i_shop,i_dk);
 exception when others then
    dbms_output.put_line(sqlcode||sqlerrm);
    rollback;
    dbms_output.put_line('tdv_refresh_one_dk_test -- i_shop = '||i_shop||' i_dk = '||i_dk);                   
end;