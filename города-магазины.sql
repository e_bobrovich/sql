select b.cityname, nvl(b.cnt_a, 0) a, nvl(c.cnt_f, 0) f, nvl(d.cnt_t, 0) t, (nvl(b.cnt_a, 0)-nvl(c.cnt_f, 0)) diff_a_f from(
  select a.cityname, nvl(count(*), 0) as cnt_a from firm.st_shop a
  where a.cityname != ' '
  group by a.cityname
  ) b
left join (
  select a.cityname, nvl(count(*), 0) as cnt_f from firm.st_shop a where a.bit_open = 'F'
  and a.cityname != ' '
  group by a.cityname
  ) c on b.cityname = c.cityname
left join (
  select a.cityname, nvl(count(*), 0) as cnt_t from firm.st_shop a where a.bit_open = 'T'
  and a.cityname != ' '
  group by a.cityname
  ) d on b.cityname = d.cityname
  where (nvl(b.cnt_a, 0)-nvl(c.cnt_f, 0)) = 0
  or nvl(d.cnt_t, 0) = 0
  --where b.cnt_a is null or or 
;
  
  
select a.id_dk, b.id_shop, c.cityname from delivery_dk a 
inner join firm.st_dk b on a.id_dk = b.id_dk
inner join firm.st_shop c on b.id_shop = c.shopid
where a.id_del = 1263
and c.cityname in
  (select a.cityname from firm.st_shop a where a.bit_open = 'T'
  and a.cityname != ' '
  group by a.cityname)
;

select a.id_dk, b.id_shop, c.cityname from delivery_dk a 
inner join firm.st_dk b on a.id_dk = b.id_dk
inner join firm.st_shop c on b.id_shop = c.shopid
where a.id_del = 1262
;

select a.cityname from firm.st_shop a where a.bit_open = 'T'
  and a.cityname != ' '
  group by a.cityname
  ;
  
select a.id_dk, b.id_shop from delivery_dk a
inner join firm.st_dk b on a.id_dk = b.id_dk
inner join firm.st_shop c on b.id_shop = c.shopid
where a.id_del = 1261
and c.cityname in (
  select d.cityname from firm.st_shop d
  where d.bit_open = 'T'
  and d.cityname != ' '
  group by d.cityname
)
;

select a.shopid from firm.st_shop a
where a.cityname in (
select b.cityname from(
  select a.cityname, nvl(count(*), 0) as cnt_a from firm.st_shop a
  where a.cityname != ' '
  group by a.cityname
  ) b
left join (
  select a.cityname, nvl(count(*), 0) as cnt_f from firm.st_shop a where a.bit_open = 'F'
  and a.cityname != ' '
  group by a.cityname
  ) c on b.cityname = c.cityname
left join (
  select a.cityname, nvl(count(*), 0) as cnt_t from firm.st_shop a where a.bit_open = 'T'
  and a.cityname != ' '
  group by a.cityname
  ) d on b.cityname = d.cityname
  where (nvl(b.cnt_a, 0)-nvl(c.cnt_f, 0)) = 0
  or nvl(d.cnt_t, 0) = 0
  --where b.cnt_a is null or or 
);


select 
  (case when count(*) = 0 then 'FALSE' else 'TRUE' end) as res
   from (
    select a.shopid from firm.st_shop a
    where a.cityname in (
    select b.cityname from(
      select a.cityname, nvl(count(*), 0) as cnt_a from firm.st_shop a
      where a.cityname != ' '
      group by a.cityname
      ) b
    left join (
      select a.cityname, nvl(count(*), 0) as cnt_f from firm.st_shop a where a.bit_open = 'F'
      and a.cityname != ' '
      group by a.cityname
      ) c on b.cityname = c.cityname
    left join (
      select a.cityname, nvl(count(*), 0) as cnt_t from firm.st_shop a where a.bit_open = 'T'
      and a.cityname != ' '
      group by a.cityname
      ) d on b.cityname = d.cityname
      where (nvl(b.cnt_a, 0)-nvl(c.cnt_f, 0)) = 0
      or nvl(d.cnt_t, 0) = 0
      --where b.cnt_a is null or or 
    )
  ) a
  where a.shopid = '4100';
