select * from pos_dk_copy_logs a where a.id_dk_to = '0000025825817';

select * from pos_dk_scores@s4505 where id_dk = '0000011269502';
select * from pos_sale1@s4505 where id_dk = '0000025825817';
select * from pos_dk_scores where id_dk = '0000025825817';

-----
-- получение старой карты из истории переноса
select id_dk_from from (select id_dk_from from pos_dk_copy_logs a where a.id_dk_to = '0000025825817' order by ddate desc) where rownum = 1;
-- 0000011269502

-- сравнение истории алоов со старой
select count(*) cnt_new from pos_dk_scores where id_dk = '0000025825817' and id_shop = '4505';
select count(*) cnt_old from pos_dk_scores where id_dk = '0000011269502' and id_shop = '4505';

select case when (firm_cnt_old = 0 and firm_cnt_new > firm_cnt_old and shop_cnt_new = 0 and shop_cnt_old > shop_cnt_new) then 'T' else 'F' end is_need from 
(
    select count(*) firm_cnt_new from pos_dk_scores where id_dk = '0000025825817' and id_shop = '4505'
) a,
(
    select count(*) firm_cnt_old from pos_dk_scores where id_dk = '0000011269502' and id_shop = '4505'
) b,
(
    select count(*) shop_cnt_new from pos_dk_scores@S4505 where id_dk = '0000025825817'
) c,
(
    select count(*) shop_cnt_old from pos_dk_scores@S4505 where id_dk = '0000011269502'
) d;