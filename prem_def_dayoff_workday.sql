select date_n, b.id_shop id_shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day, to_char(date_n, 'd') weekday,
round(case when to_char(date_n, 'd') not in (6,7) then
  (nvl(e.plan_sum, d.plan_sum) * (workday+dayoff) - (nvl(e.plan_sum, d.plan_sum) * 1.2 * dayoff))/workday 
else
  nvl(e.plan_sum, d.plan_sum) * 1.2 
end, 2) plan_sum,
round(case when to_char(date_n, 'd') not in (6,7) then
  (nvl(e.plan_pair, d.plan_pair) * (workday+dayoff) - (nvl(e.plan_pair, d.plan_pair) * 1.2 * dayoff))/workday 
else
  nvl(e.plan_pair, d.plan_pair) * 1.2 
end,0) plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||4||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 4 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(4||'.'||2019,'mm.yyyy')))
) a 
inner join (select count(*) workday from (
    select to_date(( 1 + level - 1)||'.'||4||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 4 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(4||'.'||2019,'mm.yyyy')))
  ) where to_char(date_n, 'd') not in (6,7)
) q on a.date_n = a.date_n
inner join (select count(*) dayoff from (
    select to_date(( 1 + level - 1)||'.'||4||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 4 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(4||'.'||2019,'mm.yyyy')))
  ) where to_char(date_n, 'd') in (6,7)
) w on a.date_n = a.date_n
inner join bep_prem_shop_plan b on b.id_shop = b.id_shop and a.month = b.month and a.year = b.year
left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
where b.id_shop =  '0001' and b.year = 2019 and b.month = 4
order by a.date_n;



select a.id_shop, a.year, a.month, 
cast((nvl(a.plan_sum, 0)-nvl(b.plan_sum,0))/(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-nvl(b.edit_days,0)) as number(18,2)) def_plan_sum, 
cast((nvl(a.plan_pair,0)-nvl(b.plan_pair,0))/(extract(day from last_day(to_date(a.month||'.'||a.year, 'mm.yyyy')))-nvl(b.edit_days,0)) as number(18,2)) def_plan_pair,
workday, dayoff,nvl(b.edit_days,0) edit_days
from bep_prem_shop_plan a
left join 
(
    select id_shop, extract(year from edit_date) year, 
    extract(month from edit_date) month,
    sum(plan_sum) plan_sum,
    sum(plan_pair) plan_pair,
    count(case when to_char(edit_date, 'd') not in (6,7) then 1 else 0 end) as edit_workdays,
    count(case when to_char(edit_date, 'd') in (6,7) then 1 else 0 end) as edit_dayoff
    from bep_prem_edit_day_shop_plan group by id_shop,
    extract(year from edit_date), extract(month from edit_date)
)b on a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
inner join (select count(*) workday from (
    select to_date(( 1 + level - 1)||'.'||4||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 4 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(4||'.'||2019,'mm.yyyy')))
  ) where to_char(date_n, 'd') not in (6,7)
) q on 1 = 1
inner join (select count(*) dayoff from (
    select to_date(( 1 + level - 1)||'.'||4||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 4 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(4||'.'||2019,'mm.yyyy')))
  ) where to_char(date_n, 'd') in (6,7)
) w on 1 = 1
where a.id_shop = '0001'
and a.year = 2019 and a.month = 4
;

select id_shop, extract(year from edit_date) year, 
extract(month from edit_date) month,
edit_date,
to_char(edit_date, 'd'),
case when to_char(edit_date, 'd') not in (6,7) then 1 else 0 end edit_workdays,
case when to_char(edit_date, 'd') in (6,7) then 1 else 0 end edit_dayoff
--count(case when to_char(edit_date, 'd') not in (6,7) then 1 else 0 end) as edit_workdays,
--count(case when to_char(edit_date, 'd') in (6,7) then 1 else 0 end) as edit_dayoff
from bep_prem_edit_day_shop_plan;

select id_shop, extract(year from edit_date) year, 
    extract(month from edit_date) month,
    sum(plan_sum) plan_sum,
    sum(plan_pair) plan_pair,
    count(1) as edit_days
    from bep_prem_edit_day_shop_plan group by id_shop,
    extract(year from edit_date), extract(month from edit_date);
    
    
    
    
select a.*, q.*, w.*, q.workday-1, w.dayoff-0,
cast((a.sum_ost - (a.def_sum*1.2*(w.dayoff-0)))/(q.workday-1) as number(18,2)) workday_sum,
cast((a.pair_ost - (a.def_pair*1.2*(w.dayoff-0)))/(q.workday-1) as number(18,2)) workday_pair,
cast(a.def_sum*1.2 as number(18,2)) dayoff_sum,
cast(a.def_pair*1.2 as number(18,2)) dayoff_pair,
(a.sum_ost - (a.def_sum*1.2*(w.dayoff-0))) temp_wordkday_sum
from 
(
  select a.id_shop, a.year, a.month, 
  b.plan_sum def_sum, 
  b.plan_Pair def_pair,
  cast((nvl(a.plan_sum, 0)-nvl(1000,0)-nvl(0,0)) as number(18,2)) sum_ost,
  cast((nvl(a.plan_pair,0)-nvl(95,0)-nvl(0,0)) as number (18,2)) pair_ost
  from bep_prem_shop_plan a
  inner join bep_prem_def_day_shop_plan b on a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
  where a.id_shop = '0029'
  and a.year = 2019 and a.month = 5
) a
inner join (select count(*) workday from (
    select to_date(( 1 + level - 1)||'.'||5||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 5 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(5||'.'||2019,'mm.yyyy')))
  ) where to_char(date_n, 'd') not in (6,7)
) q on 1 = 1
inner join (select count(*) dayoff from (
    select to_date(( 1 + level - 1)||'.'||5||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 5 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(5||'.'||2019,'mm.yyyy')))
  ) where to_char(date_n, 'd') in (6,7)
) w on 1 = 1
;



select date_n, b.id_shop id_shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
nvl(e.plan_sum, case when to_char(date_n, 'd') in (6,7) then d.dayoff_sum else d.workday_sum end) plan_sum,
nvl(e.plan_pair, case when to_char(date_n, 'd') in (6,7) then d.dayoff_pair else d.workday_pair end) plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||3||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 3 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(3||'.'||2019,'mm.yyyy')))
) a 
inner join bep_prem_shop_plan b on b.id_shop = b.id_shop and a.month = b.month and a.year = b.year
left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
where b.id_shop =  '0029' and b.year = 2019 and b.month = 3
order by a.date_n;