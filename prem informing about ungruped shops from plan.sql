SELECT DISTINCT 'F' AS BIT_SELE, a.shopid, substr(c.shopnum,2,5) shopnum, a.shopname FROM ST_SHOP a 
inner join s_shop c on a.shopid = c.shopid 
INNER JOIN st_retail_hierarchy b ON a.shopid = b.shopid 
WHERE b.SHOPPARENT IN ('37F1') AND a.ORG_KOD = 'SHP'
AND A.SHOPID IN 
(    SELECT GS.ID_SHOP FROM BEP_PREM_S_GROUP SG
    inner join bep_prem_group_shop gs on sg.id_group = gs.id_group
    inner join (select id_shop from bep_prem_shop_plan where year = 2019 and month = 5) sp on gs.id_shop = sp.id_shop
 WHERE SG.YEAR = 2019 AND SG.MONTH = 5) ORDER BY a.shopid
 ;
 

select gs.id_shop from bep_prem_s_group sg
inner join bep_prem_group_shop gs on sg.id_group = gs.id_group
inner join (select id_shop from bep_prem_shop_plan where year = 2019 and month = 5) sp on gs.id_shop = sp.id_shop;

select '['||sp.month||'.'||sp.year||'] - '||sp.id_shop||' - '||substr(ss.shopnum, 2, 5)||' - '||ss.shopname
from (
    select id_shop, year, month from bep_prem_shop_plan 
    where id_shop not in ('S777', 'S888') 
    and to_date(month||'.'||year, 'mm.yyyy') >= case when  
                                    extract(month from sysdate) = 1 
                                    then
                                        to_date('12'||'.'||extract(year from sysdate), 'mm.yyyy') 
                                    else 
                                        to_date((extract(month from sysdate)-1)||'.'||extract(year from sysdate), 'mm.yyyy')
                                    end
) sp
left join (
    select gs.id_shop, sg.year, sg.month from bep_prem_s_group sg 
    inner join bep_prem_group_shop gs on sg.id_group = gs.id_group
    where sg.dateb is null and sg.datee is null
) sg on sp.id_shop = sg.id_shop and sp.year = sg.year and sp.month = sg.month
left join s_shop ss on sp.id_shop = ss.shopid 
where sg.id_shop is null
order by sp.year, sp.month, sp.id_shop
;

select id_shop, year, month from bep_prem_shop_plan 
    where id_shop not in ('S777', 'S888') 
    and to_date(month||'.'||year, 'mm.yyyy') >= case when  
                                    extract(month from sysdate) = 1 
                                    then
                                        to_date('12'||'.'||extract(year from sysdate), 'mm.yyyy') 
                                    else 
                                        to_date((extract(month from sysdate)-1)||'.'||extract(year from sysdate), 'mm.yyyy')
                                    end
                                ;
select 
extract(month from sysdate) a1,
extract(month from sysdate)-1 a2,
extract(year from sysdate) a3,
to_date(12||'.'||extract(year from sysdate), 'mm.yyyy')  a4,
to_date((extract(month from sysdate)-1)||'.'||extract(year from sysdate), 'mm.yyyy') a5,
case when 1=1 then 1 else 0 end a6,
case when extract(month from sysdate) = 1 then 1 else 0 end a7
from dual;

select case when  
        extract(month from sysdate) = 1 
        then
            to_date('12'||'.'||extract(year from sysdate), 'mm.yyyy') 
        else 
            to_date((extract(month from sysdate)-1)||'.'||extract(year from sysdate), 'mm.yyyy')
        end
from dual;