select sum(shoes), sum(bags), sum(other) from (
select fil.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
  select b.shopparent from (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b
  where b.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
  group by b.shopparent
  order by b.shopparent
) fil 
left join (
    select c.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (2035,2033,2032,2031,2030,2029,2028,2026,2025,2024,2022,2021) 
    and 
    c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
    and
    trunc(d.sale_date) between
    to_date('04.01.2018', 'DD.MM.YYYY') and to_date('11.01.2018', 'DD.MM.YYYY')
    and b.prodh like '0001%'
    and (dh.id_status = 5 or dh.id_status = 6)
 and (dd.groupt = 'O' or dd.groupt is null)
    and (to_char(d.id_chek), d.id_shop) not in 
    (
         select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
    )       and d.bit_close = 'T' and d.bit_vozvr = 'F'    group by c.shopparent
    order by c.shopparent
) sh on fil.shopparent = sh.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (2035,2033,2032,2031,2030,2029,2028,2026,2025,2024,2022,2021) 
    and 
    c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
    and
    trunc(d.sale_date) between
    to_date('04.01.2018', 'DD.MM.YYYY') and to_date('11.01.2018', 'DD.MM.YYYY')
    and b.prodh like '00020038%'
    and (dh.id_status = 5 or dh.id_status = 6)
 and (dd.groupt = 'O' or dd.groupt is null)
    and (to_char(d.id_chek), d.id_shop) not in 
    (
         select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
    )       and d.bit_close = 'T' and d.bit_vozvr = 'F'    group by c.shopparent
    order by c.shopparent
  )bg on fil.shopparent = bg.shopparent
full join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (2035,2033,2032,2031,2030,2029,2028,2026,2025,2024,2022,2021) 
    and 
    c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
    and
    trunc(d.sale_date) between
    to_date('04.01.2018', 'DD.MM.YYYY') and to_date('11.01.2018', 'DD.MM.YYYY')
    and b.prodh not like '0001%' and b.prodh not like '00020038%'
    and (dh.id_status = 5 or dh.id_status = 6)
 and (dd.groupt = 'O' or dd.groupt is null)
    and (to_char(d.id_chek), d.id_shop) not in 
    (
         select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
    )       and d.bit_close = 'T' and d.bit_vozvr = 'F'    group by c.shopparent
    order by c.shopparent
  )sp on fil.shopparent = sp.shopparent  
  order by fil.shopparent
);


select sum(shoes),sum(bags),sum(other) from (
select fil.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
  select b.shopparent from (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b
  where b.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
  group by b.shopparent
  order by b.shopparent
) fil 
left join (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (2035,2033,2032,2031,2030,2029,2028,2026,2025,2024,2022,2021) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
       and
       trunc(d.sale_date) between
       to_date('04.01.2018', 'DD.MM.YYYY') and to_date('11.01.2018', 'DD.MM.YYYY')
 and (dd.groupt = 'O' or dd.groupt is null)
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where b.prodh like '0001%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F'       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
) sh on fil.shopparent = sh.shopparent
left join 
(
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (2035,2033,2032,2031,2030,2029,2028,2026,2025,2024,2022,2021) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
       and
       trunc(d.sale_date) between
       to_date('04.01.2018', 'DD.MM.YYYY') and to_date('11.01.2018', 'DD.MM.YYYY')
 and (dd.groupt = 'O' or dd.groupt is null)
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where  b.prodh like '00020038%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F'       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )bg on fil.shopparent = bg.shopparent
left join 
  (
    select shopparent, count(*) as colvo from (
       select c.shopparent, dd.id_dk, count(*) as colv from delivery_dk dd
       left join firm.pos_sale1 d on dd.id_dk = d.id_dk
       left join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) c on d.id_shop = c.shopid
       where 
       dd.id_del in (2035,2033,2032,2031,2030,2029,2028,2026,2025,2024,2022,2021) 
       and 
       c.shopparent in ('1UF1','21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1')
       and
       trunc(d.sale_date) between
       to_date('04.01.2018', 'DD.MM.YYYY') and to_date('11.01.2018', 'DD.MM.YYYY')
 and (dd.groupt = 'O' or dd.groupt is null)
       and (d.id_chek, d.id_shop) in (
           select id_chek, id_shop from firm.pos_sale2 a left join firm.s_art b on a.art = b.art 
           where b.prodh not like '0001%' and b.prodh not like '00020038%'
       )
       and (to_char(d.id_chek), d.id_shop) not in 
        (
            select ndoc,id_shop from firm.d_prixod1 where shopid = d.id_shop and ndoc = to_char(d.id_chek)
        )       and d.bit_close = 'T' and d.bit_vozvr = 'F'       group by c.shopparent, dd.id_dk
       order by c.shopparent
   )
   group by shopparent
   order by shopparent
  )sp on fil.shopparent = sp.shopparent  
  order by fil.shopparent

);