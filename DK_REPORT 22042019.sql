select 
a.id_dk, 
case when a.id_shop = ' ' then d.id_Shop else a.id_shop end id_shop, 
b.shopname, a.summ, nvl(a.scores, 0) scores, nvl(c.count_operations, 0) count_operations
from st_dk a 
left join (
  select x.id_dk, x.id_shop from pos_sale1 x where (x.id_dk, x.sale_date) = (
    select y.id_dk, min(y.sale_date) from pos_sale1 y where y.id_Dk != ' ' and x.id_dk = y.id_dk group by id_dk
  )
) d on a.id_dk = d.id_dk
left join (select shopid, substr(shopnum,2,5) shopnum, shopname from s_shop) b on case when a.id_shop = ' ' then d.id_Shop else a.id_shop end = b.shopid
left join (select id_dk, count(1) count_operations from pos_dk_scores where id_shop != 'FIRM' and to_char(dates, 'yyyymm') = '201904' group by id_dk) c on a.id_dk = c.id_dk

where a.bit_block = 'F' 
and a.dk_country = 'BY'
and a.id_shop not in ('S888', 'S999')
and a.id_dk not in ('0000000000605', '0000000000606')
and substr(case when a.id_shop = ' ' then d.id_Shop else a.id_shop end, 1,2) = '00'
;

select * from st_dk order by id_dk;

select * from st_dk where id_dk = '0000010025857';
select * from pos_sale1 where id_dk = '0000016045743';

select id_dk, count(1) from pos_dk_scores where id_shop != 'FIRM' group by id_dk;

select * from st_dk;
select id_dk, id_shop from pos_sale1 where id_dk != ' ' order by sale_date;

select x.id_dk, x.id_shop from pos_sale1 x where (x.id_dk, x.sale_date) = (
  select y.id_dk, min(y.sale_date) from pos_sale1 y where y.id_Dk != ' ' and x.id_dk = y.id_dk group by id_dk
);

select * from pos_dk_scores where id_dk = '0000013860318' order by dates, id_chek, id_sale, dk_sum;
select id_dk, count(1) count_operations from pos_dk_scores where id_shop != 'FIRM' group by id_dk;

select x.id_dk, x.id_shop from pos_sale1 x where (x.id_dk, x.sale_date) = (
    select y.id_dk, min(y.sale_date) from pos_sale1 y where y.id_Dk != ' ' and '0000010025857' = y.id_dk group by id_dk
)