create or replace function DK_MAX_SCORES_CHECK_test(i_kass_id in varchar2,
                                                                      i_id_dk in varchar2 default 'XXX',
                                                                      i_is_seller_dk varchar2 default 'F') return number is
  Result number;
  v_docid pos_skidki1.docid%type;
  v_max_summ pos_sale1.sale_sum%type;
  v_day_lim number(18);
  v_sys_dk_with_disc ri_system.sys_val%type;
  v_round number(18,2) ;
  v_numconf number(10);
	v_zk29disc varchar2(5);
begin
  select numconf into v_numconf
    from config;

  v_sys_dk_with_disc := get_system_val('DK_SCORES_WITH_DISCOUNT_PRICE');

  -- номер документа скидки по дк
  v_docid := get_dk_sc_sys_str( 'DOCID') ;

  -- до скольки баллов округляем
  v_round := get_dk_sc_sys_int( 'SCORES_ROUND') ;
	
	select case when kol > 0 then 'T' else 'F' end into v_zk29disc
		from (select count(*) kol
					from d_kassa_disc_cur a
					inner join pos_skidki1 b on a.discount_type_id = b.docid
					where kass_id = i_kass_id and trunc(sysdate) between b.dates and b.datee
						and b.disc_type_id = 'ZK29');

  if v_sys_dk_with_disc = 'T' then -- если расчитываем колличество баллов от цены со скидкой
    select sum(round_dig(case when  (get_dk_bag_as_sop(b.art, i_is_seller_dk) = 'T'  and d.docid is null ) 
                          then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                                    where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=v_docid),0))/100*
                                                                                                                    get_dk_sc_sys_int( case when i_is_seller_dk = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
                          else kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                                    where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=v_docid),0))/100*
                                                                                                                    get_dk_sc_sys_int( case when i_is_seller_dk = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
                          end, v_round, 0))  scores
      into Result          
      from d_kassa_cur b 
      left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
      left join s_art c on b.art = c.art
      left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and v_numconf = 1))) d on d.docid=discount_type_id
      where (discount_type_id = v_docid or (get_dk_bag_as_sop(b.art, i_is_seller_dk) = 'T'  and d.docid is null and not (v_numconf = 2 and b.posnr in (select posnr 
																																																																														from d_kassa_disc_cur a
																																																																														inner join pos_skidki1 b on a.discount_type_id = b.docid
																																																																														where trunc (sysdate) between b.dates and b.datee
																																																																														and b.disc_type_id = 'ZK25' and a.kass_id = i_kass_id
																																																																												))))
             and b.kass_id = i_kass_id and b.procent = 0;   
  else -- если расчитываем  от цены без скидки 
    select sum(round_dig(case when   (get_dk_bag_as_sop(b.art, i_is_seller_dk) = 'T'  and d.docid is null )
                          then kol * (price/100*get_dk_sc_sys_int( case when i_is_seller_dk = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
                          else kol * (price/100*get_dk_sc_sys_int( case when i_is_seller_dk = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
                          end, v_round, 0))  scores
      into Result          
      from d_kassa_cur b 
      left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
      left join s_art c on b.art = c.art
      left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id in ('ZK23','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and v_numconf = 1))) d on d.docid=discount_type_id
      where  (discount_type_id = v_docid or (get_dk_bag_as_sop(b.art, i_is_seller_dk) = 'T'  and d.docid is null and not (v_numconf = 2 and b.posnr in (select posnr 
																																																																														from d_kassa_disc_cur a
																																																																														inner join pos_skidki1 b on a.discount_type_id = b.docid
																																																																														where trunc (sysdate) between b.dates and b.datee
																																																																														and b.disc_type_id = 'ZK25' and a.kass_id = i_kass_id
																																																																												))))
             and b.kass_id = i_kass_id and b.procent = 0;   
  end if;
  
  -- кол-во баллов потраченных за текущий день
  select nvl(sum(case when bit_vozvr= 'T' then -c.discount_sum else c.discount_sum end),0)
  into v_max_summ
  from pos_sale1 a
  inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_chek = b.id_chek
  inner join pos_sale4 c on b.id_sale = c.id_sale
  where a.bit_close = 'T' 
        and id_dk = i_id_dk 
        and trunc(a.sale_date) = trunc(sysdate)
        and c.disc_type_id = v_docid;
  
  -- лимит баллов за день
  v_day_lim := get_dk_sc_sys_int('DAY_LIMIT');

  if v_day_lim - v_max_summ < Result then
    Result := v_day_lim - v_max_summ;
  end if;  
    

           
  return(Result);
end DK_MAX_SCORES_CHECK_test;
