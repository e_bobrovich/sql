create or replace package calculation_day_tables is

	-- Author  : TYSHEVICHDV
	-- Created : 06.03.2018 9:34:38
	-- Purpose : расчет и заполнение таблиц, которые формируются каждую ночь на 24 часа
	-- Каждая таблица должна заполняться в отдельной процедуре
	-- Каждая процедура должна быть названа именем таблицы, которую она заполняет
	-- Все таблицы заполняемые в данной процедуре должны иметь префикс "t_day_"
	-- все таблицы должны оканчиваться цифрой

	/* Author:  Tyshevich DV Created: 06.03.2018 11:47:06
  вызов этой процедуры будет висеть в ночном задании. в ней должен быть вызов всех других процедур
  */
	procedure go;

	/* Author:  Tyshevich DV Created: 13.03.2018 22:43:19
  процедура , которая по одной запускает другие процедуры в пакете, записывая результаты выполнения
  */
	procedure start_proc_calculate(i_proc       varchar2,
																 i_table_name varchar2);

	/* Author:  Tyshevich DV Created: 06.03.2018 9:35:06
  таблица с продажами магазина. без учета возвратов
  магазин, артикул, сезон, аналог, размер, обувь/сопутка, продано всего, продано за последний год
  */
	procedure t_day_shops_sales1;

	/* Author:  Tyshevich DV Created: 11.03.2018 22:43:02
  таблица с текущими остатками обуви в магазинах , без брака, без уценки
  */
	procedure t_day_shops_ost1;

	/* Author:  Tyshevich DV Created: 11.03.2018 23:25:37
  таблица с товарами в пути
  */
	procedure t_day_trans_in_way1;

	/* Author:  Tyshevich DV Created: 11.03.2018 23:32:37
  таблица с приходами обуви в магазин
  */
	procedure t_day_shops_prixod1;

end calculation_day_tables;
