select * from zbw_pos_md_shedule where id_shop = '7010' and dated = to_char(sysdate-1, 'yyyymmdd');

select id_shop, sum(kol) cnt_visitors 
from zbw_pos_md_shedule 
where to_char(to_date(dated, 'yyyymmdd'), 'yyyymm') = to_char(sysdate, 'yyyymm') 
and case when substr(id_shop,1,2) = '70' then '00'||substr(id_shop,3,2) else id_shop end = '0001' 
group by id_shop;

select a.*, to_date(dated, 'yyyymmdd') from zbw_pos_md_shedule a where id_shop = '7072' and to_char(to_date(dated, 'yyyymmdd'), 'yyyymm') = to_char(sysdate, 'yyyymm') order by dated desc;

select * from pos_sale1;

select  vop, count(*) cnt_sales 
    from bep_prem_month_sales 
    where id_shop = '0001' and sh_sop = 'Обувь' and to_char(dates, 'yyyymmdd') < to_char(sysdate, 'yyyymmdd') 
    group by vop;

select nvl(a.id_shop, b.id_shop) id_shop, 
cast(case when b.cnt_visitors = 0 or b.cnt_visitors is null then 0 else (nvl(a.cnt_sales, 0)/b.cnt_visitors)*100 end as number(18,2)) conversion ,
A.cnt_sales, b.cnt_visitors  FROM 
(
    select id_shop, sum(case when vop in ('БЕЗНАЛ','ПРОДАЖА') then 1 else -1 end) cnt_sales 
    from bep_prem_month_sales 
    where id_shop = '2101' and sh_sop = 'Обувь' and to_char(dates, 'yyyymmdd') < to_char(sysdate, 'yyyymmdd') 
    group by id_shop
) a
inner join
(
    select case when substr(id_shop,1,2) = '70' then '00'||substr(id_shop,3,2)else id_shop end id_shop,
    sum(kol) cnt_visitors 
    from zbw_pos_md_shedule 
    where to_char(to_date(dated, 'yyyymmdd'), 'yyyymm') = to_char(sysdate, 'yyyymm') 
    and dated < to_char(sysdate, 'yyyymmdd') 
    and case when substr(id_shop,1,2) = '70' then '00'||substr(id_shop,3,2) else id_shop end = '2101' 
    group by id_shop
) b
on a.id_shop = b.id_shop
;

select sum(kol) from zbw_pos_mm_sale a 
inner join s_art b on a.art = b.art
where id_shop = '7001' 
and to_char(to_date(sale_date, 'yyyymmdd'), 'yyyymm') = to_char(sysdate, 'yyyymm') 
and sale_date < to_char(sysdate, 'yyyymmdd') 
and b.mtart in ('ZROH','ZHW3');