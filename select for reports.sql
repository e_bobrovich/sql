select a.id_del, b.start_date from delivery_pool a 
inner join tech_data b on a.id_del = b.id_del
inner join delivery_reports d on a.id_del = d.id_del
where a.status = 'DONE'
and d.is_need = 'T'
and trunc(sysdate-3) >= trunc(TO_DATE(b.start_date, 'DD.MM.YYYY HH24:MI:SS'))
;

select count(1) from delivery_history a
where id_del = 2930
--and a.id_imobis is null
;

-------------
select "user", "password", "messageId" from v_reports
where id_del = 2930;

select fl.login as "user", fl.password as "password", dh.id_del||'-'||dh.id_dk as "userMessageId"
from 
delivery_history 
dh
left join (select id_dk, id_shop from firm.st_dk) sd on dh.id_dk = sd.id_dk
left join firm.SPA_FIL_SHOP sfs on sd.id_shop = sfs.shp
left join s_filial_logins fl on sfs.fil = fl.shopid
left join channel_status chs on dh.id_channel = chs.id_channel and dh.id_status = chs.id_status
where dh.id_del = 2930
and chs.final = 0
;
------------
select a.id_del, b.start_date
from delivery_pool a 
inner join tech_data b on a.id_del = b.id_del
where a.status = 'REPORT'
and trunc(sysdate-2) >= trunc(TO_DATE(b.start_date, 'DD.MM.YYYY HH24:MI:SS'))
and trunc(sysdate) <= trunc(TO_DATE(b.start_date, 'DD.MM.YYYY HH24:MI:SS'))+7;

CREATE OR REPLACE FORCE VIEW "IMOB"."V_REPORTS_IS_NEED" ("ID_DEL", "START_DATE") AS 
select a.id_del, b.start_date
from delivery_pool a 
inner join tech_data b on a.id_del = b.id_del
where a.status = 'REPORT'
and trunc(sysdate-2) >= trunc(TO_DATE(b.start_date, 'DD.MM.YYYY HH24:MI:SS'))
and trunc(sysdate) <= trunc(TO_DATE(b.start_date, 'DD.MM.YYYY HH24:MI:SS'))+7;

select c.name_channel, b.name_status, b.description, a.final, a.id_channel, a.id_status from channel_status a
inner join s_status b on a.id_status = b.id_status
inner join s_channel c on a.id_channel = c.id_channel;


select a.id_status,b.description, count(*) 
from delivery_history a
left join s_status b on a.id_status = b.id_status
where a.id_del = 2822 group by a.id_status, b.description order by id_status;

select "user", "password", "messageId" from 
v_reports where "messageId" = '2822-0000000971089';


select count(*) from(select fl.login as "user", fl.password as "password", dh.id_del||'-'||dh.id_dk as "userMessageId", dh.id_del
from delivery_history dh
left join (select id_dk, id_shop from firm.st_dk) sd on dh.id_dk = sd.id_dk
left join firm.SPA_FIL_SHOP sfs on sd.id_shop = sfs.shp
left join s_filial_logins fl on sfs.fil = fl.shopid
left join channel_status chs on dh.id_channel = chs.id_channel and dh.id_status = chs.id_status
where 
fl.login is not null and
fl.password is not null and
chs.final = 0
and
id_del = 2822
) where id_del = 2822
;

select count(1) from v_reports where id_del = 2822;

select a.id_dk, a.id_del, a.id_status, a.id_channel from delivery_history a
inner join (select id_del,id_dk from (select id_del,get_iddk("userMessageId") id_dk from v_reports where id_del = 2822)) b on a.id_dk = b.id_dk and a.id_del = b.id_del
;


select status from delivery_pool where id_del = 2822;

select imob.st_delivery_seq.currval from dual;


CREATE SEQUENCE seq_1;

CREATE SEQUENCE seq_2 INCREMENT BY 2;

SELECT seq_1.NEXTVAL, seq_1.CURRVAL, seq_1.NEXTVAL, seq_2.CURRVAL, seq_2.NEXTVAL, seq_2.NEXTVAL
FROM all_objects
WHERE ROWNUM <= 10;
  
end; 
