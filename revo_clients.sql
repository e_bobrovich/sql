select * from revo_clients;


select id_dk, substr(phone_number, -10) phone_number, activation_date, sale_date  from t_day_st_dk1 where substr(phone_number, -10) in (select phone from revo_clients);

select * from t_day_st_dk1 where '9130492254' = substr(phone_number, -10);

select a.rown, a.phone, a.date_first, a.sum, b.sale_date date_last, b.activation_date from revo_clients a
left join 
(
    select id_dk, substr(phone_number, -10) phone_number, to_char(activation_date, 'mm/dd/yyyy') activation_date, to_char(sale_date, 'mm/dd/yyyy') sale_date  from t_day_st_dk1 where substr(phone_number, -10) in (select phone from revo_clients)
) b on a.phone = b.phone_number
order by rown
;
