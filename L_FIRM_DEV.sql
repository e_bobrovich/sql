select a.*
from pos_dk_scores_firm a
where a.id_dk = '0000021470264'
and a.dates <= trunc(to_timestamp('31.10.17 23:59:59','dd.mm.yy. hh24:mi:ss'), 'ddd') 
order by a.dates
;

select 
*
--id_chek, id_sale, dk_sum, dates, id_shop, id_ac, scores_type 
from pos_dk_scores_firm a
where a.id_dk = '0000021470264'
and a.dk_sum > 0
and a.doc_type = 'ac'
and (a.id_ac = 'SC100001' or a.id_chek = 'SC100001')
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(a.dates, 'ddd')
order by a.dates;

select 
*
--nvl(sum(dk_sum), 0)
from pos_dk_scores_firm a
where a.id_dk = '0000021470264'
and a.dk_sum <= 0
and
(   
    (
        a.doc_type = 'ck'
        --and a.id_ac is null
        and a.scores_type = 'SC100001'
    )
    or
    (
        (a.doc_type = 'voz'  or a.doc_type = 'ckv')
        and (a.id_ac = 'SC100001' or a.scores_type = 'SC100001') 
    )
)  
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(a.dates, 'ddd');

select 
*
--nvl(sum(dk_sum), 0) into v_ac_deductions
from pos_dk_scores_firm a
where a.id_dk = '0000021470264'
and a.dk_sum <= 0
and
(   
        a.doc_type = 'ac' 
        --and a.id_ac is not null
        and a.scores_type = 'stnd'
        and a.id_shop = 'FIRM'
        and a.id_chek = 'DK100005'
        and a.id_ac != 'TEST0009' -- учесть "акцию" списание через год
        and a.id_sale = 2702
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(a.dates, 'ddd');

select nvl(to_char(trunc(tdv_dk.get_scores_burn_date(
                    to_timestamp('08.04.17 17:52:03', 'dd.mm.yy hh24:mi:ss'), 
                    a.burn_date, a.burn_period, a.burn_count)), 'dd.mm.yy'),  ' ')
from pos_dk_stock1 a
where a.id = 'DK100005';

select a.*, b.*
--nvl(to_char(trunc(tdv_dk.get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count)), 'dd.mm.yy'),  ' ')
from pos_dk_scores_firm a
left join POS_DK_STOCK1 b on nvl(a.id_ac, a.id_chek) = b.id and a.dk_sum > 0
where a.id_dk = '0000021470264'
--and b.id = case when b.id is null then null else b.id end
and a.id_chek = 'SC100012'
and a.id_sale = 18366
and a.id_shop = 2702
and a.dk_sum > 0
and to_timestamp('08.04.17 17:52:03', 'dd.mm.yy hh24:mi:ss') = a.dates; 