TRUNCATE TABLE imob.t_day_st_dk1;

delete from imob.t_day_st_dk1;

insert into imob.t_day_st_dk1
select distinct 
a."ID_DK",
a."FIO", 
a."FAM",
--a."IMA",--
nvl(n.name, 'Уважаемый покупатель') "IMA",   --imob.get_russian_name(a.ima) "IMA", 
a."OTCH",
a."BIRTHDAY",
a."SEX", 
replace(replace(replace(a."PHONE_NUMBER", ' '),'-'),'+') "PHONE_NUMBER", --imob.get_clean_phone(a."PHONE_NUMBER") PHONE_NUMBER , 
a."EMAIL",
a."ID_SHOP", 
a."ACTIVATION_DATE", 
a."DK_TYPE",
a."DKVID_ID",
a."DK_LEVEL",
a."SUMM",
a."DISCOUNT",
a."SCORES_DATE",
a."SCORES",
a."EDIT_DATE",
a."DK_COUNTRY",
a."CITY",
a."STREET",
imob.get_last_sale_date_new(a.id_dk) "SALE_DATE",
b.sms_status "SMS_STATUS",
c.viber_status "VIBER_STATUS",
--imob.get_dk_error(a.id_dk) "DOP_INFO"--
' ' "DOP_INFO" --
from firm.st_dk a
left join imob.russian_names n on upper(rtrim(a.ima)) = upper(rtrim(n.name))
left join 
(
    select q.id_dk, w.description "SMS_STATUS" from   
    (
        select id_dk, 
        substr(max(to_char(date_sent, 'yyyy.mm.dd hh24:mi:ss') || '!' || id_status), instr(max(to_char(date_sent, 'yyyy.mm.dd hh24:mi:ss') || '!' || id_status), '!') + length('!')) as "ID_STATUS"
        from imob.delivery_history
        where id_channel = '1' and id_del > 5
        group by id_dk
    ) q
    inner join imob.s_status w on q.id_status = w.id_status 
) b on a.id_dk = b.id_dk
left join 
(
    select q.id_dk, w.description "VIBER_STATUS" from   
    (
        select id_dk, 
        substr(max(to_char(date_sent, 'yyyy.mm.dd hh24:mi:ss') || '!' || id_status), instr(max(to_char(date_sent, 'yyyy.mm.dd hh24:mi:ss') || '!' || id_status), '!') + length('!')) as "ID_STATUS"
        from imob.delivery_history
        where id_channel = '2' and id_del > 5
        group by id_dk
    ) q
    inner join imob.s_status w on q.id_status = w.id_status 
) c on a.id_dk = c.id_dk

where a.bit_block = 'F'
and id_shop is not null
;