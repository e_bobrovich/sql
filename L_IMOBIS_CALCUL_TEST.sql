merge into imob.pos_sale_art_delivery x
using (
    select 
    a."ID_DK",
    a."ID_CHEK",
    a."SHOP_ID",
    b."ID_SALE",
    b."ART",
    c."ASSORT",
    c."SEASON",
    b."CENA3",
    a."SALE_DATE",
    a."SALE_SUM"
    from firm.pos_sale1 a
    left join firm.pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
    left join firm.s_art c on b.art = c.art
    where a.id_dk is not null and a.id_dk != ' ' and a.bit_close = 'T' and a.bit_vozvr = 'F'
) y 
on (x.id_dk = y.id_dk and x.id_chek = y.id_chek and x.shop_id = y.shop_id and x.id_sale = y.id_sale)
when matched then
    update set
        x.art = y.art,
        x.assort = y.assort,
        x.season = y.season,
        x.cena3 = y.cena3,
        x.sale_date = y.sale_date,
        x.sale_sum = y.sale_sum
when not matched then 
    insert (x.ID_DK, x.ID_CHEK, x.SHOP_ID, x.ID_SALE, x.ART, x.ASSORT, x.SEASON, x.CENA3, x.SALE_DATE, x.SALE_SUM)
    values (y.ID_DK, y.ID_CHEK, y.SHOP_ID, y.ID_SALE, y.ART, y.ASSORT, y.SEASON, y.CENA3, y.SALE_DATE, y.SALE_SUM)
;


--------------------------------------------------------------------------------
--- CALCULATE ST_DK
--------------------------------------------------------------------------------
merge into imob.st_dk_delivery x
using ( 
    select distinct 
    a."ID_DK",
    a."FIO", 
    a."FAM",
    a."IMA",--nvl(n.name, 'Уважаемый покупатель') "IMA",   --imob.get_russian_name(a.ima) "IMA", 
    a."OTCH",
    a."BIRTHDAY",
    a."SEX", 
    replace(replace(a."PHONE_NUMBER", ' '),'-') "PHONE_NUMBER", --imob.get_clean_phone(a."PHONE_NUMBER") PHONE_NUMBER , 
    a."EMAIL",
    a."ID_SHOP", 
    a."ACTIVATION_DATE", 
    a."DK_TYPE",
    a."DKVID_ID",
    a."DK_LEVEL",
    a."SUMM",
    a."DISCOUNT",
    a."SCORES_DATE",
    a."SCORES",
    a."EDIT_DATE",
    a."DK_COUNTRY",
    a."CITY",
    a."STREET",
    imob.get_last_sale_date_new(a.id_dk) "SALE_DATE",
    b.sms_status "SMS_STATUS",
    c.viber_status "VIBER_STATUS",
    --    upper( imob.get_last_sms_status(a.id_dk, 'description')) "SMS_STATUS",
    --    upper( imob.get_last_viber_status(a.id_dk, 'description')) "VIBER_STATUS",
    ' ' "DOP_INFO" --imob.get_dk_error(a.id_dk) "DOP_INFO"
    from firm.st_dk a
    --left join firm.st_russian_names n on upper(rtrim(a.ima)) = upper(rtrim(n.name))
    left join 
    (
        select q.id_dk, w.description "SMS_STATUS"
        from delivery_history q
        inner join s_status w on q.id_status = w.id_status
        where (q.id_dk,q.date_sent) in ( 
            select q1.id_dk, max(q1.date_sent) from delivery_history q1
            where q1.id_channel = 2
            and q1.id_del > 5
            group by q1.id_dk
        )    
    ) b on a.id_dk = b.id_dk
    left join 
    (
        select q.id_dk, w.description "VIBER_STATUS"
        from delivery_history q
        inner join s_status w on q.id_status = w.id_status
        where (id_dk,date_sent) in ( 
            select q1.id_dk, max(q1.date_sent) from delivery_history q1
            where q1.id_channel = 1
            and q1.id_del > 5
            group by q1.id_dk
        )    
    ) c on a.id_dk = c.id_dk
    
    where a.bit_block = 'F'
    and id_shop is not null
--    and rownum <= 10
) y
on (x.id_dk = y.id_dk)
when matched then
    update set
        -- fio, fam, ima, otch    
        x.birthday = y.birthday,
        x.phone_number =  y.phone_number,
        x.email = y.email,
        x.dk_type = y.dk_type,
        x.dkvid_id = y.dkvid_id,
        x.dk_level = y.dk_level,
        x.summ = y.summ,
        x.discount = y.discount,
        x.scores_date = y.scores_date,
        x.scores = y.scores,
        x.edit_date = y.edit_date,
        x.city = y.city,
        x.street = y.street,
        x.sms_status =  y.sms_status,
        x.viber_status = y.viber_Status--,
        --x.dop_info = y.dop_info
when not matched then 
    insert (x.ID_DK, x.FIO, x.FAM, x.IMA, x.OTCH, x.BIRTHDAY, x.SEX, x.PHONE_NUMBER, x.ID_SHOP, x.ACTIVATION_DATE,
            x.DK_TYPE, x.DKVID_ID, x.DK_LEVEL, x.SUMM, x.DISCOUNT, x.SCORES_DATE, x.SCORES, x.EDIT_DATE, x.DK_COUNTRY, 
            x.CITY, x.SALE_DATE, x.SMS_STATUS, x.VIBER_STATUS, x.DOP_INFO)
    values (y.ID_DK, y.FIO, y.FAM, y.IMA,/*imob.get_russian_name(y.IMA),*/ y.OTCH, y.BIRTHDAY, y.SEX, y.PHONE_NUMBER, y.ID_SHOP, y.ACTIVATION_DATE,
            y.DK_TYPE, y.DKVID_ID, y.DK_LEVEL, y.SUMM, y.DISCOUNT, y.SCORES_DATE, y.SCORES, y.EDIT_DATE, y.DK_COUNTRY, 
            y.CITY, y.SALE_DATE, y.SMS_STATUS, y.VIBER_STATUS, y.DOP_INFO)
;



select id_dk, count(*) from t_day_st_dk1 group by id_dk having count(*) > 1;

select id_dk, 
            substr(max(to_char(date_sent, 'yyyy.mm.dd hh24:mi:ss') || '!' || id_status), instr(max(to_char(date_sent, 'yyyy.mm.dd hh24:mi:ss') || '!' || id_status), '!') + length('!')) as "ID_STATUS"
            from delivery_history
            where id_channel = '2' and id_del > 5
            and id_dk = '0000013311544'
            
            group by id_dk;

CREATE INDEX ST_DK_DELIVERY_ID_SHOP ON ST_DK_DELIVERY (ID_SHOP ASC);

CREATE INDEX ST_DK_DELIVERY_SALE_DATE ON ST_DK_DELIVERY (SALE_DATE ASC);

CREATE INDEX ST_DK_DELIVERY_SMS_STATUS ON ST_DK_DELIVERY (SMS_STATUS ASC);

CREATE INDEX ST_DK_DELIVERY_VIBER_STATUS ON ST_DK_DELIVERY (VIBER_STATUS ASC);