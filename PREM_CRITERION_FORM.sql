select a.id_shop, a.year, a.month, a.id_combination, c.description, b.value , a.percent from bep_prem_combination_percent a
left join bep_prem_combination_criterion b on a.id_combination = b.id_combination
left join bep_prem_group_criterion c on b.id_criterion = c.id_criterion
where year = 2018 and month = 11
order by id_shop, id_combination
;


select a.id_shop, a.year, a.month, a.id_combination, c.description, b.value , a.percent from bep_prem_combination_percent a
left join bep_prem_combination_criterion b on a.id_combination = b.id_combination
left join bep_prem_group_criterion c on b.id_criterion = c.id_criterion
where year = 2018 and month = 11
order by id_shop, id_combination
;
-- ТАБЛИЦА КОМБИНАЦИИ
select cc.id_combination, gc.description, cc.value from bep_prem_combination_criterion cc
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
where cc.id_combination = 1 and cc.id_combination != 0 and cc.id_group = 1
;

-- ОСНОВНАЯ ТАБЛИЦА КОМБИНАЦИЙ ДЛЯ ГРУППЫ
-- ПРИ СОЗДАНИИ ГРУППЫ АВТОМАТОМ СОЗДАВАТЬ КОМБИНАЦИЮ 0 с процентом 0
select sg.id_group, cp.year, cp.month, cp.id_combination, case when cp.id_combination = 0 then 'Остальное' else listagg(cc.value,'/') within group(order by cc.id_criterion) end combination, cp.percent
from bep_prem_s_group sg
left join bep_prem_combination_percent cp on sg.id_group = cp.id_group
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion 
where
sg.id_group = 1
--year = coalesce(2018, extract(year from sysdate)) and month = coalesce(11, extract(month from sysdate))
group by sg.id_group, cp.year, cp.month, cp.id_combination, cp.percent
order by cp.id_combination;

select sg.id_group, p.id_combination, case when cp.id_combination = 0 then 'Остальное' else listagg(cc.value,'/') within group(order by cc.id_criterion) end combination, cp.percent
from bep_prem_s_group sg
inner join bep_prem_combination_percent cp on sg.id_group = cp.id_group
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion 
where sg.id_group = 2
group by sg.id_group, cp.id_combination, cp.percent;


--ТАБЛИЦА ГРУПП МАГАЗИНОВ
select sg.id_group, sg.year, sg.month, sg.dateb, sg.datee, case when dateb is null and datee is null then listagg(gs.id_shop,', ') within group(order by gs.id_shop) else 'Приказ по всей сети' end shop_list
from bep_prem_s_group sg
left join bep_prem_group_shop gs on sg.id_group = gs.id_group
where
sg.year = coalesce(2018, extract(year from sysdate)) and sg.month = coalesce(12, extract(month from sysdate))
group by sg.id_group, sg.year, sg.month, sg.dateb, sg.datee
;

--ВЫБОРКА КРИТЕРИЕВ
select * from bep_prem_s_criterion where id_criterion != 0 order by id_criterion;
--ВЫБОРКА ПАРАМЕТРОВ КРИТЕРИЯ

