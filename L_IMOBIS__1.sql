select 
b.id_shop shop_codes, c.id_shop shop_dk
--a.id_Del, a.id_dk, a.id_imobis,  b.id_dk, b.id_pos, b.id_shop 
from delivery_history a 
left join pos_dk_codes b on a.id_dk = b.id_pos
left join (select id_dk, id_shop from firm.st_dk) c on case when b.id_shop is null then a.id_dk else null end = c.id_dk
where a.id_imobis = '303735306'
;
--322794658
--303735306

--0-0000000386329-1522754993
--2704-0000026611273

select 
    get_auth_from_idimob('322794658'), get_auth_from_idimob('303735306'), 
    get_auth_from_idmess('0-0000000386329-1522754993'), get_auth_from_idmess('2704-0000026611273') 
from dual;

select 
    get_auth_from_idimob('322794658')
from dual;

select 
    get_auth_from_idimob('303735306')
from dual;

select  
    get_auth_from_idmess('0-0000000386329-1522754993')
from dual;

select 
    get_auth_from_idmess('2704-0000026611273') 
from dual;

select id_dk, id_del from delivery_history where id_del||'-'||id_dk = '2704-0000026611273';

select id_dk, id_del from delivery_history 
where id_del = get_iddel('0-0000000386329-1522754993') 
and id_dk = case when id_del = 0 then get_idpos('0-0000000386329-1522754993') else get_iddk('0-0000000386329-1522754993') end;

SET SERVEROUTPUT ON;

declare
    i_id_message varchar2(50) := '0-0000000386329-1522754993';
    v_id_dk varchar(50);
    v_id_shop varchar(20);
    v_id_del number(10);
    v_result varchar2(200);
begin
    select id_dk, id_del into v_id_dk, v_id_del from delivery_history 
    where id_del = get_iddel(i_id_message) 
    and id_dk = case when id_del = 0 then get_idpos(i_id_message) else get_iddk(i_id_message) end;
    dbms_output.put_line('id_del : '||v_id_del||' id_dk : '||v_id_dk);
    begin
        if (v_id_del = 0) 
        then
            select id_shop into v_id_shop from pos_dk_codes where id_pos = v_id_dk;
        else
            select id_shop into v_id_shop from firm.st_dk where id_dk = v_id_dk;
        end if;
        dbms_output.put_line('id_shop : '||v_id_shop);
        
    exception when no_data_found then
        select id_shop into v_id_shop from firm.st_dk where id_dk = v_id_dk;--get_id_dk  when id_del = 0??
        dbms_output.put_line('exception id_shop : '||v_id_shop);
    end ;
    
begin 
    select a.login||'#'||a.password into v_result 
    from s_filial_logins a
    left join firm.SPA_FIL_SHOP b on a.shopid = b.fil
    where b.shp = v_id_shop;
exception when no_data_found  then
    v_result := '#';
end;
    dbms_output.put_line('v_result : '||v_result);
end;