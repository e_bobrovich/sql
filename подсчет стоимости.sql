select login, 
cast(avgpricelogin as number(18,2)) as avgpricelogin, 
koldk, 
cast(aboutprice as number(18,2)) as aboutprice, 
cast(balance as number(18,2)) as balance, 
last_update, 
case when aboutprice>balance then '1' else ' ' end as flag from v_about_balance
where id_del=3682
union all 
select 'ВСЕГО' as login, 
cast(avg(avgpricelogin) as number(18,2)) as avgpricelogin, 
cast(sum(koldk) as number(18,2)) as koldk, 
cast(sum(aboutprice) as number(18,2)) as aboutprice, 
cast(sum(balance) as number(18,2)) as balance,
sysdate as last_update, ' ' as flag
from v_about_balance
where id_del = 3682;


select dd.id_del, a.login, round(avg(a.avgprice),2) as avgpricelogin, count(dd.id_dk) as koldk, round(avg(a.avgprice)*count(dd.id_dk),2) as aboutprice, a.balance, a.last_update
from
(
    select sfl.shopid, sfl.login,
    --nvl(round(avg(to_number(dh.price, '9.99')),2),1.2) as avgprice,
    nvl(round(avg(to_number(dh.price,'9.999')),2), 1.5) as avgprice,
    sfl.balance, sfl.last_update
    from s_filial_logins sfl
    left join firm.st_retail_hierarchy srh on sfl.shopid = srh.shopparent
    left join firm.st_dk sd on sd.id_shop = srh.shopid
    left join delivery_history dh on sd.id_dk=dh.id_dk
    where dh.price is not null or dh.price != 0.0
    group by sfl.login , sfl.balance, sfl.last_update, sfl.shopid
) a
left join firm.st_retail_hierarchy srh on a.shopid = srh.shopparent
left join firm.st_dk sd on sd.id_shop = srh.shopid
inner join delivery_dk dd on sd.id_dk=dd.id_dk
where (dd.groupt != 'K' or dd.groupt is null)
group by a.login, a.balance, a.last_update, dd.id_del;


select sfl.shopid, sfl.login,
nvl(round(avg(to_number(dh.price,'9.999')),2), 1.5) as avgprice,
sfl.balance, sfl.last_update
from s_filial_logins sfl
left join firm.st_retail_hierarchy srh on sfl.shopid = srh.shopparent
left join firm.st_dk sd on sd.id_shop = srh.shopid
left join delivery_history dh on sd.id_dk=dh.id_dk

group by sfl.login , sfl.balance, sfl.last_update, sfl.shopid;

select fl.shopid, fl.login, 
    nvl(round(avg(to_number(dh.price,'9.999')),2), 1.5) as avgprice,
    fl.balance, fl.last_update
from delivery_dk dd
left join t_day_st_dk1 dk on dd.id_dk = dk.id_dk
left join FIRM.spa_fil_shop sfs on dk.id_shop = sfs.shp
left join s_filial_logins fl on sfs.fil = fl.shopid
left join delivery_history dh on dd.id_dk = dh.id_dk

where 
--dd.id_del = 3682 and
(dh.price is not null or dh.price != 0.0)
group by fl.shopid, fl.login, fl.balance, fl.last_update
;

------------------------------------------
-- СРЕДНЯЯ СТОИМОСТЬ СООБЩЕНИЯ ПО ФИЛИАЛАМ
------------------------------------------
delete from avg_price_messages;
--insert into avg_price_messages
select fl.shopid, fl.login, 
    nvl(round(avg(to_number(dh.price,'9.999')),2), 1.5) as avgprice,
    sysdate last_calculate
    
    from delivery_history dh
left join t_day_st_dk1 dk on dh.id_dk = dk.id_dk
left join FIRM.spa_fil_shop sfs on dk.id_shop = sfs.shp
left join s_filial_logins fl on sfs.fil = fl.shopid
where 
(dh.price is not null or dh.price != 0.0 or fl.shopid = '00F1')
and 
dh.id_del not in (0,1,2,3)
and 
dk.id_shop != ' '
group by fl.shopid, fl.login
order by fl.shopid, fl.login
;


delete from avg_price_messages;
insert into avg_price_messages
select fl.shopid, fl.login, 
    nvl(round(avg(to_number(dh.price,'9.999')),2), 1.5) as avgprice,
    sysdate last_calculate
    from s_filial_logins fl
left join FIRM.spa_fil_shop sfs on fl.shopid = sfs.fil    
left join t_day_st_dk1 dk on sfs.shp  = dk.id_shop
left join delivery_history dh on dk.id_dk = dh.id_dk      
where 
(dh.price is not null or dh.price != 0.0 or fl.shopid = '00F1')
--and  dh.id_del not in (0,1,2,3)
and 
dk.id_shop != ' '
group by fl.shopid, fl.login
order by fl.shopid, fl.login
;

select * from avg_price_messages;

call CALCULATE_AVG_PRICE_MESSAGES();

select
login, 
cast(avgpricelogin as number(18,2)) as avgpricelogin, 
koldk, 
cast(aboutprice as number(18,2)) as aboutprice, 
cast(balance as number(18,2)) as balance, 
last_update, 
case when aboutprice>balance then '1' else ' ' end as flag 
from
(    
    select fl.login, round(avg(apm.avg_price),2) as avgpricelogin, count(dd.id_dk) as koldk, round(avg(apm.avg_price)*count(dd.id_dk),2) as aboutprice, fl.balance, fl.last_update
    from s_filial_logins fl
    left join avg_price_messages apm on fl.shopid = apm.shopid and fl.login = apm.login
    left join FIRM.spa_fil_shop sfs on fl.shopid = sfs.fil
    left join t_day_st_dk1 dk on sfs.shp  = dk.id_shop
    left join delivery_dk dd on dk.id_dk = dd.id_dk
    where  (dd.groupt != 'K' or dd.groupt is null)
    and id_del = 3384
    group by dd.id_del, fl.login, fl.balance, fl.last_update
    order by login
)
union all 
select 'ВСЕГО' as login, 
cast(avg(avgpricelogin) as number(18,2)) as avgpricelogin, 
cast(sum(koldk) as number(18,2)) as koldk, 
cast(sum(aboutprice) as number(18,2)) as aboutprice, 
cast(sum(balance) as number(18,2)) as balance,
sysdate as last_update, ' ' as flag
from
(    
    select fl.login, round(avg(apm.avg_price),2) as avgpricelogin, count(dd.id_dk) as koldk, round(avg(apm.avg_price)*count(dd.id_dk),2) as aboutprice, fl.balance, fl.last_update
    from s_filial_logins fl
    left join avg_price_messages apm on fl.shopid = apm.shopid and fl.login = apm.login
    left join FIRM.spa_fil_shop sfs on fl.shopid = sfs.fil
    left join t_day_st_dk1 dk on sfs.shp  = dk.id_shop
    left join delivery_dk dd on dk.id_dk = dd.id_dk
    where  (dd.groupt != 'K' or dd.groupt is null)
    and id_del = 3384
    group by dd.id_del, fl.login, fl.balance, fl.last_update
)
;

