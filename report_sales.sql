 SELECT a.fil, TO_CHAR(TO_DATE(a.month, 'MM'), 'MONTH') month, SUM(a.kol_sale_shoes) kol_sale_shoes, SUM(a.kol_sale_bags) kol_sale_bags, SUM(a.kol_sale_others) kol_sale_others, SUM(a.kol_return_shoes) kol_return_shoes, SUM(a.kol_return_bags) kol_return_bags, SUM(a.kol_return_others) kol_return_others
FROM (
    --обувь
    SELECT fil.FIL fil,  to_char(s1.SALE_DATE, 'MM') month, count(s2.ART) kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, 0 kol_return_shoes, 0 kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON dk.ID_DK = s1.ID_DK
    INNER JOIN POS_SALE2 s2 ON s1.ID_CHEK = s2.ID_CHEK AND s1.ID_SHOP = s2.ID_SHOP
    INNER JOIN S_ART art ON s2.ART = art.ART
 
 INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (s1.SALE_DATE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND s1.BIT_CLOSE = 'T' AND s1.BIT_VOZVR = 'F' AND art.PRODH LIKE '0001%'
    GROUP BY fil.FIL, to_char(s1.SALE_DATE, 'MM')
    
    UNION ALL
    
    --сумки
    SELECT fil.FIL fil,  to_char(s1.SALE_DATE, 'MM') month, 0 kol_sale_shoes, count(s2.ART) kol_sale_bags, 0 kol_sale_others, 0 kol_return_shoes, 0 kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON dk.ID_DK = s1.ID_DK
    INNER JOIN POS_SALE2 s2 ON s1.ID_CHEK = s2.ID_CHEK AND s1.ID_SHOP = s2.ID_SHOP
    INNER JOIN S_ART art ON s2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (s1.SALE_DATE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND s1.BIT_CLOSE = 'T' AND s1.BIT_VOZVR = 'F' AND art.PRODH LIKE '00020038%'
    GROUP BY fil.FIL, to_char(s1.SALE_DATE, 'MM')
    
    UNION ALL
    
    --остальное
    SELECT fil.FIL fil, to_char(s1.SALE_DATE, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, count(s2.ART) kol_sale_others, 0 kol_return_shoes, 0 kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON dk.ID_DK = s1.ID_DK
    INNER JOIN POS_SALE2 s2 ON s1.ID_CHEK = s2.ID_CHEK AND s1.ID_SHOP = s2.ID_SHOP
    INNER JOIN S_ART art ON s2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (s1.SALE_DATE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND s1.BIT_CLOSE = 'T' AND s1.BIT_VOZVR = 'F' AND art.PRODH NOT LIKE '0001%' AND art.PRODH NOT LIKE '00020038%'
    GROUP BY fil.FIL, to_char(s1.SALE_DATE, 'MM')
    
    UNION ALL
    
    SELECT fil.FIL fil, to_char(s1.SALE_DATE, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, SUM(s2.KOL) kol_return_shoes, 0 kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON dk.ID_DK = s1.ID_DK
    INNER JOIN POS_SALE2 s2 ON s1.ID_CHEK = s2.ID_CHEK AND s1.ID_SHOP = s2.ID_SHOP
    INNER JOIN S_ART art ON s2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (s1.SALE_DATE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND s1.BIT_CLOSE = 'T' AND s1.BIT_VOZVR = 'T' AND art.PRODH LIKE '0001%'
    GROUP BY fil.FIL, to_char(s1.SALE_DATE, 'MM')
    UNION ALL
    SELECT fil.FIL fil, to_char(p1.DATED, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, SUM(p2.KOL) kol_return_shoes, 0 kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON s1.ID_DK = dk.ID_DK
    INNER JOIN D_PRIXOD1 p1 ON s1.ID_CHEK = p1.NDOC AND s1.ID_SHOP = p1.ID_SHOP
    INNER JOIN D_PRIXOD2 p2 ON p1.ID = p2.ID AND p1.ID_SHOP = p2.ID_SHOP
    INNER JOIN S_ART art ON p2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (p1.DATED BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND p1.BIT_CLOSE = 'T' AND art.PRODH LIKE '0001%' AND p1.IDOP = '03'
    GROUP BY fil.FIL, to_char(p1.DATED, 'MM')
    
    UNION ALL
    
    --возврат сумок
    SELECT fil.FIL fil, to_char(s1.SALE_DATE, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, 0 kol_return_shoes, SUM(s2.KOL) kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON dk.ID_DK = s1.ID_DK
    INNER JOIN POS_SALE2 s2 ON s1.ID_CHEK = s2.ID_CHEK AND s1.ID_SHOP = s2.ID_SHOP
    INNER JOIN S_ART art ON s2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (s1.SALE_DATE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND s1.BIT_CLOSE = 'T' AND s1.BIT_VOZVR = 'T' AND art.PRODH LIKE '00020038%'
    GROUP BY fil.FIL, to_char(s1.SALE_DATE, 'MM')
    UNION ALL
    SELECT fil.FIL fil, to_char(p1.DATED, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, 0 kol_return_shoes, sum(p2.KOL) kol_return_bags, 0 kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON s1.ID_DK = dk.ID_DK
    INNER JOIN D_PRIXOD1 p1 ON s1.ID_CHEK = p1.NDOC AND s1.ID_SHOP = p1.ID_SHOP
    INNER JOIN D_PRIXOD2 p2 ON p1.ID = p2.ID AND p1.ID_SHOP = p2.ID_SHOP
    INNER JOIN S_ART art ON p2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (p1.DATED BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND p1.BIT_CLOSE = 'T' AND art.PRODH LIKE '00020038%' AND p1.IDOP = '03'
    GROUP BY fil.FIL, to_char(p1.DATED, 'MM')
    
    UNION ALL
    
    --возврат остольного
    SELECT fil.FIL fil, to_char(s1.SALE_DATE, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, 0 kol_return_shoes, 0 kol_return_bags, SUM(s2.KOL) kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON dk.ID_DK = s1.ID_DK
    INNER JOIN POS_SALE2 s2 ON s1.ID_CHEK = s2.ID_CHEK AND s1.ID_SHOP = s2.ID_SHOP
    INNER JOIN S_ART art ON s2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (s1.SALE_DATE BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND s1.BIT_CLOSE = 'T' AND s1.BIT_VOZVR = 'T' AND art.PRODH NOT LIKE '0001%' AND art.PRODH NOT LIKE '00020038%'
    GROUP BY fil.FIL, to_char(s1.SALE_DATE, 'MM')
    UNION ALL
    SELECT fil.FIL fil, to_char(p1.DATED, 'MM') month, 0 kol_sale_shoes, 0 kol_sale_bags, 0 kol_sale_others, 0 kol_return_shoes, 0 kol_return_bags, SUM(p2.KOL) kol_return_others
    FROM ST_DK dk
    INNER JOIN POS_SALE1 s1 ON s1.ID_DK = dk.ID_DK
    INNER JOIN D_PRIXOD1 p1 ON s1.ID_CHEK = p1.NDOC AND s1.ID_SHOP = p1.ID_SHOP
    INNER JOIN D_PRIXOD2 p2 ON p1.ID = p2.ID AND p1.ID_SHOP = p2.ID_SHOP
    INNER JOIN S_ART art ON p2.ART = art.ART
    INNER JOIN (
        SELECT fil.SHOPID AS FIL, shp.SHOPID AS SHP
        FROM ST_SHOP fil
        INNER JOIN S_SHOP shp ON SUBSTR(shp.SHOPID, 1, 2) = SUBSTR(fil.SHOPID, 1 , 2) AND (SUBSTR(shp.SHOPNUM, 4, 1) = SUBSTR(fil.SHOPID, 4, 1) OR fil.SHOPID = '00F1')
        INNER JOIN ST_SHOP ON ST_SHOP.SHOPID = shp.SHOPID AND ST_SHOP.ORG_KOD LIKE 'SHP'
        WHERE fil.ORG_KOD LIKE 'FIL' AND SUBSTR (fil.SHOPID, 3, 1) = 'F' AND REGEXP_LIKE(shp.SHOPID, '^\d(\d|U)\d{2,2}$')) fil
    ON fil.SHP =  s1.ID_SHOP
    WHERE dk.DKVID_ID = '009' AND (p1.DATED BETWEEN TO_DATE('01.01.2017', 'dd.mm.yyyy') AND TO_DATE('31.12.2017', 'dd.mm.yyyy')) AND p1.BIT_CLOSE = 'T' AND art.PRODH NOT LIKE '0001%' AND art.PRODH NOT LIKE '00020038%' AND p1.IDOP = '03'
    GROUP BY fil.FIL, to_char(p1.DATED, 'MM')) a
GROUP BY a.fil, a.month
ORDER BY a.fil, TO_NUMBER(a.month);
