select count(1) from t_day_st_dk1 where id_shop is null or id_shop = ' ';
select count(1) from firm.st_dk where id_shop = ' ';
select count(1) from t_day_st_dk1 where id_shop is null or id_shop != ' ' and id_dk in (select id_dk from firm.st_dk where id_shop = ' ');

select count(*) from (
    select a.id_dk, a.id_shop, b.dates from
    (
        select a1.id_dk, b1.id_shop, b1.dates from t_day_st_dk1 a1 
        inner join firm.pos_dk_scores b1 on a1.id_dk = b1.id_dk
    ) a
    inner join (
        select a2.id_dk, max(b2.dates) dates from t_day_st_dk1 a2 
        inner join firm.pos_dk_scores b2 on a2.id_dk = b2.id_dk
        where a2.id_shop = ' ' and b2.id_shop != 'FIRM'
        group by a2.id_dk
    ) b on a.id_dk = b.id_dk and a.dates = b.dates
)
;
select id_dk,count(id_dk) from
(
    select a.id_dk, max(b.dates) from t_day_st_dk1 a 
    inner join firm.pos_dk_scores b on a.id_dk = b.id_dk
    where a.id_shop = ' '
    group by a.id_dk
    )
group by id_dk having count(1) > 1;


select * from firm.st_dk where id_dk = '0000008164834';