-- логи
select * from ri_stored_proc_error order by er_time desc;

select * from delivery_history where id_del >= 8642 order by date_sent desc;

-- пул рассылок с датой начала
select * from v_pool_with_date where id_del >= 8642 order by start_date desc;

-- текущее количество отправленных сообщений, планируемое количество, процент завершения по каждой рассылке из заданных
-- общеие данные по всем заданным рассылкам
select a.id_del "id_del",nvl(b.result, 0) result, a.plan, nvl(round((b.result/a.plan)*100, 2), 0) progress from (
    select a.id_del, nvl(count(id_dk), 0) plan from delivery_dk a inner join tech_data b on a.id_del = b.id_del 
    where a.id_del >=8642 /*отслеживаемые рассылки*/ and groupt = 'O' group by a.id_del
) a
left join (
    select id_del , nvl(count(id_dk), 0) result from delivery_history 
    where id_del >= 8642 /*отслеживаемые рассылки*/ group by id_del
) b on a.id_del = b.id_del
where a.id_del not in (select id_del from st_delivery where flag_delete = 1)
union all

select 99999 as "id_del",nvl(d.result, 0), c.plan, nvl(round((d.result/c.plan)*100, 2), 0) progress from (
    select 99999 as "id_del", nvl(count(id_dk), 0) plan from delivery_dk a inner join tech_data b on a.id_del = b.id_del 
    where a.id_del >= 8642 /*отслеживаемые рассылки*/ and groupt = 'O'
    and a.id_del not in (select id_del from st_delivery where flag_delete = 1)
) c
left join (
    select 99999 as "id_del",  nvl(count(id_dk), 0) result from delivery_history 
    where id_del >= 8642 /*отслеживаемые рассылки*/
    and id_del not in (select id_del from st_delivery where flag_delete = 1)
) d on c."id_del" = d."id_del"
order by 1
;

-- количество полученных статусов, их процент, количество не полученных статусов, их процент, количество сообщений по каждой рассылке из заданных
select a.id_del, available,round(available/(available+nvl(no_available, 0)),2)*100||'%' "1%",
'  ',
nvl(no_available, 0) as no_available,   round(nvl(no_available, 0)/(available+nvl(no_available, 0)),2)*100||'%' "2%" ,
available+no_available "all"
from 
(
    select id_del, count(*) as available from delivery_history 
    where id_del >= 8642/*отслеживаемые рассылки*/ and id_status not in (21,22) group by id_del order by id_del
) a
left join 
(
    select id_del, count(*) as no_available from delivery_history 
    where id_del >= 8642/*отслеживаемые рассылки*/ and id_status in (21,22) group by id_del order by id_del
) b on a.id_del = b.id_del
order by id_del;

-- статусы и их количество по всем выбранным рассылкам
select a.id_status,b.description, count(*) 
from delivery_history a
left join s_status b on a.id_status = b.id_status
where a.id_del >= 8642/*отслеживаемые рассылки*/ group by a.id_status, b.description order by id_status;


--------------------------------------------------------------------------------
/*
    новая версия работает без v_st_dk. 
    теперь нужные данные по картам и продажам считаюся ночью в таблицы 
    T_DAY_ST_DK1 и T_DAY_POS_SALE_ART1 соответственно и с ними выполняются 
    все действия.
*/
--------------------------------------------------------------------------------


-- добавление рассылки в пул с заданным статусом
call add_delivery_pool(ID_DEL, 'NEW');

-- отложить рассылку, если она еще не начата. в пуле рассыке выставляется статус DELAY
call delay_delivery(ID_DEL);

-- изменение стауса расслыки в пуле
call set_delivery_pool_status(ID_DEL, 'NEW');

-- очистска списка рассылки от ЧС
call clear_deliv_black_list (ID_DEL);

-- очистска списка рассылки от дублей
call clear_double_phones (ID_DEL);

-- выставить процент контрольной группы для рассылки в st_delivery
call set_perc_conrol(ID_DEL, PERCENT);

-- проставить получателям контрольную группу в delivery_dk 
-- id рассылки, количество получателей, процент
call SET_CONTROL_GROUP_DK_2(ID_DEL, COUNT_ALL_RECIPIENTS, PERCENT);

-- выполнить новое формирование списка для рассылки(список хранится только в delivery_dk)
-- id рассылки, 'F' - с заполнением списка, 'T' - только формирование запроса вставки
call use_filters_new (ID_DEL, 'T');

