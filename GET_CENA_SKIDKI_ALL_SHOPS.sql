create or replace FUNCTION "GET_CENA_SKIDKI_ALL_SHOPS" (t_shops in sys.ODCIVarchar2List, i_date in date default sysdate)
return CENA_SKIDKI_ALL_SHOPS_TABLE
is
  v_ret   CENA_SKIDKI_ALL_SHOPS_TABLE;
begin
  
  
  
  select CENA_SKIDKI_ALL_SHOPS_LEN( 
      x.id_shop,x.art ,x.vidid, reg,
			case when substr(case when nvl(y.cena,0) = 0 and nvl(y.procent,0)!=0 then firm.round_dig( x.cena * ((100-nvl(y.procent,0))/100), case when reg = 'BY' then 0.01 else 0.1 end, 0)
                    when nvl(y.cena,0) != 0/* and nvl(y.procent,0)=0*/ then nvl(y.cena,0) else x.cena end,-2) = '99' and x.vidid not in ('50') then 'T' else nvl(y.doc_exception,'F') end,
			x.cena ,nvl(y.procent,0)  , nvl(y.cena,0),
      case when nvl(y.cena,0) = 0 and nvl(y.procent,0)!=0 then firm.round_dig( x.cena * ((100-nvl(y.procent,0))/100), case when reg = 'BY' then 0.01 else 0.1 end, 0)
                    when nvl(y.cena,0) != 0/* and nvl(y.procent,0)=0*/ then nvl(y.cena,0) else null end ,
      case when nvl(y.cena,0) = 0 and nvl(y.procent,0)!=0 then firm.round_dig( x.cena * ((100-nvl(y.procent,0))/100), case when reg = 'BY' then 0.01 else 0.1 end, 0)
                    when nvl(y.cena,0) != 0/* and nvl(y.procent,0)=0*/ then nvl(y.cena,0) else x.cena end )
  bulk collect into
      v_ret                  
  from
    (select id_shop,art,cena,vidid, case when id_shop in ('1U02','4114','4120','4401','4407','4409','4601','4602') then 'MOSK' when id_shop like '00%' then 'BY' else 'RU' end reg
     from ( select row_number() over(partition by y.id_shop,x.waers,x.art,x.vidid order by x.dateb desc) as nr
            ,y.id_shop,x.*
            from (select distinct art,cena,vidid,waers,dateb,datee
                  from firm.s_price
                  where waers in ('RUB','BYN') and vidid in ('01', '02', '50')
                  and trunc(i_date) between trunc(dateb) and trunc(datee)
                  order by art,vidid,dateb desc) x
            inner join (
  --                      select shopid id_shop,kval waers,priceid vidid from st_shop where org_kod = 'SHP'  --107 sec
                        select id_shop,waers,vidid from (select row_number() over(partition by p.id_shop order by dateb desc) as nr,  --95 sec
                                        p.* from st_price_shop p
                                        inner join st_shop s on s.shopid = p.id_shop
                                        inner join table(t_shops) d on d.column_value = s.shopid
                                        where trunc(i_date) between trunc(p.dateb) and trunc(p.datee)
                                        and s.org_kod = 'SHP' and p.is_sop = 'F' 
                                          --and p.id_shop in ('3615','2101','0031','4401')
                                          --and p.id_shop in ('0031','2101','4401')
                                        )
                        where nr = 1
                        ) y on y.waers = x.waers and y.vidid = x.vidid
           ) where nr = 1
		union all
		--сопутка
		select id_shop,art,cena,vidid, case when id_shop in ('1U02','4114','4120','4401','4407','4409','4601','4602') then 'MOSK' when id_shop like '00%' then 'BY' else 'RU' end reg
				 from ( select row_number() over(partition by y.shopid,x.waers,x.art,x.vidid order by x.dateb desc) as nr
								,y.shopid id_shop,x.*
								from (select distinct art,cena,vidid,waers,dateb,datee
											from firm.s_price
											where waers in ('RUB','BYN') and vidid in ('50')
											and trunc(i_date) between trunc(dateb) and trunc(datee)
											order by art,vidid,dateb desc) x
								inner join st_shop y on x.waers = y.kval and y.org_kod = 'SHP'
								inner join table(t_shops) d on d.column_value = y.shopid
							 ) where nr = 1
		--
		) x
  left join
    (select * from (
      select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
      from firm.pos_skidki1 a
      left join firm.pos_skidki2 b on a.docid = b.docid
      inner join firm.pos_skidki3 c on a.docid = c.docid
      inner join table(t_shops) d on c.shopid = d.column_value --- This is restriction view shops
      where trunc(i_date) between trunc(a.dates) and trunc(a.datee)  and a.disc_type_id in ('ZK01','ZK02') 
        --and c.shopid in ('3615','2101','0031','4401') 
        --and c.shopid in ('0031','2101','4401')
      ) where rn = 1) y on x.art = y.art and x.id_shop = y.shopid;
  
  --union all
  --
  --select x.art , x.vidid, reg, x.cena price, nvl(y.procent,0)   procent, y.cena,
  --      case when y.cena = 0 and nvl(y.procent,0)!=0 then firm.round_dig( x.cena * ((100-y.procent)/100), 0.01, 0)
  --                    when y.cena != 0 and nvl(y.procent,0)=0 then y.cena else null end price_with_discount,
  --      case when y.cena = 0 and nvl(y.procent,0)!=0 then firm.round_dig( x.cena * ((100-y.procent)/100), 0.01, 0)
  --                    when y.cena != 0 and nvl(y.procent,0)=0 then y.cena else x.cena end final_price
  --from
  --  (select art,cena, vidid, 'BY' reg from ( select row_number() over(partition by x.art,x.vidid order by x.dateb desc) as nr
  --      ,x.*
  --      from (select distinct art,cena,vidid,waers,dateb,datee
  --      from firm.s_price
  --      where waers in ('BYN') and vidid in ('01') order by art,vidid,dateb desc) x) where nr = 1) x
  --left join
  --  (select * from (
  --    select row_number() over(partition by b.art order by priority desc) rn, b.*, a.priority, a.disc_type_id
  --    from firm.pos_skidki1 a
  --    left join firm.pos_skidki2 b on a.docid = b.docid
  --    where trunc(sysdate) between trunc(a.dates) and trunc(a.datee) and substr(a.docid,1,1) = 'B' and a.disc_type_id in ('ZK01','ZK02')
  --    and a.docid not in ('B200000214','B200000213','B200000264','B200000265')) where rn = 1) y on x.art = y.art
    
  
  return v_ret;
end "GET_CENA_SKIDKI_ALL_SHOPS";