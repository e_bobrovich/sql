select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 1 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 1 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 1
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 2 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 2 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 2
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 3 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 3 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 3
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 4 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 4 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 4
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 5 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 5 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 5
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 6 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 6 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 6
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 7 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 7 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 7
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 8 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 8 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 8
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 9 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 9 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 9
order by a.id_shop, a.year, a.month, a.seller_tab;


select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 10 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 10 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 10
order by a.id_shop, a.year, a.month, a.seller_tab;