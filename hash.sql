insert into revo_hash (
id_dk, phone_number,
--hash_first, 
--salt, hash_salt,
age,sex
)
select 
a.id_dk, 
substr(b.phone_number,-10) phone_number,
--sha256.encrypt(a.id_dk||substr(b.phone_number,-10)) as hash_first,
--'aswbn9gv' as salt,
--sha256.encrypt(sha256.encrypt(a.id_dk||substr(b.phone_number,-10))||'aswbn9gv') as hash_salt,
--nvl(d.summ,0) as "summa2018", 
--nvl(d.cnt,0) as "count2018", 
--nvl(d.sred,0) as "sredn2018",
--nvl(d.maxim,0) as "maximum2018",
--
--nvl(c.summ,0) as "summa2019", 
--nvl(c.cnt,0) as "count2019", 
--nvl(c.sred,0) as "sredn2019",
--nvl(c.maxim,0) as "maximum2019",

trunc(months_between(sysdate,b.birthday)/12) age,
case when b.sex = '1' then 'М' else 'Ж' end as sex
from delivery_dk a
left join t_day_st_dk1 b on a.id_dk = b.id_dk
--
--left join (
--    select 
--    firm.pos_sale1.id_dk as id_dk, 
--    count(firm.pos_sale1.sale_sum) as cnt,
--    sum(firm.pos_sale1.sale_sum) as summ, 
--    round(avg(firm.pos_sale1.sale_sum),2) as sred,
--    max(firm.pos_sale1.SALE_SUM) as maxim
--    from firm.pos_sale1
--    where firm.pos_sale1.id_dk != ' ' and 
--    (firm.pos_sale1.sale_date >= to_date('01.01.2019', 'dd.mm.yyyy') and
--    firm.pos_sale1.sale_date <= to_date('20.02.2019', 'dd.mm.yyyy'))
--    group by firm.pos_sale1.id_dk
--) c on a.id_dk = c.id_dk
--
--left join (
--    select 
--    firm.pos_sale1.id_dk as id_dk, 
--    count(firm.pos_sale1.sale_sum) as cnt,
--    sum(firm.pos_sale1.sale_sum) as summ, 
--    round(avg(firm.pos_sale1.sale_sum),2) as sred,
--    max(firm.pos_sale1.SALE_SUM) as maxim
--    from firm.pos_sale1
--    where firm.pos_sale1.id_dk != ' ' and 
--    (firm.pos_sale1.sale_date >= to_date('01.01.2018', 'dd.mm.yyyy') and
--    firm.pos_sale1.sale_date <= to_date('31.12.2018', 'dd.mm.yyyy'))
--    group by firm.pos_sale1.id_dk
--) d on a.id_dk = d.id_dk

where a.id_del = 4602;

update revo_hash 
set new_hash = sha256.encrypt(id_dk||phone_number)
;

select * from revo_hash;

select 

b.id_dk, lower(c.ima) ima, b.phone_number, a.hash_prev, sha256.encrypt(lower(c.ima)||b.phone_number) hash_new, 
a.sum18, a.kol18, a.avg18, a.max18, 
a.sum19, a.kol19, a.avg19, a.max19,  
a.age, a.sex, a.limit
from revo_hash_new a
inner join revo_hash b on a.hash_prev = b.hash_first
inner join t_day_st_dk1 c on b.id_dk = c.id_dk

;



select count(*) from revo_hash_new;
select count(*) from revo_hash_final;
select count(*) from revo_hash;


update revo_hash a
set 
--a.ima = (select lower(b.ima) from t_day_st_dk1 b where b.id_dk = a.id_dk)
new_hash = sha256.encrypt(a.ima||a.phone_number)
;

select * from revo_hash;

select * from revo_hash a
inner join revo_hash_new b on a.hash_first = b.hash_prev;