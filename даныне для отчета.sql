--1162, 1163

--��������� ��� �� ���������
select trunc(to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS'),'DDD') as date1, 
       trunc(to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS'),'DDD') + 2 as date2 
from tech_data a
inner join delivery_pool b on a.id_del = b.id_del
where a.id_del in (1162, 1163)
and b.status = 'DONE'
order by to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS');

-- ��������� �������� �� �����
select a.id_del from tech_data a
inner join delivery_pool b on a.id_del = b.id_del
where 
to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS') between
  to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
and b.status = 'DONE';

--��������� �������� �� �����/���������
select substr(a.shopnum, 2, 5) as shopnum, a.shopid from firm.s_shop a  
where a.shopid in (
  select fl.shopid from delivery_history dh
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
  where dh.id_del in (1162, 1163)
  group by fl.shopid
) order by substr(a.shopnum, 2, 5);

-- ���������� � ���� �������� �� �������
select b.shopparent,dh.id_del,  min(trunc(to_date(c.start_date, 'DD.MM.YYYY HH24:MI:SS'),'DDD')) as start_date from delivery_history dh
left join firm.st_dk sd on dh.id_dk = sd.id_dk
inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
inner join tech_data c on dh.id_del = c.id_del
where dh.id_del in (1162, 1163,862)
and
b.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
group by b.shopparent, dh.id_del
order by b.shopparent;

--���������� �������, ����������� � ��������
select b.shopparent, count(dd.id_dk) from delivery_dk dd
left join firm.st_dk sd on dd.id_dk = sd.id_dk
inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
where dd.id_del in (1162, 1163,862)
and
b.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
--and dd.groupt = 'O'
group by b.shopparent
order by b.shopparent;

--���������� ���������, ������� �������� ��������� �� �������
--���������� ���������, ������� ��������� ��������� �� �������
--���������� ���������, ������� �������� ���-���������
--���������� ���������, ������� ��������� �� ����������
select b.shopparent, sc.name_channel, ss.name_status, count(dh.id_dk) from delivery_history dh
left join firm.st_dk sd on dh.id_dk = sd.id_dk
inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
inner join s_filial_logins fl on b.shopparent = fl.shopid
left join s_status ss on dh.id_status = ss.id_status
left join s_channel sc on dh.id_channel = sc.id_channel
where dh.id_del in (1162, 1163) 
and
b.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
and 
sc.name_channel != 'none'
and 
ss.name_status in ('DELIVRD', 'READ')
group by b.shopparent, sc.name_channel, ss.name_status 
order by b.shopparent, sc.name_channel, ss.name_status ;

--���-�� ���������, ������� ������ ���� �� ���� ����
--���-�� ���������, ������� ������ ���� �� ���� �����
--���-�� ���������, �������� ������������� ������
select sh.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season != ' '
    group by c.shopparent
    order by c.shopparent
  ) sh
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) >0
    group by c.shopparent
    order by c.shopparent
  )bg on sh.shopparent = bg.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) <=0
    group by c.shopparent
    order by c.shopparent
  )sp on sh.shopparent = sp.shopparent  
  order by sh.shopparent ;


--������� (���) �����
--������� ����� �����
--������� ������������� ������� �����
select sh.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
    select c.shopparent, count(*) as colvo from firm.pos_sale2 a
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    left join firm.Pos_Sale1 d on a.id_chek = d.id_chek and a.id_shop = d.id_shop
    where c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season != ' '
    group by c.shopparent
    order by c.shopparent
  ) sh
left join 
  (
    select c.shopparent, count(*) as colvo from firm.pos_sale2 a
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    left join firm.Pos_Sale1 d on a.id_chek = d.id_chek and a.id_shop = d.id_shop
    where c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) >0
    group by c.shopparent
    order by c.shopparent
  )bg on sh.shopparent = bg.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from firm.pos_sale2 a
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    left join firm.Pos_Sale1 d on a.id_chek = d.id_chek and a.id_shop = d.id_shop
    where c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) <=0
    group by c.shopparent
    order by c.shopparent
  )sp on sh.shopparent = sp.shopparent  
  order by sh.shopparent;
  
--������� (���) �� ���������, ���������� ��������� �� ������ ������
--������� (�����) �� ���������, ���������� ���������  �� ������ ������
--������� (��������.������) �� ���������, ���������� ��������� �� ������ ������

select sh.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season != ' '
    and (dh.id_status = 5 or dh.id_status = 6)
    group by c.shopparent
    order by c.shopparent
  ) sh
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) >0
    and (dh.id_status = 5 or dh.id_status = 6)
    group by c.shopparent
    order by c.shopparent
  )bg on sh.shopparent = bg.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) <=0
    and (dh.id_status = 5 or dh.id_status = 6)
    group by c.shopparent
    order by c.shopparent
  )sp on sh.shopparent = sp.shopparent  
  order by sh.shopparent;
  
--������� (���) �� ���������, ���������� ��������� �� �������
--������� (�����) �� ���������, ���������� ��������� �� �������
--������� (��������.������) �� ���������, ���������� ��������� �� �������
select sh.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season != ' '
    and (dh.id_status = 5 or dh.id_status = 6)
    and dh.id_channel = 1
    group by c.shopparent
    order by c.shopparent
  ) sh
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) >0
    and (dh.id_status = 5 or dh.id_status = 6)
    and dh.id_channel = 1
    group by c.shopparent
    order by c.shopparent
  )bg on sh.shopparent = bg.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) <=0
    and (dh.id_status = 5 or dh.id_status = 6)
    and dh.id_channel = 1
    group by c.shopparent
    order by c.shopparent
  )sp on sh.shopparent = sp.shopparent  
  order by sh.shopparent;
  
--������� (���) �� ���������, ���������� ���-���������
--������� (�����) �� ���������, ���������� ���-���������
--������� (��������.������) �� ���������, ���������� ���-���������
select sh.shopparent, nvl(sh.colvo, 0) as shoes, nvl(bg.colvo,0) as bags, nvl(sp.colvo,0) as other from 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season != ' '
    and (dh.id_status = 5 or dh.id_status = 6)
    and dh.id_channel = 2
    group by c.shopparent
    order by c.shopparent
  ) sh
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) >0
    and (dh.id_status = 5 or dh.id_status = 6)
    and dh.id_channel = 2
    group by c.shopparent
    order by c.shopparent
  )bg on sh.shopparent = bg.shopparent
left join 
  (
    select c.shopparent, count(*) as colvo from delivery_history dh
    left join firm.pos_sale1 d on dh.id_dk = d.id_dk
    left join firm.pos_sale2 a on d.id_chek = a.id_chek and d.id_shop = a.id_shop
    left join firm.s_art b on a.art = b.art
    left join firm.st_retail_hierarchy c on a.id_shop = c.shopid
    
    where 
    dh.id_del in (1162, 1163) 
    and 
    c.shopparent in ('1UF1','25F2','30F1','31F1','34F1','35F1','44F1')
    and
    d.sale_date between
    to_date('25.08.2017', 'DD.MM.YYYY') and to_date('26.08.2017', 'DD.MM.YYYY')
    and b.season = ' ' and INSTR(upper(b.assort), upper('�����')) <=0
    and (dh.id_status = 5 or dh.id_status = 6)
    and dh.id_channel = 2
    group by c.shopparent
    order by c.shopparent
  )sp on sh.shopparent = sp.shopparent  
  order by sh.shopparent;
