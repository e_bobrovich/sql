create or replace package body TDV_DK is

/*  -- Private type declarations
  type <TypeName> is <Datatype>;
  
  -- Private constant declarations
  <ConstantName> constant <Datatype> := <Value>;

  -- Private variable declarations
  <VariableName> <Datatype>;

  -- Function and procedure implementations
  function <FunctionName>(<Parameter> <Datatype>) return <Datatype> is
    <LocalVariable> <Datatype>;
  begin
    <Statement>;
    return(<Result>);
  end;

begin
  -- Initialization
  <Statement>;*/
  
  --*************************************************************
  -- начисление баллов на дк
  --
  --*************************************************************
  procedure scores_add(i_dk in varchar2 default null) is
    v_count integer;
    v_date_max date;
    v_date_activ date;
    v_id_sale_max integer;
    v_date_add date;
    v_ind integer :=0 ;
  begin
    -- выбираем все акции которые действует на текущую дату и у которых баллы к начислению > 0
    for r_stock in (select * from POS_DK_STOCK1 a 
                        where trunc(sysdate) between trunc(a.dateb) and trunc(a.datee) and a.scores >0
                       ) loop
      
      -- проверяю есть ли в таблице POS_DK_STOCK2 карты к данной акции, если есть, то будем только им начислять
      select count(*)
      into v_count
      from POS_DK_STOCK2
      where id = r_stock.id;
                                               
      -- перебираем все карты
      for r_dk in (select * from st_dk y
                      where y.dk_type = 'B' 
                               and (y.dkvid_id = r_stock.dkvid_id or r_stock.dkvid_id is null)  -- карты соответствуют заданному типу
                               and y.id_dk = case when i_dk is null then y.id_dk else i_dk end -- если для одной карты
                               and (y.dk_country = r_stock.landid or r_stock.landid is null) -- для заданной страны
                               and id_dk in (select case when v_count = 0 then y.id_dk else z.id_dk end from POS_DK_STOCK2 z where id = r_stock.id)
                               ) loop
        

        v_date_add := r_stock.dateb;
        
        --  проверка что уже начислялось {
        select count(*), max(dates), max(id_sale)
        into v_count, v_date_max, v_id_sale_max
        from pos_dk_scores x
        where x.id_dk = r_dk.id_dk and x.id_chek = r_stock.id and x.id_shop = 'FIRM';    
        
        -- если акция повторяемая, то проверяю начислялись ли баллы в период "повторения"  
        if r_stock.bit_repeat = 1 then

          if v_count != 0 then
            if upper(r_stock.repeat_period) = 'DAY'  then
              v_date_add := trunc(sysdate);
              if trunc(v_date_max) = trunc(sysdate) then
                v_count :=1 ;
              else 
                v_count :=0  ;                  
              end if;  
           
           elsif upper(r_stock.repeat_period) = 'WEEK' then
              v_date_add := trunc(sysdate,'DAY') + 1;
              if to_char(v_date_max,'yyyyiw') = to_char(sysdate,'yyyyiw')  then
                v_count :=1 ;
              else 
                v_count :=0  ;                  
              end if;  
           
            elsif upper(r_stock.repeat_period) = 'MONTH' then
              v_date_add := to_date('01'||to_char(sysdate,'mmyyyy'), 'ddmmyyyy');
              if to_char(v_date_max,'yyyymm') = to_char(sysdate,'yyyymm')  then  
                v_count :=1 ;
              else 
                v_count :=0  ;                  
              end if;  
          
            elsif upper(r_stock.repeat_period) = 'YEAR' then
              v_date_add := to_date('0101'||to_char(sysdate,'yyyy'), 'ddmmyyyy');              
              if to_char(v_date_max,'yyyy') = to_char(sysdate,'yyyy')  then  
                v_count :=1 ;
              else 
                v_count :=0  ;                  
              end if;  
           
            elsif r_stock.repeat_period is null then  
              v_count :=1 ;
            else
              v_count :=0  ;
            end if;  
         end if;
            
        end if;  -- проверка что уже начислялось }
        
        if v_count = 0 then
          -- все проверки пройдены, начисляем баллы
          v_id_sale_max := case when v_id_sale_max is null then 1 else v_id_sale_max + 1 end;
          
          insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
          values (r_dk.id_dk, 'ac', r_stock.id, v_id_sale_max, r_stock.scores, v_date_add,  'FIRM', 'F',  r_stock.id);
          
          v_ind := v_ind + 1;
        end if;  
    
      end loop; -- конец цикла дк                       
      commit;
      dbms_output.put_line('Баллы по акции '||r_stock.id||' начислены на '||v_ind||' карт');
        
    end loop;  -- конец цикла акций                  
    
  end;    
  
  
  
  --*************************************************************
  -- списание баллов на дк
  --
  --*************************************************************  
  procedure scores_clear(i_dk in varchar2 default null) is
    v_scores_ost integer := 0;
  begin 
    -- перебираем все карты
    for r_dk in (select * from st_dk y
                    where y.dk_type = 'B' 
                             and y.id_dk = case when i_dk is null then y.id_dk else i_dk end) loop  
      null;
      -- выбираю по карте все акции начислений, по которым еще не было списаний,
      -- и которые уже закончили свой срок
      for r_act in (select a.*, b.* 
                       from pos_dk_scores a
                       inner join POS_DK_STOCK1 b on a.id_chek = b.id and a.dk_sum > 0
                       left join pos_dk_scores c on a.id_dk = c.id_dk and a.id_chek = c.id_chek and nvl(a.id_sale,' ') = nvl(c.id_sale,' ') and c.id_shop = 'FIRM' and c.dk_sum < 0
                       where a.id_shop = 'FIRM' 
                                and c.id_dk is null -- не было списание
                                and trunc(sysdate) > trunc(a.dates) + b.burn_count -- закончили срок действия
                       order by trunc(a.dates) + b.burn_count  -- баллы которые сгорают раньше должны быть списаны в первую очередь    
                                ) loop
        
        -- расчет сколько баллов было потрачено в период действия акции
        -- берем все расходы с момента начисления баллов, до момента окончания действия акции
        select sum(dk_sum)
        into v_scores_ost
        from pos_dk_scores a
        where a.id_dk = r_act.id_dk 
                 and a.dates between r_act.dates and r_act.dates + r_act.burn_count 
                 and ((a.doc_type in ('ckv','voz') and  a.dk_sum > 0) or (a.doc_type not in ('ckv','voz') and  a.dk_sum < 0)) 
                 and a.id_chek not in (select b1.id_chek
                                              from pos_dk_scores b1
                                              left  join d_prixod1 c1 on b1.id_chek = c1.id and b1.id_shop = c1.id_shop and  b1.doc_type = 'voz'
                                              left  join pos_sale2 d1 on b1.id_chek = d1.id_chek and b1.id_shop = d1.id_shop and b1.doc_type = 'ckv'
                                              inner join pos_sale1 d2 on (c1.ndoc = to_char(d2.id_chek) and c1.id_shop = d2.id_shop) or 
                                                                                 (d1.id_chek_vozvr = d2.id_chek and d1.id_shop = d2.id_shop)
                                             where b1.doc_type in ('ckv','voz')     
                                                      and b1.id_dk = r_act.id_dk 
                                                      and d2.sale_date <  r_act.dates); -- исключаю возвраты по чеку проведенному до акции
       
        if  r_act.dk_sum + v_scores_ost > 0 then
          insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check)        
          values (r_act.id_dk, 'ac', r_act.id, r_act.id_sale, least(r_act.dk_sum + v_scores_ost, v_scores_ost),
                     to_date(to_char(r_act.dates + r_act.burn_count,'yyyymmdd')||'2359','yyyymmddhh24mi'),  'FIRM', 'F');            
        end if;  
        
      end loop;                                 
    end loop;                               
    null;
  end;   


  --*************************************************************
  -- списание баллов на дк
  -- версия 2
  --*************************************************************    
 procedure scores_clear2(i_dk in varchar2 default null, i_id_action in varchar2 default null) is   
    v_action_scores number := 0;
    v_voz_sum number := 0;
    v_scores_check_clear number := 0;
    v_add_days_after number(3):= 90; -- количество дней после окончания акции ,когда процедура еще будет пытаться списать баллы
    v_count integer :=0;
  
  begin 
     -- вызов сгорания накопительных баллов
    scores_clear_after_year(i_dk);
    
    -- перебираем все карты у которых есть действующие акции, или акции , которые закончили свой срок действия
    -- меньше месяца назад
    for r_dk in (select a.id_dk 
                       from pos_dk_scores a
                       inner join POS_DK_STOCK1 b on nvl(a.id_ac, a.id_chek) = b.id and a.dk_sum > 0
                       where --a.id_shop = 'FIRM' 
                                 sysdate between trunc(a.dates) and get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count) + v_add_days_after --case when b.burn_date is null then trunc(a.dates) + b.burn_count + v_add_days_after else b.burn_date + v_add_days_after end
                                 and a.id_dk = case when i_dk is null then a.id_dk else i_dk end
                                 and b.id = case when i_id_action is null then b.id else i_id_action end
                       group by a.id_dk) loop  

      delete from pos_dk_scores_firm 
      where id_dk =  r_dk.id_dk;

      insert into pos_dk_scores_firm
      select a.id_dk, doc_type, id_chek, 
              case when id_sale = ' ' then max(id_shop) else id_sale end id_sale,
--              case when substr(doc_type,1,2) = 'ac' and id_sale = ' ' then  to_char(id_shop) else id_sale end id_sale, 
              sum(dk_sum), 
              dates, case when sum(1) > 1 then 'SUM ' else max(id_shop) end id_shop, bit_open_check, id_ac, 'stnd' 
      from pos_dk_scores a 
      where id_dk = r_dk.id_dk and nvl(doc_type,' ') = 'ac'
      group by a.id_dk, doc_type, id_chek, id_sale, dates, bit_open_check, id_ac;

      

      insert into pos_dk_scores_firm
      select a.*, 'stnd' 
      from pos_dk_scores a 
      where id_dk = r_dk.id_dk and nvl(doc_type,' ') != 'ac';

      -- выбираю по карте все акции начислений,  ( по которым еще не было списаний - без этого )
      -- и которые уже закончили свой срок
      for r_act in (select a.*, b.* 
                       from pos_dk_scores_firm a
                       inner join POS_DK_STOCK1 b on nvl(a.id_ac, a.id_chek) = b.id and a.dk_sum > 0
                       where a.id_dk = r_dk.id_dk
                                 and b.id = case when i_id_action is null then b.id else i_id_action end
                                --and a.id_shop = 'FIRM' 
                                and sysdate between trunc(a.dates) and get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count) + v_add_days_after --case when b.burn_date is null then trunc(a.dates) + b.burn_count + v_add_days_after else b.burn_date + v_add_days_after end
                       order by get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count), dates  -- баллы которые сгорают раньше должны быть списаны в первую очередь    
                                ) loop
        
        v_action_scores := r_act.dk_sum ; --сколько начислили по акции
        v_count := 0 ;
        
/*        -- если начислено на чек, то проверяю, был ли возврат по данному чеку
        select count(*)
        into v_count
        from pos_dk_scores_firm b1
        left  join d_prixod1 c1 on b1.id_chek = c1.id and b1.id_shop = c1.id_shop and  b1.doc_type = 'voz'
        left  join pos_sale2 d1 on b1.id_chek = d1.id_chek and b1.id_shop = d1.id_shop and b1.doc_type = 'ckv'
        inner join pos_sale1 d2 on (c1.ndoc = to_char(d2.id_chek) and c1.id_shop = d2.id_shop) or 
                                           (d1.id_chek_vozvr = d2.id_chek and d1.id_shop = d2.id_shop)
        where b1.doc_type in ('ckv','voz')     
                and b1.id_dk = r_act.id_dk
                and to_char(d2.id_chek) = r_act.id_chek 
                and d2.id_shop = r_act.id_shop; 
        
        if v_count!= 0 then
          v_action_scores := 0;
        end if;  */
        
        -- вначале , если есть возвраты , то списываю баллы на них разделяя на акционные
        for r_pos in (select b1.*
                      from pos_dk_scores_firm b1
                      left  join d_prixod1 c1 on b1.id_chek = c1.id and b1.id_shop = c1.id_shop and  b1.doc_type = 'voz'
                      left  join pos_sale2 d1 on b1.id_chek = d1.id_chek and b1.id_shop = d1.id_shop and b1.doc_type = 'ckv'
                      inner join pos_sale1 d2 on (c1.ndoc = to_char(d2.id_chek) and c1.id_shop = d2.id_shop) or 
                                                         (d1.id_chek_vozvr = d2.id_chek and d1.id_shop = d2.id_shop)
                      where b1.doc_type in ('ckv','voz')     
                              and b1.id_dk = r_act.id_dk
                              and to_char(d2.id_chek) = r_act.id_chek 
                              and d2.id_shop = r_act.id_shop
                              and b1.dk_sum < 0) loop
                      
         if v_action_scores - abs(r_pos.dk_sum)  < 0 then
             
            update   pos_dk_scores_firm set dk_sum = r_pos.dk_sum + v_action_scores -- вычитаю из накопительных баллов акционные
            where id_dk = r_pos.id_dk and doc_type = r_pos.doc_type and id_chek = r_pos.id_chek 
                     and nvl(id_sale, ' ') = nvl(r_pos.id_sale, ' ') and dates = r_pos.dates and id_shop =  r_pos.id_shop 
                     and SCORES_TYPE = r_pos.SCORES_TYPE and nvl(id_ac,' ') = nvl(r_pos.id_ac, ' ') and dk_sum < 0 ;
                          
            insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, SCORES_TYPE, id_ac)        
            values (r_pos.id_dk, r_pos.doc_type, r_pos.id_chek, r_pos.id_sale, v_action_scores * -1, r_pos.dates, r_pos.id_shop, 'F', r_act.id, r_act.id_ac);   
              
            v_scores_check_clear := v_action_scores;                    
            v_action_scores := 0;     
          else
              
            update   pos_dk_scores_firm set SCORES_TYPE = r_act.id
            where id_dk = r_pos.id_dk and doc_type = r_pos.doc_type and id_chek = r_pos.id_chek 
                     and  nvl(id_sale, ' ') = nvl(r_pos.id_sale, ' ') and dates = r_pos.dates and id_shop =  r_pos.id_shop 
                     and SCORES_TYPE = r_pos.SCORES_TYPE and nvl(id_ac,' ') = nvl(r_pos.id_ac, ' ') and dk_sum < 0;
            
              
            v_scores_check_clear := abs(r_pos.dk_sum);
            v_action_scores := v_action_scores - v_scores_check_clear;  
          end if;  
        end loop;                     
        
        
        --dbms_output.put_line(get_scores_burn_date(a.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count)-1);
          
        -- перебираю все позиции по данной карте в pos_dk_scores
        -- делю на обычные баллы и акционные
        for r_pos in ( select a.*
                          from pos_dk_scores_firm a
                          where a.id_dk = r_dk.id_dk 
                                   and a.dates between r_act.dates and 
                                         to_date(to_char(get_scores_burn_date(r_act.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count), 'yyyymmdd')||'0000','yyyymmddhh24mi')     ---1 -- последний день когда быллы действуют
                                   and SCORES_TYPE = 'stnd'
                                   and a.doc_type = 'ck'
                                   and a.dk_sum < 0
                                   and v_action_scores != 0 -- не было возврата
                                   and not (a.id_chek = r_act.id_chek and a.id_shop = r_act.id_shop) -- не может быть потрачена на тот же чек , что и начислено
                           order by a.dates ) loop
         
         v_scores_check_clear := 0; 
        
         --dbms_output.put_line(r_act.id||' '||v_action_scores||' ' ); 
            
         if v_action_scores - abs(r_pos.dk_sum) < 0 then
             
            update   pos_dk_scores_firm set dk_sum = r_pos.dk_sum + v_action_scores -- вычитаю из накопительных баллов акционные
            where id_dk = r_pos.id_dk and doc_type = r_pos.doc_type and id_chek = r_pos.id_chek 
                     and id_sale = r_pos.id_sale and dates = r_pos.dates and id_shop =  r_pos.id_shop 
                     and SCORES_TYPE = r_pos.SCORES_TYPE and nvl(id_ac,' ') = nvl(r_pos.id_ac, ' ') and dk_sum < 0 ;
                          
            insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, SCORES_TYPE, id_ac)        
            values (r_pos.id_dk, r_pos.doc_type, r_pos.id_chek, r_pos.id_sale, v_action_scores * -1, r_pos.dates, r_pos.id_shop, 'F', r_act.id, r_act.id_ac);   
              
            v_scores_check_clear := v_action_scores;                    
            v_action_scores := 0;     
          else
              
            update   pos_dk_scores_firm set SCORES_TYPE = r_act.id
            where id_dk = r_pos.id_dk and doc_type = r_pos.doc_type and id_chek = r_pos.id_chek 
                     and id_sale = r_pos.id_sale and dates = r_pos.dates and id_shop =  r_pos.id_shop 
                     and SCORES_TYPE = r_pos.SCORES_TYPE and nvl(id_ac,' ') = nvl(r_pos.id_ac, ' ') and dk_sum < 0;
            
              
            v_scores_check_clear := abs(r_pos.dk_sum);
            v_action_scores := v_action_scores - v_scores_check_clear;  
          end if;     
          
          -- ищем возврат по данному чеку, если такой был - возвращаем акционные баллы
          begin
            select sum(b1.dk_sum)
            into v_voz_sum
            from pos_dk_scores_firm b1
            left  join d_prixod1 c1 on b1.id_chek = c1.id and b1.id_shop = c1.id_shop and  b1.doc_type = 'voz'
            left  join pos_sale2 d1 on b1.id_chek = d1.id_chek and b1.id_shop = d1.id_shop and b1.doc_type = 'ckv'
            inner join pos_sale1 d2 on (c1.ndoc = to_char(d2.id_chek) and c1.id_shop = d2.id_shop) or 
                                               (d1.id_chek_vozvr = d2.id_chek and d1.id_shop = d2.id_shop)
            where b1.doc_type in ('ckv','voz')     
                    and b1.id_dk = r_pos.id_dk
                    and to_char(d2.id_chek) = r_pos.id_chek 
                    and d2.id_shop = r_pos.id_shop
                    and b1.dk_sum > 0; 
            
            if v_voz_sum is null then
              v_voz_sum := 0; 
            end if;          
          exception when no_data_found then
              v_voz_sum := 0;        
          end;          
     
          if v_voz_sum > 0 then
            v_action_scores := v_action_scores + least(v_scores_check_clear, v_voz_sum);
          end if;  
          
          if v_action_scores = 0 then
            exit;
          end if;                                                              
        end loop;                                                                        

        -- если уже было списание по акции , удаляю и вставляю новую запись
        delete from pos_dk_scores_firm 
        where id_dk =  r_act.id_dk and    doc_type = 'ac' and id_chek = r_act.id and  nvl(id_sale,' ') = nvl(r_act.id_sale,' ') 
                 and dates = get_scores_burn_date(r_act.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count)
                 and id_shop = 'FIRM' and bit_open_check = 'F' and SCORES_TYPE = 'stnd' and nvl(id_ac, ' ') = nvl(r_act.id_ac, ' ') and dk_sum <= 0;
        
        insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, SCORES_TYPE, id_ac)        
        values (r_act.id_dk, 'ac', r_act.id, r_act.id_sale, v_action_scores * -1,
                  get_scores_burn_date(r_act.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count) ,--case when r_act.burn_date is null then trunc(r_act.dates) + r_act.burn_count else  r_act.burn_date end,
                  'FIRM', 'F', 'stnd', r_act.id_ac);   
        
        -- если пришло время сгорания баллов, то списываем из главной таблицы      
        if --((r_act.burn_date is null and sysdate > trunc(r_act.dates) + r_act.burn_count) or (r_act.burn_date is not null and trunc(sysdate)>=trunc(r_act.burn_date))) 
            sysdate >= get_scores_burn_date(r_act.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count) 
            and to_char(r_act.dates,'yyyymmdd')>='20161208'
            --and 'a' = 'b'
        then

          delete from pos_dk_scores a 
          where a.id_dk = r_dk.id_dk
                   and a.doc_type = 'ac'
                   and a.id_chek =  r_act.id
                   and nvl(a.id_sale,' ') = nvl(r_act.id_sale,' ')
                   and a.dk_sum <= 0
                   and a.id_shop = 'FIRM'
                   and nvl(id_ac, ' ') = nvl(id_ac, ' ');

          insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
          values (r_act.id_dk, 'ac', r_act.id, r_act.id_sale, v_action_scores * -1,
                    --case when r_act.burn_date is null then to_date(to_char(r_act.dates + r_act.burn_count ,'yyyymmdd'),'yyyymmdd') else r_act.burn_date end,
                    trunc(get_scores_burn_date(r_act.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count)), 
                    'FIRM', 'F', r_act.id_ac);  
        end if;          
      end loop;  
      
      commit;                               
    end loop;                               
    null;   
 end;  
 
  --*************************************************************
  -- списание стандартных баллов через год в тот день когда были начисленны
  --*************************************************************    
  ---------------------------------------
  -- Bobrovich 28.09.17 (upd 11.12.17)-убрана фильтрация по филиалам
  ----- сгорание стандартных баллов -----
 procedure scores_clear_after_year(i_dk in varchar2 default null, i_date in timestamp default systimestamp) is   
    v_scores_day_acc integer := 0; -- количество начисленных очков в день год назад(accrual)
    v_date_first_chek timestamp; -- дата и время первого чека год назад
    v_scores_before_sum integer := 0; -- сумма баллов по день(год назад) включительно(summary)
    v_scores_year_ded integer := 0; -- сумма вычетов в течении года (deductions)
    v_scores_aft_check_day integer := 0; -- сумма вычетов в течении дня после ччека (deductions)
  begin 
    -- проход по дисконтным картам с начислениями баллов по чеку
    --(количество обычных баллов начисленных в день год назад и дата\время первого чека)
    for r_dk in (
      select id_dk, nvl(sum(dk_sum), 0) as scores_acc, nvl(min(dates), i_date) as date_chek
      from tdv_scores_split2
      where dk_sum > 0
      and (doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null))
      and id_dk = case when i_dk is null then id_dk else i_dk end
      and  trunc(add_months(i_date, -12), 'ddd') = trunc(dates, 'ddd') -- 12 месяцев назад
      and id_dk not in (select id_dk from pos_dk_scores where id_ac = 'STND0365' and trunc(dates) = trunc(i_date))
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
--      and id_dk in (
--        select id_dk from st_dk a 
--        inner join st_retail_hierarchy b on a.id_shop = b.shopid 
--        where b.shopparent in ('21F1', '22F1', '23F1', '24F1', '25F1', '26F1')
--      )
      group by id_dk         
    ) loop
    
      -- сумма баллов в день год назад
      select nvl(sum(dk_sum), 0) into v_scores_before_sum from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum <> 0
      and (((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and trunc(r_dk.date_chek, 'ddd') >= trunc(dates, 'ddd')       
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
      order by dates;
      
      -- сумма баллов в день год назад после первого начисления
      select nvl(sum(dk_sum), 0) into v_scores_aft_check_day  from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum < 0
      and (((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and dates > r_dk.date_chek -- после первого чека
      and dates <= r_dk.date_chek+1 -- в течении дня
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      ;
      -----
      
      v_scores_before_sum := v_scores_before_sum - v_scores_aft_check_day;
      --dbms_output.put_line('id = '||r_dk.id_dk||' v_scores_before_sum - v_scores_aft_check_day*-1 = '||v_scores_before_sum||' - '||v_scores_aft_check_day||' = '||(v_scores_before_sum - v_scores_aft_check_day*-1));
      
      
      -- сумма вычетов за год после первого чека
      select nvl(sum(dk_sum), 0) into v_scores_year_ded from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum < 0
      and (((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and dates > r_dk.date_chek
      and dates <= i_date --текущая дата
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
      order by dates;
      
      -- если разность суммы начисленных год назад и суммы использованных больше 0
      --dbms_output.put('id = '||r_dk.id_dk||' scores_acc = '||r_dk.scores_acc||' v_scores_before_sum = '||v_scores_before_sum||' v_scores_year_ded = '||v_scores_year_ded||' v_scores_before_sum - v_scores_year_ded*-1 = '||(v_scores_before_sum - v_scores_year_ded*-1));
      if (v_scores_before_sum - v_scores_year_ded*-1 > 0) then

        -- то добавить запись о вычете неиспольщованных 

        --dbms_output.put('insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'');');
        
        insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(v_scores_before_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365');
        
        --dbms_output.put('insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'', ''TEST0009'');');
        
        insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(v_scores_before_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365', 'stnd');
        
        --commit;
      end if;
    end loop;
  --exception
end scores_clear_after_year;
--------------------------------------- 
 
   --*************************************************************
  -- списание стандартных баллов, которые были начсилены год назад и ранее
  --*************************************************************    
  ---------------------------------------
  -- Bobrovich 26.10.17 (upd 30.10.17)
  ----- сгорание стандартных баллов -----
 procedure scores_clear_after_year_other(i_dk in varchar2 default null, i_date in timestamp default systimestamp) is   
    v_scores_day_acc integer := 0; -- количество начисленных очков в день год назад(accrual)
    v_date_first_chek timestamp; -- дата и время первого чека год назад
    v_scores_before_sum integer := 0; -- сумма баллов по день(год назад) включительно(summary)
    v_scores_year_ded integer := 0; -- сумма вычетов в течении года (deductions)
    v_scores_aft_check_day integer := 0; -- сумма вычетов в течении дня после ччека (deductions)
    
    v_date_f_chek timestamp;
  begin 
    -- проход по дисконтным картам с начислениями баллов по чеку
    --(количество обычных баллов начисленных в день год назад и дата\время первого чека)
    for r_dk in (
      select id_dk, nvl(sum(dk_sum), 0) as /*scores_acc*/ scores_sum, nvl(max(dates), i_date) as date_chek
      from tdv_scores_split2
      --where dk_sum > 0
      where dk_sum <> 0
      and (doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null))
      and id_dk = case when i_dk is null then id_dk else i_dk end
      and  trunc(add_months(i_date, -12), 'ddd') >= trunc(dates, 'ddd') -- 12 месяцев назад
      and id_dk not in (select id_dk from pos_dk_scores where id_ac = 'STND0365')
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
--      and id_dk in (
--        select id_dk from st_dk a 
--        inner join st_retail_hierarchy b on a.id_shop = b.shopid 
--        where b.shopparent in ('21F1', '22F1', '23F1', '24F1', '25F1', '26F1')
--      )
      group by id_dk         
    ) loop
      --dbms_output.put_line('v_date_f_chek = '||to_char(r_dk.date_chek, 'dd.mm.yyyy hh24:mi:ss'));
    
--      select nvl(min(dates), i_date)
--      into v_date_f_chek
--      from tdv_scores_split2
--      where dk_sum > 0
--      and ((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd')
--      and id_dk = r_dk.id_dk
--      and  trunc(r_dk.date_chek, 'ddd') = trunc(dates, 'ddd') -- 12 месяцев назад
--      --and id_dk not in (select id_dk from pos_dk_scores where id_ac = 'STND0365' and trunc(dates) = trunc(systimestamp))
--      -----
--      and(
--      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
--      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
--      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
--      )
--      -----      
--      group by id_dk; 
--      
--      --dbms_output.put_line('v_date_last_chek = '||to_char(v_date_f_chek, 'dd.mm.yyyy hh24:mi:ss'));
--
--      -- сумма баллов в день год назад
--      select nvl(sum(dk_sum), 0) into v_scores_before_sum from tdv_scores_split2 a
--      where id_dk = r_dk.id_dk
--      and dk_sum <> 0
--      and (((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
--      and trunc(v_date_f_chek, 'ddd') >= trunc(dates, 'ddd')       
--      -----
--      and(
--      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
--      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
--      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
--      )
--      -----
--      order by dates;
--      
--      -- сумма баллов в день год назад после первого начисления
--      select nvl(sum(dk_sum), 0) into v_scores_aft_check_day  from tdv_scores_split2 a
--      where id_dk = r_dk.id_dk
--      and dk_sum < 0
--      and (((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
--      and dates > v_date_f_chek -- после первого чека
--      and dates <= v_date_f_chek+1 -- в течении дня
--      -----
--      and(
--      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
--      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
--      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
--      )
--      ;
--      -----
--      
--      v_scores_before_sum := v_scores_before_sum - v_scores_aft_check_day;
--      --dbms_output.put_line('id = '||r_dk.id_dk||' v_scores_before_sum - v_scores_aft_check_day*-1 = '||v_scores_before_sum||' - '||v_scores_aft_check_day||' = '||(v_scores_before_sum - v_scores_aft_check_day*-1));
--      
      -- сумма вычетов за год после первого чека
      select nvl(sum(dk_sum), 0) into v_scores_year_ded from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum < 0
      --and (doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null))
      and (((doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and dates > r_dk.date_chek
      and dates <= i_date --текущая дата
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
      order by dates;
      
      -- если разность суммы начисленных год назад и суммы использованных больше 0
      --dbms_output.put_line('id = '||r_dk.id_dk||' scores_acc = '||r_dk.scores_acc||' v_scores_before_sum = '||v_scores_before_sum||' v_scores_year_ded = '||v_scores_year_ded||' v_scores_before_sum - v_scores_year_ded*-1 = '||(v_scores_before_sum - v_scores_year_ded*-1));
      --if (v_scores_before_sum - v_scores_year_ded*-1 > 0) then
      if (r_dk.scores_sum - v_scores_year_ded*-1 > 0) then

        --dbms_output.put('insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'');');
        
        insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(r_dk.scores_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365');
        
        --dbms_output.put('insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'', ''TEST0009'');');
        
        --insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        --values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(r_dk.scores_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365', 'stnd');
        
        --commit;
      end if;
    end loop;
  --exception
end scores_clear_after_year_other;
--------------------------------------- 
 
 -- дата когда баллы должны сгореть
 function get_scores_burn_date(i_add_date in date, --дата, когда баллы были начислены
                                           i_burn_date in date,   -- если баллы сгорают на дату
                                           i_burn_period in varchar2, -- период действия баллов
                                           i_burn_count in number -- кол-во периодов действия баллов
                                            ) return date is
  v_ret_date date;                                            
 begin
   if i_burn_date is not null then 
     v_ret_date := i_burn_date;
   else  
    if  upper(i_burn_period) = 'DAY' then
      v_ret_date := i_add_date + i_burn_count ;
    elsif upper(i_burn_period) = 'MONTH' then
      v_ret_date :=  add_months(i_add_date, i_burn_count) ;        
    elsif upper(i_burn_period) = 'YEAR' then
      v_ret_date :=  add_months(i_add_date, i_burn_count * 12) ;    
    elsif upper(i_burn_period) = 'WEEK' then
      v_ret_date :=  i_add_date + i_burn_count * 7 ;                  
    end if;  
   end if; 
   
   return trunc(v_ret_date);
 end;    
end TDV_DK;