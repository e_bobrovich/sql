select a.id_chek, a.id_shop, a.seller_tab, a.seller_fio, b.stat2, b.massn, b.massn_name, b.stat5, b.begda, b.endda, a.sale_sum, a.sale_date from pos_sale1 a
left join (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    --where trunc(to_date(a.begda, 'yyyymmdd')) <= to_date('17.10.2018','dd.mm.yyyy') and trunc(to_date(a.endda, 'yyyymmdd')) >= to_date('17.10.2018','dd.mm.yyyy')
) b on a.seller_tab = b.pernr and trunc(begda) <= trunc(a.sale_date) and trunc(endda) >= trunc(a.sale_date)
where trunc(a.sale_date) >= to_date('10.10.2018','dd.mm.yyyy') and trunc(a.sale_date) <= to_date('17.10.2018','dd.mm.yyyy')
and a.bit_vozvr != 'T'
and 
(
b.stat2 = '0' 
or b.massn = '10' 
or b.stat5 = 1
)
;

select * from pos_sale1 
where seller_tab = '00006239' 
--and sale_date >=  to_date('10.10.2018','dd.mm.yyyy')
order by sale_date desc
;

select * from s_seller where tabno = '00023058';

select * from s_seller where fio like '%Полякус Анна Александровна%';

select  a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5  from s_seller_sap a
left join st_seller_massn b on a.massn = b.massn_id
--full join s_seller c on a.pernr = c.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
where pernr like '%20675' --'%30000369'
order by begda;




select seller_tab, count(1) from pos_sale1 a
left join (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    --where trunc(to_date(a.begda, 'yyyymmdd')) <= to_date('17.10.2018','dd.mm.yyyy') and trunc(to_date(a.endda, 'yyyymmdd')) >= to_date('17.10.2018','dd.mm.yyyy')
) b on a.seller_tab = b.pernr and trunc(begda) <= trunc(a.sale_date) and trunc(endda) >= trunc(a.sale_date)
where trunc(a.sale_date) >= to_date('01.10.2018','dd.mm.yyyy') and trunc(a.sale_date) <= to_date('24.10.2018','dd.mm.yyyy')
and 
(
--b.stat2 = '0' 
--or b.massn = '10' 
b.stat5 = 1
)
and a.bit_vozvr != 'T'
group by seller_tab
;




select b.tabno, b.fio,  b.stext2, b.stext3, b.dateb, b.dated, d.massn, d.massn_name,d.stat2, d.stat5, d.begda, d.endda, a.*, c.tabno zavmag_tab, c.fio zavmag from bep_prem_seller a
left join (
    select a1.tabno,a1.fio,a1.stext2,a1.stext3,a1.dateb,a1.dated,a1.orgno from s_seller a1
    inner join (
        select tabno, min(dated) dated from s_seller
        where  ((EXTRACT(MONTH FROM dateb) <= COALESCE(10, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and    ((EXTRACT(MONTH FROM dated) >= COALESCE(10, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dated = b1.dated
) b on a.seller_tab = b.tabno
left join (
    select * from (
        select tabno, fio, orgno from s_seller 
        where 
        orgno = lpad(COALESCE('0031', orgno), 5, '0') and
        (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
        and ((EXTRACT(MONTH FROM dateb) <= COALESCE(10, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM dated) >= COALESCE(10, EXTRACT(MONTH FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        order by stext2 desc, dated desc
    ) where rownum = 1
) c on lpad(a.id_shop, 5, '0') = c.orgno
left join (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    inner join (
        select pernr, min(endda) endda from s_seller_sap a 
        where
            ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) <= COALESCE(10, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) >= COALESCE(10, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        group by pernr
    ) c on a.pernr = c.pernr and a.endda = c.endda
) d on a.seller_tab = d.pernr
where a.year = COALESCE(2018, EXTRACT(YEAR FROM sysdate)) and a.month = COALESCE(10, EXTRACT(MONTH FROM sysdate)) 
and a.id_shop = COALESCE('0031', a.id_shop) 
and a.seller_tab != c.tabno
;

