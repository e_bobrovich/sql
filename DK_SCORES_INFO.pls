create or replace procedure dk_scores_info(
            i_id_dk in varchar2 default '', 
            i_date in varchar2 default to_char(systimestamp, 'dd.mm.yy')) is
/* для выбранной дк до заданного времени подсчет баллов каждого типа, определние даты сгорания*/
    v_stnd_deductions integer := 0;
    v_stnd_accrual integer := 0;
    v_stnd_from_ac integer := 0;
    
    v_ac_deductions integer := 0;
    v_ac_accrual integer := 0;
    v_ac_burn integer := 0;
    
    v_date_burn varchar2(10) := '' ;
    v_text_stock varchar2(200) := ' ';
    v_res integer := 0;
begin
    dbms_output.enable;
    dbms_output.put_line(i_id_dk||' '||i_date);
/*
------------------------------------------
----- ПОДСЧЕТ И ВСТАВКА АКЦИОННЫХ
------------------------------------------
    for r_sc in (
        -- АКЦИОННЫЕ БАЛЛЫ ДЛЯ ЗАДАННОЙ ДК
        select a.dk_sum,  a.dates, b.id, b.text, tdv_dk.get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count) as date_burn
        from pos_dk_scores a
        inner join pos_dk_stock1 b on nvl(a.id_ac, a.id_chek) = b.id and a.dk_sum > 0
        where 
        a.id_dk = i_id_dk
        and 
        (i_date between 
            trunc(a.dates) 
            and tdv_dk.get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count)
        )
        and a.dates <= i_date 
        order by a.dates
    ) loop
        insert into tem_dk_scores_info(id_dk,scores_type, scores_sum, text, date_burn)
        values(i_id_dk, r_sc.id, r_sc.dk_sum, r_sc.text, r_sc.date_burn);
    end loop;

------------------------------------------
----- ПОДСЧЕТ И ВСТАВКА СТАНДАРТНЫХ
------------------------------------------
    commit;
*/

/*
    dbms_output.put_line(i_id_dk||' '||i_date);
    for r_sc in (    
        select b.scores_type, sum(b.scores_sum) as scores_sum, b.text, b.burn_date from (
            select 
            case when a.id_ac is not null then a.id_ac else a.scores_type end scores_type,
            a.dk_sum as scores_sum,  a.dates, nvl(b.text, 'Накопительные баллы') text, 
            tdv_dk.get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count) burn_date
            from pos_dk_scores_firm a
            left join pos_dk_stock1 b on case when a.id_ac is not null then a.id_ac else a.scores_type end = b.id 
            where
            a.id_dk = i_id_dk
            and 
            (to_timestamp(i_date, 'dd.mm.yy') between 
                trunc(a.dates) and 
                (case when a.scores_type = 'stnd' 
                    then to_timestamp(i_date, 'dd.mm.yy')+1
                    else tdv_dk.get_scores_burn_date(a.dates, b.burn_date, b.burn_period, b.burn_count)
                end)
            )
            and a.dates <= to_timestamp(i_date, 'dd.mm.yy')
            order by a.dates
        ) b 
        group by b.scores_type, b.text, b.burn_date
        order by b.burn_date
    ) loop
        dbms_output.put_line(r_sc.scores_type||' '||r_sc.scores_sum||' '||r_sc.text||' '||r_sc.burn_date);
    end loop;
*/
    -- проход по типам баллов которые были у дк по дату включительно
    for r_sct in (
        select b.id_ac as scores_type from(
            select 
            nvl(a.id_ac, a.id_chek) id_ac 
            from pos_dk_scores_firm a 
            where a.id_dk = i_id_dk
            and a.dk_sum > 0
            and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'))
            and (id_ac is not null or not regexp_like(a.id_chek, '^[[:digit:]]+$'))
            --group by a.id_ac
        ) b   
        group by b.id_ac
        order by b.id_ac
    ) loop
        dbms_output.put_line('');
        dbms_output.put_line(r_sct.scores_type);
        --if (r_sct.scores_type = 'stnd') then
        
        --else
            dbms_output.put_line('Акционные');  
            
            -- VERSION 1
            /*
            -- СУММА НАЧИСЛЕНИЙ ДО ЭТОГО ДНЯ ПО АКЦИОННЫМ
            select nvl(sum(dk_sum), 0) into v_ac_accrual
            from pos_dk_scores_firm a
            where a.id_dk = i_id_dk
            and a.dk_sum > 0
            and a.doc_type = 'ac'
            and  (a.id_ac = r_sct.scores_type or a.id_chek = 'SC100001')
            and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'));
            
            dbms_output.put_line(r_sct.scores_type||' - v_ac_accrual = '||v_ac_accrual);
            
            -- СУММА СПИСАНИЙ ДО ЭТОГО ДНЯ ПО АКЦИООННЫМ (?сделать не учитывая списание в фирм)
            select nvl(sum(dk_sum), 0) into v_ac_deductions
            from pos_dk_scores_firm a
            where a.id_dk = i_id_dk
            and a.dk_sum <= 0
            and
            (   
                (
                    a.doc_type = 'ac' 
                    --and a.id_ac is not null
                    and a.scores_type = 'stnd'
                    and a.id_shop = 'FIRM'
                    and a.id_chek = r_sct.scores_type
                    and a.id_ac != 'TEST0009' -- учесть "акцию" списание через год
                )
                or
                (
                    a.doc_type = 'ck'
                    --and a.id_ac is null
                    and a.scores_type = r_sct.scores_type
                )
                or
                (
                    (a.doc_type = 'voz'  or a.doc_type = 'ckv')
                    and (a.id_ac = r_sct.scores_type or a.scores_type = r_sct.scores_type) 
                )
            )  
            and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'));
            
            dbms_output.put_line(r_sct.scores_type||' - v_ac_deductions = '||v_ac_deductions);
            
            if (v_ac_accrual + v_ac_deductions > 0) then
                v_stnd_from_ac := v_stnd_from_ac + (v_ac_accrual + v_ac_deductions);
                dbms_output.put_line(r_sct.scores_type||' - v_stnd_from_ac = '||v_stnd_from_ac);
            end if;
            */
            
            -- VERSION 2
            -- цикл по начислениям данного типа акционных, запоминание количества
            for r_acc in (select id_chek, id_sale, dk_sum, dates, id_shop, id_ac, scores_type from pos_dk_scores_firm a
                            where a.id_dk = i_id_dk
                            and a.dk_sum > 0
                            and a.doc_type = 'ac'
                            and (a.id_ac = r_sct.scores_type or a.id_chek = r_sct.scores_type)
                            and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'))
                            order by a.dates)
            loop
                v_ac_accrual := 0;
                v_ac_deductions := 0;
                v_res := 0;
                v_text_stock := ' ';
                v_date_burn := '';
                
                dbms_output.put_line('\\'||r_sct.scores_type||' - '||r_acc.id_sale);
                v_ac_accrual := r_acc.dk_sum;
                dbms_output.put_line(r_sct.scores_type||' - v_ac_accrual = '||v_ac_accrual);
                
                -- сумма списаний вне фирм
                select nvl(sum(dk_sum), 0)  into v_ac_deductions
                from pos_dk_scores_firm a
                where a.id_dk = i_id_dk
                and a.dk_sum <= 0
                and
                (   
                    (
                        a.doc_type = 'ck'
                        --and a.id_ac is null
                        and a.scores_type = r_sct.scores_type
                    )
                    or
                    (
                        (a.doc_type = 'voz'  or a.doc_type = 'ckv')
                        and (a.id_ac = r_sct.scores_type or a.scores_type = r_sct.scores_type) 
                    )
                )  
                and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'));
                dbms_output.put_line(r_sct.scores_type||' - v_ac_deductions = '||v_ac_deductions);
                
                -- сумма списаний в фирм
                select nvl(sum(dk_sum), 0) into v_ac_burn
                from pos_dk_scores_firm a
                where a.id_dk = i_id_dk
                and a.dk_sum <= 0
                and
                (   
                        a.doc_type = 'ac' 
                        --and a.id_ac is not null
                        and a.scores_type = 'stnd'
                        and a.id_shop = 'FIRM'
                        and a.id_chek = r_sct.scores_type
                        and a.id_sale = r_acc.id_sale 
                )  
                and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'));
                dbms_output.put_line(r_sct.scores_type||' - v_ac_burn = '||v_ac_burn);
                
                -- списания в фирм не 0
                if (v_ac_burn != 0)
                then 
                    -- то начисленное - списания вне - списания в фирм = рес
                    v_res := v_ac_accrual + v_ac_deductions + v_ac_burn;
                    dbms_output.put_line(r_sct.scores_type||' - ( '||v_ac_accrual||' + '||v_ac_deductions||' + '||v_ac_burn||')');
                    dbms_output.put_line(r_sct.scores_type||' - v_res = '||v_res);
                    
                    -- рес > 0 
                    if (v_res > 0)
                    then
                        -- то стнд + рес
                        v_stnd_from_ac := v_stnd_from_ac + v_res;
                        dbms_output.put_line(r_sct.scores_type||' - v_stnd_from_ac = '||v_stnd_from_ac);
                        dbms_output.put_line(r_sct.scores_type||' - ИГНОР');
                    else
                        dbms_output.put_line(r_sct.scores_type||' - ИГНОР');
                    end if;
                else
                    -- иначе рес = начислен - списания вне
                    v_res := v_ac_accrual + v_ac_deductions;
                    dbms_output.put_line(r_sct.scores_type||' - ( '||v_ac_accrual||' + '||v_ac_deductions||')');
                    dbms_output.put_line(r_sct.scores_type||' - v_res = '||v_res);
                    
                    -- вычисление даты сгорания
                    --trunc(tdv.get_scores_burn_date(r_act.dates, r_act.burn_date, r_act.burn_period, r_act.burn_count)), 
                    begin
                        select 
                        to_char(trunc(tdv_dk.get_scores_burn_date(r_acc.dates, a.burn_date, a.burn_period, a.burn_count)), 'dd.mm.yy'),
                        nvl(a.text, ' ')
                        into v_date_burn, v_text_stock
                        from pos_dk_stock1 a
                        where a.id = r_sct.scores_type;                                                            
                    exception when no_data_found then
                        v_date_burn := ' ';
                    end;        
                    dbms_output.put_line(r_sct.scores_type||' - v_date_burn = '||v_date_burn);
                    -- запись рес и даты
                    insert into tem_dk_scores_info_pr(id_dk, scores_type, scores_sum, text, date_burn, dates)
                    values (i_id_dk, r_sct.scores_type, v_res, v_text_stock, v_date_burn, to_char(r_acc.dates,'dd.mm.yy'));
                    dbms_output.put_line(r_sct.scores_type||' - ЗАПИСЬ в PR');
                    commit;
                end if;
            end loop;
            
            declare
                v_t_sum integer := 0;
                v_t_dates varchar2(200);
                v_t_text varchar2(200) := ' ';
                v_t_date_burn varchar2(200) := ' ';
            begin    
                -- выборка одинаковых акционных , у которых в один день сгорают
                dbms_output.put_line(r_sct.scores_type||' - выборка одинаковых акционных');
                insert into tem_dk_scores_info(id_dk, scores_type, scores_sum, text, date_burn, dates)
                select i_id_dk, r_sct.scores_type, a.summ, a.text, a.date_burn, a.dates
                --select summ, text, date_burn, dates   
                --into v_t_sum, v_t_text, v_t_date_burn, v_t_dates
                from (
                    select a.id_dk, a.scores_type, a.text, a.date_burn, a.dates ,
                    sum(a.scores_sum) as summ 
                    
                    from tem_dk_scores_info_pr a 
                    where a.id_dk = i_id_dk
                    and a.scores_type = r_sct.scores_type
                    group by a.id_dk, a.scores_type, a.text, a.date_burn, a.dates
                ) a where a.summ != 0;    
                commit;
                
                dbms_output.put_line(r_sct.scores_type||' - ЗАПИСЬ в FINAL');
                --dbms_output.put_line(r_sct.scores_type||' - ( '||v_t_sum||' , '||v_t_text||' , '||v_t_dates||' , '||v_t_date_burn||')');
                -- суммирование баллов, и группировка вствка новой записи
                -- удаление старых
            exception when no_data_found then
                dbms_output.put_line(r_sct.scores_type||' - ЗАПИСЬ в FINAL = NULL');
            end;
        --end if;
    end loop;
    
    dbms_output.put_line('Накопительные');    
        
    -- СУММА НАЧИСЛЕНИЙ ДО ЭТОГО ДНЯ ПО СТАНДАРТНЫМ
    select nvl(sum(dk_sum), 0) into v_stnd_accrual
    from pos_dk_scores_firm a
    where a.id_dk = i_id_dk
    and a.dk_sum > 0
    and a.doc_type = 'ck'
    and trunc(to_timestamp(i_date, 'dd.mm.yy')) >= trunc(a.dates, 'ddd');
    
    dbms_output.put_line('STND - v_stnd_accrual = '||v_stnd_accrual);
    
    -- СУММА СПИСАНИЙ ДО ЭТОГО ДНЯ ПО СТАНДАРТНЫМ
    select nvl(sum(dk_sum), 0) into v_stnd_deductions
    from pos_dk_scores_firm a
    where a.id_dk = i_id_dk
    and a.dk_sum <= 0
    and (a.doc_type = 'ck' or a.doc_type = 'voz' or a.doc_type = 'ckv')
    and (a.id_ac is null or a.id_ac = 'TEST0009') -- учесть "акцию" списания стандартных через год
    and a.scores_type = 'stnd'
    and trunc(to_timestamp(i_date, 'dd.mm.yy')) >= trunc(a.dates, 'ddd');
    
    dbms_output.put_line('STND - v_stnd_deductions = '||v_stnd_deductions);

    if (v_stnd_from_ac > 0) then
         --:= v_stnd_from_ac + v_stnd_from_ac;
        dbms_output.put_line('v_stnd_result = '||' ( '||v_stnd_from_ac||' + '||v_stnd_accrual||' + '||v_stnd_deductions||')');
        dbms_output.put_line('v_stnd_result = '||(v_stnd_from_ac + v_stnd_accrual + v_stnd_deductions));
    end if;
    
    --begin
        --insert into tem_dk_scores_info(id_dk, scores_type, scores_sum, text, date_burn, dates)
        --values(i_id_dk, r_sct.scores_type, a.summ, a.text, a.date_burn, a.dates);
    --exception when no_data_found then
        --dbms_output.put_line(r_sct.scores_type||' - ЗАПИСЬ в FINAL STND= NULL');
   -- end;
            
    exception when others then
        dbms_output.put_line(sqlcode||sqlerrm);
        rollback;
        WRITE_ERROR_PROC('dk_scores_info', sqlcode, sqlerrm, ' ', 'итоги по баллам для дк', ' ');                 


end dk_scores_info;