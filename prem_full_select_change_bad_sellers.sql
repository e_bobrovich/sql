--select b.tabno, b.fio,  b.stext2, b.stext3, b.dateb, b.dated, d.massn, d.massn_name,d.stat2, d.stat5, d.begda, d.endda, a.*, c.tabno zavmag_tab, c.fio zavmag from bep_prem_seller a
--left join (
--    select tabno,fio,stext2,stext3,dateb,dated from s_seller 
--    where  ((EXTRACT(MONTH FROM dateb) <= COALESCE(i_month, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(i_year, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(i_year, EXTRACT(YEAR FROM sysdate)))
--       and ((EXTRACT(MONTH FROM dated) >= COALESCE(i_month, EXTRACT(MONTH FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(i_year, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(i_year, EXTRACT(YEAR FROM sysdate)))
--) b on a.seller_tab = b.tabno
--left join (
--    select * from (
--        select tabno, fio, orgno from s_seller 
--        where 
--        orgno = lpad(COALESCE(i_id_shop, orgno), 5, '0') and
--        (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
--        and ((EXTRACT(MONTH FROM dateb) <= COALESCE(i_month, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(i_year, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(i_year, EXTRACT(YEAR FROM sysdate)))
--        and ((EXTRACT(MONTH FROM dated) >= COALESCE(i_month, EXTRACT(MONTH FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(i_year, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(i_year, EXTRACT(YEAR FROM sysdate)))
--        --and to_char(dated, 'yy') = '99'
--        order by stext2 desc, dated desc
--    ) where rownum = 1
--) c on lpad(a.id_shop, 5, '0') = c.orgno
--left join (
--    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
--    left join st_seller_massn b on a.massn = b.massn_id
--    where
--        ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) <= COALESCE(i_month, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(i_year, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
--    and ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) >= COALESCE(i_month, EXTRACT(MONTH FROM sysdate))+1 and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(i_year, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
--) d on a.seller_tab = d.pernr
--where a.year = COALESCE(i_year, EXTRACT(YEAR FROM sysdate)) and a.month = COALESCE(i_month, EXTRACT(MONTH FROM sysdate)) 
--and a.id_shop = COALESCE(i_id_shop, a.id_shop) 
--and a.seller_tab != c.tabno
--;

-- ВСЕ КРОМЕ ЗАВМАГА
select b.tabno, b.fio,  b.stext2, b.stext3, b.dateb, b.dated, d.massn, d.massn_name,d.stat2, d.stat5, d.begda, d.endda, a.*, c.tabno zavmag_tab, c.fio zavmag from bep_prem_seller a
left join (
    select a1.tabno,a1.fio,a1.stext2,a1.stext3,a1.dateb,a1.dated,a1.orgno from s_seller a1
    inner join (
        select tabno, min(dated) dated from s_seller
        where  ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and    ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dated = b1.dated
) b on a.seller_tab = b.tabno and lpad(a.id_shop, 5, '0') = b.orgno
left join (
    select * from (
        select tabno, fio, orgno from s_seller 
        where 
        orgno = lpad(COALESCE('0027', orgno), 5, '0') and
        (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
        and ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
        order by stext2 desc, dated desc
    ) where rownum = 1
) c on lpad(a.id_shop, 5, '0') = c.orgno
left join (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    inner join (
    select pernr, min(endda) endda from s_seller_sap a 
    where
        ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) <= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) >= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    group by pernr
    ) c on a.pernr = c.pernr and a.endda = c.endda
) d on a.seller_tab = d.pernr
where a.year = COALESCE(2018, EXTRACT(YEAR FROM sysdate)) and a.month = COALESCE(8, EXTRACT(MONTH FROM sysdate)) 
and a.id_shop = COALESCE('0027', a.id_shop) 
--and a.seller_tab != c.tabno
;

-- ЗАВМАГ
select * from (
    select tabno, fio, orgno from s_seller 
    where 
    orgno = lpad(COALESCE('0027', orgno), 5, '0') and
    (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
    and ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    --and to_char(dated, 'yy') = '99'
    order by stext2 desc, dated desc
) where rownum = 1
;


--ДЕКРЕТ
select * from s_seller_sap a 
left join st_seller_massn b on a.massn = b.massn_id
where
(    ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) )
 or ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) )
) and  pernr --like '%00006482' 
--00006482
--00005887
in (select seller_tab from bep_prem_seller where month = 8 and year = 2018)
and massn in ('06', '07')
;

select * from st_seller_massn;

select a.massn, b.massn_name, a.cnt from 
( select massn, count(1) cnt from s_seller_sap where stat5 = 1 group by massn) a
inner join st_seller_massn b on a.massn = b.massn_id;

--УВОЛЕН(до этого месяца)
select * from s_seller where tabno = '00006482';

select  a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5  from s_seller_sap a
left join st_seller_massn b on a.massn = b.massn_id
--full join s_seller c on a.pernr = c.tabno and begda = to_char(dateb,'yyyymmdd') and endda = to_char(dated,'yyyymmdd')
where pernr like '%00006482' --'%30000369'
order by begda;

select * from (
    select a.pernr, to_date(a.begda, 'yyyymmdd') begda, to_date(a.endda, 'yyyymmdd') endda, a.stat2, a.massn, b.massn_name, a.stat5 from s_seller_sap a
    left join st_seller_massn b on a.massn = b.massn_id
    where
        pernr = '00020967'
    and ((EXTRACT(MONTH FROM to_date(a.begda, 'yyyymmdd')) <= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.begda, 'yyyymmdd')) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and ((EXTRACT(MONTH FROM to_date(a.endda, 'yyyymmdd')) >= COALESCE(8, EXTRACT(MONTH FROM sysdate)) and EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM to_date(a.endda, 'yyyymmdd')) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    order by endda
) where rownum = 1
;



select b.tabno, b.fio, b.stext2, b.stext3, b.dateb, b.dated, c.tabno zavmag_tab, c.fio zavmag, a.* from bep_prem_seller a
left join (
    select tabno,fio,stext2,stext3,dateb,dated from s_seller 
    where  ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
       and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
) b on a.seller_tab = b.tabno
left join (
    select tabno, fio, orgno from s_seller 
    where 
    orgno = lpad(COALESCE('0027', orgno), 5, '0') and
    (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
    and ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and to_char(dated, 'yy') = '99'
) c on lpad(a.id_shop, 5, '0') = c.orgno
where a.year = COALESCE(2018, EXTRACT(YEAR FROM sysdate)) and a.month = COALESCE(8, EXTRACT(YEAR FROM sysdate)) and a.id_shop = COALESCE('0027', a.id_shop) and a.seller_tab != (
    select max(tabno) from s_seller 
    where orgno = lpad(COALESCE('0027', orgno), 5, '0') and
    (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%')
    and ((EXTRACT(MONTH FROM dateb) <= COALESCE(8, EXTRACT(YEAR FROM sysdate)) and EXTRACT(YEAR FROM dateb) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dateb) < COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and ((EXTRACT(MONTH FROM dated) >= COALESCE(8, EXTRACT(YEAR FROM sysdate))+1 and EXTRACT(YEAR FROM dated) = COALESCE(2018, EXTRACT(YEAR FROM sysdate))) or EXTRACT(YEAR FROM dated) > COALESCE(2018, EXTRACT(YEAR FROM sysdate)))
    and to_char(dated, 'yy') = '99'
);

select * from s_seller 
where tabno = '00023098'
and (((EXTRACT(MONTH FROM dateb) >= 8+1 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) > 2018)
and ((EXTRACT(MONTH FROM dated) >= 8 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018))
;


select b.tabno, b.fio, b.stext2, b.stext3, b.dateb, b.dated, a.* from bep_prem_seller a
left join (
    select tabno,fio,stext2,stext3,dateb,dated 
    from s_seller 
    where ((EXTRACT(MONTH FROM dateb) <= 8 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
      and ((EXTRACT(MONTH FROM dated) >= 8 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
) b on a.seller_tab = b.tabno
where a.year = 2018 and a.month = 8
and a.id_shop = '0064'
and not (lower(b.stext2) like '%подсобный рабочий%' or lower(b.stext3) = '%подсобный рабочий%') 
and fio = 'Масленко Ирина Кузьминична' and group_t = 'Сопутка';

select * from ri_stored_proc_error  where sp_name != 'LKA_SET_IP' order by er_time desc;


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

select * from 
