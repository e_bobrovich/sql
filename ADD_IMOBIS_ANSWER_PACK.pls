create or replace PROCEDURE ADD_IMOBIS_ANSWER_PACK (
i_id_mess in A_ARRAY_CHAR, 
i_imobis_answer in A_ARRAY_CHAR
) IS

begin
    for i in 1 .. i_id_mess.count
    loop 
        ------ RF SMS-CODES IMOBIS
        if (get_iddel(i_id_mess(i)) = 0)
        then
            -- ERROR
            if substr(i_imobis_answer(i),1,1) = '-' 
            then
                update delivery_history 
                set error = replace(i_imobis_answer(i),'"',''),
                            id_status = 20
                where id_del = get_iddel(i_id_mess(i)) and id_dk = get_idpos(i_id_mess(i));
                if sql%notfound then
                    insert into delivery_history (id_del, id_dk, error, id_channel, id_status, date_sent, date_done)
                    values(get_iddel(i_id_mess(i)), get_idpos(i_id_mess(i)), i_imobis_answer(i), 4, 20, sysdate, sysdate);
                end if;
                commit;
            --SUCCESS
            else 
                update delivery_history
                set id_status = 21,
                    id_imobis = i_imobis_answer(i)
                where id_del = get_iddel(i_id_mess(i)) and id_dk = get_idpos(i_id_mess(i));
                if sql%notfound then
                    insert into delivery_history (id_del, id_dk, id_imobis, id_channel, id_status, date_sent, date_done)
                    values(get_iddel(i_id_mess(i)), get_idpos(i_id_mess(i)), i_imobis_answer(i), 4, 21, sysdate, sysdate);
                end if;
                commit;
            end if; 
        ------ RB SMS-CODES DEVINO
        elsif (get_iddel(i_id_mess(i)) = 1)
        then 
            -- ERROR
            if length(i_imobis_answer(i)) <= 3 
            then
                update delivery_history 
                set error = replace(i_imobis_answer(i),'"',''),
                            id_status = 20
                where id_del = get_iddel(i_id_mess(i)) and id_dk = get_idpos(i_id_mess(i));
                if sql%notfound then
                    insert into delivery_history (id_del, id_dk, error, id_channel, id_status, date_sent, date_done)
                    values(get_iddel(i_id_mess(i)), get_idpos(i_id_mess(i)), i_imobis_answer(i), 4, 20, sysdate, sysdate);
                end if;
                commit;
            --SUCCESS
            else 
                update delivery_history
                set id_status = 21,
                    id_imobis = i_imobis_answer(i)
                where id_del = get_iddel(i_id_mess(i)) and id_dk = get_idpos(i_id_mess(i));
                if sql%notfound then
                    insert into delivery_history (id_del, id_dk, id_imobis, id_channel, id_status, date_sent, date_done)
                    values(get_iddel(i_id_mess(i)), get_idpos(i_id_mess(i)), i_imobis_answer(i), 4, 21, sysdate, sysdate);
                end if;
                commit;
            end if;
        -- SMS-VIBE IMOBIS
        else
            --SUCCESS
            if lower(i_imobis_answer(i))='ok' 
            then
                update delivery_history
                set id_status = 21
                where id_del = get_iddel(i_id_mess(i)) and id_dk = get_iddk(i_id_mess(i));
                if sql%notfound then
                    insert into delivery_history (id_del, id_dk, id_channel, id_status, date_sent, date_done)
                    values(get_iddel(i_id_mess(i)), get_iddk(i_id_mess(i)), 4, 21, sysdate, sysdate);
                end if;
                commit;
            -- ERROR
            else 
                update delivery_history 
                set error = replace(i_imobis_answer(i),'"',''),
                            id_status = 20
                where id_del = get_iddel(i_id_mess(i)) and id_dk = get_iddk(i_id_mess(i));
                if sql%notfound then
                    insert into delivery_history (id_del, id_dk, id_channel, id_status, error, date_sent, date_done)
                    values(get_iddel(i_id_mess(i)), get_iddk(i_id_mess(i)), 4, 20, i_imobis_answer(i), sysdate, sysdate);
                end if;
                commit;
            end if; 
        end if;


    END LOOP;
    commit;

  exception when others then
   dbms_output.put_line(sqlcode||sqlerrm);
   WRITE_ERROR_PROC('ADD_IMOBIS_ANSWER_PACK', sqlcode, sqlerrm, 'i_id_mess = '||i_id_mess(1)||', i_imobis_answer = '||i_imobis_answer(1));
   --raise;  
   rollback;

END ADD_IMOBIS_ANSWER_PACK;