--insert into delivery_dk

--select 2266, FIRM.ST_DK.ID_DK, 'O' from FIRM.ST_DK
select count(*) from FIRM.ST_DK
where 
((
  (
      (
        ( FIRM.ST_DK.ACTIVATION_DATE >= TO_DATE('01.06.2016', 'DD.MM.YYYY')) 
        and 
        (FIRM.ST_DK.id_dk not in (select id_dk from firm.pos_sale1
          inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
          inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
          where firm.pos_sale1.id_dk != ' ' and 
          FIRM.S_ART.SEASON in ('Зимняя', 'Утепленная') and 
          (firm.pos_sale1.sale_date >= to_date('01.06.2016', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.01.2018', 'dd.mm.yyyy'))
          )) 
          and 
           (FIRM.ST_DK.id_dk in (select firm.pos_sale1.id_dk from firm.pos_sale1
            inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
            where firm.pos_sale1.id_dk != ' ' and 
            (firm.pos_sale1.sale_date >= to_date('01.06.2016', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.01.2018', 'dd.mm.yyyy'))
            group by firm.pos_sale1.id_dk having count(*) >= 2)
          )
      )
  ---
  or 
  (
      (
         (
           FIRM.ST_DK.id_dk in (select id_dk from firm.pos_sale1
           inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
           inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
           where firm.pos_sale1.id_dk != ' ' and 
           FIRM.S_ART.SEASON = 'Зимняя' and 
           (firm.pos_sale1.sale_date >= to_date('01.01.2015', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.05.2016', 'dd.mm.yyyy'))
           )) 
        and 
          (FIRM.ST_DK.id_dk not in (select id_dk from firm.pos_sale1
           inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
           inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
           where firm.pos_sale1.id_dk != ' ' and 
           FIRM.S_ART.SEASON in( 'Зимняя','Утепленная') and 
           (firm.pos_sale1.sale_date >= to_date('01.06.2016', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.01.2018', 'dd.mm.yyyy'))
          )) 
          and 
          (FIRM.ST_DK.id_dk in (select firm.pos_sale1.id_dk from firm.pos_sale1
            inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
            where firm.pos_sale1.id_dk != ' ' and 
            (firm.pos_sale1.sale_date >= to_date('01.06.2016', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.01.2018', 'dd.mm.yyyy'))
            group by firm.pos_sale1.id_dk having count(*) >= 1
          ))
      ) 
      or 
      (
          (FIRM.ST_DK.id_dk not in (select id_dk from firm.pos_sale1
           inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
           inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
           where firm.pos_sale1.id_dk != ' ' and 
           FIRM.S_ART.SEASON in ( 'Зимняя','Утепленная') and 
          (firm.pos_sale1.sale_date >= to_date('01.06.2016', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.01.2018', 'dd.mm.yyyy'))
          )) 
          and 
           (FIRM.ST_DK.id_dk in (select firm.pos_sale1.id_dk from firm.pos_sale1
            inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
            where firm.pos_sale1.id_dk != ' ' and 
            (firm.pos_sale1.sale_date >= to_date('01.06.2016', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.01.2018', 'dd.mm.yyyy'))
            group by firm.pos_sale1.id_dk having count(*) >= 1
          )) 
          and 
           (FIRM.ST_DK.id_dk in (select id_dk from firm.pos_sale1
           inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop
           inner join firm.s_art on firm.pos_sale2.art = firm.s_art.art                 
           where firm.pos_sale1.id_dk != ' ' and 
           FIRM.S_ART.SEASON = 'Утепленная' and 
           (firm.pos_sale1.sale_date >= to_date('01.01.2015', 'dd.mm.yyyy') and firm.pos_sale1.sale_date <= to_date('31.05.2016', 'dd.mm.yyyy'))
          ))
      )
  )
  )
  --and FIRM.ST_DK.id_dk not in (select id_dk from black_list where V_ST_DK = 'T') 
)) 
and FIRM.ST_DK.id_shop in (select a.value1 from delivery_filters a where a.id_del = 2266 and a.field_name = 'ID_SHOP');