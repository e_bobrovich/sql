create or replace PROCEDURE RKV_ALL (V_SHOPID IN varchar2,V_PARAM IN varchar2 , V_RESULTP OUT varchar2) -- УНИВЕРСАЛЬНАЯ ПРОЦЕДУРА ОБРАБОТКИ ЗАПРОСА 035 ИЗ ПРОГРАММЫ ТОВАРОВЕД
 AS                                                                             -- 12/12/2004
V_LINK varchar2(20); -- Имя линка удаленной базы
V_KOP varchar2(3);
V_PNEW varchar2(4048);
V_PARAM1 varchar2(250);
V_SELE varchar2(250);
N NUMBER;
I NUMBER;
V_NUM NUMBER;
v_job_name varchar2(100);
v_job_index number default 0;

TYPE arr_type IS TABLE OF VARCHAR2(250)
 INDEX BY BINARY_INTEGER;
 ARR arr_type;
BEGIN

V_RESULTP:='000'; 
SELECT '@'||NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=V_SHOPID;
V_KOP:=SUBSTR(V_PARAM,1,3);
dbms_output.put_line('V_KOP = '||V_KOP);

IF V_KOP='001' THEN -- ПРОВЕРКА ШТРИХКОДА
 RKV001_LABEL(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- ПРОВЕРКА ШТРИХКОДА

IF  V_KOP='002' THEN -- ЦЕНЫ В ПРИХОД ИЗ РАСХОДА
-------------------------------------------------------------------------------------------
 FILL_SEBEST_TO_PRIXOD(V_SHOPID);
------------------------------------------------------------------------------------------- 
END IF;

IF V_KOP='003' OR V_KOP='004' THEN -- ЭКСПОРТ ПРИХОДА В РАСХОД
 RKV003_PRIXOD(V_SHOPID,V_PARAM);
END IF; -- ПРОВЕРКА ШТРИХКОДА

IF V_KOP='005' THEN -- ЗАПОЛНЕНИЕ ПУСТЫХ ШТРИХКОДОВ
 RKV005_SCAN_UPDATE(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='006' THEN -- СОЗДАНИЕ НОВЫХ ШТРИХКОДОВ НА КОРОБКИ
 RKV006_PRINT_LABEL(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='007' THEN -- СОЗДАНИЕ УНИКАЛЬНЫХ ЭТИКЕТОК НА КОРОБА
 RKV007_UNIC_LABEL(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='008' THEN -- КОПИРОВАНИЕ ДАННЫХ ПО КОРОБАВ В ОБЩУЮ БАЗУ
 RKV008_UPACK_LABEL(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='009' THEN -- ДОБАВИТЬ ТОЛЬКО НОВЫЕ ТРАНЗИТЫ ИЛИ ОБНОВИТЬ ЕСЛИ В ПАРАМЕТРАХ НОМЕР ДОКУМЕНТА
 RKV009_D_SAP_ODGRUZ_ADD(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='010' THEN -- КОПИРОВАНИЕ ДАННЫХ В ЦЕНТРАЛЬНУЮ БАЗУ  (РЕПЛИКАЦИЯ ПО ЗАКРЫТИЮ-ОТКРЫТИЮ ДОКУМЕНТА) 20.02.2015
--RKV010_REPLICATE(V_SHOPID,V_PARAM,V_RESULTP);
V_RESULTP:='OK';	
  
  -- расчет индекса задания для магазина {
  begin
    select  max(to_number(substr(job_name, 13)))
    into v_job_name
    from dba_scheduler_jobs
    where owner = 'FIRM' and  upper(job_name) like  ('RKV_10_'||V_SHOPID||'%') ;
    
    if v_job_name is not null then
      v_job_index := v_job_name +1;
    else   
      v_job_index := 1;
    end if;  
  exception when others then
    v_job_index := 1;
  end;
  -- }
  
   begin
      DBMS_SCHEDULER.drop_job( 'rkv_10_'||V_SHOPID||'_'||v_job_index);
    exception when others then
      null;
    end;

    DBMS_SCHEDULER.CREATE_JOB
    ( job_name      =>  'rkv_10_'||V_SHOPID||'_'||v_job_index
    , program_name  => 'FIRM.RKV010_REPLICATE_PROG'
    --, schedule_name => 'simple_schedule'
    , enabled       => FALSE
  --  ,AUTO_DROP => false
    );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_10_'||V_SHOPID||'_'||v_job_index
    , argument_name  => 'V_KPODR'
    , argument_value => ANYDATA.ConvertVarchar2(V_SHOPID) );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_10_'||V_SHOPID||'_'||v_job_index
    , argument_name  => 'V_PARAM'
    , argument_value => ANYDATA.ConvertVarchar2(V_PARAM) );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_10_'||V_SHOPID||'_'||v_job_index
    , argument_name  => 'V_RESULTP'
    , argument_value => ANYDATA.ConvertVarchar2(V_RESULTP) );

      DBMS_SCHEDULER.ENABLE ( 'rkv_10_'||V_SHOPID||'_'||v_job_index);  

END IF; -- 

IF V_KOP='011' THEN -- НАХОЖДЕНИЕ СЕБЕСТОИМОСТИ В ПОЛНОМ ДВИЖЕНИИ ИЗ ПОСЛЕДНЕГО ДОКУМЕНТА 13.03.2015
RKV011_CENA1(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='012' THEN -- --ОПРЕДЕЛЕНИЕ ПАРАМЕТРОВ СОЕДИНЕНИЯ ПО ЗАПРОШЕННОМУ МАС АДРЕСУ 06.04.2015
RKV012_MAC(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='014' THEN -- --ОБНОВЛЕНИЕ ОСНОВНОЙ СПРАВОЧНОЙ ИНФОРМАЦИИ
V_RESULTP:='OK';	
--RKV014_SPR_INF(V_SHOPID,V_PARAM,V_RESULTP);

   begin
      DBMS_SCHEDULER.drop_job( 'rkv_14_'||V_SHOPID);
    exception when others then
      null;
    end;

    DBMS_SCHEDULER.CREATE_JOB
    ( job_name      =>  'rkv_14_'||V_SHOPID
    , program_name  => 'FIRM.RKV014_SPR_INF_PROG'
    --, schedule_name => 'simple_schedule'
    , enabled       => FALSE
  --  ,AUTO_DROP => false
    );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_14_'||V_SHOPID
    , argument_name  => 'V_SHOPID'
    , argument_value => ANYDATA.ConvertVarchar2(V_SHOPID) );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_14_'||V_SHOPID
    , argument_name  => 'V_PARAM'
    , argument_value => ANYDATA.ConvertVarchar2(V_PARAM) );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_14_'||V_SHOPID
    , argument_name  => 'V_RESULTP'
    , argument_value => ANYDATA.ConvertVarchar2(V_RESULTP) );

      DBMS_SCHEDULER.ENABLE ( 'rkv_14_'||V_SHOPID);  

END IF; -- 

IF V_KOP='015' THEN -- --ФОРМИРОВАНИЕ ПРАЙСА
RKV015_PRICE(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='016' THEN -- --ЗАПОЛНЕНИЕ ОСТАТКОВ ПО POS
RKV016_OST_POS(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='017' THEN -- УДАЛЕНИЕ ДАННЫХ ИЗ ЦЕНТРАЛЬНОЙ БАЗЫ
RKV017_DELETE(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='018' THEN -- ПОДТВЕРЖДЕНИЕ РАСХОДА ПРИХОДА
RKV018_CONFIR(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='019' THEN -- ПРОВЕРКА ОБНОВЛЕНИЙ
RKV019_UPDATE_PRG(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='020' THEN -- ОБНОВЛЕНИЕ ЦЕН
UPDATE_PRICE_ON_SHOP2(V_SHOPID);
V_RESULTP:='OK';

/*   begin
      DBMS_SCHEDULER.drop_job( 'rkv_20_'||V_SHOPID);
    exception when others then
      null;
    end;

    DBMS_SCHEDULER.CREATE_JOB
    ( job_name      =>  'rkv_20_'||V_SHOPID
    , program_name  => 'FIRM.UPDATE_PRICE_ON_SHOP2_PROG'
    --, schedule_name => 'simple_schedule'
    , enabled       => FALSE
  --  ,AUTO_DROP => false
    );

    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      =>  'rkv_20_'||V_SHOPID
    , argument_name  => 'IDIN'
    , argument_value => ANYDATA.ConvertVarchar2(V_SHOPID) );

      DBMS_SCHEDULER.ENABLE ( 'rkv_20_'||V_SHOPID);  */

END IF; -- 
IF V_KOP='021' THEN -- ПРИСВОЕНИЕ КОДА ВНЕШНЕГО КЛИЕНТА
RKV021_KKL(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='022' THEN -- --ИЗМЕНЕНИЕ ДАТЫ НАЧАЛА ПРАЙСА ПО ДОКУМЕНТУ ПРИХОДА 11/02/2016
RKV022_PRICE_DATEB(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='023' THEN -- --ПОЛУЧЕНИЕ ПЛАНОВЫХ ЗАДАНИЙ НА ОТГРУЗКУ
RETURN;	
RKV023_PLANOT(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 
IF V_KOP='024' THEN -- --ФОРМИРОВАНИЕ ПРАЙСА
RKV024_PRICE_RASXOD(V_SHOPID,V_PARAM,V_RESULTP); --ФОРМИРОВАНИЕ ПРАЙСА ПО ДОКУМЕНТУ РАСХОДА КОНТРАГЕНТА 03/10/2016
END IF; -- 
IF V_KOP='025' THEN -- ПОЛНОЕ ОБНОВЛЕНИЕ
V_RESULTP:='OK';
	TDV_COPY_TABLE_FROM_SHOP3@SFEX_R(V_SHOPID,null,'F');   -- обмен из магазина в центр
	TDV_EX_FIRM_TO_SHOP3(V_SHOPID);        -- обмен из центра в магазин
END IF; -- 
IF V_KOP='026' THEN -- --ПРОВЕРКА ВЫПОЛНЯЕТСЯ ЛИ JOB
RKV026_RI_JOB(V_SHOPID,V_PARAM,V_RESULTP); --
END IF; -- 
IF V_KOP='027' THEN -- ---ЭКСПОРТ ВСЕХ ДАННЫХ ОТ РЦ
RKV027_EXPORT_DC(V_SHOPID,V_PARAM,V_RESULTP); --
END IF; -- 


IF V_KOP='D01' THEN -- Подробные данные по ДК
 GET_DK_DETAILS(V_SHOPID,substr(V_PARAM,5));
END IF; -- 

--IF V_KOP='D02' THEN -- Проставление партий по сопутке в излишках
--  tdv_part_sop.set_sebest_to_inv(V_SHOPID);
--END IF; -- 

IF V_KOP='D03' THEN -- Подробные данные по ДК
 tdv_refresh_one_dk(V_SHOPID,substr(V_PARAM,5));
END IF; -- 


IF V_KOP='A01' THEN -- Проверка на уведомления
 KAA_NOTICE_UPDATE(V_SHOPID,V_RESULTP);
END IF; -- 

IF V_KOP='A02' THEN -- Синхронизация уведомлений с магазином
 KAA_NOTICE_SYNCH(V_SHOPID,V_RESULTP);
END IF; -- 

IF V_KOP='A03' THEN -- Синхронизация уведомлений с магазином
 KAA_NOTICE_UPLOAD(V_SHOPID,V_PARAM,V_RESULTP);
END IF; -- 

IF V_KOP='A04' THEN -- Синхронизация уведомлений с магазином
 get_inf_from_shop(V_SHOPID,'D_ZAKAZ');
END IF; -- 

IF V_KOP='A05' THEN -- Проверка погашения ШК купона
 kaa_coupon_status_check(V_SHOPID,substr(V_PARAM,5));
END IF; -- 

IF V_KOP='A06' THEN -- погашение ШК купона
 kaa_coupon_status_close(V_SHOPID,substr(V_PARAM,5));
END IF; -- 

IF V_KOP='A07' THEN -- 22/09/2016 обновление дк и скидок в магазине
 UPDATE_SKIDKI_ON_SHOP(V_SHOPID,'S'||V_SHOPID);
 tdv_ex_firm_to_shop.copy_DK_ON_SHOP(V_SHOPID);
END IF; -- 

IF V_KOP='A08' THEN 
-- TDV_EX_FIRM_TO_SHOP.copy_table(V_SHOPID, 'DK_SYSTEM');
 UPDATE_SKIDKI_ON_SHOP(V_SHOPID,'S'||V_SHOPID);
END IF; -- 

IF V_KOP='A09' THEN 
 kaa_get_current_date(V_SHOPID);
 V_RESULTP:='OK';
END IF; -- 

IF V_KOP='A10' THEN 
 if length(V_PARAM) = 4 then
	 update d_inet_book2 
		set bit_send = 'F' 
		where id_shop not in ('N/A','KI1000')
		and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = V_SHOPID
		and (posnr,serv_ip) in (select a.posnr,a.serv_ip
														from d_inet_book2_v b 
														inner join d_inet_book1 a on a.posnr = b.posnr and a.serv_ip = b.serv_ip
														where a.bit_status = 'T' /*and b.bit_move = 'F'*/ and b.id_shop = V_SHOPID
														group by a.posnr,a.serv_ip
														having sum(case when bit_sale = 'T' then 1 else 0 end) != sum(1));
--	 update d_inet_book1 set bit_send = 'F' where (posnr,serv_ip) in (select posnr,serv_ip from d_inet_book2_v where id_shop not in ('N/A','KI1000') and id_shop = V_SHOPID)
--		and bit_status = 'T';
	 kaa_inet_book.book_exchange(V_SHOPID,null,null,null,null,V_RESULTP);
 else
	--A10~T~00001725~192.8.54.2
	--substr(V_PARAM,5,1) - bit_book
	--substr(V_PARAM,7,8) - posnr
	--substr(V_PARAM,16) - serv_ip
	update d_inet_book2 
		set bit_send = 'F' 
		where case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = V_SHOPID 
			and (posnr,serv_ip) in (select substr(V_PARAM,7,8) posnr, substr(V_PARAM,16) serv_ip
															from dual);
--	update d_inet_book1 
--		set bit_send = 'F' 
--		where (posnr,serv_ip) in (select substr(V_PARAM,7,8) posnr, substr(V_PARAM,16) serv_ip
--															from dual);
	kaa_inet_book.book_exchange(V_SHOPID,substr(V_PARAM,7,8),substr(V_PARAM,16),substr(V_PARAM,5,1),null,V_RESULTP);
 end if;
 V_RESULTP:='OK';
END IF; -- 

IF V_KOP='A11' THEN 
 rts_synch_memo(V_SHOPID);
 V_RESULTP:='OK';
END IF; -- 

IF V_KOP='P01' THEN
  V_RESULTP := '';
  spa_find_art_in_nearest_shops(V_SHOPID, substr(V_PARAM, 5), V_RESULTP);
END IF;

IF V_KOP='P02' THEN
  V_RESULTP := '';
  spa_find_art_by_scan(substr(V_PARAM, 5), V_RESULTP);
END IF;

IF V_KOP='P03' THEN
  V_RESULTP := '';
  spa_find_similar_art_in_shops(V_SHOPID, substr(V_PARAM, 5), V_RESULTP);
END IF;

IF V_KOP='P04' THEN
  V_RESULTP := SPA_GET_IMAGE(substr(V_PARAM, 5));
END IF;

IF V_KOP='P05' THEN
  SPA_UPDATE_PHONE_NUMBER(SUBSTR(SUBSTR(V_PARAM, 5), 1, INSTR(SUBSTR(V_PARAM, 5), '~') - 1), SUBSTR(SUBSTR(V_PARAM, 5), INSTR(SUBSTR(V_PARAM, 5), '~') + 1), V_RESULTP);
END IF;

IF V_KOP='P06' THEN
  V_RESULTP := SPA_GET_DK_LIST_BY_PHONE(SUBSTR(V_PARAM, 5));
  IF (V_RESULTP IS NOT NULL OR V_RESULTP = ' ') THEN
    SELECT COUNT(regexp_replace(regexp_substr(V_RESULTP,'[^,]+', 1, level), '[^[:digit:]]')) AS COUNT_DK INTO V_NUM from dual connect by regexp_substr(V_RESULTP, '[^,]+', 1, level) is not null;
    IF (V_NUM = 1) THEN
      TDV_REFRESH_ONE_DK(V_SHOPID, REGEXP_REPLACE(V_RESULTP, '[^[:digit:]]'));
    ELSE
      SPA_REFRESH_MANY_DK(V_SHOPID, V_RESULTP);
    END IF;
  end if;
end if;

IF V_KOP='E01' THEN
  V_RESULTP := bep_prem_get_day_shop_plan(
                v_shopid,
                cast(substr(v_param, instr(v_param,'~',1,1)+1,(instr(v_param,'~',1,2)-instr(v_param,'~',1,1)-1)) as number),
                cast(substr(v_param, instr(v_param,'~',1,2)+1) as number)
            );
END IF;

END RKV_ALL;