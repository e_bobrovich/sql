select * from st_dk;

select dk_country "СТРАНА", age  "ВОЗРАСТ", sex "ПОЛ", count(*) "КОЛИЧЕСТВО" from (
    select 
    id_dk, fio, activation_date, birthday, dk_country,
    trunc(months_between(to_date('31.05.2019', 'dd.mm.yyyy'), birthday)/12) age, 
    case when sex = 1 then 'М' when sex = 2 then 'Ж' else ' ' end sex
    from st_dk a 
    where bit_block = 'F' and dk_type = 'B' 
    and summ != 0 and sex != ' ' and sex is not null
    and activation_date < to_date('01.06.2019', 'dd.mm.yyyy')
) 
where to_char(birthday, 'yyyymm') < to_char(activation_date, 'yyyymm')
group by age, sex, dk_country order by dk_country, age, sex
;

select * from (
    select 
    id_dk, fio, birthday, activation_date,
    trunc(months_between(to_date('31.05.2019', 'dd.mm.yyyy'), birthday)/12) age, 
    case when sex = 1 then 'М' when sex = 2 then 'Ж' else ' ' end sex
    from st_dk a 
    where bit_block = 'F' and dk_type = 'B' 
    and summ != 0 and sex != ' ' and sex is not null
    and activation_date < to_date('01.06.2019', 'dd.mm.yyyy')
    --and to_char(birthday, 'yyyymm') < to_char(activation_date, 'yyyymm')
) where age < 18 order by age
