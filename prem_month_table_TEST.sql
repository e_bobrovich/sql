select 
    a.hour,
    a."DATES", 
    a."ID_CHEK",
    a."SCAN",
    a."SHOP_ID" id_shop, 
    a."VOP", 
    a."SELLER_TAB", 
    a."SELLER_FIO", 
    a."FACT_SALE_SUM",
    a."FACT_PAIR_KOL",
    a."ART", 
    a."PARTNO",
    a."SH_SOP" , 
    a."OWN_PURCHASE" , 
    a."BRAND", 
    a."CENNIK" , 
    a."ASSORT_TORG", 
    a."GROUPMW" , 
    fact_sale_sum * (
        case when a.sh_sop = 'Обувь' and vop in ('ПРОДАЖА', 'БЕЗНАЛ')
            then 
                (0.2 * (row_number() over (partition by a.id_chek, a.sh_sop order by fact_sale_sum desc) - 1) + 1) * max(nvl(c.percent,d.percent)) 
            else max(nvl(c.percent,d.percent)) end / 100
        ) prem

--select *

from (
    select 
    to_number(to_char(aa."DATES", 'HH24')) "HOUR"
    ,AA."DATES"
    ,aa."SHOP_ID"
    ,aa."ID_CHEK"
    ,aa."SCAN"
    ,AA."VOP"
    ,lpad(AA.SELLER_TAB, 8, '0') "SELLER_TAB"
    ,AA."SELLER_FIO"
    ,cast((AA."SUMP"-AA."SUMV") as number(18,2)) as fact_sale_sum
    ,case when c.mtart not in ('ZROH','ZHW3') then cast((kolp-kolv) as number(18)) else 0 end as fact_pair_kol
    ,cast((KOLP-KOLV) as number(18)) as koll
    ,aa."ART"
    ,aa."PARTNO"
    , case when c.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутка' end as "SH_SOP"
    , case when c.facture = ' ' and c.manufactor != 'СООО БЕЛВЕСТ' then 'Покупная'
           when c.facture = ' ' and c.manufactor = 'СООО БЕЛВЕСТ' then 'Собственная' 
           else c.facture 
      end as "OWN_PURCHASE"
    , c.manufactor as "BRAND"
    , case when "ACTION" = 'F' then 'Белый' else 'Оранжевый' end as "CENNIK" --CHANGE AFTER
    , c.assort_torg as "ASSORT_TORG"
    , c.groupmw as "GROUPMW"
    FROM (
        select 
        extract(hour from a.sale_date) as hour
        ,A.SALE_DATE AS DATES
        ,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
        ,a.id_chek
        ,b.scan
        ,A.SHOP_ID
        ,A.SELLER_FIO
        ,A.SELLER_TAB
        ,B.ART
        ,B.PARTNO
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
        ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
        ,B.ACTION
        FROM POS_SALE2 B
        INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
        where a.bit_close = 'T'
        and to_char(a.sale_date, 'yyyymm') = to_char(sysdate, 'yyyymm')
        --and a.shop_id = decode(i_id_shop, null, a.shop_id, i_id_shop)
    
        UNION ALL
    
        select 
        0 as hour
        ,COALESCE(C2.DATES, A2.date_s) as dates
        ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
        ,a2.id as id_chek
        ,b2.scan
        ,A2.ID_SHOP
        ,B2.SELLER_FIO
        ,B2.SELLER_TAB
        ,b2.art
        ,b2.partno
        ,0 AS SUMP1
        ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
        ,CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
        ,CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
        ,0  AS KOLP 
        ,B2.KOL AS KOLV
        ,B2.ACTION
        FROM D_PRIXOD2 B2
        INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
        --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
        LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                        SELECT p3.ID, p3.ID_SHOP, MAX(p3.DATE_S) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = p3.ID_SHOP GROUP BY p3.ID, p3.ID_SHOP
                        --UNION ALL
                        --SELECT ID_PRIXOD AS ID, ID_SHOP, MAX(DATE_S) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP =  ID_SHOP GROUP BY ID_PRIXOD, ID, ID_SHOP
                        ) WHERE ID != 0 GROUP BY ID, ID_SHOP
                ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
        LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = c.ID_SHOP WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
        WHERE
        ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19',/*'42',*/'45','55')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in ('03','14','19',/*'42',*/'45','55'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.DATED) IS NOT NULL)))
        AND A2.BIT_CLOSE='T' 
        and to_char(a2.dated, 'yyyymm') = to_char(sysdate, 'yyyymm')
        --and a2.id_shop = decode(i_id_shop, null, a2.id_shop, i_id_shop)
    
        UNION ALL
    
        select 
        0 as hour
        ,a.DATED as DATES
        ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
        ,a.id as id_chek
        ,b.scan
        ,b.id_shop
        ,b.SELLER_FIO
        ,b.seller_tab
        ,b.art art
        ,b.partno
        ,0 AS SUMP1
        ,0 AS SUMV1 
        ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
        ,0 AS SUMV 
        ,b.kol AS KOLP
        ,0 AS KOLV
        ,b.ACTION
        from d_rasxod1 a
        inner join (
                select x.id, x.art, x.scan, x.partno, x.seller_fio, seller_tab, x.id_shop, x.action, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2, nvl(x.kol,0) kol
                from (
                    select id, art, scan, partno, seller_fio, seller_tab, id_shop, action, sum(kol*cena3) sum, sum(kol) kol from d_rasxod2 group by id, art, scan, partno, seller_fio, seller_tab, id_shop, action
                ) x
                left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
                ) y on x.id = y.id and x.id_shop = y.id_shop
        ) b on a.id = b.id and a.id_shop = b.id_shop

        where a.bit_close = 'T' and a.idop in ('34','35',/*'37',*/'38','39','40', '44')
        and to_char(a.dated, 'yyyymm') = to_char(sysdate, 'yyyymm')
        --and b.id_shop = decode(i_id_shop, null, b.id_shop, i_id_shop)
    ) aa
    left join s_art c on aa.art=c.art
    where aa.SHOP_ID = '0002' --and aa.id_chek = '78095'
) a

left join (
    select * from (
        select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
        from bep_prem_combination_percent cp
        inner join bep_prem_s_group sg on cp.id_group = sg.id_group
        inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
        inner join st_retail_hierarchy rh on gs.id_shop = rh.shopid
        left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
        left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
        where sg.year = to_char(sysdate, 'yyyy') and sg.month = to_char(sysdate, 'mm') 
        --and id_shop = decode(i_id_shop, null, id_shop, i_id_shop)
        and cp.id_combination != 0
        group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
    ) a
) c on
    a.shop_id = c.id_shop
    and trunc(a.dates) >= case when c.dateb is not null then c.dateb else trunc(a.dates) end
    and trunc(a.dates) <= case when c.datee is not null then c.datee else trunc(a.dates) end
    and case when c.ids_criterion like '%'||1||'%' then c.combination else a.sh_sop end like '%'||a.sh_sop||'%' 
    and case when c.ids_criterion like '%'||2||'%' then c.combination else a.cennik end like '%'||a.cennik||'%'  
    and case when c.ids_criterion like '%'||3||'%' then c.combination else a.own_purchase  end like '%'||a.own_purchase ||'%'  
    and case when c.ids_criterion like '%'||4||'%' then c.combination else a.brand end like '%'||a.brand||'%'
    and case when c.ids_criterion like '%'||5||'%' then c.combination else a.assort_torg end like '%'||a.assort_torg||'%'
    and case when c.ids_criterion like '%'||6||'%' then c.combination else a.groupmw end like '%'||a.groupmw||'%'

left join (
    select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    inner join st_retail_hierarchy rh on gs.id_shop = rh.shopid
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
    where sg.year = to_char(sysdate, 'yyyy') and sg.month = to_char(sysdate, 'mm') 
    and sg.dateb is null and sg.datee is null
    --and id_shop = decode(i_id_shop, null, id_shop, i_id_shop)
    and cp.id_combination = 0
    group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
) d on a.shop_id = d.id_shop

--where a.shop_id = decode(i_id_shop, null, a.shop_id, i_id_shop)
group by a.hour, a."DATES", a."ID_CHEK", a."SCAN" , a."SHOP_ID", a."VOP", a."SELLER_TAB", 
    a."SELLER_FIO", a."FACT_SALE_SUM", a."ART", a.partno, a."SH_SOP", a."OWN_PURCHASE" , 
    a."BRAND", a."CENNIK", a."FACT_PAIR_KOL", a."ASSORT_TORG", a."GROUPMW"
   -- having count(*) > 1
;
   
select * from d_prixod2 b2
        INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop where b2.id_shop = '3003' and b2.id = 158 and b2.art = '333097';
        
select b2.id, b2.id_shop, b2.art, b2.id_rec, count(*) from d_prixod2 b2
        inner join d_prixod1 a2 on a2.id=b2.id and a2.id_shop = b2.id_shop --where b2.id_shop = '3003'
group by b2.id, b2.id_shop, b2.art, b2.id_rec having count(*) > 1;