select replace(substr(a.shopnum, 2, 5) ,'05200', '24100') as shopnum , a.shopid from firm.s_shop a  
where a.shopid in (
  select fl.shopid from delivery_dk dh
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
  where dh.id_del in (2035,2033,2032,2031,2030)
  group by fl.shopid
) order by replace(substr(a.shopnum, 2, 5),'05200', '24100');

select trunc(to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS'),'DDD') as date1, 
       trunc(to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS'),'DDD') + 6 as date2 
from tech_data a
inner join delivery_pool b on a.id_del = b.id_del
where a.id_del in (2035,2033,2032,2031,2030)
and b.status in ('DONE', 'PROCESS')
order by to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS');

select a.id_del from tech_data a
inner join delivery_pool b on a.id_del = b.id_del
where 
trunc(to_date(a.start_date, 'DD.MM.YYYY HH24:MI:SS')) between
  to_date('  .  .    ', 'DD.MM.YYYY') and to_date('  .  .    ', 'DD.MM.YYYY')
and b.status in ('DONE', 'PROCESS');

select shopparent, sum(cnt) as cnt from ( select replace(b.shopparent, '25F2', '44F1') as shopparent, count(dd.id_dk) as cnt from delivery_dk dd
 left join firm.st_dk sd on dd.id_dk = sd.id_dk
 inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
 where dd.id_del in (2035,2033,2032,2031,2030)
 and
 b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','32F1','33F1','37F1','40F1','43F1','25F2','44F1','45F1')
 and (dd.groupt = 'O' or dd.groupt is null)
 group by b.shopparent
 order by replace(b.shopparent, '25F2', '44F1'))
group by shopparent
order by shopparent;

countSatus
select replace(vd.shopparent, '25F2', '44F1') as shopparent, (nvl(vd.colvo, 0) + nvl(vr.colvo, 0)) as viber_d, nvl(vr.colvo, 0) as viber_r, nvl(sd.colvo, 0) as sms_d from (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
where dh.id_del in (2035,2033,2032,2031,2030) 
and
b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','32F1','33F1','37F1','40F1','43F1','25F2','44F1','45F1')
  and (dh.id_status = 5)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) fil 
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2035,2033,2032,2031,2030) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','32F1','33F1','37F1','40F1','43F1','25F2','44F1','45F1')
  and (dh.id_status = 5)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) vd on fil.shopparent = vd.shopparent
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2035,2033,2032,2031,2030) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','32F1','33F1','37F1','40F1','43F1','25F2','44F1','45F1')
  and (dh.id_status = 6)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) vr on fil.shopparent = vr.shopparent
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2035,2033,2032,2031,2030) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','32F1','33F1','37F1','40F1','43F1','25F2','44F1','45F1')
  and (dh.id_status = 5)
  and dh.id_channel = 2
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) sd on vd.shopparent = sd.shopparent order by replace(vd.shopparent, '25F2', '44F1');
---------------------------------------
select replace(substr(a.shopnum, 2, 5) ,'05200', '24100') as shopnum , a.shopid from firm.s_shop a  
where a.shopid in (
  select fl.shopid from delivery_dk dh
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
  where dh.id_del in (2035,2033,2032,2031,2030,2029)
  group by fl.shopid
) order by replace(substr(a.shopnum, 2, 5),'05200', '24100');

select substr(a.shopnum, 2, 5) as shopnum , a.shopid from firm.s_shop a  
where a.shopid in (
  select fl.shopid from delivery_dk dh
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
  where dh.id_del in (2035,2033,2032,2031,2030,2029)
  group by fl.shopid
) order by substr(a.shopnum, 2, 5);

select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy b order by shopparent;
