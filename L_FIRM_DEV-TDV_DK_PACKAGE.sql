procedure scores_clear_after_year(i_dk in varchar2 default null, i_date in timestamp default systimestamp) is   
    v_scores_day_acc integer := 0; -- количество начисленных очков в день год назад(accrual)
    v_date_first_chek timestamp; -- дата и время первого чека год назад
    v_scores_before_sum integer := 0; -- сумма баллов по день(год назад) включительно(summary)
    v_scores_year_ded integer := 0; -- сумма вычетов в течении года (deductions)
    v_scores_aft_check_day integer := 0; -- сумма вычетов в течении дня после ччека (deductions)
  begin 
    -- проход по дисконтным картам с начислениями баллов по чеку
    --(количество обычных баллов начисленных в день год назад и дата\время первого чека)
    for r_dk in (
      select id_dk, nvl(sum(dk_sum), 0) as scores_acc, nvl(min(dates), i_date) as date_chek
      from tdv_scores_split2
      where dk_sum > 0
      and (doc_type = 'ck' or (doc_type = 'ac' and id_chek = 'SC100012' and id_ac is null) or (doc_type in ('voz', 'ckv') and id_ac is null))
      and id_dk = case when i_dk is null then id_dk else i_dk end
      and  trunc(add_months(i_date, -12), 'ddd') = trunc(dates, 'ddd') -- 12 месяцев назад
      and id_dk not in (select id_dk from pos_dk_scores where id_ac = 'STND0365' and trunc(dates) = trunc(i_date))
      -----
      and(
          doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
          and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
          and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      group by id_dk         
    ) loop
    
      -- сумма баллов в день год назад
      select nvl(sum(dk_sum), 0) into v_scores_before_sum from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum <> 0
      and (((doc_type = 'ck' or (doc_type = 'ac' and id_chek = 'SC100012' and id_ac is null)  or (doc_type in ('voz', 'ckv') and id_ac is null )) and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and trunc(r_dk.date_chek, 'ddd') >= trunc(dates, 'ddd')       
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
      order by dates;
      
      -- сумма баллов в день год назад после первого начисления
      select nvl(sum(dk_sum), 0) into v_scores_aft_check_day  from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum < 0
      and (((doc_type = 'ck' or (doc_type = 'ac' and id_chek = 'SC100012' and id_ac is null)  or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and dates > r_dk.date_chek -- после первого чека
      and dates <= trunc(r_dk.date_chek)+1 -- в течении дня
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      ;
      -----
      
      v_scores_before_sum := v_scores_before_sum - v_scores_aft_check_day;
      --dbms_output.put_line('id = '||r_dk.id_dk||' v_scores_before_sum - v_scores_aft_check_day*-1 = '||v_scores_before_sum||' - '||v_scores_aft_check_day||' = '||(v_scores_before_sum - v_scores_aft_check_day*-1));
      
      
      -- сумма вычетов за год после первого чека
      select nvl(sum(dk_sum), 0) into v_scores_year_ded from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum < 0
      and (((doc_type = 'ck'  or (doc_type = 'ac' and id_chek = 'SC100012' and id_ac is null) or (doc_type in ('voz', 'ckv') and id_ac is null )) and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and dates > r_dk.date_chek
      and dates <= i_date --текущая дата
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
      order by dates;
      
      -- если разность суммы начисленных год назад и суммы использованных больше 0
      --dbms_output.put('id = '||r_dk.id_dk||' scores_acc = '||r_dk.scores_acc||' v_scores_before_sum = '||v_scores_before_sum||' v_scores_year_ded = '||v_scores_year_ded||' v_scores_before_sum - v_scores_year_ded*-1 = '||(v_scores_before_sum - v_scores_year_ded*-1));
      if (v_scores_before_sum - v_scores_year_ded*-1 > 0) then

        -- то добавить запись о вычете неиспольщованных 

        --dbms_output.put('insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'');');
        
        insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(v_scores_before_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365');
        
        --dbms_output.put('insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'', ''TEST0009'');');
        
        insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(v_scores_before_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365', 'stnd');
        
        --commit;
      end if;
    end loop;
  --exception
end scores_clear_after_year;
--------------------------------------- 
 
   --*************************************************************
  -- списание стандартных баллов, которые были начсилены год назад и ранее
  --*************************************************************    
  ---------------------------------------
  -- Bobrovich 26.10.17 (upd 30.10.17)
  ----- сгорание стандартных баллов -----
 procedure scores_clear_after_year_other(i_dk in varchar2 default null, i_date in timestamp default systimestamp) is   
    v_scores_day_acc integer := 0; -- количество начисленных очков в день год назад(accrual)
    v_date_first_chek timestamp; -- дата и время первого чека год назад
    v_scores_before_sum integer := 0; -- сумма баллов по день(год назад) включительно(summary)
    v_scores_year_ded integer := 0; -- сумма вычетов в течении года (deductions)
    v_scores_aft_check_day integer := 0; -- сумма вычетов в течении дня после ччека (deductions)
    
    v_date_f_chek timestamp;
  begin 
    -- проход по дисконтным картам с начислениями баллов по чеку
    --(количество обычных баллов начисленных в день год назад и дата\время первого чека)
    for r_dk in (
      select id_dk, nvl(sum(dk_sum), 0) as /*scores_acc*/ scores_sum, nvl(max(dates), i_date) as date_chek
      from tdv_scores_split2
      --where dk_sum > 0
      where dk_sum <> 0
      and (doc_type = 'ck'  or (doc_type = 'ac' and id_chek = 'SC100012' and id_ac is null) or (doc_type in ('voz', 'ckv') and id_ac is null))
      and id_dk = case when i_dk is null then id_dk else i_dk end
      and  trunc(add_months(i_date, -12), 'ddd') >= trunc(dates, 'ddd') -- 12 месяцев назад
      and id_dk not in (select id_dk from pos_dk_scores where id_ac = 'STND0365')
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----

      group by id_dk         
    ) loop
      --dbms_output.put_line('v_date_f_chek = '||to_char(r_dk.date_chek, 'dd.mm.yyyy hh24:mi:ss'));

      -- сумма вычетов за год после первого чека
      select nvl(sum(dk_sum), 0) into v_scores_year_ded from tdv_scores_split2 a
      where id_dk = r_dk.id_dk
      and dk_sum < 0
      --and (doc_type = 'ck' or (doc_type in ('voz', 'ckv') and id_ac is null))
      and (((doc_type = 'ck'  or (doc_type = 'ac' and id_chek = 'SC100012' and id_ac is null) or (doc_type in ('voz', 'ckv') and id_ac is null ))and scores_type = 'stnd') or a.id_ac = 'STND0365')
      and dates > r_dk.date_chek
      and dates <= i_date --текущая дата
      -----
      and(
      doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
      and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
      and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
      )
      -----
      order by dates;
      
      -- если разность суммы начисленных год назад и суммы использованных больше 0
      --dbms_output.put_line('id = '||r_dk.id_dk||' scores_acc = '||r_dk.scores_acc||' v_scores_before_sum = '||v_scores_before_sum||' v_scores_year_ded = '||v_scores_year_ded||' v_scores_before_sum - v_scores_year_ded*-1 = '||(v_scores_before_sum - v_scores_year_ded*-1));
      --if (v_scores_before_sum - v_scores_year_ded*-1 > 0) then
      if (r_dk.scores_sum - v_scores_year_ded*-1 > 0) then

        --dbms_output.put('insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'');');
        
        insert into pos_dk_scores z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac)        
        values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(r_dk.scores_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365');
        
        --dbms_output.put('insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        --values ('||r_dk.id_dk||', ''ac'', ''TEST0009'', null, '||((-1)*(v_scores_before_sum + v_scores_year_ded))||', systimestamp, ''FIRM'', ''F'', ''TEST0009'', ''TEST0009'');');
        
        --insert into pos_dk_scores_firm z (id_dk, doc_type, id_chek, id_sale, dk_sum, dates, id_shop, bit_open_check, id_ac, scores_type)        
        --values (r_dk.id_dk, 'ac', 'STND0365', null, ((-1)*(r_dk.scores_sum + v_scores_year_ded)), i_date, 'FIRM', 'F', 'STND0365', 'stnd');
        
        --commit;
      end if;
    end loop;
  --exception
end scores_clear_after_year_other;
--------------------------------------- 