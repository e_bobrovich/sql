create or replace package body calculation_day_tables is

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 06.03.2018 11:51:27
	-- Comments:
	--
	--****************************************************************************************************************************************************
	procedure t_day_shops_sales1 is
	begin
		delete from t_day_shops_sales1;
	
		insert into t_day_shops_sales1
			select id_shop, art, season, analog, asize, shoes_flag, sum(kol_sale), sum(kol_sale_last_year)
			from (select a.id_shop, b.art, c.season, nvl(d.analog, b.art) analog, b.asize,
										case
											 when scan != ' ' then
												'T'
											 else
												null
										 end shoes_flag, 0 kol_sale, sum(b.kol) kol_sale_last_year
						 from pos_sale1 a
						 inner join pos_sale2 b on a.id_chek = b.id_chek
																			 and a.id_shop = b.id_shop
						 inner join s_art c on b.art = c.art
						 left join TDV_MAP_NEW_ANAL d on b.art = d.art
						 where a.bit_close = 'T'
									 and a.bit_vozvr = 'F'
									 and scan != ' ' --только обувь
									 and trunc(a.sale_date) >= ADD_MONTHS(sysdate, -12) -- период за год
									 and a.id_shop in (select shopid
																		 from st_shop z
																		 where z.org_kod = 'SHP')
						 group by a.id_shop, d.analog, b.art, b.asize, c.season,
											case
												when scan != ' ' then
												 'T'
												else
												 null
											end
						 union
						 select a.id_shop, b.art, c.season, nvl(d.analog, b.art) analog, b.asize,
										case
											 when scan != ' ' then
												'T'
											 else
												null
										 end shoes_flag, sum(b.kol) kol_sale, 0 kol_sale_last_year
						 from pos_sale1 a
						 inner join pos_sale2 b on a.id_chek = b.id_chek
																			 and a.id_shop = b.id_shop
						 inner join s_art c on b.art = c.art
						 left join TDV_MAP_NEW_ANAL d on b.art = d.art
						 where a.bit_close = 'T'
									 and a.bit_vozvr = 'F'
									 and scan != ' ' --только обувь
									 and a.id_shop in (select shopid
																		 from st_shop z
																		 where z.org_kod = 'SHP')
						 group by a.id_shop, d.analog, b.art, b.asize, c.season,
											case
												when scan != ' ' then
												 'T'
												else
												 null
											end)
			group by id_shop, art, season, analog, asize, shoes_flag;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 11.03.2018 22:41:08
	-- Comments:
	--
	--****************************************************************************************************************************************************
	procedure t_day_shops_ost1 is
	begin
		delete from t_day_shops_ost1;
	
		insert into t_day_shops_ost1
			select a.id_shop, a.art, a.asize, b.season, sum(a.kol) kol
			from e_osttek a
			left join s_art b on a.art = b.art
			left join pos_brak x on a.scan = x.scan
															and a.id_shop = x.id_shop
			where a.scan != ' ' -- только обувь
						and a.id_shop in (select shopid
															from st_shop a
															where a.org_kod = 'SHP')
						and a.procent = 0 -- без уценки
						and x.scan is null -- не брак
			group by a.id_shop, a.art, a.asize, b.season
			having sum(kol) > 0;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 11.03.2018 23:26:50
	-- Comments:
	--
	--****************************************************************************************************************************************************
	procedure t_day_trans_in_way1 is
	begin
		delete from t_day_trans_in_way1;
	
		insert into t_day_trans_in_way1
			select a1.shopid id_shop, a1.art, a1.asize, sum(a1.kol) kol
			from trans_in_way a1
			where scan != ' '
						and a1.shopid in (select shopid
															from st_shop z
															where z.org_kod = 'SHP')
			group by a1.shopid, a1.art, a1.asize
			having sum(kol) > 0;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 11.03.2018 23:33:25
	-- Comments:
	--
	--****************************************************************************************************************************************************
	procedure t_day_shops_prixod1 is
	begin
		delete from t_day_shops_prixod1;
	
		insert into t_day_shops_prixod1
			select distinct a.id_shop, b.art, b.asize, c.season, sum(kol) kol
			from d_prixod1 a
			inner join d_prixod2 b on a.id = b.id
																and a.id_shop = b.id_shop
			left join s_art c on b.art = c.art
			where a.idop in ('01', '04', '08', '07')
						and scan != ' '
						and a.id_shop in (select shopid
															from st_shop z
															where z.org_kod = 'SHP')
			group by a.id_shop, b.art, b.asize, c.season
			having sum(kol) > 0;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 13.03.2018 22:27:01
	-- Comments:
	--
	--****************************************************************************************************************************************************
	procedure start_proc_calculate(i_proc       varchar2,
																 i_table_name varchar2) is
		v_table_name varchar2(50 char);
		v_str varchar2(500 char);
	begin
    v_table_name := i_table_name;
		if i_table_name is null then
			v_table_name := i_proc;
		end if;
	
		insert into t_day_calculate_result
		values
			(i_proc, v_table_name, systimestamp, null, null);
		
    commit;
    
		execute immediate 'call calculation_day_tables.'||i_proc;
	
		update t_day_calculate_result
		set time_end = systimestamp, calc_result = 'ok'
		where proc_name = i_proc
					and table_name = v_table_name
					and time_start = (select max(time_start)
														from t_day_calculate_result);
		commit;
	exception
		when others then
			rollback;
		
			v_str := sqlcode || sqlerrm;
		
			update t_day_calculate_result
			set time_end = systimestamp, calc_result = v_str
			where proc_name = i_proc
						and table_name = v_table_name
						and time_start = (select max(time_start)
															from t_day_calculate_result);
			commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 06.03.2018 11:51:21
	-- Comments:
	--
	--****************************************************************************************************************************************************
	procedure go is
	begin
		start_proc_calculate('t_day_shops_sales1()', 't_day_shops_sales1');
		start_proc_calculate('t_day_shops_ost1()', 't_day_shops_ost1');
		start_proc_calculate('t_day_trans_in_way1()', 't_day_trans_in_way1');
		start_proc_calculate('t_day_shops_prixod1()', 't_day_shops_prixod1');
		/*    t_day_shops_sales1;
    t_day_shops_ost1;
    t_day_trans_in_way1;
    t_day_shops_prixod1;*/
	end;

end calculation_day_tables;
