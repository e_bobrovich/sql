SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor cur is 
  select shopid from st_shop where 
  substr(shopid,1,2) in ('27')
  and bit_open = 'T' and org_kod = 'SHP'
;
curRow cur%rowtype;
p_shopid varchar2(20 CHAR);
BEGIN
 
open cur;
  loop  
       FETCH cur INTO p_shopid;
       EXIT WHEN cur%NOTFOUND;  
          begin
          
					begin
						DBMS_SCHEDULER.drop_job( 'bep_j_mass_script_'||p_shopid);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'bep_j_mass_script_'||p_shopid,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
BEGIN 
update st_dk@S]'||p_shopid||q'[
set fio = ' ', phone_number = ' ', fam = ' ', ima = ' ', otch = ' ', bit_black_list = 'T', bit_block = 'T'
where id_dk = '0000030572010'
commit;
END;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('bep_j_mass_script_'||p_shopid); 
					
          commit;
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
  
END ;