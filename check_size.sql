-------------------------------
SELECT
  df.tablespace_name "Tablespace",
  totalusedspace "Used MB",
  (df.totalspace - tu.totalusedspace) "Free MB",
  df.totalspace "Total MB",
  ROUND(100 * ( (df.totalspace - tu.totalusedspace)/ df.totalspace))
  "Pct. Free"
FROM
  (SELECT tablespace_name,
  ROUND(SUM(bytes) / 1048576) TotalSpace
  FROM dba_data_files
  GROUP BY tablespace_name) df,
 
  (SELECT ROUND(SUM(bytes)/(1024*1024)) totalusedspace, tablespace_name
   FROM dba_segments
   GROUP BY tablespace_name) tu
WHERE df.tablespace_name = tu.tablespace_name ;

select 
   segment_name           table_name,	   
   sum(bytes)/(1024*1024) table_size_meg 
   
from   
   user_extents a 
where  
   segment_type='TABLE' 
group  by segment_name  
order by sum(bytes)/(1024*1024) desc;

select a.owner, a.index_name, b.file_name 
from dba_indexes a, dba_data_files b 
where a.tablespace_name=b.tablespace_name and owner = 'IMOB';

