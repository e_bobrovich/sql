select cp.id_combination, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_combination
from bep_prem_combination_percent cp
inner join bep_prem_s_group sg on cp.id_group = sg.id_group
inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination
left join BEP_PREM_S_CRITERION gc on cc.id_criterion = gc.id_criterion

where gs.id_shop = '0001' and sg.year = coalesce(2018, extract(year from sysdate)) and sg.month = coalesce(11, extract(month from sysdate))
and cp.id_combination != 0
group by cp.id_combination
order by cp.id_combination;


select sg.id_group, cp.year, cp.month, cp.id_combination, case when cp.id_combination = 0 then 'Остальное' else listagg(cc.value,'/') within group(order by cc.id_criterion) end combination, cp.percent
from bep_prem_s_group sg
left join bep_prem_combination_percent cp on sg.id_group = cp.id_group
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion 
where sg.id_group = 1
group by sg.id_group, cp.year, cp.month, cp.id_combination, cp.percent
order by cp.id_combination;

select a.id_shop||' - '||b.shopname from bep_prem_group_shop  a
left join firm.st_shop b on a.id_shop = b.shopid
where id_group = 1;


SELECT DISTINCT 'F' AS BIT_SELE, a.shopid, a.shopname FROM ST_SHOP a INNER JOIN st_retail_hierarchy b ON a.shopid = b.shopid WHERE b.SHOPPARENT IN ('00F1') AND a.ORG_KOD = 'SHP'
AND A.SHOPID NOT IN 
(
    SELECT GS.ID_SHOP FROM BEP_PREM_S_GROUP SG
    INNER JOIN BEP_PREM_GROUP_SHOP GS ON SG.ID_GROUP = GS.ID_GROUP
    WHERE SG.YEAR = 2018 AND SG.MONTH = 11 AND SG.ID_GROUP != 1
)
ORDER BY a.shopid;


select cp.percent from bep_prem_s_group sg
inner join bep_prem_group_shop gs on sg.id_group = gs.id_group
left join bep_prem_combination_percent cp on sg.id_group = cp.id_group 
where sg.year = 2018 and sg.month = 11 and gs.id_shop = '0001' and cp.id_combination = 1
; 

select cp.percent from (select * from bep_prem_s_group where year = 2018 and month = 11) sg
inner join (select * from bep_prem_group_shop where id_shop = '0001') gs on sg.id_group = gs.id_group
left join (select * from bep_prem_combination_percent where id_combination = 1) cp on sg.id_group = cp.id_group 
; 


SELECT DISTINCT 'F' AS BIT_SELE, a.shopid, a.shopname FROM ST_SHOP a INNER JOIN st_retail_hierarchy b ON a.shopid = b.shopid WHERE b.SHOPPARENT IN ('00F1') AND a.ORG_KOD = 'SHP'
AND A.SHOPID NOT IN 
(SELECT GS.ID_SHOP FROM BEP_PREM_S_GROUP SG
INNER JOIN BEP_PREM_GROUP_SHOP GS ON SG.ID_GROUP = GS.ID_GROUP
WHERE SG.YEAR = 2018 AND SG.MONTH = 12) ORDER BY a.shopid;

delete from bep_prem_group_shop where id_group = 1;

select ('0001','0002','0003','0004','0005','0008','0009','0014') from dual;

select BEP_PREM_S_GROUP_ID.nextval as id_group from dual;
insert into bep_prem_s_group(id_group, year, month) values(23, 2018, 11);
insert into bep_prem_combination_criterion(id_group, id_combination, percent) values(24, 0, 0);
insert into bep_prem_combination_percent(id_group, id_combination, percent) 
values(29, nvl((select max(id_combination)+1 from bep_prem_combination_percent where id_group = 29 group by id_group),1) ,0);
select * from bep_prem_combination_percent where id_group = 29;
;

select * from bep_prem_combination_criterion;

select cc.id_combination, gc.id_criterion, gc.description, cc.value from bep_prem_combination_criterion cc
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
where cc.id_combination = 1 and cc.id_combination != 0;