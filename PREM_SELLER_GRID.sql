select replace('qwe''qwe', '''') from dual;

select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 1 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 1 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 12
order by a.id_shop, a.year, a.month, a.seller_tab;

---------------------------

select * from bep_prem_seller a
--left join (
--    select id_shop , year , month , 
--    plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end sale_percent,
--    plan_pair, nvl(fact_pair, 0) fact_pair  , case when plan_pair !=0 then round(100*nvl(fact_pair, 0)/plan_pair, 2) else 0 end pair_percent
--    from bep_prem_shop_plan
--) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 12 and a.id_shop = '0004'
order by a.id_shop, a.year, a.month, a.seller_tab
;

select a.year, a.month, a.id_shop, 
null shop_plan_to, null shop_fact_sale_, null shop_fact_correction_to, null shop_plan_sale_percent,
a.seller_tab, a.seller_fio, 'ИТОГО' group_t ,sum(a.fact_sum) fact_sum/*, sum(fact_pair_kol) fact_pair_kol*/, sum(a.prem_individ_group) prem_individ_group from bep_prem_seller a 
where a.year = 2018 and a.month = 12 and a.id_shop = '0004'
group by a.year, a.month, a.id_shop, a.seller_tab, a.seller_fio
order by a.year, a.month, a.id_shop, a.seller_tab, a.seller_fio
;

---------------------------------------------
--ПОЛНЫЙ ЗАПРОС
---------------------------------------------
select  q.year, q.month, q.id_shop, 
cast(q.shop_plan_to as number(18,2)) shop_plan_to, cast(q.shop_fact_sale as number(18,2)) shop_fact_sale, cast(q.shop_fact_correction_to as number(18,2)) shop_fact_correction_to, cast(q.shop_plan_sale_percent as number(18,2)) shop_plan_sale_percent,
case when shop_plan_sale_percent >= 100 then 'Перевыполнен' when shop_plan_sale_percent >= 90 and shop_plan_sale_percent < 100 then 'Выполнен' when shop_plan_sale_percent < 90 then 'Не выполнен' else null end is_completed,
q.seller_tab, q.seller_fio, q.group_t, q.period, 
cast(q.fact_sum as number(18,2)) fact_sum, 
cast(q.prem_individ_group as number(18,2)) prem_individ_group, 
cast(q.prem_plan_full as number(18,2)) prem_plan_full, 
cast(q.prem_full as number(18,2)) prem_full
from (
    select 1 priority, x.year, x.month, x.id_shop, 
    null shop_plan_to, null shop_fact_sale, null shop_fact_correction_to, null shop_plan_sale_percent,
    x.seller_tab, x.seller_fio, x.group_t, 
    case when x.dateb is not null and x.datee is not null then to_char(x.dateb, 'dd.mm.yy')||' - '||to_char(x.datee, 'dd.mm.yy') else null end as period, 
    x.fact_sum, x.prem_individ_group, null prem_plan_full, null prem_full
    from bep_prem_seller x
    where x.year = 2018 and x.month = 12 and x.id_shop = case when '0004' is null then x.id_shop else '0004' end -- and x.id_shop = '0004'
    
    union all
    
    select 2 priority, a.year, a.month, a.id_shop, 
    null shop_plan_to, null shop_fact_sale, null shop_fact_correction_to, null shop_plan_sale_percent,
    a.seller_tab, a.seller_fio, 'ИТОГО' group_t ,
    null period, 
    sum(a.fact_sum) fact_sum, sum(a.prem_individ_group) prem_individ_group, sum(a.prem_plan_group) prem_plan_full, 
    sum(a.prem_individ_group) + sum(a.prem_plan_group) prem_full
    from bep_prem_seller a
    where a.year = 2018 and a.month = 12 and a.id_shop = case when '0004' is null then a.id_shop else '0004' end -- and id_shop = '0004'
    group by a.year, a.month, a.id_shop, a.seller_tab, a.seller_fio
    
    union all
    
    select 3 priority, b.year, b.month, b.id_shop, 
    b.plan_sum shop_plan_to, nvl(b.fact_sum, 0) shop_fact_sale, null shop_fact_correction_to, case when b.plan_sum !=0 then round(100*nvl(b.fact_sum, 0)/b.plan_sum, 2) else 0 end shop_plan_sale_percent,
    null seller_tab, null seller_fio, null group_t,
    null period,
    nvl(b.fact_sum, 0) fact_sum, b1.prem_individ_group prem_individ_group, b1.prem_plan_full prem_plan_full, b1.prem_full prem_full
    from bep_prem_shop_plan b
    left join
    (
        select year, month, id_shop,
        sum(fact_sum) fact_sum, sum(prem_individ_group) prem_individ_group, sum(prem_plan_group) prem_plan_full, 
        sum(prem_individ_group) + sum(prem_plan_group) prem_full
        from bep_prem_seller 
        where year = 2018 and month = 12 and id_shop = case when '0004' is null then id_shop else '0004' end 
        group by year, month, id_shop 
    ) b1 on b1.year = b.year and b1.month = b.month and b1.id_shop = b.id_shop
    where b.year = 2018 and b.month = 12 and b.id_shop = case when '0004' is null then b.id_shop else '0004' end

--    select 3 priority, b.year, b.month, b.id_shop, 
--    b.plan_sum shop_plan_to, nvl(b.fact_sum, 0) shop_fact_sale, null shop_fact_correction_to, case when b.plan_sum !=0 then round(100*nvl(b.fact_sum, 0)/b.plan_sum, 2) else 0 end shop_plan_sale_percent,
--    null seller_tab, null seller_fio, null group_t,
--    null period,
--    null fact_sum, null prem_individ_group, null prem_plan_full, null prem_full
--    from bep_prem_shop_plan b
--    where b.year = 2018 and b.month = 12 and b.id_shop = case when '0004' is null then b.id_shop else '0004' end-- and b.id_shop = '0004'
) q
order by q.year, q.month, q.id_shop, q.seller_tab, q.priority, q.period, q.group_t
;

select 3 priority, b.year, b.month, b.id_shop, 
b.plan_sum shop_plan_to, nvl(b.fact_sum, 0) shop_fact_sale, null shop_fact_correction_to, case when b.plan_sum !=0 then round(100*nvl(b.fact_sum, 0)/b.plan_sum, 2) else 0 end shop_plan_sale_percent,
null seller_tab, null seller_fio, null group_t,
null period,
null fact_sum, b1.prem_individ_group prem_individ_group, b1.prem_plan_full prem_plan_full, b1.prem_full prem_full
from bep_prem_shop_plan b
left join
(
    select year, month, id_shop,
    sum(fact_sum) fact_sum, sum(prem_individ_group) prem_individ_group, sum(prem_plan_group) prem_plan_full, 
    sum(prem_individ_group) + sum(prem_plan_group) prem_full
    from bep_prem_seller 
    where year = 2018 and month = 12 and id_shop = case when '0004' is null then id_shop else '0004' end 
    group by year, month, id_shop 
) b1 on b1.year = b.year and b1.month = b.month and b1.id_shop = b.id_shop
where b.year = 2018 and b.month = 12 and b.id_shop = case when '0004' is null then b.id_shop else '0004' end

;

    select  a.year, a.month, a.id_shop,
    sum(a.fact_sum) fact_sum, sum(a.prem_individ_group) prem_individ_group, sum(a.prem_plan_group) prem_plan_full, 
    sum(a.prem_individ_group) + sum(a.prem_plan_group) prem_full
    from bep_prem_seller a
    where a.year = 2018 and a.month = 12 and a.id_shop = case when '0004' is null then a.id_shop else '0004' end -- and id_shop = '0004'
    group by a.year, a.month, a.id_shop
;

--ЛИНИЯ ИТОГА ПО КАЖДОМУ ПРОДАВЦУ
select a.year, a.month, a.id_shop, 
null shop_plan_to, null shop_fact_sale, null shop_fact_correction_to, null shop_plan_sale_percent,
a.seller_tab, a.seller_fio, 'ИТОГО' group_t ,sum(a.fact_sum) fact_sum, sum(a.prem_individ_group) prem_individ_group from bep_prem_seller a
where year = 2018 and month = 12 and id_shop = '0004'
group by year, month, id_shop, seller_tab, seller_fio;

--ЛИНИЯ ИТОГА ПО МАГАЗИНУ
select year, month, id_shop, 
plan_sum shop_plan_to, nvl(fact_sum, 0) shop_fact_sale, null shop_fact_correction_to, case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end shop_plan_sale_percent,
null seller_tab, null seller_fio, null group_t, null fact_sum, null prem_individ_group
from bep_prem_shop_plan
where year = 2018 and month = 12 and id_shop = '0004';

select a.year, a.month, a.id_shop, 
b.shop_plan_to, b.shop_fact_sale, b.shop_fact_correction_to, b.shop_plan_sale_percent,
a.seller_tab, a.seller_fio, 'ИТОГО' group_t, a.fact_sum fact_sum/*, sum(fact_pair_kol) fact_pair_kol*/, a.prem_individ_group prem_individ_group 
from (
    select year, month, id_shop, seller_tab, seller_fio, sum(fact_sum) fact_sum, sum(prem_individ_group) prem_individ_group from bep_prem_seller
    where year = 2018 and month = 12 and a.id_shop = case when '0004' is null then a.id_shop else '0004' end
    group by year, month, id_shop, seller_tab, seller_fio
) a
left join (select year, month, id_shop, plan_sum shop_plan_to, nvl(fact_sum, 0) shop_fact_sale, null shop_fact_correction_to , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end shop_plan_sale_percent from bep_prem_shop_plan) b on a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
order by a.year, a.month, a.id_shop, a.seller_tab, a.seller_fio
;

select year, month, id_shop, plan_sum shop_plan_to, nvl(fact_sum, 0) shop_fact_sale, null shop_fact_correction_to, case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end shop_plan_sale_percent from bep_prem_shop_plan;
