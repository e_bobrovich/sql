select scores,  get_scores_clear_other(id_dk, to_timestamp('31.12.2017', 'dd.mm.yyyy')) from firm.st_dk where id_dk = '0000007244469';
call dbms_output.enable;

select * from firm.st_dk where id_dk = '0000022996503'; --s
select * from FIRM.tdv_scores_split2 a where a.id_dk = '0000022996503' order by dates, id_sale,dk_sum;

select case when scores < 0 then 0 else scores end as scores, 
case when (cleared*-1) > scores then scores else cleared end as cleared
from (
select  get_scores('0000020505363') as scores, get_scores_clear_other('0000020505363', to_timestamp('31.12.2017', 'dd.mm.yyyy')) as cleared from dual
);

select value, id_dk, hash, case when scores < 0 then 0 else scores end, 
case when (cleared*-1) > scores then scores else cleared end
from
(
select a.value, b.id_dk, a.hash, get_scores(b.id_dk) as scores, get_scores_clear_other(b.id_dk, to_timestamp('31.12.2017', 'dd.mm.yyyy')) as cleared from imob.revo_res a
left join imob.hash_revo b on a.hash = b.hash
--left join firm.st_dk c on b.id_dk = c.id_dk
--where rownum < 100
order by a.value
)
;


select  nvl(max(sum(nvl(dk_sum,0))),0) 
from  pos_dk_scores s1 
where doc_type not in ('ac01','ac03') and NOT(s1.dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
         and NOT(s1.dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
         and  not (doc_type in ('voz', 'ckv') and (s1.id_shop,s1.id_chek,s1.doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
         and s1.id_dk = '0000018224511'
         and s1.bit_open_check = 'F'
group by s1.id_dk;


