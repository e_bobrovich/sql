SET SERVEROUTPUT ON;

-- ДК : 0000021470264
-- ДАТА : to_timestamp('13.04.17 23:59:59','dd.mm.yy. hh24:mi:ss')

select b.id_ac as scores_type from(
    select 
    --case when regexp_like(nvl(a.id_ac, a.id_chek), '^[[:digit:]]+$') 
    --        then a.doc_type
    --        else nvl(a.id_ac, a.id_chek) end as id_ac
    nvl(a.id_ac, a.id_chek) id_ac 
    from tdv_scores_split2 a 
    where a.id_dk = '0000021470264'
    and a.dk_sum > 0
    --and trunc(a.dates) <= trunc(to_timestamp(i_date, 'dd.mm.yy'))
    and trunc(a.dates) <= trunc(sysdate)
    --and doc_type like 'ac%'
    and (id_ac is not null or not regexp_like(a.id_chek, '^[[:digit:]]+$'))
) b   
group by b.id_ac
order by b.id_ac
;

select nvl(sum(dk_sum), 0)
from tdv_scores_split2 a
where a.id_dk = '0000021470264'
and a.dk_sum <= 0
and
(   
    (
        a.doc_type = 'ck'
        --and a.id_ac is null
        and a.scores_type = 'SC100012'
    )
    or
    (
        (a.doc_type = 'voz'  or a.doc_type = 'ckv')
        and (a.id_ac = 'SC100012' or a.scores_type = 'SC100012') 
    )
)  
and trunc(a.dates) <= trunc(sysdate)
and('F' = is_ignor_scores(a.doc_type, a.dk_sum, a.dates, a.id_shop , a.id_chek)
)
;

select id_chek, id_sale, dk_sum, dates, id_shop, id_ac, scores_type from tdv_scores_split2 a
where a.id_dk = '0000021470264'
and a.dk_sum > 0
and a.doc_type = 'ac'
and (a.id_ac = 'SC100012' or a.id_chek = 'SC100012')
and trunc(a.dates) <= trunc(sysdate)
and('F' = is_ignor_scores(a.doc_type, a.dk_sum, a.dates, a.id_shop , a.id_chek)
)
order by a.dates
;


select 
--*
nvl(sum(dk_sum), 1)
from tdv_scores_split2 a
where a.id_dk = '0000021470264'
and a.dk_sum <= 0
and
(   
        a.doc_type = 'ac' 
        and a.id_shop = 'FIRM'
        and a.id_chek = 'SC100012'
        and a.id_sale = '18366'
)  
and trunc(a.dates) <= trunc(sysdate)
and('F' = is_ignor_scores(a.doc_type, a.dk_sum, a.dates, a.id_shop , a.id_chek)
)
;


select
--*
nvl(sum(dk_sum), 0)
from tdv_scores_split2 a
where a.id_dk = '0000021470264'
and a.dk_sum > 0
and (a.doc_type in ('ck', 'ac03', 'ac01') or (a.doc_type in ('ckv', 'voz') and a.id_ac is null and a.scores_type = 'stnd'))
--and trunc(to_timestamp(i_date, 'dd.mm.yy')) >= trunc(a.dates, 'ddd')
and trunc(sysdate) >= trunc(a.dates, 'ddd')

and('F' = is_ignor_scores(a.doc_type, a.dk_sum, a.dates, a.id_shop , a.id_chek)
--doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
--and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
--and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
)
;


-- СУММА СПИСАНИЙ ДО ЭТОГО ДНЯ ПО СТАНДАРТНЫМ
select nvl(sum(dk_sum), 0)
from tdv_scores_split2 a
where a.id_dk = '0000021470264'
and a.dk_sum <= 0
and ( ( (a.doc_type in ('ck', 'ac03', 'ac01') or (a.doc_type in ('ckv', 'voz') and a.id_ac is null and a.scores_type = 'stnd')) and a.scores_type = 'stnd') 
    or a.id_ac = 'STND0365')
--and trunc(to_timestamp(i_date, 'dd.mm.yy')) >= trunc(a.dates, 'ddd')
and trunc(sysdate) >= trunc(a.dates, 'ddd')

and('F' = is_ignor_scores(a.doc_type, a.dk_sum, a.dates, a.id_shop , a.id_chek)
--doc_type not in ('ac01','ac03') and NOT(dk_sum<0 and to_char(dates,'yyyymm') in ('201603','201602') and doc_type not in ('voz', 'ckv'))
--and not(dk_sum<0 and to_char(dates,'yyyymmdd') between 20160817 and 20160921 and doc_type not in ('voz', 'ckv'))
--and  not (doc_type in ('voz', 'ckv') and (id_shop,id_chek,doc_type) in (select id_shop, id_chek, doc_type from st_dk_ac01_vozv))
)
;

select * from pos_dk_scores a where a.id_dk = '0000021470264' order by dates, id_sale,dk_sum;
select * from pos_dk_scores_firm a where a.id_dk = '0000021470264' order by dates, id_sale,dk_sum;

select * from pos_dk_scores@s2906 a where a.id_dk = '0000024874182' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s2920 a where a.id_dk = '0000024874182' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s2910 a where a.id_dk = '0000024874182' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s2204 a where a.id_dk = '0000024874182' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s2218 a where a.id_dk = '0000024874182' order by dates, id_sale,dk_sum;
select * from pos_dk_scores@s2221 a where a.id_dk = '0000024874182' order by dates, id_sale,dk_sum;

call dk_scores_info('0000021470264');
select * from tem_dk_scores_info where id_dk = '0000021470264';
select * from tem_dk_scores_info_pr where id_dk = '0000021470264';
delete from tem_dk_scores_info_pr where id_dk = '0000021470264';
delete from tem_dk_scores_info_pr where id_dk = '0000021470264';