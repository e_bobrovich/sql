select * from pos_users a;

SELECT SELLER_FIO AS GGROUP
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  from pos_salea_v
 where trunc(dated)>= trunc(to_timestamp('01.12.2019 00:00:00.000','DD.MM.YYYY HH24:MI:SS,ff3')) and 
       trunc(dated)<=trunc(to_timestamp('31.12.2019 00:00:00.000','DD.MM.YYYY HH24:MI:SS,ff3')) 
       and bit_close='T'   
       group by seller_fio order by ggroup 
;

SELECT SELLER_FIO
    ,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
    ,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
    ,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
    ,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
    ,SUM(KOLP) AS KOLP 
    ,SUM(KOLV) AS KOLV 
    ,SUM(KOLP-KOLV) AS KOLR 
      FROM POS_SALEA_V_PREM
     WHERE DATED>=to_date('01.05.2019','dd.MM.yyyy') 
     AND DATED<=to_date('31.05.2019','dd.MM.yyyy') AND BIT_CLOSE='T'   
     GROUP BY SELLER_FIO;
     
 SELECT SELLER_FIO
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  FROM POS_SALEA_V_PREM
 WHERE trunc(DATED)>=to_date('01.05.2019','dd.MM.yyyy') 
 AND trunc(DATED)<=to_date('31.05.2019','dd.MM.yyyy') AND BIT_CLOSE='T'   
 GROUP BY SELLER_FIO;     
     
select rownum rn, x.* from 
(select a.tabel,nvl(a.fio,b.seller_fio) fio,A.DOLJNOST,
case when a.doljnost is null then 0 when upper(a.doljnost) like '%ПРОДАВЕЦ%' then 2 else 1 end code,
nvl(b.sumr,0) sumr
from pos_users a
full join 
(
    SELECT SELLER_FIO
    ,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
    ,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
    ,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
    ,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
    ,SUM(KOLP) AS KOLP 
    ,SUM(KOLV) AS KOLV 
    ,SUM(KOLP-KOLV) AS KOLR 
      FROM POS_SALEA_V_PREM
     WHERE DATED>=to_date('01.01.2019','dd.MM.yyyy') 
     AND DATED<=to_date('31.01.2019','dd.MM.yyyy') AND BIT_CLOSE='T'   
     GROUP BY SELLER_FIO
) b on a.fio = b.seller_fio
 where upper(a.doljnost) not like '%КАССИР%'
 order by a.tabel) x;
 
select 
case when get_dk_bag_as_sop(a.art) = 'F' then 'SHO'
     when get_dk_bag_as_sop(a.art) = 'T' then 'SOP' end GROUP_T
, a.* 
FROM POS_SALEA_V_PREM a
WHERE a.DATED>=to_date('01.08.2018','dd.MM.yyyy') 
AND a.DATED<=to_date('31.08.2018','dd.MM.yyyy') AND BIT_CLOSE='T' 
and seller_fio = 'Леонец Наталья Николаевна'
;

select 
count(1)
FROM POS_SALEA_V_PREM a
WHERE a.DATED>=to_date('01.08.2018','dd.MM.yyyy') 
AND a.DATED<=to_date('31.08.2018','dd.MM.yyyy') AND BIT_CLOSE='T' 
--and seller_fio = 'Миранович Екатерина Анатольевна'
;

SELECT SELLER_FIO AS GGROUP,GRUP
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  FROM POS_SALEA_V
 WHERE trunc(DATED)>=to_timestamp('01.08.2018 00:00:00.000','DD.MM.YYYY HH24:MI:SS,ff3') AND trunc(DATED)<=to_timestamp('31.08.2018 00:00:00.000','DD.MM.YYYY HH24:MI:SS,ff3') AND BIT_CLOSE='T'   
 GROUP BY SELLER_FIO ,GRUP ORDER BY GGROUP,GRUP ; 
 
 SELECT SELLER_FIO AS GGROUP,GRUP
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  FROM POS_SALEA_V
 WHERE DATED>=to_timestamp('01.08.2018 00:00:00.000','DD.MM.YYYY HH24:MI:SS,ff3') AND DATED<=to_timestamp('31.08.2018 00:00:00.000','DD.MM.YYYY HH24:MI:SS,ff3') AND BIT_CLOSE='T'   
 GROUP BY SELLER_FIO ,GRUP ORDER BY GGROUP,GRUP ; 
 
select * from pos_salea_v_prem
where trunc(DATED)>=to_date('01.12.2019','dd.MM.yyyy') 
AND trunc(DATED)<=to_date('31.12.2019','dd.MM.yyyy') AND BIT_CLOSE='T' ;

select * from pos_salea_v_prem 
where DATED>=to_date('01.08.2018','dd.MM.yyyy') 
AND DATED<=to_date('31.08.2018','dd.MM.yyyy') AND BIT_CLOSE='T' ;



SELECT SELLER_FIO
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  from pos_salea_v_prem
 where trunc(dated)>=trunc(to_date('01.05.2019','dd.MM.yyyy')) 
 and trunc(dated)<=trunc(to_date('31.05.2019','dd.MM.yyyy')) and bit_close='T'   
 GROUP BY SELLER_FIO;
 
 select 
 trunc(to_date('31.05.2019','dd.MM.yyyy')) d1,
 trunc(to_date('31.05.2019 15:22:00','dd.MM.yyyy HH24:MI:SS')) d2,
    case when 
        trunc(to_date('31.05.2019','dd.MM.yyyy')) >= trunc(to_date('31.05.2019 15:22:00','dd.MM.yyyy HH24:MI:SS'))
        then 1
        else 0 end res
 from dual;

SELECT *
  from pos_salea_v_prem
 where trunc(dated)>=trunc(to_date('01.01.2019','dd.MM.yyyy')) 
 and trunc(dated)<=trunc(to_date('31.01.2019','dd.MM.yyyy')) and bit_close='T' 
 and seller_fio ='Буйко Дина Сергеевна'
;