select 
b.posnr, b.art, b.price, b.discount, b.discount_r, b.price_sum, a.discount_sum, is_orange_cennik(b.art),

round_dig(
          case when is_orange_cennik(b.art) = 'T' -- проверка на оранжевый ценник
          then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID'))
                                  ,0))/100*get_dk_orange_discount('0000000000125'))
          else
            case when  (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null ) -- проверка на сопутку
            -- сопутка
            then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                    where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                    get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
            -- обувь
            else kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                    where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!=get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                    get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB'))                                                                                    
            end
          end, 1, 0
) scores



--round_dig(case when   (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null )
--                          then kol * (price/100*get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
--                          else kol * (price/100*get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
--                          end, 1, 0)  scores
    
from d_kassa_cur b 
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
left join s_art c on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1))) d on d.docid=discount_type_id
where (discount_type_id = get_dk_sc_sys_str( 'DOCID') or (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                                                                                                            from d_kassa_disc_cur a
                                                                                                                            inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                            where trunc (sysdate) between b.dates and b.datee
                                                                                                                            and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                                    ))))
and b.kass_id = '1#lacit010-m' and b.procent = 0;  


select get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB') from dual;


B0488-2V   
435040    00000000001002568840
420001    00000000001003797017

select * from st_dk where id_dk = '0000000000125';




select distinct a.* from d_kassa_cur a
inner join d_kassa_disc_cur b on a.posnr = b.posnr and a.kass_id = b.kass_id
inner join pos_skidki1 c on c.docid = B.DISCOUNT_TYPE_ID
where a.kass_id = '1#lacit010-m'
--and C.DISC_TYPE_ID in ('ZK02','ZK01') \n"
--and ((substr(to_char(a.price_sum),-1,1) = '7' and to_char(a.price_sum) not like '%.%' and C.DISC_TYPE_ID in ('ZK02')) or C.DOC_EXCEPTION = 'F')"
;

insert into d_kassa_disc_cur (POSNR,DISCOUNT,DISCOUNT_SUM,DISCOUNT_TYPE_ID,KASS_ID) 
select POSNR, 0, 0 ,'A000000001', '1#lacit010-m'  from d_kassa_cur a
where 
a.kass_id = '1#lacit010-m'
and a.posnr not in (select posnr from d_kassa_disc_cur where kass_id = '1#lacit010-m' and discount_type_id = 'A000000001')
;


select BEP_DK_MAX_SCORES_CHECK('1#lacit010-m','0000000000125','F') SCORES from dual;


select posnr from d_kassa_disc_cur where kass_id = '1#lacit010-m' and discount_type_id = 'A000000001';



select a.*, rownum from 
(select b.*,
     a.discount_type_id,
     a.discount_sum,
     c.mtart, case when (DK_MAX_SCORES_CHECK( '1#lacit010-m'))!=0 then
     round_dig(case
                 when  (get_dk_bag_as_sop(b.art, 'F') = 'T'   and d.docid is null) then
                  kol * ((price- case when get_system_val('DK_SCORES_WITH_DISCOUNT_PRICE') = 'T' then discount_r else 0 end) / 100 * 
                                     get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP'))
                 else
                  kol * ((price - case when get_system_val('DK_SCORES_WITH_DISCOUNT_PRICE') = 'T' then discount_r else 0 end) / 100 * 
                                       get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB'))
               end,
               1,
               0) / (DK_MAX_SCORES_CHECK( '1#lacit010-m') / 100)  else 0 end scores_proc
from d_kassa_cur b
left join d_kassa_disc_cur a
  on a.posnr = b.posnr
 and a.kass_id = b.kass_id
left join s_art c
  on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id in ('ZK23','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1)) ) d on d.docid=discount_type_id

where (nvl(discount_type_id, ' ') = get_dk_sc_sys_str('DOCID') or
        (get_dk_bag_as_sop(b.art, 'F') = 'T'   and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                        from d_kassa_disc_cur a
                                        inner join pos_skidki1 b on a.discount_type_id = b.docid
                                        where trunc (sysdate) between b.dates and b.datee
                                        and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                ))))
 and b.kass_id = '1#lacit010-m' 
 and b.procent = 0
-- and (action_pricelist(b.art,sysdate,b.procent) = 'F' or 'F' = 'T')
order by kol) a
order by rownum desc;




