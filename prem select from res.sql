select * from s_seller where fio like '%Криштопович Оксана Владимировна';

select * from pos_users@s0026;
select * from pos_sale1@s0026 where seller_tab = '00022494';
select * from pos_sale1 where seller_tab = '00023046';

select stext2, stext3, count(1) from s_seller group by stext2, stext3;
select post, count(1) from s_seller where stext2 = 'Заведующий магазином' group by post;

select * from bep_prem_seller 
where id_shop = '0064'
order by id_shop, year, month, seller_tab, group_t;

select * from bep_prem_shop_plan 
where fact_sum is not null and id_shop = '0064'
order by year, month, id_shop;

select id_shop, count(1) from bep_prem_shop_plan 
where fact_sum is not null and fact_sum != 0
group by id_shop
order by id_shop;

call bep_calculate_prem_full(2018, 8, '0064');

select shp from spa_fil_shop where fil = '00F1' order by shp;

delete from bep_prem_seller;

--begin
--    for item in (select shp from spa_fil_shop where fil = '00F1' order by shp)
--    loop
--        bep_calculate_prem_full(2018, 8, item.shp);
--        --bep_calculate_prem_plan(2018, 8, item.shp);
--        DBMS_OUTPUT.PUT_LINE(item.shp || ' Complite');
--    end loop;
--end;

call bep_calculate_prem_full(2018, 8, '0045');


select a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО", a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab, seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select tabno,fio from s_seller where sysdate >= dateb and sysdate <= dated) d on a.seller_tab = d.tabno
where a.year = 2018 and a.month = 9
order by a.id_shop, a.year, a.month, a.seller_tab;

select tabno,fio from s_seller where sysdate >= dateb and sysdate <= dated;
--(ectract(year from dateb) <= 2018 and ectract(month from dateb) <= 8)
--and
--(extract(year from dated) >= 2018 and ectract(month from dated) >= 8);
select * from s_seller where tabno = '00005882';





select 
 a.id_shop as "МАГАЗИН", a.year as "ГОД", a.month as "МЕСЯЦ", 
e.plan_sum as "ПЛАН ТО МАГАЗИНА", 
'0' as "КОРРЕКЦИЯ",
e.fact_sum as "ФАКТ ТО МАГАЗИНА", 
e.proc as "% ВЫПОЛНЕНИЯ",
a.seller_tab as "ТАБЕЛЬНЫЙ",nvl(d.fio, a.seller_fio) as "ФИО",
nvl(c.fact_sum, 0) as "ОБУВЬ", 
nvl(b.fact_sum, 0) as "СОПУТКА",
nvl(b.fact_sum, 0)+nvl(c.fact_sum,0) as "ОБЩИЙ ТО",
nvl(c.prem_individ_group,0) "ПРЕМИЯ ОБУВЬ", 
nvl(b.prem_individ_group,0) as "ПРЕМИЯ СОПУТКА",  
nvl(a.prem_individ_full, 0)  as "ПРЕМИЯ ИНДИВИД", 
nvl(a.prem_plan_full, 0) as "ПРЕМИЯ ПЛАН ТО"
from (select seller_tab, max(seller_fio) seller_fio, id_shop, year, month, prem_individ_full, prem_plan_full from bep_prem_seller group by seller_tab/*, seller_fio*/, id_shop, year, month, prem_individ_full, prem_plan_full) a
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Сопутка') b on a.seller_tab = b.seller_tab and a.id_shop = b.id_shop and a.year = b.year and a.month = b.month
left join (select seller_tab, id_shop, year, month, fact_sum, prem_individ_group  from bep_prem_seller where group_t = 'Обувь') c on a.seller_tab = c.seller_tab and a.id_shop = c.id_shop and a.year = c.year and a.month = c.month
left join (select a1.tabno, a1.fio from s_seller  a1
    inner join (
        select tabno, max(dateb) dateb from s_seller where ((EXTRACT(MONTH FROM dateb) <= 11 and EXTRACT(YEAR FROM dateb) = 2018) or EXTRACT(YEAR FROM dateb) < 2018)
                                                       and ((EXTRACT(MONTH FROM dated) >= 11 and EXTRACT(YEAR FROM dated) = 2018) or EXTRACT(YEAR FROM dated) > 2018)
        group by tabno
    ) b1 on a1.tabno = b1.tabno and a1.dateb = b1.dateb
) d on a.seller_tab = d.tabno
left join (select id_shop , year , month , plan_sum, nvl(fact_sum, 0) fact_sum  , case when plan_sum !=0 then round(100*nvl(fact_sum, 0)/plan_sum, 2) else 0 end proc  from bep_prem_shop_plan) e on a.id_shop = e.id_shop and a.year = e.year and a.month = e.month
where a.year = 2018 and a.month = 11
order by a.id_shop, a.year, a.month, a.seller_tab;
