--insert into revo_hash (
--id_dk, phone_number, hash_first, 
----salt, hash_salt,
--age,sex
--)
select 
e.hash_first,
e.new_hash,
e.id_dk, 
e.phone_number,
--sha256.encrypt(a.id_dk||substr(b.phone_number,-10)) as hash_first,
--'aswbn9gv' as salt,
--sha256.encrypt(sha256.encrypt(a.id_dk||substr(b.phone_number,-10))||'aswbn9gv') as hash_salt,
nvl(d.summ,0) as "summa2018", 
nvl(d.cnt,0) as "count2018", 
nvl(d.sred,0) as "sredn2018",
nvl(d.maxim,0) as "maximum2018",

nvl(c.summ,0) as "summa2019", 
nvl(c.cnt,0) as "count2019", 
nvl(c.sred,0) as "sredn2019",
nvl(c.maxim,0) as "maximum2019",

--e.age,
--e.sex
trunc(months_between(sysdate,b.birthday)/12) age,
case when b.sex = '1' then 'М' else 'Ж' end as sex

from revo_hash e
left join t_day_st_dk1 b on e.id_dk = b.id_dk

left join (
    select 
    firm.pos_sale1.id_dk as id_dk, 
    count(firm.pos_sale1.sale_sum) as cnt,
    sum(firm.pos_sale1.sale_sum) as summ, 
    round(avg(firm.pos_sale1.sale_sum),2) as sred,
    max(firm.pos_sale1.SALE_SUM) as maxim
    from firm.pos_sale1
    where firm.pos_sale1.id_dk != ' ' and 
    (firm.pos_sale1.sale_date >= to_date('01.01.2019', 'dd.mm.yyyy') and
    firm.pos_sale1.sale_date <= to_date(/*'20.02.2019'*/'14.03.2019', 'dd.mm.yyyy'))
    group by firm.pos_sale1.id_dk
) c on e.id_dk = c.id_dk

left join (
    select 
    firm.pos_sale1.id_dk as id_dk, 
    count(firm.pos_sale1.sale_sum) as cnt,
    sum(firm.pos_sale1.sale_sum) as summ, 
    round(avg(firm.pos_sale1.sale_sum),2) as sred,
    max(firm.pos_sale1.SALE_SUM) as maxim
    from firm.pos_sale1
    where firm.pos_sale1.id_dk != ' ' and 
    (firm.pos_sale1.sale_date >= to_date('01.01.2018', 'dd.mm.yyyy') and
    firm.pos_sale1.sale_date <= to_date('31.12.2018', 'dd.mm.yyyy'))
    group by firm.pos_sale1.id_dk
) d on e.id_dk = d.id_dk
--left join revo_hash e on a.id_dk = e.id_dk
--where a.id_del = 4602
;


select a.id_dk, a.phone_number, sha256.encrypt(a.id_dk||a.phone_number) as hash_first  from revo_hash a;
