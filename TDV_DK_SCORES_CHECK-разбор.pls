create or replace procedure TDV_DK_SCORES_CHECK(i_id_dk in varchar2, i_id_check in varchar2 default null) is
-- начисление баллов после проведения продажи
v_dk_type st_dk.dk_type%type;
v_dk_level st_dk.dk_level%type;
v_dk_discount st_dk.discount%type;
v_dk_summ st_dk.summ%type;
v_docid pos_skidki1.docid%type;
v_scores number(10,2) default 0;
v_scores_act number(10,2) default 0;
v_scores_plus number(10,2) default 0;
v_scores_plus_act number(10,2) default 0;
v_scores_minus number(10,2) default 0;
v_scores_start_date date;
v_sale_sum pos_sale1.sale_sum%type;
v_bonus number(18,2);
v_7_proc varchar2(1) default 'F';
v_count number(10,1) default 0;
v_dk_vid st_dk.dkvid_id%type;
v_act_date st_dk.activation_date%type;
v_numconf config.numconf%type;
v_is_dr number(1);
v_round number(18,2);
v_NOT_ADD_SCORES number(1);
v_is_sertif varchar2(1) :='F';
v_use_scores varchar2(1) :='F';
v_use_credit varchar2(1) :='F';

v_after_type st_dk.dk_type%type;
v_after_level st_dk.dk_level%type;
v_after_discount st_dk.discount%type;
v_before_type st_dk.dk_type%type;
v_before_level st_dk.dk_level%type;
v_before_discount st_dk.discount%type;

v_zk18 varchar2(1) :='F'; -- отмена сбрасывания скидок по халве для РБ
v_zk11 varchar2(1) :='F'; -- третья пара в подарок

v_shopid config.shopid%type;
begin
  begin
    select a.dk_type, a.discount, a.summ, a.dk_level, a.dkvid_id, a.activation_date
    into v_dk_type, v_dk_discount, v_dk_summ, v_dk_level, v_dk_vid, v_act_date from st_dk a 
    where id_dk = i_id_dk ;
  exception when no_data_found then
    return;
  end;    
  
  v_before_type := v_dk_type;
  v_before_level := v_dk_level;
  v_before_discount := v_dk_discount;
  
  v_scores_start_date := to_date(get_system_val('DK_START_SCORES_DATE'),'dd.mm.yyyy');
  
  if v_scores_start_date is null or trunc(v_scores_start_date) > trunc(sysdate) then
    return;
  end if;  

  if is_dk_scores_type(i_id_dk) = 'F' then
    return;
  end if;  
  --

  v_docid := get_dk_sc_sys_str('DOCID');

  -- до скольки баллов округляем
  v_round := get_dk_sc_sys_int( 'SCORES_ROUND') ;

  -- нужно ли начислять баллы, если за чек платили баллами
  v_NOT_ADD_SCORES := get_dk_sc_sys_int( 'NOT_ADD_SCORES') ;
  
  -- 
  begin
    select numconf, shopid 
    into v_numconf, v_shopid
    from config;
  exception when no_data_found then 
    v_numconf := 1;
  end;  

  -- проверка , есть ли оплата сертификатом
  begin
    select distinct  'T'
    into v_is_sertif
    from pos_sale3 a
    where a.id_chek = i_id_check and a.vid_op_id IN (3);
  exception when no_data_found then
    v_is_sertif := 'F';
  end;        

 -- кредит рассрочка (для РБ) {
  begin
    select distinct 'T'
    into v_use_credit
    from pos_sale8 x
    where x.id_chek = i_id_check  and x.vid_op_id = 2 and x.podvid_op_id   IN (2,4,5, 6) and v_numconf = 2;
  exception when no_data_found then
    v_use_credit := 'F';
  end; 

  -- проверка отмены сбрасывания скидок по халве (для РБ)
  begin
    select distinct 'T'
    into v_zk18
    from pos_skidki1 r
    where upper(r.disc_type_id) = 'ZK18' and trunc(sysdate) between trunc(r.dates) and trunc(r.datee);
  exception when no_data_found then
    v_zk18 := 'F';
  end;   

  -- проверка отмены сбрасывания скидок по халве (для РБ)
  begin
    select distinct 'T'
    into v_zk11
    from pos_sale1 a
    inner join pos_sale2 b on a.id_chek = b.id_chek
    inner join pos_sale4 c on b.id_sale = c.id_sale
    inner join pos_skidki1 d on c.disc_type_id = d.docid
    where a.id_chek = i_id_check and upper(d.disc_type_id) = 'ZK11';
  exception when no_data_found then
    v_zk11 := 'F';
  end;   

  
  if v_use_credit = 'F' and v_numconf=1 then 
  begin
      select distinct  'T'
      into v_use_credit
      from pos_sale3 a
      where a.id_chek = i_id_check and a.vid_op_id IN (4);
    exception when no_data_found then
      v_use_credit := 'F';
    end; 
  end if;  
  -- )
  ----------------------------------------------------------------------------------------------------------------------
  -- Если карта уже бальная, то :
  ----------------------------------------------------------------------------------------------------------------------
  if v_dk_type = 'B' and i_id_check is not null then
    
    delete from POS_DK_SCORES where DOC_TYPE = 'ck' and ID_CHEK = i_id_check;
    delete from POS_DK_SCORES where DOC_TYPE = 'ac' and ID_CHEK = i_id_check;

    -- сколько начислить баллов {
    v_scores := 0; 
    
    if    v_dk_vid !='002'  then 
      for r_plus in (select a.*, b.id_sale as minus_scores, y.docid from pos_sale2 a
                        left join (select * from pos_sale4 x where x.disc_type_id = v_docid and x.discount = 0 and x.discount_sum !=0) b on a.id_sale = b.id_sale
                        left join (select art, max(docid) docid from pos_skidki2 y where y.docid in ('B200000037','B200000152') group by art) y on y.art = a.art
                        where a.id_chek = i_id_check  
                                 and a.art not in (select a1.art from st_sertif a1)  -- если покупают сертификат, не начисляем ничего
                                 --and NOT (v_NOT_ADD_SCORES = 1 and b.id_sale is not null)
           ) loop
          
          v_scores := 0;
          
          if  NOT (v_use_credit = 'T' and v_numconf=1)  -- не начисляются при покупке в рассрочку только РФ (в РБ с 20170313 отменили, приказ 135)
              --and NOT (v_NOT_ADD_SCORES = 1 and r_plus.minus_scores is not null and to_char(sysdate,'yyyymm')< '201703')   -- не начисляются при покупке , на которую использованы баллы
              and v_is_sertif = 'F'  -- не начисляются при оплате сертификатом
              then
              if to_char(sysdate,'yyyymmdd')>='20170817'  and to_char(sysdate,'yyyymmdd')<='20171025' and v_numconf = 2
                and ((to_char(sysdate,'yyyymmdd')<='20170901' and r_plus.scan != ' '   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
                and NOT action_pricelist(r_plus.art,sysdate) = 'T'  
                and NOT r_plus.docid is not null -- сток в РБ тоже не начислять
                and (r_plus.minus_scores is not null or v_use_credit = 'T')) 
                or (to_char(sysdate,'yyyymmdd')>'20170901' and to_char(sysdate,'yyyymmdd')<='20170913' and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')  
                and v_use_credit = 'T'))
                then
                  v_scores := 0;
                else
                  v_scores := get_dk_scores_sale(i_id_dk, r_plus.cena3, r_plus.kol); 
              end if;
            if v_scores!=0 then
              insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ck', r_plus.id_chek, r_plus.id_sale, v_scores);
            end if;     
          end if;
          
          v_scores_act := 0;   

        ------------ АКЦИИИ ----------------------------
          -- с 01.02.2017 по 12.03.2017 начисляем за покупку 100% стоимости {
          if  to_char(sysdate,'yyyymmdd')>='20170201'  and ((to_char(sysdate,'yyyymmdd')<='20170326' and v_numconf = 2) or (to_char(sysdate,'yyyymmdd')<='20170326' and v_numconf = 1))
              and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
              and NOT (action_pricelist(r_plus.art,sysdate) = 'T' and v_numconf = 2 and baggins(r_plus.art) = 'T' and v_use_credit ='F') 
                    --or (v_use_credit = 'T' and v_zk18 = 'F' and v_numconf = 2) ) --и артикул не на оранжевом ценнике для РБ
              --and NOT(v_numconf = 2 and r_plus.docid is not null and to_char(sysdate,'yyyymm')< '201703') -- сток в РБ тоже не начислять
              and v_zk11 = 'F' -- если третья пара в подарок, то не начисляем
              and v_shopid != '1U02'
              then
                
            v_scores_act := round_dig( r_plus.cena3, v_round, 0) * r_plus.kol - v_scores;
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum, id_ac) values (i_id_dk, 'ac', r_plus.id_chek, r_plus.id_sale, v_scores_act, 'AC030001');  
          -- }
          end if;
          
          -- с 17.08.2017 по 13.09.2017 начисляем за покупку 100% стоимости в РБ {
          if  to_char(sysdate,'yyyymmdd')>='20170817'  and to_char(sysdate,'yyyymmdd')<='20171025' and v_numconf = 2
              and ((to_char(sysdate,'yyyymmdd')<='20170901' and r_plus.scan != ' '   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
                  and NOT action_pricelist(r_plus.art,sysdate) = 'T'  
                  and v_is_sertif = 'F'
                  and NOT r_plus.docid is not null) 
                or (to_char(sysdate,'yyyymmdd')>'20170901' and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
                  and v_is_sertif = 'F')) -- сток в РБ тоже не начислять
              then
                
            v_scores_act := round_dig( r_plus.cena3, v_round, 0) * r_plus.kol - v_scores;
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum, id_ac) values (i_id_dk, 'ac', r_plus.id_chek, r_plus.id_sale, v_scores_act, 'AC030002');  
          -- }
          end if;
          
           -- с 21.09.2017 по 04.10.2017 начисляем за покупку 100% стоимости в 1 филиале РФ {
          if  ((to_char(sysdate,'yyyymmdd')>='20170921'  and to_char(sysdate,'yyyymmdd')<='20171025' and substr(v_shopid,1,2) not in ('34'))
              or (to_char(sysdate,'yyyymmdd')>='20171005'  and to_char(sysdate,'yyyymmdd')<='20171025' and substr(v_shopid,1,2) in ('34')))
              and v_numconf = 1
              and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
              then
                
            v_scores_act := round_dig( r_plus.cena3, v_round, 0) * r_plus.kol - v_scores;
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum, id_ac) values (i_id_dk, 'ac', r_plus.id_chek, r_plus.id_sale, v_scores_act, 'AC030003');  
          -- }
          end if;
          
          v_scores_plus := v_scores_plus + v_scores+v_scores_act;
          v_scores_plus_act :=  v_scores_plus_act + v_scores_act;
      end loop;  
    end if;
    -- }

    -- если день рождения удваиваем начисленные баллы
    begin 
      select distinct 1
      into v_is_dr
      from st_dk  a
      inner join POS_DK_SCORES b on a.id_dk = b.id_dk 
      where a.id_dk = i_id_dk and to_char(sysdate,'ddmm') = to_char(a.birthday,'ddmm') and b.id_chek = i_id_check and v_dk_vid !='002';
          
      insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ac', 'SC100012', i_id_check, v_scores_plus);
          
      v_scores_plus := v_scores_plus + v_scores_plus;
    exception when no_data_found then
      v_is_dr := 0;
    end ;  

    
    -- сколько списать баллов
    v_scores := 0;
    for r_minus in (select b.id_chek, b.id_sale, a.discount_sum, b.kol from  pos_sale4 a
                    inner join pos_sale2 b on a.id_sale = b.id_sale
                    where b.id_chek  = i_id_check and a.disc_type_id = v_docid) loop
       v_scores := r_minus.discount_sum * r_minus.kol;
       v_scores_minus := v_scores_minus + v_scores;
       if v_scores!=0 then
         insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ck', r_minus.id_chek, r_minus.id_sale, -1*v_scores);                            
       end if; 
    end loop;                    
    -- }


    -- баллы на новую карту мая 2015
    if to_char(sysdate, 'yyyymmdd') >= '20160520' and  to_char(sysdate, 'yyyymmdd') <= '20160531' and trunc(v_act_date)=trunc(sysdate) then -- {
    
        select count(*) 
        into v_count
        from POS_DK_SCORES a 
        where a.id_chek = 'SC100010' and a.doc_type = 'ac' and a.id_dk = i_id_dk;

        if v_count = 0 then  
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ac', 'SC100010', ' ', 500);
            
            update st_dk a set a.edit_date = sysdate,
                               a.scores = a.scores + 500
                   where id_dk =  i_id_dk;            
            
        end if;          
      null; 
    end if;
    
    -- новые параметры ДК {
    select a.sale_sum
    into v_sale_sum
    from pos_sale1 a
    where a.id_chek = i_id_check;
    
    v_dk_level := least(GET_DK_SCORES_LEVEL(v_dk_summ + v_sale_sum), v_dk_level);
    
    v_dk_discount := get_dk_sc_sys_int('PROC_LEVEL', v_dk_level);
    -- }
    
    
    if v_dk_vid ='002' then --если это  карты продавцов, то ничего не начисляем
      v_scores_plus := 0;
      v_scores_plus_act := 0;
    end if;  
  
    update st_dk a set a.edit_date = sysdate,
                       a.scores = a.scores + v_scores_plus - v_scores_minus,
                       a.dk_level = v_dk_level,
                       a.discount = v_dk_discount,
                       a.summ = v_dk_summ + v_sale_sum,
                       a.dkvid_id = case when v_dk_vid = '022' then '003' else a.dkvid_id end
      where id_dk = i_id_dk; 

  -- запись в историю изменения карты
   insert into POS_DK_SALE_HIS values (i_id_dk, v_dk_level, v_dk_discount, 'B', v_before_level, v_before_discount, v_before_type, i_id_check, systimestamp);
          
    -- если это те баллы которыми нельзя воспользоваться, то:
    if  to_char(sysdate,'yyyymmdd')>='20160115'  and to_char(sysdate,'yyyymmdd')<='20160310' then
      update st_dk a set a.inf1 = nvl(a.inf1,0) + v_scores_plus_act
        where id_dk = i_id_dk;       
    end if;
          
     -- пишу в inf3 акционные баллы:
    if  to_char(sysdate,'yyyymmdd')>='20160817'  and to_char(sysdate,'yyyymmdd')<='20160920' then
      update st_dk a set a.inf3 = nvl(a.inf3,0) + v_scores_plus_act
        where id_dk = i_id_dk;       
    end if;

   
   -- для VIP карт
  if v_dk_vid ='017' then
    update st_dk a set a.dk_level = 1,
                              a.discount = 15
      where id_dk = i_id_dk;       
  end if;  
    
    commit;
    
    return; --!!!!
  end if;  
  
  
  ----------------------------------------------------------------------------------------------------------------------
  --- Если это переход на балльную
  ----------------------------------------------------------------------------------------------------------------------
 
/* -- если не обувь и не сумка, то не переводим в балльную при продаже {
  if i_id_check is not null then
    select count(*)
    into v_count
    from pos_sale2 a
    where a.id_chek = i_id_check and (scan!= ' ' or baggins(art) = 'T');
    
    if v_count = 0 then
      return;
    end if;  
  end if;  
 -- }*/
 
  v_scores_plus := 0;
  v_sale_sum := 0;
  
  
  if v_dk_discount = 7 and v_numconf != 2 then -- для 7% РФ карт переходим на второй уровень
    v_dk_level := 2;
    v_dk_discount := 10;
    v_7_proc := 'T';
  elsif v_dk_discount in (4,5) and v_numconf = 2 then -- для 4, 5% РБ карт переходим на второй уровень
    v_dk_level := 3;
    v_dk_discount := 5;
    v_7_proc := 'T';      
  elsif v_dk_discount in (6,7) and v_numconf = 2 then -- для 6, 7% РБ карт переходим на второй уровень
    v_dk_level := 2;
    v_dk_discount := 7;
    v_7_proc := 'T';      
  elsif v_dk_discount in (8,9,10) and v_numconf = 2 then -- для 8, 9, 10% РБ карт переходим на первый уровень
    v_dk_level := 1;
    v_dk_discount := 10;
    v_7_proc := 'T';      
  else  
    v_dk_level := get_dk_sc_sys_str('EX_LEVEL', v_dk_discount);

    v_dk_discount := get_dk_sc_sys_int('PROC_LEVEL', v_dk_level);  
  end if;
  

  update st_dk a set a.dk_type = 'B',
                     a.dk_level = v_dk_level,
                     a.discount = v_dk_discount
    where id_dk = i_id_dk; 

  if v_7_proc = 'T' then
    update st_dk a set inf2 = v_dk_level
      where id_dk = i_id_dk;                                                               
  end if;  

  if i_id_check is not null then
    
    delete from POS_DK_SCORES where DOC_TYPE = 'ck' and ID_CHEK = i_id_check;
    delete from POS_DK_SCORES where DOC_TYPE = 'ac' and ID_CHEK = i_id_check;
    
     -- сколько начислить баллов {
    v_scores := 0;
    if   v_dk_vid !='002'  then     
      for r_plus in (select a.*, y.docid from pos_sale2 a
                        left join (select art, max(docid) docid from pos_skidki2 y where y.docid in ('B200000037','B200000152') group by art) y on y.art = a.art        
                        where a.id_chek = i_id_check  
                                 and a.art not in (select a1.art from st_sertif a1) 
          ) loop

          v_scores := 0;
          
          if  NOT (v_use_credit = 'T' and v_numconf=1)  -- не начисляются при покупке в рассрочку только РФ (в РБ с 20170313 отменили, приказ 135)
              and v_is_sertif = 'F'  then
              if to_char(sysdate,'yyyymmdd')>='20170817'  and to_char(sysdate,'yyyymmdd')<='20171025' and v_numconf = 2
                and ((to_char(sysdate,'yyyymmdd')<='20170901' and r_plus.scan != ' '   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
                and NOT action_pricelist(r_plus.art,sysdate) = 'T'  
                and NOT r_plus.docid is not null -- сток в РБ тоже не начислять
                and v_use_credit = 'T') 
                or (to_char(sysdate,'yyyymmdd')>'20170901' and to_char(sysdate,'yyyymmdd')<='20170913' and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')  
                and v_use_credit = 'T'))
                then
                  v_scores := 0;
                else
                  v_scores := get_dk_scores_sale(i_id_dk, r_plus.cena3, r_plus.kol); 
              end if;
            
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ck', r_plus.id_chek, r_plus.id_sale, v_scores);
          end if;
          
          v_scores_act := 0;   

          -- с 01.02.2017 по 12.03.2017 начисляем за покупку 100% стоимости {
          if  to_char(sysdate,'yyyymmdd')>='20170201'  and ((to_char(sysdate,'yyyymmdd')<='20170326' and v_numconf = 2) or (to_char(sysdate,'yyyymmdd')<='20170326' and v_numconf = 1))
              and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')  -- и это обувь или сумка
              and  NOT (action_pricelist(r_plus.art,sysdate) = 'T' and v_numconf = 2 and baggins(r_plus.art) = 'T' and v_use_credit ='F') 
                      --or (v_use_credit = 'T' and v_zk18 = 'F' and v_numconf = 2)) --и артикул не на оранжевом ценнике
              --and NOT(v_numconf = 2 and r_plus.docid is not null and to_char(sysdate,'yyyymm')< '201703')
              and v_zk11 = 'F'
              then
                
            v_scores_act := round_dig( r_plus.cena3, v_round, 0) * r_plus.kol - v_scores;
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum, id_ac) values (i_id_dk, 'ac', r_plus.id_chek, r_plus.id_sale, v_scores_act, 'AC030001');  
          -- }
          end if;
          
          -- с 17.08.2017 по 13.09.2017 начисляем за покупку 100% стоимости в РБ {
          if  to_char(sysdate,'yyyymmdd')>='20170817'  and to_char(sysdate,'yyyymmdd')<='20171025' and v_numconf = 2
              and ((to_char(sysdate,'yyyymmdd')<='20170901' and r_plus.scan != ' '   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
                  and NOT action_pricelist(r_plus.art,sysdate) = 'T'  
                  and v_is_sertif = 'F'
                  and NOT r_plus.docid is not null) 
                or (to_char(sysdate,'yyyymmdd')>'20170901' and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
                  and v_is_sertif = 'F')) -- сток в РБ тоже не начислять
              then
                
            v_scores_act := round_dig( r_plus.cena3, v_round, 0) * r_plus.kol - v_scores;
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum, id_ac) values (i_id_dk, 'ac', r_plus.id_chek, r_plus.id_sale, v_scores_act, 'AC030002');  
          -- }
          end if;
          
          -- с 21.09.2017 по 04.10.2017 начисляем за покупку 100% стоимости в 1 филиале РФ {
          if  ((to_char(sysdate,'yyyymmdd')>='20170921'  and to_char(sysdate,'yyyymmdd')<='20171025' and substr(v_shopid,1,2) not in ('34'))
              or (to_char(sysdate,'yyyymmdd')>='20171005'  and to_char(sysdate,'yyyymmdd')<='20171025' and substr(v_shopid,1,2) in ('34')))
              and v_numconf = 1
              and (r_plus.scan != ' ' or baggins(r_plus.art) = 'T')   -- и это обувь или сумка или для Рб кредит or (v_numconf = 2 and v_use_credit ='T')
              then
                
            v_scores_act := round_dig( r_plus.cena3, v_round, 0) * r_plus.kol - v_scores;
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum, id_ac) values (i_id_dk, 'ac', r_plus.id_chek, r_plus.id_sale, v_scores_act, 'AC030003');  
          -- }
          end if;
          
          v_scores_plus := v_scores_plus + v_scores + v_scores_act;
          v_scores_plus_act :=  v_scores_plus_act + v_scores_act;
      end loop;  
    end if;    
    -- }

    -- если день рождения удваиваем начисленные баллы
    begin 
      select distinct 1
      into v_is_dr
      from st_dk  a
      inner join POS_DK_SCORES b on a.id_dk = b.id_dk 
      where a.id_dk = i_id_dk and to_char(sysdate,'ddmm') = to_char(a.birthday,'ddmm') and b.id_chek = i_id_check and  v_dk_vid !='002';
          
      insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ac', 'SC100012', i_id_check, v_scores_plus);
          
      v_scores_plus := v_scores_plus + v_scores_plus;
    exception when no_data_found then
      v_is_dr := 0;
    end ;  

    -- победные баллы с 07.05.2016. по 09.05.2016 
    if to_char(sysdate, 'yyyymmdd') >= '20160507' and  to_char(sysdate, 'yyyymmdd') <= '20160509' then -- {
        
        select count(*) 
        into v_count
        from POS_DK_SCORES a 
        where a.id_chek = 'SC100009' and a.doc_type = 'ac' and a.id_dk = i_id_dk;

        if v_count = 0 then  
            insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ac', 'SC100009', ' ', 300);
            
            v_scores_plus := v_scores_plus + 300;            
        end if;          

    end if; --}

  -- баллы на новую карту мая 2015
  if to_char(sysdate, 'yyyymmdd') >= '20160520' and  to_char(sysdate, 'yyyymmdd') <= '20160531'  then -- {
  
      select count(*) 
      into v_count
      from POS_DK_SCORES a 
      where a.id_chek = 'SC100010' and a.doc_type = 'ac' and a.id_dk = i_id_dk;

      if v_count = 0 then  
          insert into POS_DK_SCORES (id_dk, DOC_TYPE, ID_CHEK, ID_SALE, dk_sum) values (i_id_dk, 'ac', 'SC100010', ' ', 500);
          
          update st_dk a set a.edit_date = sysdate,
                             a.scores = a.scores + 500
                 where id_dk =  i_id_dk;            
          
      end if;          
    null; 
  end if;    

    select a.sale_sum
    into v_sale_sum
    from pos_sale1 a
    where a.id_chek = i_id_check;

    

    if v_dk_level > GET_DK_SCORES_LEVEL(v_dk_summ )  then --v_dk_summ + v_sale_sum
      
      v_dk_level := GET_DK_SCORES_LEVEL(v_dk_summ);
    end if;

    v_dk_discount := get_dk_sc_sys_int('PROC_LEVEL', v_dk_level);    

    v_scores_plus := round_dig(v_scores_plus,v_round,0);
    
/*    if v_7_proc = 'T' and v_dk_level = 3 then
      v_dk_level:=2;
      v_dk_discount:=10;
    end if;  */
  end if;  
  
  if v_dk_vid ='002' then --если это  карты продавцов, то ничего не начисляем
    v_scores_plus := 0;
    v_scores_plus_act := 0;
  end if;  
    
  update st_dk a set a.dk_type = 'B',
                     a.scores_date = trunc(sysdate),
                     a.edit_date = sysdate,
                     a.scores = a.scores + v_scores_plus,
                     a.dk_level = v_dk_level,
                     a.discount = v_dk_discount,
                     a.dkvid_id = case when v_dk_vid = '022' then '003' else a.dkvid_id end
     --                a.summ = v_dk_summ + v_sale_sum 
    where id_dk = i_id_dk;                                                           
 
   insert into POS_DK_SALE_HIS values (i_id_dk, v_dk_level, v_dk_discount, 'B', v_before_level, v_before_discount, v_before_type, i_id_check, systimestamp);

                                         
  -- если это те баллы которыми нельзя воспользоваться, то:
  if  to_char(sysdate,'yyyymmdd')>='20160115'  and to_char(sysdate,'yyyymmdd')<='20160310' then
    update st_dk a set a.inf1 = nvl(a.inf1,0) +  v_scores_plus_act
      where id_dk = i_id_dk;       
  end if;
    
  -- пишу в inf3 акционные баллы:
  if  to_char(sysdate,'yyyymmdd')>='20160817'  and to_char(sysdate,'yyyymmdd')<='20160920' then
    update st_dk a set a.inf3 = nvl(a.inf3,0) +  v_scores_plus_act
      where id_dk = i_id_dk;       
  end if;
  
  -- для VIP карт
  if v_dk_vid ='017' then
    update st_dk a set a.dk_level = 1,
                              a.discount = 15
      where id_dk = i_id_dk;       
  end if;  
  
  commit;   
  
  exception when others then 
    dbms_output.put_line(sqlcode||sqlerrm);
    rollback;
    WRITE_ERROR_PROC('TDV_DK_SCORES_CHECK', sqlcode, sqlerrm, ' i_id_dk = '||i_id_dk||' i_id_check = '||  i_id_check, ' ', ' ');     
    raise;
  
end;
