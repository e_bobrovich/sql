select * from t_map_new_poexalo where id = 20458 and art = '8835006';

select * from d_planot1 order by dated desc;

select * from d_rasxod1;
select * from d_rasxod2;
select * from d_prixod1 order by dated desc;
select * from d_prixod2 a;
select * from d_prixod1 where idop = '01' order by dated desc; 
select * from d_sap_odgruz1;

select id,id_shop,season,sum(kol) kolf from d_rasxod2 x inner join s_art y on x.art = y.art group by id,id_shop,season;


-- ПОСТАВКИ
select 
a.id_shop, c.season , sum(b.kol) 
from d_prixod1 a
left join d_prixod2 b on a.id = b.id and a.id_Shop = b.id_shop
left join s_art c on b.art = c.art
--left join (select id,id_shop,season,sum(kol) kolf from d_rasxod2 x inner join s_art y on x.art = y.art group by id,id_shop,season) b on b.id = a.id and b.id_shop = a.id_shop
where a.idop = '01' 
and to_char(a.dated, 'yyyymmdd') >= '20190101'  
and to_char(a.dated, 'yyyymmdd') < '20191001'  
and a.id_shop not in ('S777', 'S888', 'I002')
group by a.id_shop, c.season 
order by a.id_shop, c.season
; 


select *
  from d_prixod1 a
  inner join d_prixod2 b on a.id = b.id and a.id_Shop = b.id_shop
  left join s_art c on b.art = c.art
  where a.idop = '01' 
  and to_char(a.dated, 'yyyymmdd') >= '20190101'  
  and to_char(a.dated, 'yyyymmdd') < '20191001'  
  and a.id_shop not in ('S777', 'S888', 'I002')
  and substr(a.id_Shop, 3, 1) != 'W'
  and substr(a.id_Shop, 3, 1) != 'S' 
  and substr(a.id_Shop, 1, 1) != 'W'
  and a.id_shop = '4010'
  and c.art is null
  and a.bit_close = 'F'
  ;

select rel, kpodr from d_planot1 where (kkl not like '%KI1000%' and kkl not like '%D%') order by dated desc;


select * from d_planot1 where kpodr = '3622' and rel = 343;
select * from d_rasxod1 where id_Shop = '0041' and idop = '24';

select * from d_rasxod2 where id_Shop = '3622';


select * from d_planot3 where kpodr = '3622' and rel = 343;

-- РАСХОДЫ
select
--a.id_shop, c.season, sum(kol) kol 
*
from d_rasxod1 a
--inner join d_planot1 x on a.id_shop = x.kpodr and a.rel_planot = x.rel and a.bit_close = 'T' and x.kkl not like '%KI1000%' and x.kkl not like '%D%'
left join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop
left join s_art c on b.art = c.art
where a.idop = '24'
and to_char(a.dated, 'yyyymmdd') >= '20190101'  
and to_char(a.dated, 'yyyymmdd') < '20191001'  
and a.id_shop not in ('S777', 'S888', 'I002')
--group by a.id_shop, c.season
order by a.id_shop, c.season
;


-- ПРИХОДЫ
select a.id_shop, c.season, sum(kol) kol from d_prixod1 a
--inner join d_planot1 x on a.id_shop = x.kpodr and a.rel_planot = x.rel and a.bit_close = 'T' and x.kkl not like '%KI1000%' and x.kkl not like '%D%'
left join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop
left join s_art c on b.art = c.art
where idop = '04'
and to_char(a.dated, 'yyyymmdd') >= '20190101'  
and to_char(a.dated, 'yyyymmdd') < '20191001'  
and a.id_shop not in ('S777', 'S888', 'I002')
group by a.id_shop, c.season
order by a.id_shop, c.season
;

select * from d_planot1 where kkl = '0070'  /*rel = 17*/ order by rel desc;
select * from d_prixod1 where id_shop = '0070' and idop = '04' order by id desc;


-- ПОСТАВКИ
select 
  nvl(x.id_shop, nvl(y.id_Shop, z.id_shop)) id_shop, 
  nvl(x.season, nvl(y.season, z.season)) season,  
  nvl(x.kol, 0) post_kol,
  nvl(y.kol, 0) prix_kol,
  nvl(z.kol, 0) rasx_kol
from (select 
  a.id_shop, c.season , sum(b.kol) kol
  from d_prixod1 a
  inner join d_prixod2 b on a.id = b.id and a.id_Shop = b.id_shop
  left join s_art c on b.art = c.art
  --left join (select id,id_shop,season,sum(kol) kolf from d_rasxod2 x inner join s_art y on x.art = y.art group by id,id_shop,season) b on b.id = a.id and b.id_shop = a.id_shop
  where a.idop = '01' 
  and to_char(a.dated, 'yyyymmdd') >= '20190101'  
  and to_char(a.dated, 'yyyymmdd') < '20191001'  
  and a.id_shop not in ('S777', 'S888', 'I002')
  and substr(a.id_Shop, 3, 1) != 'W'
  and substr(a.id_Shop, 3, 1) != 'S' 
  and substr(a.id_Shop, 1, 1) != 'W'
  and a.bit_close != 'F'
  group by a.id_shop, c.season 
  --order by a.id_shop, c.season
) x
full join

( -- ПРИХОДЫ
  select a.id_shop, c.season, sum(kol) kol 
  from d_prixod1 a
  --inner join d_planot1 x on a.id_shop = x.kpodr and a.rel_planot = x.rel and a.bit_close = 'T' and x.kkl not like '%KI1000%' and x.kkl not like '%D%'
  inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop
  left join s_art c on b.art = c.art
  where idop = '04'
  and to_char(a.dated, 'yyyymmdd') >= '20190101'  
  and to_char(a.dated, 'yyyymmdd') < '20191001'  
  and a.id_shop not in ('S777', 'S888', 'I002')
  and substr(a.id_Shop, 3, 1) != 'W'
  and substr(a.id_Shop, 3, 1) != 'S' 
  and substr(a.id_Shop, 1, 1) != 'W'
  and a.bit_close != 'F'
  group by a.id_shop, c.season
--  order by a.id_shop, c.season
) y on x.id_shop = y.id_shop and x.season = y.season

full join (
-- РАСХОДЫ
  select
  a.id_shop, c.season, sum(kol) kol 
  from d_rasxod1 a
  --inner join d_planot1 x on a.id_shop = x.kpodr and a.rel_planot = x.rel and a.bit_close = 'T' and x.kkl not like '%KI1000%' and x.kkl not like '%D%'
  inner join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop
  left join s_art c on b.art = c.art
  where a.idop = '24'
  and to_char(a.dated, 'yyyymmdd') >= '20190101'  
  and to_char(a.dated, 'yyyymmdd') < '20191001'  
  and a.id_shop not in ('S777', 'S888', 'I002')
  and substr(a.id_Shop, 3, 1) != 'W'
  and substr(a.id_Shop, 3, 1) != 'S'
  and substr(a.id_Shop, 1, 1) != 'W'
  and a.bit_close != 'F'
  group by a.id_shop, c.season
--  order by a.id_shop, c.season
) z on nvl (x.id_shop, y.id_shop) = z.id_shop and nvl(x.season, y.season) = z.season
order by nvl(x.id_shop, nvl(y.id_Shop, z.id_shop)), nvl(x.season, nvl(y.season, z.season))
  
; 