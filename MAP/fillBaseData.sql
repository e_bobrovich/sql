/*
1) Некондиция
2) Брак
3) Детская
4) Артикула заблокированные (покупные)
5) Сток

брак - ШК лежит в таблице pos_brak
некондиция - в таблице s_label поле procent > 0
сток - во вьювере cena_skidki_all_shops поле final_price кончается на 99
*/
select * from e_osttek_online where id_shop = '3716' and art = '1741002';
-- формирую остатки на сегодня
select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
       from e_osttek_online a2
       inner join s_art c on a2.art = c.art
       left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
       left join st_muya_art z on a2.art = z.art
       left join cena_skidki_all_shops y on a2.art = y.art
       where a2.scan != ' '
             and ',Зимняя,' like '%,' || c.season || ',%' -- только нужный сезон
             
             and a2.id_shop in (select shopid from tdm_map_new_shop_group where s_group = 15901)
             and a2.id_shop in (select shopid from st_shop z where z.org_kod = 'SHP')
             and case when 'T' = 'T' then x.scan else null end is null -- исключение брака
             and case when 'T' = 'T' then a2.procent else 0 end = 0 -- исключение некондиции
             and case when 'T' = 'T' then z.art else null end is null -- исключение покупной
             and case when 'T' = 'T' then c.groupmw else 'Мужские' end in ('Мужские', 'Женские') -- исключение детской обуви
             and case when 'T' = 'T' then substr(to_char(y.cena), -2) else ' ' end = '99' -- исключение стоковой
             -- исключение детской
       group by a2.id_shop, a2.art, a2.asize, c.season
       having sum(kol) > 0)
group by id_shop, art, asize, season;

-- формирую остатки на дату по документу i_stock_doc_no
select id_shop, art, asize, sum(kol) kol, season, 0 out_block, sum(kol_rc) kol_rc, sum(kol) kol
from (select shopid id_shop, a.art, asize, sum(oste) kol, c.season, 0 out_block, 0 kol_rc
       from e_oborot2 a
       inner join s_art c on a.art = c.art
       where id = '822'--i_stock_doc_no
             and c.mtart != 'ZHW3'
             and procent = 0
             and ',Зимняя,' like '%,' || c.season || ',%' -- только нужный сезон
             and a.shopid in (select shopid from tdm_map_new_shop_group where s_group = 15901)
       group by shopid, a.art, asize, c.season)
group by id_shop, art, asize, season;


select * from e_oborot2 where id = '822';