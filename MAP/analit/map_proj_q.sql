insert into temp_reports (prim,id_shop)
select 'prognoz_02072019',substr(shopnum,2)
from s_shop
where substr(shopnum,2) in 
--('23019','26102','02107','17101','23021','26103','02108','17104','23022','26104','02109',
--'17107','23010','24110','02110','17112','23011','24101','02113','17116','23012','24107',
--'02115','17118','23014','24109','02120','17121','23016','24104','02124','17122','23017',
--'02101','17124','23018','02102','23013','02103','06101','02111','06120','02119','06130',
--'02122','06133','06137','06140','06141','06144','06145','06146','06147')
(
'00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
'00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
'00008','00009','00017','00020','00031','00033','00041','00047','00063'
)
;

--1
select 
a.art "vendor_code",b.asize "size",a.assort "name",a.groupmw "sex",a.season "season",
a.style "style_line",
case when nvl(to_char(a.release_year),' ') = ' ' then null else a.release_year||'-01-01' end "date_start",
a.mat_lining "lining_material",
a.mat_sole "sole_material",null "heel_type",null "toe_type",a.high_heel "heel_height"
from s_art a
inner join s_size_scale b on a.art = b.art
;
--

--2
select substr(b.shopnum,2) "id_store",b.cityname||', '||b.address "address",
c.region "region",
case when nvl(shop_location,' ') = ' ' then 'стрит' else 'ТЦ' end "location",null "area"
from st_shop a
inner join s_shop b on a.shopid = b.shopid
inner join st_region_shop c on c.id_pos = substr(a.shopid,1,2)
inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(b.shopnum,2)
;
--

--3
select "vendor_code","size","id_store","date",sum("count") "count"
from (
	select b.art "vendor_code",b.asize "size",substr(c.shopnum,2) "id_store",to_char(a.sale_date,'yyyy-mm-dd') "date",sum(case when a.bit_vozvr = 'T' then -b.kol else b.kol end) "count"
	from pos_sale1 a
	inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
	inner join s_shop c on c.shopid = a.id_shop
	inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
	where a.bit_close = 'T' and b.scan != ' '
	and to_char(a.sale_date,'yyyymmdd') >= '20160101' and to_char(a.sale_date,'yyyymm') <= '201902'
	group by b.art,b.asize,substr(c.shopnum,2),to_char(a.sale_date,'yyyy-mm-dd')
	union all
	select b.art "vendor_code",b.asize "size",substr(c.shopnum,2) "id_store",to_char(a.dated,'yyyy-mm-dd') "date",sum(-b.kol) "count"
	from d_prixod1 a
	inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop
	inner join s_shop c on c.shopid = a.id_shop
	inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
	where a.bit_close = 'T' and a.idop = '03' and b.scan != ' '
	and to_char(a.dated,'yyyymmdd') >= '20160101' and to_char(a.dated,'yyyymm') <= '201902'
	group by b.art,b.asize,substr(c.shopnum,2),to_char(a.dated,'yyyy-mm-dd')
)
group by "vendor_code","size","id_store","date"
having sum("count") != 0
;
--

--if (P.MESSYESNO("...���� ����������?") == 0) {
--                    P.SQLEXECT("select distinct \"id_store\" ID_STORE\n"
--                            + "from (select \"vendor_code\",\"size\",\"id_store\",\"date\",sum(\"count\") \"count\"\n"
--                            + "from (\n"
--                            + "	select b.art \"vendor_code\",b.asize \"size\",substr(c.shopnum,2) \"id_store\",to_char(a.sale_date,'yyyy-mm-dd') \"date\",sum(case when a.bit_vozvr = 'T' then -b.kol else b.kol end) \"count\"\n"
--                            + "	from pos_sale1 a\n"
--                            + "	inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop\n"
--                            + "	inner join s_shop c on c.shopid = a.id_shop\n"
--                            + "	inner join st_shop d on c.shopid = d.shopid and d.org_kod = 'SHP'\n"
--                            + "	where a.bit_close = 'T' and b.scan != ' '\n"
--                            + "	and to_char(a.sale_date,'yyyymmdd') >= '20190312' and to_char(a.sale_date,'yyyymmdd') <= '20190627'\n"
--                            + "	group by b.art,b.asize,substr(c.shopnum,2),to_char(a.sale_date,'yyyy-mm-dd')\n"
--                            + "	union all\n"
--                            + "	select b.art \"vendor_code\",b.asize \"size\",substr(c.shopnum,2) \"id_store\",to_char(a.dated,'yyyy-mm-dd') \"date\",sum(-b.kol) \"count\"\n"
--                            + "	from d_prixod1 a\n"
--                            + "	inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop\n"
--                            + "	inner join s_shop c on c.shopid = a.id_shop\n"
--                            + "	inner join st_shop d on c.shopid = d.shopid and d.org_kod = 'SHP'\n"
--                            + "	where a.bit_close = 'T' and a.idop = '03' and b.scan != ' '\n"
--                            + "	and to_char(a.dated,'yyyymmdd') >= '20190312' and to_char(a.dated,'yyyymmdd') <= '20190627'\n"
--                            + "	group by b.art,b.asize,substr(c.shopnum,2),to_char(a.dated,'yyyy-mm-dd')\n"
--                            + ")\n"
--//                            + "where \"id_store\" like '20%'\n"
--                            + "group by \"vendor_code\",\"size\",\"id_store\",\"date\"\n"
--                            + "having sum(\"count\") != 0)\n"
--                            + "order by ID_STORE", "MAGS_TMP");
--                    ArrayList<String> al = new ArrayList<>();
--                    A.GOTOP("MAGS_TMP");
--                    for (int i = 0; i < A.RECCOUNT("MAGS_TMP"); i++) {
--                        al.add(A.GETVALS("MAGS_TMP.ID_STORE"));
--                        A.GOTO("MAGS_TMP", i + 2);
--                    }
--                    for (int ind = 1; ind <= al.size(); ind++) {
--                        System.out.println("Start - " + al.get(ind - 1));
--                        JTable tab1 = P.ORACLE_SERVER_QUERY("select \"vendor_code\",\"size\",\"id_store\",\"date\",sum(\"count\") \"count\"\n"
--                                + "from (\n"
--                                + "	select b.art \"vendor_code\",b.asize \"size\",substr(c.shopnum,2) \"id_store\",to_char(a.sale_date,'yyyy-mm-dd') \"date\",sum(case when a.bit_vozvr = 'T' then -b.kol else b.kol end) \"count\"\n"
--                                + "	from pos_sale1 a\n"
--                                + "	inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop\n"
--                                + "	inner join s_shop c on c.shopid = a.id_shop\n"
--                                + "	inner join st_shop d on c.shopid = d.shopid and d.org_kod = 'SHP'\n"
--                                + "	where a.bit_close = 'T' and b.scan != ' '\n"
--                                + "	and to_char(a.sale_date,'yyyymmdd') >= '20190312' and to_char(a.sale_date,'yyyymmdd') <= '20190627'\n"
--                                + "	group by b.art,b.asize,substr(c.shopnum,2),to_char(a.sale_date,'yyyy-mm-dd')\n"
--                                + "	union all\n"
--                                + "	select b.art \"vendor_code\",b.asize \"size\",substr(c.shopnum,2) \"id_store\",to_char(a.dated,'yyyy-mm-dd') \"date\",sum(-b.kol) \"count\"\n"
--                                + "	from d_prixod1 a\n"
--                                + "	inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop\n"
--                                + "	inner join s_shop c on c.shopid = a.id_shop\n"
--                                + "	inner join st_shop d on c.shopid = d.shopid and d.org_kod = 'SHP'\n"
--                                + "	where a.bit_close = 'T' and a.idop = '03' and b.scan != ' '\n"
--                                + "	and to_char(a.dated,'yyyymmdd') >= '20190312' and to_char(a.dated,'yyyymmdd') <= '20190627'\n"
--                                + "	group by b.art,b.asize,substr(c.shopnum,2),to_char(a.dated,'yyyy-mm-dd')\n"
--                                + ")\n"
--                                + "where \"id_store\" = '" + al.get(ind - 1) + "'\n"
--                                + "group by \"vendor_code\",\"size\",\"id_store\",\"date\"\n"
--                                + "having sum(\"count\") != 0");
--                        System.out.println("Query - " + al.get(ind - 1));
--                        ArrayList<String> arrr = new ArrayList<>();
--                        arrr.add("vendor_code");
--                        arrr.add("size");
--                        arrr.add("id_store");
--                        arrr.add("date");
--                        arrr.add("count");
--                        P.EXCEL_SAVING(tab1, "D:\\Work\\ebanina_ebanaya\\������� ������_" + al.get(ind - 1) + ".xlsx", arrr);
--                        ((DefaultTableModel) tab1.getModel()).getDataVector().removeAllElements();
--                        ((DefaultTableModel) tab1.getModel()).fireTableDataChanged();
--                        tab1 = null;
--                        System.out.println("Complete - " + al.get(ind - 1));
--                    }
--                }

--if (P.MESSYESNO("...���� ����������?") == 0) {
--ArrayList<String> al = new ArrayList<>();
--al.add("02101");
--al.add("02102");
--al.add("02103");
--al.add("02104");
--al.add("02105");
--al.add("02106");
--al.add("02107");
--al.add("02108");
--al.add("02109");
--al.add("02110");
--al.add("02111");
--al.add("02113");
--al.add("02115");
--al.add("02118");
--al.add("02119");
--al.add("02120");
--al.add("02121");
--al.add("02122");
--al.add("02124");
--for (int ind = 1; ind <= 1; ind++) {
--		P.SQLEXECT("select max(rn) rn from map_proj.e_daily_ost2 a inner join s_shop b on a.id_shop = b.shopid where substr(b.shopnum,2) = '" + al.get(ind - 1) + "'", "TMP");
--		int max = (int) Double.parseDouble(A.GETVALS("TMP.RN"));
--		max = (int) (max / 1000000) + 1;
--
--		for (int ind2 = 1; ind2 <= max; ind2++) {
--				System.out.println("Start - " + ind);
--				JTable tab1 = P.ORACLE_SERVER_QUERY("select a.art \"vendor_code\",a.asize \"size\",substr(b.shopnum,2) \"id_store\",to_char(a.dated,'yyyy-mm-dd') \"date\",a.kol \"count\",rn\n"
--								+ "from map_proj.e_daily_ost2 a\n"
--								+ "inner join s_shop b on a.id_shop = b.shopid\n"
--								+ "inner join temp_reports c on c.id_shop = substr(b.shopnum,2) and c.prim = 'prognoz_29042019'\n"
--								+ "where c.id_shop = '" + al.get(ind - 1) + "' and rn >= (1000000*(" + ind2 + "-1))+1 and rn <= (1000000*(" + ind2 + "-1))+1000000");
--				System.out.println("Query - " + ind);
--				ArrayList<String> arrr = new ArrayList<>();
--				arrr.add("vendor_code");
--				arrr.add("size");
--				arrr.add("id_store");
--				arrr.add("date");
--				arrr.add("count");
--				P.EXCEL_SAVING(tab1, "D:\\Work\\ebanina_ebanaya\\������� ��������_" + al.get(ind - 1) + "_" + ind2 + ".xlsx", arrr);
--				((DefaultTableModel) tab1.getModel()).getDataVector().removeAllElements();
--				((DefaultTableModel) tab1.getModel()).fireTableDataChanged();
--				tab1 = null;
--				System.out.println("Complete - " + ind);
--		}
--}
--}

--4
SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor dd is select dt
							from (SELECT TRUNC (SYSDATE - ROWNUM) dt
								FROM DUAL CONNECT BY ROWNUM < 100000
								order by dt)
							where trunc(dt) not in (select trunc(dated) from map_proj.e_daily_ost2 group by trunc(dated))
							and trunc(dt) between to_date('20170404','yyyymmdd') and to_date('20190228','yyyymmdd')
							order by dt;
ddRow dd%rowtype;
p_date date;
BEGIN
 
open dd;
  loop  
       FETCH dd INTO p_date;
       EXIT WHEN dd%NOTFOUND;  
          begin
          execute immediate q'[insert into map_proj.e_daily_ost2 (ID_SHOP,DATED,ART,ASIZE,KOL)
          select a.shopid,to_date(']'||to_char(p_date,'ddmmyyyy')||q'[','ddmmyyyy'),a.art,a.asize,sum(a.kol*a.koef)
          from kart_v a
          inner join s_shop b on a.shopid = b.shopid and substr(b.shopnum,2) in ('23019','26102','02107','17101','23021','26103','02108','17104','23022','26104','02109',
						'17107','23010','24110','02110','17112','23011','24101','02113','17116','23012','24107',
						'02115','17118','23014','24109','02120','17121','23016','24104','02124','17122','23017',
						'02101','17124','23018','02102','23013','02103','06101','02111','06120','02119','06130',
						'02122','06133','06137','06140','06141','06144','06145','06146','06147')
          where to_char(a.dated,'yyyyMMdd') < to_char(to_date(']'||to_char(p_date,'ddmmyyyy')||q'[','ddmmyyyy'),'yyyyMMdd')
          and a.scan != ' '
          group by a.shopid,to_date(']'||to_char(p_date,'ddmmyyyy')||q'[','ddmmyyyy'),a.art,a.asize
          having sum(a.kol*a.koef) > 0]'
          ;
          commit;
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
  
END ;

SET serveroutput ON
begin
	for cur in (select b.shopid from temp_reports a inner join s_shop b on a.id_shop = substr(b.shopnum,2) where prim = 'prognoz_02072019' and id_shop not in ('02102','02101') order by b.shopid) loop
		begin
			update map_proj.e_daily_ost2 a
				set rn = rownum
				where id_shop = cur.shopid;
			dbms_output.put_line(cur.shopid||' - '||sql%rowcount);
			commit;
		end;
	end loop;
end;

select art "vendor_code",asize "size",substr(b.shopnum,2) "id_store",to_char(a.dated,'yyyy-mm-dd') "date",a.kol "count",rn
from map_proj.e_daily_ost2 a
inner join s_shop b on a.id_shop = b.shopid
where c.id_shop = '0001' and rn >= (1000000*(1-1))+1 and rn <= (1000000*(1-1))+1000000
;
--

--5
drop table map_proj.temp_price_history;
create table map_proj.temp_price_history as
--select count(*)
--from (
select q.art "vendor_code",q.asize "size",substr(y.shopnum,2) "id_store",to_char(q.dateb,'yyyy-mm-dd') "start_date",q.cena "price"
from st_price_shop x
inner join s_shop y on x.id_shop = y.shopid
inner join map_proj.temp_reports z on z.id_shop = substr(y.shopnum,2)
inner join s_price q on q.vidid = x.vidid and q.waers = x.waers
	and to_char(q.dateb,'yyyymmdd') <= to_char(x.datee,'yyyymmdd') and to_char(q.dateb,'yyyymmdd') >= to_char(x.dateb,'yyyymmdd')
where x.is_sop = 'F'
and to_char(x.datee,'yyyymmdd') >= '20160101'
--)
;
commit;

update map_proj.temp_price_history
set rn = rownum;
commit;

select "vendor_code","size","id_store","start_date","price",rn
from map_proj.temp_price_history
where rn >= (1000000*(ind-1))+1 and rn <= (1000000*(ind-1))+1000000
;
--

--6
create table map_proj.temp_promo_history as
--select count(*)
--from (
select q.art "vendor_code",q.asize "size",q.shopnum "id_store",
q.discription "description",q.proc "sale_size",q.fix "sale_size_fix",q.disc_type_name,
to_char(q.dates,'yyyy-mm-dd') "start_date",to_char(q.datee,'yyyy-mm-dd') "end_date"
from (
			select b.art,c.asize,a.discription,
			case when a.disc_type_id = 'ZK01' then b.procent else 0 end proc,
			case when a.disc_type_id = 'ZK02' then b.cena else 0 end fix,
			f.disc_type_name,a.dates,a.datee,
			substr(y.shopnum,2) shopnum
			from pos_skidki1 a
			inner join pos_skidki2 b on a.docid = b.docid
			inner join pos_skidki3 d on a.docid = d.docid
			inner join s_size_scale c on b.art = c.art
			inner join s_art e on e.art = b.art
			inner join s_discount_type f on a.disc_type_id = f.disc_type_id
			inner join s_shop y on d.shopid = y.shopid
			inner join map_proj.temp_reports z on z.id_shop = substr(y.shopnum,2)
			where a.disc_type_id in ('ZK01','ZK02') and e.mtart not in ('ZHW3','ZROH')
			and '20160101' <= to_char(a.datee,'yyyymmdd')
) q
--)
;
commit;

select "vendor_code","size","id_store",
"description","sale_size","sale_size_fix",disc_type_name "name",
"start_date","end_date"
from map_proj.temp_promo_history
where rn >= (1000000*(1-1))+1 and rn <= (1000000*(1-1))+1000000
;

--drop table map_proj.temp_promo_history;
--create table map_proj.temp_promo_history as
--select count(*)
--from (
--select q.art "vendor_code",q.asize "size",substr(y.shopnum,2) "id_store",
--q.discription "description",q.proc "sale_size",q.fix "sale_size_fix",q.disc_type_name,
--to_char(q.dates,'yyyy-mm-dd') "start_date",to_char(q.datee,'yyyy-mm-dd') "end_date"
--from st_price_shop x
--inner join s_shop y on x.id_shop = y.shopid
--inner join map_proj.temp_reports z on z.id_shop = substr(y.shopnum,2)
--inner join map_proj.temp_promo_history1 q on q.vidid = x.vidid
--	and to_char(q.dates,'yyyymmdd') <= to_char(x.datee,'yyyymmdd') and to_char(q.dates,'yyyymmdd') >= to_char(x.dateb,'yyyymmdd')
--where to_char(x.datee,'yyyymmdd') >= '20160101'
--)
--;
--commit;


--