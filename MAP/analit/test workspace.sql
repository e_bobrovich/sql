select a.id_shop, a.dated, count(*) from  map_proj.e_daily_ost a
inner join map_proj.s_shop b on a.id_shop = b.shopid

where to_char(a.dated,'yyyyMMdd') >= '20190304' and to_char(a.dated,'yyyyMMdd') <= '20190626'
and substr(b.shopnum,2,5) in (
    '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
    '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
    '00008','00009','00017','00020','00031','00033','00041','00047','00063'
)
group by a.id_shop, a.dated
order by a.id_shop, a.dated
;

select max(a.dated) from  map_proj.e_daily_ost a
inner join map_proj.s_shop b on a.id_shop = b.shopid

where to_char(a.dated,'yyyyMMdd') >= '20190304' and to_char(a.dated,'yyyyMMdd') <= '20190626'
and substr(b.shopnum,2,5) in (
    '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
    '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
    '00008','00009','00017','00020','00031','00033','00041','00047','00063'
)
;

--------------------------------------------------------------------------------
select * from map_proj.e_daily_ost a
inner join s_shop c on a.id_shop = c.shopid

;

select * from temp_reports where prim = 'prognoz_02072019';


select distinct a.dated from map_proj.e_daily_ost a 
inner join s_shop c on a.id_shop = c.shopid
inner join temp_reports b on substr(c.shopnum,2) = b.id_shop
where b.prim = 'prognoz_02072019'
and to_char(a.dated,'yyyyMMdd') >= '20190304' and to_char(a.dated,'yyyyMMdd') <= '20190626'
;

insert into map_proj.e_daily_ost2
select a.*, null from map_proj.e_daily_ost a
;


select id_shop, max(rn) from map_proj.e_daily_ost2 where rn is not null group by id_shop order by id_shop;

select a.art "vendor_code", a.asize "size", substr(b.shopnum,2) "id_store", to_char(a.dated,'yyyy-mm-dd') "date", a.kol "count", a.rn
from map_proj.e_daily_ost2 a
inner join s_shop b on a.id_shop = b.shopid
inner join temp_reports c on substr(b.shopnum,2) = c.id_shop
where c.prim = 'prognoz_02072019'
and to_char(a.dated,'yyyyMMdd') >= '20190304' and to_char(a.dated,'yyyyMMdd') <= '20190626'
and a.rn >= (1000000*(161-1))+1 and a.rn <= (1000000*(161-1))+1000000
;


select a.art "vendor_code",a.asize "size",substr(b.shopnum,2) "id_store",to_char(a.dated,'yyyy-mm-dd') "date",a.kol "count",rn
from map_proj.e_daily_ost2 a
inner join s_shop b on a.id_shop = b.shopid
inner join temp_reports c on c.id_shop = substr(b.shopnum,2) and c.prim = 'prognoz_02072019'
where c.id_shop = '00010' and rn >= (1000000*(1-1))+1 and rn <= (1000000*(1-1))+1000000;
--------------------------------------------------------------------------------
--delete from map_proj.e_daily_ost a
--where to_char(a.dated,'yyyyMMdd') >= '20190304';

select trunc(dated) from map_proj.e_daily_ost a
inner join map_proj.s_shop b on a.id_shop = b.shopid
where substr(b.shopnum,2,5) in (
            '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
            '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
            '00008','00009','00017','00020','00031','00033','00041','00047','00063'
        ) group by trunc(dated);

select a.shopid,a.art,a.asize,sum(a.kol*a.koef)
from kart_v a
inner join map_proj.s_shop b on a.shopid = b.shopid
where to_char(a.dated,'yyyyMMdd') < '20190626'
        --and b.shopid like '25%'
        and substr(b.shopnum,2,5) in (
            '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
            '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
            '00008','00009','00017','00020','00031','00033','00041','00047','00063'
        )
and a.scan != ' '
group by a.shopid,a.art,a.asize
having sum(a.kol*a.koef) > 0;


select * from map_proj.s_shop
where substr(shopnum,2,5) in (
            '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
            '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
            '00008','00009','00017','00020','00031','00033','00041','00047','00063'
        );
        
        
---------------------------------------------------------------------------------   
select "id_receipt","id_store","date",sum("amount") "amount"
from (
	select a.id_chek "id_receipt", substr(c.shopnum,2) "id_store",to_char(a.sale_date,'yyyy-mm-dd') "date", sum(case when a.bit_vozvr = 'T' then -b.cena3*b.kol else b.cena3*b.kol  end) "amount"--, sum(b.kol) kol --sum(case when a.bit_vozvr = 'T' then -b.kol else b.kol end) "count"
    from pos_sale1 a
    inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
    inner join s_shop c on c.shopid = a.id_shop
    inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
    where a.bit_close = 'T' and b.scan != ' '
    and to_char(a.sale_date,'yyyymmdd') >= '20190304' and to_char(a.sale_date,'yyyymmdd') <= '20190626'
    group by a.id_chek, substr(c.shopnum,2),to_char(a.sale_date,'yyyy-mm-dd'),  case when a.bit_vozvr = 'T' then -a.sale_sum else a.sale_sum end
	union all
	select a.id "id_receipt", substr(c.shopnum,2) "id_store",to_char(a.dated,'yyyy-mm-dd') "date",sum(-b.cena3*b.kol) "amount"
    from d_prixod1 a
    inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop
    inner join s_shop c on c.shopid = a.id_shop
    inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
    where a.bit_close = 'T' and a.idop = '03' and b.scan != ' '
    and to_char(a.dated,'yyyymmdd') >= '20190304' and to_char(a.dated,'yyyymmdd') <= '20190626'
    group by a.id, substr(c.shopnum,2),to_char(a.dated,'yyyy-mm-dd')
)
group by "id_receipt","id_store","date"
having sum("amount") != 0
;

select a.id_chek "id_receipt", substr(c.shopnum,2) "id_store",to_char(a.sale_date,'yyyy-mm-dd') "date", sum(case when a.bit_vozvr = 'T' then -b.cena3*b.kol else b.cena3*b.kol  end) "amount"--, sum(b.kol) kol --sum(case when a.bit_vozvr = 'T' then -b.kol else b.kol end) "count"
from pos_sale1 a
inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
inner join s_shop c on c.shopid = a.id_shop
inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
where a.bit_close = 'T' and b.scan != ' '
and to_char(a.sale_date,'yyyymmdd') >= '20190304' and to_char(a.sale_date,'yyyymmdd') <= '20190626'
group by a.id_chek, substr(c.shopnum,2),to_char(a.sale_date,'yyyy-mm-dd'),  case when a.bit_vozvr = 'T' then -a.sale_sum else a.sale_sum end
;

select a.id "id_receipt", substr(c.shopnum,2) "id_store",to_char(a.dated,'yyyy-mm-dd') "date",sum(-b.cena3*b.kol) "amount"
from d_prixod1 a
inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop
inner join s_shop c on c.shopid = a.id_shop
inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
where a.bit_close = 'T' and a.idop = '03' and b.scan != ' '
and to_char(a.dated,'yyyymmdd') >= '20190304' and to_char(a.dated,'yyyymmdd') <= '20190626'
group by a.id, substr(c.shopnum,2),to_char(a.dated,'yyyy-mm-dd')
;