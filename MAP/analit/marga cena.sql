-- ЗАЧИСТКА МАРЖИ
delete from TDV_MAP_SHOP_MARGA_MONTH where marga is null;
select * from TDV_MAP_SHOP_MARGA_MONTH where marga is null;

-- ПЕРЕИМЕНОВЫВАНИЕ ОТДЕЛЕНИЙ МАРЖИ
update tdv_map_shop_marga_month set id_shop = '00'||substr(id_shop,3,2) where substr(id_shop,1,2) = '70';
select * from TDV_MAP_SHOP_MARGA_MONTH where id_shop = '0001';

select a.art "vendor_code", a.asize "size", substr(b.shopnum,2) "id_store", to_char(a.dated,'yyyy-mm-dd') "date", a.kol "count"
from map_proj.e_daily_ost a
inner join s_shop b on a.id_shop = b.shopid
inner join temp_reports c on substr(b.shopnum,2) = c.id_shop
where c.prim = 'prognoz_02072019'
and to_char(a.dated,'yyyyMMdd') = '20190626'
;

insert into map_proj.MARGA_OST_PRICE_DATE
select a.id_shop, a.art, a.asize, a.dated, a.kol, null, null, null
from map_proj.e_daily_ost a
inner join s_shop b on a.id_shop = b.shopid
inner join temp_reports c on substr(b.shopnum,2) = c.id_shop
where c.prim = 'prognoz_02072019'
and to_char(a.dated,'yyyyMMdd') = '20190626'
;

select * from map_proj.marga_temp;
select aszie_str from map_proj.marga_temp group by aszie_str;
select asize from map_proj.marga_temp group by asize;
select a.*, cast(aszie_str as number) / 10 from map_proj.marga_temp a;
select cast('20,91' as number) from dual;

update map_proj.marga_temp set id_shop = '00'||substr(id_shop,3,2) where substr(id_shop,1,2) = '70';
update map_proj.marga_temp set aszie_str = replace(aszie_str, 'F', '');
update map_proj.marga_temp set aszie_str = replace(aszie_str, 'A', '');
update map_proj.marga_temp set asize = cast(aszie_str as number) / 10;
update map_proj.marga_temp set marga = cast(marga_str as number);


select * from map_proj.marga_ost_price_date order by id_shop, art, asize;
select * from map_proj.marga_temp;

update map_proj.marga_ost_price_date a 
set marga = (
    select marga from map_proj.marga_temp b where b.id_shop = a.id_shop and b.art = a.art and b.asize = a.asize 
)
where exists (
    select marga from map_proj.marga_temp b where b.id_shop = a.id_shop and b.art = a.art and b.asize = a.asize 
);

select id_shop, art, asize from map_proj.MARGA_OST_PRICE_DATE order by id_shop, art, asize;


select a.*, b.priceid, c.final_price  from map_proj.marga_ost_price_date a
inner join (select shopid , priceid, landid from st_shop where org_kod = 'SHP') b on a.id_shop = b.shopid
inner join  
    table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4409'), to_date('26.06.2019', 'dd.mm.yyyy')))
c on b.priceid = c.vidid and b.landid = case when b.priceid = '01' then c.reg else b.landid end and a.art = c.art
;

update map_proj.marga_ost_price_date w 
set price = (
    select c.final_price  from map_proj.marga_ost_price_date a
    inner join (select shopid , priceid, landid from st_shop where org_kod = 'SHP') b on a.id_shop = b.shopid
    inner join map_proj.price_date
    c on b.priceid = c.vidid and b.landid = case when b.priceid = '01' then c.reg else b.landid end and a.art = c.art and c.dated = to_date('26.06.2019', 'dd.mm.yyyy')
    where w.id_shop = a.id_shop and w.art = a.art
    group by a.id_shop, a.art, c.final_price
)
where exists (
    select a.id_shop, a.art, c.final_price from map_proj.marga_ost_price_date a
    inner join (select shopid , priceid, landid from st_shop where org_kod = 'SHP') b on a.id_shop = b.shopid
    inner join map_proj.price_date
    c on b.priceid = c.vidid and b.landid = case when b.priceid = '01' then c.reg else b.landid end and a.art = c.art and c.dated = to_date('26.06.2019', 'dd.mm.yyyy')
    where w.id_shop = a.id_shop and w.art = a.art
    group by a.id_shop, a.art, c.final_price
);

select c.final_price  from map_proj.marga_ost_price_date a
    inner join (select shopid , priceid, landid from st_shop where org_kod = 'SHP') b on a.id_shop = b.shopid
    inner join map_proj.price_date
    c on b.priceid = c.vidid and b.landid = case when b.priceid = '01' then c.reg else b.landid end and a.art = c.art and c.dated = to_date('26.06.2019', 'dd.mm.yyyy')
    where '0001' = a.id_shop and '013004' = a.art
    group by a.id_shop, a.art, c.final_price
    ;
    
    
select a.id_shop from map_proj.marga_ost_price_date a
group by a.id_shop order by a.id_shop
;

select a.art "vendor_code", a.asize "size", a.id_shop "id_store", nvl(a.marga,0) "margin", a.price "price" from map_proj.marga_ost_price_date a
where a.id_shop = '0001'
;