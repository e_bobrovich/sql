select "id_receipt","id_store","date",sum("amount") "amount"
from (
	select a.id_chek "id_receipt", substr(c.shopnum,2) "id_store",to_char(a.sale_date,'yyyy-mm-dd') "date", sum(case when a.bit_vozvr = 'T' then -b.cena3*b.kol else b.cena3*b.kol  end) "amount"--, sum(b.kol) kol --sum(case when a.bit_vozvr = 'T' then -b.kol else b.kol end) "count"
    from pos_sale1 a
    inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
    inner join s_shop c on c.shopid = a.id_shop
    inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
    where a.bit_close = 'T' and b.scan != ' '
    and to_char(a.sale_date,'yyyymmdd') >= '20190304' and to_char(a.sale_date,'yyyymmdd') <= '20190626'
    group by a.id_chek, substr(c.shopnum,2),to_char(a.sale_date,'yyyy-mm-dd'),  case when a.bit_vozvr = 'T' then -a.sale_sum else a.sale_sum end
	union all
	select a.id "id_receipt", substr(c.shopnum,2) "id_store",to_char(a.dated,'yyyy-mm-dd') "date",sum(-b.cena3*b.kol) "amount"
    from d_prixod1 a
    inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop
    inner join s_shop c on c.shopid = a.id_shop
    inner join temp_reports d on d.prim = 'prognoz_02072019' and d.id_shop = substr(c.shopnum,2)
    where a.bit_close = 'T' and a.idop = '03' and b.scan != ' '
    and to_char(a.dated,'yyyymmdd') >= '20190304' and to_char(a.dated,'yyyymmdd') <= '20190626'
    group by a.id, substr(c.shopnum,2),to_char(a.dated,'yyyy-mm-dd')
)
group by "id_receipt","id_store","date"
having sum("amount") != 0
;


select b.art "vencor_code", b.asize "size", substr(c.shopnum,2) "id_store", to_char(a.sale_date,'yyyy-mm-dd') "date", sum(b.kol) "count" 
from pos_sale1 a
inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek
inner join s_shop c on a.id_shop = c.shopid
where a.id_shop = '2601' --and rn >= (1000000*(1-1))+1 and rn <= (1000000*(1-1))+1000000
and a.bit_vozvr = 'F' and scan != ' '
group by b.art, b.asize, substr(c.shopnum,2), to_char(a.sale_date,'yyyy-mm-dd')
;