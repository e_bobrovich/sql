
SET serveroutput ON

select * from map_proj.e_daily_ost
where 
--dated between to_date('20190304','yyyymmdd') and to_date('20190626','yyyymmdd')
to_char(dated, 'yyyymmdd') = '20190304' --and to_char(dated, 'yyyymmdd') <= '20190626'
;



declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor dd is select dt
							from (SELECT TRUNC (SYSDATE - ROWNUM) dt
								FROM DUAL CONNECT BY ROWNUM < 100000
								order by dt)
							where trunc(dt) not in (
--                                select trunc(dated) from map_proj.e_daily_ost where id_shop like '25%' group by trunc(dated)
                                select trunc(dated) from map_proj.e_daily_ost a
                                inner join map_proj.s_shop b on a.id_shop = b.shopid
                                where substr(b.shopnum,2,5) in (
                                            '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
                                            '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
                                            '00008','00009','00017','00020','00031','00033','00041','00047','00063'
                                ) group by trunc(dated)
                            )
							and trunc(dt) between to_date('20190304','yyyymmdd') and to_date('20190626','yyyymmdd')
							order by dt;
ddRow dd%rowtype;
p_date date;
BEGIN
 
open dd;
  loop  
       FETCH dd INTO p_date;
       EXIT WHEN dd%NOTFOUND;  
          begin
          insert into map_proj.e_daily_ost (ID_SHOP,DATED,ART,ASIZE,KOL)
          select a.shopid,p_date,a.art,a.asize,sum(a.kol*a.koef)
          from kart_v a
          inner join map_proj.s_shop b on a.shopid = b.shopid
          where to_char(a.dated,'yyyyMMdd') < to_char(p_date,'yyyyMMdd')
					--and b.shopid like '25%'
                    and substr(b.shopnum,2,5) in (
                        '00010','00019','00026','00029','00042','00043','00044','00053','00060','00061',
                        '00062','00065','00068','00069','00071','00074','00001','00002','00003','00004',
                        '00008','00009','00017','00020','00031','00033','00041','00047','00063'
                    )
          and a.scan != ' '
          group by a.shopid,p_date,a.art,a.asize
          having sum(a.kol*a.koef) > 0
          ;
          commit;
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
  
END ;


--------------------------------------------------------------------------------
select count(*) from  map_proj.e_daily_ost2;
--delete from map_proj.e_daily_ost2;

--truncate table map_proj.e_daily_ost2;

insert into map_proj.e_daily_ost2
select a.*, null from map_proj.e_daily_ost a
inner join s_shop c on a.id_shop = c.shopid
inner join temp_reports b on substr(c.shopnum,2) = b.id_shop
where b.prim = 'prognoz_02072019'
and to_char(a.dated,'yyyyMMdd') >= '20190304' and to_char(a.dated,'yyyyMMdd') <= '20190626'
;

select max(rn) rn from map_proj.e_daily_ost2 a inner join s_shop b on a.id_shop = b.shopid where substr(b.shopnum,2) ='00001';

SET serveroutput ON
begin
	for cur in (select b.shopid from temp_reports a inner join s_shop b on a.id_shop = substr(b.shopnum,2) where prim = 'prognoz_02072019' and id_shop not in ('02102','02101') order by b.shopid) loop
		begin
			update map_proj.e_daily_ost2 a
				set rn = rownum
				where id_shop = cur.shopid;
			dbms_output.put_line(cur.shopid||' - '||sql%rowcount);
			commit;
		end;
	end loop;
end;