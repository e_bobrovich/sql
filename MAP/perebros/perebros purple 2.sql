--1608B
--1637225

select * from t_map_new_poexalo where id = 14436;

select * from tdv_test100 where art = '1608B';

--select a.id_shop, a.art, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, a1.style
select a.id_shop /*x.shopid*/ id_shop, a.art, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, a1.style
from tdv_test100 a
left join s_art a1 on a1.art = a.art
inner join tdm_map_new_shop_group x on x.s_group = 38101
                                     and a.id_shop = x.shopid
                                     --and (nvl(x.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                                     and x.season_in like '%' || a1.season || '%'
left join tdv_map_shop_reit b on a.id_shop = b.id_shop
left join (select distinct id_shop, analog
        from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                          and a.art = c.analog
where (a.id_shop, a.art) not in (select x.id_shop, x.art
                               from TDV_NEW_SHOP_OST x
                               where x.verme > 0) -- которых нет в транзитах
    
    ----------------------------------------------------
    --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
    and x.count_limit_in > 0
    --and x.count_limit_out > 0
    ----------------------------------------------------
    
    and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
    and a.id_shop not in
    (select a.id_shop
         from t_map_new_poexalo a
         inner join tdm_map_new_shop_group x on x.s_group = 38101
                                                and a.id_shop = x.shopid
         left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
         where id = 14435
         group by a.id_shop, x.count_limit_in, y.kol_in
         having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
--group by a.id_shop, a.art, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, a1.style
group by a.id_shop /*x.shopid*/, a.art, season, x.shopid, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, a1.style
order by nvl(b.reit2, 0);

select distinct a.asize, sum(kol) kol
from T_MAP_NEW_OTBOR a
where a.analog = '1608B'
   and a.kol > 0
   and a.id_shop not in
   (select id_shop_from
        from t_map_new_poexalo a
        inner join tdm_map_new_shop_group x on x.s_group = 38101
                                               and a.id_shop_from = x.shopid
        left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
        where id = 14435
        group by id_shop_from, x.count_limit_out, y.kol_out
        having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
   and (nvl(null, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
group by a.asize
order by a.asize;


select count(distinct a.id_shop)
from t_map_new_poexalo a
inner join tdm_map_new_shop_group x on x.s_group = 38101
                                       and a.id_shop = x.shopid
left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
where id = 14435
      and a.id_shop = '3105'
group by a.id_shop, x.count_limit_in, y.kol_in
having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);


--insert into t_map_new_poexalo
select '3105', art, 'Всесезонная', 1, asize, 1, id_shop, 14435, systimestamp, 'purple', 0, null owner,
     null text1, null text2, null text3
from (select a.*
     from T_MAP_NEW_OTBOR a
     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
     left join (select id_shop, sum(kol_sale) kol_sale
               from TDV_MAP_NEW_OST_SALE
               where art = '1608B'
               group by id_shop) c on a.id_shop = c.id_shop
     where analog = '1608B'
           and asize = 45
           and kol > 0
           and a.id_shop not in
           (select id_shop_from
                from t_map_new_poexalo a
                inner join tdm_map_new_shop_group x on x.s_group = 38101
                                                       and a.id_shop_from = x.shopid
                left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                where id = 14435
                group by id_shop_from, x.count_limit_out, y.kol_out
                having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
           and (nvl(null, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
     order by c.kol_sale, a.kol, b.reit2)
where rownum = 1;

select * from t_map_new_poexalo where id = 14435;

select count(asize),
nvl(sum(
    case when asize in (4, 4.5, 5, 5.5)    and 'Мужские' = 'Женские' and ' ' != 'Sport' then 1
         when asize in (8, 8.5, 9)         and 'Мужские' = 'Мужские' and ' ' != 'Sport' then 1
         when asize in (5, 5.5, 6, 6.5)    and 'Мужские' = 'Женские' and ' ' = 'Sport' then 1
         when asize in (8.5, 9, 9.5, 10)   and 'Мужские' = 'Мужские' and ' ' = 'Sport' then 1
         when asize in (37, 38, 39)        and 'Мужские' = 'Женские' then 1
         when asize in (42, 43)            and 'Мужские' = 'Мужские' then 1
         else 0 end), 0) 
from (
--                select distinct asize from tdv_map_new_ost_sale where id_shop = i_id_shop and art = i_art and kol_ost > 0
--                union all
--                select distinct asize from tdv_new_shop_ost where id_shop = i_id_shop and art = i_art and verme > 0
--                union all
    select distinct asize from t_map_new_poexalo where id = 14435 and id_shop = '3105' and /*text1*/ art = '1608B' --and kol > 0 and flag = 1
);

select '3105', art, 'Всесезонная', 1, asize, 1, id_shop, 14435, systimestamp, 'purple', 0, null owner,
     null text1, null text2, null text3
from (select a.*
     from T_MAP_NEW_OTBOR a
     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
     left join (select id_shop, sum(kol_sale) kol_sale
               from TDV_MAP_NEW_OST_SALE
               where art = '1608B'
               group by id_shop) c on a.id_shop = c.id_shop
     where analog = '1608B'
           --and asize = 45
           and kol > 1
           and a.id_shop not in
           (select id_shop_from
                from t_map_new_poexalo a
                inner join tdm_map_new_shop_group x on x.s_group = 38101
                                                       and a.id_shop_from = x.shopid
                left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                where id = 14435
                group by id_shop_from, x.count_limit_out, y.kol_out
                having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
           and (nvl(null, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
     order by c.kol_sale, a.kol, b.reit2)
where rownum = 1;