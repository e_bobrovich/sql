
insert into tdm_map_new_shop_group
select 66102, shopid, dtype, ' ', dtype_out, cluster_name, season_in, season_out, count_limit_in, count_limit_out, shop_in, ost_month
from tdm_map_new_shop_group where s_group = 66101;

insert into  T_MAP13_STOCK--t_map13_no_stock
  select distinct a.art, nvl(c.analog, a.art)
  from cena_skidki_all_shops a
  left join TDV_MAP_NEW_ANAL c on a.art = c.art
  where substr(to_char(cena), -2) = '99';
commit;

select * from t_map13_no_stock;

select * from tdv_map_black_list_all;

select a.id_shop id_shop, a.art, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, a1.style, stks.id_Shop id_Shops, stka.analog analogs
from tdv_test100 a
left join s_art a1 on a1.art = a.art
inner join tdm_map_new_shop_group x on x.s_group = 62003
                                       and a.id_shop = x.shopid
                                       --and (nvl(x.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                                       and x.season_in like '%' || a1.season || '%'
left join tdv_map_shop_reit b on a.id_shop = b.id_shop
--left join (select distinct id_shop, analog
--          from tdv_map_black_list_all) c on c.id_shop = a.id_shop
--                                            and a.art = c.analog
left join t_map13_no_stock stks on a.id_shop = stks.id_shop
left join T_MAP13_STOCK stka on a.art = stka.analog
where (a.id_shop, a.art) not in (select x.id_shop, x.art
                                 from TDV_NEW_SHOP_OST x
                                 where x.verme > 0) -- которых нет в транзитах
      
      ----------------------------------------------------
      --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
      and x.count_limit_in > 0
      --and x.count_limit_out > 0
      ----------------------------------------------------
      -- без покупных
      and a1.facture != 'Покупная'
      ----------------------------------------------------
--      and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
      ----------------------------------------------------
      -- 09.01.2020 Bobrovich Исключение стоков для магазинов из списка
      and case when stks.id_shop is not null then stka.analog else null end is null
      ----------------------------------------------------
--      and ((stks.id_shop is not null and stka.analog is null) or (stks.id_shop is null))
--      and case when stks.id_shop is not null then stka.analog else null end is null
      ----------------------------------------------------
      and a.id_shop not in
      (select a.id_shop
           from t_map_new_poexalo a
           inner join tdm_map_new_shop_group x on x.s_group = 62003
                                                  and a.id_shop = x.shopid
           left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
           where id = 123123312
           group by a.id_shop, x.count_limit_in, y.kol_in
           having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
--group by a.id_shop, a.art, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, a1.style
group by a.id_shop , a.art, season, x.shopid, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, a1.style, stks.id_Shop, stka.analog
order by nvl(b.reit2, 0)
;


select * from t_map_new_poexalo a
inner join t_map13_no_stock b on a.id_shop = b.id_shop
inner join T_MAP13_STOCK c on a.art = c.art
where id = 19097;