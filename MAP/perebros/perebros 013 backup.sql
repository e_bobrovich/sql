create or replace package body tdv_map_013 is

  --************** vaiable *****************************
  pv_rc varchar2(1) default 'F';
  pv_exclude_buy varchar2(1) default 'F'; -- если этот флаг установлен в 'T' , то исключаем из перебросок покупную, образцы, а также обувь на 'Р' и 'Р2' 
  pv_sgp_rasp varchar2(1) default 'F'; -- является ли данный расчет распределением с СГП
  pv_removal_shoes_to_rc varchar2(1) default 'F'; -- является ли данный расчет отгрузкой на РЦ по лимиту 
  pv_is_from_single_shop varchar2(1) default 'F'; -- является ли данный расчет распределением с одного магазина  
  --********************************************************************************************
  --  PROCEDURE
  -- запись в лог
  --********************************************************************************************

  procedure writeLog(i_text varchar2,
                     i_proc varchar2,
                     i_id   in integer) is
  begin
    write_log(i_text, i_proc, i_id);
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: возвращает номер расчета на 1,2,3.. месяца назад
  --
  --****************************************************************************************************************************************************
  function getOstCalculationID(i_months_ago in integer) return e_oborot1.id%type is
    v_id e_oborot1.id%type;
  begin
  
    select id
    into v_id
    from e_oborot1
    where text like '%$MAP#%' || i_months_ago * 30 || '%'
          and date_r = (select max(date_r)
                        from e_oborot1
                        where text like '%$MAP#%' || i_months_ago * 30 || '%');
  
    return v_id;
  end;

  --********************************************************************************************
  --  
  -- 
  --********************************************************************************************
  /*procedure blockStart(i_block_no in integer) is 
    pragma autonomous_transaction;
  begin
    pv_CurBlockNo := i_block_no;
    pv_CurBlockStartTime := dbms_utility.get_time;
    
    update TDV_MAP_13_RESULT set ACTIVE_BLOCK = i_block_no, TIME_START_BLOCK = systimestamp where id = pv_id; 
    commit; 
  end ;                       
  
  procedure blockEnd(i_id number,
                     i_block_text varchar2) is 
  begin
    
    if pv_CurBlockNo is null then 
      return;
    end if;  
    
    writeLog( 'Fill_ross3/Block: '|| pv_CurBlockNo ||'. '||i_block_text  , (dbms_utility.get_time - pv_CurBlockStartTime) / 100  , i_id );  
    pv_CurBlockNo := null;
    pv_CurBlockStartTime := null;
  end ;    */

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: возвращает номер расчета на 1,2,3.. месяца назад
  --
  --****************************************************************************************************************************************************
  procedure warehouse8countbox(i_id number) is
    v_kol number;
  begin
    execute immediate 'truncate table tdv_map_warehouse_stock';
    execute immediate 'truncate table tdv_map_warehouse_result';
  
    -- собираем остатки в парах по 8-му складу
    -- короба
    insert into tdv_map_warehouse_stock
      select art, asize, sum(kol) kol, boxty
      from ( -- короба
             select d.art, null asize, sum(d.verme) / f.kol kol, d.boxty
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join (select boxty, sum(kol) kol
                        from T_SAP_BOX_ROST
                        group by boxty) f on d.boxty = f.boxty
             where d.boxty not in (' ', 'N')
                   and lgtyp in ('R08', 'Z08', 'S08')
                   and e.season in ('Зимняя', 'Утепленная')
             group by d.art, d.boxty, f.kol
             union all
             
             -- россыпь 
             select d.art, d1.asize, sum(d.verme) kol, null
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join s_all_mat d1 on d.matnr = d1.matnr
             where d.boxty in (' ')
                   and d1.asize != 0
                   and lgtyp in ('R08', 'Z08', 'S08')
                   and e.season in ('Зимняя', 'Утепленная')
             group by d.art, d1.asize)
      group by art, asize, boxty;
  
    -- от россыпи 8-ого склада отнимаю резервы 8-ого склада
    update tdv_map_warehouse_stock x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, asize, sum(verme) verme
                 from tdv_map_new_sap_reserv2 x3
                 where x3.boxty = ' '
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE')
                       and ABLAD = '08'
                 group by art, asize) x2
          where x1.art = x2.art
                and x1.asize = x2.asize)
    where exists (select x1.kol - x2.verme
           from (select art, asize, sum(verme) verme
                  from tdv_map_new_sap_reserv2 x3
                  where x3.boxty = ' '
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE')
                        and ABLAD = '08'
                  group by art, asize) x2
           where x1.asize = x2.asize
                 and x1.art = x2.art);
  
    delete from tdv_map_warehouse_stock
    where kol <= 0;
  
    /*      -- от коробов 8-ого склада отнимаю резервы 8-ого склада
    update tdv_map_warehouse_stock x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, boxty, sum(verme) verme
                 from tdv_map_new_sap_reserv2 x3
                 where  x3.boxty != ' '
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE') 
                       and ABLAD = '08'
                 group by art, boxty) x2
          where x1.art = x2.art
                and x1.boxty = x2.boxty)
    where exists (select x1.kol - x2.verme
           from (select art, boxty, sum(verme) verme
                  from tdv_map_new_sap_reserv2 x3
                  where  x3.boxty != ' '
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE') 
                        and ABLAD = '08'
                  group by art, boxty) x2
           where x1.art = x2.art
                 and x1.boxty = x2.boxty)  ; */
  
    /*    
    -- перебираем короба
    for r_row in (select id_shop, a.art, a.boxty, 1 kol
                  from (select distinct id_shop, a.art, a.boxty, 1 kol 
                        from t_map_new_poexalo a 
                        where id = i_id and a.boxty  in (select boxty from T_SAP_BOX_ROST)) a
                  inner join tdv_map_warehouse_stock b on a.art = b.art and a.boxty = b.boxty
                  order by id_shop, a.art)
    loop
        select least(a.kol, r_row.kol) kol
        into v_kol
        from tdv_map_warehouse_stock a
        where art = r_row.art and boxty = r_row.boxty;
        
        insert into tdv_map_warehouse_result
        select r_row.id_shop, r_row.art, null, v_kol, 8, r_row.boxty
        from dual;
        
        update tdv_map_warehouse_stock
        set kol = kol - v_kol
        where art = r_row.art and boxty= r_row.boxty;    
    end loop;
    
    delete from tdv_map_warehouse_result
    where kol = 0;  */
  
    -- остатки коробов забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, null, 1 kol, null lgtype, boxty
      from t_map_new_poexalo a
      where id = i_id
            and a.boxty in (select boxty
                            from T_SAP_BOX_ROST)
            and (id_shop, art, boxty) not in (select id_shop, art, boxty
                                              from tdv_map_warehouse_result
                                              where boxty is not null)
      group by a.id_shop, a.art, boxty;
  
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    -- перебираем все артикула по магазинам куда должна ехать обувь и соотносим с остатками склада
    for r_row in (select id_shop, a.art, a.asize, sum(a.kol) kol
                  from t_map_new_poexalo a
                  inner join tdv_map_warehouse_stock b on a.art = b.art
                                                          and a.asize = b.asize
                  where id = i_id
                        and (a.boxty not in (select boxty
                                             from T_SAP_BOX_ROST) or a.boxty is null)
                  group by id_shop, a.art, a.asize
                  order by id_shop, a.art, a.asize)
    loop
      select least(a.kol, r_row.kol) kol
      into v_kol
      from tdv_map_warehouse_stock a
      where art = r_row.art
            and asize = r_row.asize;
    
      insert into tdv_map_warehouse_result
        select r_row.id_shop, r_row.art, r_row.asize, v_kol, 8, null boxty
        from dual;
    
      update tdv_map_warehouse_stock
      set kol = kol - v_kol
      where art = r_row.art
            and asize = r_row.asize
            and boxty is null;
    end loop;
    commit;
  
    dbms_output.put_line('111');
  
    -- остатки россыпи забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, a.asize, sum(a.kol) - nvl(b.kol, 0) kol, null lgtype, null boxty
      from t_map_new_poexalo a
      left join (select *
                 from tdv_map_warehouse_result
                 where asize is not null) b on a.id_shop = b.id_shop
                                               and a.art = b.art
                                               and a.asize = b.asize
      where id = i_id
            and (a.boxty not in (select boxty
                                 from T_SAP_BOX_ROST) or a.boxty is null)
      group by a.id_shop, a.art, a.asize, nvl(b.kol, 0);
    dbms_output.put_line('1112');
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    commit;
  end;

  --********************************************************************************************
  --  
  -- 
  --********************************************************************************************
  procedure test(i_id               in number,
                 i_s_group          number,
                 i_test_no          in number default null,
                 i_show_only_failed in varchar2 default 'F') is
    pragma autonomous_transaction;
    v_count integer;
    v_test_no number default 0;
    v_start_time timestamp;
  begin
    v_test_no := 1;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы отметили к вывозу те сезоны, которые проставлены в tdm_map_new_shop_group.season_out *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, season
             from tdm_map_new_shop_group x
             inner join (select distinct season
                        from s_art
                        where season != ' ') y on upper(x.season_out) like '%' || upper(y.season) || '%'
             where s_group = i_s_group) a1
      full join (select distinct id_shop, season
                 from TDV_MAP_NEW_OST_SALE a
                 where a.out_block = 0) a2 on a1.id_shop = a2.id_shop
                                              and upper(a1.season) = upper(a2.season)
      where a1.id_shop is null --or a2.id_shop is null
      ;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы отметили к вывозу те сезоны, которые проставлены в tdm_map_new_shop_group.season_out', null,
           null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы отметили к вывозу те сезоны, которые проставлены в tdm_map_new_shop_group.season_out', null,
           null);
      end if;
    end if;
  
    v_test_no := 2;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не вывезли из магазина сезон который нельзя было вывозить *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, season
             from tdm_map_new_shop_group x
             inner join (select distinct season
                        from s_art
                        where season != ' ') y on upper(x.season_out) like '%' || upper(y.season) || '%'
             where s_group = i_s_group) a1
      full join (select distinct id_shop_from id_shop, season
                 from t_map_new_poexalo a
                 where a.id = i_id) a2 on a1.id_shop = a2.id_shop
                                          and upper(a1.season) = upper(a2.season)
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не вывезли из магазина сезон который нельзя было вывозить', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не вывезли из магазина сезон который нельзя было вывозить', null, null);
      end if;
    end if;
  
    v_test_no := 3;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не ввезли в магазина сезон который нельзя было ввозить *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, season
             from tdm_map_new_shop_group x
             inner join (select distinct season
                        from s_art
                        where season != ' ') y on upper(x.season_in) like '%' || upper(y.season) || '%'
             where s_group = i_s_group) a1
      full join (select distinct id_shop id_shop, season
                 from t_map_new_poexalo a
                 where a.id = i_id) a2 on a1.id_shop = a2.id_shop
                                          and upper(a1.season) = upper(a2.season)
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не ввезли в магазина сезон который нельзя было ввозить', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не ввезли в магазина сезон который нельзя было ввозить', null, null);
      end if;
    end if;
  
    v_test_no := 4;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не вывезли из магазина больше пар, чем разрешено *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, x.count_limit_out
             from tdm_map_new_shop_group x
             where s_group = i_s_group) a1
      full join (select distinct id_shop_from id_shop, sum(kol) kol
                 from t_map_new_poexalo a
                 where a.id = i_id
                 group by id_shop_from) a2 on a1.id_shop = a2.id_shop
                                              and a1.count_limit_out >= a2.kol
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не вывезли из магазина больше пар, чем разрешено', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не вывезли из магазина больше пар, чем разрешено', null, null);
      end if;
    end if;
  
    v_test_no := 5;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не ввезли в магазина больше пар, чем разрешено *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, x.count_limit_in
             from tdm_map_new_shop_group x
             where s_group = i_s_group) a1
      full join (select distinct id_shop id_shop, sum(kol) kol
                 from t_map_new_poexalo a
                 where a.id = i_id
                 group by id_shop) a2 on a1.id_shop = a2.id_shop
                                         and a1.count_limit_in + 8 >= a2.kol
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не ввезли в магазина больше пар, чем разрешено', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не ввезли в магазина больше пар, чем разрешено', null, null);
      end if;
    end if;
  
    v_test_no := 6;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы ввозим на магазин артикула только из разрешенного списка артикулов*******');
    
      -- заполнение блэклиста для магазинов, которые могут брать только определенные артикула {
      execute immediate 'truncate table tdv_map_black_list_all';
    
      insert into tdv_map_black_list_all
        select distinct x.shopid id_shop, art, analog
        from tdm_map_new_shop_group x
        left join (select bl1.name, bl2.art, nvl(an.analog, bl2.art) analog
                   from tdv_map_black_list1 bl1
                   left join tdv_map_black_list2 bl2 on bl1.id = bl2.id
                   left join TDV_MAP_NEW_ANAL an on an.art = bl2.art) y on ',' || y.name || ',' like
                                                                           '%,' || x.dtype_in || ',%'
        where x.s_group = i_s_group
              and rtrim(x.dtype_in, ' ') is not null
              and art is not null;
      -- }
    
      select count(*)
      into v_count
      from (select a.id_shop, a.art
             from t_map_new_poexalo a
             inner join tdm_map_new_shop_group b on a.id_shop = b.shopid
                                                    and b.s_group = i_s_group
                                                    and rtrim(b.dtype_in) is not null
             left join (select distinct id_shop, art, analog
                       from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                         and (a.art = c.art or a.art = c.analog)
             where id = i_id
                   and c.art is null);
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы ввозим на магазин артикула только из разрешенного списка артикулов', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы ввозим на магазин артикула только из разрешенного списка артикулов', null, null);
      end if;
    end if;
  
    v_test_no := 7;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что магазин  получает обувь только от разрешенных отделений *******');
      select count(*)
      into v_count
      from (select x.id_shop
             from t_map_new_poexalo x
             inner join tdm_map_new_shop_group b on x.id_shop = b.shopid
                                                    and b.s_group = i_s_group
                                                    and rtrim(b.shop_in) is not null
             left join (select distinct id_shop, id_shop_from
                       from t_map_new_poexalo y
                       where id = i_id) y on x.id_shop = y.id_shop
                                             and ',' || b.shop_in || ',' like '%,' || y.id_shop || ',%'
             where id = i_id
                   and y.id_shop_from is null);
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что магазин  получает обувь только от разрешенных отделений', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что магазин  получает обувь только от разрешенных отделений', null, null);
      end if;
    end if;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 28.02.2018 9:30:34
  -- Start Procedure: 
  -- Заполняет таблицу данными с РЦ и СВХ 
  --****************************************************************************************************************************************************
  procedure fill_rc_svx_data(i_s_group number) is
  begin
    insert into tdv_map_rc_data
      select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
      from sklad_ost_owners
      where shopid not in ('0', ' ')
            and shopid in (select shopid
                           from tdm_map_new_shop_group
                           where s_group = i_s_group)
            and shopid in (select shopid
                           from st_shop z
                           where z.org_kod = 'SHP') --and 1 = 0
      group by skladid, shopid, art, asize
      union all
      select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
      from dist_center.e_osttek
      where owner in (select shopid
                      from tdm_map_new_shop_group
                      where s_group = i_s_group)
            and owner in (select shopid
                          from st_shop z
                          where z.org_kod = 'SHP')
      group by DCID, owner, art, asize
      having sum(kol) > 0;
  
    commit;
  end;

  --********************************************************************************************
  --  
  -- 
  --********************************************************************************************
  procedure fillBaseData(i_s_group             varchar2,
                         i_rules_last_count_id varchar2,
                         i_is_sgp              varchar2) is
  begin
    execute immediate 'truncate table TDV_NEW_SHOP_OST'; -- таблица с транзитами и данными с заказами , лежащими на СГП
    execute immediate 'truncate table TDV_MAP_NEW_OST_SALE'; -- - Остатки вместе с продажами в разрезе аналогов
    execute immediate 'truncate table TDV_MAP_NEW_OST_FULL'; -- текущие остатки для расчета
    execute immediate 'truncate table tdv_map_osttek'; -- текущие остатки собраные в одной таблице (нужно было на случай, когда текущие 
    -- текщие остатки должны считаться как-то по другому, не через e_osttek)
  
    execute immediate 'truncate table tdv_map_shop_stage_count'; -- если это второй этап расчета, то в эту таблицу сохраняем
    -- суммы получил-отдал по каждому магазину на предыдущих этапах  
  
    execute immediate 'truncate table tdv_map_rc_data '; -- таблица с данными РЦ и СВХ
  
    dbms_output.put_line('i_rules_last_count_id: ' || i_rules_last_count_id);
  
    -- если требуется учет РЦ
    if pv_rc = 'T' then
      fill_rc_svx_data(i_s_group);
    end if;
  
    -- отдельно закидываю транзиты, что бы потом каждый раз не пересчитывать {
    insert into TDV_NEW_SHOP_OST
      select a1.shopid, nvl(a2.analog, a1.art) art, asize, 1 kol, 'TRANS'
      from trans_in_way a1
      left join TDV_MAP_NEW_ANAL a2 on a1.art = a2.art
      where scan != ' '
            and a1.shopid in (select shopid
                              from tdm_map_new_shop_group
                              where s_group = i_s_group)
            and a1.shopid in (select shopid
                              from st_shop z
                              where z.org_kod = 'SHP')
      group by a1.shopid, nvl(a2.analog, a1.art), asize
      having sum(kol) > 0
      
      union
      
      select distinct b.shopid, nvl(a2.analog, a.art) art, a.asize, 1, 'TRANS'
      from ST_SAP_DLVRY a -- то что лежит на СГП, зарезервированное за магазином, но еще не попало в ПОС
      inner join s_shop b on a.kunnr = b.shopnum
      left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
      where b.shopid in (select shopid
                         from tdm_map_new_shop_group
                         where s_group = i_s_group)
            and b.shopid in (select shopid
                             from st_shop z
                             where z.org_kod = 'SHP')
            and a.menge > 0;
  
    /*   union 
    
    -- учет прошлых перемещений
    select distinct kkl, nvl(a2.analog, b.art) art, b.asize , 1, 'TRANS'
    from d_planot1 a
    inner join d_planot2 b on a.rel = b.rel and a.kpodr = b.kpodr
    left join TDV_MAP_NEW_ANAL a2 on b.art = a2.art
    where trunc(dated) >= to_date('20180129', 'yyyymmdd') 
            and  trunc(dated) <= to_date('20180210', 'yyyymmdd')
            and a.kkl in (select shopid from tdm_map_new_shop_group where s_group = i_s_group)  
    
    union 
    
    select distinct kkl, nvl(a2.analog, b.art) art, b.asize , 1, 'TRANS'
    from d_planot1 a
    inner join d_planot2 b on a.rel = b.rel and a.kpodr = b.kpodr
    left join TDV_MAP_NEW_ANAL a2 on b.art = a2.art
    where trunc(dated) >= to_date('20180212', 'yyyymmdd') 
            and  trunc(dated) <= to_date('20180228', 'yyyymmdd')
            and a.kkl in (select shopid from tdm_map_new_shop_group where s_group = i_s_group);   */
  
    -- если нужно учесть другие расчеты
    if i_rules_last_count_id is not null then
      insert into TDV_NEW_SHOP_OST
        select distinct id_shop, nvl(a2.analog, a.art) art, a.asize, 1, 'TRANS'
        from t_map_new_poexalo a
        left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
        where i_rules_last_count_id like '%,' || a.id || ',%'
              and kol > 0
              and (id_shop, nvl(a2.analog, a.art), a.asize) not in
              (select id_shop, art, asize
                   from TDV_NEW_SHOP_OST);
    end if;
    -- }
  
    -- если нужно учесть другие расчеты
    if i_rules_last_count_id is not null then
      insert into tdv_map_osttek
        select nvl(a.id_shop, nvl(b.id_shop, c.id_shop)) id_shop, nvl(a.art, nvl(b.art, c.art)) art,
               nvl(a.asize, nvl(b.asize, c.asize)) asize, sum(nvl(a.kol, 0) - nvl(b.kol, 0) + nvl(c.kol, 0)) kol,
               a1.season, 1, sum(nvl(a.kol_rc, 0)),
               -- остатки РЦ
               sum(nvl(a.kol, 0)) -- остатки магазинов без учета перемещений по другим заданиям (нужны что бы считать процент продаж) 
        from (select id_shop, art, asize, sum(kol) kol, season, 1 out_block, sum(kol_rc) kol_rc
               from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 1 out_block, 0 kol_rc
                      from e_osttek_online a2
                      inner join s_art c on a2.art = c.art
                      left join pos_brak x on a2.scan = x.scan
                                              and a2.id_shop = x.id_shop
                      where a2.scan != ' '
                            and a2.procent = 0
                            and a2.id_shop in (select shopid
                                               from tdm_map_new_shop_group
                                               where s_group = i_s_group)
                            and a2.id_shop in (select shopid
                                               from st_shop z
                                               where z.org_kod = 'SHP')
                            and x.scan is null
                      group by a2.id_shop, a2.art, a2.asize, c.season
                      having sum(kol) > 0
                      
                      union all
                      
                      select a2.id_shop, a2.art, a2.asize, 0 kol, c.season, 1 out_block, sum(a2.kol) kol_rc
                      from tdv_map_rc_data a2
                      inner join s_art c on a2.art = c.art
                      group by a2.id_shop, a2.art, a2.asize, c.season
                      having sum(a2.kol) > 0)
               group by id_shop, art, asize, season) a
        
        full join (select id_shop_from id_shop, art, asize, sum(kol) kol
                   from t_map_new_poexalo
                   where i_rules_last_count_id like '%,' || id || ',%'
                   group by id_shop_from, art, asize) b on a.id_shop = b.id_shop
                                                           and a.art = b.art
                                                           and a.asize = b.asize
        
        full join (select id_shop, art, asize, sum(kol) kol
                   from t_map_new_poexalo
                   where i_rules_last_count_id like '%,' || id || ',%'
                   group by id_shop, art, asize) c on nvl(a.id_shop, b.id_shop) = c.id_shop
                                                      and nvl(a.art, b.art) = c.art
                                                      and nvl(a.asize, b.asize) = c.asize
        
        left join s_art a1 on nvl(a.art, nvl(b.art, c.art)) = a1.art
        
        group by nvl(a.id_shop, nvl(b.id_shop, c.id_shop)), nvl(a.art, nvl(b.art, c.art)),
                 nvl(a.asize, nvl(b.asize, c.asize)), a1.season
        having sum(nvl(a.kol, 0) - nvl(b.kol, 0) + nvl(c.kol, 0)) > 0;
    else
      -- текущие остатки:
      insert into tdv_map_osttek
      
        select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
        from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 1 out_block, 0 kol_rc
               from e_osttek_online a2
               inner join s_art c on a2.art = c.art
               left join pos_brak x on a2.scan = x.scan
                                       and a2.id_shop = x.id_shop
               where a2.scan != ' '
                     and a2.procent = 0
                     and a2.id_shop in (select shopid
                                        from tdm_map_new_shop_group
                                        where s_group = i_s_group)
                     and a2.id_shop in (select shopid
                                        from st_shop z
                                        where z.org_kod = 'SHP')
                     and x.scan is null
               group by a2.id_shop, a2.art, a2.asize, c.season
               having sum(kol) > 0
               
               union all
               
               select a2.id_shop, a2.art, a2.asize, 0 kol, c.season, 1 out_block, sum(a2.kol) kol_rc
               from tdv_map_rc_data a2
               inner join s_art c on a2.art = c.art
               group by a2.id_shop, a2.art, a2.asize, c.season
               having sum(a2.kol) > 0)
        group by id_shop, art, asize, season;
    
    end if;
    
    -- П.Суманеев 25.07.2019 : 
    --"артикула, которые заблокированы для продажи либо должны быть возвращены в Витебск, прошу учесть в перебросках, что мы их тоже не должны трогать"
    delete from tdv_map_osttek a where (id_shop, art) in (
        select a.art, b.shopid from st_sale_block a 
        inner join (
            select shopid, case when landid = 'RU' then 1 when landid = 'BY' then 2 end numconf from st_shop where org_kod = 'SHP'
        ) b on a.numconf = b.numconf
    );
    
    --П.Суманеев(02.08.2019): Еще просьба исключить из переброски детсткую обувь
    delete from tdv_map_osttek where art not in (select art from s_art where groupmw in ('Мужские', 'Женские'));
    
    --С.Бунто(12.09.2019): исключить спорт из переброски. исключить только спорт бренды, Адидас, Пума, Лотто, Скетчерс
    delete from tdv_map_osttek where art in (select art from s_art where is_brand in ('Adidas','Puma','LOTTO','Skechers'));
  
    --  считаем РЦ, как остатки магазина, но не берем эти остатки к распределению {
    if pv_rc = 'T' then
      update tdv_map_osttek
      set kol2 = kol2 + kol_rc;
    end if;
    -- }
  
    --  считаем РЦ, как остатки магазина {
    /*    if pv_rc = 'T' then
      update tdv_map_osttek
      set kol = kol + kol_rc, kol2 = kol2 + kol_rc;
    end if;*/
    -- }
  
    /*   -- вычитаем ТОЛЬКО для расчета по сибири созданные заказы из текущих остатков {
    update tdv_map_osttek x
    set (kol, kol2) =
         (select greatest(x.kol - y.kol, 0), greatest(x.kol2 - y.kol, 0)
          from (select a.kpodr, b.art, b.asize, sum(b.kol) kol
                 from d_planot1 a
                 inner join d_planot2 b on a.rel = b.rel
                                           and a.kpodr = b.kpodr
                 where to_char(a.date_s, 'yyyyMMdd') >= '20180215'
                       and to_char(b.date_s, 'yyyyMMdd') >= '20180215'
                       and a.text = 'Зимняя'
                       and kkl like '%D%'
                 group by a.kpodr, b.art, b.asize) y
          where x.id_shop = y.kpodr
                and x.art = y.art
                and x.asize = y.asize)
    where exists (select greatest(x.kol - y.kol, 0), greatest(x.kol2 - y.kol, 0)
           from (select a.kpodr, b.art, b.asize, sum(b.kol) kol
                  from d_planot1 a
                  inner join d_planot2 b on a.rel = b.rel
                                            and a.kpodr = b.kpodr
                  where to_char(a.date_s, 'yyyyMMdd') >= '20180215'
                        and to_char(b.date_s, 'yyyyMMdd') >= '20180215'
                        and a.text = 'Зимняя'
                        and kkl like '%D%'
                  group by a.kpodr, b.art, b.asize) y
           where x.id_shop = y.kpodr
                 and x.art = y.art
                 and x.asize = y.asize);
    -- }  */
  
    -- если это возврат на СГП то удаляю с остатков магазина ПОКУПНУЮ обувь, что бы они никуда не уехала
    if i_is_sgp = 'T' then
      pv_exclude_buy := 'T';
    end if;
  
    if pv_exclude_buy = 'T' then
      delete from tdv_map_osttek
      where art in (select art
                    from st_muya_art)
            and art in (select art
                        from s_art x
                        where replace(x.release_year, ' ', '') <= 2017
                              and x.release_year != ' ');
    
      delete from tdv_map_osttek
      where art in (select art
                    from s_art a
                    where a.facture = 'Покупная'
                          and replace(a.release_year, ' ', '') <= 2017
                          and a.release_year != ' ');
    
      delete from tdv_map_osttek
      where art like '%Р%';
    
      delete from tdv_map_osttek
      where art like '%О%';
      
      delete from tdv_map_osttek
      where art in (select art from t_map_arts_out where id = 10); -- исключение заблокированных к вывозу
    end if;
  
    -- разрешаю для вывоза по каждому магазину только те сезоны, которые проставлены в конфиге 
    update tdv_map_osttek a
    set out_block =
         (select 0
          from tdm_map_new_shop_group b
          where s_group = i_s_group
                and a.id_shop = b.shopid
                and upper(b.season_out) like '%' || upper(a.season) || '%')
    where exists (select 0
           from tdm_map_new_shop_group b
           where s_group = i_s_group
                 and a.id_shop = b.shopid
                 and upper(b.season_out) like '%' || upper(a.season) || '%');
  
    -- отдельно закидываю текущие остатки, что бы потом не лазить по этой большой таблице
    insert into TDV_MAP_NEW_OST_FULL
      select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, a2.season, a2.out_block, sum(a2.kol_rc) kol_rc
      from tdv_map_osttek a2
      group by a2.id_shop, a2.art, a2.asize, a2.season, a2.out_block
      having sum(kol) > 0 or sum(kol_rc) > 0;
  
    -- Остатки вместе с продажами в разрезе аналогов
    insert into TDV_MAP_NEW_OST_SALE
      select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 1 out_block, sum(kol_rc) kol_rc,
             sum(kol2) kol2
      from (
             -- текущие остатки
             select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
             from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, max(z.season) season, null,
                            sum(kol_rc) kol_rc, sum(a2.kol2) kol2
                     from tdv_map_osttek a2
                     left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
                     left join s_art z on a2.art = z.art
                     group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize --, z.season
                     having sum(kol) > 0) a
             group by id_shop, a.art, asize, season
             
             union all
             
             -- все продажи данной обуви за выбранный период
             select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, max(c.season) season, 0, sum(b.kol) kol_sale,
                     0 kol_rc, 0 kol2
             from pos_sale1 a
             inner join pos_sale2 b on a.id_chek = b.id_chek
                                       and a.id_shop = b.id_shop
             inner join s_art c on b.art = c.art
             left join TDV_MAP_NEW_ANAL d on b.art = d.art
             where a.bit_close = 'T'
                   and a.bit_vozvr = 'F'
                   and b.scan != ' '
                   and a.id_shop in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group)
                   and trunc(a.sale_date) >= to_date(20190101, 'yyyymmdd') 
                   
--                   and trunc(a.sale_date) >= case when a.id_shop in ('3910','3913','3914' ) then 
--                                                        case when c.season = 'Зимняя' then to_date(20180801, 'yyyymmdd') 
--                                                             when c.season = 'Утепленная' then to_date(20190601, 'yyyymmdd')
--                                                        end
--                                                        else to_date(20190801, 'yyyymmdd') end

                   --to_date(20190101, 'yyyymmdd')
                   --to_date(20190201, 'yyyymmdd')
                   --to_date(20180801, 'yyyymmdd') 
                   --and trunc(a.sale_date) <= to_date(20180901, 'yyyymmdd') 
                   
                   
                  --and trunc(a.sale_date) >=  ADD_MONTHS(sysdate, -12) -- период за который берем продажи
                   and a.id_shop in (select shopid
                                     from st_shop z
                                     where z.org_kod = 'SHP')
             group by a.id_shop, nvl(d.analog, b.art), b.asize --, c.season
             ) a
      
      group by id_shop, a.art, a.asize;
  
    -- разрешаю для вывоза по каждому магазину только те сезоны, которые проставлены в конфиге 
    update TDV_MAP_NEW_OST_SALE a
    set out_block =
         (select 0
          from tdm_map_new_shop_group b
          where s_group = i_s_group
                and a.id_shop = b.shopid
                and upper(b.season_out) like '%' || upper(a.season) || '%')
    where exists (select 0
           from tdm_map_new_shop_group b
           where s_group = i_s_group
                 and a.id_shop = b.shopid
                 and upper(b.season_out) like '%' || upper(a.season) || '%');
  
    /*    -- проставляю одинаковый сезон в аналогах, на случай если артикула были из разного сезона
    update TDV_MAP_NEW_OST_SALE a
    set season = (select max(season) from
      TDV_MAP_NEW_OST_SALE b where a.art = b.art)
      where exists(select season from
      TDV_MAP_NEW_OST_SALE b where a.art = b.art)  ; */
  
    -- собираю общее кол-во обуви по прошлым расчетам по каждому магазину принял/отдал. Это если будут ограничения по ввозу/вывозу
    /*    if i_rules_last_count_id is not null then
        insert into  tdv_map_shop_stage_count 
        select id_shop, sum(kol_out) kol_out, sum(kol_in) kol_in
        from (
          select id_shop_from id_shop, sum(kol) kol_out, 0 kol_in
          from t_map_new_poexalo
          where i_rules_last_count_id  like '%,'||id||',%' 
          group by id_shop_from 
          union all
          select id_shop, 0 kol_out, sum(kol) kol_in
          from t_map_new_poexalo
          where i_rules_last_count_id  like '%,'||id||',%' 
          group by id_shop)
        group by id_shop  ;
    end if;  */
  
    --------
    commit;
  
  end;

  --********************************************************************************************
  --  
  -- 
  --********************************************************************************************
  procedure fillOborotDaysLast(i_day_last number) is
    v_id integer;
  begin
    select max(id) + 1
    into v_id
    from e_oborot1;
  
    insert into E_OBOROT1
      (ID, USERS, DATEB, DATEE, MOL_DOLG, MOL_FIO, MOL_TAB, TEXT)
    values
      (v_id, 'Только чтение', sysdate - i_day_last, sysdate - i_day_last, ' ', ' ', ' ',
       'Расчет ' || i_day_last || ' дней назад , $MAP#' || i_day_last || '#');
  
    commit;
  
    insert into E_OBOROT3
      (ID, SHOPID, DATEN)
      select AA.*
      from (select v_id, BB.SHOPID,
                    (select max(DATEN)
                      from CONFIG
                      where CONFIG.SHOPID = BB.SHOPID) as DATEN
             from (select a.shopid
                    from s_shop a
                    inner join st_shop b on a.shopid = b.shopid
                                           --where lower(a.cityname)  in ('краснодар', 'астрахань', 'ставрополь', 'ростов', 'волгоград', 'волжский', 'воронеж', 'липецк', 'тамбов' )
                                            and b.org_kod = 'SHP') BB) AA
      where not DATEN is null
      order by SHOPID;
  
    commit;
  
    OBOROTN1(v_id, 'F');
  
    commit;
  
    dbms_output.put_line('Complete. DocNo: ' || v_id);
  end;

  /*procedure stopProcedure is
    v_count number;
  begin
    
    select sum(kol) 
    into v_count
    from t_map_new_poexalo where id = pv_id ;
  
    update TDV_MAP_13_RESULT set DATE_END = systimestamp,
                                                 COUNT_RESULT = 'complete',
                                                 SUM_KOL = v_count
    where id = pv_id;
    
    commit;  
    
    RAISE_APPLICATION_ERROR (-20000, 'Stop procedure manual');
    
  end stopProcedure;*/

  --********************************************************************************************
  --  
  -- настройка расчета с учетом предыдущих в блоке "3. запуск безэтапных расчетов"
  --
  --********************************************************************************************
  procedure run_baby(i_id_hist number default null -- если требуется повторить один из прошлых расчетов
                     ) is
    v_id t_map_new_poexalo.id%type;
    v_str varchar2(2000);
    v_count number;
  
    v_min_no number default 62001;--61205;--61201;--60201;--59601;--59601 ;
    v_max_no number default 62001;--61205;--61201;--60210;--59677;--59677;
  begin
    pv_is_from_single_shop := 'F';
  
    -- сохраняем в истории данные по расчетам {
    if i_id_hist is null then
      delete from TDV_MAP_COLOR_PROC_HIST
      where count_id = v_id;
      insert into TDV_MAP_COLOR_PROC_HIST
        select v_id, a.*
        from TDV_MAP_COLOR_PROC a;
    
      delete from TDV_MAP_CLUSTER_PROP_HIST
      where count_id = v_id;
      insert into TDV_MAP_CLUSTER_PROP_HIST
        select v_id, a.*
        from TDV_MAP_CLUSTER_PROP a;
    else
      -- если нужно повторить расчет , то берем данные из истории
      -- проверяем был ли такой расчет:
      select count(id)
      into v_count
      from TDV_MAP_13_RESULT
      where id = i_id_hist;
    
      if v_count = 0 then
        return;
        -- нужно добавить запись ошибки в лог
      end if;
    
      delete from TDV_MAP_COLOR_PROC;
      insert into TDV_MAP_COLOR_PROC
        select x.id, x.color, SALE_OT, SALE_TO, PROC_OT, PROC_TO
        from TDV_MAP_COLOR_PROC_HIST x
        where count_id = i_id_hist;
    
      delete from TDV_MAP_CLUSTER_PROP;
      insert into TDV_MAP_CLUSTER_PROP
        select x.cluster_name, x.is_stage, x.stage_parent, x.id_color_proc, x.is_sgp, x.ost_month_last
        from TDV_MAP_CLUSTER_PROP_HIST x
        where count_id = i_id_hist;
    
      select substr(max(s_group), 1, length(max(s_group)) - 1) || '0',
             substr(max(s_group), 1, length(max(s_group)) - 1) || '0' + 100
      into v_min_no, v_max_no
      from TDV_MAP_13_RESULT
      where id = i_id_hist;
    end if;
  
    commit;
    -- }
  
    for r_row in (select distinct a.stage_parent
                  from TDV_MAP_CLUSTER_PROP a
                  where a.is_stage is not null
                        and a.stage_parent is not null)
    loop
    
      begin
        v_str := null;
        
      
        -- 1. запуск расчетов первого этапа с общим "родителем"
        for r_stage in (select distinct a.s_group, b.is_sgp, b.id_color_proc, b.ost_month_last
                        from tdm_map_new_shop_group a
                        inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) = trim(upper(b.cluster_name))
                        where b.stage_parent = r_row.stage_parent
                              and b.is_stage is not null
                              and a.s_group >= v_min_no
                              and a.s_group <= v_max_no)
        loop
        
          /*          go(r_stage.s_group, '1,2,3,4,9,10', 'F', null, r_stage.id_color_proc, r_stage.is_sgp, r_row.stage_parent,
          r_stage.ost_month_last, pv_sgp_rasp);*/
        
          go(r_stage.s_group, '!4', 'F', null, r_stage.id_color_proc, r_stage.is_sgp, r_row.stage_parent,
             r_stage.ost_month_last, pv_sgp_rasp);
             
--          go(r_stage.s_group, null, 'F', null, r_stage.id_color_proc, r_stage.is_sgp, r_row.stage_parent,
--             r_stage.ost_month_last, pv_sgp_rasp);
        
          select max(id)
          into v_id
          from t_map_new_poexalo;
        
          v_str := v_str || ',' || v_id;
        
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_stage.s_group, r_row.stage_parent, v_id);
        end loop;
        dbms_output.put_line(v_str);
      
        -- 2. Запуск расчета второго этапа ("родителя"), который учитывает расчеты всех своих первых этапов
        for r_parent_stage in (select distinct a.s_group, b.id_color_proc, b.is_sgp, b.ost_month_last
                               from tdm_map_new_shop_group a
                               inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) =
                                                                    trim(upper(b.cluster_name))
                               where b.cluster_name = r_row.stage_parent
                                     and b.is_stage is not null
                                     and a.s_group >= v_min_no
                                     and a.s_group <= v_max_no)
        loop
          if r_parent_stage.is_sgp = 'T' then
            go(r_parent_stage.s_group, '1,13', 'F', v_str, r_parent_stage.id_color_proc, r_parent_stage.is_sgp,
               r_row.stage_parent, r_parent_stage.ost_month_last, pv_sgp_rasp);
          else
--            go(r_parent_stage.s_group, null, 'F', v_str, r_parent_stage.id_color_proc, r_parent_stage.is_sgp,
--               r_row.stage_parent, r_parent_stage.ost_month_last, pv_sgp_rasp);
               
              go(r_parent_stage.s_group, '!4', 'F', v_str, r_parent_stage.id_color_proc, r_parent_stage.is_sgp,
               r_row.stage_parent, r_parent_stage.ost_month_last, pv_sgp_rasp);
          end if;
        
          select max(id)
          into v_id
          from t_map_new_poexalo;
        
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_parent_stage.s_group, r_row.stage_parent,
                           v_id);
        end loop;
      
      exception
        when others then
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, ' ', 'расчет', ' ');
      end;
    
    end loop;
  
    -- 3. запуск безэтапных расчетов
    for r_row in (select distinct a.s_group, b.id_color_proc, b.is_sgp, b.ost_month_last
                  from tdm_map_new_shop_group a
                  inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) = trim(upper(b.cluster_name))
                  where b.is_stage is null
                        and a.s_group >= v_min_no
                        and a.s_group <= v_max_no)
    loop
      begin
      -- Если с учетом предыдущих расчтов, то 
      -- 1 заполнить v_str списоком расчетов расчтетам, по которым были созданы задания
      -- 2 раскомментировать v_str со списком расчетов и закоментировать v_str := null;
      -- 3 расскомментировать строку под комментарием "-- условие комментирую со слов П.Суманеева, он сказал не нужно 05.02.2018:"
      -- иначе 
      -- 1 раскомментировать v_str := null и закомментировать v_str со списком расчетов
      -- 2 закомментировать строку под комментарием "-- условие комментирую со слов П.Суманеева, он сказал не нужно 05.02.2018:"
      
      v_str := null;
      --v_str := '12174,12175,12176, 11996,11995, 11820,11819,11818,11817,11816, 10697,10696,10695,10694,11042,11041,11040,11039,11038,11037,11036,11554,11553,11552,11551,11550,11548,11547,11546,11545,11544,11543,11542,11541,11540,11539,11538,11537,11535,11534,11533,11531,11530,11529,11528,11527,11526,11525,11524,11523,11522,11521,11520,11518,11517,11516,11515';
      
        if r_row.is_sgp = 'T' then
          go(r_row.s_group, '1,13', 'F', v_str/*null*/, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last,pv_sgp_rasp);
        else
          
--          go(r_row.s_group, /*null*/ '1,4', 'F', v_str/*null*/, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last, pv_sgp_rasp);
          --go(r_row.s_group, /*null*/ '1,3', 'F', v_str/*null*/, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last, pv_sgp_rasp);
          
          go(r_row.s_group, null, 'F', v_str/*null*/, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last, pv_sgp_rasp);
--          go(r_row.s_group, '!4', 'F', v_str/*null*/, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last, pv_sgp_rasp);
        end if;
      
        select max(id)
        into v_id
        from t_map_new_poexalo;
        dbms_output.put_line(v_id);
        WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_row.s_group, 'расчет', v_id);
      exception
        when others then
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_row.s_group, 'расчет', ' ');
      end;
    end loop;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 18.12.2018 9:06:33
  -- Comments:
  -- t_map_shop_priority
  -- TDV_NEW_SHOP_OST
  -- tdv_map_osttek
  -- TDV_MAP_NEW_ANAL
  --****************************************************************************************************************************************************
  /*procedure shop_getout(i_id      number,
                        i_s_group number) is
    v_count number := 0;
    v_ost number;
  begin
  	execute immediate 'truncate table tdv_map_bad_sales';  
  
    fillBaseData(i_s_group, null, 'F');

  
    insert into tdv_map_bad_sales
      select id_shop, art -- только аналоги  , которые плохо продавались
      from TDV_MAP_NEW_OST_SALE a4
      group by id_shop, art
      having sum(a4.kol_sale) <= 2;

  
    -- из остатков сразу вычитаю то, что в пути, т.к. это забирать мы не можем
    delete from tdv_map_osttek
    where (id_shop, art) in (select b.id_shop, nvl(b.art, c.art)
                             from TDV_NEW_SHOP_OST b
                             left join TDV_MAP_NEW_ANAL c on b.art = c.analog);
  
    commit;
  
    for r_row in (select art, asize, sum(kol) kol
                  from t_map_new_poexalo
                  where id = i_id
                  group by art, asize)
    loop
      v_ost := r_row.kol;
    
      for r_shop in (select distinct a.id_shop
                     from tdv_map_osttek a
                     left join t_map_shop_priority b on a.id_shop = b.id_shop
                     left join tdv_map_shop_reit c on a.id_shop = c.id_shop
                     where a.art = r_row.art
                           and a.asize = r_row.asize
                           and a.kol >0)
      loop
      
        null;
      end loop;
    
    end loop;
  end;*/

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 08.12.2018 19:51:35
  -- Comments:
  --
  --****************************************************************************************************************************************************
  /*procedure stage_start(i_s_group      number,
                        i_cluster_name in varchar2 default null,
                        i_count_no     number default null,
                        o_str          out varchar2) is
    v_count integer;
    v_str varchar2(5000) default null;
    v_str_return varchar2(5000) default null;
    v_id number;
    v_count_no number default null;
  begin
    if i_count_no is null then
      v_count_no := 1;
    else
      v_count_no := i_count_no + 1;
    end if;
  
    for r_cluster in (select *
                      from TDV_MAP_CLUSTER_PROP a
                      where a.stage_parent = i_cluster_name
                            or (i_cluster_name is null and a.stage_parent is null))
    loop
    
      -- проверяю существуют ли дочерние кластеры для данного
      -- если да, то вначале их расчитываю  
      select count(*)
      into v_count
      from TDV_MAP_CLUSTER_PROP a
      where a.stage_parent = r_cluster.cluster_name;
    
      if v_count != 0 then
        stage_start(i_s_group, r_cluster.cluster_name, v_count_no, o_str => v_str_return);
        v_str := v_str || ',' || v_str_return;
      end if;
    
      go(i_s_group, null, 'F', v_str_return, r_cluster.id_color_proc, r_cluster.is_sgp, null, r_cluster.ost_month_last,
         pv_sgp_rasp, v_count_no);
    
      select max(id)
      into v_id
      from t_map_new_poexalo;
    
      v_str := v_str || ',' || v_id;
    end loop;
    o_str := v_str;
  end;*/

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 27.02.2018 21:59:41
  -- Start Procedure: 
  --
  --****************************************************************************************************************************************************
  procedure cut_shop_and_rc(i_id varchar2) is
    v_kol number default 0;
    v_art varchar2(50 char);
    v_asize number;
    v_skladid tdv_map_rc_data.skladid%type;
  begin
    -- возвращаю реальные остатки магазина
    update tdv_map_osttek
    set kol = kol - kol_rc, kol2 = kol2 - kol_rc;
  
    -- заношу данные по таблице СВХ-РЦ в расчитываемое поле
    update tdv_map_rc_data
    set kol2 = kol;
  
    -- прохожусь по каждому магазину в расчете
    -- и по каждому артикулу-размеру который был у этого магазина на РЦ
    for r_shop in (select distinct id_shop_from id_shop
                   from t_map_new_poexalo
                   where id = i_id)
    loop
      v_kol := 0;
      for r_row in (select a.*, b.kol kol_ost, b.kol_rc
                    from t_map_new_poexalo a
                    inner join tdv_map_osttek b on a.id_shop_from = b.id_shop
                                                   and a.art = b.art
                                                   and a.asize = b.asize
                                                   and b.kol_rc > 0
                    where a.id_shop_from = r_shop.id_shop
                          and id = i_id
                    order by a.art, a.asize)
      loop
        if r_row.art = v_art and r_row.asize = v_asize then
          v_kol := v_kol + r_row.kol;
        else
          v_art := r_row.art;
          v_asize := r_row.asize;
          v_kol := r_row.kol;
        end if;
      
        if v_kol > r_row.kol_ost then
          select skladid
          into v_skladid
          from (select skladid
                 from tdv_map_rc_data
                 where id_shop = r_shop.id_shop
                       and art = r_row.art
                       and asize = r_row.asize
                       and kol2 > 0
                 order by skladid)
          where rownum = 1;
        
          update t_map_new_poexalo a
          set id_shop_from = v_skladid, owner = r_shop.id_shop
          where id = i_id
                and a.id_shop = r_row.id_shop
                and a.art = r_row.art
                and a.asize = r_row.asize
                and a.id_shop_from = r_shop.id_shop
                and a.boxty = r_row.boxty;
        
          update tdv_map_rc_data
          set kol2 = kol2 - 1
          where id_shop = r_shop.id_shop
                and art = r_row.art
                and asize = r_row.asize
                and skladid = v_skladid;
        end if;
      end loop;
    end loop;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 29.08.2018 15:40:33
  -- Comments: процедура расчитывает вывоз обуви из кластера, согласно ограничениям вместимости на РЦ
  --
  -- 1.делаем обычный расчет для одного этапа
  -- 2. потом вызываем fillBaseData , для второго этапа, учитываем уже остатки переброшенной обуви как остатки отделений
  -- 3. имея остаки и продажи отделения начинаем вывоз обуви, пока не достигнем лимита на кластере
  -- при этом учитываем ростовки. Если без ростовки  - вывозим в первую очередь + по продажам + по рейтингу
  -- если не достигли лимита , начинаем вывозить уже то, что в ростовке
  --****************************************************************************************************************************************************
  procedure removal_shoes_to_rc(i_s_group             varchar2,
                                i_id                  integer,
                                i_rules_last_count_id varchar2 default null) is
    v_rc_limit tdv_map_rc_limit.limit%type;
    v_cluster_current_stock integer; -- текущие остатки кластера
  begin
  
    -- считаем лимит на РЦ для текущего расчета
    select limit
    into v_rc_limit
    from tdv_map_rc_limit
    where id_shop in (select shopid
                      from tdm_map_new_shop_group
                      where s_group = i_s_group)
    group by limit;
  
    -- считаем текущие остатки кластера
    /*    select sum(kol_ost)
    into v_cluster_current_stock
    from TDV_MAP_NEW_OST_SALE a
    where a.out_block = 0
          and kol_ost > 0;*/
    select sum(a2.kol) kol
    into v_cluster_current_stock
    from e_osttek a2
    inner join s_art c on a2.art = c.art
    inner join (select shopid, season_out
                from tdm_map_new_shop_group
                where s_group = i_s_group) d on a2.id_shop = d.shopid
                                                and season_out like '%' || c.season || '%'
    where a2.scan != ' '
          and a2.id_shop in (select shopid
                             from st_shop z
                             where z.org_kod = 'SHP') having sum(kol) > 0;
  
    update t_map_new_poexalo
    set flag = 0
    where flag = 1
          and id = i_id;
  
    for r_sale_ost in (select *
                       from (select a.id_shop, a.art, b.asize_sum, sum(kol_sale) kol_sale, nvl2(b.art, 1, 0) rost_exists,
                                     reit2
                              from TDV_MAP_NEW_OST_SALE a
                              left join (select a.art, id_shop,
                                               -- запрос на определение сбора ростовки
                                               sum(case
                                                      when c.stype = 'cent' then
                                                       10
                                                      else
                                                       1
                                                    end) asize_sum
                                        from TDV_MAP_NEW_OST_SALE a
                                        left join s_art b on a.art = b.art
                                        left join T_MAP12_ASIZE_TYPE c on b.groupmw = c.groupmw
                                                                          and a.asize = c.asize
                                        where a.out_block = 0
                                              and kol_ost > 0
                                        group by a.art, id_shop
                                        having sum(case
                                          when c.stype = 'cent' then
                                           10
                                          else
                                           1
                                        end) > 20) b on a.art = b.art
                                                        and a.id_shop = b.id_shop
                              left join tdv_map_shop_reit c on a.id_shop = c.id_shop -- рейтинг магазинов
                              where a.out_block = 0
                                    and kol_ost > 0
                              
                              group by a.id_shop, a.art, reit2, nvl2(b.art, 1, 0), b.asize_sum)
                       order by rost_exists, kol_sale, reit2 desc)
    loop
      if v_cluster_current_stock <= v_rc_limit then
        exit;
      end if;
    
      -- если аналог, то разбиваю на артикула
      for r_art in (select distinct nvl(b.art, c.art) art
                    from TDV_MAP_NEW_OST_SALE a
                    left join TDV_MAP_NEW_ANAL b on a.art = b.analog
                    left join s_art c on a.art = c.art
                    where a.art = r_sale_ost.art
                          and a.id_shop = r_sale_ost.id_shop
                          and not ((nvl(b.art, c.art) in (select art
                                                          from st_muya_art) or c.facture = 'Покупная') and
                           replace(c.release_year, ' ', '') <= 2017)
                          and nvl(b.art, c.art) not like '%Р%'
                          and nvl(b.art, c.art) not like '%О%')
      loop
        insert into t_map_new_poexalo
          select '9999', a.art, b.season, a.kol, a.asize, 1, a.id_shop, i_id, systimestamp, 'removal', 0, null owner,
                 null text1, null text2, null text3
          from tdv_map_osttek a
          left join s_art b on a.art = b.art
          where a.id_shop = r_sale_ost.id_shop
                and a.art = r_art.art
                and a.kol > 0;
      end loop;
    
      select v_cluster_current_stock - nvl(sum(kol), 0)
      into v_cluster_current_stock
      from t_map_new_poexalo
      where flag = 1
            and id = i_id;
      --dbms_output.put_line(v_cluster_current_stock);
    
      update t_map_new_poexalo
      set flag = 0
      where flag = 1
            and id = i_id;
    
    end loop;
  
    commit;
  
    -- если вывозим то, что на первом этапе закинули в магазин
    -- то вывозим сразу из магазина первого этапа
  
    update t_map_new_poexalo a
    set flag =
         (select 1
          from t_map_new_poexalo b
          where id = i_id
                and a.id_shop = b.id_shop_from
                and a.art = b.art
                and a.asize = b.asize)
    where id like '%' || i_rules_last_count_id || '%'
          and exists (select 1
           from t_map_new_poexalo b
           where id = i_id
                 and a.id_shop_from = b.id_shop
                 and a.art = b.art
                 and a.asize = b.asize);
  
    --dbms_output.put
  
    update t_map_new_poexalo a
    set id_shop_from =
         (select id_shop_from
          from t_map_new_poexalo b
          where id like '%' || i_rules_last_count_id || '%'
                and a.id_shop_from = b.id_shop
                and a.art = b.art
                and a.asize = b.asize)
    where id = i_id
          and exists (select id_shop_from
           from t_map_new_poexalo b
           where id like '%' || i_rules_last_count_id || '%'
                 and a.id_shop_from = b.id_shop
                 and a.art = b.art
                 and a.asize = b.asize);
  
    delete from t_map_new_poexalo a
    where id like '%' || i_rules_last_count_id || '%'
          and a.flag = 1;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 01.01.2018 21:59:18
  -- Start Procedure: 
  --
  --****************************************************************************************************************************************************
  procedure go(i_s_group             number default null,
               i_block_no_run        varchar2 default null, --  -- перечисление блоков, которые должны быть выполнены в программе. если нужны все блоки то NULL
               i_no_data_refresh     varchar2 default 'F', --если НЕ нужно  пересчитывать текущие остатки и продажи то 'T'
               i_rules_last_count_id varchar2 default null, -- если нужно делать расчет с учетом предыдущих
               i_color_id            integer default null,
               i_is_sgp              varchar2 default 'F', -- является ли данный расчет возвратом на СГП 
               i_stage               varchar2 default null,
               i_ost_month_last      integer default 3, -- на сколько месяцев назад учитывать остатки
               i_is_sgp_rasp         varchar2 default 'F', -- если данный расчет распределяет остатки с СГП на магазины
               i_deep_no             number default 1 -- уровень "вложенности расчета"
               ) is
    v_count integer;
    v_sqlrow integer := 0;
    v_s_group integer;
  
    v_id integer;
    v_time timestamp;
  
    v_color varchar2(20);
  
    v_limit_proc number;
    v_limit_pr number;
    v_limit_rs number;
    v_art s_art.art%type;
    v_limit_shop s_shop.shopid%type;
    v_limit_ost number;
  
    v_limit_pr_over varchar2(1) := 'F';
    v_limit_rs_over varchar2(1) := 'F';
  
    v_razn integer default 30000;
  
    v_best_shop varchar2(10 char);
    v_best_shop_in varchar2(500 char);
  
    v_block_no_run varchar2(50 char);
    v_rules_last_count_id varchar2(3000 char);
  
    v_color_id tdv_map_cluster_prop.id_color_proc%type;
  
    v_list_city varchar(5000 char);
    v_list_filial varchar2(2000 char);
  
    v_ost_calculation_id e_oborot1.id%type;
    v_cluster_name tdm_map_new_shop_group.cluster_name%type;
    
    v_all_size_count number := 0;
    v_cent_size_count number := 0;
    
    v_art_pair_count number := 0;
  begin
  
    if i_s_group is not null then
      v_s_group := i_s_group;
    end if;
  
    if i_block_no_run is not null then
      v_block_no_run := ',' || i_block_no_run || ',';
    end if;
  
    if i_rules_last_count_id is not null then
      v_rules_last_count_id := replace(',' || i_rules_last_count_id || ',', ' ', '');
    end if;
  
    -- номер нового расчета
    v_id := map_fl.getNewCalculationID();
  
    dbms_output.put_line('номер расчета: ' || v_id);
  
    v_time := systimestamp;
  
    if i_color_id is null then
      begin
        select nvl(max(b.id_color_proc), 1)
        into v_color_id
        from tdm_map_new_shop_group a
        inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) = trim(upper(b.cluster_name))
        where a.s_group = v_s_group;
      exception
        when no_data_found then
          v_color_id := 1;
      end;
    else
      v_color_id := i_color_id;
    end if;
  
    select max(a.cluster_name)
    into v_cluster_name
    from tdm_map_new_shop_group a
    where a.s_group = v_s_group;
  
    -- вставка новой записи в таблицу расчетов {
    for r_row in (select distinct a.cityname
                  from s_shop a
                  where a.shopid in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = v_s_group))
    loop
      v_list_city := v_list_city || ', ' || r_row.cityname;
    end loop;
    v_list_city := substr(ltrim(v_list_city, ','), 1, least(1000, length(v_list_city)));
  
    for r_row in (select distinct substr(shopid, 1, 2) fil
                  from tdm_map_new_shop_group
                  where s_group = v_s_group)
    loop
      v_list_filial := v_list_filial || ', ' || r_row.fil;
    end loop;
    v_list_filial := ltrim(v_list_filial, ',');
  
    insert into TDV_MAP_13_RESULT
    values
      (v_id, v_time, null, v_s_group, v_block_no_run, v_rules_last_count_id, v_color_id, i_is_sgp, v_list_city,
       v_list_filial, 1, v_time, i_stage, 0, 'in progress', v_cluster_name, USERENV('sessionid'), i_deep_no, 'tdv_map_013');
    commit;
    -- }
  
    update tdm_map_new_shop_group a
    set a.shop_in = ',' || replace(rtrim(ltrim(replace(a.shop_in, ' ', ''), ','), ','), '70', '00') || ','
    where a.s_group = v_s_group
          and a.shop_in is not null;
  
    if i_no_data_refresh = 'F' then
      fillBaseData(v_s_group, v_rules_last_count_id, i_is_sgp);
    end if;
  
    -- заполнение блэклиста для магазинов, которые могут брать только определенные артикула {
    execute immediate 'truncate table tdv_map_black_list_all';
  
    insert into tdv_map_black_list_all
      select distinct x.shopid id_shop, art, analog
      from tdm_map_new_shop_group x
      left join (select bl1.name, bl2.art, nvl(an.analog, bl2.art) analog
                 from tdv_map_black_list1 bl1
                 left join tdv_map_black_list2 bl2 on bl1.id = bl2.id
                 left join TDV_MAP_NEW_ANAL an on an.art = bl2.art) y on ',' || y.name || ',' like
                                                                         '%,' || x.dtype_in || ',%'
      where x.s_group = v_s_group
            and rtrim(x.dtype_in, ' ') is not null
            and art is not null;
    -- }
  
    -- если это вывоз на РЦ по лимиту и идет второй этап, то
    if pv_removal_shoes_to_rc = 'T' and v_rules_last_count_id is not null then
      removal_shoes_to_rc(i_s_group, v_id, v_rules_last_count_id);
    
      update TDV_MAP_13_RESULT
      set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = 0, CLUSTER_NAME = 'removal'
      where id = v_id;
    
      commit;
    
      return;
    end if;
  
    -- заполнение остатков магазина на кол-во месяцев назад соотвествующее настройкам {
    execute immediate 'truncate table tdv_map_oborot';
  
    v_ost_calculation_id := getostcalculationid(i_ost_month_last);
/* 
        insert into tdv_map_oborot
    select distinct a.id_shop, a.art, b.analog
    from tdv_map_osttek a
    left join tdv_map_new_anal b on a.art = b.art;
*/
  
/*    insert into tdv_map_oborot
      select distinct z.shopid, z.art, an.analog
      from e_oborot2 z
      left join TDV_MAP_NEW_ANAL an on an.art = z.art
      inner join tdm_map_new_shop_group x on x.shopid = z.shopid
                                             and x.s_group = v_s_group
                                             and z.id = v_ost_calculation_id
      where z.oste > 0;*/


    insert into tdv_map_oborot
    
--    select distinct a.id_shop, a.art, b.analog
--    from tdv_map_osttek a
--    left join tdv_map_new_anal b on a.art = b.art
--    inner join tdm_map_new_shop_group x on x.shopid = a.id_shop
--                                           and x.s_group = v_s_group
    
--      select distinct z.shopid, z.art, an.analog
--      from e_oborot2 z
--      left join TDV_MAP_NEW_ANAL an on an.art = z.art
--      left join s_art Y on y.art = z.art
--      inner join tdm_map_new_shop_group x on x.shopid = z.shopid
--                                             and x.s_group = v_s_group
--                                             and z.id = '844'
--                                            
--      where z.oste > 0  and y.season = 'Летняя'
--      
--      union all

      select distinct z.shopid, z.art, an.analog
      from e_oborot2 z
      left join TDV_MAP_NEW_ANAL an on an.art = z.art
      left join s_art Y on y.art = z.art
      inner join tdm_map_new_shop_group x on x.shopid = z.shopid
                                             and x.s_group = v_s_group
                                             and z.id = '872'--'872'--'863'--'860'--'859'--'857'--'855'--'845'
                                            
      --where z.oste > 0  and y.season = 'Всесезонная'
      ;

    --}
  
    test(v_id, v_s_group, 1);
  
    --delete from T_MAP_NEW_OTBOR;
    execute immediate 'truncate table T_MAP_NEW_OTBOR';
    commit;
  
    -- +++ Block 1 start ++ {
    if map_fl.blockStart(v_id, 1, v_block_no_run) = 'T' then
      --if (v_block_no_run is null or v_block_no_run like '%,1,%') and nvl(v_block_no_run,'X') not like '%,!1,%' then
      --    blockStart(1);
      -- отбор {
      for r_row in (select id_shop, art, sum(kol_sale) kol_sale, sum(kol_ost2) sale_proc,
                           sum(kol_sale) / (sum(kol_ost2) + sum(kol_sale)) * 100 sale_proc2
                    from TDV_MAP_NEW_OST_SALE a1
                    where (id_shop, art) not in (select x.id_shop, x.art
                                                 from TDV_NEW_SHOP_OST x
                                                 where x.verme > 0) -- не едет в транзитах
                          and (id_shop, art) in (select shopid, analog
                                                 from tdv_map_oborot z) -- были на остатках X дней назад
                          and out_block = 0 -- данный сезон можно вывозить из магазина 
                            ------------------------------------------------------------
                            -- не добавлять в доступные к отбору, если у магазина 0 в лимите на вывоз
                            ------------------------------------------------------------
                            and id_shop not in (select shopid from tdm_map_new_shop_group where s_group = v_s_group and count_limit_out = 0)
                            ------------------------------------------------------------
                    group by id_shop, art
                    having sum(kol_ost) > 0)
      loop
      
        begin
          select color
          into v_color
          from TDV_MAP_COLOR_PROC r
          where r.sale_ot <= r_row.kol_sale
                and r.sale_to >= r_row.kol_sale
                and r.proc_ot <= r_row.sale_proc
                and r.proc_to >= r_row.sale_proc
                and r.id = v_color_id
                and r.color in ('white', 'red');
        exception
          when no_data_found then
            v_color := 'XXX';
        end;
      
        -- если аналог, то разбиваю на артикула
        for r_art in (select distinct nvl(b.art, c.art) art
                      from TDV_MAP_NEW_OST_SALE a
                      left join TDV_MAP_NEW_ANAL b on a.art = b.analog
                      left join s_art c on a.art = c.art
                      where a.art = r_row.art)
        loop
        
          if v_color = 'white' --r_row.kol_sale <= 1 --and r_row.kol_sale / (r_row.kol_ost+r_row.kol_sale) * 100 <= 25 
          
           then
            insert into T_MAP_NEW_OTBOR
              select a.id_shop, a.art, null, sum(kol), a.asize, r_row.art, 0--sum(kol)--1--0
              from tdv_map_osttek a
              where a.id_shop = r_row.id_shop
                    and a.art = r_art.art
                    
              group by a.id_shop, a.art, a.asize
              having sum(kol) > 0;
          
          elsif v_color = 'red'
          
           then
            insert into T_MAP_NEW_OTBOR
              select a.id_shop, a.art, null, sum(kol), a.asize, r_row.art, 1 --sum(kol)-1
              from tdv_map_osttek a
              where a.id_shop = r_row.id_shop
                    and a.art = r_art.art
              group by a.id_shop, a.art, a.asize
              having sum(kol) > 0;
          
          end if;
        
        end loop;
      
      end loop;
      -- }
    
      select sum(kol)
      into v_count
      from T_MAP_NEW_OTBOR;
    
      map_fl.blockEnd(v_id, 'Отбор ' || v_count);
    
      dbms_output.put_line('Отбор ' || v_count);
    
    end if;
    -- ++ Block 1 End ++ } 
  
    /*    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;  */
    commit;
    -- return;
  
    /* Author:  Tyshevich DV Created: 11.07.2018 16:26:14
    если это распределение со склада на Магазины, то закидываю остатки 
    россыпи склада в таблицу распределения
    */
    if i_is_sgp_rasp = 'T' then
      map_fl.fillWarehouseStock();
    
      insert into T_MAP_NEW_OTBOR
        select '9999', a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0
        from tem_mapif_fill_warehouse_stock a
        inner join s_art b on a.art = b.art
        left join TDV_MAP_NEW_ANAL c on a.art = c.art
        where --b.facture != 'Покупная'
         a.art in (select distinct art
                   from pos_sale1 x
                   inner join pos_sale2 y on x.id_chek = y.id_chek
                                             and x.id_shop = y.id_shop
                   where sale_date < sysdate - 120
                         and scan != ' '
                         and bit_close = 'T')
         and b.art not like '%/О%'
        group by a.art, b.season, a.asize, nvl(c.analog, b.art);
    
    end if;
  
    -- ******************************************************************
    --  Если это возврат на СГП, то после первого этапа останавливаю расчет.
    if i_is_sgp = 'T' and map_fl.blockStart(v_id, 13, v_block_no_run) = 'T' then
      --if i_is_sgp = 'T' and ((v_block_no_run is null or v_block_no_run like '%,13,%') and nvl(v_block_no_run,'X') not like '%,!13,%') then
      --  blockStart(13);
      for vi_color in 0 .. 1
      loop
        for r_row in (select a.id_shop, x.count_limit_out - nvl(b.kol, 0) count_limit_out
                      from T_MAP_NEW_OTBOR a
                      inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                             and a.id_shop = x.shopid
                      left join (select id_shop_from, sum(kol) kol
                                from t_map_new_poexalo
                                where id = v_id
                                group by id_shop_from) b on a.id_shop = b.id_shop_from
                      where STATUS_FLAG = vi_color
                      group by a.id_shop, x.count_limit_out, nvl(b.kol, 0))
        loop
          v_count := 0;
          for r_art in (select art, sum(kol) kol
                        from T_MAP_NEW_OTBOR a
                        where a.id_shop = r_row.id_shop
                              and STATUS_FLAG = vi_color
                        group by art)
          loop
            if v_count >= r_row.count_limit_out then
              exit;
            end if;
          
            insert into t_map_new_poexalo
              select '9999', a.art, b.season, a.kol, a.asize, 0, a.id_shop, v_id, v_time, 'sgp', 0, null owner,
                     null text1, null text2, null text3
              from T_MAP_NEW_OTBOR a
              left join s_art b on a.art = b.art
              where a.id_shop = r_row.id_shop
                    and a.art = r_art.art
                    and STATUS_FLAG = vi_color;
          
            v_count := v_count + r_art.kol;
          end loop;
          commit;
          dbms_output.put_line(r_row.id_shop || ' ' || v_count || ' ' || r_row.count_limit_out);
        end loop;
      
      end loop;
      map_fl.blockEnd(v_id, ' ');
      return;
    end if;
  
    commit;
    
    --!!!!!!!!!!!!!
    --return;
    --!!!!!!!!!!!!!
    
    -- +++ Block 2 start ++ {
    if map_fl.blockStart(v_id, 2, v_block_no_run) = 'T' then
      --   if (v_block_no_run is null or v_block_no_run  like '%,2,%') and nvl(v_block_no_run,'X') not like '%,!2,%' then
      --     blockStart(2);
      -- беру магазины и артикула в которые нужно добросить обувь
      for r_row in (select x.*
                    from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, season, x.shop_in, sum(kol_sale) kol_sale, sum(kol_ost2) sale_kol,
                                  sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol2, x.count_limit_in
                           from TDV_MAP_NEW_OST_SALE a
                           inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                  and a.id_shop = x.shopid
                                                                  and x.season_in like '%' || a.season || '%'
                           left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                           left join (select distinct id_shop, analog
                                     from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                                       and a.art = c.analog
                           where --(a.id_shop,a.art) not in (select x.id_shop, x.art from TDV_NEW_SHOP_OST x where x.verme > 0) -- которых нет в транзитах
                            kol_sale + kol_ost2 != 0
                            and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
                            ----------------------------------------------------
                            --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
                            and x.count_limit_in > 0
                            ----------------------------------------------------
                            and a.id_shop not in
                            (select a.id_shop
                                 from t_map_new_poexalo a
                                 inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                        and a.id_shop = x.shopid
                                 left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                                 where id = v_id
                                 group by a.id_shop, x.count_limit_in, y.kol_in
                                 having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
                           group by a.id_shop, a.art, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in
                           having(sum(kol_sale) + sum(kol_ost) != 0)) x
                    inner join TDV_MAP_COLOR_PROC r on x.kol_sale <= r.sale_to
                                                       and x.kol_sale >= r.sale_ot
                                                       and x.sale_kol <= r.proc_to
                                                       and x.sale_kol >= r.proc_ot
                                                       and r.color = 'blue'
                                                       and r.id = v_color_id
                    
                    order by kol_sale desc, reit2)
      loop
      
        -- беру размеры которые могу докинуть
        for r_size in (select distinct a.asize
                       from T_MAP_NEW_OTBOR a
                       left join (select distinct asize
                                 from TDV_MAP_NEW_OST_SALE
                                 where id_shop = r_row.id_shop
                                       and art = r_row.art
                                       and kol_ost2 != 0) b on a.asize = b.asize
                       left join (select distinct asize
                                 from t_map_new_poexalo a
                                 left join TDV_MAP_NEW_ANAL c on a.art = c.art
                                 where id_shop = r_row.id_shop
                                       and nvl(c.analog, a.art) = r_row.art
                                       and id = v_id) c on a.asize = c.asize
                       where a.analog = r_row.art
                             and a.kol > 0
                             and b.asize is null
                             and c.asize is null
                             and a.id_shop not in
                             (select a.id_shop_from
                                  from t_map_new_poexalo a
                                  inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                         and a.id_shop_from = x.shopid
                                  left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                  where id = v_id
                                  group by a.id_shop_from, x.count_limit_out, y.kol_out
                                  having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                             and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                             and (r_row.id_shop, r_row.art, a.asize) not in
                             (select x.id_shop, x.art, x.asize
                                  from TDV_NEW_SHOP_OST x
                                  where x.verme > 0) -- которых нет в транзитах                                  
                       order by a.asize)
        loop
        
          if r_row.count_limit_in != 10000 then
            begin
              select count(distinct a.id_shop)
              into v_count
              from t_map_new_poexalo a
              inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                     and a.id_shop = x.shopid
              left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
              where id = v_id
                    and a.id_shop = r_row.id_shop
              group by a.id_shop, x.count_limit_in, y.kol_in
              having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);
            
              if v_count = 1 then
                exit;
              end if;
            exception
              when no_data_found then
                null;
            end;
          end if;
        
          -- беру данные размеры из магазина с худшими продажами и рейтингом
          insert into t_map_new_poexalo
            select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, v_id, v_time, 'blue', 0, null owner,
                   null text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                   left join (select id_shop, sum(kol_sale) kol_sale
                             from TDV_MAP_NEW_OST_SALE
                             where art = r_row.art
                             group by id_shop) c on a.id_shop = c.id_shop
                   where analog = r_row.art
                         and asize = r_size.asize
                         and kol > 0
                         and a.id_shop not in
                         (select id_shop_from
                              from t_map_new_poexalo a
                              inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                     and a.id_shop_from = x.shopid
                              left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                              where id = v_id
                              group by id_shop_from, x.count_limit_out, y.kol_out
                              having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                         and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                   order by c.kol_sale, a.kol, b.reit2)
            where rownum = 1;
        
        end loop;
      
        update T_MAP_NEW_OTBOR a
        set kol = kol - 1
        where (id_shop, art, asize) in (select id_shop_from, art, asize
                                        from t_map_new_poexalo
                                        where flag = 1
                                              and id = v_id);
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = v_id;
      
      end loop;
    
      select sum(kol)
      into v_count
      from t_map_new_poexalo
      where id = v_id;
    
      map_fl.blockEnd(v_id, 'Поехало ' || v_count);
    
      dbms_output.put_line('Поехало ' || v_count);
    
      select sum(kol)
      into v_count
      from T_MAP_NEW_OTBOR;
    
      dbms_output.put_line('Отбор после отдачи ' || v_count);
    
      writeLog('Отбор после отдачи ' || v_count, 'fill_ross3', v_id);
    
    end if;
    --++ Block 2 End ++ }
  
    commit;
  
    /* Author:  Tyshevich DV Created: 11.07.2018 16:35:17
    Если это распределение со склада, то останавливаем расчет
    */
    if i_is_sgp_rasp = 'T' then
    
      update TDV_MAP_13_RESULT
      set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
      where id = v_id;
      
      commit;
      
      return;
    end if;
  
    -- +++ Block 3 start ++ {
    if map_fl.blockStart(v_id, 3, v_block_no_run) = 'T' then
      --   if (v_block_no_run is null or v_block_no_run  like '%,3,%') and nvl(v_block_no_run,'X') not like '%,!3,%' then
      --     blockStart(3); 
      -- перед вторым распределением удаляем всё что в зоне red
      delete from T_MAP_NEW_OTBOR
      where status_flag != 0;
    
      -- беру магазины и артикула в которые нужно добросить обувь
      for r_row in (select x.*
                    from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, season, x.shop_in, sum(kol_sale) kol_sale, sum(kol_ost2) sale_proc,
                                  sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_proc2, x.count_limit_in
                           from TDV_MAP_NEW_OST_SALE a
                           inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                  and a.id_shop = x.shopid
                                                                  and x.season_in like '%' || a.season || '%'
                           left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                           left join (select distinct id_shop, analog
                                     from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                                       and a.art = c.analog
                           where --(a.id_shop,a.art) not in (select x.id_shop, x.art from TDV_NEW_SHOP_OST x where x.verme > 0) 
                            kol_sale + kol_ost2 != 0
                            and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
                            
                            ----------------------------------------------------
                            --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
                            and x.count_limit_in > 0
                            ----------------------------------------------------
                            
                            and a.id_shop not in
                            (select a.id_shop
                                 from t_map_new_poexalo a
                                 inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                        and a.id_shop = x.shopid
                                 left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                                 where id = v_id
                                 group by a.id_shop, x.count_limit_in, y.kol_in
                                 having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
                           
                           group by a.id_shop, a.art, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in
                           having(sum(kol_sale) + sum(kol_ost) != 0)) x
                    inner join TDV_MAP_COLOR_PROC r on x.kol_sale <= r.sale_to
                                                       and x.kol_sale >= r.sale_ot
                                                       and x.sale_proc <= r.proc_to
                                                       and x.sale_proc >= r.proc_ot
                                                       and r.color = 'green'
                                                       and r.id = v_color_id
                    
                    order by kol_sale desc, reit2)
      loop
      
        -- беру размеры которые могу докинуть
        for r_size in (select distinct a.asize
                       from T_MAP_NEW_OTBOR a
                       left join (select asize
                                 from TDV_MAP_NEW_OST_SALE
                                 where id_shop = r_row.id_shop
                                       and art = r_row.art
                                       and kol_ost2 != 0) b on a.asize = b.asize
                       left join (select distinct asize
                                 from t_map_new_poexalo a
                                 left join TDV_MAP_NEW_ANAL c on a.art = c.art
                                 where id_shop = r_row.id_shop
                                       and nvl(c.analog, a.art) = r_row.art
                                       and id = v_id) c on a.asize = c.asize
                       where a.analog = r_row.art
                             and a.kol > 0
                             and b.asize is null
                             and c.asize is null
                             and a.id_shop not in
                             (select id_shop_from
                                  from t_map_new_poexalo a
                                  inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                         and a.id_shop_from = x.shopid
                                  left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                  where id = v_id
                                  group by id_shop_from, x.count_limit_out, y.kol_out
                                  having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                             and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                             and (r_row.id_shop, r_row.art, a.asize) not in
                             (select x.id_shop, x.art, x.asize
                                  from TDV_NEW_SHOP_OST x
                                  where x.verme > 0)
                       order by a.asize)
        loop
        
          if r_row.count_limit_in != 10000 then
            begin
              select count(distinct a.id_shop)
              into v_count
              from t_map_new_poexalo a
              inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                     and a.id_shop = x.shopid
              left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
              where id = v_id
                    and a.id_shop = r_row.id_shop
              group by a.id_shop, x.count_limit_in, y.kol_in
              having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);
            
              if v_count = 1 then
                exit;
              end if;
            exception
              when no_data_found then
                null;
            end;
          end if;
        
          -- беру данные размеры из магазина с худшими продажами и рейтингом
          insert into t_map_new_poexalo
            select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, v_id, v_time, 'green', 0, null owner,
                   null text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                   left join (select id_shop, sum(kol_sale) kol_sale
                             from TDV_MAP_NEW_OST_SALE
                             where art = r_row.art
                             group by id_shop) c on a.id_shop = c.id_shop
                   where analog = r_row.art
                         and asize = r_size.asize
                         and kol > 0
                         and a.id_shop not in
                         (select id_shop_from
                              from t_map_new_poexalo a
                              inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                     and a.id_shop_from = x.shopid
                              left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                              where id = v_id
                              group by id_shop_from, x.count_limit_out, y.kol_out
                              having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                         and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                   order by kol_sale, a.kol, b.reit2)
            where rownum = 1;
        
        end loop;
      
        update T_MAP_NEW_OTBOR a
        set kol = kol - 1
        where (id_shop, art, asize) in (select id_shop_from, art, asize
                                        from t_map_new_poexalo
                                        where flag = 1
                                              and id = v_id);
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = v_id;
      
      end loop;
    
      select sum(kol)
      into v_count
      from t_map_new_poexalo
      where id = v_id;
    
      map_fl.blockEnd(v_id, 'Этап 3 Поехало ' || v_count);
    
      dbms_output.put_line('Этап 3 Поехало ' || v_count);
    
      select sum(kol)
      into v_count
      from T_MAP_NEW_OTBOR;
    
      dbms_output.put_line('Этап 3 Отбор после отдачи ' || v_count);
    
    end if;
    --++ Block 3 End ++ }
  
    commit;
    /*
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    */
  
    
      -- +++ Block 4 start ++ {
      if map_fl.blockStart(v_id, 4, v_block_no_run) = 'T' then
        --    if (v_block_no_run is null or v_block_no_run  like '%,4,%' ) and i_color_id = 2 and nvl(v_block_no_run,'X') not like '%,!4,%'  then
        --     blockStart(4);  
        dbms_output.put_line('Этап 4 фиолетовый');
      
        execute immediate 'truncate table tdv_test100';
      
        for r_shop in (select shopid
                       from tdm_map_new_shop_group x
                       where x.s_group = v_s_group)
        loop
           -- какому магазину какой артикул можно завезти
          insert into tdv_test100
            select r_shop.shopid, x.analog
            from (select nvl(b.analog, a.art) analog, count(distinct asize) asize_count
                   from T_MAP_NEW_OTBOR a
                   left join tdv_map_new_anal b on a.art = b.art
                   left join s_art c on a.art = c.art
                   where a.kol > 0
                   group by nvl(b.analog, a.art)
                   having count(distinct asize) >= 3) x
            left join (select distinct art
                       from TDV_MAP_NEW_OST_SALE
                       where id_shop = r_shop.shopid) y on x.analog = y.art
           where y.art is null;

--            left join (select art,sum(kol_sale) kol_sale, sum(kol_ost) sale_proc,
--                        sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_proc2
--                        from TDV_MAP_NEW_OST_SALE a
--                        where id_shop = r_shop.shopid 
--                          and kol_sale + kol_ost2 != 0
--                        group by a.art
--                        having(sum(kol_sale) + sum(kol_ost) != 0)
--                       ) y on x.analog = y.art
--            left join TDV_MAP_COLOR_PROC r on nvl(y.kol_sale, 0) <= r.sale_to
--                                           and nvl(y.kol_sale, 0) >= r.sale_ot
--                                           and nvl(y.sale_proc, 0) <= r.proc_to
--                                           and nvl(y.sale_proc, 0) >= r.proc_ot
--                                           and r.color = 'purple'
--                                           and r.id = v_color_id
--            where (r.color is null and y.art is null) or r.color is not null;
        
        end loop;
      
        commit;
        -- доброс в магазины, в которых данной обуви не было вообще
        -- беру магазины и артикула в которые нужно добросить обувь
        for r_row in (
                      --select a.id_shop, a.art, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, a1.style
                      select a.id_shop id_shop, a.art, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, a1.style
                      from tdv_test100 a
                      left join s_art a1 on a1.art = a.art
                      inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                             and a.id_shop = x.shopid
                                                             --and (nvl(x.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                                                             and x.season_in like '%' || a1.season || '%'
                      left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                      left join (select distinct id_shop, analog
                                from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                                  and a.art = c.analog
                      where (a.id_shop, a.art) not in (select x.id_shop, x.art
                                                       from TDV_NEW_SHOP_OST x
                                                       where x.verme > 0) -- которых нет в транзитах
                            
                            ----------------------------------------------------
                            --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
                            and x.count_limit_in > 0
                            --and x.count_limit_out > 0
                            ----------------------------------------------------
                            -- без покупных
                            and a1.facture != 'Покупная'
                            ----------------------------------------------------
                            and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
                            and a.id_shop not in
                            (select a.id_shop
                                 from t_map_new_poexalo a
                                 inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                        and a.id_shop = x.shopid
                                 left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                                 where id = v_id
                                 group by a.id_shop, x.count_limit_in, y.kol_in
                                 having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
                      --group by a.id_shop, a.art, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, a1.style
                      group by a.id_shop , a.art, season, x.shopid, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, a1.style
                      order by nvl(b.reit2, 0))
        loop
        v_art_pair_count:= 0;
          -- беру размеры которые могу докинуть
          for r_size in (select  a.asize, sum(kol) kol
                         from t_map_new_otbor a
                         where a.analog = r_row.art
                               and a.kol > 0
                               and a.id_shop not in
                               (select id_shop_from
                                    from t_map_new_poexalo a
                                    inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                           and a.id_shop_from = x.shopid
                                    left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                    where id = v_id
                                    group by id_shop_from, x.count_limit_out, y.kol_out
                                    having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                               and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                               ------------------------------------
                         --
                         group by a.asize
                         --
                         order by a.asize)
          loop
            --запоминаем количество пар в остатке
            --------------------------------------------------------------------
            v_art_pair_count := v_art_pair_count + r_size.kol;
            --------------------------------------------------------------------
          
            if r_row.count_limit_in != 10000 then
              begin
                select count(distinct a.id_shop)
                into v_count
                from t_map_new_poexalo a
                inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                       and a.id_shop = x.shopid
                left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                where id = v_id
                      and a.id_shop = r_row.id_shop
                group by a.id_shop, x.count_limit_in, y.kol_in
                having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);
              
                if v_count = 1 then
                  exit;
                end if;
              exception
                when no_data_found then
                  null;
              end;
            end if;
          
            -- беру данные размеры из магазина с худшими продажами и рейтингом
            insert into t_map_new_poexalo
              select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, v_id, v_time, 'purple', 0, null owner,
                     null text1, null text2, null text3
              from (select a.*
                     from T_MAP_NEW_OTBOR a
                     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                     
--                     left join (select id_shop, sum(kol_sale) kol_sale
--                               from TDV_MAP_NEW_OST_SALE
--                               where art = r_row.art
--                               group by id_shop) c on a.id_shop = c.id_shop
--                               
                     left join (select id_shop, sum(kol_sale) kol_sale, sum(kol_ost) sale_proc,
                                  sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_proc2
                                  from TDV_MAP_NEW_OST_SALE a
                                  where art = r_row.art and kol_sale + kol_ost2 != 0
                                  group by id_shop
                                  having(sum(kol_sale) + sum(kol_ost) != 0)
                                ) c on a.id_shop = c.id_shop
                      left join TDV_MAP_COLOR_PROC r on nvl(c.kol_sale, 0) <= r.sale_to
                                                     and nvl(c.kol_sale, 0) >= r.sale_ot
                                                     and nvl(c.sale_proc, 0) <= r.proc_to
                                                     and nvl(c.sale_proc, 0) >= r.proc_ot
                                                     and r.color = 'purple'
                                                     and r.id = v_color_id 
                          
                     
                     where analog = r_row.art
                           and asize = r_size.asize
                           and kol > 0
                           
                           and ((r.color is null and c.id_shop is null) or r.color is not null)
                           
                           and a.id_shop not in
                           (select id_shop_from
                                from t_map_new_poexalo a
                                inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                       and a.id_shop_from = x.shopid
                                left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                where id = v_id
                                group by id_shop_from, x.count_limit_out, y.kol_out
                                having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                           and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                           
                     order by c.kol_sale, a.kol, b.reit2)
              where rownum = 1;

          end loop;
--------------------------------------------------------------------------------          
            --выборка из распределенного количествацентральных размеров и всех
            select count(asize),
            nvl(sum(
                case when asize in (4, 4.5, 5, 5.5)    and r_row.groupmw = 'Женские' and r_row.style != 'Sport' then 1
                     when asize in (8, 8.5, 9)         and r_row.groupmw = 'Мужские' and r_row.style != 'Sport' then 1
                     when asize in (5, 5.5, 6, 6.5)    and r_row.groupmw = 'Женские' and r_row.style = 'Sport' then 1
                     when asize in (8.5, 9, 9.5, 10)   and r_row.groupmw = 'Мужские' and r_row.style = 'Sport' then 1
                     when asize in (37, 38, 39)        and r_row.groupmw = 'Женские' then 1
                     when asize in (42, 43)            and r_row.groupmw = 'Мужские' then 1
                     else 0 end), 0) 
            into v_all_size_count, v_cent_size_count
            from (
--                select distinct asize from tdv_map_new_ost_sale where id_shop = i_id_shop and art = i_art and kol_ost > 0
--                union all
--                select distinct asize from tdv_new_shop_ost where id_shop = i_id_shop and art = i_art and verme > 0
--                union all
                select distinct asize from t_map_new_poexalo where id = v_id and id_shop = r_row.id_shop and art = r_row.art and kol > 0 and flag = 1
            );
            
            --dbms_output.put_line('id_shop ' || r_row.id_shop || ' art ' || r_row.art || ' v_all_size_count ' || v_all_size_count|| ' v_cent_size_count ' || v_cent_size_count);
          -- минимум 1 средний размер, и всего 2 размера мужских или 3 женских
--------------------------------------------------------------------------------
          -- проверяю , что собрали минимум три размера. Если нет, то отменяю действие
--          select count(distinct asize)
--          into v_count
--          from t_map_new_poexalo a
--          where id = v_id
--                and id_shop = r_row.id_shop
--                and flag = 1;
        
          if --v_count >= 3
            v_cent_size_count >= 1
            and ((r_row.groupmw = 'Мужские' and v_all_size_count >= 2) or (r_row.groupmw = 'Женские' and v_all_size_count >= 3))
            --and v_all_size_count >= 2
          then
            update T_MAP_NEW_OTBOR a
            set kol = kol - 1
            where (id_shop, art, asize) in (select id_shop_from, art, asize
                                            from t_map_new_poexalo
                                            where flag = 1
                                                  and id = v_id);
            --------------------------------------------------------------------                                      
            -- если распределение с одного магазина и отается одна пара 
            if pv_is_from_single_shop = 'T' and (v_art_pair_count - v_all_size_count = 1)
            then --то вставить в распределенное и обновить остаток отбора
            
                dbms_output.put_line('r_row.id_shop :'|| r_row.id_shop || ' r_row.art : '||r_row.art);
                
                insert into t_map_new_poexalo
                  select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, v_id, v_time, 'purple', 0, null owner,
                         null text1, null text2, null text3
                  from (select a.*
                         from T_MAP_NEW_OTBOR a
                         left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                         left join (select id_shop, sum(kol_sale) kol_sale
                                   from TDV_MAP_NEW_OST_SALE
                                   where art = r_row.art
                                   group by id_shop) c on a.id_shop = c.id_shop
                         where analog = r_row.art
                               and kol > 0
                               and a.id_shop not in
                               (select id_shop_from
                                    from t_map_new_poexalo a
                                    inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                           and a.id_shop_from = x.shopid
                                    left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                    where id = v_id
                                    group by id_shop_from, x.count_limit_out, y.kol_out
                                    having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                               and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                         order by c.kol_sale, a.kol, b.reit2)
                  where rownum = 1;
            
                update T_MAP_NEW_OTBOR a
                set kol = kol - 1
                where (id_shop, art, asize) in (select id_shop_from, art, asize
                                                from t_map_new_poexalo
                                                where flag = 1
                                                      and id = v_id);
            end if;
            --------------------------------------------------------------------
          else
            delete from t_map_new_poexalo
            where id = v_id
                  and id_shop = r_row.id_shop
                  and flag = 1;
          end if;
        
          update t_map_new_poexalo
          set flag = 0
          where flag = 1
                and id = v_id;
        
        end loop;
      
        select sum(kol)
        into v_count
        from t_map_new_poexalo
        where id = v_id;
      
        map_fl.blockEnd(v_id, 'Этап 4 Поехало ' || v_count);
      
        dbms_output.put_line('Этап 4 Поехало ' || v_count);
        
        select sum(kol)
        into v_count
        from T_MAP_NEW_OTBOR;
      
        dbms_output.put_line('Этап 4 Отбор после отдачи ' || v_count);
      end if;
      --++ Block 4 ++}
    
      commit;
      
      --!!!!!!!!!!!!!
      --return;
      --!!!!!!!!!!!!!
    
  
    /*
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    */
    -- +++ Block 5 start ++ {
    if map_fl.blockStart(v_id, 5, v_block_no_run) = 'T' then
      --    if v_block_no_run is null or v_block_no_run  like '%,5,%' then
      --      blockStart(5);
      -- вторые пары раскидываем тем у кого их нет
      delete from T_MAP_NEW_OTBOR;
    
      insert into T_MAP_NEW_OTBOR
        select a.id_shop, a.art, c.season, sum(a.kol) - nvl(e.kol, 0) - 1, a.asize, nvl(b.analog, a.art), 1 --sum(kol)-1 --- case when sum(a.kol_rc)!=0 then 0 else 1 end
        from TDV_MAP_OSTTEK a
        inner join s_art c on a.art = c.art
        left join (select id_shop_from, art, asize, sum(kol) kol
                   from t_map_new_poexalo a
                   where id = v_id
                   group by id_shop_from, art, asize) e on a.id_shop = e.id_shop_from
                                                           and a.art = e.art
                                                           and a.asize = e.asize
        left join TDV_MAP_NEW_ANAL b on a.art = b.art
        where a.out_block = 0
        --and  (a.id_shop, a.art) in (select shopid, analog from tdv_map_oborot z )
        -- условие комментирую со слов П.Суманеева, он сказал не нужно 05.02.2018:
        --and (a.id_shop, a.art) not in (select x.id_shop, x.art from TDV_NEW_SHOP_OST x where x.verme > 0) -- не едет в транзитах
        
        ------------------------------------------------------------
        -- не добавлять в доступные к отбору, если у магазина 0 в лимите на вывоз
        ------------------------------------------------------------
        and a.id_shop not in (select shopid from tdm_map_new_shop_group where s_group = v_s_group and count_limit_out = 0)
        ------------------------------------------------------------
        
        group by a.id_shop, a.art, c.season, a.asize, nvl(b.analog, a.art), e.kol
        having sum(a.kol) - nvl(e.kol, 0) > 1;
    
      execute immediate 'truncate table tdv_test100';
    
      insert into tdv_test100
        select a.id_shop, b.art
        from (select distinct id_shop
               from TDV_MAP_NEW_OST_SALE x) a,
             (select distinct art
               from T_MAP_NEW_OTBOR) b;
    
      -- не берем , если едет в поставке
      delete from tdv_test100 a
      where (id_shop, art) in (select x.id_shop, x.art
                               from TDV_NEW_SHOP_OST x
                               where x.verme > 0);
    
      execute immediate 'truncate table tdv_test101';
      insert into tdv_test101
        select a.id_shop, a.art, nvl(b.kol_sale, 0), c.analog, c.season, x.shop_in, b.kol_ost
        from tdv_test100 a
        inner join s_art y on a.art = y.art
        left join TDV_MAP_NEW_ANAL b on a.art = b.art
        inner join tdm_map_new_shop_group x on a.id_shop = x.shopid
                                               and x.season_in like '%' || y.season || '%'
                                               and x.s_group = v_s_group
        left join (select id_shop, art, sum(kol_sale) kol_sale, sum(kol_ost) kol_ost
                   from TDV_MAP_NEW_OST_SALE
                   group by id_shop, art) b on a.id_shop = b.id_shop
                                               and nvl(b.analog, a.art) = b.art
        left join (select distinct art, analog, season
                   from T_MAP_NEW_OTBOR) c on a.art = c.art
        left join (select distinct id_shop, art
                   from tdv_map_black_list_all) c1 on c1.id_shop = a.id_shop
                                                      and a.art = c1.art
        where nvl(c1.art, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
              and nvl(kol_sale, 0) != 0;
    
      for r_row in (select a.*, x.count_limit_in
                    from tdv_test101 a
                    left join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                          and a.id_shop = x.shopid
                    left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                    where 
                    ----------------------------------------------------
                    --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
                    x.count_limit_in > 0 and
                    ----------------------------------------------------
                    a.id_shop not in
                          (select a.id_shop
                           from t_map_new_poexalo a
                           inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                  and a.id_shop = x.shopid
                           left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                           where id = v_id
                           group by a.id_shop, x.count_limit_in, y.kol_in
                           having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
                    order by case
                               when kol_ost != 0 then
                                0
                               else
                                1
                             end, nvl(a.kol_sale, 0) desc, b.reit2, a.id_shop)
      loop
      
        -- беру размеры которые могу докинуть
        for r_size in (select distinct a.asize
                       from T_MAP_NEW_OTBOR a
                       left join (select distinct a.asize
                                 from TDV_MAP_NEW_OST_SALE a
                                 left join (select asize, id_shop_from, nvl(b.analog, a.art) art, season, sum(kol) kol
                                           from t_map_new_poexalo a
                                           left join TDV_MAP_NEW_ANAL b on a.art = b.art
                                           where id = v_id
                                                 and id_shop_from = r_row.id_shop
                                                 and a.art = r_row.art
                                           group by asize, id_shop_from, nvl(b.analog, a.art), season) c on a.art =
                                                                                                            c.art
                                                                                                            and
                                                                                                            a.id_shop =
                                                                                                            c.id_shop_from
                                                                                                            and a.asize =
                                                                                                            c.asize
                                 where id_shop = r_row.id_shop
                                       and a.art = r_row.analog
                                       and kol_ost - nvl(c.kol, 0) != 0) b on a.asize = b.asize
                       --where id_shop = r_row.id_shop and a.art = r_row.analog and kol_rc - nvl(c.kol,0) != 0) b on a.asize = b.asize
                       left join (select distinct asize
                                 from t_map_new_poexalo
                                 where id_shop = r_row.id_shop
                                       and art = r_row.art
                                       and id = v_id) c on a.asize = c.asize
                       where a.analog = r_row.analog
                             and a.kol > 0
                             and b.asize is null
                             and c.asize is null
                       order by a.asize)
        loop
        
          if r_row.count_limit_in != 10000 then
            begin
              select count(distinct a.id_shop)
              into v_count
              from t_map_new_poexalo a
              inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                     and a.id_shop = x.shopid
              left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
              where id = v_id
                    and a.id_shop = r_row.id_shop
              group by a.id_shop, x.count_limit_in, y.kol_in
              having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);
            
              if v_count = 1 then
                exit;
              end if;
            exception
              when no_data_found then
                null;
            end;
          end if;
        
          -- беру данные размеры из магазина с худшими продажами и рейтингом
          insert into t_map_new_poexalo
            select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, v_id, v_time, 'double', 0, null owner,
                   null text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                   left join (select id_shop, sum(kol_sale) kol_sale
                             from TDV_MAP_NEW_OST_SALE
                             where art = r_row.analog
                             group by id_shop) c on a.id_shop = c.id_shop
                   where art = r_row.art
                         and asize = r_size.asize
                         and kol > 0
                         and a.id_shop not in
                         (select id_shop_from
                              from t_map_new_poexalo a
                              inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                     and a.id_shop_from = x.shopid
                              left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                              where id = v_id
                              group by id_shop_from, x.count_limit_out, y.kol_out
                              having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                         and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                   order by c.kol_sale, a.kol, b.reit2)
            where rownum = 1;
        
        end loop;
      
        update T_MAP_NEW_OTBOR a
        set kol = kol - 1
        where (id_shop, art, asize) in (select id_shop_from, art, asize
                                        from t_map_new_poexalo
                                        where flag = 1
                                              and id = v_id);
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = v_id;
      
      end loop;
    
      map_fl.blockEnd(v_id, 'Этап 5 раскидывание вторых пар ');
    end if;
    --++ Block 5 End++ }
  
    commit;
  
    -- +++ Block 6 start ++ {
    if map_fl.blockStart(v_id, 6, v_block_no_run) = 'T' then
      --   if v_block_no_run is null or v_block_no_run like '%,6,%' then  
      --     blockStart(6);     
      v_count := 0;
      while v_count = 0
      loop
        v_count := 1;
        for r_row in (select a.id_shop id_shop1, a.id_shop_from id_shop_from1, a.art, a.asize, a.season,
                             b.id_shop id_shop2, b.id_shop_from id_shop_from2, c.shop_in
                      from (select *
                             from t_map_new_poexalo
                             where id = v_id) a
                      inner join (select *
                                 from t_map_new_poexalo
                                 where id = v_id) b on a.art = b.art
                                                       and a.asize = b.asize
                                                       and a.id_shop = b.id_shop_from
                      left join (select *
                                from tdm_map_new_shop_group
                                where s_group = v_s_group) c on c.shopid = b.id_shop
                      where rownum = 1)
        loop
        
          delete from t_map_new_poexalo
          where id = v_id
                and id_shop = r_row.id_shop1
                and art = r_row.art
                and asize = r_row.asize
                and id_shop_from = r_row.id_shop_from1;
        
          delete from t_map_new_poexalo
          where id = v_id
                and id_shop = r_row.id_shop2
                and art = r_row.art
                and asize = r_row.asize
                and id_shop_from = r_row.id_shop_from2;
        
          if r_row.id_shop1 = r_row.id_shop_from2 and r_row.id_shop_from1 = r_row.id_shop2 then
            null;
          else
            if nvl(r_row.shop_in, ',' || r_row.id_shop_from1 || ',') like '%,' || r_row.id_shop_from1 || ',%' then
              -- проверка, что магазин может быть донором                                     
              insert into t_map_new_poexalo
              values
                (r_row.id_shop2, r_row.art, r_row.season, 1, r_row.asize, 1, r_row.id_shop_from1, v_id, v_time, 'Z1', 0,
                 null, null, null, null);
            end if;
          end if;
        
          v_count := 0;
        end loop;
      end loop;
      commit;
      map_fl.blockEnd(v_id, 'Этап 6 выравнивание после вторых пар ');
    end if;
    --++Block 6 ++}    
  
    -- +++ Block 7 start ++ {
    if map_fl.blockStart(v_id, 7, v_block_no_run) = 'T' then
      --  if v_block_no_run is null or v_block_no_run like '%,7,%' then  
      --    blockStart(7);
      -- доброс того что осталось в одной паре, на магазины с двумя и более парами
      delete from T_MAP_NEW_OTBOR;
      delete from T_MAP_NEW_OTBOR_ART;
      delete from T_MAP_NEW_OTBOR_ART_ASIZE;
    
      insert into T_MAP_NEW_OTBOR
        select id_shop, a.art, ' ', sum(kol), max(asize), a.art, 0
        from (select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) kol,
                      nvl(a.id_shop, c.id_shop) id_shop, nvl(a2.analog, nvl(a.art, c.art)) art
               from (select *
                      from TDV_MAP_NEW_OST_Full a
                      where nvl(a.out_block, 0) = 0) a
               left join (select id_shop_from, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = v_id
                         group by id_shop_from, art, asize) b on a.art = b.art
                                                                 and a.asize = b.asize
                                                                 and a.id_shop = b.id_shop_from
               full join (select id_shop, nvl(a2.analog, a.art) art, asize, max(a.season) season, sum(kol) kol
                         from t_map_new_poexalo a
                         left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
                         where id = v_id
                         group by id_shop, nvl(a2.analog, a.art), asize) c on a.art = c.art
                                                                              and a.asize = c.asize
                                                                              and a.id_shop = c.id_shop
               left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
               left join s_art d on a.art = d.art
               where nvl(a.out_block, 0) = 0
                    ------------------------------------------------------------
                    -- не добавлять в доступные к отбору, если у магазина 0 в лимите на вывоз
                    ------------------------------------------------------------
                    and a.id_shop not in (select shopid from tdm_map_new_shop_group where s_group = v_s_group and count_limit_out = 0)
                    ------------------------------------------------------------
               group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop), nvl(a2.analog, (nvl(a.art, c.art)))
               having sum((nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0))) > 0) a
        group by a.art, id_shop
        having count(*) = 1;
    
      delete from T_MAP_NEW_OTBOR
      where (id_shop, art) in (select id_shop, art
                               from TDV_MAP_NEW_OST_SALE
                               where out_block = 1);
    
      insert into T_MAP_NEW_OTBOR_ART
        select id_shop, a.art, ' ', sum(kol), max(asize), a.analog
        from (select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) kol,
                      nvl(a.id_shop, c.id_shop) id_shop, nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art))) analog,
                      nvl(a.art, c.art) art
               from TDV_MAP_NEW_OST_FULL a
               left join (select id_shop_from, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = v_id
                         group by id_shop_from, art, asize) b on a.art = b.art
                                                                 and a.asize = b.asize
                                                                 and a.id_shop = b.id_shop_from
               full join (select id_shop, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = v_id
                         group by id_shop, art, asize) c on a.art = c.art
                                                            and a.asize = c.asize
                                                            and a.id_shop = c.id_shop
               left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
               left join TDV_MAP_NEW_ANAL a3 on c.art = a3.art
               group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop), nvl(a.art, c.art),
                        nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art)))
               having sum((nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0))) > 0) a
        where (id_shop, a.analog) in (select id_shop, analog
                                      from T_MAP_NEW_OTBOR)
        group by a.art, id_shop, a.analog;
    
      commit;
    
      delete from t_map_new_shop_in;
    
      /*    insert into t_map_new_shop_in
      select a.id_shop  , nvl(d.analog, b.art) art, b.asize, max(c.season) season,  sum(b.kol) kol_sale, asize_count
          from pos_sale1 a
          inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
          left join s_art c on b.art = c.art
          left join TDV_MAP_NEW_ANAL d on b.art = d.art
          inner join (  select id_shop, art, count(*) asize_count   from (
                            select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol,0) - nvl(b.kol,0)) kol, nvl(a.id_shop, c.id_shop) id_shop,
                                   nvl(a2.analog,nvl(a.art, nvl(a3.analog, c.art))) art
                            from TDV_MAP_NEW_OST_FULL a
                            left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art                                    
                            left join (select id_shop_from, art, asize, sum(kol) kol from t_map_new_poexalo where id = v_id group by id_shop_from, art, asize)  b on a.art = b.art and a.asize = b.asize and a.id_shop = b.id_shop_from
                            full join (select id_shop, art, asize, sum(kol) kol from t_map_new_poexalo where id = v_id group by id_shop, art, asize)  c on a.art = c.art and a.asize = c.asize and a.id_shop = c.id_shop
                            left join TDV_MAP_NEW_ANAL a3 on c.art = a3.art
                            group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop) , nvl(a2.analog,nvl(a.art, nvl(a3.analog, c.art))) having sum(nvl(c.kol, 0) + nvl(a.kol,0) - nvl(b.kol,0))>0 ) a
                            group by id_shop, art having count(*) >= 2) e on a.id_shop = e.id_shop and nvl(d.analog, b.art) = e.art
          where a.bit_close = 'T'
                   and a.bit_vozvr = 'F' 
                   and a.id_shop in
                    (select shopid from tdm_map_new_shop_group where s_group = v_s_group)
                    and a.id_shop in (select shopid from st_shop z where  z.org_kod = 'SHP')
           group by a.id_shop, nvl(d.analog, b.art), b.asize,  asize_count
           order by sum(kol)  ;*/
    
      insert into t_map_new_shop_in
        select a.id_shop, a.art, a.asize, max(a.season) season, sum(a.kol_sale) kol_sale, asize_count
        from (select id_shop, art, asize, season, kol_sale
               from TDV_MAP_NEW_OST_SALE
               where kol_sale != 0) a
        inner join (select id_shop, art, count(*) asize_count
                    from (select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) kol,
                                  nvl(a.id_shop, c.id_shop) id_shop, nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art))) art
                           from TDV_MAP_NEW_OST_FULL a
                           left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
                           left join (select id_shop_from, art, asize, sum(kol) kol
                                     from t_map_new_poexalo
                                     where id = v_id
                                     group by id_shop_from, art, asize) b on a.art = b.art
                                                                             and a.asize = b.asize
                                                                             and a.id_shop = b.id_shop_from
                           full join (select id_shop, art, asize, sum(kol) kol
                                     from t_map_new_poexalo
                                     where id = v_id
                                     group by id_shop, art, asize) c on a.art = c.art
                                                                        and a.asize = c.asize
                                                                        and a.id_shop = c.id_shop
                           left join TDV_MAP_NEW_ANAL a3 on c.art = a3.art
                           group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop),
                                    nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art)))
                           having sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) > 0) a
                    group by id_shop, art
                    having count(*) >= 2) e on a.id_shop = e.id_shop
                                               and a.art = e.art
        group by a.id_shop, a.art, a.asize, asize_count
        order by 5;
    
      insert into T_MAP_NEW_OTBOR_ART_ASIZE
        select nvl(a.asize, c.asize) asize, nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art))) analog,
               nvl(a.id_shop, c.id_shop) id_shop
        from TDV_MAP_NEW_OST_FULL a
        left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
        left join (select id_shop_from, art, asize, sum(kol) kol
                   from t_map_new_poexalo
                   where id = v_id
                   group by id_shop_from, art, asize) b on a.art = b.art
                                                           and a.asize = b.asize
                                                           and a.id_shop = b.id_shop_from
        full join (select id_shop, art, asize, sum(kol) kol
                   from t_map_new_poexalo
                   where id = v_id
                   group by id_shop, art, asize) c on a.art = c.art
                                                      and a.asize = c.asize
                                                      and a.id_shop = c.id_shop
        left join TDV_MAP_NEW_ANAL a3 on c.art = a3.art
        where (nvl(a.id_shop, c.id_shop), nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art)))) in
              (select id_shop, art
               from t_map_new_shop_in)
        group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop), nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art)))
        having sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) > 0;
    
      v_sqlrow := 0;
      commit;
      for r_art in (select distinct art, analog
                    from T_MAP_NEW_OTBOR_ART)
      loop
      
        for r_row in (select a.*, x.shop_in, x.count_limit_in
                      from t_map_new_shop_in a
                      inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                             and a.id_shop = x.shopid
                                                             and x.season_in like '%' || a.season || '%'
                      left join (select distinct id_shop, analog
                                from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                                  and a.art = c.analog
                      where 
                            ----------------------------------------------------
                            --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
                            x.count_limit_in > 0 and
                            ----------------------------------------------------
                            a.art = r_art.analog
                            and kol_sale != 0 -- пока что не отдаем на магазины, которые продали 0 пар
                            and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
                            and a.id_shop not in
                            (select a.id_shop
                                 from t_map_new_poexalo a
                                 inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                        and a.id_shop = x.shopid
                                 left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                                 where id = v_id
                                 group by a.id_shop, x.count_limit_in
                                 having sum(kol) + nvl(sum(kol_in), 0) >= nvl(x.count_limit_in, 0))
                      order by kol_sale desc)
        loop
        
          for r_size in (select distinct a.asize
                         from T_MAP_NEW_OTBOR_ART a
                         left join (select distinct asize
                                   from T_MAP_NEW_OTBOR_ART_ASIZE a
                                   where id_shop = r_row.id_shop
                                         and analog = r_art.analog) b on a.asize = b.asize
                         where a.analog = r_art.analog
                               and b.asize is null)
          loop
          
            if r_row.count_limit_in != 10000 then
              begin
                select count(distinct a.id_shop)
                into v_count
                from t_map_new_poexalo a
                inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                       and a.id_shop = x.shopid
                left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                where id = v_id
                      and a.id_shop = r_row.id_shop
                group by a.id_shop, x.count_limit_in, y.kol_in
                having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);
              
                if v_count = 1 then
                  exit;
                end if;
              exception
                when no_data_found then
                  null;
              end;
            end if;
          
            insert into t_map_new_poexalo
              select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, v_id, v_time, 'ALONE', 0, null owner,
                     null text1, null text2, null text3
              from (select a.*
                     from T_MAP_NEW_OTBOR_ART a
                     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                     where art = r_art.art
                           and asize = r_size.asize
                           and kol > 0
                           and a.id_shop not in
                           (select id_shop_from
                                from t_map_new_poexalo a
                                inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                       and a.id_shop_from = x.shopid
                                left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                where id = v_id
                                group by id_shop_from, x.count_limit_out, y.kol_out
                                having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
                           and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                     order by a.kol, b.reit2)
              where rownum = 1;
          
            update T_MAP_NEW_OTBOR_ART a
            set kol = kol - 1
            where (id_shop, art, asize) in (select id_shop_from, art, asize
                                            from t_map_new_poexalo
                                            where flag = 1
                                                  and id = v_id);
          
            select v_sqlrow + count(*)
            into v_sqlrow
            from t_map_new_poexalo
            where flag = 1
                  and id = v_id;
          
            update t_map_new_poexalo
            set flag = 0
            where flag = 1
                  and id = v_id;
          
            insert into T_MAP_NEW_OTBOR_ART_ASIZE
            values
              (r_size.asize, r_art.analog, r_row.id_shop);
          end loop;
        
        end loop;
      
      end loop;
    
      map_fl.blockEnd(v_id, 'Этап 7 доброс по одной паре ');
      commit;
    end if;
    --++ Block 7 End }
  
    -- +++ Block 8 start ++ {
    if map_fl.blockStart(v_id, 8, v_block_no_run) = 'T' then
      --    if v_block_no_run is null or v_block_no_run like '%,8,%' then    
      --      blockStart(8);
      -- теперь нужно избавится от случаев, когда в магазин приход артикул, а потом у него же и забирается
      v_count := 0;
      while v_count = 0
      loop
        v_count := 1;
        for r_row in (select a.id_shop id_shop1, a.id_shop_from id_shop_from1, a.art, a.asize, a.season,
                             b.id_shop id_shop2, b.id_shop_from id_shop_from2, c.shop_in
                      from (select *
                             from t_map_new_poexalo
                             where id = v_id) a
                      inner join (select *
                                 from t_map_new_poexalo
                                 where id = v_id) b on a.art = b.art
                                                       and a.asize = b.asize
                                                       and a.id_shop = b.id_shop_from
                      left join (select *
                                from tdm_map_new_shop_group
                                where s_group = v_s_group) c on c.shopid = b.id_shop
                      where rownum = 1)
        loop
        
          delete from t_map_new_poexalo
          where id = v_id
                and id_shop = r_row.id_shop1
                and art = r_row.art
                and asize = r_row.asize
                and id_shop_from = r_row.id_shop_from1;
        
          delete from t_map_new_poexalo
          where id = v_id
                and id_shop = r_row.id_shop2
                and art = r_row.art
                and asize = r_row.asize
                and id_shop_from = r_row.id_shop_from2;
        
          if r_row.id_shop1 = r_row.id_shop_from2 and r_row.id_shop_from1 = r_row.id_shop2 then
            null;
          else
            if nvl(r_row.shop_in, ',' || r_row.id_shop_from1 || ',') like '%,' || r_row.id_shop_from1 || ',%' then
              insert into t_map_new_poexalo
              values
                (r_row.id_shop2, r_row.art, r_row.season, 1, r_row.asize, 1, r_row.id_shop_from1, v_id, v_time, 'ZZ', 0,
                 null, null, null, null);
            end if;
          end if;
        
          v_count := 0;
        end loop;
      end loop;
    
      --v_count := 2;
      -- end loop;           
      commit;
      map_fl.blockEnd(v_id, 'Этап 8 выравнивание после одной пары ');
    end if;
    --++ Block 8 End ++ }
  
    -- +++ Block 11 start ++ {
    if map_fl.blockStart(v_id, 11, v_block_no_run) = 'T' then
      --  if v_block_no_run is null or v_block_no_run  like '%,11,%' then  
      --    blockStart(11);
      -- одноразмерки в один магазин
      delete from T_MAP_NEW_OTBOR;
      delete from T_MAP_NEW_OTBOR_ART;
    
      insert into T_MAP_NEW_OTBOR
        select id_shop, a.art, ' ', sum(kol), max(asize), a.art, 0
        from (select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) kol,
                      nvl(a.id_shop, c.id_shop) id_shop, nvl(a2.analog, nvl(a.art, c.art)) art
               from (select *
                      from TDV_MAP_NEW_OST_Full a
                      where nvl(a.out_block, 0) = 0) a
               left join (select id_shop_from, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = v_id
                         group by id_shop_from, art, asize) b on a.art = b.art
                                                                 and a.asize = b.asize
                                                                 and a.id_shop = b.id_shop_from
               full join (select id_shop, nvl(a2.analog, a.art) art, asize, max(a.season) season, sum(kol) kol
                         from t_map_new_poexalo a
                         left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
                         where id = v_id
                         group by id_shop, nvl(a2.analog, a.art), asize) c on a.art = c.art
                                                                              and a.asize = c.asize
                                                                              and a.id_shop = c.id_shop
               left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
               left join s_art d on a.art = d.art
               where nvl(a.out_block, 0) = 0
                    ------------------------------------------------------------
                    -- не добавлять в доступные к отбору, если у магазина 0 в лимите на вывоз
                    ------------------------------------------------------------
                    and a.id_shop not in (select shopid from tdm_map_new_shop_group where s_group = v_s_group and count_limit_out = 0)
                    ------------------------------------------------------------
               group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop), nvl(a2.analog, (nvl(a.art, c.art)))
               having sum((nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0))) > 0) a
        group by a.art, id_shop
        having count(*) = 1;
    
      delete from T_MAP_NEW_OTBOR
      where (id_shop, art) in (select id_shop, art
                               from TDV_MAP_NEW_OST_SALE
                               where out_block = 1);
    
      insert into T_MAP_NEW_OTBOR_ART
        select id_shop, a.art, max(season), sum(kol), max(asize), a.analog
        from (select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) kol,
                      nvl(a.id_shop, c.id_shop) id_shop, nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art))) analog,
                      nvl(a.art, c.art) art
               from TDV_MAP_NEW_OST_FULL a
               left join (select id_shop_from, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = v_id
                         group by id_shop_from, art, asize) b on a.art = b.art
                                                                 and a.asize = b.asize
                                                                 and a.id_shop = b.id_shop_from
               full join (select id_shop, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = v_id
                         group by id_shop, art, asize) c on a.art = c.art
                                                            and a.asize = c.asize
                                                            and a.id_shop = c.id_shop
               left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
               left join TDV_MAP_NEW_ANAL a3 on c.art = a3.art
               group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop), nvl(a.art, c.art),
                        nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art)))
               having sum((nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0))) > 0) a
        left join s_art b on a.art = b.art
        where (id_shop, a.analog) in (select id_shop, analog
                                      from T_MAP_NEW_OTBOR -- беру только те аналоги, которые представлены хотя бы в двух размерах
                                      where analog in (select analog
                                                       from T_MAP_NEW_OTBOR
                                                       group by analog
                                                       having count(distinct asize) > 1))
        group by a.art, id_shop, a.analog;
    
      commit;
      --v_sqlrow := 0;
    
      for r_row in (select distinct analog
                    from T_MAP_NEW_OTBOR_ART)
      loop
        -- находим лучший магазин из одноразмерных. с остальных магазинов везем ему
        begin
          select id_shop, shop_in
          into v_best_shop, v_best_shop_in
          from (select a.id_shop, shop_in, rownum rn
                 from T_MAP_NEW_OTBOR a
                 inner join (select a.id_shop, art, sum(kol_sale) kol_sale, x.shop_in
                            from TDV_MAP_NEW_OST_SALE a
                            inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                   and a.id_shop = x.shopid
                                                                   and x.season_in like '%' || a.season || '%'
                            left join (select distinct id_shop, analog
                                      from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                                        and a.art = c.analog
                            where 
                                    ----------------------------------------------------
                                    --10.09.2019 Bobrovich проверка чтобы не добрасывалось в магазиныс 0 на ввоз
                                    x.count_limit_in > 0 and
                                    ----------------------------------------------------
                                  a.art = r_row.analog
                                  and kol_sale != 0 -- пока что не отдаем на магазины, которые продали 0 пар
                                  and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
                                  and a.id_shop not in
                                  (select a.id_shop
                                       from t_map_new_poexalo a
                                       inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                              and a.id_shop = x.shopid
                                       left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
                                       where id = v_id
                                       group by a.id_shop, x.count_limit_in, y.kol_in
                                       having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0))
                            group by a.id_shop, art, x.shop_in
                            order by nvl(kol_sale, 0) desc) b on a.id_shop = b.id_shop
                                                                 and a.art = b.art
                 order by nvl(kol_sale, 0) desc)
          where rn = 1;
        exception
          when no_data_found then
            v_best_shop := '0000';
        end;
      
        if v_best_shop != '0000' then
          for r_size in (select distinct a.asize
                         from T_MAP_NEW_OTBOR_ART a
                         left join (select asize
                                   from T_MAP_NEW_OTBOR_ART
                                   where analog = r_row.analog
                                         and id_shop = v_best_shop) b on a.asize = b.asize
                         where analog = r_row.analog
                               and id_shop != v_best_shop
                               and b.asize is null
                               and a.kol > 0)
          loop
          
            -- закидываем все размеры в этот магазин , которые можем
            insert into t_map_new_poexalo
              select v_best_shop, art, season, 1, asize, 1, id_shop, v_id, v_time, 'ALONE2', 0, null owner, null text1,
                     null text2, null text3
              from (select a.*
                     from T_MAP_NEW_OTBOR_ART a
                     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                     where analog = r_row.analog
                           and asize = r_size.asize
                           and kol > 0
                           and a.id_shop not in
                           (select id_shop_from
                                from t_map_new_poexalo a
                                inner join tdm_map_new_shop_group x on x.s_group = v_s_group
                                                                       and a.id_shop_from = x.shopid
                                left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                                where id = v_id
                                group by id_shop_from, x.count_limit_out
                                having sum(kol) + nvl(sum(kol_out), 0) >= nvl(x.count_limit_out, 0))
                           and (nvl(v_best_shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                     order by a.kol, b.reit2)
              where rownum = 1;
          
          end loop;
        
          -- проверяю , что собрали минимум три размера. Если нет, то отменяю действие
          select count(distinct asize)
          into v_count
          from t_map_new_poexalo a
          where id = v_id
                and id_shop = v_best_shop
                and flag = 1;
        
          if v_count + 1 >= 2 then
            update T_MAP_NEW_OTBOR a
            set kol = kol - 1
            where (id_shop, art, asize) in (select id_shop_from, art, asize
                                            from t_map_new_poexalo
                                            where flag = 1
                                                  and id = v_id);
          else
            delete from t_map_new_poexalo
            where id = v_id
                  and id_shop = v_best_shop
                  and flag = 1;
          end if;
        
          update t_map_new_poexalo
          set flag = 0
          where flag = 1
                and id = v_id;
        
        end if;
      end loop;
    
      map_fl.blockEnd(v_id, 'Этап 11 соединение однопарок на одном магазине');
      commit;
    end if;
  
    --++ Block 11 End ++ }
  
    -- +++ Block 12 start ++ {
    if map_fl.blockStart(v_id, 12, v_block_no_run) = 'T' then
      --    if v_block_no_run is null or v_block_no_run like '%,12,%' then    
      --      blockStart(12);
      -- теперь нужно избавится от случаев, когда в магазин приход артикул, а потом у него же и забирается
      v_count := 0;
      while v_count = 0
      loop
        v_count := 1;
        for r_row in (select a.id_shop id_shop1, a.id_shop_from id_shop_from1, a.art, a.asize, a.season,
                             b.id_shop id_shop2, b.id_shop_from id_shop_from2, c.shop_in
                      from (select *
                             from t_map_new_poexalo
                             where id = v_id) a
                      inner join (select *
                                 from t_map_new_poexalo
                                 where id = v_id) b on a.art = b.art
                                                       and a.asize = b.asize
                                                       and a.id_shop = b.id_shop_from
                      left join (select *
                                from tdm_map_new_shop_group
                                where s_group = v_s_group) c on c.shopid = b.id_shop
                      where rownum = 1)
        loop
        
          delete from t_map_new_poexalo
          where id = v_id
                and id_shop = r_row.id_shop1
                and art = r_row.art
                and asize = r_row.asize
                and id_shop_from = r_row.id_shop_from1;
        
          delete from t_map_new_poexalo
          where id = v_id
                and id_shop = r_row.id_shop2
                and art = r_row.art
                and asize = r_row.asize
                and id_shop_from = r_row.id_shop_from2;
        
          if r_row.id_shop1 = r_row.id_shop_from2 and r_row.id_shop_from1 = r_row.id_shop2 then
            null;
          else
            if nvl(r_row.shop_in, ',' || r_row.id_shop_from1 || ',') like '%,' || r_row.id_shop_from1 || ',%' then
              insert into t_map_new_poexalo
              values
                (r_row.id_shop2, r_row.art, r_row.season, 1, r_row.asize, 1, r_row.id_shop_from1, v_id, v_time, 'ZZZ',
                 0, null, null, null, null);
            end if;
          end if;
        
          v_count := 0;
        end loop;
      end loop;
    
      --v_count := 2;
      -- end loop;           
      commit;
      map_fl.blockEnd(v_id, 'Этап 12 выравнивание после одной пары ');
    end if;
    --++ Block 12 End ++ }
  
    insert into t_map_new_poexalo_delete
      select *
      from t_map_new_poexalo
      where id = v_id;
  
    -- +++ Block 9 start ++ {
    if map_fl.blockStart(v_id, 9, v_block_no_run) = 'T' then
      --  if v_block_no_run is null or v_block_no_run like '%,9,%' then  
      --      blockStart(9);
      -- удаляем все перемещения на магазины, где получается одна пара
      for r_row in (select id_shop, art, sum(kol), sum(kol_poexalo)
                    from (select nvl(a.asize, c.asize) asize, sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) kol,
                                  sum(nvl(c.kol, 0)) kol_poexalo, nvl(a.id_shop, c.id_shop) id_shop,
                                  nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art))) art
                           from TDV_MAP_NEW_OST_FULL a
                           left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
                           left join (select id_shop_from, art, asize, sum(kol) kol
                                     from t_map_new_poexalo
                                     where id = v_id
                                     group by id_shop_from, art, asize) b on a.art = b.art
                                                                             and a.asize = b.asize
                                                                             and a.id_shop = b.id_shop_from
                           full join (select id_shop, art, asize, sum(kol) kol
                                     from t_map_new_poexalo
                                     where id = v_id
                                     group by id_shop, art, asize) c on a.art = c.art
                                                                        and a.asize = c.asize
                                                                        and a.id_shop = c.id_shop
                           left join TDV_MAP_NEW_ANAL a3 on c.art = a3.art
                           group by nvl(a.asize, c.asize), nvl(a.id_shop, c.id_shop),
                                    nvl(a2.analog, nvl(a.art, nvl(a3.analog, c.art)))
                           having sum(nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0)) > 0) a
                    group by id_shop, art
                    having sum(kol_poexalo) = 1 and sum(kol) = 1)
      loop
      
        delete from t_map_new_poexalo
        where id = v_id
              and id_shop = r_row.id_shop
              and art in (select art
                          from TDV_MAP_NEW_ANAL
                          where analog = r_row.art
                          union
                          select r_row.art
                          from dual);
      
      end loop;
    
      map_fl.blockEnd(v_id, 'Этап 9 удаляем все перемещения на магазины, где получается одна пара ');
    
      commit;
    end if;
    -- ++ Block 9 ++ }
  
    -- часть заключительная 
    -- перебираем все магазины у которых расход-приход получился отклонением от остатков более X %
    -- начинаем перебираеть с магазина, у которого наибольшие отклонения
  
    -- +++ Block 10 start ++ {
    if map_fl.blockStart(v_id, 10, v_block_no_run) = 'T' then
      --  if v_block_no_run is null or v_block_no_run like '%,10,%' then  
      --     blockStart(10);   
    
      v_limit_shop := 'XXX';
    
      while v_limit_shop is not null
      loop
        v_limit_shop := null;
      
        for r_row in (select z.dtype, x.id_shop, kol_pr, kol_rs, kol_ost, abs(kol_rs - kol_pr) proc --abs(kol_pr - kol_rs)/(kol_ost/100) proc 
                      from tdm_map_new_shop_group z
                      left join (select nvl(a.id_shop, b.id_shop_from) id_shop, nvl(sum(a.kol), 0) kol_pr,
                                       nvl(sum(b.kol), 0) kol_rs
                                from (select *
                                       from t_map_new_poexalo
                                       where id = v_id) a
                                full join (select *
                                          from t_map_new_poexalo
                                          where id = v_id) b on a.art = b.art
                                                                and a.asize = b.asize
                                                                and a.id_shop = b.id_shop_from
                                group by nvl(a.id_shop, b.id_shop_from)) x on z.shopid = x.id_shop
                      left join (select id_shop, sum(kol) kol_ost
                                from TDV_MAP_NEW_OST_FULL
                                group by id_shop) y on x.id_shop = y.id_shop
                      where z.s_group = v_s_group
                            and z.dtype is not null
                            and abs(kol_rs - kol_pr) > z.dtype --abs(kol_pr - kol_rs)/(kol_ost/100) > 20
                      --and x.id_shop in (select shopid from tdm_map_new_shop_group where s_group = v_s_group and dtype_in != 'Всё' and dtype_out != 'Всё')
                      
                      order by abs(kol_rs - kol_pr) desc --abs(kol_pr - kol_rs)/(kol_ost/100) desc
                      )
        loop
        
          v_limit_shop := r_row.id_shop;
          v_limit_proc := r_row.proc;
          v_limit_pr := r_row.kol_pr;
          v_limit_rs := r_row.kol_rs;
          v_limit_ost := r_row.kol_ost;
          v_razn := r_row.dtype;
          exit;
        end loop;
      
        --dbms_output.put_line(v_limit_shop||','||v_limit_proc||',!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
      
        if v_limit_shop is null then
          exit;
        end if;
      
        v_limit_pr_over := 'F';
        v_limit_rs_over := 'F';
      
        while v_limit_proc > v_razn
        loop
        
          -- теперь смотрим что в первую очередь уменьшить, приход или расход
          null;
        
          if (v_limit_pr <= v_limit_rs and v_limit_rs_over = 'F') or
             (v_limit_pr <= v_limit_rs and v_limit_pr_over = 'T') or
             (v_limit_pr > v_limit_rs and v_limit_pr_over = 'T' and v_limit_rs_over = 'F') then
            -- если расход больше
            /*               if v_i = 10 then
               dbms_output.put_line('1. rs '||v_id||','||v_limit_shop);               
            end if;*/
            -- выстраиваем магазины которым были сделаны расходы, в порядке превышения лимита
            for r_rs in (select x.id_shop, kol_pr, kol_rs, kol_ost, abs(kol_rs - kol_pr) proc --abs(kol_pr - kol_rs)/(kol_ost/100) proc 
                         from (select nvl(a.id_shop, b.id_shop_from) id_shop, nvl(sum(a.kol), 0) kol_pr,
                                       nvl(sum(b.kol), 0) kol_rs
                                from (select *
                                       from t_map_new_poexalo
                                       where id = v_id
                                             and id_shop_from = v_limit_shop) a
                                left join (select *
                                          from t_map_new_poexalo
                                          where id = v_id) b on a.art = b.art
                                                                and a.asize = b.asize
                                                                and a.id_shop = b.id_shop_from
                                group by nvl(a.id_shop, b.id_shop_from)) x
                         left join (select id_shop, sum(kol) kol_ost
                                   from TDV_MAP_NEW_OST_FULL
                                   group by id_shop) y on x.id_shop = y.id_shop
                         where x.id_shop != v_limit_shop
                         order by abs(kol_rs - kol_pr) desc)
            loop
            
              /*               if v_i = 10 then
                 dbms_output.put_line('2. rs '||r_rs.id_shop||','||r_rs.kol_pr||','||r_rs.kol_rs);               
              end if; */
            
              if v_limit_rs_over != 'T' and r_rs.proc < v_razn then
                v_limit_rs_over := 'T';
                --dbms_output.put_line('v_limit_rs_over = T');
              end if;
            
              if v_limit_rs_over = 'F' or v_limit_pr_over = 'T' then
              
                select art
                into v_art
                from (select *
                       from (select a.art, sum(kol_ost) kol_ost, sum(kol_sale) kol_sale
                              from t_map_new_poexalo a
                              left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
                              left join (select art, asize, kol_sale, kol_ost
                                        from TDV_MAP_NEW_OST_SALE
                                        where id_shop = v_limit_shop) b on nvl(a2.analog, a.art) = b.art
                                                                           and a.asize = b.asize
                              where id = v_id
                                    and id_shop_from = v_limit_shop
                                    and id_shop = r_rs.id_shop
                              group by a.art)
                       order by kol_sale desc, kol_ost)
                where rownum = 1;
              
                select v_limit_rs - sum(kol)
                into v_limit_rs
                from t_map_new_poexalo
                where id = v_id
                      and id_shop_from = v_limit_shop
                      and art = v_art;
              
                delete from t_map_new_poexalo
                where id = v_id
                      and art = v_art;
              
                /*               if v_i = 10 then
                   dbms_output.put_line('3. rs '||r_rs.id_shop||','||r_rs.kol_pr||','||r_rs.kol_rs||','||v_art);               
                end if; */
                --delete from t_map_new_poexalo where id = v_id and id_shop_from = v_limit_shop and id_shop = r_rs.id_shop and art = v_art;
              
                --v_limit_rs := v_limit_rs - sql%rowcount;      
              
              end if;
              exit;
            end loop;
          
            -- если цикл бул пустой, то exit не сработал и мы отмечаем:
            v_limit_rs_over := 'T';
            --dbms_output.put_line('v_limit_rs_over = T');
          
          else
            -- если приход больше
            -- выстраиваем магазины которым были сделаны приходы, в порядке превышения лимита 
            for r_rx in (select x.id_shop, kol_pr, kol_rs, kol_ost, abs(kol_rs - kol_pr) proc
                         from (select nvl(a.id_shop, b.id_shop_from) id_shop, nvl(sum(a.kol), 0) kol_pr,
                                       nvl(sum(b.kol), 0) kol_rs
                                from (select *
                                       from t_map_new_poexalo
                                       where id = v_id) a
                                right join (select *
                                           from t_map_new_poexalo
                                           where id = v_id
                                                 and id_shop = v_limit_shop) b on a.art = b.art
                                                                                  and a.asize = b.asize
                                                                                  and a.id_shop = b.id_shop_from
                                group by nvl(a.id_shop, b.id_shop_from)) x
                         left join (select id_shop, sum(kol) kol_ost
                                   from TDV_MAP_NEW_OST_FULL
                                   group by id_shop) y on x.id_shop = y.id_shop
                         where x.id_shop != v_limit_shop
                         order by abs(kol_rs - kol_pr) desc)
            loop
            
              --dbms_output.put_line('pr '||r_rx.id_shop||','||r_rx.kol_pr||','||r_rx.kol_rs); 
            
              if r_rx.proc < v_razn then
                v_limit_pr_over := 'T';
                --dbms_output.put_line('v_limit_pr_over = T');
              end if;
            
              if v_limit_pr_over = 'F' or (v_limit_rs_over = 'T' and v_limit_pr_over = 'T') then
                select art
                into v_art
                from (select *
                       from (select a.art, sum(kol_ost) kol_ost, sum(kol_sale) kol_sale
                              from t_map_new_poexalo a
                              left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
                              left join (select art, asize, kol_sale, kol_ost
                                        from TDV_MAP_NEW_OST_SALE
                                        where id_shop = r_rx.id_shop) b on nvl(a2.analog, a.art) = b.art
                                                                           and a.asize = b.asize
                              where id = v_id
                                    and id_shop_from = r_rx.id_shop
                                    and id_shop = v_limit_shop
                              group by a.art)
                       order by kol_sale desc, kol_ost)
                where rownum = 1;
              
                select v_limit_pr - sum(kol)
                into v_limit_pr
                from t_map_new_poexalo
                where id = v_id
                      and id_shop = v_limit_shop
                      and art = v_art;
              
                delete from t_map_new_poexalo
                where id = v_id
                      and art = v_art;
              
                /*                 if v_i = 10 then
                  dbms_output.put_line('pr '||r_rx.id_shop||','||r_rx.kol_pr||','||r_rx.kol_rs); 
                end if; */
                --delete from t_map_new_poexalo where id = v_id and id_shop_from = r_rx.id_shop and id_shop = v_limit_shop and art = v_art; 
              
                --v_li++mit_pr := v_limit_pr - sql%rowcount;    
              
              end if;
              exit;
            end loop;
            v_limit_pr_over := 'T';
            --dbms_output.put_line('v_limit_pr_over = T');                   
          end if;
        
          v_limit_proc := abs(v_limit_pr - v_limit_rs);
          /*         if v_i  = 10 then
           dbms_output.put_line(v_limit_shop||','||v_limit_proc||','||v_limit_pr||','||v_limit_rs||','||v_limit_pr_over||','||v_limit_rs_over);  
          v_i := 0;
          else 
            v_i := v_i +1;
          end if;   */
        end loop;
      end loop; -- while    
    
      map_fl.blockEnd(v_id, 'Этап 10 конец ');
      commit;
    end if;
    --++ Block 10 end ++ }
  
    -- удаление перечня артикулов 
    delete from t_map_new_poexalo 
    where id = v_id and art in (select art from t_map_art_black_list_perebros);   
    commit;
    
    test(v_id, v_s_group, 2);
    test(v_id, v_s_group, 3);
    test(v_id, v_s_group, 4);
    test(v_id, v_s_group, 5);
    test(v_id, v_s_group, 6);
    test(v_id, v_s_group, 7);
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = v_id;
  
    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;
  
    --  определяем откуда реально брать кол-во
/*    if pv_rc = 'T' then
      cut_shop_and_rc(v_id);
    end if;*/
    -- }
  
    dbms_output.put_line('Окончено');
    commit;
    /*  EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line(sqlcode||sqlerrm);
      rollback;                
      WRITE_ERROR_PROC('FILL_ROSS3', sqlcode, sqlerrm, 'i_s_group = '||i_s_group, 'MAP', ' ');
      RAISE_APPLICATION_ERROR(sqlcode,sqlerrm);*/
  
  end;

end tdv_map_013;