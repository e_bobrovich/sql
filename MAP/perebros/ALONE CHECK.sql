select * from TDV_MAP_13_RESULT order by id desc;

select * from TDV_MAP_NEW_OST_Full where art = '19263301';

select * from t_map_new_poexalo where id = 20096 
and art = '19280501'
;

select * from s_art where art = '19263301';
select art, normt from s_art where art = '19263301';
select art, normt from s_art where normt = '19263301';
--19СМФ51295
--19СМФ51285-1   19СМФ51285

--insert into T_MAP_NEW_OTBOR
select id_shop, a.art, ' ', sum(kol), max(asize), a.art analog, 0
from (
      select 
            nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop) id_Shop, 
            nvl(nvl(nvl(a2.analog, a.art), b.art), c.art) art,
            nvl(nvl(a.asize, b.asize),c.asize) asize,
            sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) kol

       from (select *
              from TDV_MAP_NEW_OST_Full a
              where nvl(a.out_block, 0) = 0) a
       full join (select id_shop_from, art, asize, sum(kol) kol
                 from t_map_new_poexalo
                 where id = 20096
                 group by id_shop_from, art, asize) b on a.art = b.art
                                                         and a.asize = b.asize
                                                         and a.id_shop = b.id_shop_from
       full join (select id_shop, art, asize, max(a.season) season, sum(kol) kol
                 from t_map_new_poexalo a
                 where id = 20096
                 group by id_shop, art, asize) c on nvl(a.art, b.art) = c.art
                                                                      and nvl(a.asize, b.asize) = c.asize
                                                                      and nvl(a.id_shop, b.id_shop_from) = c.id_shop 
       left join TDV_MAP_NEW_ANAL a2 on nvl(nvl(a.art, b.art), c.art) = a2.art
       left join s_art d on a.art = d.art
       where nvl(a.out_block, 0) = 0
            ------------------------------------------------------------
            -- не добавлять в доступные к отбору, если у магазина 0 в лимите на вывоз
            ------------------------------------------------------------
            and nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop) not in (select shopid from tdm_map_new_shop_group where s_group = 70403 and count_limit_out = 0)
            ------------------------------------------------------------
       group by 
       nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop), 
       nvl(nvl(nvl(a2.analog, a.art), b.art), c.art),
       nvl(nvl(a.asize, b.asize),c.asize)

       having sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) > 0  ) a
group by a.art, id_shop
having count(*) > 0 and count(*) <= 1;


select * from T_MAP_NEW_OTBOR order by id_shop, art, asize;    



select * from T_MAP_NEW_OTBOR
      where (id_shop, art) in (select id_shop, art
                               from TDV_MAP_NEW_OST_SALE
                               where out_block = 1);

--delete from T_MAP_NEW_OTBOR_ART;
--insert into T_MAP_NEW_OTBOR_ART
select id_shop, a.art, ' ', sum(kol), /*max(asize)*/ asize /*28.01.2019 asize Bobrovich EP*/, a.analog
        from (
              select 
                  nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop) id_Shop, 
                  nvl(nvl(nvl(a2.analog, a.art), b.art), c.art) analog,
                  nvl(nvl(a.art, b.art),c.art) art,
                  nvl(nvl(a.asize, b.asize),c.asize) asize,
                  sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) kol
               from TDV_MAP_NEW_OST_FULL a
               full join (select id_shop_from, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = 20096
                         group by id_shop_from, art, asize) b on a.art = b.art
                                                                 and a.asize = b.asize
                                                                 and a.id_shop = b.id_shop_from
               full join (select id_shop, art, asize, sum(kol) kol
                         from t_map_new_poexalo
                         where id = 20096
                         group by id_shop, art, asize) c on nvl(a.art, b.art) = c.art
                                                            and nvl(a.asize, b.asize) = c.asize
                                                            and nvl(a.id_shop, b.id_shop_from) = c.id_shop
               left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
               group by nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop), 
                        nvl(nvl(nvl(a2.analog, a.art), b.art), c.art),
                        nvl(nvl(a.art, b.art),c.art),
                        nvl(nvl(a.asize, b.asize),c.asize)
               having sum((nvl(c.kol, 0) + nvl(a.kol, 0) - nvl(b.kol, 0))) > 0) a
        where (id_shop, a.analog) in (select id_shop, analog
                                      from T_MAP_NEW_OTBOR)
        group by a.art, id_shop, a.analog , asize /*28.01.2019 asize Bobrovich EP*/;

select * from T_MAP_NEW_OTBOR_ART  order by id_shop, art, asize;

-- магазины с аналогами, которых >=2 размера на остатке. отсортированы по количеству продаж аналога
--delete from t_map_new_shop_in;
--insert into t_map_new_shop_in
select a.id_shop, a.art, a.asize, max(a.season) season, sum(a.kol_sale) kol_sale, asize_count
from (select id_shop, art, asize, season, kol_sale
       from TDV_MAP_NEW_OST_SALE
       where kol_sale != 0) a
inner join (select id_shop, art, count(*) asize_count -- магазины + артикула с 2 размерами на итоговом остатке
            from (
                   select 
                      nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop) id_Shop, 
                      nvl(nvl(nvl(a2.analog, a.art), b.art), c.art) art,
                      nvl(nvl(a.asize, b.asize),c.asize) asize,
                      
                      sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) kol
                   from TDV_MAP_NEW_OST_FULL a

                   full join (select id_shop_from, art, asize, sum(kol) kol
                             from t_map_new_poexalo
                             where id = 20096
                             group by id_shop_from, art, asize) b on a.art = b.art
                                                                     and a.asize = b.asize
                                                                     and a.id_shop = b.id_shop_from
                   full join (select id_shop, art, asize, sum(kol) kol
                             from t_map_new_poexalo
                             where id = 20096
                             group by id_shop, art, asize) c on nvl(a.art, b.art) = c.art
                                                                and nvl(a.asize, b.asize) = c.asize
                                                                and nvl(a.id_shop, b.id_shop_from) = c.id_shop 
                   left join TDV_MAP_NEW_ANAL a2 on nvl(nvl(a.art, b.art), c.art) = a2.art 
                   
                   group by nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop), 
                             nvl(nvl(nvl(a2.analog, a.art), b.art), c.art),
                             nvl(nvl(a.asize, b.asize),c.asize)
                                              
                   having sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) > 0) a
                   
--                   where (id_shop, a.art) not in (select id_shop, analog
--                                    from T_MAP_NEW_OTBOR)  
--                                    
            group by id_shop, art
            having count(*) >= /*pv_count_asize_alone+1*/ /*2*/ 2) e on a.id_shop = e.id_shop
                                       and a.art = e.art
group by a.id_shop, a.art, a.asize, asize_count
order by 5;


select * from t_map_new_shop_in  order by id_shop, art, asize;
select * from t_map_new_shop_in ;

--delete from T_MAP_NEW_OTBOR_ART_ASIZE;
 -- аналоги и размеры, котоыре можно распределить
--insert into T_MAP_NEW_OTBOR_ART_ASIZE
select 
  nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop) id_Shop, 
  nvl(nvl(nvl(a2.analog, a.art), b.art), c.art) art,
  nvl(nvl(a.asize, b.asize),c.asize) asize
  
from TDV_MAP_NEW_OST_FULL a
full join(select id_shop_from, art, asize, sum(kol) kol
         from t_map_new_poexalo
         where id = 20096
         group by id_shop_from, art, asize) b on a.art = b.art
                                                 and a.asize = b.asize
                                                 and a.id_shop = b.id_shop_from
full join (select id_shop, art, asize, sum(kol) kol
         from t_map_new_poexalo
         where id = 20096
         group by id_shop, art, asize) c on nvl(a.art, b.art) = c.art
                                            and nvl(a.asize, b.asize) = c.asize
                                            and nvl(a.id_shop, b.id_shop_from) = c.id_shop 
                                            
left join TDV_MAP_NEW_ANAL a2 on nvl(nvl(a.art, b.art), c.art) = a2.art 

where (nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop), nvl(nvl(nvl(a2.analog, a.art), b.art), c.art) ) in
        (select id_shop, art
         from t_map_new_shop_in)

group by 
    nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop), 
    nvl(nvl(nvl(a2.analog, a.art), b.art), c.art),
    nvl(nvl(a.asize, b.asize),c.asize)
           
having sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) > 0 ;

select * from T_MAP_NEW_OTBOR_ART_ASIZE;



-- прход по артикулам доступным к распределению
select distinct art, analog
                    from T_MAP_NEW_OTBOR_ART;
                    
-- проход по магазинам(аналог + размер), куда можно распределить рассматриваемый арикул\аналог
select a.*, gsi.max_kol_sale, gsi.kol_sale_all, x.shop_in, x.count_limit_in
from t_map_new_shop_in a
inner join tdm_map_new_shop_group x on x.s_group = 70403
                                       and a.id_shop = x.shopid
                                       and x.season_in like '%' || a.season || '%'
left join (select distinct id_shop, analog
          from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                            and a.art = c.analog
left join t_map13_no_stock stks on a.id_shop = stks.id_shop
left join T_MAP13_STOCK stka on a.art = stka.analog

left join (select id_shop, art, max(kol_sale) max_kol_sale, sum(kol_sale) kol_sale_all 
            from t_map_new_shop_in 
            where art = '19263301' group by id_shop, art) gsi on a.id_shop = gsi.id_shop and a.art = gsi.art
where 
      ----------------------------------------------------
      --проверка чтобы не добрасывалось в магазиныс 0 на ввоз
--                            x.count_limit_in > 0 and
      ----------------------------------------------------
      a.art = '19263301'
--      and kol_sale != 0 -- пока что не отдаем на магазины, которые продали 0 пар
      and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
      ----------------------------------------------------
      -- 09.01.2020 Bobrovich Исключение стоков для магазинов из списка
--      and case when stks.id_shop is not null then stka.analog else null end is null
      ----------------------------------------------------
--      and a.id_shop not in
--      (select a.id_shop
--           from t_map_new_poexalo a
--           inner join tdm_map_new_shop_group x on x.s_group = 68801
--                                                  and a.id_shop = x.shopid
--           left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
--           where id = 20096
--           group by a.id_shop, x.count_limit_in
--           having sum(kol) + nvl(sum(kol_in), 0) >= nvl(x.count_limit_in, 0))

order by gsi.max_kol_sale desc, gsi.kol_sale_all desc, a.kol_sale  desc
;

select id_shop, sum(kol) from t_map_new_poexalo where id = 20096 group by id_shop;
select id_shop_from, sum(kol) from t_map_new_poexalo where id = 20096 group by id_shop_from;

--select a.id_shop, a.art, a.season, max(kol_sale) max_kol_sale, sum(kol_sale) kol_sale, x.shop_in, x.count_limit_in
--from t_map_new_shop_in a
--inner join tdm_map_new_shop_group x on x.s_group = 70403
--                                       and a.id_shop = x.shopid
--                                       and x.season_in like '%' || a.season || '%'
--left join (select distinct id_shop, analog
--          from tdv_map_black_list_all) c on c.id_shop = a.id_shop
--                                            and a.art = c.analog
--left join t_map13_no_stock stks on a.id_shop = stks.id_shop
--left join T_MAP13_STOCK stka on a.art = stka.analog
--where 
--      a.art = '2106751GN'
--      and nvl(c.analog, '0') = nvl2(rtrim(x.dtype_in, ' '), a.art, '0')
--group by a.id_shop, a.art, a.season, x.shop_in, x.count_limit_in
--order by kol_sale desc
--;

select * from T_MAP_NEW_OTBOR_ART_ASIZE;
-- проход по доступным к распределению размерам аналога, которых нет в целевом магазине
select *--distinct a.asize
from T_MAP_NEW_OTBOR_ART a
left join (select distinct asize
         from T_MAP_NEW_OTBOR_ART_ASIZE a
         where id_shop = '0065'
               and analog = '19263301') b on a.asize = b.asize
where a.analog = '19263301' and a.id_shop !='0065'
     and case when /*pv_double_in_alone*/'T' = 'T' then null else b.asize end is null
;

-- выборка пары из доступного
--insert into t_map_new_poexalo
select '0065', art, 'SEASON', LEAST(/*pv_count_go_alone*/3, kol) kol, asize, 1, id_shop, 20096 id, systimestamp, 'ALONE', 0, null owner,
       null text1, null text2, null text3
--select '0065', art, r_row.season, LEAST(pv_count_go_alone, kol) kol, asize, 1, id_shop, 20096, v_time, 'ALONE', 0, null owner,
--       null text1, null text2, null text3    
from (select a.*
       from T_MAP_NEW_OTBOR_ART a
       left join tdv_map_shop_reit b on a.id_shop = b.id_shop
       where art = '19263301'
             and a.id_shop != '0065'
             and asize = '4.5'
             and kol > 0
             and a.id_shop not in
             (select id_shop_from
                  from t_map_new_poexalo a
                  inner join tdm_map_new_shop_group x on x.s_group = 70403
                                                         and a.id_shop_from = x.shopid
                  left join tdv_map_shop_stage_count y on a.id_shop_from = y.id_shop
                  where id = 20096
                  group by id_shop_from, x.count_limit_out, y.kol_out
                  having sum(kol) + nvl(y.kol_out, 0) >= nvl(x.count_limit_out, 0))
             and (nvl(null, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
       order by a.kol, b.reit2)
where rownum = 1;



select
  sum(nvl(a.kol, 0) + nvl(c.kol, 0) - nvl(b.kol, 0)) kol
from (select *
    from TDV_MAP_NEW_OST_Full a
    where nvl(a.out_block, 0) = 0) a
full join (select id_shop_from, art, asize, sum(kol) kol
       from t_map_new_poexalo
       where id = 20096
       group by id_shop_from, art, asize) b on a.art = b.art
                                               and a.asize = b.asize
                                               and a.id_shop = b.id_shop_from
full join (select id_shop, art, asize, max(a.season) season, sum(kol) kol
       from t_map_new_poexalo a
       where id = 20096
       group by id_shop, art, asize) c on nvl(a.art, b.art) = c.art
                                                            and nvl(a.asize, b.asize) = c.asize
                                                            and nvl(a.id_shop, b.id_shop_from) = c.id_shop 
left join TDV_MAP_NEW_ANAL a2 on nvl(nvl(a.art, b.art), c.art) = a2.art
where nvl(a.out_block, 0) = 0
and nvl(nvl(nvl(a2.analog, a.art), b.art), c.art) = '19263301'
and nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop) = '0065' --целевой
and nvl(nvl(a.asize, b.asize),c.asize) = '4.5'

group by 
nvl(nvl(a.id_shop, b.id_shop_from),c.id_Shop), 
nvl(nvl(nvl(a2.analog, a.art), b.art), c.art),
nvl(nvl(a.asize, b.asize),c.asize);



update T_MAP_NEW_OTBOR_ART a
set kol = kol - LEAST(/*pv_count_go_alone*/3, kol) /*1*/
where (id_shop, art, asize) in (select id_shop_from, art, asize
                                from t_map_new_poexalo
                                where flag = 1
                                      and id = 20096);  
                                      
select * from T_MAP_NEW_OTBOR_ART where art = '2106751GN';

select count(*)
from t_map_new_poexalo
where flag = 1
      and id = 20096;

update t_map_new_poexalo
set flag = 0
where flag = 1
      and id = 20096;


merge into T_MAP_NEW_OTBOR_ART_ASIZE a
using (select /*'4.5' asize, r_art.analog analog, '0065' id_shop*/ 31 asize, '17СМФ31213' analog, '3622' id_shop from dual) b
on (a.asize = b.asize and a.analog = b.analog and a.id_shop = b.id_shop)
when not matched then insert (asize, analog, id_Shop) values (b.asize, b.analog, b.id_Shop);

select * from t_map_new_poexalo where id = 20096;
select * from t_map_new_poexalo where id = 19891 and boxty = 'ALONE';

select * from T_MAP_NEW_OTBOR_ART;
select * from T_MAP_NEW_OTBOR_ART_ASIZE;
select * from T_MAP_NEW_OTBOR;
select * from T_MAP_NEW_OTBOR
      where (id_shop, art) in (select id_shop, art
                               from TDV_MAP_NEW_OST_SALE
                               where out_block = 1);