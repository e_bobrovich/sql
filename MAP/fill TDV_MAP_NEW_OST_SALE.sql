-- Остатки вместе с продажами в разрезе аналогов

--insert into TDV_MAP_NEW_OST_SALE
select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 0 out_block, sum(kol_rc) kol_rc,
       sum(kol2) kol2
from (
       -- текущие остатки
       select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
       from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, z.season, null,
                      sum(kol_rc) kol_rc, sum(a2.kol2) kol2
               from tdv_map_osttek a2
               left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
               left join s_art z on a2.art = z.art
               group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize, z.season
               having sum(kol) > 0) a
       where ',Зимняя,' like '%,' || season || ',%' -- только нужный сезон        
       group by id_shop, a.art, asize, season
       
       union all
       
       -- все продажи данной обуви за выбранный период
       select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0, sum(b.kol) kol_sale, 0 kol_rc,
               0 kol2
       from pos_sale1 a
       inner join pos_sale2 b on a.id_chek = b.id_chek
                                 and a.id_shop = b.id_shop
       inner join s_art c on b.art = c.art
       left join TDV_MAP_NEW_ANAL d on b.art = d.art
       where a.bit_close = 'T'
             and a.bit_vozvr = 'F'
             and b.scan != ' '
             and b.procent = 0
             and a.id_shop in (select shopid
                               from tdm_map_new_shop_group
                               where s_group = i_s_group)
             and trunc(a.sale_date) >= trunc(i_sales_start_date)
             and trunc(a.sale_date) <= trunc(i_sales_end_date)
             and a.id_shop in (select shopid
                               from tdm_map_new_shop_group
                               where s_group = i_s_group)
            and ',Зимняя,' like '%,' || c.season || ',%' -- только нужный сезон  
       group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season
      ) a

group by id_shop, a.art, a.asize;
