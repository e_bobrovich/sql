SET SERVEROUTPUT ON;

declare
  v_capacity number:= 0;
  v_kol_ost number:= 0;
  v_kol_stay number:= 0;
begin
  
  for r_shop in (
    --select shopid id_shop, dtype capacity from tdm_map_new_shop_group where s_group = 15902
    select a.shopid id_shop, 
    a.dtype -  nvl(b.sum_kol_stay, 0) capacity from tdm_map_new_shop_group a
    left join (select id_shop, sum(kol_stay) sum_kol_stay from t_map_stay_in_shop_kol group by id_shop) b on a.shopid = b.id_shop
    where a.s_group = 15902
  )
  loop
    dbms_output.put_line('r_shop.id_shop: '||r_shop.id_shop||' '||r_shop.capacity);
    v_capacity := r_shop.capacity;
    
    for r_art in (
      select x.id_shop, x.art, y.priority, y.kol_sale, sum(x.kol_ost) kol_ost from t_map_stay_in_shop_kol x
      left join (
        select a.id_shop, b.art, a.priority, a.kol_sale from t_map_stay_in_shop a
        inner join tdv_map_new_anal b on a.art = b.analog
      ) y on x.id_shop = y.id_shop and x.art = y.art and x.priority = y.priority
      where x.id_shop = r_shop.id_shop and x.kol_ost > 0 and (x.kol_stay is null or x.kol_stay = 0)
      group by x.id_shop, x.art, y.priority, y.kol_sale 
      order by y.priority, y.kol_sale desc -- сортировка по номеру условия(приоритету) и количеству продаж этого артикула
    )
    loop
      v_kol_ost := r_art.kol_ost;
      dbms_output.put_line('r_art.art: '||r_art.art||' '||'v_kol_ost: '||v_kol_ost);

      if (v_capacity <= 0 or v_capacity <= v_kol_ost) then
        exit;
      elsif (v_capacity > v_kol_ost) then
--        update t_map_stay_in_shop_kol set set kol_stay = kol_ost
--        where id_shop = r_shop.id_shop and art = r_art.art and priority = r_art.priority;
        v_capacity := v_capacity - v_kol_ost;
      end if;
      
      dbms_output.put_line('r_art.art: '||r_art.art||' '||'v_kol_stay: '||v_kol_stay||' v_capacity: '||v_capacity);
    end loop;
    
    --commit;
  end loop;
  
end;
