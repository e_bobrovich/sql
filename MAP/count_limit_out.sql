set serveroutput on;

select * from t_map_new_poexalo a
inner join tdm_map_new_shop_group b on a.id_shop_from = b.shopid and 15902 = b.s_group
where a.id= 8874
;

select shopid, count_limit_out from tdm_map_new_shop_group where s_group = 15902
;

select * from tdm_map_new_shop_group where s_group = 15901 and count_limit_out = 200;
select * from tdm_map_new_shop_group where s_group = 15901;


select * from tdm_map_new_shop_group where s_group = 15902;
select * from t_map_new_poexalo where id = 8874;


select id_shop_from, sum(kol) from t_map_new_poexalo 
where id = 8899
group by id_shop_from
;

select * from t_map_stay_in_shop;
select * from t_map_stay_in_shop_kol;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

select shopid, count_limit_out from tdm_map_new_shop_group where s_group = 15901 and count_limit_out != 10000;

select id_shop_from, count(*) from t_map_new_poexalo a 
where a.id = 8897 group by id_shop_from;

--select count(*) from (

select a.*, b.* from t_map_new_poexalo a
left join t_map_stay_in_shop b on a.id_shop_from = b.id_shop and a.art = b.art
where a.id = 8897
--and b.priority is not null
order by b.priority desc

--)
;

select x.*, nvl(y.kol_sale, 0) kol_sale, z.priority priority from t_map_new_poexalo x 
left join (
  select a.id_shop, b.art, b.asize, sum(b.kol) kol_sale
  from pos_sale1 a
  inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
  where a.bit_close = 'T'
       and a.bit_vozvr = 'F'
       and b.scan != ' '
       and b.procent = 0
       and (a.id_shop) in (select shopid from tdm_map_new_shop_group where s_group = '15902')
       and trunc(a.sale_date) >= trunc(to_date('01.09.2018', 'dd.mm.yyyy'))
  group by a.id_shop, b.art, b.asize
) y on x.id_shop_from = y.id_shop and x.art = y.art and x.asize = y.asize
left join t_map_stay_in_shop_kol z on x.id_shop_from = z.id_shop and x.art = z.art and x.asize = z.asize
where x.id = 8899 and x.id_shop_from = '4507'
order by  z.priority desc, nvl(y.kol_sale, 0)

;


select a.id_shop, b.art, b.asize, sum(b.kol) kol_sale
from pos_sale1 a
inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
where a.bit_close = 'T'
     and a.bit_vozvr = 'F'
     and b.scan != ' '
     and b.procent = 0
     and (a.id_shop) in (select shopid from tdm_map_new_shop_group where s_group = '15902')
     and trunc(a.sale_date) >= trunc(to_date('01.09.2018', 'dd.mm.yyyy'))
group by a.id_shop, b.art, b.asize
;


select * from t_map_new_poexalo where id = 9225 and id_shop_from = '3309';
---------------------------------------------------------------------------------------------------------
select sum(kol)  from t_map_new_poexalo x where x.id = 9225 and x.id_shop_from = /*r_shop.id_shop*/ '3309';

select 
sum(kol)
--nvl(y.analog, x.art) aart, x.*, y.* 
from t_map_new_poexalo x 
left join tdv_map_new_anal y on x.art = y.art
where x.id = 9225 and x.id_shop_from = /*r_shop.id_shop*/ '3309';
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

declare
 v_count_limit_out number := 0;
 v_kol_out number := 0;
BEGIN
    FOR r_shop IN (
    --select shopid id_shop, dtype, count_limit_out from tdm_map_new_shop_group where s_group = 16402 and count_limit_out != 10000
      select a.shopid id_shop, a.dtype, a.count_limit_out , b.kol_ost
      from tdm_map_new_shop_group a
      inner join (
        select id_shop, sum(kol) kol_ost from (
          select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol
          from e_osttek_online a2
          inner join s_art c on a2.art = c.art
          left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
          left join st_muya_art z on a2.art = z.art
          left join (select id_shop, art from cena_skidki_all_shops where substr(to_char(cena), -2) = '99') s on a2.id_shop = s.id_shop and a2.art = s.art
          where a2.scan != ' '
               and a2.id_shop in (select shopid
                                  from tdm_map_new_shop_group
                                  where s_group = 16402)
               and a2.id_shop in (select shopid
                                  from st_shop z
                                  where z.org_kod = 'SHP')
               and a2.procent = 0 -- исключение некондиции
               and x.scan is null -- исключение брака
               and z.art is null -- исключение покупной
               and c.groupmw in ('Мужские', 'Женские') -- исключение детской
               and c.season = 'Зимняя'
               and case when substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') then s.art else null end is null
          group by a2.id_shop, a2.art, a2.asize, c.season
          having sum(kol) > 0
        ) group by id_shop 
      ) b on a.shopid = b.id_shop
      where a.s_group = 16402 and a.count_limit_out != 10000 and a.shopid = '3309'
    ) loop
      /* 
      1) Если (остаток - вместимость) > 300 пар, то максимум на вывоз (v_count_limit_out) = 300 пар - 
      2) Если (остаток - вместимость) <= 300 пар, то максимум на вывоз (v_count_limit_out) = (остаток - вместимость)
      */
      if r_shop.kol_ost - r_shop.dtype > 300 then
        v_count_limit_out := 300;
      else
        if r_shop.kol_ost - r_shop.dtype < 0 then
          v_count_limit_out := 0;
        else
          v_count_limit_out := r_shop.kol_ost - r_shop.dtype;
        end if;
      end if;
      dbms_output.put_line('r_shop '||r_shop.id_shop||' '||r_shop.kol_ost||' '||r_shop.dtype);
      for r_art in (
        select 
        x.id_shop_from, nvl(y.analog, x.art) art, z.priority, nvl(y.kol_sale, 0) kol_sale,  sum(x.kol) kol_out  
        from t_map_new_poexalo x 
        left join tdv_map_new_anal y on x.art = y.art
        left join (
          select a.id_shop, b.art, sum(b.kol) kol_sale
          from pos_sale1 a
          inner join pos_sale2 b on a.id_chek = b.id_chek and a.id_shop = b.id_shop
          where a.bit_close = 'T'
               and a.bit_vozvr = 'F'
               and b.scan != ' '
               and b.procent = 0
               and (a.id_shop) in (select shopid id_shop from tdm_map_new_shop_group where s_group = 16402)
               and trunc(a.sale_date) >= trunc(to_date('01.09.2018','dd.mm.yyyy'))
          group by a.id_shop, b.art
        ) y on x.id_shop_from = y.id_shop and x.art = y.art
        left join t_map_stay_in_shop_kol z on x.id_shop_from = z.id_shop and x.art = z.art and x.asize = z.asize
        where x.id = 9225 and x.id_shop_from = r_shop.id_shop
        group by x.id_shop_from, nvl(y.analog, x.art), z.priority, nvl(y.kol_sale, 0)
        order by  z.priority desc, nvl(y.kol_sale, 0)
      ) loop
      
        v_kol_out := r_art.kol_out;
        dbms_output.put_line('r_art.art: '||r_art.art||' '||'v_kol_out: '||v_kol_out);
        if (v_count_limit_out <= 0 or v_count_limit_out < v_kol_out) then
          delete from t_map_new_poexalo 
          where id = 9225 and id_shop_from = r_shop.id_shop and (art = r_art.art or art in (select art from tdv_map_new_anal where analog = r_art.art));
          dbms_output.put_line('deleted');
        elsif (v_count_limit_out >= v_kol_out) then
          v_count_limit_out := v_count_limit_out - v_kol_out;
          dbms_output.put_line('stay');
        end if;
        dbms_output.put_line('r_shop.id_shop :'||r_shop.id_shop||' r_art.art: '||r_art.art||' '||'v_kol_out: '||v_kol_out||' v_count_limit_out: '||v_count_limit_out);
      end loop;
      
      v_count_limit_out := 0;
      --commit;
    end loop;
  end;