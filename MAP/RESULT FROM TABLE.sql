select 
--* 
id_shop "ОТДЕЛЕНИЕ",
shopname "НАЗВАНИЕ",
region "РЕГИОН",
art "АРТИКУЛ",
kol_ost "ОСТАТОК",
kol_poexalo "ВЫВОЗ", 
kol_sale03 "ПРОДАЖИ(МАРТ)",
kol_sale02 "ПРОДАЖИ(ФЕВРАЛЬ)",
kol_sale "ПРОДАЖИ(СЕГОДНЯ)",
kol_filter "ОТФИЛЬТРОВАНО"
from t_map_bep001_result
where id = 9054 
--and kol_ost - kol_poexalo > 0 and kol_poexalo > 0
order by region, id_shop, kol_poexalo desc, kol_sale03 desc, kol_sale02 desc, kol_sale desc;


insert into t_map_bep001_result
select 9054 id, o.id_shop, s.shopname, r.region, o.art, 
o.kol_ost,
nvl(p.kol_poexalo, 0) kol_poexalo,
nvl(sale03.kol_sale03, 0) kol_sale03,
nvl(sale02.kol_sale02, 0) kol_sale02,
nvl(sale.kol_sale, 0) kol_sale,
nvl(f.kol_filter, 0) kol_filter
from (
  select a.id_shop, a.art, sum(a.kol) kol_ost --количество на остатке до вывоза
  from e_osttek_online a
  inner join s_art c on a.art = c.art
  inner join (select shopid from tdm_map_new_shop_group where s_group = 16001) d on a.id_shop = d.shopid
  where c.season = 'Зимняя'
  group by a.id_shop, a.art
  having sum(kol) > 0
) o
inner join (select region, id_pos from st_region_shop) r on substr(o.id_shop,1,2) = r.id_pos
inner join s_shop s on o.id_shop = s.shopid

left join (
  select a.id_shop_from id_shop, a.art, sum(a.kol) kol_poexalo from t_map_new_poexalo a
  where a.id = 9054
  group by a.id_shop_from, a.art
) p on o.id_shop = p.id_shop and o.art = p.art

left join (
  select a.id_shop, b.art, sum(b.kol) kol_sale03 from pos_sale1 a
  inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek
  inner join (select shopid from tdm_map_new_shop_group where s_group = 16001) c on a.id_shop = c.shopid
  where a.bit_close = 'T'
    and a.bit_vozvr = 'F'
    and trunc(a.sale_date) >= trunc(to_date('01.09.2018','dd.mm.yyyy'))
    and trunc(a.sale_date) <= trunc(to_date('01.03.2019','dd.mm.yyyy'))
  group by a.id_shop, b.art
) sale03 on o.id_shop = sale03.id_shop and o.art = sale03.art

left join (
  select a.id_shop, b.art, sum(b.kol) kol_sale02 from pos_sale1 a
  inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek
  inner join (select shopid from tdm_map_new_shop_group where s_group = 16001) c on a.id_shop = c.shopid
  where a.bit_close = 'T'
    and a.bit_vozvr = 'F'
    and trunc(a.sale_date) >= trunc(to_date('01.09.2018','dd.mm.yyyy'))
    and trunc(a.sale_date) <= trunc(to_date('01.02.2019','dd.mm.yyyy'))
  group by a.id_shop, b.art
) sale02 on o.id_shop = sale02.id_shop and o.art = sale02.art

left join (
  select a.id_shop, b.art, sum(b.kol) kol_sale from pos_sale1 a
  inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek
  inner join (select shopid from tdm_map_new_shop_group where s_group = 16001) c on a.id_shop = c.shopid
  where a.bit_close = 'T'
    and a.bit_vozvr = 'F'
    and trunc(a.sale_date) >= trunc(to_date('01.09.2018','dd.mm.yyyy'))
    and trunc(a.sale_date) <= trunc(systimestamp)
  group by a.id_shop, b.art
) sale on o.id_shop = sale.id_shop and o.art = sale.art

left join (
  select id_shop, art, sum(kol) kol_filter from (
    select a.id_shop, a.art, a.scan, sum(a.kol) kol  --количество штрихкода на остатке под фильтрами 
    from e_osttek_online a
    inner join s_art c on a.art = c.art
    inner join (select shopid from tdm_map_new_shop_group where s_group = 16001) d on a.id_shop = d.shopid
    
    left join pos_brak x on a.scan = x.scan and a.id_shop = x.id_shop
    left join st_muya_art z on a.art = z.art
    left join (select id_shop, art from cena_skidki_all_shops where substr(to_char(cena), -2) = '99') s on a.id_shop = s.id_shop and a.art = s.art
    
    where c.season = 'Зимняя'
    and a.scan != ' '
    and (
      a.procent != 0
      or x.scan is not null
      or z.art is not null 
      or c.groupmw not in ('Мужские', 'Женские')
      or case when substr(a.id_shop, 1, 2) in ('36', '38', '39', '40') then s.art else null end is not null
    )
    group by a.id_shop, a.art, a.scan
    having sum(kol) > 0
  ) group by id_shop, art 
) f on o.id_shop = f.id_shop and o.art = f.art
--order by r.region, o.id_shop, nvl(sale03.kol_sale03,0) desc, nvl(sale02.kol_sale02,0) desc, nvl(sale.kol_sale,0) desc, nvl(p.kol_poexalo,0) desc
--order by r.region, o.id_shop, nvl(p.kol_poexalo,0) desc, nvl(sale03.kol_sale03,0) desc, nvl(sale02.kol_sale02,0) desc, nvl(sale.kol_sale,0) desc
;