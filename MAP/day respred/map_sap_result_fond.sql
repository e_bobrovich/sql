
--россыпь фонда из подсортировки с учетом вчерашнего фонда (сегодняшний + предыдущие)
select
nvl(a.art, c1.art) art, nvl(a.verme, 0) /*+ nvl(c1.verme, 0)*/ verme, ' ' boxty, ' ' a1, ' ' a2, ' ' a3, nvl(a.id_shop,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, 'X_REZ_FT' id_shop,
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost) and id_shop = 'fond' --'X_REZ_FT'
    group by art, id_shop, boxty
) a
full join (
    select a.kunnr, a.art, sum(a.verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where kunnr = 'X_REZ_FT' and boxty = ' ' and asize is not null and trans = ' '
    and rasp = 'X'  and ablad = 48
    group by a.kunnr, a.art, a.asize
    
    union all
    select kunnr, art, sum(verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from (
      select 'X_REZ_FT' kunnr, '000' art, 100 verme, '37,0' asize from dual
      union all
      select 'X_REZ_FT' kunnr, '000' art, 200 verme, '39,0' asize from dual
      union all
      select 'X_REZ_FT' kunnr, '000' art, 300 verme, '38,0' asize from dual
    ) group by kunnr, art, asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = a.id_shop
left join s_all_mat g on nvl(a.art, c1.art) = g.art
                     and nvl(a.asize, c1.asize) = case when instr(replace(to_char(g.asize),'.',',') , ',') != 0 then replace(g.asize,'.',',') else replace(g.asize,'.',',')||',0' end

;








select a.art, sum(verme) verme, case when a.id_shop in ('X_FND_INET','0010000854', '0010000578') then  ' ' else boxty end boxty, ' ' f1, ' ' f2, ' ' f3, nvl(c.shopnum,a.id_shop) shopnum,
g.matnr, cast(case when a.id_shop in ('X_FND_INET','0010000854','0010000578') then case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end else '0,0' end as varchar2(5)) asize --else replace(boxty,'.',',')||',0' end else '0,0' end as varchar2(5)) asize
from (select nvl(a.art, b.art) art, nvl(a.boxty, to_number(b.asize)) boxty, nvl(a.verme,0) verme,
        nvl(case when a.id_shop not in ( '0010000854', '0010000578') then  'X_FND_INET' else a.id_shop end, b.kunnr) id_shop
          from
          (select * from tdv_map_new_result a
          where verme>0 and a.group_ras in ('imrf', 'lamoda', 'wb_ross')) a
          full join
          (select  art, asize, sum(verme), kunnr from tdv_map_new_sap_reserv  where (kunnr in ('X_FND_INET','0010000854') or  (kunnr = '0010000578' and boxty = ' ')) group by art, asize, kunnr) b
          on a.art = b.art and boxty = to_number(b.asize) and a.id_shop = b.kunnr) a

left join firm.s_shop c on a.id_shop  = case when substr(c.shopid,1,2) = '00' and length(c.shopid)<10 then '7'||substr(c.shopid,2)  else c.shopid end
inner join s_art d on a.art = d.art
left join s_all_mat g on a.art = g.art and a.boxty = g.asize
--where verme>0 and a.group_ras = 'imrf'--and d.mtart = 'ZFRT'
group by a.art,  boxty,  c.shopnum, a.id_shop, g.matnr
having sum(verme) > 0
union all

-- данные для оффлайн магазинов
-- select a.art, sum(verme) verme, boxty, ' ' f1, ' ' f2, ' ' f3, c.shopnum, cast(' ' as varchar2(20)) matnr, cast('0,0' as varchar2(5)) asize
-- from firm.tdv_map_new_result a
-- left join firm.s_shop c on a.id_shop  = case when substr(c.shopid,1,2) = '00'  and length(c.shopid)<10 then '7'||substr(c.shopid,2) else c.shopid end
-- inner join s_art d on a.art = d.art
-- where verme>0 and a.group_ras not in ('imrf', 'lamoda', 'wb_ross') and a.group_ras != 'podsort' --and d.mtart = 'ZFRT'
-- group by a.art,  boxty,  c.shopnum

-- данные для оффлайн магазинов
select nvl(a.art,b.art) art, nvl(a.verme,0) + nvl(b.VERME, 0) as verme, nvl(b.boxty, a.BOXTY) boxty, f1, f2, f3, nvl(a.SHOPNUM, b.KUNNR) shopnum, nvl(a.matnr, ' ') matnr, nvl(a.ASIZE, '0,0') asize
from (select a.art, sum(a.verme) as verme, a.boxty, ' ' f1, ' ' f2, ' ' f3, c.shopnum, cast(' ' as varchar2(20)) matnr, cast('0,0' as varchar2(5)) as asize
from firm.tdv_map_new_result a
left join firm.s_shop c on a.id_shop  = case when substr(c.shopid,1,2) = '00'  and length(c.shopid)<10 then '7'||substr(c.shopid,2) else c.shopid end
inner join s_art d on a.art = d.art
where a.verme>0 and a.group_ras not in ('imrf', 'lamoda', 'wb_ross') and a.group_ras != 'podsort' --and d.mtart = 'ZFRT'
group by a.art,  a.boxty,  c.shopnum) a
full join (select art, kunnr, boxty, sum(verme) verme
 from tdv_map_new_sap_reserv x3
 where x3.trans != 'X'
       and x3.boxty != ' '
       and substr(nvl(kunnr, ' '), 1, 2) != 'X_' and x3.kunnr != ' '
       group by art, kunnr, boxty) b on a.ART = b.ART and a.BOXTY = b.BOXTY and a.shopnum = b.KUNNR

union all

-- фирменная торговля
select nvl(b.art, c1.art) art , nvl(sum(b.verme), 0) verme, nvl(b.boxty, c1.boxty) , ' ', ' ', ' ', nvl(b.res_type, c1.kunnr), ' ', '0,0'
from  (select b.* from firm.TDV_MAP_NEW_RESRV b inner join s_art c2 on b.art = c2.art where res_type = 'X_REZ_FT' ) b --and c2.mtart = 'ZFRT'
full join (select * from tdv_map_new_sap_reserv where kunnr = 'X_REZ_FT') c1 on b.art = c1.art and b.boxty = c1.boxty
--where b.res_type != 'X_REZ_SD' and  verme>0
group by nvl(b.art, c1.art)  ,  nvl(b.boxty, c1.boxty) , nvl(b.res_type, c1.kunnr)
union all

-- оптовая торговля
select nvl(b.art, c1.art) art , nvl(sum(b.verme), 0) verme, nvl(b.boxty, c1.boxty) , ' ', ' ', ' ', nvl(b.res_type, c1.kunnr), ' ', '0,0'
from  (select b.* from firm.TDV_MAP_NEW_RESRV b inner join s_art c2 on b.art = c2.art where res_type = 'X_REZ_SD' ) b --and c2.mtart = 'ZFRT'
full join (select * from tdv_map_new_sap_reserv where kunnr = 'X_REZ_SD') c1 on b.art = c1.art and b.boxty = c1.boxty
--where b.res_type != 'X_REZ_SD' and  verme>0
group by nvl(b.art, c1.art)  ,  nvl(b.boxty, c1.boxty) , nvl(b.res_type, c1.kunnr)
union all

-- остаток нераспределенного
select a.art,  a.verme - nvl(b.verme,0) -  nvl(c.verme,0) - nvl(d.verme,0) , a.boxty, ' ', ' ', ' ', 'X_NERASP', ' ', '0,0'
from (select a.art, sum(a.verme)/f.kol verme,  a.boxty
        from st_sap_ob_ost a
        left join (select boxty, sum(kol) kol from T_SAP_BOX_ROST group by boxty) f on a.boxty = f.boxty
        where a.boxty NOT IN (' ', 'N') group by a.art, a.boxty, f.kol) a
full join (select art, boxty, sum(verme) verme from  tdv_map_new_result b where verme > 0 group by art, boxty) b on a.art = b.art and a.boxty  = b.boxty
full join (select art, boxty, sum(verme) verme from  TDV_MAP_NEW_RESRV b where verme > 0 group by art, boxty) c on a.art = c.art and a.boxty  = c.boxty
full join (select art, boxty, sum(verme) verme from  tdv_map_new_sap_reserv b where  trans != 'X' and boxty != ' ' and substr(nvl(kunnr, ' '),1,2) != 'X_' and verme > 0 group by art, boxty) d on a.art = d.art and a.boxty  = d.boxty
where a.verme - nvl(b.verme,0) -  nvl(c.verme,0)- nvl(d.verme,0) >= 0
union all


-- ?
/*select f.art, f.verme, ' ', ' ', ' ', ' ', c.shopnum, g.matnr, case when instr(to_char(f.asize),',')!= 0 then to_char(f.asize) else to_char(f.asize)||',0' end
from TDV_MAP_NEW_RESULT_SIZE f
left join s_shop c on f.id_shop  = c.shopid --case when substr(c.shopid,1,2) = '00' then '7'||substr(c.shopid,2) else c.shopid end
left join s_all_mat g on f.art = g.art and f.asize = g.asize
where verme > 0


union all*/

--россыпь подсортировки с учетом вчерашних транзитов
select
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, ' ' boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, id_shop,
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost) and id_shop != 'fond' --'X_REZ_FT'
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select a.kunnr, a.art, sum(a.verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where kunnr like 'S%' and boxty = ' ' and asize is not null and trans = ' ' --and kunnr not in (select id_shop from st_sap_ob_reserv)
    and rasp = 'X'  and ablad = 48 --and b.season != 'Зимняя'
    group by a.kunnr, a.art, a.asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = c.shopnum
left join s_all_mat g on nvl(a.art, c1.art) = g.art
                     and nvl(a.asize, c1.asize) = case when instr(replace(to_char(g.asize),'.',',') , ',') != 0 then replace(g.asize,'.',',') else replace(g.asize,'.',',')||',0' end
union all

--короба подсортировки
select
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, nvl(a.boxty, c1.boxty) boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, ' ' matnr, '0,0' asize
--a.art,a.verme, a.boxty, ' ', ' ', ' ', c.shopnum, ' ', '0,0', c1.*
from (
    select art, sum(verme) verme, id_shop, boxty
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty in (select boxty from t_sap_box_rost) and id_shop != 'fond' --'X_REZ_FT'
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select a.kunnr, a.art, sum(a.verme) verme, a.boxty
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where boxty != ' ' and trans = ' ' and kunnr not in (select id_shop from st_sap_ob_reserv) and kunnr not like 'X%' and kunnr != ' '
    and rasp = 'X'  and ablad = 48 --and b.season != 'Зимняя'
    group by a.kunnr, a.art, a.boxty
)  c1 on a.art = c1.art and a.boxty = c1.boxty and c1.kunnr = c.shopnum

union all
--россыпь фонда из подсортировки с учетом вчерашнего фонда (сегодняшний заменяет предыдущий)
select
nvl(a.art, c1.art) art, nvl(a.verme, 0) /*+ nvl(c1.verme, 0)*/ verme, ' ' boxty, ' ' a1, ' ' a2, ' ' a3, nvl(a.id_shop,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, 'X_REZ_FT' id_shop,
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost) and id_shop = 'fond' --'X_REZ_FT'
    group by art, id_shop, boxty
) a
full join (
    select a.kunnr, a.art, sum(a.verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where kunnr = 'X_REZ_FT' and boxty = ' ' and asize is not null and trans = ' '
    and rasp = 'X'  and ablad = 48
    group by a.kunnr, a.art, a.asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = a.id_shop
left join s_all_mat g on nvl(a.art, c1.art) = g.art
                     and nvl(a.asize, c1.asize) = case when instr(replace(to_char(g.asize),'.',',') , ',') != 0 then replace(g.asize,'.',',') else replace(g.asize,'.',',')||',0' end
;


select * from map_sap_result where shopnum = 'X_REZ_FT' and boxty = ' ';
