--select distinct(asize) from (
select 
--nvl(a.art, c1.art) art, nvl(c.shopnum,c1.kunnr) shopnum, nvl(a.asize, c1.asize) asize, nvl(a.verme, 0) verme, g.matnr
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, ' ' boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, id_shop, 
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from /*tdv_map_new_result*/(
        select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0 as step_no, 0 sum_transit, 0 sum_reserv, 0 sum_ost, 0 sum_rc_ost, 'podsort' group_ras 
        from t_map_new_poexalo
        where id = 10281
    ) where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost) and boxty is not null
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select kunnr, art, sum(verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv 
    where kunnr like 'S%' and boxty = ' ' and asize is not null and trans = ' '
    group by kunnr, art, asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = c.shopnum
left join s_all_mat g on nvl(a.art, c1.art) = g.art and nvl(a.asize, c1.asize) = g.asize

 
--where 
--a.art is not null
--and 
--c1.art is not null                                                                                                               
--)
;

select 
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, ' ' boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, id_shop, 
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from /*tdv_map_new_result*/(
        select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0 as step_no, 0 sum_transit, 0 sum_reserv, 0 sum_ost, 0 sum_rc_ost, 'podsort' group_ras 
        from t_map_new_poexalo
        where id = 10281
    ) where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost) and boxty is not null
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select kunnr, art, sum(verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv 
    where kunnr like 'S%' and boxty = ' ' and asize is not null and trans = ' '
    group by kunnr, art, asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = c.shopnum
left join s_all_mat g on nvl(a.art, c1.art) = g.art and nvl(a.asize, c1.asize) = g.asize
;

select distinct(asize) from (
    select kunnr, art, 
    --asize,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize,
    verme 
    from tdv_map_new_sap_reserv where kunnr like 'S%' and boxty = ' '-- and trans = ' '
);

select distinct(asize) from (
    select art, verme, id_shop, 
    --to_char(boxty ,'99999.9') asize
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    --case when instr(to_char(boxty ,'99999.9'),'.') = 0 then '0' else to_char(boxty ,'99999.9') end asize
    from (
        select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0 as step_no, 0 sum_transit, 0 sum_reserv, 0 sum_ost, 0 sum_rc_ost, 'podsort' group_ras from t_map_new_poexalo
        where id = 10281
    ) where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost) and boxty is not null
) a;



select a.art,a.verme, a.boxty, ' ', ' ', ' ', c.shopnum, ' ', '0,0'
from (
    select art, sum(verme) verme, id_shop, boxty
    from /*tdv_map_new_result*/(
        select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0 as step_no, 0 sum_transit, 0 sum_reserv, 0 sum_ost, 0 sum_rc_ost, 'podsort' group_ras 
        from t_map_new_poexalo
        where id = 10281
    ) where group_ras = 'podsort' and boxty in (select boxty from t_sap_box_rost) and boxty is not null
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid;