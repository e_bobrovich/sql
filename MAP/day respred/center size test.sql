select * from (
    select 
    a.id_shop, a.art, b.groupmw, b.style,
    count(a.asize) v_count,
    nvl(sum(
        case when a.asize in (4, 4.5, 5, 5.5)    and b.groupmw = 'Женские' and b.style != 'Sport' then 1
             when a.asize in (8, 8.5, 9)         and b.groupmw = 'Мужские' and b.style != 'Sport' then 1
             when a.asize in (5, 5.5, 6, 6.5)    and b.groupmw = 'Женские' and b.style = 'Sport' then 1
             when a.asize in (8.5, 9, 9.5, 10)   and b.groupmw = 'Мужские' and b.style = 'Sport' then 1
             when a.asize in (37, 38, 39)        and b.groupmw = 'Женские' then 1
             when a.asize in (42, 43)            and b.groupmw = 'Мужские' then 1
             else 0 end
    ), 0) v_cent_size
    from (
        select id_shop, art, asize from tdv_map_new_ost_sale_after where kol_ost > 0 and id = 10736 group by id_shop, art, asize
        union all
        select id_shop, art, asize from tdv_new_shop_ost_after where verme > 0 and id = 10736 group by id_shop, art, asize
        union all
        select id_shop, text1 art, asize from t_map_new_poexalo where kol > 0 and id = 10736 and boxty is null group by id_shop, text1, asize
    ) a
    left join s_art b on a.art = b.art
    left join (
        select id_shop, art, asize from t_map_new_poexalo where kol > 0 and id = 10736 and boxty is null group by id_shop, art, asize
    ) c on a.art = c.art and a.id_shop = c.id_shop --and a.asize = c.asize
    group by a.id_shop, a.art, b.groupmw, b.style
)

--where v_cent_size < 1 or (groupmw = 'Мужские' and v_count < 2) or (groupmw = 'Женские' and v_count < 3)
;


--id_shop = '0063' and art = '8938775'

select * from t_map_new_poexalo where id = 10736;
select id_shop, art, asize from tdv_map_new_ost_sale where kol_ost > 0 and id_shop = '0063' and art = '8938775' group by id_shop, art, asize;
select id_shop, art, asize from tdv_new_shop_ost where verme > 0 and id_shop = '0063' and art = '8938775' group by id_shop, art, asize;
select id_shop, art, asize from t_map_new_poexalo where kol > 0 and id = 10736 and id_shop = '0063' and art = '8938775' and boxty is null group by id_shop, art, asize;
select * from tdv_map_new_ost_sale where kol_ost > 0 and id_shop = '2328' and art = '1826015';
select * from tdv_new_shop_ost where verme > 0 and id_shop = '2328' and art = '1826015';

select * from t_map_new_poexalo where kol > 0 and id = 10736 and id_shop = '2213' and art = '8916110';

select id, id_shop, art, asize, count(*) from tdv_map_new_ost_sale_after where id = 10736 group by id, id_shop, art, asize having count(*) > 1;
delete from tdv_map_new_ost_sale_after where id = 9574;

select * from tdv_map_new_ost_sale_after where id = 10736 and id_shop = '2328' and art = '1826015';

select id_shop, art, asize from tdv_map_new_ost_sale_after where kol_ost > 0 and id = 10736 group by id_shop, art, asize;
select id_shop, art, asize from tdv_new_shop_ost_after where verme > 0 and id = 10736  group by id_shop, art, asize;
select id_shop, art, asize from t_map_new_poexalo where kol > 0 and id = 10736  group by id_shop, art, asize;