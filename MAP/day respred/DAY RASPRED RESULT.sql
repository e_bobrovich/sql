select a.id_shop "НОМЕР ПОЛУЧ.", c.shopname "ПОЛУЧАТЕЛЬ", sum(a.kol) "КОЛИЧЕСТВО" 
from t_map_new_poexalo a 
inner join s_shop b on a.id_shop_from = b.shopid
inner join s_shop c on a.id_shop = c.shopid
where a.id = 10735
group by a.id_shop_from, b.shopname, a.id_shop, c.shopname 
order by a.id_shop_from, a.id_shop;

select * from t_map_day_sort_report where id = 10735;

-- РЕЗУЛЬТАТ ПОДСОРТИРОВКИ
select 
a.id_shop "НОМЕР МАГАЗИНА",
a.season "СЕЗОН",
a.art "АРТИКУЛ",
a.analog "АНАЛОГ",
a.asize "РАЗМЕР",
a.size_ost_kol "ОСТАТОК РАЗМЕРА",
a.analog_ost_kol "ОСТАТОК АНАЛОГА",
a.analog_sale_kol "ПРОДАЖИ АНАЛОГА",
a.raspred_kol "РАСПРЕДЕЛЕНО",
a.block_no ,
a.stage "ЭТАП"
from t_map_day_sort_report a
where a.id = 10735
order by block_no, id_shop, art, asize;

-- ОСТАТОК СКАЛАДА ПОСЛЕ ПОДСОРТИРОВКИ
select 
--sum(kol)
a.season "СЕЗОН",
a.art "АРТИКУЛ",
a.stype "ТИП", 
a.asize "РАЗМЕР/КОРОБ", 
a.kol "КОЛИЧЕСТВО" 
from (
  select art, season, 'россыпь' stype, to_char(asize) asize, kol, id from t_map_new_otbor_after where id = 9575
  union all 
  select art, season, 'короб' stype, boxty, kol, id from t_all_rasp_box_after where id = 9575
) a where kol > 0 order by season, art, asize
;

-- ОСТАТОК СКАЛАДА ДО ПОДСОРТИРОВКИ
select 
  a.season "СЕЗОН",
  a.art "АРТИКУЛ",
  a.stype "ТИП", 
  a.asize "РАЗМЕР/КОРОБ", 
  a.kol "КОЛИЧЕСТВО" 
from (
  select art, season, 'россыпь' stype, to_char(asize) asize, kol, id from t_map_new_otbor_before where id = 9575
  union all 
  select art, season, 'короб' stype, boxty, kol, id from t_all_rasp_box_before where id = 9575
) a 
where kol > 0 order by season, art, asize;


select 
a.id_shop "НОМЕР МАГАЗИНА",
a.season "СЕЗОН",
a.art "АРТИКУЛ",
a.text1 "АНАЛОГ",
a.boxty "КОРОБ",
a.asize "РАЗМЕР",
--b.kol_ost "ОСТАТОК РАЗМЕРА",
nvl((select sum(kol) kol_ost 
from e_osttek_online where id_shop = a.id_shop and art = a.art and asize = a.asize group by id_shop, art, asize having sum(kol) > 0
),0) "ОСТАТОК РАЗМЕРА",
a.text2 "ОСТАТОК АНАЛОГА",
a.text3 "ПРОДАЖИ АНАЛОГА",
a.kol "РАСПРЕДЕЛЕНО",
a.block_no ,
case 
  when a.block_no = 1 then 'короб'
  when a.block_no = 2 then 'желтый'
  when a.block_no = 3 then 'синий'
  when a.block_no = 4 then 'зеленый'
  when a.block_no = 5 then '2 пара'
  when a.block_no = 6 then 'крассный'
  when a.block_no = 7 then '2 пара'
  
  when a.block_no = 8 then 'короб'
  when a.block_no = 9 then 'желтый'
  when a.block_no = 10 then 'синий'
  when a.block_no = 11 then 'зеленый'
  when a.block_no = 12 then '2 пара'
  when a.block_no = 13 then 'крассный'
  
  when a.block_no = 14 then 'короб'
  when a.block_no = 15 then 'желтый'
  when a.block_no = 16 then 'синий'
  when a.block_no = 17 then 'зеленый'
  when a.block_no = 18 then '2 пара'
  when a.block_no = 19 then 'крассный'
  when a.block_no = 20 then '2 пара'
  
  when a.block_no = 21 then 'короб'
  when a.block_no = 22 then 'желтый'
  when a.block_no = 23 then 'синий'
  when a.block_no = 24 then 'зеленый'
  when a.block_no = 25 then 'крассный'
end "ЭТАП"
from t_map_new_poexalo a
--left join (select id_shop, art, asize, sum(kol) kol_ost from e_osttek_online group by id_shop, art, asize) b on a.id_shop = b.id_shop and a.art = b.art and a.asize = b.asize
where a.id = 9515--9426
--and id_shop = '0010'
--and season = 'Утепленная'
--and stage = '2 пара'
and a.kol > 0 
--and (select sum(kol) kol_ost 
--from e_osttek_online where id_shop = a.id_shop and art = a.art and asize = a.asize group by id_shop, art, asize having sum(kol) > 0
--) > 0
order by block_no, id_shop, art, asize
;





select * from t_map_new_poexalo where id = 9426;

select 
*
--a.id_shop "НОМЕР МАГАЗИНА",
--a.season "СЕЗОН",
--a.art "АРТИКУЛ",
--a.text1 "АНАЛОГ",
--a.asize "РАЗМЕР"
from t_map_new_otbor_after where id = 9478 and kol > 0;


select id_shop, art, asize, sum(kol) kol_ost 
from e_osttek_online where id_shop = '0009' and art = '1742001/1'
--and asize = '40' 
group by id_shop, art, asize;







select sum(raspred_kol) from t_map_day_sort_report a
where a.id = 9554;
