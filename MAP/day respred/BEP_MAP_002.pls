create or replace package body bep_map_002 is
	/*
  Летняя
  
  Реализация с  01.01 текущего года по дату расчета
  Распределять короб с коэф.реал >80% по синим и остатков <=3  продажи >=3
  Распределение россыпи согласно цветовой схеме не включая красные
  Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (по списку магазинов)
  Распределение россыпи согласно цветовой схеме по красным
  Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
  
  Всесезонная
  
  Реализация с даты расчета минус 2 месяца по дату расчета
  Распределять короб с коэф.реал >80% по синим и остатков <=3  продажи >=3
  Распределение россыпи согласно цветовой схеме не включая красные
  Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
  Распределение россыпи согласно цветовой схеме по красным
  
  Реализация с даты расчета минус 6 месяцев по дату расчета
  Распределять короб с коэф.реал >80% по синим и остатков <=3  продажи >=3
  Распределение россыпи согласно цветовой схеме не включая красные
  Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (по списку магазинов)
  Распределение россыпи согласно цветовой схеме по красным
  Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
  
  Утепленная
  
  Реализация с даты расчета минус 6 месяцев по дату расчета
  Распределять короб с коэф.реал >80% по синим и остатков <=3  продажи >=3
  Распределение россыпи согласно цветовой схеме не включая красные
  Распределение россыпи согласно цветовой схеме по красным
  
  */

	--************** variable *****************************

	pv_season varchar2(100) default ',Летняя,Всесезонная,Утепленная,';
	--pv_sale_start_date date default to_date('20180801', 'yyyymmdd');
	pv_color_id number default 1;

	pv_iter_count number default 0;
	--********************************************************************************************
	--  PROCEDURE
	-- запись в лог
	--********************************************************************************************

	procedure writeLog(i_text varchar2,
										 i_proc varchar2,
										 i_id   in integer) is
	begin
		write_log(i_text, i_proc, i_id);
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 15.05.2019 9:17:31
	--****************************************************************************************************************************************************
	procedure log_time(i_id       number,
										 i_block_no number,
										 i_time     number) is
	begin
		insert into t_map_new_time_log
		values
			(i_id, i_block_no, (dbms_utility.get_time - i_time) / 100, pv_iter_count);
	
		pv_iter_count := 0;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 28.02.2018 9:30:34
	-- Start Procedure:
	-- Заполняет таблицу данными с РЦ и СВХ
	--****************************************************************************************************************************************************
	procedure fill_rc_svx_data(i_s_group number) is
	begin
		insert into tdv_map_rc_data
			select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
			from sklad_ost_owners
			where shopid not in ('0', ' ')
						and shopid in (select shopid
													 from tdm_map_new_shop_group
													 where s_group = i_s_group)
						and shopid in (select shopid
													 from st_shop z
													 where z.org_kod = 'SHP')              
			group by skladid, shopid, art, asize
			union all
			select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
			from dist_center.e_osttek
			where owner in (select shopid
											from tdm_map_new_shop_group
											where s_group = i_s_group)
						and owner in (select shopid
													from st_shop z
													where z.org_kod = 'SHP')
            and DCID not in (select shopid from st_shop where org_kod = 'TST')              
			group by DCID, owner, art, asize
			having sum(kol) > 0;
	
		commit;
	end;

	--********************************************************************************************
	--
	--
	--********************************************************************************************
	procedure fillBaseData(i_s_group          varchar2,
												 i_sales_start_date date,
												 i_sales_end_date   date) is
	begin
	
		execute immediate 'truncate table TDV_MAP_NEW_OST_SALE'; -- - Остатки вместе с продажами в разрезе аналогов
		execute immediate 'truncate table tdv_map_osttek'; -- текущие остатки собраные в одной таблице (нужно было на случай, когда текущие
		execute immediate 'truncate table tdv_map_rc_data '; -- таблица с данными РЦ и СВХ
	
		-- если требуется учет РЦ
		fill_rc_svx_data(i_s_group);
	
		-- текущие остатки:
		insert into tdv_map_osttek
		
			select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
			from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
						 from e_osttek_online a2
						 inner join s_art c on a2.art = c.art
						 left join pos_brak x on a2.scan = x.scan
																		 and a2.id_shop = x.id_shop
						 where a2.scan != ' '
									 and pv_season like '%,' || c.season || ',%'
									 and a2.procent = 0
									 and a2.id_shop in (select shopid
																			from tdm_map_new_shop_group
																			where s_group = i_s_group)
									 and x.scan is null
						 group by a2.id_shop, a2.art, a2.asize, c.season
						 having sum(kol) > 0
						 
						 union all
						 
						 select a2.id_shop, a2.art, a2.asize, 0 kol, c.season, 1 out_block, sum(a2.kol) kol_rc
						 from tdv_map_rc_data a2
						 inner join s_art c on a2.art = c.art
						 where pv_season like '%,' || c.season || ',%'
						 group by a2.id_shop, a2.art, a2.asize, c.season
						 having sum(a2.kol) > 0)
			group by id_shop, art, asize, season;
	
		-- Остатки вместе с продажами в разрезе аналогов
		insert into TDV_MAP_NEW_OST_SALE
			select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 0 out_block, sum(kol_rc) kol_rc, sum(kol2) kol2
			from (
						 -- текущие остатки
						 select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
						 from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, z.season, null, sum(kol_rc) kol_rc, sum(a2.kol2) kol2
										 from tdv_map_osttek a2
										 left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
										 left join s_art z on a2.art = z.art
										 group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize, z.season
										 having sum(kol) > 0) a
						 group by id_shop, a.art, asize, season
						 
						 union all
						 
						 -- все продажи данной обуви за выбранный период
						 select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0, sum(b.kol) kol_sale, 0 kol_rc, 0 kol2
						 from pos_sale1 a
						 inner join pos_sale2 b on a.id_chek = b.id_chek
																			 and a.id_shop = b.id_shop
						 inner join s_art c on b.art = c.art
						 left join TDV_MAP_NEW_ANAL d on b.art = d.art
						 where a.bit_close = 'T'
									 and pv_season like '%,' || c.season || ',%'
									 and a.bit_vozvr = 'F'
									 and b.scan != ' '
									 and b.procent = 0
									 and a.id_shop in (select shopid
																		 from tdm_map_new_shop_group
																		 where s_group = i_s_group)
									 and trunc(a.sale_date) >= i_sales_start_date
									 and trunc(a.sale_date) <= i_sales_end_date
						 group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season) a
			
			group by id_shop, a.art, a.asize;
	
		commit;
	
	end;

	--********************************************************************************************
	--
	--
	--********************************************************************************************

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 24.09.2018 21:17:05
	-- Comments:сбор всего что нужно распределить
	--
	--****************************************************************************************************************************************************
	procedure sum_all_rasp(i_id      in integer default null,
												 i_s_group in number) is
	begin
	
		execute immediate 'truncate table T_MAP_NEW_OTBOR';
		execute immediate 'truncate table t_all_rasp_box';
		execute immediate 'truncate table t_all_rasp_box_work';
		execute immediate 'truncate table TDV_NEW_SHOP_OST'; -- таблица с транзитами и данными с заказами , лежащими на СГП
		execute immediate 'truncate table tdv_map_stock2'; -- артикула на стоке
	
		-- записываю короба к распределению
		insert into t_all_rasp_box
			select d.art, d.boxty, sum(d.verme) / f.kol, 0 flag, e.season, nvl(g.analog, d.art) analog
			from st_sap_ob_ost d
			inner join s_art e on d.art = e.art
			left join (select boxty, sum(kol) kol
								 from T_SAP_BOX_ROST -- таблица с ростовками по коробам
								 group by boxty) f on d.boxty = f.boxty
			left join TDV_MAP_NEW_ANAL g on d.art = g.art
			where d.boxty not in (' ', 'N')
						and pv_season like ',' || e.season || ','
						and d.art not like '%/О'
			group by d.art, d.boxty, f.kol, e.season, nvl(g.analog, d.art);
	
		-- от коробов отнимаю, что в резерве САПА
		update t_all_rasp_box x1
		set x1.kol =
				 (select x1.kol - x2.verme
					from (select art, boxty, sum(verme) verme
								 from tdv_map_new_sap_reserv x3
								 where x3.boxty != ' '
											 and trans != 'X'
											 and (kunnr not in ('X_NERASP', 'X_SD_USE')
                                                or rasp != 'X')
								 group by art, boxty) x2
					where x1.art = x2.art
								and x1.boxty = x2.boxty)
		where exists (select x1.kol - x2.verme
					 from (select art, boxty, sum(verme) verme
									from tdv_map_new_sap_reserv x3
									where x3.boxty != ' '
												and trans != 'X'
												and (kunnr not in ('X_NERASP', 'X_SD_USE')
                                                    or rasp != 'X')
									group by art, boxty) x2
					 where x1.art = x2.art
								 and x1.boxty = x2.boxty);
	
        -- исключение другого расчета из коробов склада
        -----------------------------------------------------------------------------
        update t_all_rasp_box x1
        set kol =   
                (select x1.kol - x2.verme from (
                    select art, boxty, sum(verme) verme from (
                            select art, boxty, verme from tdv_map_new_result
                            union all
                            select art, boxty, verme from tdv_map_new_resrv
                    ) group by art, boxty
                ) x2
                where x2.art = x1.art and x2.boxty = x1.boxty)
        where exists 
                (select x2.verme from (
                    select art, boxty, sum(verme) verme from (
                            select art, boxty, verme from tdv_map_new_result
                            union all
                            select art, boxty, verme from tdv_map_new_resrv
                    ) group by art, boxty
                ) x2
                where x2.art = x1.art and x2.boxty = x1.boxty);
        -----------------------------------------------------------------------------
		delete from t_all_rasp_box x
		where x.kol <= 0;
	
		insert into t_all_rasp_box_work
			select a.*
			from t_all_rasp_box a;
	
		commit;
	
		-- россыпь к распределению
		map_fl.fillWarehouseStock();
        
        -- исключение другого расчета из россыпи склада
        -----------------------------------------------------------------------------
        update tem_mapif_fill_warehouse_stock x1
        set kol =   
                (select x1.kol - x2.verme from (
                    select art, boxty, sum(verme) verme from tdv_map_new_result
                    group by art, boxty
                ) x2
                where x2.art = x1.art and x2.boxty = to_char(x1.asize))
        where exists 
                (select x2.verme from (
                    select art, boxty, sum(verme) verme from tdv_map_new_result
                    group by art, boxty
                ) x2
                where x2.art = x1.art and x2.boxty = to_char(x1.asize));
        
        delete from tem_mapif_fill_warehouse_stock x
		where x.kol <= 0;
		commit;
        -----------------------------------------------------------------------------
        
		insert into T_MAP_NEW_OTBOR
			select '9999', a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0
			from tem_mapif_fill_warehouse_stock a
			inner join s_art b on a.art = b.art
			left join TDV_MAP_NEW_ANAL c on a.art = c.art
			where pv_season like ',' || b.season || ','
						and b.art not like '%/О%'
			group by a.art, b.season, a.asize, nvl(c.analog, b.art);
	
		commit;
	
		-- отдельно закидываю транзиты, что бы потом каждый раз не пересчитывать {
		insert into TDV_NEW_SHOP_OST
			select a1.shopid, nvl(a2.analog, a1.art) art, to_number(asize), sum(a1.kol) kol, 'TRANS'
			from trans_in_way a1
			left join TDV_MAP_NEW_ANAL a2 on a1.art = a2.art
			inner join s_art c on a1.art = c.art
			where scan != ' '
						and a1.shopid in (select shopid
															from tdm_map_new_shop_group
															where s_group = i_s_group)
						and pv_season like '%,' || c.season || ',%'
			group by a1.shopid, nvl(a2.analog, a1.art), to_number(asize)
			having sum(kol) > 0
			
			-- добавляю резервы из САПА
			union
			select y.shopid, nvl(a2.analog, x3.art), to_number(asize, '999.99'), sum(x3.verme) kol, 'TRANS'
			from tdv_map_new_sap_reserv x3
			left join s_shop y on x3.kunnr = y.shopnum
			left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
			inner join s_art c on x3.art = c.art
			where x3.boxty = ' '
						and pv_season like '%,' || c.season || ',%'
						and to_number(asize, '999.99') != 0 -- россыпь
						and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
                        
                        and ((kunnr not in ('X_NERASP', 'X_SD_USE') and kunnr = 'X_FND_INET') 
                                                    or rasp != 'X')
                                                    
						and verme > 0
						and shopid is not null
						and trans != 'X'
                        
			group by y.shopid, nvl(a2.analog, x3.art), to_number(asize, '999.99')
			
			--
			union
			select y.shopid, nvl(a2.analog, a.art), to_number(b.asize), sum(b.kol) kol, 'TRANS'
			from tdv_map_new_sap_reserv a
			inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
			inner join s_shop y on a.kunnr = y.shopnum
			left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
			inner join s_art c on a.art = c.art
			where a.boxty != ' ' --короба
						and verme > 0
						and pv_season like '%,' || c.season || ',%'
						and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
                        
                        and (kunnr not in ('X_NERASP', 'X_SD_USE')
                                                or rasp != 'X')
                        
						and trans != 'X'
			group by y.shopid, nvl(a2.analog, a.art), to_number(b.asize)
			
			union
			select x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, to_number(x3.asize, '999.99')) asize,
											sum(decode(x3.boxty, ' ', x3.verme, f.kol)) kol, 'TRANS'
			from tdv_map_new_sap_reserv x3
			left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
			left join (select boxty, sum(kol) kol
								 from T_SAP_BOX_ROST
								 group by boxty) e on x3.boxty = e.boxty
			left join s_shop x on x3.kunnr = x.shopnum
			left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
			inner join s_art c on x3.art = c.art
			where --kunnr not in ('X_NERASP', 'X_SD_USE')
            
                    (kunnr not in ('X_NERASP', 'X_SD_USE')
                                                or rasp != 'X')
                                                
						and trans = 'X'
						and pv_season like '%,' || c.season || ',%'
						and verme > 0
			group by nvl(a2.analog, x3.art), nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid;
	
		commit;
	
		insert into t_map_new_otbor_before
			(id, id_shop, art, season, kol, asize, analog, status_flag)
			select i_id, id_shop, art, season, kol, asize, analog, status_flag
			from t_map_new_otbor;
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 16.05.2019
	-- Comments:отличается от sum_all_rasp тем, что заполняет таблицы T_MAP_NEW_OTBOR и t_all_rasp_box
	-- бесконечным запасом артикулов
	--****************************************************************************************************************************************************
	procedure sum_all_rasp2(i_id      in integer default null,
													i_s_group in number) is
	begin
	
		execute immediate 'truncate table T_MAP_NEW_OTBOR';
		execute immediate 'truncate table t_all_rasp_box';
		execute immediate 'truncate table t_all_rasp_box_work';
		execute immediate 'truncate table TDV_NEW_SHOP_OST'; -- таблица с транзитами и данными с заказами , лежащими на СГП
		execute immediate 'truncate table tdv_map_stock2'; -- артикула на стоке
	
		-- записываю короба к распределению
		insert into t_all_rasp_box
			select a.art, 'B1', 1000000, 0, b.season, nvl(g.analog, a.art) analog
			from (select distinct art
						 from e_osttek_online x
						 where x.kol > 0
									 and art not like '%/О') a
			left join s_art b on a.art = b.art
			left join TDV_MAP_NEW_ANAL g on a.art = g.art
			where b.art not like '%О'
			union all
			select a.art, 'A5', 1000000, 0, b.season, nvl(g.analog, a.art) analog
			from (select distinct art
						 from e_osttek_online x
						 where x.kol > 0
									 and art not like '%/О') a
			left join s_art b on a.art = b.art
			left join TDV_MAP_NEW_ANAL g on a.art = g.art
			where b.art not like '%О';
		commit;
	
		-- россыпь к распределению
		insert into T_MAP_NEW_OTBOR
			select '9999', a.art, b.season, 1000000, c.asize, nvl(g.analog, a.art) analog, 0
			from (select distinct art
						 from e_osttek_online x
						 where x.kol > 0
									 and art not like '%/О') a
			left join s_art b on a.art = b.art
			left join s_size_scale c on a.art = c.art
			left join TDV_MAP_NEW_ANAL g on a.art = g.art
			where c.asize is not null
						and b.art not like '%О';
		commit;
	
		-- отдельно закидываю транзиты, что бы потом каждый раз не пересчитывать {
		insert into TDV_NEW_SHOP_OST
			select a1.shopid, nvl(a2.analog, a1.art) art, to_number(asize), sum(a1.kol) kol, 'TRANS'
			from trans_in_way a1
			left join TDV_MAP_NEW_ANAL a2 on a1.art = a2.art
			inner join s_art c on a1.art = c.art
			where scan != ' '
						and a1.shopid in (select shopid
															from tdm_map_new_shop_group
															where s_group = i_s_group)
						and pv_season like '%,' || c.season || ',%'
			group by a1.shopid, nvl(a2.analog, a1.art), to_number(asize)
			having sum(kol) > 0
			
			-- добавляю резервы из САПА
			union
			select y.shopid, nvl(a2.analog, x3.art), to_number(asize, '999.99'), sum(x3.verme) kol, 'TRANS'
			from tdv_map_new_sap_reserv x3
			left join s_shop y on x3.kunnr = y.shopnum
			left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
			inner join s_art c on x3.art = c.art
			where x3.boxty = ' '
						and pv_season like '%,' || c.season || ',%'
						and to_number(asize, '999.99') != 0 -- россыпь
						and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
						and verme > 0
						and shopid is not null
						and trans != 'X'
			group by y.shopid, nvl(a2.analog, x3.art), to_number(asize, '999.99')
			
			--
			union
			select y.shopid, nvl(a2.analog, a.art), to_number(b.asize), sum(b.kol) kol, 'TRANS'
			from tdv_map_new_sap_reserv a
			inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
			inner join s_shop y on a.kunnr = y.shopnum
			left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
			inner join s_art c on a.art = c.art
			where a.boxty != ' ' --короба
						and verme > 0
						and pv_season like '%,' || c.season || ',%'
						and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
						and trans != 'X'
			group by y.shopid, nvl(a2.analog, a.art), to_number(b.asize)
			
			union
			select distinct x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, to_number(x3.asize, '999.99')) asize, sum(e.kol) kol, 'TRANS'
			from tdv_map_new_sap_reserv x3
			left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
			left join (select boxty, sum(kol) kol
								 from T_SAP_BOX_ROST
								 group by boxty) e on x3.boxty = e.boxty
			left join s_shop x on x3.kunnr = x.shopnum
			left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
			inner join s_art c on x3.art = c.art
			where kunnr not in ('X_NERASP', 'X_SD_USE')
						and trans = 'X'
						and pv_season like '%,' || c.season || ',%'
			group by nvl(a2.analog, x3.art), nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid;
	
		commit;
	
		insert into t_map_new_otbor_before
			(id, id_shop, art, season, kol, asize, analog, status_flag)
			select i_id, id_shop, art, season, kol, asize, analog, status_flag
			from t_map_new_otbor;
		commit;
	
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 16.05.2019
	-- Comments: разброс размеров артикулов 
	--****************************************************************************************************************************************************
	procedure ross_count_details(i_id             number,
															 i_block_no       number,
															 i_only_cent_size varchar2,
															 i_id_shop        varchar2,
															 i_landid         varchar2,
															 i_art            varchar2,
															 i_season         varchar2,
															 i_current_limit  number,
															 i_style          varchar2,
															 i_groupmw        varchar2) is
	
		v_correct_data_flag varchar(1) := 'T';
	
		v_shop_current_limit number := 0;
		v_kol_add number := 0;
	
        v_all_size_count number := 0;
        v_cent_size_count number := 0;
        
		r_otbor T_MAP_NEW_OTBOR%rowtype;
	begin
	
		v_shop_current_limit := i_current_limit;
		v_kol_add := 0;
	
		SAVEPOINT limit_save;
	
		-- беру размеры которые могу докинуть*/
		for r_size in (select distinct a.asize
									 from T_MAP_NEW_OTBOR a
									 left join s_art e on a.art = e.art
									 left join (select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol
														 from TDV_MAP_NEW_OST_SALE
														 where id_shop = i_id_shop
																	 and art = i_art
														 group by asize) b on a.asize = b.asize
									 left join (select asize, sum(kol) kol
														 from t_map_new_poexalo x
														 where id_shop = i_id_shop
																	 and x.text1 = i_art
																	 and id = i_id
														 group by asize) c on a.asize = c.asize
									 left join (select x.asize, sum(x.verme) kol
														 from TDV_NEW_SHOP_OST x
														 where x.verme > 0
																	 and x.art = i_art
																	 and id_shop = i_id_shop
														 group by x.asize) d on a.asize = d.asize
									 
									 where a.analog = i_art
												 and a.kol > 0
												 and nvl(b.kol_sale, 0) >= decode(i_only_cent_size, 'T', 2, 0)
												 and case
													 when i_only_cent_size = 'F' and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) = 0 then
														1
													 when i_only_cent_size = 'T' and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) <= 1 then
														1
													 else
														0
												 end = 1
									 order by a.asize)
		loop
			pv_iter_count := pv_iter_count + 1;
		
			v_correct_data_flag := 'T';
		
			begin
				select a.*
				into r_otbor
				from (select a.*
							 from T_MAP_NEW_OTBOR a
							 left join (select distinct x.art
												 from tdv_map_osttek x
												 where x.id_shop = i_id_shop
															 and art in (select art
																					 from TDV_MAP_NEW_ANAL
																					 where analog = i_art)
															 and x.kol > 0) b on a.art = b.art
							 where analog = i_art
										 and asize = r_size.asize
										 and kol > 0
										 and not (a.art like '%Р2%' and i_landid = 'RU') -- На РФ не везем артикула с Р2 
							 -- какие в первую очередь берем артикула:  
							 -- 1. артикул , который уже есть в магазине
							 -- 2. артикул , которого МЕНЬШЕ на складе  
							 order by case
													when b.art is not null then
													 0
													else
													 1
												end, a.kol) a
				where rownum = 1;
			exception
				when no_data_found then
					v_correct_data_flag := 'F';
			end;
		
			-- если разрешен только центральный размер
			if i_only_cent_size = 'T' then
				if r_size.asize in (4, 4.5, 5, 5.5) and i_groupmw = 'Женские' and i_style != 'Sport' then
					null;
				elsif r_size.asize in (8, 8.5, 9) and i_groupmw = 'Мужские' and i_style != 'Sport' then
					null;
				elsif r_size.asize in (5, 5.5, 6, 6.5) and i_groupmw = 'Женские' and i_style = 'Sport' then
					null;
				elsif r_size.asize in (8.5, 9, 9.5, 10) and i_groupmw = 'Мужские' and i_style = 'Sport' then
					null;
				elsif r_size.asize in (37, 38, 39) and i_groupmw = 'Женские' then
					null;
				elsif r_size.asize in (42, 43) and i_groupmw = 'Мужские' then
					null;
				else
					v_correct_data_flag := 'F';
				end if;
			end if;
		
			if v_correct_data_flag = 'T' then
				insert into t_map_new_poexalo
					select i_id_shop, r_otbor.art, i_season, 1 kol, r_otbor.asize, 0 flag, r_otbor.id_shop, i_id, systimestamp, null boxty, i_block_no block_no,
								 null owner, i_art text1, 0 text2, 0 text3 --i_kol_sale text3
					from dual;
			
				v_kol_add := v_kol_add + 1;
			
				update T_MAP_NEW_OTBOR a
				set kol = kol - 1
				where a.art = r_otbor.art
							and a.id_shop = r_otbor.id_shop
							and a.asize = r_otbor.asize;
			end if;
		
		end loop;
	
        -- не менее 1 размера центрального, при этом 2 размера средних для женской и 1 средний для мужской
        select count(asize),
        nvl(sum(
            case when asize in (4, 4.5, 5, 5.5)    and i_groupmw = 'Женские' and i_style != 'Sport' then 1
                 when asize in (8, 8.5, 9)         and i_groupmw = 'Мужские' and i_style != 'Sport' then 1
                 when asize in (5, 5.5, 6, 6.5)    and i_groupmw = 'Женские' and i_style = 'Sport' then 1
                 when asize in (8.5, 9, 9.5, 10)   and i_groupmw = 'Мужские' and i_style = 'Sport' then 1
                 when asize in (37, 38, 39)        and i_groupmw = 'Женские' then 1
                 when asize in (42, 43)            and i_groupmw = 'Мужские' then 1
                 else 0 end), 0) 
        into v_all_size_count, v_cent_size_count
        from (
            select distinct asize from tdv_map_new_ost_sale where id_shop = i_id_shop and art = i_art and kol_ost > 0
            union all
            select distinct asize from tdv_new_shop_ost where id_shop = i_id_shop and art = i_art and verme > 0
            union all
            select distinct asize from t_map_new_poexalo where id = i_id and id_shop = i_id_shop and /*art*/ text1 = i_art and kol > 0
        );
        
        
		if v_shop_current_limit - v_kol_add >= 0 
            and v_cent_size_count >= 1
            and ((i_groupmw = 'Мужские' and v_all_size_count >= 2) or (i_groupmw = 'Женские' and v_all_size_count >= 3)) 
--            and v_all_size_count >= 3
--            and ((i_groupmw = 'Мужские' and v_cent_size_count >= 2) or (i_groupmw = 'Женские' and v_cent_size_count >= 3)) 
        then
		
			-- вычитаем от лимита добавленное
			v_shop_current_limit := v_shop_current_limit - v_kol_add;
		
			-- обновляем текущий лимит магазина
			update t_map_current_shop_limit
			set current_limit = v_shop_current_limit
			where id_shop = i_id_shop;
		
		else
			rollback to limit_save;
		end if;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 20.11.2018 23:03:22
	-- Comments: распределяем короба на магазины , которые продали i_sale_pair_count и больше пар
	-- по i_color, и остатков до i_max_kol_ost включительно 
	--
	-- box_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 1);
	--****************************************************************************************************************************************************
	procedure box_count(i_id       integer,
											i_s_group  integer,
											i_block_no number,
											i_color    varchar2 default null) is
		v_count number;
		v_map_fl_result varchar2(1);
	
		v_shop_current_limit number := 0;
		v_kol_in_box number := 0;
	begin
	
		v_map_fl_result := map_fl.blockStart(i_id, i_block_no, i_block_no);
		if v_map_fl_result = 'F' then
			return;
		end if;
	
		commit;
	
		dbms_output.put_line('box_count');
		-- беру магазины и артикула в которые нужно добросить обувь -- по заданным условиям
		for r_row in (select a1.*
									from ((select x.*
													from (select a.id_shop, g.landid, nvl(b.reit2, 0) reit2, a.art, season, x.shop_in, sum(kol_sale) kol_sale1,
																				sum(kol_ost2) kol_ost2, sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 proc_real
																 from TDV_MAP_NEW_OST_SALE a
																 left join s_shop g on a.id_shop = g.shopid
																 inner join tdm_map_new_shop_group x on x.s_group = i_s_group
																																				and a.id_shop = x.shopid
																 left join tdv_map_shop_reit b on a.id_shop = b.id_shop
																 where kol_sale + kol_ost2 != 0
																 group by a.id_shop, g.landid, a.art, season, x.shop_in, nvl(b.reit2, 0)
																 having(sum(kol_sale) + sum(kol_ost) != 0)) x
													
													inner join MAP_DAY_SORT_COLOR_PROC r on x.kol_sale1 <= r.sale_to
																														 and x.kol_sale1 >= r.sale_ot
																														 and x.kol_ost2 <= r.proc_to
																														 and x.kol_ost2 >= r.proc_ot
																														 and r.color = case
																															 when i_color is null then
																																r.color
																															 else
																																i_color
																														 end
																														 and r.id = pv_color_id
													order by kol_sale1 desc, reit2)) a1
									inner join (select distinct analog
														 from t_all_rasp_box_work) a2 on a1.art = a2.analog)
		loop
		
			pv_iter_count := pv_iter_count + 1;
		
			select current_limit
			into v_shop_current_limit
			from t_map_current_shop_limit
			where id_shop = r_row.id_shop;
			--v_shop_current_limit := r_row.current_limit;
		
			if v_shop_current_limit > 0 then
				dbms_output.put_line(1);
				--  пытаюсь докинуть короб
				for r_boxty in (select art, a.boxty, sum(f.kol) kol
												from t_all_rasp_box_work a
												left join T_SAP_BOX_ROST f on a.boxty = f.boxty
												left join (select distinct asize
																	from TDV_MAP_NEW_OST_SALE
																	where id_shop = r_row.id_shop
																				and art = r_row.art
																				and kol_ost != 0) b on f.asize = b.asize
												left join (select x.asize -- проверка на остатки в пути
																	from TDV_NEW_SHOP_OST x
																	where x.verme > 0
																				and x.id_shop = r_row.id_shop
																				and x.art = r_row.art) c on f.asize = c.asize
												left join (select distinct asize
																	from t_map_new_poexalo x
																	where id_shop = r_row.id_shop
																				and x.text1 = r_row.art
																				and id = i_id) d on f.asize = d.asize
												where analog = r_row.art
															and a.kol > 0
															and not (a.art like '%Р2%' and r_row.landid = 'RU')
												group by art, a.boxty
												having sum(case
													when b.asize is null and c.asize is null and d.asize is null and f.kol = 1 then
													 0
													else
													 1
												end) = 0
												order by sum(a.kol) desc)
				loop
				
					-- запоминаем сколько пар в коробе, который добавляем магазину
					v_kol_in_box := coalesce(r_boxty.kol, 0);
				
					if v_shop_current_limit - v_kol_in_box >= 0 then
					
						insert into t_map_new_poexalo
							select r_row.id_shop, r_boxty.art, r_row.season, a.kol, a.asize, 0 flag, '9999', i_id,
										 --1 flag
										 systimestamp, r_boxty.boxty, i_block_no block_no, null owner, r_row.art text1, r_row.kol_ost2 text2, r_row.kol_sale1 text3
							from T_SAP_BOX_ROST a
							where a.boxty = r_boxty.boxty;
					
						update t_all_rasp_box_work a
						set kol = kol - 1
						where art = r_boxty.art
									and boxty = r_boxty.boxty;
					
						-- вычитаем от лимита короб
						v_shop_current_limit := v_shop_current_limit - v_kol_in_box;
					end if;
					exit;
				end loop;
			
				-- обновляем текущий лимит магазина
				update t_map_current_shop_limit
				set current_limit = v_shop_current_limit
				where id_shop = r_row.id_shop;
			
			end if;
		end loop;
	
		select nvl(sum(kol), 0)
		into v_count
		from t_map_new_poexalo
		where id = i_id;
	
		map_fl.blockEnd(i_id, 'Поехало ' || v_count);
	
		update TDV_MAP_13_RESULT
		set SUM_KOL = v_count
		where id = i_id;
	
		commit;
	EXCEPTION
		WHEN OTHERS THEN
			dbms_output.put_line(sqlcode || sqlerrm);
			WRITE_ERROR_PROC('BEP_MAP_002.box_count', sqlcode, sqlerrm,
											 'i_id: ' || i_id || ', i_s_group: ' || i_s_group || ', i_block_no: ' || i_block_no || ', i_color:' || i_color,
											 DBMS_UTILITY.format_error_backtrace);
			raise_application_error(sqlcode, sqlerrm);
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 20.03.2019 14:00:00
	-- Comments: распределяем россыпь на магазины , которые продали i_sale_pair_count и больше пар
	-- ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 2, i_color => 'blue');
	--****************************************************************************************************************************************************
	procedure ross_count_color(i_id             integer,
														 i_s_group        integer,
														 i_block_no       number,
														 i_only_cent_size varchar2 default 'F', -- распределять только центральные размеры
														 i_color          varchar2 default 'F') is
		v_count number;
		v_map_fl_result varchar2(1);
	
	begin
	
		v_map_fl_result := map_fl.blockStart(i_id, i_block_no, i_block_no);
		if v_map_fl_result = 'F' then
			return;
		end if;
	
		commit;
	
		-- беру магазины и артикула в которые нужно добросить обувь -- по заданным условиям
		for r_row in (select a1.*
									from (select x.*
												 from (select a.id_shop, g.landid, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale1, sum(kol_ost2) kol_ost2,
																			 sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, z.current_limit, max(d.style) style,
																			 max(d.groupmw) groupmw
																from TDV_MAP_NEW_OST_SALE a
																left join s_shop g on a.id_shop = g.shopid
																inner join (select distinct analog
																					 from T_MAP_NEW_OTBOR where kol >0) c on a.art = c.analog -- проходимся только по тем артикулам, которые есть на остатках распределения
																inner join tdm_map_new_shop_group x on x.s_group = i_s_group
																																			 and a.id_shop = x.shopid
																inner join t_map_current_shop_limit z on a.id_shop = z.id_shop
																left join tdv_map_shop_reit b on a.id_shop = b.id_shop
																left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
																					from s_art
																					group by decode(normt, ' ', art, normt)) d on d.normt = a.art
																where kol_sale + kol_ost2 != 0
																			and current_limit > 0 --сразу откидываем магазины с нулевым лимитом
																group by a.id_shop, g.landid, a.art, a.season, nvl(b.reit2, 0), z.current_limit
																having(sum(kol_sale) + sum(kol_ost) != 0)) x
												 
												 inner join MAP_DAY_SORT_COLOR_PROC r on x.kol_sale1 <= r.sale_to
																														and x.kol_sale1 >= r.sale_ot
																														and x.kol_ost2 <= r.proc_to
																														and x.kol_ost2 >= r.proc_ot
																														and r.color = case
																															when i_color is null then
																															 r.color
																															else
																															 i_color
																														end
																														and r.id = pv_color_id
												 order by kol_sale1 desc, reit2) a1)
		loop
			ross_count_details(i_id => i_id, i_block_no => i_block_no, i_only_cent_size => i_only_cent_size, i_id_shop => r_row.id_shop,
												 i_landid => r_row.landid, i_art => r_row.art, i_season => r_row.season, i_current_limit => r_row.current_limit,
												 i_groupmw => r_row.groupmw, i_style => r_row.style);
		
		end loop;
	
		select sum(kol)
		into v_count
		from t_map_new_poexalo
		where id = i_id;
	
		map_fl.blockEnd(i_id, 'Поехало ' || v_count);
	
		update TDV_MAP_13_RESULT
		set SUM_KOL = v_count
		where id = i_id;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 20.11.2018 23:03:22
	-- Comments: распределяем россыпь на магазины , которые продали i_sale_pair_count и больше пар
	--
	--****************************************************************************************************************************************************
	procedure ross_count(i_id                   integer,
											 i_s_group              integer,
											 i_sale_pair_count      integer,
											 i_block_no             number,
											 i_only_cent_size       varchar2 default 'F', -- распределять только центральные размеры
											 i_max_pair_count       number default 0, --сколько максимум пар может быть данного размера, чтобы мы могли докинуть еще одну пару
											 i_sale_size_pair_count number default 0, -- сколько минимум пар определенного размера должно быть продано
											 i_max_kol_ost          number default 1000,
											 i_to_shop_list         varchar2 default 'F') is
		v_count number;
		v_map_fl_result varchar2(1);
	
	begin
	
		v_map_fl_result := map_fl.blockStart(i_id, i_block_no, i_block_no);
		if v_map_fl_result = 'F' then
			return;
		end if;
	
		commit;
	
		-- беру магазины и артикула в которые нужно добросить обувь -- по заданным условиям
		for r_row in (select x.*
									from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
																sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, y.current_limit, c.landid, max(d.groupmw) groupmw,
																max(d.style) style
												 from tdv_map_new_ost_sale a
												 inner join (select distinct analog
																		from T_MAP_NEW_OTBOR
																		where kol > 0) e on a.art = e.analog -- проходимся только по тем артикулам, которые есть на остатках распределения
												 inner join tdm_map_new_shop_group x on x.s_group = i_s_group
																																and a.id_shop = x.shopid
												 left join t_map_adding_shop_list z on a.id_shop = z.shopid
												 inner join t_map_current_shop_limit y on a.id_shop = y.id_shop
												 left join tdv_map_shop_reit b on a.id_shop = b.id_shop
												 left join s_shop c on a.id_shop = c.shopid
												 left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
																	 from s_art
																	 group by decode(normt, ' ', art, normt)) d on d.normt = a.art
												 left join (select distinct analog
																	 from tdv_map_stock2) f on a.art = f.analog -- вторую пару на стоки не даем        
												 where kol_sale + kol_ost2 != 0
															 and decode(i_to_shop_list, 'T', z.shopid, a.id_shop) is not null
															 and case
																 when i_only_cent_size = 'T' and f.analog is not null then
																	1
																 else
																	0
															 end = 0
												 group by a.id_shop, a.art, a.season, nvl(b.reit2, 0), y.current_limit, c.landid
												 having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= i_sale_pair_count and sum(kol_ost2) <= i_max_kol_ost) x
									
									order by kol_sale desc, reit2)
		loop
		
			ross_count_details(i_id => i_id, i_block_no => i_block_no, i_only_cent_size => i_only_cent_size, i_id_shop => r_row.id_shop,
												 i_landid => r_row.landid, i_art => r_row.art, i_season => r_row.season, i_current_limit => r_row.current_limit,
												 i_groupmw => r_row.groupmw, i_style => r_row.style);
		
		end loop;
	
		select sum(kol)
		into v_count
		from t_map_new_poexalo
		where id = i_id;
	
		map_fl.blockEnd(i_id, 'Поехало ' || v_count);
	
		update TDV_MAP_13_RESULT
		set SUM_KOL = v_count
		where id = i_id;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 19.03.2019 11:20:00
	-- Comments: Выполнение блока летней обуви
	--
	--****************************************************************************************************************************************************
	procedure go_count_summer(i_id      in integer,
														i_s_group in integer) is
		v_time number;
	begin
	
		pv_season := ',Летняя,';
	
		v_time := dbms_utility.get_time;
		fillBaseData(i_s_group, trunc(to_date('01.01.2019', 'dd.mm.yyyy')), trunc(sysdate));
		log_time(i_id, -101, v_time);
	
		v_time := dbms_utility.get_time;
		sum_all_rasp(i_id, i_s_group);
		log_time(i_id, -100, v_time);
	
		-- 1) Распределение коробов желтый, остатки <=3  продажи >=3
		v_time := dbms_utility.get_time;
		dbms_output.put_line(5);
		box_count(i_id => i_id, i_s_group => i_s_group, i_block_no => 1, i_color => 'yellow');
		dbms_output.put_line(6);
		log_time(i_id, 1, v_time);
	
		--  2) Распределение россыпи согласно цветовой схеме не включая красные(только синие(+желтые) и зеленые)
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 2, i_color => 'yellow');
		log_time(i_id, 2, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 3, i_color => 'blue');
		log_time(i_id, 3, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 4, i_color => 'green');
		log_time(i_id, 4, v_time);
	
		--  3) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (по списку магазинов)
		v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 5, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2, i_to_shop_list => 'T');
		log_time(i_id, 5, v_time);
	
		--  4) Распределение россыпи согласно цветовой схеме по красным
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 6, i_color => 'red');
		log_time(i_id, 6, v_time);
	
		--  5) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
		v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 7, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2);
		log_time(i_id, 7, v_time);
	
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 21.03.2019 10:00:00
	-- Comments: Выполнение блока всесезонной обуви
	--
	--****************************************************************************************************************************************************
	procedure go_count_all_seasons(i_id      in integer,
																 i_s_group in integer) is
		v_time number;
		v_asize number;
	begin
		pv_season := ',Всесезонная,';
		----------------------------------------------------------------------------
		-- PART 1
		----------------------------------------------------------------------------
	
		v_time := dbms_utility.get_time;
		fillbasedata(i_s_group, add_months(trunc(sysdate), -2), trunc(sysdate));
		log_time(i_id, -201, v_time);
	
		v_time := dbms_utility.get_time;
		sum_all_rasp(i_id, i_s_group);
		log_time(i_id, -200, v_time);
	
		--dbms_output.put_line(v_asize);
	
		-- 1) Распределение коробов желтый, остатки <=3  продажи >=3
		v_time := dbms_utility.get_time;
		box_count(i_id => i_id, i_s_group => i_s_group, i_block_no => 8, i_color => 'yellow');
		log_time(i_id, 8, v_time);
	
		--  2) Распределение россыпи согласно цветовой схеме не включая красные(только синие(+желтые) и зеленые)
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 9, i_color => 'yellow');
		log_time(i_id, 9, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 10, i_color => 'blue');
		log_time(i_id, 10, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 11, i_color => 'green');
		log_time(i_id, 11, v_time);
	
		--  3) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
		v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 12, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2);
		log_time(i_id, 12, v_time);
	
		--  4) Распределение россыпи согласно цветовой схеме по красным
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 13, i_color => 'red');
		log_time(i_id, 13, v_time);
	
		----------------------------------------------------------------------------
		-- PART 2
		----------------------------------------------------------------------------
		v_time := dbms_utility.get_time;
		fillbasedata(i_s_group, add_months(trunc(sysdate), -6), trunc(sysdate));
		log_time(i_id, -401, v_time);
		--sum_all_rasp(i_s_group);
	
		-- 1) Распределение коробов желтый, остатки <=3  продажи >=3
		v_time := dbms_utility.get_time;
		box_count(i_id => i_id, i_s_group => i_s_group, i_block_no => 14, i_color => 'yellow');
		log_time(i_id, 14, v_time);
	
		--  2) Распределение россыпи согласно цветовой схеме не включая красные(только синие(+желтые) и зеленые)
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 15, i_color => 'yellow');
		log_time(i_id, 15, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 16, i_color => 'blue');
		log_time(i_id, 16, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 17, i_color => 'green');
		log_time(i_id, 17, v_time);
	
		--  3) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (по списку магазинов)
		v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 18, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2, i_to_shop_list => 'T');
		log_time(i_id, 18, v_time);
	
		--  4) Распределение россыпи согласно цветовой схеме по красным
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 19, i_color => 'red');
		log_time(i_id, 19, v_time);
	
		--  5) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
		v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 20, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2);
		log_time(i_id, 20, v_time);
	
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 21.03.2019 10:00:00
	-- Comments: Выполнение блока утепленной обуви
	--
	--****************************************************************************************************************************************************
	procedure go_count_warm(i_id      in integer,
													i_s_group in integer) is
		v_time number;
	begin
		pv_season := ',Утепленная,';
	
		v_time := dbms_utility.get_time;
		fillBaseData(i_s_group, add_months(trunc(sysdate), /*-6*/-12), trunc(sysdate));
		log_time(i_id, -301, v_time);
	
		v_time := dbms_utility.get_time;
		sum_all_rasp(i_id, i_s_group);
		log_time(i_id, -300, v_time);
	
		-- 1) Распределение коробов желтый, остатки <=3  продажи >=3
		v_time := dbms_utility.get_time;
		box_count(i_id => i_id, i_s_group => i_s_group, i_block_no => 21, i_color => 'yellow');
		log_time(i_id, 21, v_time);
	
		--  2) Распределение россыпи согласно цветовой схеме не включая красные(только синие(+желтые) и зеленые)
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 22, i_color => 'yellow');
		log_time(i_id, 22, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 23, i_color => 'blue');
		log_time(i_id, 23, v_time);
	
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 24, i_color => 'green');
		log_time(i_id, 24, v_time);
        
        -- 2.5)Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (по списку магазинов)
        v_time := dbms_utility.get_time;
        ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 33, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2, i_to_shop_list => 'T');
        log_time(i_id, 33, v_time);
    
		--  3) Распределение россыпи согласно цветовой схеме по красным
		v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 25, i_color => 'red');
		log_time(i_id, 25, v_time);
        
        --  3.5) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
		v_time := dbms_utility.get_time;
        ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 34, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2);
        log_time(i_id, 34, v_time);
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 21.03.2019 10:00:00
	-- Comments: Выполнение блока зимней обуви
	--
	--****************************************************************************************************************************************************
	procedure go_count_winter(i_id      in integer,
														i_s_group in integer) is
        v_time number;
    begin
		pv_season := ',Зимняя,';
	
        v_time := dbms_utility.get_time;
		fillbasedata(i_s_group, add_months(trunc(sysdate), /*-6*/-12), trunc(sysdate));
		log_time(i_id, -501, v_time);
    
		v_time := dbms_utility.get_time;
		sum_all_rasp(i_id, i_s_group);
		log_time(i_id, -500, v_time);
	
		-- 1) Распределение коробов желтый, остатки <=3  продажи >=3
        v_time := dbms_utility.get_time;
		box_count(i_id => i_id, i_s_group => i_s_group, i_block_no => 26, i_color => 'yellow');
        log_time(i_id, 26, v_time);
        
		--  2) Распределение россыпи согласно цветовой схеме не включая красные(только синие(+желтые) и зеленые)
        v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 27, i_color => 'yellow');
        log_time(i_id, 27, v_time);
        
        v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 28, i_color => 'blue');
        log_time(i_id, 28, v_time);
        
        v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 29, i_color => 'green');
        log_time(i_id, 29, v_time);
        
		--  3) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (по списку магазинов)
        v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 30, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2, i_to_shop_list => 'T');
        log_time(i_id, 30, v_time);
        
		--  4) Распределение россыпи согласно цветовой схеме по красным
        v_time := dbms_utility.get_time;
		ross_count_color(i_id => i_id, i_s_group => i_s_group, i_block_no => 31, i_color => 'red');
        log_time(i_id, 31, v_time);
        
		--  5) Распределить 2ую пару только на средний размер, артикул продажи >=5, продажи размера >=2 остаток размера <=1 (все магазины)
        v_time := dbms_utility.get_time;
		ross_count(i_id => i_id, i_s_group => i_s_group, i_sale_pair_count => 5, i_block_no => 32, i_only_cent_size => 'T', i_max_pair_count => 1,
							 i_sale_size_pair_count => 2);
        log_time(i_id, 32, v_time);
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 25.03.2019 10:00:00
	-- Comments: Создание бэкапов распределяемого, остатков магазинов до и после текущего расчета
	--
	--****************************************************************************************************************************************************
	procedure save_sklad_ost(i_id in number) is
	begin
		insert into t_map_new_otbor_after
			(id, id_shop, art, season, kol, asize, analog, status_flag)
			select i_id, id_shop, art, season, kol, asize, analog, status_flag
			from t_map_new_otbor;
	
		insert into t_all_rasp_box_after
			(id, art, boxty, kol, flag, season, analog)
			select i_id, art, boxty, kol, flag, season, analog
			from t_all_rasp_box_work;
	
		insert into t_all_rasp_box_before
			(id, art, boxty, kol, flag, season, analog)
			select i_id, art, boxty, kol, flag, season, analog
			from t_all_rasp_box;
	
		insert into tdv_new_shop_ost_after
			(id, id_shop, art, asize, verme, location)
			select i_id, id_shop, art, asize, verme, location
			from tdv_new_shop_ost;
	
		insert into tdv_map_new_ost_sale_after
			(id, id_shop, art, asize, season, kol_ost, kol_sale, out_block, kol_rc, kol_ost2)
			select i_id, id_shop, art, asize, SEASON, kol_ost, kol_sale, out_block, kol_rc, kol_ost2
			from tdv_map_new_ost_sale;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Bobrovich EP
	-- Created: 25.03.2019 10:00:00
	-- Comments: Сохранение отчета по данным сразу после текущего расчета
	--
	--****************************************************************************************************************************************************  
	procedure save_result_report(i_id in number) is
	begin
		insert into t_map_day_sort_report
			select i_id, a.id_shop, a.season, a.art, a.text1, a.asize,
						 nvl((select sum(kol) kol_ost
									from e_osttek_online
									where id_shop = a.id_shop
												and art = a.art
												and asize = a.asize
									group by id_shop, art, asize
									having sum(kol) > 0), 0), a.text2, a.text3, a.kol, a.block_no,
						 case
							 when a.block_no = 1 then
								'короб'
							 when a.block_no = 2 then
								'желтый'
							 when a.block_no = 3 then
								'синий'
							 when a.block_no = 4 then
								'зеленый'
							 when a.block_no = 5 then
								'2 пара'
							 when a.block_no = 6 then
								'крассный'
							 when a.block_no = 7 then
								'2 пара'
						 
							 when a.block_no = 8 then
								'короб'
							 when a.block_no = 9 then
								'желтый'
							 when a.block_no = 10 then
								'синий'
							 when a.block_no = 11 then
								'зеленый'
							 when a.block_no = 12 then
								'2 пара'
							 when a.block_no = 13 then
								'крассный'
						 
							 when a.block_no = 14 then
								'короб'
							 when a.block_no = 15 then
								'желтый'
							 when a.block_no = 16 then
								'синий'
							 when a.block_no = 17 then
								'зеленый'
							 when a.block_no = 18 then
								'2 пара'
							 when a.block_no = 19 then
								'крассный'
							 when a.block_no = 20 then
								'2 пара'
						 
							 when a.block_no = 21 then
								'короб'
							 when a.block_no = 22 then
								'желтый'
							 when a.block_no = 23 then
								'синий'
							 when a.block_no = 24 then
								'зеленый'
                             when a.block_no = 33 then
								'2 пара'   
							 when a.block_no = 25 then
								'крассный'
                             when a.block_no = 34 then
								'2 пара'   
                                
                             when a.block_no = 26 then
								'короб'
							 when a.block_no = 27 then
								'желтый'
							 when a.block_no = 28 then
								'синий'
							 when a.block_no = 29 then
								'зеленый'
							 when a.block_no = 30 then
								'2 пара'
                             when a.block_no = 31 then
								'крассный'
                             when a.block_no = 32 then
								'2 пара'
						 end
			from t_map_new_poexalo a
			where a.id = i_id
						and a.kol > 0
			order by block_no, id_shop, art, asize;
	
		commit;
	end;

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 01.01.2018 21:59:18
	-- Start Procedure:
	--
	--****************************************************************************************************************************************************
	procedure go(i_s_group         number default null,
                 i_is_test varchar2 default 'F',
                 i_no_data_refresh varchar2 default 'F' --если НЕ нужно  пересчитывать текущие остатки и продажи то 'T'
							 ) is
		v_count integer;
		v_s_group integer;
	
		v_id integer;
		v_time timestamp;
	
		v_list_city varchar2(1000 char);
		v_list_filial varchar2(1000 char);
		v_cluster_name varchar2(1000 char);
		v_package_name varchar2(1000 char);
	
	begin
	
		execute immediate 'truncate table t_map_current_shop_limit'; -- таблица с текущими значениями лимита на распределение в магазин
		execute immediate 'truncate table tdv_map_stock2'; -- таблица стоков
	
		if i_s_group is not null then
			v_s_group := i_s_group;
		end if;
	
		-- номер нового расчета
		v_id := map_fl.getNewCalculationID();
	
		dbms_output.put_line('номер расчета: ' || v_id);
	
		v_time := systimestamp;
        
        ------------------------------------------------------------------------
        --ИСКЛЮЧЕНИЕ ИЗ РАСЧЕТА ПЕРЕЧНЯ ФИЛИАЛОВ '36F1' , '38F1', '39F1', '40F1'
        ------------------------------------------------------------------------
        delete from tdm_map_new_shop_group 
        where s_group = i_s_group 
        and shopid in (select shopid from st_retail_hierarchy 
                        where shopparent in ('36F1' , '38F1', '39F1', '40F1'));
        commit;
        ------------------------------------------------------------------------
	
		-- список городов
		v_list_city := map_fl.get_city_list(i_s_group, 1000);
		dbms_output.put_line('v_list_city: ' || v_list_city);
	
		-- список филиалов
		v_list_filial := map_fl.get_filial_list(i_s_group, 1000);
		dbms_output.put_line('v_list_filial: ' || v_list_filial);
	
		-- имя кластера
		v_cluster_name := map_fl.get_cluster_name(i_s_group);
		dbms_output.put_line('v_cluster_name: ' || v_cluster_name);
	
		-- имя пакета
		v_package_name := map_fl.get_package_name(dbms_utility.format_call_stack);
		dbms_output.put_line('v_package_name: ' || v_package_name);
	
		insert into TDV_MAP_13_RESULT
		values
			(v_id, v_time, null, v_s_group, null, null, null, 'F', v_list_city, v_list_filial, 1, v_time, null, 0, 'in progress', v_cluster_name,
			 USERENV('sessionid'), 1, v_package_name);
		commit;
		-- }
	
		update tdm_map_new_shop_group a
		set a.shop_in = ',' || replace(rtrim(ltrim(replace(a.shop_in, ' ', ''), ','), ','), '70', '00') || ','
		where a.s_group = v_s_group
					and a.shop_in is not null;
	
		--    if i_no_data_refresh = 'F' then
		--      fillBaseData(i_s_group, pv_sale_start_date, trunc(sysdate));
		--      sum_all_rasp(v_s_group);
		--    end if;
	
		commit;
	
		-- инициализация исходных лимитов
		insert into t_map_current_shop_limit
			select shopid, count_limit_in
			from tdm_map_new_shop_group
			where s_group = v_s_group;
		commit;
	
		insert into tdv_map_stock2
			select distinct a.art, nvl(c.analog, a.art)
			from cena_skidki_all_shops a
			left join TDV_MAP_NEW_ANAL c on a.art = c.art
			where substr(to_char(cena), -2) = '99';
		commit;
	
    -- 32-ой магазин не должен участвовать в расчетах
    delete from tdm_map_new_shop_group 
    where s_group = v_s_group and shopid = '0032';
  
    commit;
  
		go_count_summer(v_id, v_s_group);
		save_sklad_ost(v_id);
    map_fl.test(v_id, v_package_name, 10);
    map_fl.test(v_id, v_package_name, 11);
    
    
		go_count_all_seasons(v_id, v_s_group);
		save_sklad_ost(v_id);
    map_fl.test(v_id, v_package_name, 10);
    map_fl.test(v_id, v_package_name, 11);   
    
		go_count_warm(v_id, v_s_group);
		save_sklad_ost(v_id);
    map_fl.test(v_id, v_package_name, 10);
    map_fl.test(v_id, v_package_name, 11);
    
        go_count_winter(v_id, v_s_group);
		save_sklad_ost(v_id);
    map_fl.test(v_id, v_package_name, 10);
    map_fl.test(v_id, v_package_name, 11);
	
    map_fl.test(v_id, v_package_name, 3); 
    map_fl.test(v_id, v_package_name, 6);  
    map_fl.test(v_id, v_package_name, 12);
    map_fl.test(v_id, v_package_name, 13);
    map_fl.test(v_id, v_package_name, 14);
  
		select sum(kol)
		into v_count
		from t_map_new_poexalo
		where id = v_id;
	
		save_result_report(v_id);
        if i_is_test = 'F' then
            insert into tdv_map_new_result
            select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0, 0, 0, 0, 0, 'podsort' group_ras 
            from t_map_new_poexalo 
            where id = v_id and boxty is null
            
            union all
            
            select a.art, sum(a.kol)/f.kol verme, a.boxty, a.id_shop, 0, 0, 0, 0, 0, 'podsort' group_ras 
            from t_map_new_poexalo a
            left join (select boxty, sum(kol) kol from t_sap_box_rost group by boxty) f on a.boxty = f.boxty
            where a.id = v_id and a.boxty is not null
            group by a.art, a.boxty, f.kol, a.id_shop;
            
--            select a.art, count(*) as verme, a.boxty, a.id_shop, 0, 0, 0, 0, 0, 'podsort' group_ras 
--            from (
--                select art, id_shop, boxty, sum(kol) kol  from t_map_new_poexalo 
--                where id = v_id and boxty is not null
--                group by art, id_shop, boxty
--            ) a
--            inner join (
--                select boxty, count(kol) kol from t_sap_box_rost group by boxty
--            ) b on a.boxty = b.boxty and a.kol = b.kol
--            group by a.art, a.id_shop, a.boxty;
        end if;
        
		update TDV_MAP_13_RESULT
		set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
		where id = v_id;
	
		commit;
	end;

end bep_map_002;