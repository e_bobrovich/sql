select * from ri_test_result b order by times desc;
select * from t_map_new_time_log order by id desc, block_no;
select * from TDV_MAP_13_RESULT b order by id desc;

select id_shop, sum(kol) from t_map_new_poexalo where id = 14438 group by id_shop;

select id_shop_from, sum(kol) from t_map_new_poexalo where id = 14438 group by id_shop_from;

select * from t_map_new_poexalo where id = 14323;

select season, sum(kol) from t_map_new_poexalo where id = 15154 group by season;


select * from t_map_new_poexalo where id = 12914 and id_shop = '0063' and (id_shop, art) in (
    select a2.id_shop, a2.art from cena_skidki_all_shops a2
    where substr(to_char(a2.cena), -2) = '99'
);


delete from t_map_new_poexalo where id >= 12914 and id <= 12914 
and id_shop = '0063'
--and id_shop in ('0010', '0044', '0063', '0064', '0065','0074')
and (id_shop_from, art) in (
select a2.id_shop, a2.art from cena_skidki_all_shops a2
where substr(to_char(a2.cena), -2) = '99'
)
;

delete from t_map_new_poexalo where id >= 12914 and id <= 12914 
and id_shop = '0063'
--and id_shop_from in ('0010', '0044', '0063', '0064', '0065','0074')
and (id_shop_from, art) in (
select a2.id_shop, a2.art from cena_skidki_all_shops a2
where substr(to_char(a2.cena), -2) = '99'
)
;



select distinct (select groupmw from s_art where art = a.art) from t_map_new_poexalo a where id >= 12915 and id<= 12926;
select distinct season from t_map_new_poexalo a where id >= 13934 and id<= 13934;
select distinct season from t_map_new_poexalo a where id >= 12914  and id<= 12914 ;

--проверка, что вторые пары получили только артикула-размеры, которых продано не меньше двух
select *
from (select a.id_shop, a.art, a.asize, sum(a.kol_ost) kol_ost, sum(kol) kol, sum(kol_sale) kol_sale
             from (select a.id_shop, a.art, a.asize, sum(a.kol_ost2) kol_ost, 0 kol, sum(kol_sale) kol_sale
                            from TDV_MAP_NEW_OST_SALE a
                            group by a.id_shop, a.art, a.asize
                            union all
                            select a.id_shop, a.art, a.asize, sum(a.verme) kol_ost, 0 kol, 0 kol_sale
                            from TDV_NEW_SHOP_OST a
                            group by a.id_shop, a.art, a.asize
                            union all
                            select a.id_shop, nvl(b.analog, a.art) art, a.asize, 0, sum(kol) kol, 0 kol_sale
                            from t_map_new_poexalo a 
                            left join tdv_map_new_anal b on a.art = b.art
                            where id = 10594 and season = (select distinct season from TDV_MAP_NEW_OST_SALE) and a.block_no in (7, 12, 18, 20)
                            group by a.id_shop, nvl(b.analog, a.art), a.asize) a
             group by a.id_shop, a.art, a.asize)
where kol != 0
            and kol_ost + kol = 2
            and kol_sale < 2;
            
            
            
select x3.*, b.season
--from tdv_map_new_sap_reserv x3
from tdv_map_new_sap_reserv2 x3
inner join s_art b on x3.art = b.art
where x3.boxty = ' '
         and to_number(asize, '999.99') != 0 -- россыпь
         and trans != 'X'
         and kunnr not in ('X_NERASP', 'X_SD_USE')
and b.season = 'Зимняя' 
;




select * from t_map_new_poexalo where id = 10614;
select * from st_sap_ob_ost 
--where art = '8938156' and asize = 38
;
------------------------------
select art, a.asize,sum(kol) kol, 0 kol_ost, 0 kol_reserv
from t_map_new_poexalo a 
where id = 10594 --and season = (select distinct season from tdv_map_new_ost_sale)
and boxty is null
group by art, a.asize
;

select a.art, d.asize, 0 kol, sum(a.verme) kol_ost, 0 kol_reserv
from st_sap_ob_ost a
--left join s_art b on a.art = b.art
--left join TDV_MAP_NEW_ANAL c on a.art = c.art
left join s_all_mat d on a.matnr = d.matnr
where d.asize != 0
            and a.boxty = ' '
group by a.art, d.asize
;
----------------------

select art, to_number(asize, '999.99') asize, 0 kol, 0 kol_ost, sum(verme) kol_reserv
from tdv_map_new_sap_reserv x3
--from tdv_map_new_sap_reserv2 x3
where x3.boxty = ' '
            and to_number(asize, '999.99') != 0
            and trans != 'X'
            and kunnr not in ('X_NERASP', 'X_SD_USE')
            and kunnr = 'X_FND_INET'
group by art, to_number(asize, '999.99')

union all

select art, to_number(boxty, '999.99'), 0 kol, 0 kol_ost, sum(verme) kol_reserv
from tdv_map_new_result
where boxty not in (select boxty from t_sap_box_rost) and boxty != ' '
group by art, boxty;

select distinct boxty
from tdv_map_new_result
where boxty not in (select boxty from t_sap_box_rost) and boxty != ' '
;
---------------
select * from (
    select art, asize, sum(kol_ost) kol_ost, sum(kol) kol, sum(kol_reserv) kol_reserv  from (
        select art, a.asize,sum(kol) kol, 0 kol_ost, 0 kol_reserv
        from t_map_new_poexalo a 
        where id = 10616
        and boxty is null
        group by art, a.asize
        
        union all
        
        select a.art, d.asize, 0 kol, sum(a.verme) kol_ost, 0 kol_reserv
        from st_sap_ob_ost a
        left join s_all_mat d on a.matnr = d.matnr
        where d.asize != 0
                    and a.boxty = ' '
        group by a.art, d.asize
        
        union all
        
        select art, to_number(asize, '999.99') asize, 0 kol, -sum(verme) kol_ost, sum(verme) kol_reserv
        from tdv_map_new_sap_reserv x3
        where x3.boxty = ' '
                    and to_number(asize, '999.99') != 0
                    and trans != 'X'
                    and kunnr not in ('X_NERASP', 'X_SD_USE')
                    and kunnr = 'X_FND_INET'
        group by art, to_number(asize, '999.99')
        
        union all
        
        select art, to_number(boxty, '999.99') asize,  0 kol, -sum(verme) kol_ost, sum(verme) kol_reserv
        from tdv_map_new_result
        where boxty not in (select boxty from t_sap_box_rost) and boxty != ' ' and  'podsort' != group_ras 
        group by art, boxty 
    ) 
    where kol_ost >= 0
    --and kol > 0 
    group by art, asize
) where kol_ost - kol < 0
;

select art, to_number(asize, '999.99') asize, 0 kol, -sum(verme) kol_ost, sum(verme) kol_reserv
from tdv_map_new_sap_reserv x3
where x3.boxty = ' '
        and to_number(asize, '999.99') != 0
        and trans != 'X'
        and kunnr not in ('X_NERASP', 'X_SD_USE')
        and kunnr = 'X_FND_INET'
group by art, to_number(asize, '999.99')
;

call map_fl.test(10615, 'FIRM.BEP_MAP_002', 13);