select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0 as step_no, 0 sum_transit, 0 sum_reserv, 0 sum_ost, 0 sum_rc_ost, 'podsort' group_ras from t_map_new_poexalo
where id = 10735;


with t_shops as
 (select shopid
	from tdm_map_new_shop_group
	where s_group = 20501),
t_art as
 (select art
	from s_art where season in (select distinct season from t_map_new_poexalo a where a.id = 10735) and season!= ' ')

select a.id_shop, a.art, a.asize, b.normt, b.season, b.groupmw, sum(kol_poexalo) kol_poexalo, sum(kol_sale_0101) kol_sale_0101,
			 sum(kol_sale_02) kol_sale_02, sum(kol_sale_06) kol_sale_06, sum(kol_ost) kol_ost, sum(kol_trans) kol_trans, sum(kol_rc_svh) kol_rc_svh
from (select a.id_shop, a.art, a.asize, sum(kol) kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost, 0 kol_trans, 0 kol_rc_svh
			 from t_map_new_poexalo a
			 where a.id = 10735
						 and id_shop in (select shopid
														 from t_shops)
			 group by a.id_shop, a.art, a.asize
			 
			 union all
			 select a.id_shop id_shop, art, b.asize, 0 kol_poexalo, sum(b.kol) kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost, 0 kol_trans,
							0 kol_rc_svh
			 from pos_sale1 a
			 inner join pos_sale2 b on a.id_chek = b.id_chek
																 and a.id_shop = b.id_shop
			 where a.bit_close = 'T'
						 and a.bit_vozvr = 'F'
						 and b.scan != ' '
						 and b.procent = 0
						 and a.sale_date >= to_date('01012019', 'ddmmyyyy')
						 and a.id_shop in (select shopid
															 from t_shops)
						 and b.art in (select art
													 from t_art)
			 group by a.id_shop, b.art, b.asize
			 
			 union all
			 select a.id_shop id_shop, art, b.asize, 0 kol_poexalo, 0 kol_sale_0101, sum(b.kol) kol_sale_02, 0 kol_sale_06, 0 kol_ost, 0 kol_trans,
							0 kol_rc_svh
			 from pos_sale1 a
			 inner join pos_sale2 b on a.id_chek = b.id_chek
																 and a.id_shop = b.id_shop
			 where a.bit_close = 'T'
						 and a.bit_vozvr = 'F'
						 and b.scan != ' '
						 and b.procent = 0
						 and a.sale_date >= add_months(sysdate, -2)
						 and a.id_shop in (select shopid
															 from t_shops)
						 and b.art in (select art
													 from t_art)
			 group by a.id_shop, b.art, b.asize
			 
			 union all
			 select a.id_shop id_shop, art, b.asize, 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, sum(b.kol) kol_sale_06, 0 kol_ost, 0 kol_trans,
							0 kol_rc_svh
			 from pos_sale1 a
			 inner join pos_sale2 b on a.id_chek = b.id_chek
																 and a.id_shop = b.id_shop
			 where a.bit_close = 'T'
						 and a.bit_vozvr = 'F'
						 and b.scan != ' '
						 and b.procent = 0
						 and a.sale_date >= add_months(sysdate, -6)
						 and a.id_shop in (select shopid
															 from t_shops)
						 and b.art in (select art
													 from t_art)
			 group by a.id_shop, b.art, b.asize
			 
			 union all
			 select a.id_shop, a.art, a.asize, 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, sum(kol) kol_ost, 0 kol_trans, 0 kol_rc_svh
			 from e_osttek_online a
			 left join pos_brak x on a.scan = x.scan
															 and a.id_shop = x.id_shop
			 where a.id_shop in (select shopid
													 from t_shops)
						 and x.art is null
						 and a.procent = 0
						 and a.art in (select art
													 from t_art)
			 group by a.id_shop, a.art, a.asize
			 having sum(kol) > 0
			 
			 -- часть транзитных документов ------------------------
			 union all
			 select a1.shopid, a1.art art, to_number(asize, '9999.99'), 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost,
							sum(a1.kol) kol_trans, 0 kol_rc_svh
			 from trans_in_way a1
			 inner join s_art c on a1.art = c.art
			 where scan != ' '
						 and a1.shopid in (select shopid
															 from t_shops)
						 and a1.art in (select art
														from t_art)
			 group by a1.shopid, a1.art, to_number(asize, '9999.99')
			 having sum(kol) > 0
			 
			 -- добавляю резервы из САПА
			 union all
			 select y.shopid, x3.art, to_number(asize, '999.99'), 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost,
							sum(x3.verme) kol_trans, 0 kol_rc_svh
			 from tdv_map_new_sap_reserv x3
			 left join s_shop y on x3.kunnr = y.shopnum
			 inner join s_art c on x3.art = c.art
			 where x3.boxty = ' '
						 and to_number(asize, '999.99') != 0 -- россыпь
						 and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
						 and verme > 0
						 and shopid is not null
						 and y.shopid in (select shopid
															from t_shops)
						 and x3.art in (select art
														from t_art)
             and trans != 'X'               
			 group by y.shopid, x3.art, asize
			 
			 --
			 union all
			 select y.shopid, a2.art, to_number(b.asize), 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost, sum(a2.verme) kol_trans,
							0 kol_rc_svh
			 from tdv_map_new_sap_reserv a2
			 inner join T_SAP_BOX_ROST b on a2.boxty = b.boxty
			 inner join s_shop y on a2.kunnr = y.shopnum
			 inner join s_art c on a2.art = c.art
			 where a2.boxty != ' ' --короба
						 and verme > 0
						 and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
						 and y.shopid in (select shopid
															from t_shops)
						 and a2.art in (select art
														from t_art)
             and trans != 'X'               
			 group by y.shopid, a2.art, b.asize
			 
			 union all
			 select x.shopid, x3.art, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06,
							0 kol_ost, sum(decode(x3.boxty, ' ', x3.verme, f.kol)) kol_trans, 0 kol_rc_svh
			 from tdv_map_new_sap_reserv x3
			 left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
			 left join s_shop x on x3.kunnr = x.shopnum
			 left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
			 inner join s_art c on x3.art = c.art
			 where kunnr not in ('X_NERASP', 'X_SD_USE')
						 and trans = 'X'
						 and x.shopid in (select shopid
															from t_shops)
						 and x3.art in (select art
														from t_art)
			 group by x3.art, nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid
			 
			 -- РЦ и СВХ
			 union all
			 select shopid id_shop, art, asize, 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost, 0 kol_trans, count(*) kol
			 from sklad_ost_owners
			 where shopid not in ('0', ' ')
						 and shopid in (select shopid
														from t_shops)
						 and art in (select art
												 from t_art)
			 group by skladid, shopid, art, asize
			 union all
			 
			 select owner id_shop, art, asize, 0 kol_poexalo, 0 kol_sale_0101, 0 kol_sale_02, 0 kol_sale_06, 0 kol_ost, 0 kol_trans, sum(kol)
			 from dist_center.e_osttek
			 where owner in (select shopid
											 from t_shops)
						 and art in (select art
												 from t_art)
             and DCID not in (select shopid from st_shop where org_kod = 'TST')             
			 group by DCID, owner, art, asize
			 having sum(kol) > 0
			 
			 ) a
left join s_art b on a.art = b.art
group by a.id_shop, a.art, a.asize, b.normt, b.season, b.groupmw
--where a.id = 10075
;