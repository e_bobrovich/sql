select * from TDV_MAP_13_RESULT b order by id desc;

select * from t_map_new_otbor_after where id = '14334';
select * from t_map_new_otbor_before where id = '14334';

select * from tdv_new_shop_ost_after where id = '14334';
select * from tdv_map_new_ost_sale_after where id = '14334';

select * from s_art where art = '1830190';

select * from t_map_new_poexalo where id = 14334 and id_shop = '2642';

--38201
select x.*, r.*
from (select a.id_shop, g.landid, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale1, sum(kol_ost2) kol_ost2,
                         sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, z.current_limit, max(d.style) style,
                         max(d.groupmw) groupmw
            from TDV_MAP_NEW_OST_SALE_after a
            left join s_shop g on a.id_shop = g.shopid
            inner join (select distinct analog
                                 from T_MAP_NEW_OTBOR_after where kol >0 and id = '14334') c on a.art = c.analog -- проходимся только по тем артикулам, которые есть на остатках распределения
            inner join tdm_map_new_shop_group x on x.s_group = 38201
                                                                                         and a.id_shop = x.shopid
            inner join t_map_current_shop_limit z on a.id_shop = z.id_shop
            left join tdv_map_shop_reit b on a.id_shop = b.id_shop
            left join (select distinct analog from tdv_map_stock2) f on a.art = f.analog -- стоки
            left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                                from s_art
                                group by decode(normt, ' ', art, normt)) d on d.normt = a.art
            
            where a.id = 14334 and a.id_shop = '2642' and a.art = '1830190'  
                        and
                        kol_sale + kol_ost2 != 0
                        --and current_limit > 0 --сразу откидываем магазины с нулевым лимитом
                        and case 
                        when a.id_shop in ('0006', '0010', '0044', '0063', '0064', '0065', '0074', '0075', '0076') 
                            then f.analog else null end is null -- магазины 10, 44, 63, 64, 65, 74, 75,  76 мы не грузим сток
            group by a.id_shop, g.landid, a.art, a.season, nvl(b.reit2, 0), z.current_limit
            having(sum(kol_sale) + sum(kol_ost) != 0)) x

inner join MAP_DAY_SORT_COLOR_PROC r on x.kol_sale1 <= r.sale_to
                                                                    and x.kol_sale1 >= r.sale_ot
                                                                    and x.kol_ost2 <= r.proc_to
                                                                    and x.kol_ost2 >= r.proc_ot
                                                                    and r.color = case
                                                                        when null is null then
                                                                         r.color
                                                                        else
                                                                         null
                                                                    end
                                                                    and r.id = 1
order by kol_sale1 desc, reit2;



select distinct a.asize
--from T_MAP_NEW_OTBOR_after a
from T_MAP_NEW_OTBOR_before a
left join s_art e on a.art = e.art
left join (select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol
                 from TDV_MAP_NEW_OST_SALE_after
                 where id_shop = '2642'
                             and art = '1830190'
                             and id = 14334
                 group by asize) b on a.asize = b.asize
left join (select asize, sum(kol) kol
                 from t_map_new_poexalo x
                 where id_shop = '2642'
                             and x.text1 = '1830190'
                             and id = 14334
                 group by asize) c on a.asize = c.asize
left join (select x.asize, sum(x.verme) kol
                 from TDV_NEW_SHOP_OST_after x
                 where x.verme > 0
                             and x.art = '1830190'
                             and id_shop = '2642'
                             and id = 14334
                 group by x.asize) d on a.asize = d.asize

where a.id = 14334 and a.analog = '1830190'
         and a.kol > 0
         and nvl(b.kol_sale, 0) >= decode('F', 'T', 2, 0)
         and case
             when 'F' = 'F' and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) = 0 then
                1
             when 'F' = 'T' and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) <= 1 then
                1
             else
                0
         end = 1
order by a.asize;

select a.*
from (select a.*
             from T_MAP_NEW_OTBOR_after a
             left join (select distinct x.art
                                 from tdv_map_new_ost_sale_after x
                                 where x.id = 14334 and x.id_shop = '2642'
                                             and art in (select art
                                                                     from TDV_MAP_NEW_ANAL
                                                                     where analog = '1830190')
                                             and x.kol_ost > 0) b on a.art = b.art
             where id = 14334 and analog = '1830190'
                         and asize = 37
                         and kol > 0
                         and not (a.art like '%Р2%' and 'BY' = 'RU') -- На РФ не везем артикула с Р2 
             -- какие в первую очередь берем артикула:  
             -- 1. артикул , который уже есть в магазине
             -- 2. артикул , которого МЕНЬШЕ на складе  
             order by case
                                    when b.art is not null then
                                     0
                                    else
                                     1
                                end, a.kol) a
where rownum = 1;


select count(asize),
nvl(sum(
    case when asize in (4, 4.5, 5, 5.5)    and 'Женские' = 'Женские' and ' ' != 'Sport' then 1
         when asize in (8, 8.5, 9)         and 'Женские' = 'Мужские' and ' ' != 'Sport' then 1
         when asize in (5, 5.5, 6, 6.5)    and 'Женские' = 'Женские' and ' ' = 'Sport' then 1
         when asize in (8.5, 9, 9.5, 10)   and 'Женские' = 'Мужские' and ' ' = 'Sport' then 1
         when asize in (37, 38, 39)        and 'Женские' = 'Женские' then 1
         when asize in (42, 43)            and 'Женские' = 'Мужские' then 1
         else 0 end), 0) 
from (
    select distinct asize from tdv_map_new_ost_sale_after where id = 14334 and id_shop = '2642' and art = '1830190' and kol_ost > 0
    union all
    select distinct asize from tdv_new_shop_ost_after where id = 14334 and id_shop = '2642' and art = '1830190' and verme > 0
    union all
    select distinct asize from t_map_new_poexalo where id = 14334 and id_shop = '2642' and /*art*/ text1 = '1830190' and kol > 0
);