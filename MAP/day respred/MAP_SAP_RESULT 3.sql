--россыпь подсортировки с учетом вчерашних транзитов
select 
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, ' ' boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, id_shop, 
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from tdv_map_new_result 
    where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost)
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select kunnr, art, sum(verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv 
    where kunnr like 'S%' and boxty = ' ' and asize is not null and trans = ' '
    and rasp = 'X'  and ablad = 48
    group by kunnr, art, asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = c.shopnum
left join s_all_mat g on nvl(a.art, c1.art) = g.art and nvl(a.asize, c1.asize) = g.asize
;

--короба подсортировки
select a.art,a.verme, a.boxty, ' ', ' ', ' ', c.shopnum, ' ', '0,0'
from (
    select art, sum(verme) verme, id_shop, boxty
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty in (select boxty from t_sap_box_rost)
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid;




select
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, nvl(a.boxty, c1.boxty) boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, ' ' matnr, '0,0' asize
--a.art,a.verme, a.boxty, ' ', ' ', ' ', c.shopnum, ' ', '0,0', c1.*
from (
    select art, sum(verme) verme, id_shop, boxty
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty in (select boxty from t_sap_box_rost)
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select * --distinct kunnr
    --kunnr, art, sum(verme) verme, boxty
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where boxty != ' ' and trans = ' ' and kunnr not in (select id_shop from st_sap_ob_reserv) and kunnr not like 'X%' and kunnr != ' '  
    and rasp = 'X'  and ablad = 48 and b.season != 'Зимняя' 
    --group by kunnr, art, boxty
)  c1 on a.art = c1.art and a.boxty = c1.boxty and c1.kunnr = c.shopnum
;


select kunnr, art, sum(verme) verme,
    boxty
    from tdv_map_new_sap_reserv 
    where kunnr like 'S%' and boxty != ' ' and trans = ' '
    group by kunnr, art, boxty;
    
select distinct  boxty, trans from tdv_map_new_sap_reserv;