select * from map_proj.CURRENT_ART_FOND_LIMIT_PODSORT;
select * from map_proj.ART_FOND_LIMIT_PODSORT;

select * from map_proj.ART_FOND_GROUPMW_ASIZE_PODSORT;

--delete from map_proj.CURRENT_ART_FOND_LIMIT_PODSORT;
--insert into map_proj.CURRENT_ART_FOND_LIMIT_PODSORT
select art, limit - current_fond, asize from map_proj.ART_FOND_LIMIT_PODSORT;


--update map_proj.CURRENT_ART_FOND_LIMIT_PODSORT set asize = 0;
update map_proj.ART_FOND_LIMIT_PODSORT set current_fond = 0, is_completed = 'F';
delete from map_proj.ART_FOND_LIMIT_PODSORT;

insert into map_proj.ART_FOND_LIMIT_PODSORT
select 
a.art, ceil(a.limit/count(*) over (partition by a.art)) asize_limit, a.current_fond, c.asize
--a.*, c.*, count(*) over (partition by a.art) count_sizes,
--ceil(a.limit/count(*) over (partition by a.art)) asize_limit
from map_proj.ART_FOND_LIMIT_PODSORT a
inner join s_art b on a.art = b.art
inner join map_proj.ART_FOND_GROUPMW_ASIZE_PODSORT c on b.groupmw = c.groupmw
;

delete from map_proj.ART_FOND_LIMIT_PODSORT where asize = 0;


select a.*, b.current_limit from map_proj.OTBOR_PODSORT a
      inner join map_proj.CURRENT_ART_FOND_LIMIT_PODSORT b on a.art = b.art and a.asize = b.asize
--      where a.kol > 0 and b.current_limit > a.kol
;


select * from t_map_new_poexalo where id = 29416;

select * from map_proj.CURRENT_ART_FOND_LIMIT_PODSORT;
select * from map_proj.ART_FOND_LIMIT_PODSORT;


merge into map_proj.ART_FOND_LIMIT_PODSORT a
using (
  select art, asize, sum(kol) kol from t_map_new_Poexalo 
  where id = 29416 and id_shop = 'fond' 
  group by art, asize
) b on (a.art = b.art and a.asize = b.asize)
when matched then
update set current_fond = current_fond + kol
;

--29404
--29416
--29699


-- текущий фонд
-- добавленное в фонд
insert into map_proj.ART_FOND_HISTORY_PODSORT
select art, asize, sum(kol) kol, id, sysdate from t_map_new_poexalo where id = 29416 and id_shop = 'fond' group by art, asize, id;
-- вычтенное из фонда

insert into map_proj.ART_FOND_HISTORY_PODSORT
select 
--*
a.art, a.asize, -1*least( sum(b.current_fond), sum(a.kol)) kol_res, a.id, sysdate 
from t_map_new_poexalo a
inner join map_proj.ART_FOND_LIMIT_PODSORT b on a.art = b.art and a.asize = b.asize
where a.id = 29416 and a.id_shop != 'fond' 
group by a.art, a.asize, a.id
having least( sum(b.current_fond), sum(a.kol)) != 0
;

select * from map_proj.ART_FOND_LIMIT_PODSORT;
select * from map_proj.ART_FOND_HISTORY_PODSORT;

select 
--*
a.art, a.asize, max(limit) limit, max(a.current_fond) current_fond, nvl(sum(b.kol),0) kol, least(max(limit), nvl(sum(b.kol),0)) res_kol
from map_proj.ART_FOND_LIMIT_PODSORT a
left join map_proj.ART_FOND_HISTORY_PODSORT b on a.art = b.art and a.asize = b.asize
group by a.art, a.asize
;


merge into map_proj.ART_FOND_LIMIT_PODSORT a
using (
  select a.art, a.asize,least(max(limit), nvl(sum(b.kol),0)) kol
  from map_proj.ART_FOND_LIMIT_PODSORT a
  left join map_proj.ART_FOND_HISTORY_PODSORT b on a.art = b.art and a.asize = b.asize
  group by a.art, a.asize
) b on (a.art = b.art and a.asize = b.asize)
when matched then
update set current_fond = kol
;

select * from tdv_map_new_result;

insert into tdv_map_new_result
select art, current_fond as verme, to_char(asize) boxty, 'fond', 0, 0, 0, 0, 0, 'podsort' group_ras 
from map_proj.ART_FOND_LIMIT_PODSORT where current_fond != 0; 

delete from tdv_map_new_result where id_shop = 'fond' and group_ras = 'podsort';


      select art, kol as verme, nvl(boxty, asize) boxty, id_shop, 0, 0, 0, 0, 0, 'podsort' group_ras 
      from t_map_new_poexalo 
      where id = 29902 and boxty is null and id_shop != 'fond' --без фонда
      
      union all
      
      select a.art, sum(a.kol)/f.kol verme, a.boxty, a.id_shop, 0, 0, 0, 0, 0, 'podsort' group_ras 
      from t_map_new_poexalo a
      left join (select boxty, sum(kol) kol from t_sap_box_rost group by boxty) f on a.boxty = f.boxty
      where a.id = 29902 and a.boxty is not null
      group by a.art, a.boxty, f.kol, a.id_shop
      
      union all
      -- фонд весь
      select art, current_fond as verme, to_char(asize) boxty, 'fond' id_shop, 0, 0, 0, 0, 0, 'podsort' group_ras 
      from map_proj.ART_FOND_LIMIT_PODSORT where current_fond != 0; 
      
      
select * from map_proj.ART_FOND_HISTORY_PODSORT where trunc(dates) = trunc(sysdate) and id != 29416;

