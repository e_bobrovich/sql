select * from TDV_MAP_13_RESULT order by id desc;

select * from tdv_map_shop_stage_count;

select * from t_map_new_poexalo where id  = 15314 and id_shop = '0048' and art = '1930001';
select * from t_map_new_otbor_before where id = 15474 and art = '1930001';
select * from t_map_new_otbor_after where id = 15474 and art = '1930001';
select * from TDV_NEW_SHOP_OST_after where id = 15314 and art = '1930001';

select * from t_map_current_shop_limit;


update t_map_new_poexalo set id_shop_from = 3528 where id = 15347 and id_shop_from = 3526;
delete t_map_new_poexalo where id = 15347 and id_shop_from = 3528 and kol <= 0;


select art, asize, sum(kol_ost) kol_ost, sum(rezerv) rezerv, sum(prev_rasp) prev_rasp, sum(kol_ost) - sum(rezerv) - sum(prev_rasp) diff  
from (
  select a.art, d.asize, sum(a.verme) kol_ost, 0 rezerv, 0 prev_rasp--, nvl(c.analog, a.art) analog
  from st_sap_ob_ost a
  --from st_sap_ob_ost2 a
  left join s_art b on a.art = b.art
  left join TDV_MAP_NEW_ANAL c on a.art = c.art
  left join s_all_mat d on a.matnr = d.matnr
  where d.asize != 0
        and a.boxty = ' '
        and a.art = '1930001'
  group by a.art, d.asize, nvl(c.analog, a.art)
  
  union all
  
  select art, to_number(asize, '999.99') asize, 0 kol_ost, sum(verme) rezerv, 0 prev_rasp
  from tdv_map_new_sap_reserv x3
  where x3.boxty = ' '
       and to_number(asize, '999.99') != 0 -- россыпь
       and trans != 'X'
       and ((/*kunnr not in ('X_NERASP', 'X_SD_USE') and */ kunnr in ('X_FND_INET', '0010000854')) 
                                or rasp != 'X')
       and art = '1930001'
  group by art, to_number(asize, '999.99')
  
  union all
  
  select art, to_number(boxty, '999.99') asize, 0 kol_ost, 0 rezerv, sum(verme) prev_rasp from tdv_map_new_result
  where art = '1930001' and group_ras != 'podsort'
  group by art, boxty
) group by art, asize
order by asize
;

select a.art, d.asize, sum(a.verme) kol, 0 flag, nvl(c.analog, a.art) analog
from st_sap_ob_ost a
--from st_sap_ob_ost2 a
left join s_art b on a.art = b.art
left join TDV_MAP_NEW_ANAL c on a.art = c.art
left join s_all_mat d on a.matnr = d.matnr
where d.asize != 0
      and a.boxty = ' '
      and a.art = '1930001'
group by a.art, d.asize, nvl(c.analog, a.art)
order by asize
;

select art, to_number(asize, '999.99') asize, sum(verme) verme
from tdv_map_new_sap_reserv x3
--from tdv_map_new_sap_reserv2 x3
where x3.boxty = ' '
     and to_number(asize, '999.99') != 0 -- россыпь
     and trans != 'X'
     and ((kunnr not in ('X_NERASP', 'X_SD_USE') and kunnr in ('X_FND_INET', '0010000854')) 
                              or rasp != 'X')
     and art = '1930001'
group by art, to_number(asize, '999.99');


select art, boxty, sum(verme) verme from tdv_map_new_result
where art = '1930001' and group_ras != 'podsort'
group by art, boxty;
                    

select id_shop, sum(kol) kol from (
select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc
from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
       from e_osttek_online a2
       inner join s_art c on a2.art = c.art
       inner join st_shop d on a2.id_shop = d.shopid
       left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
       left join st_muya_art z on a2.art = z.art
       left join st_sale_block y on a2.art = y.art and case when d.landid = 'BY' then 2 when d.landid = 'RU' then 1 end = y.numconf  
       left join (
            select id_shop, art from cena_skidki_all_shops
            where substr(to_char(cena), -2) = '99'
       ) w on a2.id_shop = w.id_shop and a2.art = w.art
       
       left join t_map_arts_out u on a2.art = u.art

       where a2.scan != ' '
       and a2.id_shop in ('3710','3712','3721','3723')
       and c.season = 'Летняя'

        and case when 'T' = 'T' then a2.procent else 0 end = 0 -- исключение некондиции
        and case when 'T' = 'T' then x.scan else null end is null -- исключение брака
        and case when 'F' = 'T' then 
                                            case when (z.art is not null) then 0 else 1 end
                                            else 1 end = 1 -- исключение покупной
        and case when 'T' = 'T' then c.groupmw else 'Мужские' end in ('Мужские', 'Женские') -- исключение детской
        and case when 'T' = 'T' then y.art else null end is null -- исключение заблокированной
        and case when 'F' = 'T' then w.art else null end is null -- исключение стоковой
        and case when 'F' = 'T' then 
                                            case when (c.facture = 'Покупная') then 0 else 1 end
                                            else 1 end = 1 -- исключение покупной -- исключение стоковой
        and case when 'F' = 'T' then u.art else ' ' end is not null --только из списка

       group by a2.id_shop, a2.art, a2.asize, c.season
       having sum(kol) > 0)
group by id_shop, art, asize, season
) group by id_shop;


select * from pvl_scan;

select * from e_osttek a
inner join pvl_scan b on a.scan = b.scan ;

select * from kart_v where scan = '000000000001001401222';