
  --CREATE OR REPLACE FORCE VIEW "FIRM"."MAP_SAP_RESULT" ("ART", "VERME", "BOXTY", "F1", "F2", "F3", "SHOPNUM", "MATNR", "ASIZE") AS 
  select a.art, sum(verme) verme, case when a.id_shop = 'X_FND_INET' then  ' ' else boxty end boxty, ' ' f1, ' ' f2, ' ' f3, nvl(c.shopnum,a.id_shop) shopnum,
g.matnr, cast(case when a.id_shop = 'X_FND_INET' then case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end else '0,0' end as varchar2(5)) asize --else replace(boxty,'.',',')||',0' end else '0,0' end as varchar2(5)) asize
from (select nvl(a.art, b.art) art, nvl(a.boxty, to_number(b.asize)) boxty, nvl(a.verme,0) verme, 'X_FND_INET' id_shop
          from
          (select * from tdv_map_new_result a
          where verme>0 and a.group_ras = 'imrf') a
          full join
          (select  art, asize, sum(verme) from tdv_map_new_sap_reserv  where kunnr = 'X_FND_INET' group by art, asize) b on a.art = b.art and boxty = to_number(b.asize)) a
left join firm.s_shop c on a.id_shop  = case when substr(c.shopid,1,2) = '00' and length(c.shopid)<10 then '7'||substr(c.shopid,2)  else c.shopid end
inner join s_art d on a.art = d.art
left join s_all_mat g on a.art = g.art and a.boxty = g.asize
--where verme>0 and a.group_ras = 'imrf'--and d.mtart = 'ZFRT'
group by a.art,  boxty,  c.shopnum, a.id_shop, g.matnr
union all

select a.art, sum(verme) verme, boxty, ' ' f1, ' ' f2, ' ' f3, c.shopnum, cast(' ' as varchar2(20)) matnr, cast('0,0' as varchar2(5)) asize
from firm.tdv_map_new_result a
left join firm.s_shop c on a.id_shop  = case when substr(c.shopid,1,2) = '00'  and length(c.shopid)<10 then '7'||substr(c.shopid,2) else c.shopid end
inner join s_art d on a.art = d.art
where verme>0 and a.group_ras != 'imrf' and a.group_ras != 'podsort' --and d.mtart = 'ZFRT'
group by a.art,  boxty,  c.shopnum
union all

select nvl(b.art, c1.art) art , nvl(sum(b.verme), 0) verme, nvl(b.boxty, c1.boxty) , ' ', ' ', ' ', nvl(b.res_type, c1.kunnr), ' ', '0,0'
from  (select b.* from firm.TDV_MAP_NEW_RESRV b inner join s_art c2 on b.art = c2.art where res_type = 'X_REZ_FT' ) b --and c2.mtart = 'ZFRT'
full join (select * from tdv_map_new_sap_reserv where kunnr = 'X_REZ_FT') c1 on b.art = c1.art and b.boxty = c1.boxty
--where b.res_type != 'X_REZ_SD' and  verme>0
group by nvl(b.art, c1.art)  ,  nvl(b.boxty, c1.boxty) , nvl(b.res_type, c1.kunnr)
union all

select nvl(b.art, c1.art) art , nvl(sum(b.verme), 0) verme, nvl(b.boxty, c1.boxty) , ' ', ' ', ' ', nvl(b.res_type, c1.kunnr), ' ', '0,0'
from  (select b.* from firm.TDV_MAP_NEW_RESRV b inner join s_art c2 on b.art = c2.art where res_type = 'X_REZ_SD' ) b --and c2.mtart = 'ZFRT'
full join (select * from tdv_map_new_sap_reserv where kunnr = 'X_REZ_SD') c1 on b.art = c1.art and b.boxty = c1.boxty
--where b.res_type != 'X_REZ_SD' and  verme>0
group by nvl(b.art, c1.art)  ,  nvl(b.boxty, c1.boxty) , nvl(b.res_type, c1.kunnr)
union all

select a.art,  a.verme - nvl(b.verme,0) -  nvl(c.verme,0) - nvl(d.verme,0) , a.boxty, ' ', ' ', ' ', 'X_NERASP', ' ', '0,0'
from (select a.art, sum(a.verme)/f.kol verme,  a.boxty
        from st_sap_ob_ost a
        left join (select boxty, sum(kol) kol from T_SAP_BOX_ROST group by boxty) f on a.boxty = f.boxty
        where a.boxty NOT IN (' ', 'N') group by a.art, a.boxty, f.kol) a
full join (select art, boxty, sum(verme) verme from  tdv_map_new_result b where verme > 0 group by art, boxty) b on a.art = b.art and a.boxty  = b.boxty
full join (select art, boxty, sum(verme) verme from  TDV_MAP_NEW_RESRV b where verme > 0 group by art, boxty) c on a.art = c.art and a.boxty  = c.boxty
full join (select art, boxty, sum(verme) verme from  tdv_map_new_sap_reserv b where  trans != 'X' and boxty != ' ' and substr(nvl(kunnr, ' '),1,2) != 'X_' and verme > 0 group by art, boxty) d on a.art = d.art and a.boxty  = d.boxty
where a.verme - nvl(b.verme,0) -  nvl(c.verme,0)- nvl(d.verme,0) > 0
union all

select f.art, f.verme, ' ', ' ', ' ', ' ', c.shopnum, g.matnr, case when instr(to_char(f.asize),',')!= 0 then to_char(f.asize) else to_char(f.asize)||',0' end
from TDV_MAP_NEW_RESULT_SIZE f
left join s_shop c on f.id_shop  = c.shopid --case when substr(c.shopid,1,2) = '00' then '7'||substr(c.shopid,2) else c.shopid end
left join s_all_mat g on f.art = g.art and f.asize = g.asize
where verme > 0

union all

--россыпь подсортировки с учетом вчерашних транзитов
select 
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, ' ' boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, g.matnr, nvl(a.asize, c1.asize) asize
from (
    select art, sum(verme) verme, id_shop, 
    case when instr(replace(to_char(boxty),'.',',') , ',') != 0 then replace(boxty,'.',',') else replace(boxty,'.',',')||',0' end asize
    from tdv_map_new_result 
    where group_ras = 'podsort' and boxty not in (select boxty from t_sap_box_rost)
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select a.kunnr, a.art, sum(a.verme) verme,
    case when instr(replace(to_char(asize),'.',',') , ',') != 0 then replace(asize,'.',',') else replace(asize,'.',',')||',0' end asize
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where kunnr like 'S%' and boxty = ' ' and asize is not null and trans = ' '
    and rasp = 'X'  and ablad = 48 and b.season != 'Зимняя' 
    group by a.kunnr, a.art, a.asize
) c1 on a.art = c1.art and a.asize = c1.asize and c1.kunnr = c.shopnum
left join s_all_mat g on nvl(a.art, c1.art) = g.art 
                     and nvl(a.asize, c1.asize) = case when instr(replace(to_char(g.asize),'.',',') , ',') != 0 then replace(g.asize,'.',',') else replace(g.asize,'.',',')||',0' end
union all

--короба подсортировки
select
nvl(a.art, c1.art) art, nvl(a.verme, 0) verme, nvl(a.boxty, c1.boxty) boxty, ' ', ' ', ' ', nvl(c.shopnum,c1.kunnr) shopnum, ' ' matnr, '0,0' asize
--a.art,a.verme, a.boxty, ' ', ' ', ' ', c.shopnum, ' ', '0,0', c1.*
from (
    select art, sum(verme) verme, id_shop, boxty
    from tdv_map_new_result
    where group_ras = 'podsort' and boxty in (select boxty from t_sap_box_rost)
    group by art, id_shop, boxty
) a
left join firm.s_shop c on a.id_shop  = c.shopid
full join (
    select a.kunnr, a.art, sum(a.verme) verme, a.boxty
    from tdv_map_new_sap_reserv a
    inner join s_art b on a.art = b.art
    where boxty != ' ' and trans = ' ' and kunnr not in (select id_shop from st_sap_ob_reserv) and kunnr not like 'X%' and kunnr != ' '  
    and rasp = 'X'  and ablad = 48 and b.season != 'Зимняя' 
    group by a.kunnr, a.art, a.boxty
)  c1 on a.art = c1.art and a.boxty = c1.boxty and c1.kunnr = c.shopnum;
