select id_season, season from map_proj.SETUP_SEASONS_PODSORT 
        where trunc(date_start) <= trunc(sysdate) and trunc(sysdate) <= trunc(date_end) 
        order by order_calc;

select id_block, block_name from map_Proj.SETUP_SEASON_BLOCKS_PODSORT
where id_season = 9
order by priority;

select param_name, param_value from map_proj.SETUP_BLOCK_PARAMS_PODSORT
where id_block = 18 
order by param_name;

select * 
from map_proj.SETUP_SEASONS_PODSORT s
left join map_Proj.SETUP_SEASON_BLOCKS_PODSORT sb on s.id_season = sb.id_season
left join map_proj.SETUP_BLOCK_PARAMS_PODSORT bp on sb.id_block = bp.id_block
where s.id_season = 9
order by sb.priority
;

select * 
from map_Proj.SETUP_SEASON_BLOCKS_PODSORT sb
left join map_proj.SETUP_BLOCK_PARAMS_PODSORT bp on sb.id_block = bp.id_block
where sb.id_season = 10
order by sb.priority, bp.param_name
;


select sb.id_block, sb.block_name, 
listagg(bep_map_002.get_pair_param_value(sb.block_name, bp.param_name, bp.param_value), ', ') within group (order by bp.param_name) param_val
--bep_map_002.get_pair_param_value('sum_all_rasp', null, null) pair_pram_value
from map_Proj.SETUP_SEASON_BLOCKS_PODSORT sb
left join map_proj.SETUP_BLOCK_PARAMS_PODSORT bp on sb.id_block = bp.id_block
where sb.id_season = 10
group by sb.id_block, sb.block_name, sb.priority
order by sb.priority
;

--funct for pram-val pair
-- in : block_name, param_name, param_value
-- out string: pair format param_name => value (with cast to datatype)
select * from map_proj.setup_block_params_podsort;
select * from map_proj.s_block_param_podsort;
select * from MAP_PROJ.setup_season_blocks_podsort;

select distinct param_type from map_proj.s_block_param_podsort;

select sbp.*,
case when param_type = 'NUMBER' then '2'
     when param_type = 'DATE' then 'to_date(''dd.mm.yyyy'', ''2'')'
     when param_type like 'VARCHAR%' then '''2'''
end param_value_with_cast
from map_proj.s_block_param_podsort sbp
where block_name = 'fillBaseData' and param_name = 'i_sales_start_date_months'
;


select
bep_map_002.get_pair_param_value('fillBaseData', 'i_sales_start_date', '01.01.2021'),
to_date('01.01.2021','dd.mm.yyyy')

from dual;
--;