create or replace package body bep_map_001 is

  /*
  оставить в магазинах обувь которая:
  продажи с 1 сентября    
      
  1) продажи >= 5, по 1 марта 2019    
  2) продажи = 4 на 1 фев 2019, остаток <= 1 на 1 фев 2019
  3) продажи = 3 на 1 фев 2019, остаток = 0 на 1 фев 2019
  4) продажи >= 4 на сегодня    
  5) фиолетовые  при этом не более  500 пар на магазины открытые до 1 авг 2018
  6) продажи >= 2 на сегодня    
      
  при распределении некондиция НЕ учитывается на остатке и НЕ учитывается в продажах.
      
  Не вывозим:
  1) Некондиция
  2) Брак
  3) Детская
  
  */

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 12:58:37
  -- Comments:
  --
  --****************************************************************************************************************************************************
  procedure fillBaseData(i_s_group          integer,
                         i_sales_start_date date,
                         i_sales_end_date   date,
                         i_stock_doc_no     integer default null) is
  begin
    execute immediate 'truncate table TDV_NEW_SHOP_OST'; -- таблица с транзитами и данными с заказами , лежащими на СГП
    execute immediate 'truncate table TDV_MAP_NEW_OST_SALE'; -- - Остатки вместе с продажами в разрезе аналогов
    --execute immediate 'truncate table TDV_MAP_NEW_OST_FULL'; -- текущие остатки для расчета
    execute immediate 'truncate table tdv_map_osttek'; -- текущие остатки собраные в одной таблице (нужно было на случай, когда текущие
    
    /*    insert into TDV_NEW_SHOP_OST
    
    -- добавляю резервы из САПА
    
      select distinct y.shopid, nvl(a2.analog, x3.art), to_number(asize), 1, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join s_shop y on x3.kunnr = y.shopnum
      left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
      where x3.boxty = ' '
            and asize != 0 -- россыпь
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
            and verme > 0
            and shopid is not null
      
      union
      select distinct y.shopid, nvl(a2.analog, a.art), to_number(b.asize), 1, 'TRANS'
      from tdv_map_new_sap_reserv2 a
      inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
      inner join s_shop y on a.kunnr = y.shopnum
      left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
      where a.boxty != ' ' --короба
            and verme > 0
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
      
      union
      select distinct x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, x3.asize) asize, 1, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST
                 group by boxty) e on x3.boxty = e.boxty
      left join s_shop x on x3.kunnr = x.shopnum
      left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
      where kunnr not in ('X_NERASP', 'X_SD_USE')
            and trans = 'X'
      group by nvl(a2.analog, x3.art), nvl(f.asize, x3.asize), x.shopid
      
      union
      -- остатки магазинов -- покупную для РФ оставляем в магазине -- и некондицию
      select a2.id_shop, a2.analog, a2.asize, 1, 'TRANS'
      from e_osttek_online_short a2
      left join st_muya_art f on a2.art = f.art
      left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
                                       and a2.analog = j.art
      where (1 = case
              when a2.landid = 'RU' and f.art is not null then
               1
              else
               0
            end or a2.procent != 0 -- некондицию оставляем
            or j.art is null -- что хорошо продавалось не забираем
            or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40')) --из сибири не забираем
      
      group by a2.id_shop, a2.analog, a2.asize
      having sum(kol) > 0
      
      union
      -- остатки РЦ и СВХ
      select a2.id_shop, nvl(d.analog, a2.art) analog, a2.asize, 1, 'TRANS'
      from tdv_map_rc_data a2
      inner join s_art c on a2.art = c.art
      left join TDV_MAP_NEW_ANAL d on d.art = a2.art
      where pv_season like '%,' || c.season || ',%'
            and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')
      
      \*            and (a2.art like '%/О%' or j.art is null -- только то, что плохо продавалось
      or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') --из сибири не забираем
      and 1 = case
        when e.landid = 'RU' and f.art is not null then
         1
        else
         0
      end) -- исключаем для РФ вывоз покупной    *\
      group by a2.id_shop, a2.asize, nvl(d.analog, a2.art)
      having sum(a2.kol) > 0;
    
    commit;*/
  
    -- формирую остатки на дату
    if i_stock_doc_no is null then
      insert into tdv_map_osttek
      
        select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
        from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
               from e_osttek_online a2
               inner join s_art c on a2.art = c.art
               left join pos_brak x on a2.scan = x.scan
                                       and a2.id_shop = x.id_shop
               where a2.scan != ' '
                    --and pv_season like '%' || c.season || '%' -- только нужный сезон
                     and a2.procent = 0
                     and a2.id_shop in (select shopid
                                        from tdm_map_new_shop_group
                                        where s_group = i_s_group)
                     and a2.id_shop in (select shopid
                                        from st_shop z
                                        where z.org_kod = 'SHP')
                     and x.scan is null
               group by a2.id_shop, a2.art, a2.asize, c.season
               having sum(kol) > 0)
        group by id_shop, art, asize, season;
    else
    
      insert into tdv_map_osttek
        select id_shop, art, asize, sum(kol) kol, season, 0 out_block, sum(kol_rc) kol_rc, sum(kol) kol
        from (select shopid id_shop, a.art, asize, sum(oste) kol, c.season, 0 out_block, 0 kol_rc
               from e_oborot2 a
               inner join s_art c on a.art = c.art
               where id = i_stock_doc_no
                     and c.mtart != 'ZHW3'
                     and procent = 0
                     --and pv_season like '%' || c.season || '%' -- только нужный сезон
                     and a.shopid in (select shopid
                                      from tdm_map_new_shop_group
                                      where s_group = i_s_group)
               group by shopid, a.art, asize, c.season)
        group by id_shop, art, asize, season;
    end if;
  
    -- разрешить оставить по каждому магазину только те сезоны, которые проставлены в конфиге 
    update tdv_map_osttek a
    set out_block =
         (select 0
          from tdm_map_new_shop_group b
          where s_group = i_s_group
                and a.id_shop = b.shopid
                and upper(b.season_out) like '%' || upper(a.season) || '%')
    where exists (select 0
           from tdm_map_new_shop_group b
           where s_group = i_s_group
                 and a.id_shop = b.shopid
                 and upper(b.season_out) like '%' || upper(a.season) || '%');
  
    delete from tdv_map_osttek
    where out_block != 0;
    commit;
  
    -- Остатки вместе с продажами в разрезе аналогов
    insert into TDV_MAP_NEW_OST_SALE
      select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 0 out_block, sum(kol_rc) kol_rc,
             sum(kol2) kol2
      from (
             -- текущие остатки
             select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
             from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, z.season, null,
                            sum(kol_rc) kol_rc, sum(a2.kol2) kol2
                     from tdv_map_osttek a2
                     left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
                     left join s_art z on a2.art = z.art
                     group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize, z.season
                     having sum(kol) > 0) a
             group by id_shop, a.art, asize, season
             
             union all
             
             -- все продажи данной обуви за выбранный период
             select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0, sum(b.kol) kol_sale, 0 kol_rc,
                     0 kol2
             from pos_sale1 a
             inner join pos_sale2 b on a.id_chek = b.id_chek
                                       and a.id_shop = b.id_shop
             inner join s_art c on b.art = c.art
             left join TDV_MAP_NEW_ANAL d on b.art = d.art
             where a.bit_close = 'T'
                   and a.bit_vozvr = 'F'
                   and b.scan != ' '
                   and b.procent = 0
                   and a.id_shop in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group)
                   and trunc(a.sale_date) >= trunc(i_sales_start_date)
                   and trunc(a.sale_date) <= trunc(i_sales_end_date)
                   and a.id_shop in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group)
             group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season) a
      
      group by id_shop, a.art, a.asize;
  
    commit;
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 28.02.2019 15:30:00
  -- Comments: блок расчета количества пар, которые будут оставлены в магазине, в зависимости от ограничения на хранение
  --
  --****************************************************************************************************************************************************
  procedure use_capacity_filter(i_s_group integer -- номер группы магазинов из таблицы tdm_map_new_shop_group
                                ) is
    v_capacity number:= 0;
    v_kol_ost number:= 0;
    v_kol_stay number:= 0;
  begin
    -- перебор отделений и их вместительности для оставляемых пар
    for r_shop in (select shopid id_shop, dtype capacity from tdm_map_new_shop_group where s_group = i_s_group)
    loop
      dbms_output.put_line('r_shop.id_shop: '||r_shop.id_shop||' '||r_shop.capacity);
      v_capacity := r_shop.capacity;
      
      for r_art in (
        select x.id_shop, x.art, x.asize, x.priority, y.kol_sale, x.kol_ost from t_map_stay_in_shop_kol x
        left join (
          select a.id_shop, b.art, a.priority, a.kol_sale from t_map_stay_in_shop a
          inner join tdv_map_new_anal b on a.art = b.analog
        ) y on x.id_shop = y.id_shop and x.art = y.art and x.priority = y.priority
        where x.id_shop = r_shop.id_shop and x.kol_ost > 0
        order by x.priority, y.kol_sale desc -- сортировка по номеру условия(приоритету) и количеству продаж этого артикула(???или нужно количество продаж по размеру артикула???)
      )
      loop
        v_kol_ost := r_art.kol_ost;
        dbms_output.put_line('r_art.art: '||r_art.art||' '||'r_art.asize: '||r_art.asize||' '||'v_kol_ost: '||v_kol_ost);
        
        if (v_capacity <= 0) then
          v_kol_stay := 0;
        elsif (v_capacity <= v_kol_ost) then
          v_kol_stay := v_capacity;
          v_capacity := 0;
          v_kol_ost := v_kol_ost - v_kol_stay;
        elsif (v_capacity > v_kol_ost) then
          v_kol_stay := v_kol_ost;
          v_kol_ost := 0;
          v_capacity := v_capacity - v_kol_stay;
        end if;
        
        dbms_output.put_line('r_art.art: '||r_art.art||' '||'r_art.asize: '||r_art.asize||' '||'v_kol_stay: '||v_kol_stay||' v_capacity: '||v_capacity);
        
  --      update t_map_stay_in_shop_kol set kol_stay = v_kol_stay /*, kol_ost = v_kol_ost*/
  --      where id_shop = r_shop.id_shop and art = r_art.art and asize = r_art.asize and priority = r_art.priority;
      end loop;
      v_capacity := 0;
    end loop;  
    null;
  end;
                            
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 28.02.2019 16:00:00
  -- Comments: заполнение t_map_new_poexalo арикулами, которые есть на остатках и не числятся в t_map_stay_in_shop
  --
  --****************************************************************************************************************************************************
  procedure fill_poexalo(i_id      integer, -- номер расчета
                         i_s_group integer -- номер группы магазинов из таблицы tdm_map_new_shop_group
                         ) is
  begin
    insert into t_map_new_poexalo
    (id_shop, art, season, kol, asize, flag, id_shop_from, id, timed, block_no)
    select 9999 id_shop, a.art, c.season, a.kol, a.asize, 0 flag, a.id_shop id_shop_from, i_id id,  systimestamp, 1 block_no
    from e_osttek_online a
    inner join tdm_map_new_shop_group d on a.id_shop = d.shopid and i_s_group = d.s_group
    left join (
      select a.id_shop, b.art from t_map_stay_in_shop a
      inner join tdv_map_new_anal b on a.art = b.analog
      group by a.id_shop, b.art 
    ) b on a.id_shop = b.id_shop and a.art = b.art
    inner join s_art c on a.art = c.art
    where b.art is null 
    and a.kol > 0 and d.season_out like '%' || c.season || '%'
    group by a.id_shop, a.art, c.season, a.kol, a.asize;
    
    commit;
  end;
  
    --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 28.02.2019 22:15:00
  -- Comments: заполнение t_map_new_poexalo арикулами, которые есть на остатках, числятся в t_map_stay_in_shop но не остаются из-за лимита хранения 
  --
  --****************************************************************************************************************************************************
  procedure fill_poexalo_staying_rest(i_id      integer, -- номер расчета
                                      i_s_group integer -- номер группы магазинов из таблицы tdm_map_new_shop_group
                                      ) is
  begin
    insert into t_map_new_poexalo
    (id_shop, art, season, kol, asize, flag, id_shop_from, id, timed, block_no)
    select 
    -- TODO добавить season к t_map_stay_in_shop_kol season и заменить параметр в запросе
    9999 id_shop, a.art, 'Зимняя' season/*c.season*/, kol_ost - kol_stay kol, a.asize, 0 flag, a.id_shop id_shop_from, i_id id,  systimestamp, 1 block_no
    from t_map_stay_in_shop_kol a 
    where kol_ost != 0 and kol_ost - kol_stay > 0;
    
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 10:05:11
  -- Comments: блок расчета обуви, которую нужно оставить в магазине по заданным условиям
  --
  --****************************************************************************************************************************************************
  procedure staying_art_count(i_id                  integer, -- номер расчета
                              i_s_group             integer, -- номер группы магазинов из таблицы tdm_map_new_shop_group
                              i_sales_start_date    date, -- дата начала периода продаж
                              i_sales_end_date      date, -- дата окончания периода продаж
                              i_stock_doc_no        integer, -- номер документа остатков
                              i_sale_pair_count     integer, -- минимальное кол-во проданных пар за период
                              i_ost_pair_count      integer, -- кол-во пар на остатках магазина
                              i_siberia_in          varchar2, -- признак, что нужно Сибирь расчитывать по отдельному алгоритму
                              i_sale_max_pair_count integer := 1000, --максимальное кол-во проданных пар за период
                              i_priority            integer -- приоритет артикулов. чем мешьше число, тем больше приоритет
                              ) is
  
    v_count integer;
    v_cent_size integer;
  begin

    -- перезаполняю таблицу остатков и продаж для распределения
    fillBaseData(i_s_group, i_sales_start_date, i_sales_end_date, i_stock_doc_no);
    commit;    
    
--    if i_siberia_in = 'F' then
--      delete from TDV_MAP_NEW_OST_SALE
--      where substr(id_shop, 1, 2) in ('36', '38', '39', '40');
--    end if;

    -- заполнение таблицы артикулов(аналогов), которые нужно оставить в магазине
    insert into t_map_stay_in_shop
    select a.id_shop, a.art, i_priority, sum(a.kol_sale) kol_sale from tdv_map_new_ost_sale a
    --where (a.id_shop, a.art) not in (select id_shop, art from t_map_stay_in_shop)
    group by a.id_shop, a.art
    having(sum(a.kol_sale) + sum(a.kol_ost) != 0) and sum(a.kol_sale) >= i_sale_pair_count and sum(a.kol_sale) <= i_sale_max_pair_count and sum(a.kol_ost) <= i_ost_pair_count;
    commit; 
    
  end;


  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 28.02.2019 16:40:00
  -- Comments: блок заполнения количества пар на остатках, которые доджны остаться в магазине
  --
  --****************************************************************************************************************************************************
  procedure staying_art_kol_ost_count is
  begin
      insert into t_map_stay_in_shop_kol (id_shop, art, asize, priority, kol_ost)
      select x.id_shop, x.art, y.asize, x.priority, sum(y.kol) from (
        select a.id_shop, b.art, min(priority) priority from t_map_stay_in_shop a
        inner join tdv_map_new_anal b on a.art = b.analog
        group by a.id_shop, b.art
      ) x inner join e_osttek_online y on x.id_shop = y.id_shop and x.art = y.art
      group by x.id_shop, x.art, y.asize, x.priority;
      
      commit; 
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.02.2019 09:34:42
  -- Comments: тип расчета 1
  --
  --****************************************************************************************************************************************************
  procedure go_count1(i_id      in integer,
                      i_s_group in integer) is
  begin
    -- 1) продажи >= 5, по 1 марта 2019 
    staying_art_count(i_id => i_id, i_s_group => i_s_group, 
                      i_sales_start_date => to_date('20180901', 'yyyymmdd'), 
                      i_sales_end_date => to_date('20190301', 'yyyymmdd'),
                      i_stock_doc_no => null, 
                      i_sale_pair_count => 5, 
                      i_ost_pair_count => 100, 
                      i_siberia_in => 'F',
                      i_priority => 1);
  
    -- 2) продажи = 4 на 1 фев 2019, остаток <= 1 на 1 фев 2019
    --staying_art_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 4, 1, 'F', 4);
    staying_art_count(i_id => i_id, i_s_group => i_s_group, 
                      i_sales_start_date => to_date('20180901', 'yyyymmdd'), 
                      i_sales_end_date => to_date('20190802', 'yyyymmdd'),
                      i_stock_doc_no => 822, 
                      i_sale_pair_count => 4, 
                      i_ost_pair_count => 1, 
                      i_siberia_in => 'F',
                      i_sale_max_pair_count => 4,
                      i_priority => 2);
--    -- 3) продажи = 3 на 1 фев 2019, остаток = 0 на 1 фев 2019
--    --staying_art_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 3, 0, 'F', 3);
    staying_art_count(i_id => i_id, i_s_group => i_s_group, 
                      i_sales_start_date => to_date('20180901', 'yyyymmdd'), 
                      i_sales_end_date => to_date('20190802', 'yyyymmdd'),
                      i_stock_doc_no => 822, 
                      i_sale_pair_count => 3, 
                      i_ost_pair_count => 0, 
                      i_siberia_in => 'F',
                      i_sale_max_pair_count => 3,
                      i_priority => 3);
--    -- 4) продажи >= 4 на сегодня 
--    --staying_art_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190301', 'yyyymmdd'), null, 4, 100, 'F');
    staying_art_count(i_id => i_id, i_s_group => i_s_group, 
                      i_sales_start_date => to_date('20180901', 'yyyymmdd'), 
                      i_sales_end_date => to_date('20190301', 'yyyymmdd'),
                      i_stock_doc_no => null, 
                      i_sale_pair_count => 4, 
                      i_ost_pair_count => 100, 
                      i_siberia_in => 'F',
                      i_priority => 4);
--    -- 5) фиолетовые
--    --purple_count(i_id, i_s_group, 'F');
--  
--    -- 6) продажи >= 2 на сегодня 
--    --staying_art_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190301', 'yyyymmdd'), null, 2, 100, 'F');
    staying_art_count(i_id => i_id, i_s_group => i_s_group, 
                      i_sales_start_date => to_date('20180901', 'yyyymmdd'), 
                      i_sales_end_date => to_date('20190301', 'yyyymmdd'),
                      i_stock_doc_no => null, 
                      i_sale_pair_count => 2, 
                      i_ost_pair_count => 100, 
                      i_siberia_in => 'F',
                      i_priority => 6);
                      
    -- заполнение таблицы артикулов с количеством на остатках по размерам
    staying_art_kol_ost_count();
      
    -- расчет количества оставляемых в зависимости от вместительности  магазина
    use_capacity_filter(i_s_group);
    
    -- вставка в t_map_new_poexalo арикулов, которые не числятся в t_map_stay_in_shop
    --fill_poexalo(i_id, i_s_group);  
    
    -- вставка в t_map_new_poexalo арикулов, которые числятся в t_map_stay_in_shop с учетом количества оставляемых пар из t_map_stay_in_shop_kol
    --fill_poexalo_staying_rest(i_id, i_s_group); 
    -- TODO исключить группы артикулов из ограничений  
      
    -- TODO ограничить количество вывозимых пар              
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.02.2019 9:43:19
  -- Comments: основная процедура пакета
  --
  --****************************************************************************************************************************************************
  procedure go(i_s_group         number default null, -- номер группы магазинов из таблицы tdm_map_new_shop_group
               i_no_data_refresh varchar2 default 'F' --если НЕ нужно  пересчитывать текущие остатки и продажи то 'T'
               ) is
  
    v_id number;
    v_time timestamp;
    v_list_city varchar2(1000 char);
    v_list_filial varchar2(1000 char);
    v_cluster_name varchar2(1000 char);
    v_package_name varchar2(1000 char);
  begin
    null;
  
    execute immediate 'truncate table t_map_stay_in_shop'; -- таблица с артикулами(аналогами), которые необходимо оставить в магазине
    execute immediate 'truncate table t_map_stay_in_shop_kol'; -- таблица с артикулами, которые необходимо оставить в магазине, их остатками и  количеством оставляемых 
  
    -- номер нового расчета
    v_id := map_fl.getNewCalculationID();
    dbms_output.put_line('номер расчета: ' || v_id);
  
    -- текущее время
    v_time := systimestamp;
  
    -- список городов
    v_list_city := map_fl.get_city_list(i_s_group, 1000);
    dbms_output.put_line('v_list_city: '||v_list_city);

    -- список филиалов
    v_list_filial := map_fl.get_filial_list(i_s_group, 1000);
    dbms_output.put_line('v_list_filial: '||v_list_filial);

    -- имя кластера
    v_cluster_name := map_fl.get_cluster_name(i_s_group);
    dbms_output.put_line('v_cluster_name: '||v_cluster_name);

    -- имя пакета
    v_package_name := map_fl.get_package_name(dbms_utility.format_call_stack);
    dbms_output.put_line('v_package_name: '||v_package_name);
    
  
    insert into TDV_MAP_13_RESULT
    values
      (v_id, v_time, null, i_s_group, null, null, null, null, v_list_city, v_list_filial, 1, v_time, null, 0,
       'in progress', v_cluster_name, USERENV('sessionid'), 1, v_package_name);
    commit;

    -- апдейт на случай, если магазины РБ начинаются на 7
    update tdm_map_new_shop_group a
    set a.shop_in = ',' || replace(rtrim(ltrim(replace(a.shop_in, ' ', ''), ','), ','), '70', '00') || ','
    where a.s_group = i_s_group
          and a.shop_in is not null;
  
    go_count1(i_id => v_id, i_s_group => i_s_group);
  end;
end bep_map_001;