select * from tdm_map_new_shop_group where s_group >= 36601 and s_group <= 36602 order by s_group desc;
select * from t_map003_svx_shop;


select distinct a.s_group
from tdm_map_new_shop_group a
inner join t_map003_svx_shop b on trim(upper(a.cluster_name)) = trim(upper(b.svx))
where a.s_group >= 36601 and a.s_group <= 36602;

select * from t_map_new_otbor;

-------------- остатки склада -----------------------
select id_shop, art, season, sum(kol) kol, asize, analog, 0 status_flag from (
    select a.id_shop,a.art,a.asize,a.scan, c.season, c.normt analog, sum(kol) kol 
    from e_osttek_online a 
    inner join s_art c on a.art = c.art
    where (a.id_shop, c.season) in (
        select distinct cluster_name id_shop, season_in 
        from tdm_map_new_shop_group where s_group = 36601
    ) 
    group by a.id_shop,a.art,a.asize,a.scan, c.season, c.normt having sum(kol) > 0
) group by id_shop, art, season, kol, asize, analog;

select id_shop, art, season, sum(kol) kol, asize, analog, 0 status_flag from (
    select a.id_shop,a.art,a.asize,a.scan, c.season, c.normt analog, sum(kol) kol 
    from e_osttek_online a 
    inner join s_art c on a.art = c.art
    where a.id_shop = 'W392' and c.season = 'Зимняя' 
    group by a.id_shop,a.art,a.asize,a.scan, c.season, c.normt having sum(kol) > 0
) group by id_shop, art, season, kol, asize, analog;
-----------------------------------------------------------------------------------

-------- остатки магазинов клдастера ---------------------------------------
select * from tdv_map_osttek;
select sum(kol) from(
select id_shop, art, asize, sum(kol) kol, season, 1 out_block, sum(kol_rc) kol_rc, sum(kol) kol1
from (
    select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
    from e_osttek_online a2
    inner join s_art c on a2.art = c.art
    left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
    where 
    a2.scan != ' '
    and a2.procent = 0
    and 
    (a2.id_shop, c.season) in (
        select distinct shopid, season_in
        from tdm_map_new_shop_group
        where s_group = 36601
    )
    and x.scan is null
    group by a2.id_shop, a2.art, a2.asize, c.season
    having sum(kol) > 0
) 
group by id_shop, art, asize, season);


-------------------------------------------------------------------------------

select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0 kol_ost, sum(b.kol) kol_sale, 0 kol_rc, 0 kol2
from pos_sale1 a
inner join pos_sale2 b on a.id_chek = b.id_chek
                                                 and a.id_shop = b.id_shop
inner join s_art c on b.art = c.art
left join tdv_map_new_anal d on b.art = d.art
inner join t_map_new_otbor e on nvl(d.analog, b.art) = e.analog
where a.bit_close = 'T'
and a.bit_vozvr = 'F'
and b.scan != ' '
and b.procent = 0
and (a.id_shop,c.season) in (
    select distinct shopid, season_in
    from tdm_map_new_shop_group
    where s_group = 36601
)
and trunc(a.sale_date) >= to_date('20180801', 'yyyymmdd')
and trunc(a.sale_date) <= to_date('20190301', 'yyyymmdd')
group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season;


-- инициализация исходных лимитов
insert into t_map003_shop_limit
    select shopid, count_limit_in
    from tdm_map_new_shop_group
    where s_group = 36602;
commit;

-------------------------------------------------------------------------------
------- проход по магазам и артикулам куда добрасывать
select x.analog, x.art, sum(x.kol) kol 
from t_map_new_otbor x
group by x.analog, x.art;

select * from (
    select  a.id_shop, a.art, c.groupmw, c.season, c.style, sum(kol_sale) kol_sale, l.current_limit, d.kol kol_otbor
    from TDV_MAP_NEW_OST_SALE a -- тут не должно быть отфильтрованной обуви и сибири
    inner join (
                select analog, sum(kol) kol 
                from t_map_new_otbor
                where kol > 0
                group by analog
    ) d on a.art = d.analog
    inner join tdm_map_new_shop_group x on x.s_group = 36601
                                            and a.id_shop = x.shopid
                                            and x.season_in like '%' || a.season || '%'
    inner join t_map003_shop_limit l on a.id_shop = l.id_shop
    left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                from s_art
                group by decode(normt, ' ', art, normt)
    ) c on c.normt = a.art
    left join (
                select id_shop, text1 analog
                from t_map_new_poexalo where id = 1489
                group by id_shop, text1 having sum(kol) > 0
    ) z  on a.id_shop = z.id_shop and a.art = z.analog   
    where kol_sale + kol_ost != 0
        and l.current_limit > 0
            and z.analog is null
    group by a.id_shop, a.art, d.art, c.groupmw, c.season, c.style, l.current_limit, d.kol
    having(sum(kol_sale) + sum(kol_ost) != 0)
) order by kol_sale desc;

------------------------------------------------------------------------------
-- можем ли докинуть в этот магазин

select distinct a.asize
from T_MAP_NEW_OTBOR a
left join (
        select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2
        from tdv_map_new_ost_sale
        where id_shop = '3913'--r_row.id_shop
             and art = '1813160'--r_row.art
        group by asize
) b on a.asize = b.asize
left join (
        select asize, sum(kol) kol
        from t_map_new_poexalo x
        where id_shop = '3913'--r_row.id_shop
           and x.text1 = '1813160'--r_row.art
           and id = 000
        group by asize
) c on a.asize = c.asize
where a.analog = '1813160'--r_row.art
       and a.kol > 0
       and nvl(b.kol_sale, 0) >= 0
       and nvl(b.kol_ost2, 0) + nvl(c.kol, 0) <= 0
--       and (r_row.id_shop, r_row.art, a.asize) not in
--       (select x.id_shop, x.art, x.asize
--            from TDV_NEW_SHOP_OST x
--            where x.verme > 0
--                  and x.art = r_row.art
--                  and id_shop = r_row.id_shop) -- которых нет в транзитах
order by a.asize
;


---------------------------------------------------------------------------------
--- ТРАНЗИТЫ

select * from tdv_new_shop_ost;

select a1.shopid, nvl(a2.analog, a1.art) art, to_number(asize), sum(a1.kol) kol, 'TRANS'
from trans_in_way a1
left join TDV_MAP_NEW_ANAL a2 on a1.art = a2.art
inner join s_art c on a1.art = c.art
where scan != ' '
and (a1.shopid, c.season) in (select distinct shopid, season_in
                                from tdm_map_new_shop_group
                                where s_group = 36606)
group by a1.shopid, nvl(a2.analog, a1.art), to_number(asize)
having sum(kol) > 0

-- добавляю резервы из САПА
union
select y.shopid, nvl(a2.analog, x3.art), to_number(asize, '999.99'), sum(x3.verme) kol, 'TRANS'
from tdv_map_new_sap_reserv x3
left join s_shop y on x3.kunnr = y.shopnum
left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
inner join s_art c on x3.art = c.art
where x3.boxty = ' '
and (y.shopid, c.season) in (select distinct shopid, season_in
                                from tdm_map_new_shop_group
                                where s_group = 36606)

and to_number(asize, '999.99') != 0 -- россыпь
and substr(nvl(kunnr, ' '), 1, 2) != 'X_'

and ((kunnr not in ('X_NERASP', 'X_SD_USE') and kunnr = 'X_FND_INET') 
                            or rasp != 'X')
                            
and verme > 0
and shopid is not null
and trans != 'X'
            
group by y.shopid, nvl(a2.analog, x3.art), to_number(asize, '999.99')

--
union
select y.shopid, nvl(a2.analog, a.art), to_number(b.asize), sum(b.kol) kol, 'TRANS'
from tdv_map_new_sap_reserv a
inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
inner join s_shop y on a.kunnr = y.shopnum
left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
inner join s_art c on a.art = c.art
where a.boxty != ' ' --короба
and (y.shopid, c.season) in (select distinct shopid, season_in
                                from tdm_map_new_shop_group
                                where s_group = 36606)
and verme > 0
--and pv_season like '%,' || c.season || ',%'

and substr(nvl(kunnr, ' '), 1, 2) != 'X_'

and (kunnr not in ('X_NERASP', 'X_SD_USE')
                        or rasp != 'X')

and trans != 'X'
group by y.shopid, nvl(a2.analog, a.art), to_number(b.asize)

union
select x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, to_number(x3.asize, '999.99')) asize,
                                sum(decode(x3.boxty, ' ', x3.verme, f.kol)) kol, 'TRANS'
from tdv_map_new_sap_reserv x3
left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
left join (select boxty, sum(kol) kol
                     from T_SAP_BOX_ROST
                     group by boxty) e on x3.boxty = e.boxty
left join s_shop x on x3.kunnr = x.shopnum
left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
inner join s_art c on x3.art = c.art
where --kunnr not in ('X_NERASP', 'X_SD_USE')
(kunnr not in ('X_NERASP', 'X_SD_USE')
                            or rasp != 'X')
                            
and trans = 'X'
and (x.shopid, c.season) in (select distinct shopid, season_in
                                from tdm_map_new_shop_group
                                where s_group = 36606)
and verme > 0
group by nvl(a2.analog, x3.art), nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid;


---------------------------------------------------------------------------------
-- РАЗМЕРЫ КОТОРЫЕ МОЖНО ДОКИНУТЬ
select distinct a.asize
from T_MAP_NEW_OTBOR a
left join s_art e on a.art = e.art
left join (
        select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol
        from tdv_map_new_ost_sale
        where id_shop = '3913'/*i_id_shop*/
                 and art = '1813160'/*i_art*/
        group by asize
) b on a.asize = b.asize
left join (
        select asize, sum(kol) kol
        from t_map_new_poexalo x
        where id_shop = '3913'/*i_id_shop*/
                 and x.text1 = '1813160'/*i_art*/
                 and id = 14041/*i_id*/
        group by asize
) c on a.asize = c.asize
left join (
        select x.asize, sum(x.verme) kol
        from TDV_NEW_SHOP_OST x
        where x.verme > 0
                 and x.art = '51813160'/*i_art*/
                 and id_shop = '3913'/*i_id_shop*/
        group by x.asize
) d on a.asize = d.asize

where a.analog = '1813160'/*i_art*/
and a.kol > 0
and nvl(b.kol_sale, 0) >= 0
and case
 when 'T' = 'T' or coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) = 0 then
    1
 else
    0
end = 1
order by a.asize;



select b.art, a.*
from T_MAP_NEW_OTBOR a
left join (
        select distinct x.art
        from tdv_map_osttek x
        where x.id_shop = '3913'/*i_id_shop*/
                 and art in (
                            select art
                            from TDV_MAP_NEW_ANAL
                            where analog = '524008/Б'/*i_art*/
                 )
                 and x.kol > 0
) b on a.art = b.art
where analog = '524008/Б'/*i_art*/
         and asize = 47/*r_size.asize*/
         and kol > 0
--                         and not (a.art like '%Р2%' and i_landid = 'RU') -- На РФ не везем артикула с Р2 
-- какие в первую очередь берем артикула:  
-- 1. артикул , который уже есть в магазине
-- 2. артикул , которого МЕНЬШЕ на складе  
order by case
    when b.art is not null then
     0
    else
     1
end, a.kol;

-------------------------------------------------------------------------------
-- доброс дублей размеров

select * from (
            select a.id_shop, a.art, c.groupmw, c.season, c.style, sum(kol_sale) kol_sale, sum(a.kol_ost) kol_ost, l.current_limit, d.kol kol_otbor, nvl(z.kol, 0) as kol_raspred_analog
            from TDV_MAP_NEW_OST_SALE a
            inner join (
                        select analog, art, sum(kol) kol 
                        from t_map_new_otbor
                        where kol > 0
                        group by analog, art
            ) d on a.art = d.analog
            inner join tdm_map_new_shop_group x on x.s_group = /*i_s_group*/ 36601
                                                    and a.id_shop = x.shopid
                                                    and x.season_in like '%' || a.season || '%'
            inner join t_map003_shop_limit l on a.id_shop = l.id_shop
            left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                        from s_art
                        group by decode(normt, ' ', art, normt)
            ) c on c.normt = a.art
            left join (
                        select id_shop, text1 analog, sum(kol) kol
                        from t_map_new_poexalo where id = /*i_id*/ 14089 
                        group by id_shop, text1 having sum(kol) > 0
            ) z  on a.id_shop = z.id_shop and a.art = z.analog   
            where kol_sale + kol_ost != 0
            group by a.id_shop, a.art, c.groupmw, c.season, c.style, l.current_limit, d.kol, z.kol
            having(sum(kol_sale) + sum(kol_ost) != 0)
) order by kol_raspred_analog desc, kol_sale desc;

--3913 524008/Б

select * from t_map_new_poexalo where id = 14041 and art = '1834171';
select * from t_map_new_otbor where art = '524008/Б';

select sum(kol) from t_map_new_otbor_before where id = 14078;