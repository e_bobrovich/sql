create or replace package body map_fl is
  /*
  map_functions_list
  набор общих, стандартных функций и процедур , которые обычно используются в
  пакетах MAP
  */

  pv_currentBlockNo integer;
  pv_currrentBlockStartTime number;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 06.04.2018 9:02:16
  -- Comments: проверяет можно ли выполнить данный блок
  --                  если можно, записывает данные по старту блока
  -- PUBLIC
  --****************************************************************************************************************************************************
  function blockStart(i_calculation_id t_map_new_poexalo.id%type,
                      i_block_no       in integer,
                      i_block_no_list  in varchar2 default null) return varchar2 is
    pragma autonomous_transaction;
  
    v_block_no_list varchar2(100 char);
  begin
    /* входной параметр i_block_no_list может содержать
    перечисление блоков, которые будут выполнены в программе,
    например '1,4,5'
    возможно также указание запрета выполнение блока например
    '!5,!10' -выполнить все блоки,  кроме 5 и 10
    */
  
    if i_block_no_list is not null then
      v_block_no_list := ',' || i_block_no_list || ',';
    end if;
  
    if not ((v_block_no_list is null or v_block_no_list like '%,' || i_block_no || ',%' or v_block_no_list like '%!%') and
        nvl(v_block_no_list, 'X') not like '%,!' || i_block_no || ',%') then
      return 'F';
    end if;
  
    pv_currentBlockNo := i_block_no;
    pv_currrentBlockStartTime := dbms_utility.get_time;
  
    update TDV_MAP_13_RESULT
    set ACTIVE_BLOCK = i_block_no, TIME_START_BLOCK = systimestamp
    where id = i_calculation_id;
    commit;
  
    return 'T';
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 06.04.2018 9:10:08
  -- Comments: завершает выполнение блока , фиксируя время
  --
  -- PUBLIC
  --****************************************************************************************************************************************************
  procedure blockEnd(i_calculation_id t_map_new_poexalo.id%type,
                     i_block_text     varchar2) is
  begin
  
    if pv_currentBlockNo is null then
      return;
    end if;
  
    writeLog('Block: ' || pv_currentBlockNo || '. ' || i_block_text,
             (dbms_utility.get_time - pv_currrentBlockStartTime) / 100, i_calculation_id);
    pv_currentBlockNo := null;
    pv_currrentBlockStartTime := null;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 01.04.2018 12:25:43
  -- Comments: возвращает список со всеми магазинами
  --          закрытые не фильтрую, потому что и по ним может быть нужен расчет
  -- PRIVATE
  --****************************************************************************************************************************************************
  function getAllShopsList return t_shop_array is
    v_shop_list t_shop_array;
  begin
    for r_shop in (select shopid, rownum rn
                   from st_shop a
                   where a.org_kod = 'SHP')
    loop
      v_shop_list(r_shop.rn) := r_shop.shopid;
    end loop;
  
    return v_shop_list;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 04.04.2018 21:26:40
  -- Comments: проверяет что список магазинов не пустой,
  --            если пустой, то заполняет всеми магазинами
  --            возвращает список обратно
  --  PRIVATE
  --****************************************************************************************************************************************************
  function getCorrectShopsList(i_shop_list in t_shop_array) return t_shop_array is
    v_shop_list t_shop_array;
  begin
    v_shop_list := i_shop_list;
  
    if v_shop_list is null or v_shop_list.count = 0 then
      v_shop_list := getAllShopsList();
    end if;
  
    return v_shop_list;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 01.04.2018 12:30:13
  -- Comments: очищает заданную таблицу
  -- PRIVATE
  --****************************************************************************************************************************************************
  procedure clearTable(i_table_name varchar2) is
  begin
    --execute immediate 'truncate table '||i_table_name;
    execute immediate 'delete from ' || i_table_name;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: возвращает номер расчета остатков магазинов на X месяцев назад
  -- PUBLIC
  --****************************************************************************************************************************************************
  function getStockCalculationID(i_stock_month_ago in integer) return e_oborot1.id%type is
    v_id e_oborot1.id%type;
  begin
    /* в каждом расчете e_oborot для МАП , в поле text
    записывается следующий тег: $MAP#30#
    число 30 обозначает - на сколько дней назад был сделан расчет.
    т.е. если расчет на три месяца назад, тег будет: $MAP#90#
    
    обработку no_data_found сюда не ставлю
    потому что нужно знать , что данных не было,
    а не продолжать расчет без данных
    */
  
    select id
    into v_id
    from e_oborot1
    where text like '%$MAP#%' || to_char(i_stock_month_ago * 30) || '%'
          and date_r = (select max(date_r)
                        from e_oborot1
                        where text like '%$MAP#%' || to_char(i_stock_month_ago * 30) || '%');
  
    return v_id;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 06.04.2018 9:06:04
  -- Comments: сохранение в лог
  -- PUBLIC
  --****************************************************************************************************************************************************
  procedure writeLog(i_text varchar2,
                     i_proc varchar2,
                     i_id   in integer) is
  begin
    write_log(i_text, i_proc, i_id);
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 01.04.2018 10:17:04
  -- Comments: заполняет таблицу tdv_map_oborot артикулами , которые были в магазине X
  --           месяцев назад.
  --          Список магазинов передается вторым параметром, если пустой -то для всех магазинов
  -- PUBLIC
  -- table: tem_mapif_stock_month_ago
  --****************************************************************************************************************************************************
  procedure fillStockMonthAgo(i_stock_month_ago in integer,
                              i_shop_list       in t_shop_array default cast(null as t_shop_array)) is
  
    v_stock_calculation_id e_oborot1.id%type;
    v_shop_list t_shop_array;
  begin
  
    clearTable('tem_mapif_stock_month_ago');
  
    if i_stock_month_ago is null then
      return;
    end if;
  
    v_stock_calculation_id := getStockCalculationID(i_stock_month_ago);
  
    v_shop_list := getCorrectShopsList(i_shop_list);
  
    forall shopnum in indices of v_shop_list
      insert into tem_mapif_stock_month_ago
        select z.shopid, z.art, nvl(an.analog, z.art), sum(z.oste)
        from e_oborot2 z
        left join TDV_MAP_NEW_ANAL an on an.art = z.art
        where z.oste > 0
              and z.id = v_stock_calculation_id
              and z.shopid = v_shop_list(shopnum)
        group by z.shopid, z.art, an.analog;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 01.04.2018 12:15:00
  -- Comments: заполняю таблицу продаж на заданную дату
  --          Список магазинов передается вторым параметром, если пустой -то для всех магазинов
  -- PUBLIC
  -- table: tem_mapif_shop_sales
  --****************************************************************************************************************************************************
  procedure fillSales(i_date      date,
                      i_shop_list in t_shop_array default cast(null as t_shop_array)) is
  
    v_shop_list t_shop_array;
  begin
    clearTable('tem_mapif_shop_sales');
  
    if i_date is null then
      return;
    end if;
  
    v_shop_list := getCorrectShopsList(i_shop_list);
  
    forall shopnum in indices of v_shop_list
      insert into tem_mapif_shop_sales
        select a.id_shop, b.art, b.asize, nvl(d.analog, b.art), sum(b.kol)
        from pos_sale1 a
        inner join pos_sale2 b on a.id_chek = b.id_chek
                                  and a.id_shop = b.id_shop
        left join TDV_MAP_NEW_ANAL d on b.art = d.art
        where a.bit_close = 'T'
              and a.bit_vozvr = 'F'
              and b.scan != ' '
              and a.id_shop = v_shop_list(shopnum)
              and a.sale_date >= trunc(i_date)
        group by a.id_shop, d.analog, b.art, b.asize;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 04.04.2018 21:22:31
  -- Comments: формирование текущих остатков по заданным магазинам
  --            Список магазинов передается  параметром, если пустой -то для всех магазинов
  -- PUBLIC
  -- table: tem_mapif_shop_current_stock
  --****************************************************************************************************************************************************
  procedure fillCurrentStock(i_shop_list in t_shop_array default cast(null as t_shop_array)) is
    v_shop_list t_shop_array;
  begin
    clearTable('tem_mapif_shop_current_stock');
  
    v_shop_list := getCorrectShopsList(i_shop_list);
  
    forall shopnum in indices of v_shop_list
      insert into tem_mapif_shop_current_stock
        select a.id_shop, a.art, a.asize, nvl(b.analog, a.art) analog, sum(kol) kol
        from e_osttek a
        left join TDV_MAP_NEW_ANAL b on a.art = b.art
        left join pos_brak x on a.scan = x.scan
                                and a.id_shop = x.id_shop
        where a.id_shop = v_shop_list(shopnum)
              and a.scan != ' '
              and a.procent = 0
              and x.scan is null
        group by a.id_shop, b.analog, a.art, a.asize
        having sum(kol) > 0;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 28.02.2018 9:30:34
  -- Comments:
  -- Заполняет таблицу данными с РЦ и СВХ
  --****************************************************************************************************************************************************
  procedure fillCurrentRCStock(i_shop_list in t_shop_array default cast(null as t_shop_array)) is
    v_shop_list t_shop_array;
  begin
  
    clearTable('tem_mapif_rc_current_stock');
  
    v_shop_list := getCorrectShopsList(i_shop_list);
  
    forall shopnum in indices of v_shop_list
      insert into tem_mapif_rc_current_stock
        select skladid, shopid id_shop, art, asize, count(*) kol
        from sklad_ost_owners
        where shopid not in ('0', ' ')
              and shopid = v_shop_list(shopnum)
        group by skladid, shopid, art, asize
        
        union all
        
        select DCID skladid, owner id_shop, art, asize, sum(kol) kol
        from dist_center.e_osttek
        where owner = v_shop_list(shopnum)
        group by DCID, owner, art, asize
        having sum(kol) > 0;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 04.04.2018 21:42:10
  -- Comments: заполнение таблицы товаров в пути для каждого магазина
  --            Список магазинов передается  параметром, если пустой -то для всех магазинов
  -- PUBLIC
  -- table: tem_mapif_shoes_in_transit
  --****************************************************************************************************************************************************
  procedure fillShoesInTransit(i_shop_list in t_shop_array default cast(null as t_shop_array)) is
    v_shop_list t_shop_array;
  begin
    clearTable('tem_mapif_shoes_in_transit');
  
    v_shop_list := getCorrectShopsList(i_shop_list);
  
    forall shopnum in indices of v_shop_list
      insert into tem_mapif_shoes_in_transit
        select a.shopid, a.art, a.asize, nvl(b.analog, a.art) analog, sum(kol) kol
        from trans_in_way a -- товары в пути
        left join TDV_MAP_NEW_ANAL b on a.art = b.art
        where scan != ' '
              and a.shopid = v_shop_list(shopnum)
        group by a.shopid, b.analog, a.art, asize
        having sum(kol) > 0
        
        union
        
        select b.shopid, a.art, a.asize, nvl(c.analog, a.art) analog, sum(a.menge)
        from ST_SAP_DLVRY a -- то что лежит на СГП, зарезервированное за магазином, но еще не попало в ПОС
        inner join s_shop b on a.kunnr = b.shopnum
        left join TDV_MAP_NEW_ANAL c on a.art = c.art
        where b.shopid = v_shop_list(shopnum)
        group by b.shopid, a.art, a.asize, c.analog
        having sum(a.menge) > 0;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 04.04.2018 22:43:50
  -- Comments: Возвращает номер нового расчета в t_map_new_poexalo
  -- PUBLIC
  --****************************************************************************************************************************************************
  function getNewCalculationID return number is
    --v_id t_map_new_poexalo.id%type;
  begin
    /*    select nvl(max(id), 1) + 1
    into v_id
    from t_map_new_poexalo;*/
  
    return MAP_CALCULATE_ID.Nextval;
  
    --return v_id;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 11.07.2018 9:08:43
  -- Comments: заполняет таблицу с остатками склада ГП доступными для распределения
  --
  --****************************************************************************************************************************************************
  procedure fillWarehouseStock is
  begin
    writeLog('начинаем формировать таблицу остатков СГП ', 'map_fl', 0);
  
    clearTable('tem_mapif_fill_warehouse_stock');
  
    -- создаю копии таблиц с остатками и резервами САПа перед расчетом {
    clearTable('st_sap_ob_ost_copy');
    clearTable('tdv_map_new_sap_reserv_copy');
  
    insert into st_sap_ob_ost_copy
      select *
      from st_sap_ob_ost;
  
    insert into tdv_map_new_sap_reserv_copy
      select *
      from tdv_map_new_sap_reserv;
  
    commit;
    -- }
  
    /*    -- записываю короба к распределению
    insert into t_all_rasp_box
      select d.art, d.boxty, sum(d.verme) / f.kol, 0 flag, e.season, nvl(g.analog, d.art) analog
      from st_sap_ob_ost d
      left join s_art e on d.art = e.art
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                 group by boxty) f on d.boxty = f.boxty
      left join TDV_MAP_NEW_ANAL g on d.art = g.art
      where d.boxty not in (' ', 'N')
      --            and f.kol > 3 -- не берем к распределению короба, с кол-ом пар три и меньше
      group by d.art, d.boxty, f.kol, e.season, nvl(g.analog, d.art);
    
    -- от коробов отнимаю, что в резерве САПА
    update t_all_rasp_box x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, boxty, sum(verme) verme
                 from tdv_map_new_sap_reserv x3
                 where x3.trans != 'X'
                       and x3.boxty != ' '
                       and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
                 group by art, boxty) x2
          where x1.art = x2.art
                and x1.boxty = x2.boxty)
    where exists (select x1.kol - x2.verme
           from (select art, boxty, sum(verme) verme
                  from tdv_map_new_sap_reserv x3
                  where x3.trans != 'X'
                        and x3.boxty != ' '
                        and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
                  group by art, boxty) x2
           where x1.art = x2.art
                 and x1.boxty = x2.boxty);
    
    -- отнимаю еще и то, что распределилось ночью в tdv_map_11 {
    -- 1. фонды
    update t_all_rasp_box x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, boxty, sum(verme) verme
                 from TDV_MAP_NEW_RESRV
                 group by art, boxty) x2
          where x1.art = x2.art
                and x1.boxty = x2.boxty)
    where exists (select x1.kol - x2.verme
           from (select art, boxty, sum(verme) verme
                  from TDV_MAP_NEW_RESRV
                  group by art, boxty) x2
           where x1.art = x2.art
                 and x1.boxty = x2.boxty);
    
    -- 2. резерв магазинов
    update t_all_rasp_box x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, boxty, sum(verme) verme
                 from tdv_map_new_result
                 group by art, boxty) x2
          where x1.art = x2.art
                and x1.boxty = x2.boxty)
    where exists (select x1.kol - x2.verme
           from (select art, boxty, sum(verme) verme
                  from tdv_map_new_result
                  group by art, boxty) x2
           where x1.art = x2.art
                 and x1.boxty = x2.boxty);
    -- }
    
    delete from t_all_rasp_box x
    where x.kol <= 0;
    
    commit;
    -- }*/
  
    -- записываю россыпь к распределению {
    insert into tem_mapif_fill_warehouse_stock
      select a.art, d.asize, sum(a.verme) kol, 0 flag, nvl(c.analog, a.art) analog
      from st_sap_ob_ost a
      left join s_art b on a.art = b.art
      left join TDV_MAP_NEW_ANAL c on a.art = c.art
      left join s_all_mat d on a.matnr = d.matnr
      where d.asize != 0
            and a.boxty = ' '
      group by a.art, d.asize, nvl(c.analog, a.art);
    --}
  
    -- от россыпи отнимаю, что в резерве
    update tem_mapif_fill_warehouse_stock x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, asize, sum(verme) verme
                 from tdv_map_new_sap_reserv x3
                 where x3.boxty = ' '
                       and asize != 0 -- россыпь
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE')
                 group by art, asize) x2
          where x1.art = x2.art
                and x1.asize = x2.asize)
    where exists (select x2.verme
           from (select art, asize, sum(verme) verme
                  from tdv_map_new_sap_reserv x3
                  where x3.boxty = ' '
                        and asize != 0
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE')
                  group by art, asize) x2
           where x1.art = x2.art
                 and x1.asize = x2.asize);
  
    delete from tem_mapif_fill_warehouse_stock x
    where x.kol <= 0;
    commit;
    -- }
  
    writeLog('закончили формировать таблицу остатков СГП ', 'map_fl', 0);
  
    commit;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.02.2019 9:15:57
  -- Comments: возвращает список городов по группе магазинов из tdm_map_new_shop_group
  --
  --****************************************************************************************************************************************************
  function get_city_list(i_s_group      in number,
                         max_char_count in number default null) return varchar2 is
    v_list_city varchar2(3000);
  begin
    for r_row in (select distinct a.cityname
                  from s_shop a
                  where a.shopid in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group))
    loop
      if length(v_list_city || ', ' || r_row.cityname) < 3000 then
        v_list_city := v_list_city || ', ' || r_row.cityname;
      end if;
    end loop;
  
    if max_char_count is not null then
      v_list_city := substr(ltrim(v_list_city, ','), 1, least(max_char_count, length(v_list_city)));
    end if;
  
    return v_list_city;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 26.02.2019 11:15:57
  -- Comments: возвращает список филиалов по группе магазинов из tdm_map_new_shop_group
  --
  --****************************************************************************************************************************************************
  function get_filial_list(i_s_group      in number,
                           max_char_count in number default null) return varchar2 is
    v_list_filial varchar2(3000 char);
  begin
    for r_row in (select distinct substr(shopid, 1, 2) fil
                  from tdm_map_new_shop_group
                  where s_group = i_s_group)
    loop
      begin
        if length(v_list_filial || ', ' || r_row.fil) < 3000 then
          v_list_filial := v_list_filial || ', ' || r_row.fil;
        end if;
      exception
        when others then
          v_list_filial := v_list_filial || '';
      end;
    end loop;
  
    if max_char_count is not null then
      v_list_filial := substr(ltrim(v_list_filial, ','), 1, least(max_char_count, length(v_list_filial)));
    end if;
  
    v_list_filial := ltrim(v_list_filial, ',');
  
    return v_list_filial;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 26.02.2019 11:30:57
  -- Comments: возвращает имя кластера по группе магазинов из tdm_map_new_shop_group
  --
  --****************************************************************************************************************************************************
  function get_cluster_name(i_s_group in number) return varchar2 is
    v_cluster_name tdm_map_new_shop_group.cluster_name%type;
  begin
    select max(a.cluster_name)
    into v_cluster_name
    from tdm_map_new_shop_group a
    where a.s_group = i_s_group;
  
    return v_cluster_name;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 26.02.2019 12:00:00
  -- Comments: возвращает имя пакета, из которого был получен стек вызова dbms_utility.format_call_stack
  --
  --****************************************************************************************************************************************************
  function get_package_name(i_call_stack in varchar2) return varchar2 is
    v_package_name varchar2(3000 char);
  begin
    v_package_name := i_call_stack;
    v_package_name := substr(v_package_name, instr(v_package_name, chr(10), 1, 3));
    v_package_name := substr(v_package_name,
                             greatest(
                                       --                                    instr(v_package_name,'function'),
                                       --                                    instr(v_package_name,'procedure'),
                                       instr(v_package_name, 'package')));
    v_package_name := substr(v_package_name, 1, instr(v_package_name, chr(10)) - 1);
    v_package_name := substr(v_package_name, instr(v_package_name, ' ', -1) + 1);
    return v_package_name;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 10.04.2018 23:13:09
  -- Comments:
  --
  --****************************************************************************************************************************************************
  procedure displayTestInConsole(i_map_package_name in varchar2,
                                 i_test_no          in number,
                                 i_test_result      in varchar2, -- 'F' or 'T'
                                 i_text             in varchar2,
                                 i_show_type        in number default 1) is
  begin
    if i_show_type = 1 or (i_show_type = 2 and i_test_result = 'F') then
      dbms_output.put_line('*** ' || i_map_package_name || ' ***');
      dbms_output.put_line('** testNo: ' || i_test_no || ' **');
      dbms_output.put_line('* ' || i_text || ' *');
      dbms_output.put_line('result: ' || i_test_result);
    end if;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 10.04.2018 23:01:27
  -- Comments: блок тестов
  -- PUBLIC
  --****************************************************************************************************************************************************
  procedure test(i_id               in number, -- номер расчета
                 i_map_package_name in varchar2, -- имя пакета который вызвал тест
                 i_test_no          in number, --номер теста
                 i_show_type        in number default 1, -- режим отображения в консоли:
                 -- 1- отображать всё,
                 -- 2- только провальные тесты,
                 -- 3- ничего
                 i_param1 in varchar2 default null, -- два параметра , если нужно передать что-то еще
                 i_param2 in varchar2 default null) is
    pragma autonomous_transaction;
    v_count integer;
    v_test_no number default 0;
    v_start_time timestamp;
    v_test_text varchar2(1000);
    v_test_result varchar2(10) default 'FAILED';
  begin
    /*
    TODO: owner="asus" category="Optimize" priority="2 - Medium" created="10.04.2018"
    text="нужно сделать, что бы параметр i_test_no был массивом"
    */
    --********************
    -- test1
    --********************
    v_test_no := 1;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      v_test_text := 'проверяем, что расчет не взял себе больше коробов, чем было доступно на остатках САП';
    
      select count(*)
      into v_count
      from (select *
             from (select sum(d.kol) / f.kol kol, art, d.boxty
                    from t_map_new_poexalo d
                    left join (select boxty, sum(kol) kol
                              from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                              group by boxty) f on d.boxty = f.boxty
                    where d.boxty is not null
                          and d.boxty != ' '
                          and id = i_id
                    group by art, d.boxty, f.kol) a
             inner join (select d.art, d.boxty, sum(d.verme) / f.kol kol
                        from st_sap_ob_ost d
                        left join s_art e on d.art = e.art
                        left join (select boxty, sum(kol) kol
                                  from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                                  group by boxty) f on d.boxty = f.boxty
                        where d.boxty not in (' ', 'N')
                        group by d.art, d.boxty, f.kol) b on a.art = b.art
                                                             and a.boxty = b.boxty
             where a.kol > b.kol);
    
      v_test_result := case
                         when v_count = 0 then
                          'OK'
                         else
                          'FAILED'
                       end;
      insert into RI_TEST_RESULT
      values
        (i_id, i_map_package_name, v_start_time, systimestamp, v_test_no, v_test_result, v_test_text, null, null);
    
      commit;
      displayTestInConsole(i_map_package_name, i_test_no, v_test_result, v_test_text, i_show_type);
    end if;
  
    --********************
    -- test2
    --********************
    v_test_no := 2;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      v_test_text := 'проверяем, что расчет не взял себе больше коробов, чем было доступно на остатках, минус резерв';
    
      select count(*)
      into v_count
      from (select *
             from (select sum(d.kol) / f.kol kol, art, d.boxty
                    from t_map_new_poexalo d
                    left join (select boxty, sum(kol) kol
                              from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                              group by boxty) f on d.boxty = f.boxty
                    where d.boxty is not null
                          and d.boxty != ' '
                          and id = i_id
                    group by art, d.boxty, f.kol) a
             inner join (select d.art, d.boxty, sum(d.verme) / f.kol kol
                        from st_sap_ob_ost d
                        left join s_art e on d.art = e.art
                        left join (select boxty, sum(kol) kol
                                  from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                                  group by boxty) f on d.boxty = f.boxty
                        where d.boxty not in (' ', 'N')
                        group by d.art, d.boxty, f.kol) b on a.art = b.art
                                                             and a.boxty = b.boxty
             where a.kol > b.kol);
    
      v_test_result := case
                         when v_count = 0 then
                          'OK'
                         else
                          'FAILED'
                       end;
      insert into RI_TEST_RESULT
      values
        (i_id, i_map_package_name, v_start_time, systimestamp, v_test_no, v_test_result, v_test_text, null, null);
      commit;
      displayTestInConsole(i_map_package_name, i_test_no, v_test_result, v_test_text, i_show_type);
    end if;
  
    --********************
    -- test 3
    --********************
    v_test_no := 3;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      v_test_text := 'артикула с "Р2" не едут в магазины РФ';
    
      select count(*)
      into v_count
      from t_map_new_poexalo a
      left join s_shop b on a.id_shop = b.shopid
      where b.landid = 'RU'
            and a.art like '%Р2%'
            and id = i_id;
    
      v_test_result := case
                         when v_count = 0 then
                          'OK'
                         else
                          'FAILED'
                       end;
      insert into RI_TEST_RESULT
      values
        (i_id, i_map_package_name, v_start_time, systimestamp, v_test_no, v_test_result, v_test_text, null, null);
      commit;
      displayTestInConsole(i_map_package_name, i_test_no, v_test_result, v_test_text, i_show_type);
    end if;
  
    --********************
    -- test 4
    --********************
    v_test_no := 4;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      v_test_text := 'артикул/размер, который есть в транзитах, не едет в магазин';
    
      select count(*)
      into v_count
      from t_map_new_poexalo a
      left join TDV_NEW_SHOP_OST b on a.id_shop = b.id_shop
                                      and a.art = b.art
                                      and a.asize = b.asize
      where id = i_id
            and b.id_shop is not null;
    
      v_test_result := case
                         when v_count = 0 then
                          'OK'
                         else
                          'FAILED'
                       end;
      insert into RI_TEST_RESULT
      values
        (i_id, i_map_package_name, v_start_time, systimestamp, v_test_no, v_test_result, v_test_text, null, null);
      commit;
      displayTestInConsole(i_map_package_name, i_test_no, v_test_result, v_test_text, i_show_type);
    end if;
  
    --********************
    -- test 5
    --********************
    v_test_no := 5;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      v_test_text := 'в таблице t_map_cluster нет магазинов с первой цифрой 7';
    
      select count(*)
      into v_count
      from t_map_cluster a
      where substr(a.id_shop, 1, 1) = '7';
    
      v_test_result := case
                         when v_count = 0 then
                          'OK'
                         else
                          'FAILED'
                       end;
      insert into RI_TEST_RESULT
      values
        (i_id, i_map_package_name, v_start_time, systimestamp, v_test_no, v_test_result, v_test_text, null, null);
      commit;
      displayTestInConsole(i_map_package_name, i_test_no, v_test_result, v_test_text, i_show_type);
    end if;
  
    --********************
    -- test 6
    --********************
    v_test_no := 6;
    if i_test_no is null or v_test_no = i_test_no then
      --i_param1  - в этом параметре нужно передать s_group
      v_start_time := systimestamp;
      v_test_text := 'проверка, что из магазинов не вывозим больше обуви, чем проставлено в tdm_map_new_shop_group.count_limit_out';
    
      select count(*)
      into v_count
      from (select id_shop, sum(kol) kol
             from t_map_new_poexalo a
             where a.id = i_id
             group by id_shop) x
      left join (select shopid id_shop, count_limit_out
                 from tdm_map_new_shop_group
                 where s_group = i_param1) y on x.id_shop = y.id_shop
      where x.kol > y.count_limit_out;
    
      v_test_result := case
                         when v_count = 0 then
                          'OK'
                         else
                          'FAILED'
                       end;
      insert into RI_TEST_RESULT
      values
        (i_id, i_map_package_name, v_start_time, systimestamp, v_test_no, v_test_result, v_test_text, null, null);
      commit;
      displayTestInConsole(i_map_package_name, i_test_no, v_test_result, v_test_text, i_show_type);
    end if;
  
  end;

end map_fl;
