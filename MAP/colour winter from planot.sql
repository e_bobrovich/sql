select case when d.id is not null then 'ВЫПОЛНЕНО' else null end compl,
a.rel,d.id,d.dated dated_r,a.dated,a.kpodr,substr(c.shopnum,2) shopnum,c.cityname,c.shopname,a.kkl,a.nkl,e.season,sum(b.kol) kol, nvl(f.kolf,0) kolf
from d_planot1 a
inner join d_planot2 b on a.rel = b.rel and a.kpodr = b.kpodr
inner join s_art e on e.art = b.art
inner join s_shop c on a.kpodr = c.shopid
left join d_rasxod1 d on d.id_shop = a.kpodr and d.rel_planot = a.rel and d.bit_close = 'T'
left join (select id,id_shop,season,sum(kol) kolf from d_rasxod2 x inner join s_art y on x.art = y.art group by id,id_shop,season) f on f.id = d.id and f.id_shop = d.id_shop and f.season = e.season
where to_char(a.dated,'yyyymmdd') >= '20190301' and to_char(a.dated,'yyyymmdd') <= '20190331'
group by case when d.id is not null then 'ВЫПОЛНЕНО' else null end,a.rel,d.id,d.dated,a.dated,a.kpodr,substr(c.shopnum,2),c.cityname,c.shopname,a.kkl,a.nkl,e.season,nvl(f.kolf,0)
order by a.kpodr,a.rel;


select distinct substr(shopnum,2,5) from (
    select 
     g.matnr, c.shopnum --, sum(f.kol)
    --c.shopnum, a.kpodr, a.rel, d.id, f.art, f.asize, g.matnr 
    from d_planot1 a 
    inner join s_shop c on a.kpodr = c.shopid
    left join d_rasxod1 d on d.id_shop = a.kpodr and d.rel_planot = a.rel and d.bit_close = 'T'
    left join d_rasxod2 f on f.id = d.id and f.id_shop = d.id_shop
    inner join s_all_mat g on f.art = g.art and f.asize = g.asize
    where to_char(a.dated,'yyyymmdd') >= '20190301' and to_char(a.dated,'yyyymmdd') <= '20190430'
    --and substr(a.kpodr,1,2) in ('36', '38', '39', '40')
    and substr(c.shopnum,2,5) in (select a from tem_bep_otgr_shop)
    and a.text = 'Зимняя'
    group by c.shopnum, g.matnr
) order by substr(shopnum,2,5)
;

select * from d_planot1 a 
inner join s_shop c on a.kpodr = c.shopid
where to_char(a.dated,'yyyymmdd') >= '20190301' and to_char(a.dated,'yyyymmdd') <= '20190430'  and substr(c.shopnum,2,5) in (select a from tem_bep_otgr_shop)