function get_city_list(i_s_group      in number,
                         max_char_count in number default null) return varchar2 is
    v_list_city varchar2(3000) := '';
  begin
    for r_row in (select distinct a.cityname
                  from s_shop a
                  where a.shopid in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group))
    loop
      if length(v_list_city || ', ' || r_row.cityname) < 3000 then
        --dbms_output.put_line(length(v_list_city || ', ' || r_row.cityname) || ' '||length(v_list_city)||'+'||length(', ')||'+'||length(r_row.cityname));
        v_list_city := v_list_city || ', ' || r_row.cityname;
      end if;
    end loop;
    
    if max_char_count is not null then
      v_list_city := substr(ltrim(v_list_city, ','), 1, least(max_char_count, length(v_list_city)));
    end if;
  
    return v_list_city;
  end;