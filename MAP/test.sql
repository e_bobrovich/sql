select count(*) from (
  select id_shop, sum(kol) kol from t_map_new_poexalo a
  where a.id = 8900
  group by id_shop
) x
left join (
  select shopid id_shop, count_limit_out from tdm_map_new_shop_group
  where s_group = 15902
) y on x.id_shop = y.id_shop
where x.kol > y.count_limit_out;


--проверка, что к вывозу не попала детская обувь
select count(*) from t_map_new_poexalo a
where a.id = 8900 and art not in (select art from s_art where groupmw in ('Мужские', 'Женские'));


select count(*)
from t_map_new_poexalo a
inner join s_art b on a.art = b.art
where a.id = 8900 and groupmw not in ('Мужские', 'Женские');

--Проверка , что вывозим только зимнюю обувь
select count(*) from t_map_new_poexalo a
inner join s_art b on a.art = b.art
where a.id = 8900 and b.season != 'Зимняя';


select shopid, season_out from tdm_map_new_shop_group where s_group = 15902;

select * from cena_skidki_all_shops where substr(to_char(cena), -2) = '99' and substr(id_shop, 1, 2) in ('36', '38', '39', '40') ; 

--Проверка, что в магазине не остается больше зимней обуви, чем он может вместить
select x.id_shop,x.dtype, y.ost_kol, z.away_kol, nvl(y.ost_kol,0)-nvl(z.away_kol,0) difference,
case when nvl(y.ost_kol,0)-nvl(z.away_kol,0) <= x.dtype then 'OK' else 'FAILED' end result
from(
  select shopid id_shop, dtype from tdm_map_new_shop_group
  where s_group = 15901
) x
left join (
  select id_shop, sum(kol) ost_kol from (
    select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
    from e_osttek_online a2
    inner join s_art c on a2.art = c.art
    left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
    left join st_muya_art z on a2.art = z.art
    left join (select id_shop, art from cena_skidki_all_shops where substr(to_char(cena), -2) = '99') s on a2.id_shop = s.id_shop and a2.art = s.art
    where a2.scan != ' '
         and a2.id_shop in (select shopid
                            from tdm_map_new_shop_group
                            where s_group = 15901)
         and a2.id_shop in (select shopid
                            from st_shop z
                            where z.org_kod = 'SHP')
         and a2.procent = 0 -- исключение некондиции
         and x.scan is null -- исключение брака
         and z.art is null -- исключение покупной
         and c.groupmw in ('Мужские', 'Женские') -- исключение детской
         and c.season = 'Зимняя'
         and case when substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') 
         then s.art else null end is null
    group by a2.id_shop, a2.art, a2.asize, c.season
    having sum(kol) > 0
  ) group by id_shop
) y on x.id_shop = y.id_shop
left join (
  select id_shop_from id_shop, sum(kol) away_kol  from t_map_new_poexalo where id = 9015 and season = 'Зимняя' group by id_shop_from
) z on x.id_shop = z.id_shop
--where substr(x.id_shop, 1, 2) in ('36', '38', '39', '40')
;

select a.id_shop, sum(kol) ost_kol  from e_osttek_online a inner join s_art b on a.art = b.art where b.season = 'Зимняя' 
and a.id_shop in (select shopid from tdm_map_new_shop_group where s_group = 15901)
group by a.id_shop
;

select *  from e_osttek_online a inner join s_art b on a.art = b.art where b.season = 'Зимняя' 
and a.id_shop in (select shopid from tdm_map_new_shop_group where s_group = 15901)
;

select id_shop_from, count(*) from t_map_new_poexalo  where id = 8936 and season = 'Зимняя' group by id_shop_from;
select id_shop_from, sum(kol) from t_map_new_poexalo  where id = 8936 and season = 'Зимняя' group by id_shop_from;


select 9999 id_shop, a.art, c.season, a.kol, a.asize, 0 flag, a.id_shop id_shop_from, 555 id,  systimestamp, 1 block_no
from e_osttek_online a
inner join tdm_map_new_shop_group d on a.id_shop = d.shopid and 15901 = d.s_group
left join (
  select a.id_shop, b.art from t_map_stay_in_shop a
  inner join tdv_map_new_anal b on a.art = b.analog
  group by a.id_shop, b.art 
) b on a.id_shop = b.id_shop and a.art = b.art
inner join s_art c on a.art = c.art
left join pos_brak x on a.scan = x.scan and a.id_shop = x.id_shop
where b.art is not null
and case when 'T' = 'T' then x.art else null end is null
and a.kol > 0 and d.season_out like '%' || c.season || '%'

and a.art = '035022/3' and a.asize = 39 and a.id_shop = '0018' 

group by a.id_shop, a.art, c.season, a.kol, a.asize;


select id_shop, sum(kol) from (
  select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
  from e_osttek_online a2
  inner join s_art c on a2.art = c.art
  left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
  left join st_muya_art z on a2.art = z.art
  where a2.scan != ' '
      --and pv_season like '%' || c.season || '%' -- только нужный сезон
       and a2.id_shop in (select shopid
                          from tdm_map_new_shop_group
                          where s_group = 15902)
       and a2.id_shop in (select shopid
                          from st_shop z
                          where z.org_kod = 'SHP')
       and a2.procent = 0 -- исключение некондиции
       and x.scan is null -- исключение брака
       and z.art is null -- исключение покупной
       and c.groupmw in ('Мужские', 'Женские') -- исключение детской
       --and case when i_filter_stock = 'T' then  else  end is null -- исключение стоковой
       and a2.id_shop = '3811'
       and c.season = 'Зимняя'
  group by a2.id_shop, a2.art, a2.asize, c.season
  having sum(kol) > 0
) group by id_shop--, art
;

select sum(kol)  from t_map_new_poexalo  where id = 8595 and id_shop_from = '2643' group by id_shop_from;

select id_shop_from id_shop, sum(kol) away_kol  from t_map_new_poexalo where id = 8957 and season = 'Зимняя'  and id_shop_from = '2643' group by id_shop_from;

select *  from t_map_new_poexalo where id = 8957 and season = 'Зимняя'  and id_shop_from = '2643';


select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season
from e_osttek_online a2
inner join s_art c on a2.art = c.art
left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
left join st_muya_art z on a2.art = z.art
left join (select id_shop, art from cena_skidki_all_shops where substr(to_char(cena), -2) = '99') s on a2.id_shop = s.id_shop and a2.art = s.art
where a2.scan != ' '
     and a2.id_shop in (select shopid
                        from tdm_map_new_shop_group
                        where s_group = 15901)
     and a2.id_shop in (select shopid
                        from st_shop z
                        where z.org_kod = 'SHP')
     and a2.procent = 0 -- исключение некондиции
     and x.scan is null -- исключение брака
     and z.art is null -- исключение покупной
     and c.groupmw in ('Мужские', 'Женские') -- исключение детской
     and c.season = 'Зимняя'
     and case when substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') 
     then s.art else null end is null
     and a2.id_shop = '3811'
group by a2.id_shop, a2.art, a2.asize, c.season
having sum(kol) > 0
;

select * from tdm_map_new_shop_group where s_group = 15901 and substr(shopid, 1, 2) in ('36', '38', '39', '40')