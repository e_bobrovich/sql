select 1 from dual;

select * from (
  select a.id_shop, a.art, a.season, sum(kol_ost) kol_ost, sum(kol_sale) kol_sale  from tdv_map_new_ost_sale a
  --where 
  --id_shop = '4506'
  group by a.id_shop, a.art, a.season
  having(sum(a.kol_sale) + sum(a.kol_ost) != 0) and sum(a.kol_sale) >= 5/*i_sale_pair_count*/ and sum(a.kol_sale) <= 100/*i_sale_max_pair_count*/ and sum(a.kol_ost) <= 100/*i_ost_pair_count*/
  order by a.id_shop, a.art
) 
--where 
--    kol_sale >= 4
--and kol_sale <= 4
--and kol_ost <= 1
--kol_ost = 0
;

select * from t_map_;

truncate table t_map_stay_in_shop;

select * from tdv_map_new_ost_sale;

--insert into t_map_stay_in_shop
select a.id_shop, a.art, 1, sum(a.kol_ost) kol_ost, sum(a.kol_sale) kol_sale
from tdv_map_new_ost_sale a
--where (a.id_shop, a.art) not in (select id_shop, art from t_map_stay_in_shop)
group by a.id_shop, a.art
having(sum(a.kol_sale) + sum(a.kol_ost) != 0) and sum(a.kol_sale) >= 5/*i_sale_pair_count*/ and sum(a.kol_sale) <= 100/*i_sale_max_pair_count*/ and sum(a.kol_ost) <= 100/*i_ost_pair_count*/
--order by a.id_shop, a.art
;


select id_shop, art from t_map_stay_in_shop
group by id_shop, art
having count(art) > 1
;

select * from t_map_stay_in_shop;

select * from t_map_stay_in_shop group by id_shop, art order by id_shop, art desc;


select a.*, b.limit, sum(kol_ost) over(partition by id_shop order by art rows unbounded preceding) kol_ost_all from (  
  select a.id_shop, a.art, a.season, sum(a.kol_ost) kol_ost from tdv_map_new_ost_sale a
  group by a.id_shop, a.art, a.season
  having(sum(a.kol_sale) + sum(a.kol_ost) != 0) and sum(a.kol_sale) >= 5/*i_sale_pair_count*/ and sum(a.kol_sale) <= 100/*i_sale_max_pair_count*/ and sum(a.kol_ost) <= 100/*i_ost_pair_count*/
) a left join (select shopid, dtype as limit from tdm_map_new_shop_group where s_group = 15901) b on a.id_shop = b.shopid
order by id_shop, art
;

select * from t_map_stay_in_shop;

select count(1) from t_map_stay_in_shop where id_shop = '4505' and priority = 1 and (id_shop, art) not in (select id_shop, art from t_map_stay_in_shop where priority != 1 and id_shop = '4505');

select * from (select art, max(priority) from t_map_stay_in_shop where id_shop = '4505' group by id_shop, art);
select count(*) from t_map_stay_in_shop where id_shop = '4505' and priority = 1;

select * from t_map_stay_in_shop_kol;


select a.id_shop, a.art, c.asize/*, a.priority*/, c.kol from (
  select a.id_shop, b.art/*, a.priority*/
  from t_map_stay_in_shop a
  inner join tdv_map_new_anal b on a.art = b.analog
  group by a.id_shop, b.art/*, a.priority */
) a inner join e_osttek_online c on a.id_shop = c.id_shop and a.art = c.art
where c.kol > 0
--order by a.id_shop, a.art, c.asize
;


--insert into t_map_stay_in_shop_kol (id_Shop, art, asize, kol_ost)
select x.id_shop, x.art, y.asize, x.priority, sum(y.kol) from (
  select a.id_shop, b.art, min(priority) priority from t_map_stay_in_shop a
  inner join tdv_map_new_anal b on a.art = b.analog
  group by a.id_shop, b.art
) x inner join e_osttek_online y on x.id_shop = y.id_shop and x.art = y.art
group by x.id_shop, x.art, y.asize, x.priority
;


select a.id_shop, b.art, min(priority) priority from t_map_stay_in_shop a
inner join tdv_map_new_anal b on a.art = b.analog
group by a.id_shop, b.art;

select x.id_shop, x.art, y.asize, sum(y.kol) from (
  select a.id_shop, b.art from t_map_stay_in_shop a
  inner join tdv_map_new_anal b on a.art = b.analog
  group by a.id_shop, b.art
) x inner join e_osttek_online y on x.id_shop = y.id_shop and x.art = y.art
group by x.id_shop, x.art, y.asize
;


select * from t_map_stay_in_shop_kol where id_shop = '4507' and art = '1637085' ;

select a.art, c.asize, c.kol, a.priority, a.kol_sale from (
  select a.id_shop, b.art, a.priority, a.kol_sale
  from t_map_stay_in_shop a
  inner join tdv_map_new_anal b on a.art = b.analog
  where a.id_shop = '4505'
  group by a.id_shop, b.art, a.priority,a.kol_sale
) a inner join e_osttek_online c on a.id_shop = c.id_shop and a.art = c.art
where c.kol > 0
order by a.priority, a.kol_sale,  a.art, c.asize;

