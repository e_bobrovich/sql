--ЗАДАНИЯ ПО ЗАПРОСУ ПО ОПРЕДЕЛЕННОМУ АССОРТИМЕНТУ НА ВЫВОЗ НА БЕЛВЕСТ
--в temp_reports запихиваем артикула нужные и ставим им примечание какое-нибудь в поле prim
--inner join temp_reports b on a.art = b.art and b.prim = ''01032019_shag'' - чтобы работала такая связка
SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor shop is select shopid
from st_shop a
left join temp_reports b on a.shopid = b.id_shop and b.prim = '01032019_shag_perem'
where bit_open = 'T' and org_kod = 'SHP' and shopid not like '00%'-- and shopid != '3619'
and b.id_shop is null
order by shopid;
shopRow shop%rowtype;
p_shopID varchar2(5);
BEGIN
 
open shop;
  loop  
       FETCH shop INTO p_shopID;
       EXIT WHEN shop%NOTFOUND;  
          begin
					
          select db_link_name into  p_shopLink from st_shop where shopid = p_shopID;  -- получаем линк    
					
					begin
						DBMS_SCHEDULER.drop_job( 'zad_j_mass_script_'||p_shopid);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'zad_j_mass_script_'||p_shopid,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
declare
p_sql varchar2(2000 CHAR);
p_var number;
BEGIN 

							execute immediate 'select count(*) into :i
																	from (select a.scan,sum(a.kol)
																				from e_osttek@'||']'||p_shopLink||q'['||' a
																				inner join temp_reports b on a.art = b.art and b.prim = ''01032019_shag''
																				group by a.scan
																				having sum(a.kol) > 0)'
							 into p_var;
							 
							 if p_var = 0 then
									dbms_output.put_line(']'||p_shopID||q'['||' NOT EXIST ');
									
									insert into temp_reports (prim,id_shop)
										values ('01032019_shag_perem',']'||p_shopID||q'[');
									commit;
									return;
							 end if;

							--для РФ здесь берем ЗАО, а для РБ KKL = 'KI1000', NKL = 'СООО "Белвест"'
							--проверками на даты в таблицах d_planot проверяем, чтобы не создались одни и те же задания повторно
							execute immediate 'insert into d_planot1@'||']'||p_shopLink||q'['||' (REL,KPODR,ID,DATED,DATEOT,DATETO,KKL,NKL,TEXT,USERS)
																	select REL,'''||']'||p_shopID||q'['||''' KPODR,rel ID,trunc(sysdate) DATED,trunc(sysdate) DATEOT,trunc(sysdate+30) DATETO,b.shopid KKL,b.shopname NKL,''Шаговита'' TEXT,''КУЗЬМЕНКОВ А.А.'' USERS
																	from (select nvl(max(rel)+1,300) rel from d_planot1@'||']'||p_shopLink||q'['||') a
																	inner join st_shop b on 1 = 1
																	where b.org_kod = ''ZAO'' and substr('''||']'||p_shopID||q'['||''',1,2) = substr(b.shopid,1,2)'; 
							execute immediate 'insert into d_planot2@'||']'||p_shopLink||q'['||' (REL,ART,ASIZE,EAN,ASSORT,KOL,NEI)
																	select rel,art,asize,ean,assort,sum(kol) kol,nei
																	from (
																		select c.REL,a.ART,a.ASIZE,'' '' EAN,'' '' ASSORT,sum(a.KOL) kol,''ПАР'' NEI
																		from e_osttek@'||']'||p_shopLink||q'['||' a
																		inner join s_art@'||']'||p_shopLink||q'['||' d on a.art = d.art
																		inner join (select max(rel) rel from d_planot1@'||']'||p_shopLink||q'['||') c on 1 = 1
																		inner join temp_reports e on e.art = d.art and e.prim = ''01032019_shag''
																		group by c.REL,a.ART,a.ASIZE
																		having sum(a.kol) > 0)
															group by rel,art,asize,ean,assort,nei';
							execute immediate 'update d_planot2@'||']'||p_shopLink||q'['||' a set a.assort = (select max(b.assort) from s_art b where a.art = b.art) where a.rel in (select rel from d_planot1@'||']'||p_shopLink||q'['||' where to_char(date_s,''yyyyMMdd'') >= ''20190301'')';
							execute immediate 'delete from d_planot1@'||']'||p_shopLink||q'['||' a where to_char(a.date_s,''yyyyMMdd'') >= ''20190301'' and (select count(*) from d_planot2@'||']'||p_shopLink||q'['||' c where c.rel = a.rel) = 0';
							dbms_output.put_line(']'||p_shopID||q'['||' deleted - '||sql%rowcount);
							execute immediate 'insert into d_planot1 select * from d_planot1@'||']'||p_shopLink||q'['||' where to_char(date_s,''yyyyMMdd'') >= ''20190301''';

					execute immediate 'insert into d_planot2 (REL,ART,ASIZE,PROCENT,EAN,ASSORT,KOL,CENA1,NEI,DATE_S,VID_INS,KPODR) 
																	select REL,ART,ASIZE,PROCENT,EAN,ASSORT,KOL,CENA1,NEI,DATE_S,VID_INS,'''||']'||p_shopID||q'['||'''
																	from d_planot2@'||']'||p_shopLink||q'['||' where to_char(date_s,''yyyyMMdd'') >= ''20190301''';
																	
					insert into temp_reports (prim,id_shop)
					values ('01032019_shag_perem',']'||p_shopID||q'[');
					commit;
																	
					EXCEPTION
						WHEN OTHERS THEN
						dbms_output.put_line(sqlcode||sqlerrm);
						p_sql := sqlcode||sqlerrm;
						rollback;				
						insert into temp_reports (prim,id_shop,text1)
							select '01032019_errorwithshag',']'||p_shopid||q'[',p_sql from dual;
						commit;

END;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('zad_j_mass_script_'||p_shopID); 
					
					
					
          commit;
          dbms_output.put_line(p_shopID||' COMPLETE ');
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            dbms_output.put_line(p_shopID||' ERROR ');
            rollback;
						
          end;
  end loop;
  
END ;


select distinct kpodr from d_planot1@S3711 where to_char(date_s,'yyyyMMdd') = '20190913' and  (kpodr in ('3711'));

select distinct kpodr from d_planot1 where to_char(date_s,'yyyyMMdd') >= '20190930' and (kkl like '%KI1000%' or kkl like '%D%') --and kpodr not in ('0045','3126','4210')
;

select distinct id_shop kpodr from temp_reports2 where prim = 'PLANOT_CREATION' and (id >= 15034   and id <= 15038) order by id_shop;

select distinct kpodr from d_Planot1@s3728;
-----------------------------------------
-----------------------------------------
--УДАЛЕНИЕ ЗАДАНИЙ
SET serveroutput ON
DECLARE 
V_SELE VARCHAR2(1024):='';
V_SHOPID VARCHAR2(21):=''; 
V_RESULTP VARCHAR2(99):='OK'; 
CURSOR1 sys_refcursor;
V_KOL NUMBER(3,0):=0;
V_LINK VARCHAR2(20) ; 
V_BIT_CLOSE VARCHAR2(1) ;
V_CL_NAME VARCHAR2(33):=' '; 
v_id number(5) := 542;
BEGIN
V_CL_NAME:=UPPER(V_CL_NAME);
--сначала удаляем задания в магазинах. в курсоре задаем нужные параметры даты и магазов
--когда по всем магазам успешно отработает удаление. удаляем из центра
--DELETE FROM D_PLANOT3 A WHERE to_char(date_s,'yyyyMMdd') = '20171113' and (kpodr like '26%' or kpodr like '43%');
--DELETE FROM D_PLANOT2 A WHERE to_char(date_s,'yyyyMMdd') = '20171113' and (kpodr like '26%' or kpodr like '43%');
--DELETE FROM D_PLANOT1 A WHERE to_char(date_s,'yyyyMMdd') = '20171113' and (kpodr like '26%' or kpodr like '43%');
   open cursor1 for  
--'select distinct kpodr from d_planot1 where to_char(date_s,''yyyyMMdd'') = ''20190913'' and  (kpodr in (''3711''))' ;
--'select distinct kpodr from d_planot1 where to_char(date_s,''yyyyMMdd'') >= ''20190930'' and (kkl like ''%KI1000%'' or kkl like ''%D%'')';
--'select distinct id_shop kpodr from temp_reports2 where prim = ''PLANOT_CREATION'' and (id >= 15034   and id <= 15038) order by id_shop';
'select distinct kpodr from d_Planot3 where calc_id = 15341';
 LOOP --
   <<LAB>>
   FETCH CURSOR1 INTO V_SHOPID; --
   EXIT WHEN CURSOR1%NOTFOUND; --

   SELECT NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=V_SHOPID;  
    
begin  
--DELETE FROM D_PLANOT3 A WHERE to_char(date_s,'yyyyMMdd') >= '20190930' and (kpodr = V_SHOPID);
--delete from d_planot2 a where to_char(date_s,'yyyyMMdd') >= '20190930' and (kpodr = V_SHOPID);
--delete from d_planot1 a where to_char(date_s,'yyyyMMdd') >= '20190930' and (kpodr = v_shopid);

V_SELE:=' DELETE FROM D_PLANOT2@'||V_LINK||' A WHERE to_char(date_s,''yyyyMMdd'') >= ''20191007'' '  ;
execute immediate  v_sele  ;
V_SELE:=' DELETE FROM D_PLANOT1@'||V_LINK||' A WHERE to_char(date_s,''yyyyMMdd'') >= ''20191007'' '  ;
EXECUTE IMMEDIATE  V_SELE  ;
  EXCEPTION
  WHEN OTHERS THEN
  dbms_output.put_line('ОШИБКА '||V_SHOPID);
  GOTO LAB;
end;    
 COMMIT;
 dbms_output.put_line(V_SHOPID);
 V_KOL:=V_KOL+1;
 
 
END LOOP; --
END ;