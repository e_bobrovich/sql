select * from st_shop where shopid in ('3813');

select * from st_shop_access where shopid = '4503';

--delete from d_planot1@s2805 where to_char(date_s,'yyyyMMdd') >= '20191115';
--delete from d_planot2@s2805 where to_char(date_s,'yyyyMMdd') >= '20191115';
--
--delete from d_planot1  where kpodr = '2805' and to_char(date_s,'yyyyMMdd') >= '20191115';
--delete from d_planot2  where kpodr = '2805' and to_char(date_s,'yyyyMMdd') >= '20191115';
--delete from d_planot3  where kpodr = '2805' and to_char(date_s,'yyyyMMdd') >= '20191115';

select * from pos_sale1@s3809;
select * from d_Planot1@s4501 order by rel desc;
select * from d_Planot2@s2620 where rel = 465 order by rel desc;

select * from d_planot1@s4501 where to_char(date_s,'yyyyMMdd') >= '20200101' order by date_s desc;-- where to_char(date_s,'yyyyMMdd') >= '20190930';
select rel, sum(kol) from d_planot2@s2730 where to_char(date_s,'yyyyMMdd') >= '20191115' group by rel order by rel;

select distinct rel from d_planot1 where kpodr = '3320' and to_char(date_s,'yyyyMMdd') >= '20191115';
select rel, sum(kol) from d_planot2 where kpodr = '2805' and to_char(date_s,'yyyyMMdd') >= '20191115' group by rel order by rel;

select * from d_Planot1@s2105 where rel in (403,404,405,406);
select rel, sum(kol) from d_Planot2@s3623 where rel >=346  group by rel;

update d_Planot1@s2805
set text = 'Вывоз сток'
where rel in (385,386,387,388,389,390,391,392);

select * from d_planot1 where to_char(date_s,'yyyyMMdd') >= '20200319' order by date_s desc;

select * from d_planot3 where calc_id in ('28878','28878','28881');

select distinct id_main,id_shop
from temp_reports2
where prim = 'PLANOT_CREATION'
and (id >= 22955   and id <= 15974)
and id_shop not in (select kpodr from d_planot1 where to_char(dated,'yyyyMMdd') >= '20191104')
--and id_shop not like '00%'
order by id_shop;

select distinct id_main,id_shop
from temp_reports2
where prim = 'PLANOT_CREATION'
--and (id >= 18454   and id <= 18454)
and id in ('18580','18552','18553')
and id_shop not in (select kpodr from d_planot1 where to_char(dated,'yyyyMMdd') >= '20191219' and text != 'Вывоз')
--and id_shop not like '00%'
order by id_shop;

select distinct id_main,id_shop
from temp_reports2
where prim = 'PLANOT_CREATION'
and (id in (21142,21154,21141,21157))
and id_shop not in (select kpodr from d_planot1 where to_char(dated,'yyyyMMdd') >= '20200312' and text != 'Вывоз')
--and id_shop not like '00%'
order by id_shop;

select * from d_planot1 where kpodr = '2646' order by date_s desc;
select * from d_planot1 order by  date_s;

select distinct id_main,id_shop
from temp_reports2
where prim = 'PLANOT_CREATION'
--and (id >= 21034 and id <= 21034)
and id in (21200)
--and id in ('20007')
and id_shop not in (select kpodr from d_planot1 where to_char(dated,'yyyyMMdd') >= '20200323'  and text = ' ')
--and id_shop not like '00%'
order by id_shop;

select distinct id_main,id_shop, id, max(bit_access) bit_access
from temp_reports2 a
inner join st_shop_access b on a.id_shop = b.shopid
where prim = 'PLANOT_CREATION'
and (id >= 28316 and id <= 28316)--27528--27528
and (id_shop, id)  not in (
  select a.kpodr,b.calc_id from d_planot1 a 
  inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id >= 28316 and b.calc_id <= 28316
  where to_char(a.date_s,'yyyyMMdd') >= '20210115' 
)
group by id_shop, id_main,id
order by id_shop, id_main,id
;

select distinct id_main,id_shop, id, max(bit_access) bit_access
from temp_reports2 a
left join st_shop_access b on a.id_shop = b.shopid
where prim = 'PLANOT_CREATION'
and (id >= 28878 and id <= 28878)
and (id_shop, id)  not in (

  select a.kpodr,b.calc_id from d_planot1 a 
  inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id >= 28878 and b.calc_id <= 28878
  where to_char(a.date_s,'yyyyMMdd') >= '20210128' 
)
group by id_shop, id_main,id
order by id_shop, id_main,id;


--ПЕРЕМЕЩЕНИЯ
SET serveroutput ON
DECLARE 
V_SELE VARCHAR2(1024):='';
V_SHOPID VARCHAR2(21):=''; 
V_RESULTP VARCHAR2(99):='OK'; 
CURSOR1 sys_refcursor;
V_KOL NUMBER(3,0):=0;
V_LINK VARCHAR2(20) ; 
V_BIT_CLOSE VARCHAR2(1) ;
V_CL_NAME VARCHAR2(33):=' ';
v_id number(5) := 99999;
BEGIN
   --(id >= 11036   and id <= 11042) - номера расчетов
	 --to_char(dated,'yyyyMMdd') >= '20190610' - фильтр по дате, чтобы исключить уже созданные задания
   OPEN CURSOR1 FOR  
q'[

  select distinct id_main,id_shop
  from temp_reports2
  where prim = 'PLANOT_CREATION'
  and (id >= 28878 and id <= 28878)
  and (id_shop, id)  not in (
    select a.kpodr,b.calc_id from d_planot1 a 
    inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id >= 28878 and b.calc_id <= 28878
    where to_char(a.date_s,'yyyyMMdd') >= '20210128' 
  )
  order by id_shop, id_main
]' ;

 LOOP --
   <<LAB>>
   FETCH CURSOR1 INTO V_ID,V_SHOPID; --
   EXIT WHEN CURSOR1%NOTFOUND; --

   SELECT NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=V_SHOPID; 

  COMMIT;
	
	begin
						DBMS_SCHEDULER.drop_job( 'zad_j_mass_script_'||V_SHOPID/*||'_'||V_ID*/);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'zad_j_mass_script_'||V_SHOPID/*||'_'||V_ID*/,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
declare
v_res varchar2(1000 CHAR);
BEGIN 
RKV023_PLANOT_2_NEW(']'||V_SHOPID||q'[','023~ ~2',]'||v_id||q'[,V_RES); -- все сезоны в одно задание
--RKV023_PLANOT_2_NEW_SEASON(']'||V_SHOPID||q'[','023~ ~2',]'||v_id||q'[,V_RES); -- разбивка по сезонам. бывало просили объединить некоторые сезоны в один. для этого прямо в процедуре можно сделать изменения 
commit;
END;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('zad_j_mass_script_'||V_SHOPID/*||'_'||V_ID*/); 

 COMMIT;
 dbms_output.put_line(V_SHOPID/*||'_'||V_ID*/);
 V_KOL:=V_KOL+1;
 
END LOOP; --

END ;