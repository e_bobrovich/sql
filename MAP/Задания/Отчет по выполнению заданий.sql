select case when d.id is not null then 'ВЫПОЛНЕНО' else null end "Статус",
a.rel "Номер_задания",d.id "Номер_расхода",d.dated "Дата_расхода",a.dated "Дата_задания",a.kpodr "Отправитель",substr(c.shopnum,2) "Отправитель",
c.cityname "Город_отпр",c.shopname "Отправитель",a.kkl "Получатель",a.nkl "Получатель",e.season "Сезон",sum(b.kol) "Кол-во",nvl(f.kolf,0) "Кол-во факт"
from d_planot1 a
inner join d_planot2 b on a.rel = b.rel and a.kpodr = b.kpodr
inner join s_art e on e.art = b.art
inner join s_shop c on a.kpodr = c.shopid
left join d_rasxod1 d on d.id_shop = a.kpodr and d.rel_planot = a.rel and d.bit_close = 'T'
left join (select id,id_shop,season,sum(kol) kolf from d_rasxod2 x inner join s_art y on x.art = y.art group by id,id_shop,season) f on f.id = d.id and f.id_shop = d.id_shop and f.season = e.season
where to_char(a.date_s,'yyyymmdd') <= '20190920' and to_char(a.date_s,'yyyymmdd') >= '20190805' and e.season = 'Летняя'
and (a.kkl like '%D%' or a.kkl like '%KI%') and a.text != 'Вывоз брака'
group by case when d.id is not null then 'ВЫПОЛНЕНО' else null end,a.rel,d.id,d.dated,a.dated,a.kpodr,substr(c.shopnum,2),c.cityname,c.shopname,a.kkl,a.nkl,e.season,nvl(f.kolf,0)
order by a.kpodr,a.rel;

select distinct c.matnr, d.shopnum  from d_rasxod1 a 
inner join d_rasxod2 b on a.id_shop = b.id_shop and a.id = b.id
left join firm.s_all_mat c on b.art = c.art and b.asize = c.asize
left join firm.s_shop d on a.id_shop = d.shopid
where to_char(a.date_s,'yyyy') = '2018'
and (a.kkl like '%D%' or a.kkl like '%KI%')-- and a.text != 'Вывоз брака'
and substr(a.id_shop,1,2) in ('36','38','39','40')
and b.scan != ' '
--and c.matnr is null
;