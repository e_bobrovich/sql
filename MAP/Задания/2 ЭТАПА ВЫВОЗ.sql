select * from t_map_new_poexalo where id_Shop_from in ('3521', '3524', '3527', '3528') order by id desc;

delete from t_map_new_Poexalo where id = 27010;

insert into t_map_new_Poexalo
select a.* from (
  select id_shop ,art ,season ,kol ,asize ,flag ,id_shop_from , 27010 id ,timed ,boxty ,block_no ,owner ,text1 ,text2 ,text3  
  from t_map_new_Poexalo where id in (27015,27016) --and id_shop_from in ('2812')
) a
left join (
  select distinct id_shop_from from t_map_new_Poexalo where id = 27010
) b on a.id_shop_from = b.id_shop_from
where b.id_shop_from is null
;

select id_shop_from, sum(kol) kol from t_map_new_poexalo where id = 25495 group by id_shop_from;
select * from tdm_map_new_shop_group where s_group = '123801';
update tdm_map_new_shop_group a
set dtype = dtype - (select sum(kol) kol from t_map_new_poexalo where id = 25495 and id_shop_from = a.shopid group by id_shop_from)
where s_group = '123801';

select distinct id_shop_from from t_map_new_Poexalo where id = 27010;

--delete from t_map_new_Poexalo where id = 25243 and id_shop_from in ('3521', '3524', '3527', '3528');

select * from d_planot3 where calc_id = '25243';

select  distinct a.id,case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end shop_from, max(bit_access) bit_access
from t_map_logistic_v a
left join st_shop_access b on a.shop_from = b.shopid
where  ID in (25230)
and case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end 
not in (
    select a.kpodr from d_planot1 a 
    inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id in (25230)
    where to_char(a.date_s,'yyyyMMdd') >= '20200917' and (a.kkl like '%KI1000%' or a.kkl like '%D%') 
)
group by shop_from,a.id
order by shop_from,a.id;


--ЗАДАНИЯ НА ВЫВОЗ НА БЕЛВЕСТ
SET serveroutput ON
DECLARE
V_SELE VARCHAR2(1024):='';
V_SHOPID VARCHAR2(21):='';  
V_RESULTP VARCHAR2(99):='OK'; 
CURSOR1 sys_refcursor;
V_KOL NUMBER(3,0):=0;
V_LINK VARCHAR2(20) ; 
V_BIT_CLOSE VARCHAR2(1) ;
V_ID NUMBER(10,0):=0;

BEGIN
	 --ID=9054 - номер расчета
	 --to_char(date_s,'yyyyMMdd') >= '20190306' - датами фильтровать уже созданные задания
   OPEN CURSOR1 FOR  
q'[
  select  distinct a.id,case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end shop_from from t_map_logistic_v a
  where  ID in (25230)
  and case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end 
  not in (
      select a.kpodr from d_planot1 a 
      inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id in (25230)
      where to_char(a.date_s,'yyyyMMdd') >= '20200917' and (a.kkl like '%KI1000%' or a.kkl like '%D%') 
  )
  group by shop_from,a.id
  order by shop_from,a.id
  
]' ; 


 LOOP --
   <<LAB>>
   FETCH CURSOR1 INTO V_ID,V_SHOPID; --
   EXIT WHEN CURSOR1%NOTFOUND; --

   SELECT NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=V_SHOPID;  
	 
	 begin
						DBMS_SCHEDULER.drop_job( 'zad_j_mass_script_'||V_SHOPID);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'zad_j_mass_script_'||V_SHOPID,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
declare
V_RES  VARCHAR2(99):='OK';
begin
BEGIN

EXECUTE IMMEDIATE  'select * from config@]'||V_LINK||q'[';
  EXCEPTION
  WHEN OTHERS THEN
  return;
END;    


 RKV023_PLANOT_2_ZAO(']'||V_SHOPID||q'[','023~ ~2',]'||V_ID||q'[,V_RES);
-- RKV023_PLANOT_2_ZAO_ALL_S(']'||V_SHOPID||q'[','023~ ~2',]'||V_ID||q'[,V_RES);
 COMMIT;
end;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('zad_j_mass_script_'||V_SHOPID); 
    
 commit;
 dbms_output.put_line(V_SHOPID);
 V_KOL:=V_KOL+1;
 
 
END LOOP; --

  EXCEPTION
  WHEN OTHERS THEN
  dbms_output.put_line('Ошибка '||V_SHOPID||' '||sqlcode||' '||sqlerrm);
  
END
;