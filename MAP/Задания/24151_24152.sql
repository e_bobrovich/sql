select shopid, max(bit_access) from st_shop_access where shopid in (
  select id_shop_from from t_map_new_poexalo where id = 24151
) group by shopid order by shopid;

select * from d_planot1 where kpodr = '2628' order by rel desc;
select * from d_Planot2 where kpodr = '2628' and rel = 371;


select a.kpodr, a.rel, sum(b.kol) kol from d_planot1 a
left join d_planot2 b on a.kpodr = b.kpodr and a.rel = b.rel 
where (a.kpodr, a.rel) in (select kpodr, rel from d_planot3 where calc_id in (24151,24152))
group by a.kpodr, a.rel
order by a.kpodr, a.rel;

select  distinct a.id,case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end shop_from from t_map_logistic_v a
where  ID in (24152)
and case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end 
not in (
    select a.kpodr from d_planot1 a 
    inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id = 24152
    where to_char(a.date_s,'yyyyMMdd') >= '20200804' and (a.kkl like '%KI1000%' or a.kkl like '%D%') 
)
--and a.shop_from = '2628'
group by shop_from,a.id
order by shop_from,a.id;

SET serveroutput ON
DECLARE
V_SELE VARCHAR2(1024):='';
V_SHOPID VARCHAR2(21):='';  
V_RESULTP VARCHAR2(99):='OK'; 
CURSOR1 sys_refcursor;
V_KOL NUMBER(3,0):=0;
V_LINK VARCHAR2(20) ; 
V_BIT_CLOSE VARCHAR2(1) ;
V_ID NUMBER(10,0):=0;

BEGIN
	 --ID=9054 - номер расчета
	 --to_char(date_s,'yyyyMMdd') >= '20190306' - датами фильтровать уже созданные задания
   OPEN CURSOR1 FOR  
q'[
  select  distinct a.id,case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end shop_from from t_map_logistic_v a
  where  ID in (24152)
  and case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end 
  not in (
      select a.kpodr from d_planot1 a 
      inner join d_planot3 b on a.kpodr = b.kpodr and a.rel = b.rel and b.calc_id = 24152
      where to_char(a.date_s,'yyyyMMdd') >= '20200804' and (a.kkl like '%KI1000%' or a.kkl like '%D%') 
  )
--  and a.shop_from = '2628'
  group by shop_from,a.id
  order by shop_from,a.id

]' ; 

 LOOP --
   <<LAB>>
   FETCH CURSOR1 INTO V_ID,V_SHOPID; --
   EXIT WHEN CURSOR1%NOTFOUND; --

   SELECT NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=V_SHOPID;  
	 
	 begin
						DBMS_SCHEDULER.drop_job( 'zad_j_mass_script_'||V_SHOPID);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'zad_j_mass_script_'||V_SHOPID,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
declare
V_RES  VARCHAR2(99):='OK';
begin
BEGIN

EXECUTE IMMEDIATE  'select * from config@]'||V_LINK||q'[';
  EXCEPTION
  WHEN OTHERS THEN
  return;
END;    


-- RKV023_PLANOT_2_ZAO(']'||V_SHOPID||q'[','023~ ~2',]'||V_ID||q'[,V_RES);
 RKV023_PLANOT_2_ZAO_ALL_S(']'||V_SHOPID||q'[','023~ ~2',]'||V_ID||q'[,V_RES);
 COMMIT;
end;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('zad_j_mass_script_'||V_SHOPID); 
    
 commit;
 dbms_output.put_line(V_SHOPID);
 V_KOL:=V_KOL+1;
 
 
END LOOP; --

  EXCEPTION
  WHEN OTHERS THEN
  dbms_output.put_line('Ошибка '||V_SHOPID||' '||sqlcode||' '||sqlerrm);
  
END
;