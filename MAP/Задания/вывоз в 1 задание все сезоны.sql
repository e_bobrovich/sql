--2519
--2519
--2519
--2519
--2519

select  distinct case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end shop_from from t_map_logistic_v a
where  ID=16290
and case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end 
  not in (select kpodr from d_planot1 where to_char(date_s,'yyyyMMdd') >= '20191030' and (kkl like '%KI1000%' or kkl like '%D%') )
group by shop_from
order by shop_from;

select distinct id_shop_from from t_map_new_poexalo 
where id = 16654 
and 
(id_shop_from not like '%W%' and id_shop_from not like '%S%' and id_shop_from != '2224')
order by id_shop_from;

select  kpodr, rel, sum(kol) from d_planot2 where to_char(date_s,'yyyyMMdd') >= '20191030'
and kpodr in (select distinct id_shop_from from t_map_new_poexalo where id = 16290 ) group by kpodr, rel order by kpodr;

select  * from d_planot1 where to_char(date_s,'yyyyMMdd') >= '20191022'
and kpodr in (select distinct id_shop_from from t_map_new_poexalo where id = 16290 ) order by kpodr;

select  distinct a.id,case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end shop_from from T_MAP_LOGISTIC_V a
where  ID=16290  and a.shop_from not like ('W%') and a.shop_from not like ('%S%') --and  a.shop_from in ('2519', '2519')
and case when a.shop_from like '%#%' then substr(a.shop_from,6) else a.shop_from end 
  not in (select kpodr from d_planot1 where to_char(date_s,'yyyyMMdd') >= '20191030' and (kkl like '%KI1000%' or kkl like '%D%') )
--and a.shop_from = '0001'
group by shop_from,a.id
order by shop_from,a.id;


--16290
select * from d_planot1 order by date_s desc;
select * from d_planot1@s2519 order by id desc;

select * from d_planot2 where kpodr = '2519' order by date_s desc;
select * from d_planot2@s2519 order by date_s desc;

SELECT NVL(MAX(REL)+1,0) FROM  D_PLANOT1 where kpodr = '2519';

--МАГАЗ 1
insert into  d_planot1@s2519 (rel,id,kpodr,kkl,nkl,dated,dateot,dateto,text,users) 
select id_rel, id_rel, shop_from,shop_to,shopname,sdate,sdate,sdate+30, 'Шаговита', ' '
from (select X.SHOP_FROM,
case when substr(X.SHOP_FROM,1,2) = '00' then 'KI1000' when substr(X.SHOP_FROM,1,1) = 'W' then substr(X.SHOP_FROM,2,2)||'D0' else substr(X.SHOP_FROM,1,2)||'D0' end SHOP_TO,
X.SDATE,X.KOL,X.SEASON,
case when substr(X.SHOP_FROM,1,2) = '00' then 'СООО "Белвест"' else Y.SHOPNAME end SHOPNAME 
from (SELECT A.SHOP_FROM,A.SHOP_TO,A.SDATE,SUM(A.KOL) AS KOL,C.SEASON,D.SHOPNAME  FROM T_MAP_LOGISTIC_V A 
LEFT JOIN S_ART C ON A.ART=C.ART
LEFT JOIN S_SHOP D ON A.SHOP_TO=D.SHOPID
where not ('2519' not like 'W%' and shop_from like '%W%')
and SHOP_FROM like '%'||'2519'||'%' AND SHOP_FROM!=SHOP_TO AND id = 16290
GROUP BY A.SHOP_FROM,A.SHOP_TO,A.SDATE,C.SEASON,D.SHOPNAME ) X
left join st_shop y on (case when substr(x.shop_from,1,2) = '00' then 'KI1000' when substr(x.shop_from,1,1) = 'W' then substr(x.shop_from,2,2)||'D0' else substr(x.shop_from,1,2)||'D0' end) = y.shopid)
left join (SELECT NVL(MAX(REL)+1,0) id_rel FROM  D_PLANOT1 where kpodr = '2519') e on '2519' = shop_from
group by id_rel, shop_from,shop_to,sdate,/*SEASON,*/shopname
ORDER BY SHOP_TO;
--(rel,id,kpodr,kkl,nkl,dated,dateot,dateto,text,users) 
--select '400', '400', shop_from,shop_to,shopname,sdate,sdate,sdate+30, 'Шаговита', ' ' 

--ЦЕНЕТР 1
insert into  d_planot1 (rel,id,kpodr,kkl,nkl,dated,dateot,dateto,text,users) 
select id_rel, id_rel, shop_from,shop_to,shopname,sdate,sdate,sdate+30, 'Шаговита', ' '
from (select X.SHOP_FROM,
case when substr(X.SHOP_FROM,1,2) = '00' then 'KI1000' when substr(X.SHOP_FROM,1,1) = 'W' then substr(X.SHOP_FROM,2,2)||'D0' else substr(X.SHOP_FROM,1,2)||'D0' end SHOP_TO,
X.SDATE,X.KOL,X.SEASON,
case when substr(X.SHOP_FROM,1,2) = '00' then 'СООО "Белвест"' else Y.SHOPNAME end SHOPNAME 
from (SELECT A.SHOP_FROM,A.SHOP_TO,A.SDATE,SUM(A.KOL) AS KOL,C.SEASON,D.SHOPNAME  FROM T_MAP_LOGISTIC_V A 
LEFT JOIN S_ART C ON A.ART=C.ART
LEFT JOIN S_SHOP D ON A.SHOP_TO=D.SHOPID
where not ('2519' not like 'W%' and shop_from like '%W%')
and SHOP_FROM like '%'||'2519'||'%' AND SHOP_FROM!=SHOP_TO AND id = 16290
GROUP BY A.SHOP_FROM,A.SHOP_TO,A.SDATE,C.SEASON,D.SHOPNAME ) X
left join st_shop y on (case when substr(x.shop_from,1,2) = '00' then 'KI1000' when substr(x.shop_from,1,1) = 'W' then substr(x.shop_from,2,2)||'D0' else substr(x.shop_from,1,2)||'D0' end) = y.shopid)
left join (SELECT NVL(MAX(REL)+1,0) id_rel FROM  D_PLANOT1 where kpodr = '2519') e on '2519' = shop_from
group by id_rel, shop_from,shop_to,sdate,/*SEASON,*/shopname
ORDER BY SHOP_TO;



--МАГАЗ 2
insert into  d_planot2@s2519  (rel,art,asize,procent,assort,kol,nei,ean,vid_ins)
 SELECT id_rel,A.ART,A.ASIZE,0,MAX(B.ASSORT),SUM(A.KOL),MAX(B.NEI),MAX(C.EAN),0 FROM 	 T_MAP_LOGISTIC_V A 
 LEFT JOIN S_ART_V B ON A.ART=B.ART
 left join s_ean c on a.art=c.art and a.asize=c.asize
 left join (SELECT NVL(MAX(REL),0) id_rel FROM  D_PLANOT1 where kpodr = '2519') e on '2519' = shop_from
 where  not ('2519' not like 'W%' and a.shop_from like '%W%')
and a.shop_from like '%'||'2519'||'%' and a.sdate=TO_DATE(sysdate) and a.id = 16290 
group by id_rel,a.art,a.asize ;


--ЦЕНТР 2
INSERT INTO  D_PLANOT2 (KPODR,REL,ART,ASIZE,PROCENT,ASSORT,KOL,NEI,EAN,VID_INS)
SELECT '2519',id_rel,A.ART,A.ASIZE,0,MAX(B.ASSORT),SUM(A.KOL),MAX(B.NEI),MAX(C.EAN),0 FROM   T_MAP_LOGISTIC_V A 
LEFT JOIN S_ART_V B ON A.ART=B.ART
LEFT JOIN S_EAN C ON A.ART=C.ART AND A.ASIZE=C.ASIZE
left join (SELECT NVL(MAX(REL),0) id_rel FROM  D_PLANOT1 where kpodr = '2519') e on '2519' = shop_from
where  not ('2519' not like 'W%' and a.shop_from like '%W%')
and A.SHOP_FROM like '%'||'2519'||'%' AND A.SDATE=TO_DATE(sysdate) and A.ID = 16290  
GROUP BY id_rel,A.ART,A.ASIZE;

--ЦЕНТР 3
insert into d_planot3 (rel,kpodr,calc_id,text1)
values (
(SELECT NVL(MAX(REL),0) id_rel FROM  D_PLANOT1 where kpodr = '2519')
,'2519',16290,' ');

commit;