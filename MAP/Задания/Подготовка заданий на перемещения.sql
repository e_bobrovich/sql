﻿--это все нужно, потому что были задания какие-то этапные и там они могли создаваться по нескольким расчетам. процедуры были переделаны под таблицу temp_reports2
delete from temp_reports where prim = 'PLANOT_CREATION';

insert into temp_reports (id,id_shop,text1,prim)
select   y.id,y.shop_from,z.text1,'PLANOT_CREATION' 
from t_map_logistic_v y
--inner join (select a.text2 id from ri_stored_proc_error a where a.sp_name = 'run_baby' and text2 >= '28901'   and text2 <= '28901' ) x on x.id = y.id
--inner join (select a.* from ri_stored_proc_error a where a.sp_name = 'run_baby'  and text2 >= '28901'   and text2 <= '28901' ) z on z.text2 = y.id
inner join (select a.text2 id from ri_stored_proc_error a where a.sp_name = 'run_baby' and text2 in ('28878','28882','28881') ) x on x.id = y.id
inner join (select a.* from ri_stored_proc_error a where a.sp_name = 'run_baby'  and text2 in ('28878','28882','28881') ) z on z.text2 = y.id
GROUP BY y.id,y.shop_from,z.text1;


--insert into temp_reports (id,id_shop,text1,prim)
--select   y.id,y.shop_from,'расчет','PLANOT_CREATION' 
--from t_map_logistic_v y
----where id in (24451, 24496)
--where id >= 27025 and id <= 27025
----and shop_from = '3707'
--GROUP BY y.id,y.shop_from;



update temp_reports
set id_main = rownum
where prim = 'PLANOT_CREATION' and upper(text1) = 'РАСЧЕТ'
;
update temp_reports a
set a.id_main = (select p.id_main from (select rownum+q.id id_main,z.*
                  from (select distinct x.id_shop,x.text1
                        from temp_reports x
                        where x.prim = 'PLANOT_CREATION' and x.id_main is null) z
                  inner join (select nvl(max(id_main),1) id from temp_reports where prim = 'PLANOT_CREATION') q on 1 = 1) p
                  where p.id_shop = a.id_shop and a.text1 = p.text1)
where a.prim = 'PLANOT_CREATION' and a.id_main is null
;

delete from temp_reports2 where prim = 'PLANOT_CREATION';

insert into temp_reports2 (id_main,id,id_shop,text1,prim)
select id_main,id,id_shop,text1,prim
from temp_reports
where prim = 'PLANOT_CREATION'
;
commit;


select * from temp_reports;
select * from temp_reports2;