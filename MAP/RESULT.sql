select max(priority) from t_map_stay_in_shop;

select id, id_shop_from, sum(kol) from t_map_new_poexalo where id_shop_from = '3524' group by id, id_shop_from order by id desc;

select id_shop, sum(kol) from t_map_new_poexalo where id = '9425' group by id_shop;
select sum(kol) from t_map_new_poexalo where id = '9314';

select count(*) from (
  select id_shop_from, sum(kol) from t_map_new_poexalo where id = 9294 group by id_shop_from having sum(kol) > 300 
);

select * from tdm_map_new_shop_group where s_group = 16401 ;--and shopid not in (select id_shop_from from t_map_new_poexalo where id = 9194);
select * from tdv_map_new_ost_sale where id_shop = '3309';
select art, asize, scan, sum(kol) from e_osttek_online where id_shop = '3309' group by art, asize, scan having sum(kol) > 0;
select * from t_map_stay_in_shop where id_shop = '3309';
select * from t_map_stay_in_shop_kol where id_shop = '3309';

select priority, count(*) from t_map_stay_in_shop group by priority;

select count(*) from t_map_stay_in_shop_kol where kol_ost = 0;
select count(*) from t_map_stay_in_shop_kol where kol_stay is not null and kol_ost != 0;
select count(*) from t_map_stay_in_shop_kol where kol_stay = 0;

select sum(kol_ost) from t_map_stay_in_shop_kol;
select sum(kol_stay) from t_map_stay_in_shop_kol;
select sum(dtype) from tdm_map_new_shop_group where s_group = '16001';

select sum(kol_ost) from t_map_stay_in_shop_kol where kol_ost > 0 and kol_stay is null;



select * from t_map_new_poexalo where id = 9594;

select block_no, count(*) from t_map_new_poexalo where id = 9099 group by block_no;

select id_shop, sum(kol)from T_MAP_NEW_OTBOR group by id_shop having sum(kol) > 0;

SELECT ID_SHOP_FROM, ART, ASIZE FROM T_MAP_NEW_POEXALO WHERE FLAG = 1 AND ID = 9099;

select max(priority) from t_map_stay_in_shop; 

select * from e_oborot1 order by id desc;


select * from s_shop;

select 
--* 
id_shop "ОТДЕЛЕНИЕ",
shopname "НАЗВАНИЕ",
region "РЕГИОН",
art "АРТИКУЛ",
kol_ost "ОСТАТОК",
kol_poexalo "ВЫВОЗ", 
kol_sale03 "ПРОДАЖИ(МАРТ)",
kol_sale02 "ПРОДАЖИ(ФЕВРАЛЬ)",
kol_sale "ПРОДАЖИ(СЕГОДНЯ)",
kol_filter "ОТФИЛЬТРОВАНО"
from t_map_bep001_result
where id = 9076
--and kol_ost - kol_poexalo > 0 and kol_poexalo > 0
order by region, id_shop, kol_poexalo desc, kol_sale03 desc, kol_sale02 desc, kol_sale desc;


-- POEXALO RESULT
select 
  a.id_shop_from "ОТДЕЛЕНИЕ", 
  b.shopname "НАЗВАНИЕ", 
  a.art "АРТИКУЛ", 
  a.asize "РАЗМЕР", 
  a.kol "КОЛИЧЕСТВО"
from t_map_new_poexalo a
inner join s_shop b on a.id_shop_from = b.shopid
inner join s_shop с on a.id_shop = с.shopid
where a.id = 9354
order by a.id_shop_from, a.art, a.asize
;

-- OSTTEK RESULT
select a.id_shop, b.shopname, a.art, c.season, a.asize, sum(kol) kol from e_osttek_online a
inner join s_shop b on a.id_shop = b.shopid
inner join s_art c on a.art = c.art
inner join (select shopid from tdm_map_new_shop_group where s_group = 15901) d on a.id_shop = d.shopid
where c.season = 'Зимняя'
group by a.id_shop, b.shopname, a.art, c.season, a.asize
having sum(kol) > 0
order by a.id_shop, a.art, a.asize
;
-- OSTTEK RESULT FILTER
select a.id_shop, b.shopname, a.art, c.season, a.asize, sum(kol) kol from e_osttek_online a
inner join s_shop b on a.id_shop = b.shopid
inner join s_art c on a.art = c.art
inner join (select shopid from tdm_map_new_shop_group where s_group = 15901) d on a.id_shop = d.shopid

left join pos_brak x on a.scan = x.scan and a.id_shop = x.id_shop
left join st_muya_art z on a.art = z.art
left join (select id_shop, art from cena_skidki_all_shops where substr(to_char(cena), -2) = '99') s on a.id_shop = s.id_shop and a.art = s.art

where c.season = 'Зимняя'
and a.scan != ' '
and a.procent = 0 -- исключение некондиции
and x.scan is null -- исключение брака
and z.art is null -- исключение покупной
and c.groupmw in ('Мужские', 'Женские') -- исключение детской
and c.season = 'Зимняя'
and case when substr(a.id_shop, 1, 2) in ('36', '38', '39', '40') 
then s.art else null end is null

group by a.id_shop, b.shopname, a.art, c.season, a.asize
having sum(kol) > 0
order by a.id_shop, a.art, a.asize
;


-- OSTTEK-POEXALO RESULT
select a.id_shop, b.shopname, a.art, c.season, a.asize, sum(kol) kol from e_osttek_online a
inner join s_shop b on a.id_shop = b.shopid
inner join s_art c on a.art = c.art
inner join (select shopid from tdm_map_new_shop_group where s_group = 15901) d on a.id_shop = d.shopid

where (a.id_shop, a.art) not in (
  select x.id_shop_from,x.art from t_map_new_poexalo x
  inner join s_shop y on x.id_shop_from = y.shopid
  where x.id = 9054
)
and c.season = 'Зимняя'
group by a.id_shop, b.shopname, a.art, c.season, a.asize
having sum(kol) > 0
order by a.id_shop, a.art, a.asize

;

-- OSTTEK-POEXALO RESULT
select a.id_shop, b.shopname, a.art, c.season, a.asize, sum(kol) kol from e_osttek_online a
inner join s_shop b on a.id_shop = b.shopid
inner join s_art c on a.art = c.art
inner join (select shopid from tdm_map_new_shop_group where s_group = 15901) d on a.id_shop = d.shopid


left join pos_brak x on a.scan = x.scan and a.id_shop = x.id_shop
left join st_muya_art z on a.art = z.art
left join (select id_shop, art from cena_skidki_all_shops where substr(to_char(cena), -2) = '99') s on a.id_shop = s.id_shop and a.art = s.art


where (a.id_shop, a.art) not in (
  select x.id_shop_from,x.art from t_map_new_poexalo x
  inner join s_shop y on x.id_shop_from = y.shopid
  where x.id = 9054
)
and c.season = 'Зимняя'
and a.scan != ' '
and a.procent = 0 -- исключение некондиции
and x.scan is null -- исключение брака
and z.art is null -- исключение покупной
and c.groupmw in ('Мужские', 'Женские') -- исключение детской
and c.season = 'Зимняя'
and case when substr(a.id_shop, 1, 2) in ('36', '38', '39', '40') 
then s.art else null end is null

group by a.id_shop, b.shopname, a.art, c.season, a.asize
having sum(kol) > 0
order by a.id_shop, a.art, a.asize
;



select a.id_shop_from "НОМЕР ОТПР.", b.shopname "ОТПРАВИТЕЛЬ", a.id_shop "НОМЕР ПОЛУЧ.", c.shopname "ПОЛУЧАТЕЛЬ", sum(a.kol) "КОЛИЧЕСТВО" 
from t_map_new_poexalo a 
inner join s_shop b on a.id_shop_from = b.shopid
inner join s_shop c on a.id_shop = c.shopid
where a.id between 9398 and 9398  
group by a.id_shop_from, b.shopname, a.id_shop, c.shopname 
order by a.id_shop_from, a.id_shop;

--4211
select * from d_planot1 where kpodr = '4211' and rel = 344 order by id desc; --344
select * from d_planot2 where kpodr = '4211' and rel = 344;
select * from d_planot3 where kpodr = '4211' and rel = 344;
