--текущие остатки собраные в одной таблице (нужно было на случай, когда текущие

--insert into tdv_map_osttek
      
select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
       from e_osttek_online a2
       inner join s_art c on a2.art = c.art
       left join pos_brak x on a2.scan = x.scan
                               and a2.id_shop = x.id_shop              
       left join st_muya_art z on a2.art = z.art                        
       where a2.scan != ' '
             and ',Зимняя,' like '%,' || c.season || ',%' -- только нужный сезон
             and a2.procent = 0
             and c.groupmw in ('Мужские', 'Женские')
             and z.art is null
             and x.art is null
             and a2.id_shop in (select shopid
                                from tdm_map_new_shop_group
                                where s_group = 16202)
             and a2.id_shop in (select shopid
                                from st_shop z
                                where z.org_kod = 'SHP')
             and x.scan is null
       group by a2.id_shop, a2.art, a2.asize, c.season
       having sum(kol) > 0)
group by id_shop, art, asize, season;


--insert into tdv_map_osttek
select id_shop, art, asize, sum(kol) kol, season, 0 out_block, sum(kol_rc) kol_rc, sum(kol) kol
from (select shopid id_shop, a.art, asize, sum(oste) kol, c.season, 0 out_block, 0 kol_rc
     from e_oborot2 a
     inner join s_art c on a.art = c.art
     left join st_muya_art z on a.art = z.art      
     where id = 824
           and c.mtart != 'ZHW3'
           and procent = 0
           and c.groupmw in ('Мужские', 'Женские')
           and z.art is null
           
           and ',Зимняя,' like '%,' || c.season || ',%' -- только нужный сезон
           and a.shopid in (select shopid
                            from tdm_map_new_shop_group
                            where s_group = 16202)
     group by shopid, a.art, asize, c.season)
group by id_shop, art, asize, season;
