select * from TDV_MAP_13_RESULT order by id desc;

select block_no, sum(kol), max(timed)  from t_map_new_poexalo where id = 18997 group by block_no order by block_no;
select sum(kol) from t_map_new_poexalo where id = 20251;

select block_no, sum(kol), min(timed), max(timed)  from t_map_new_poexalo where id = 20166 group by block_no order by block_no;
select sum(kol) from t_map_new_poexalo where id = 20243;

select season, sum(kol) from t_map_new_poexalo where id = 20956 group by season;

select boxty, sum(kol), max(timed), min(timed)  from t_map_new_poexalo where id = 19099 group by boxty order by boxty;

select id_shop, sum(kol), max(timed)  from t_map_new_poexalo where id = 20999 and boxty = 'purple' group by id_Shop order by max(timed) desc;

select id_Shop, sum(kol) from t_map_new_poexalo where id = '21154' group by id_shop;
select id_Shop_from, sum(kol) from t_map_new_poexalo where id = '21154' group by id_shop_from;
select id_Shop_from, season, sum(kol) from t_map_new_poexalo where id = '21151' group by id_shop_from, season;
select id_Shop_from, id_shop, sum(kol) from t_map_new_poexalo where id = '20252' group by id_shop_from, id_shop;

select * from t_map_new_poexalo where id = 19099;

select sum(kol) from t_map_new_poexalo where id = 20150;

select sum(kol) from t_map_new_otbor;

select * from T_OB_LOGS where id = '19099';
select * from T_OB_LOGS where id = '18464';
select * from T_OB_LOGS where id = '19058';

-- ТЕСТ НА НАЛИЧИЕ В ФИОЛЕТОВОМ БЛОКЕ ЗАБЛОКИРОВАННЫХ БРЕНДОВ ДЛЯ МАГАЗИНА
select * from t_map_new_poexalo a 
inner join s_art c on a.art = c.art
inner join s_limmag b on a.id_shop = b.shopid and c.is_brand = b.brand
where a.id = 16796
and block_no = 7
;

select * from TDV_MAP_13_RESULT where id >= 17714 and id <= 17790 order by CLUSTER_NAME, id ;

-- ТЕСТ НА НАЛИЧИЕ РАЗНЫХ АРТИКУЛОВ\АНАЛОГОВ ПО МОДЕЛИ ДЛЯ МАГАЗИНА
select id_shop, model, count(*) from (
  select distinct a.id_shop, a.text2 model, a.text1 analog-- , a.art
  from t_map_new_poexalo a 
  where a.id = 16796
  and block_no = 7
) 
group by id_shop, model
having count(*) > 1
;


-- 
select *
from t_map_new_poexalo a 
inner join s_art c on a.art = c.art
where a.id = 16796 and c.groupmw not in ('Мужские','Женские')
;

-- ТЕСТ НА ПЕРЕВЫПОЛНЕНИЕ ЛИМИТОВ НА ВВОЗ
select id_shop, sum(limit_in) - sum(kol_poexalo) diff from (
  select shopid id_shop, count_limit_in limit_in, 0 kol_poexalo  
  from tdm_map_new_shop_group 
  where s_group = 47201
  union all
  select id_shop, 0 limit_in, sum(kol) kol_poexalo from t_map_new_poexalo a
  where id = 16796 and id_shop != 'fond'
  group by id_shop
) group by id_shop
having sum(limit_in) - sum(kol_poexalo) < 0;

-- ТЕСТ НА ПРЕВЫШЕНИЕ ЛИМИТОВ
select id_shop ,sum(kol_limit) kol_limit, sum(kol_poexalo) kol_poexalo, sum(kol_limit) - sum(kol_poexalo) diff from (
  select id_shop, sum(kol) kol_poexalo, 0 kol_limit from t_map_new_poexalo where id = 16796 group  by id_shop
  union all
  select shopid id_shop, 0 kol_poexalo, count_limit_in kol_limit from tdm_map_new_shop_group where s_group = 47201 and count_limit_in != 0
) 
group by id_shop 
order by id_shop;

select id_shop, block_no, sum(kol) kol_poexalo, 0 kol_limit 
from t_map_new_poexalo 
where id = 16796 and id_shop = '3725'
group  by id_shop, block_no 
order by id_shop, block_no;

-- ТЕСТ НА ЗАБЛОКИРОВАННЫЕ АРТИКУЛА
select * from t_map_new_poexalo a
inner join (
      select art from s_art 
      WHERE GROUPMW NOT IN ('Мужские','Женские')
      union all
      select art from st_sale_block
      union all
      select art from st_muya_art
      union all
      select art from t_map_block_art_summer_raspred
) b on a.art = b.art
where a.id = 16796;

-- ТЕСТ НА КОЛИЧЕСТВО РАМЕРОВ В БЛОКЕ ДЛЯ ВБ
select a.art, count(a.asize) from t_map_new_poexalo a
where a.id = 16796 and block_no = -1
group by a.art
having count(a.asize) < 3;

-- ТЕСТ НА КОЛИЧЕСТВО ПАР В РАЗМЕРЕ В БЛОКЕ ДЛЯ ВБ
select a.art, a.asize, sum(a.kol) from t_map_new_poexalo a
where a.id = 16796 and block_no = -1
group by a.art, a.asize
having sum(a.kol) < 5 and sum(a.kol) > 20;

