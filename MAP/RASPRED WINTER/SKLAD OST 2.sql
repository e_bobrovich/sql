insert into tdv_map_warehouse_stock
select art, asize, sum(kol) kol, boxty
from (
       
       -- россыпь 
       select d.art, d1.asize, sum(d.verme) kol, null boxty
       from st_sap_ob_ost2 d
       left join s_art e on d.art = e.art
       left join s_all_mat d1 on d.matnr = d1.matnr
       where d.boxty in (' ')
             and d1.asize != 0
             and lgtyp in ('R08', 'Z08', 'S08')
             and ',Летняя,' like '%,' || e.season || ',%'
             and e.art not like '%/О%'
       and e.groupmw in ('Мужские', 'Женские')
       and d.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
       group by d.art, d1.asize
       
       union all
       
       -- транзиты
       select a1.art, asize, sum(kol) kol, null
       from trans_in_way a1
       left join TDV_MAP_NEW_ANAL d on a1.art = d.art
       left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                         and a1.Postno_Lifex = e.id
       inner join s_art c on a1.art = c.art
       where scan != ' '
             and ',Летняя,' like '%,' || c.season || ',%'
             and a1.art not like '%/О%'
             and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
             and not (
                ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%')) 
                and substr(skladid, 1, 2) in ('36', '38', '39', '40')
             )
             and c.art not like '%/О%'
       and c.groupmw in ('Мужские', 'Женские')
       and c.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
       group by a1.art, asize
       having sum(kol) > 0
       
       union all
       -- РЦ
       select a2.art, a2.asize, sum(a2.kol) kol, null
       from tdv_map_rc_data a2
       inner join s_art c on a2.art = c.art
       left join TDV_MAP_NEW_ANAL d on d.art = a2.art
       where ',Летняя,' like '%,' || c.season || ',%'
             and a2.art not like '%/О%'
             and substr(skladid, 1, 1) != 'W'
             and substr(skladid, 3, 1) != 'S'
             and case when 'F' = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
             and c.art not like '%/О%'
       and c.groupmw in ('Мужские', 'Женские')
       and c.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
       group by a2.art, a2.asize
       having sum(a2.kol) > 0)

group by art, asize, boxty;


-- от россыпи 8-ого склада отнимаю резервы 8-ого склада
update tdv_map_warehouse_stock x1
set x1.kol =
     (select x1.kol - x2.verme
      from (select art, to_number(asize, '99999.99') asize, sum(verme) verme
             from tdv_map_new_sap_reserv2 x3
             where x3.boxty = ' '
                   and trans != 'X'
                   and kunnr not in ('X_NERASP', 'X_SD_USE')
                   --and ABLAD = '08'
             group by art, to_number(asize, '99999.99')) x2
      where x1.art = x2.art
            and x1.asize = x2.asize)
where exists (select x1.kol - x2.verme
       from (select art, to_number(asize, '99999.99') asize, sum(verme) verme
              from tdv_map_new_sap_reserv2 x3
              where x3.boxty = ' '
                    and trans != 'X'
                    and kunnr not in ('X_NERASP', 'X_SD_USE')
                    --and ABLAD = '08'
              group by art, to_number(asize, '99999.99')) x2
       where x1.asize = x2.asize
             and x1.art = x2.art);
  
delete from tdv_map_warehouse_stock
where kol <= 0;

select * from tdv_map_warehouse_stock;

select sum(kol) from tdv_map_warehouse_stock a2
inner join s_art c on a2.art = c.art
       where ',Летняя,' like '%,' || c.season || ',%'
             and a2.art not like '%/О%'
             and c.art not like '%/О%'
       and c.groupmw in ('Мужские', 'Женские')
       and c.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
;

call map_fl.fillWarehouseStock();


select * from tdv_map_warehouse_stock;

select sum(kol) from tem_mapif_fill_warehouse_stock;

select sum(kol) from (
      select a.art, d.asize, sum(a.verme) kol, 0 flag, nvl(c.analog, a.art) analog
      --from st_sap_ob_ost a
      from st_sap_ob_ost2 a
      left join s_art b on a.art = b.art
      left join TDV_MAP_NEW_ANAL c on a.art = c.art
      left join s_all_mat d on a.matnr = d.matnr
      where d.asize != 0
            and a.boxty = ' '
            and ',Летняя,' like '%,' || b.season || ',%'
            --and a.art not like '%/О%'
            --and b.groupmw in ('Мужские', 'Женские')
--            and b.art not in (select art from t_map_new_art
--                  union select '1715000/1' from dual
--                  union select '1715001/1' from dual)
            and lgtyp not in ('R08', 'Z08', 'S08')
      group by a.art, d.asize, nvl(c.analog, a.art)
);

insert into tdv_map_warehouse_stock
select art, asize, sum(kol) kol, boxty from (
      select a.art, d.asize, sum(a.verme) kol, null boxty
      --from st_sap_ob_ost a
      from st_sap_ob_ost2 a
      left join s_art b on a.art = b.art
      left join TDV_MAP_NEW_ANAL c on a.art = c.art
      left join s_all_mat d on a.matnr = d.matnr
      where d.asize != 0
            and a.boxty = ' '
            and ',Летняя,' like '%,' || b.season || ',%'
            and a.art not like '%/О%'
            and b.groupmw in ('Мужские', 'Женские')
            and b.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
            and lgtyp not in ('R08', 'Z08', 'S08')
      group by a.art, d.asize
)group by art, asize, boxty;

