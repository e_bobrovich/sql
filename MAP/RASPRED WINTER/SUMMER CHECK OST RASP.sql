select * from TDV_MAP_13_RESULT order by id desc;

select block_no, sum(kol), max(timed)  from t_map_new_poexalo where id = 15739 group by block_no order by block_no;
select * from t_map_new_poexalo where id = 15739 and block_no = 0;

--15413--15739
select sum(kol) from t_map_new_poexalo where id = 15739;

select * from t_map_new_poexalo where id = 15739;
--delete from t_map_new_poexalo where id = 15739;

call map_fl.fillwarehousestock2();

select * from t_map_new_otbor_before where id = 15739;

select sum(kol) from t_map_new_otbor_rasp;
select sum(kol) from t_map_new_otbor_before where id = 15739;
select sum(kol) from t_map_new_otbor_after where id = 15739;


select sum(kol) from t_all_rasp_box_work_rasp;
select sum(kol) from t_all_rasp_box_before where id = 15739;
select sum(kol) from t_all_rasp_box_after where id = 15739;

select sum(a.kol * f.kol) kol from t_all_rasp_box_before a
left join (select boxty, sum(kol) kol
             from T_SAP_BOX_ROST -- таблица с ростовками по коробам
             group by boxty) f on a.boxty = f.boxty
where id = 15739;

select sum(kol) from (
  select '9999' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
  from tem_mapif_fill_warehouse_stock a
  inner join s_art b on a.art = b.art
  left join TDV_MAP_NEW_ANAL c on a.art = c.art
  where b.art not like '%/О%'
       and ',Летняя,' like '%,' || b.season || ',%'
  group by a.art, b.season, a.asize, nvl(c.analog, b.art)
);

-- короба
select sum(kol_pair), sum(kol_box) from (
  select d.art, d.boxty, sum(d.verme) kol_pair ,sum(d.verme) / f.kol kol_box, 0 flag, e.season, nvl(g.analog, d.art) analog
  from st_sap_ob_ost2 d
  left join s_art e on d.art = e.art
  left join (select boxty, sum(kol) kol
             from T_SAP_BOX_ROST -- таблица с ростовками по коробам
             group by boxty) f on d.boxty = f.boxty
  left join TDV_MAP_NEW_ANAL g on d.art = g.art
  where d.boxty not in (' ', 'N')
  and ',Летняя,' like '%,' || e.season || ',%'
  and e.groupmw in ('Мужские', 'Женские')
  and e.art not like '%/О%'
  --and lgtyp in ('R08', 'Z08', 'S08')
  group by d.art, d.boxty, f.kol, e.season, nvl(g.analog, d.art)
);

select sum(a.verme) kol from st_sap_ob_ost2 a;

-- россыпь
select sum(kol) from (
    select a.art, d.asize, sum(a.verme) kol, 0 flag, nvl(c.analog, a.art) analog
    --from st_sap_ob_ost a
    from st_sap_ob_ost2 a
    left join s_art b on a.art = b.art
    left join TDV_MAP_NEW_ANAL c on a.art = c.art
    left join s_all_mat d on a.matnr = d.matnr
    where d.asize != 0
          and a.boxty = ' '
          and ',Летняя,' like '%,' || b.season || ',%'
          --and b.groupmw in ('Мужские', 'Женские')
          --and a.art not like '%/О%'
          --and lgtyp in ('R08', 'Z08', 'S08')
    group by a.art, d.asize, nvl(c.analog, a.art)
);

select * from st_sap_ob_ost2 a
left join s_art b on a.art = b.art
left join s_all_mat d on a.matnr = d.matnr
where d.asize != 0
          and a.boxty = ' '
          and ',Летняя,' like '%,' || b.season || ',%';

-- транзиты
select type_t, sum(kol) from (select '9999', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag,
  case when e.id_shop is not null then 1 when a1.shopid not like '%D%' then 2 when a1.skladid != 'KI1000' then 3 end type_t
  from trans_in_way a1
  left join TDV_MAP_NEW_ANAL d on a1.art = d.art
  left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                   and a1.Postno_Lifex = e.id
  inner join s_art c on a1.art = c.art
  where scan != ' '
       and ',Летняя,' like '%,' || c.season || ',%'
--       and a1.art not like '%/О%'
--       and e.id_shop is null
       and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')   
       and not (
          ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%')) 
          and case when 'F' = 'F' then substr(skladid, 1, 2) else ' ' end in ('36', '38', '39', '40')
       )
  group by a1.art, c.season, asize, nvl(d.analog, a1.art),
  case when e.id_shop is not null then 1 when a1.shopid not like '%D%' then 2 when a1.skladid != 'KI1000' then 3 end
  having sum(kol) > 0
    
) group by type_t;

select e.*, a1.*,
case when e.id_shop is not null then 1 when a1.shopid not like '%D%' then 2 when a1.skladid != 'KI1000' then 3 end type_t
from trans_in_way a1
inner join s_art c on a1.art = c.art
left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                   and a1.postno_lifex = e.id
where 
',Летняя,' like '%,' || c.season || ',%'
and e.id is not null
--and a1.shopid not like '%D%' 
--and a1.skladid != 'KI1000'
--and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')   
;
select *
--a1.skladid, a1.postno_lifex--, sum(kol)
--from trans_in_way a1
from D_Sap_Odgruz1 a1
left join tdv_map_ex_transit e on a1.skladid = e.id_shop and a1.postno_lifex = e.id
--group by a1.skladid, a1.postno_lifex
;


-- ТРАНЗИТЫ КОТОРЫХ НЕТ ВО ВЬЮВЕРЕ ИБЕРЕМ ИХ ИЗ РАСХОДОВ
select sum(kol) from (
  select a.*, c.*, d.* from tdv_map_ex_transit a 
  left join (
    select skladid, postno_lifex from trans_in_way 
    group by skladid, postno_lifex
  ) b on b.skladid = a.id_shop and b.postno_lifex = a.id
  left join d_rasxod1 c on a.id_shop = c.id_shop and a.id = c.id 
  left join d_rasxod2 d on c.id_shop = d.id_shop and c.id = d.id 
  where b.skladid is null
  and c.id is not null
)
;

--select sum(kol) from (
  select distinct a1.id, a1.id_shop,e.id, e.id_shop, b.postno_lifex, b.skladid, b.shopid
  --'9999', a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag,
  --case when e.id_shop is not null then 4 else 0 end type_t
  from d_rasxod1 a1
  inner join d_rasxod2 a2 on a1.id_shop = a2.id_shop and a1.id = a2.id 
  left join tdv_map_new_anal d on a2.art = d.art
  left join tdv_map_ex_transit e on a1.id_shop = e.id_shop and a1.id = e.id
  left join (
    select skladid, postno_lifex, shopid from trans_in_way 
    group by skladid, postno_lifex, shopid
  ) b on b.skladid = e.id_shop and b.postno_lifex = e.id                                 
  
  inner join s_art c on a2.art = c.art
  where b.skladid is null
        and e.id is not null
--        and scan != ' '
--        and ',Летняя,' like '%,' || c.season || ',%'
        --and a2.art not like '%/О%'
  
  --      and (e.id_shop is not null and b.shopid not like '%D%' and b.skladid != 'KI1000')   
  --      and not (
  --         ((e.id_shop is not null and b.shopid not like '%D%') or (e.id_shop is null and b.shopid not like '%D%')) 
  --         and case when 'F' = 'F' then substr(b.skladid, 1, 2) else ' ' end in ('36', '38', '39', '40')
  --      )
  --group by a2.art, c.season, a2.asize, nvl(d.analog, a2.art),
  --case when e.id_shop is not null then 4 else 0 end
  --having sum(kol) > 0
--)
;

select *from trans_in_way where skladid = '2829' and postno_lifex = '20';

select * from d_sap_odgruz1 where skladid = '3727';
select * from d_sap_odgruz2;
select * from d_planot1 order by date_s desc;
select * from d_rasxod1 where id_shop = '3727' order by date_s desc;

select shopid id_shop from tdm_map_new_shop_group where s_group = '43301';

call CREATE_TRANS_DOC_FROM_SHOP('id_shop');

SELECT IDOP FROM ST_OP WHERE CONFIR=1 AND  PR_RX=1;

select * from ri_exchange_info where id_shop = '3727' order by ex_date desc;

select * from d_rasxod1 a
                    where KKL != '0' and KKL != ' ' and KKL is not null and BIT_CLOSE ='T' 
                          and (idop in(select idop from st_op where confir=1 and  pr_rx=1) or (idop = '21' and to_char(dated,'yyyymm') >201612  ))
                          and ID||ID_SHOP not in (select postno_lifex||skladid from d_sap_odgruz1 where pr_ud in (0,2)) and id_shop = '0044';

SELECT MAX(IDOP) , max(bit_close)
  FROM D_RASXOD1 
  where id_shop=v_id_shop and id=v_id 
            AND (idop IN(SELECT IDOP FROM ST_OP WHERE CONFIR=1 AND  PR_RX=1) or (idop = '21' and to_char(dated,'yyyymm') >201609  ));


select sum(kol) from (
  select '9999', a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
  from TDV_MAP_RC_DATA_RASP a2
  inner join s_art c on a2.art = c.art
  left join TDV_MAP_NEW_ANAL d on d.art = a2.art
  where ',Летняя,' like '%,' || c.season || ',%'
       and a2.art not like '%/О%'
       and substr(skladid, 1, 1) != 'W' --склады
       and substr(skladid, 3, 1) != 'S' -- санг
       and case when 'T' = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
);