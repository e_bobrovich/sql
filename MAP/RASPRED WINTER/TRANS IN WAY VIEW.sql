
  select a4.prdate,a4.postno,A4.Postno_Lifex,A4.Postno_Traid,a4.skladid,
        a4.shopid,a5.art,a5.asize,a5.scan,a5.kol,a5.partno , a4.postvid, a5.price
from D_Sap_Odgruz1 a4
left join d_sap_odgruz2 a5 on a4.postno = a5.postno
left join
(select distinct A3.Postno_Traid from FIRM.D_Sap_Odgruz1  a3
  inner join (
      select y.Postno
      from FIRM.d_prixod1 x
      inner join FIRM.d_prixod2 y on x.id = y.id and x.id_shop = y.id_shop
      where x.bit_close = 'T' and x.postno != ' '      
      group by y.postno      
      having count(*) > 0      
      
      union      
      
      select a12.postno      
      from dist_center.d_prixod1 a11        
      left join dist_center.d_prixod2 a12 on a11.rel = a12.rel and a11.dcid = a12.dcid      
      where a11.bit_close = 'T' and a12.postno != ' '
      ) a8 on a3.postno = a8.postno      
      
      union      
      
      select distinct id_shop||id postno_traid from s_accepted_odgruz1      
      
      union      
      
      select distinct id_shop||id from d_rasxod1 where bit_close = 'F'      
      
      union      
      
      select postno_traid from d_sap_odgruz1 
      where shopid in (select kkl from st_skl where substr(kkl,0,1) = 'D') 
      and prdate < to_char(sysdate-7,'yyyymmdd')
) c on a4.postno_traid = c.postno_traid      
where c.postno_traid is null and a4.shopid not like 'TD%' and      
a4.prdate >= '20160901' and pr_ud = 0  and a4.skladid not in ('8899','S777','S888') and a4.shopid not in ('0010000359','S777','S888') and       
(case when a4.shopid like '%U%' then (case when a5.asize != 0 then 1 else 0 end) else 1 end) = 1;
