select case when id_shop = 'fond' then '2108' else id_shop end id_shop_result, x.* from t_map_warehouse_result_rasp x;

  --CREATE OR REPLACE FORCE VIEW "FIRM"."MAP_SAP_RESULT_W_8_ROSS" ("ART", "VERME", "BOXTY", "SHOPNUM", "MATNR", "ASIZE", "POSTGR") AS 
  select a.art, sum(a.kol) verme, case when boxty is null then ' ' else boxty end boxty , b.shopnum, c.matnr, case when mod(a.asize,1)!=0 then to_char(a.asize) else to_char( a.asize)||',0' end asize, d.postno postgr
from (select case when id_shop = 'fond' then '2108' else id_shop end id_shop_result, x.* from t_map_warehouse_result_rasp x)  a
inner join /*tdv_map_shop_copy*/ s_shop b on a.id_shop_result = b.shopid
left join s_all_mat c on a.art = c.art and a.asize = c.asize
left join t_map_shop_post_result_rasp d on a.art = d.art and a.id_shop = d.id_shop
where lgtype = 8 
and fond_boxty is  null 
and a.id_shop is not null
group by a.art, a.boxty,  b.shopnum, c.matnr, a.asize, d.postno;


  --CREATE OR REPLACE FORCE VIEW "FIRM"."MAP_SAP_RESULT_W_8_FOND_BOX" ("ART", "VERME", "BOXTY", "SHOPNUM", "MATNR", "ASIZE", "POSTGR") AS 
  select a.art, sum(a.kol) verme, fond_boxty boxty , b.shopnum, c.matnr, case when mod(a.asize,1)!=0 then to_char(a.asize) else to_char( a.asize)||',0' end asize, d.postno postgr
from (select case when id_shop = 'fond' then '2108' else id_shop end id_shop_result, x.* from t_map_warehouse_result_rasp x)  a
inner join /*tdv_map_shop_copy*/ s_shop b on a.id_shop_result = b.shopid
left join s_all_mat c on a.art = c.art and a.asize = c.asize
left join t_map_shop_post_result_rasp d on a.art = d.art and a.id_shop = d.id_shop
where lgtype = 8 
and fond_boxty is not null 
and a.id_shop is not null
group by a.art, a.fond_boxty,  b.shopnum, c.matnr, a.asize, d.postno;

CREATE OR REPLACE FORCE VIEW "FIRM"."MAP_SAP_RESULT_W_8_FOND" ("ART", "VERME", "BOXTY", "SHOPNUM", "MATNR", "ASIZE", "POSTGR") AS 
select a.art, sum(a.kol) verme, /*fond_boxty*/ ' ' boxty , b.shopnum, c.matnr, case when mod(a.asize,1)!=0 then to_char(a.asize) else to_char( a.asize)||',0' end asize, d.postno postgr
from (select case when id_shop = 'fond' then '2108' else id_shop end id_shop_result, x.* from t_map_warehouse_result_rasp x)  a
inner join /*tdv_map_shop_copy*/ s_shop b on a.id_shop_result = b.shopid
left join s_all_mat c on a.art = c.art and a.asize = c.asize
left join t_map_shop_post_result_rasp d on a.art = d.art and a.id_shop = d.id_shop
where lgtype = 8 
and fond_boxty is not null 
and a.id_shop is not null
group by a.art, /*a.fond_boxty,*/  b.shopnum, c.matnr, a.asize, d.postno;

  --CREATE OR REPLACE FORCE VIEW "FIRM"."MAP_SAP_RESULT_W_8_WB_IM_FOND" ("ART", "VERME", "BOXTY", "SHOPNUM", "MATNR", "ASIZE", "POSTGR") AS 
  select a.art, sum(a.kol) verme, fond_boxty boxty , /*b.shopnum*/ a.id_shop shopnum, c.matnr, case when mod(a.asize,1)!=0 then to_char(a.asize) else to_char( a.asize)||',0' end asize, d.postno postgr
from (select case when id_shop = 'fond' then '2108' else id_shop end id_shop_result, x.* from t_map_warehouse_result_rasp x)  a
--inner join /*tdv_map_shop_copy*/ s_shop b on a.id_shop_result = b.shopid
left join s_all_mat c on a.art = c.art and a.asize = c.asize
left join t_map_shop_post_result_rasp d on a.art = d.art and a.id_shop = d.id_shop
where lgtype = 8 
--and fond_boxty is not null 
--and a.id_shop is not null
and a.id_shop in ('wb', 'im')
group by a.art, a.fond_boxty,  /*b.shopnum*/ a.id_shop, c.matnr, a.asize, d.postno;

select * from MAP_SAP_RESULT_W_8_WB_IM_FOND;