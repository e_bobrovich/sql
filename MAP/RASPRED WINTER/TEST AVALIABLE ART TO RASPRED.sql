select * from t_map_new_otbor where analog = '1934091';

select x.id_shop, x.art, x.asize
from TDV_NEW_SHOP_OST x
where x.verme > 0
and x.art = '1934091'
and id_shop = '0010'
;
                                        
select * from tdv_map_stock2;
select * from tdv_map_without_stock;

select id_shop, art, asize, sum(verme) kol_trans from TDV_NEW_SHOP_OST group by id_shop, art, asize;


select * from s_art where mod != ' ';
select * from TDV_MAP_NEW_ANAL;
select * from s_art where art = '1735011/1';


select distinct a.asize
from T_MAP_NEW_OTBOR a
left join (
          select asize, sum(kol) kol
         from t_map_new_poexalo x
         --left join TDV_MAP_NEW_ANAL y on x.art = y.art
         where id_shop = '0047'
              --and nvl(y.analog, x.art) = '1834095'
               and x.text1 = '1834095'
               and id = 9655
         group by asize
         ) c on a.asize = c.asize
where a.analog = '1834095'
     and a.kol > 0
     --and c.asize is null
     and ('0047', '1834095', a.asize) not in
     (
        select x.id_shop, x.art, x.asize
        from TDV_NEW_SHOP_OST x
        where x.verme > 0
              and x.art = '1834095'
              and id_shop = '0047'
      ) -- которых нет в транзитах
order by a.asize;

select * from TDV_NEW_SHOP_OST where id_shop = '0047' and art = '1834095';

select distinct a.asize
from T_MAP_NEW_OTBOR a
left join s_art e on a.art = e.art
left join (select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2
         from TDV_MAP_NEW_OST_SALE
         where id_shop = '0001'
               and art = '1833060'
         group by asize) b on a.asize = b.asize
left join (select asize, sum(kol) kol
         from t_map_new_poexalo x
         where id_shop = '0001'
               and x.text1 = '1833060'
               and id = 9695
         group by asize) c on a.asize = c.asize
where a.analog = '1834095'
     and a.kol > 0
    --and b.asize is null
    --and c.asize is null
     and nvl(b.kol_sale, 0) >= 0
     and nvl(b.kol_ost2, 0) + nvl(c.kol, 0) <= 1
     and ('0001', '1833060', a.asize) not in
     (select x.id_shop, x.art, x.asize
          from TDV_NEW_SHOP_OST x
          where x.verme > 0
                and x.art = '1833060') -- которых нет в транзитах
order by a.asize
;