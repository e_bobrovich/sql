select 
--id_shop, art, nvl(analog, art) analog, asize, sum(kol) kol, 'TRANS' ost_type
--from TDV_NEW_SHOP_OST 
sum(kol)
from (
    select distinct a.id_shop, a.art, nvl(a.analog, a.art) analog, a.asize, a.kol, 'TRANS' from ( 
      select distinct y.shopid id_shop, x3.art, a2.analog, to_number(asize, '9999.99') asize, 1 kol, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join s_shop y on x3.kunnr = y.shopnum
      left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
      where x3.boxty = ' '
            and to_number(asize, '9999.99') != 0 -- россыпь
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
            and verme > 0
            and shopid is not null
      
      union
      
      select distinct y.shopid id_shop, a.art, a2.analog, to_number(b.asize, '9999.99') asize, 1 kol, 'TRANS'
      from tdv_map_new_sap_reserv2 a
      inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
      inner join s_shop y on a.kunnr = y.shopnum
      left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
      where a.boxty != ' ' --короба
            and verme > 0
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
      
      union
      select distinct x.shopid id_shop, x3.art, a2.analog, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 1 kol, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST
                 group by boxty) e on x3.boxty = e.boxty
      left join s_shop x on x3.kunnr = x.shopnum
      left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
      where kunnr not in ('X_NERASP', 'X_SD_USE')
            and trans = 'X'
      group by a2.analog, x3.art, nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid
    ) a inner join s_art c on a.art = c.art  
    where c.groupmw in ('Мужские', 'Женские') and a.art not like '%/О%' and ',Зимняя,' like '%,' || c.season || ',%' and id_Shop =  '3623'
    
    union all
    select a1.skladid id_shop, a1.art, nvl(d.analog, a1.art) analog, asize,  sum(kol) kol, 'TRANS'
    from trans_in_way a1
    left join TDV_MAP_NEW_ANAL d on a1.art = d.art
    left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                     and a1.Postno_Lifex = e.id
    inner join s_art c on a1.art = c.art
    where scan != ' '
         and ',Зимняя,' like '%,' || c.season || ',%'
         and a1.art not like '%/О%'
         and ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%'))
         --and substr(a1.skladid, 1, 2) in ('36', '38', '39', '40')
    group by a1.skladid,  a1.art, nvl(d.analog, a1.art), asize
    having sum(kol) > 0
    
    union
    -- остатки РЦ и СВХ
    select a2.id_shop, a2.art, d.analog, a2.asize , sum(kol) kol, 'TRANS'
    from (
    --СВХ
      select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
      from /*sklad_ost_owners*/
      (
        select shopid skladid,shopidto shopid,art,asize,scan
        from (select row_number() over(partition by b.scan order by b.dated desc) as nr,
              b.shopid,b.shopidto,b.art,b.asize,b.scan
              from kart_v b
              inner join (select a.id_shop,a.art,a.asize,a.scan,sum(kol) kol
                          from e_osttek_online a
                          where substr(a.id_shop,1,1) = 'W' and a.scan != ' '
                          group by a.id_shop,a.art,a.asize,a.scan
                          having sum(kol) > 0) c on c.id_shop = b.shopid and c.art = b.art and c.asize = b.asize and c.scan = b.scan
              where b.kvid in (0,2,1,5)
              and substr(b.shopid,1,1) = 'W' and b.scan != ' ')
        where nr = 1
      )
      where shopid not in ('0', ' ')
            and shopid in (select shopid
                           from st_shop z
                           where z.org_kod = 'SHP') --and 1 = 0
            --and case when i_siberia_in = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
      group by skladid, shopid, art, asize
      
      union all
      --РЦ
      select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
      from dist_center.e_osttek
      where owner in (select shopid
                      from st_shop z
                      where z.org_kod = 'SHP')
      --and case when i_siberia_in = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
      group by DCID, owner, art, asize
      having sum(kol) > 0
    ) a2
    inner join s_art c on a2.art = c.art
    left join TDV_MAP_NEW_ANAL d on d.art = a2.art
    where ',Зимняя,' like '%,' || c.season || ',%'
          and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')
    and (a2.art like '%/О%' /*or j.art is null*/ -- только то, что плохо продавалось
    or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') --из сибири не забираем
    )
    group by a2.id_shop, a2.asize, d.analog, a2.art
    having sum(a2.kol) > 0
) 
where id_Shop =  '3623'
--group by id_shop, analog, art, asize having sum(kol) > 0
;