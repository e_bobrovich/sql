--номер магазина    артикул (если это аналог, то тот артикул, который попал в распределение, а не сам аналог)   размер  кол-во
--S11124   1931096   38,0   F19310956-F380   1
select b.shopnum, a.id_shop, a.art, a.text1, a.asize, a.kol, c.matnr from t_map_new_poexalo a
inner join tdv_map_shop_copy b on a.id_shop = b.shopid
left join s_all_mat c on a.art = c.art and a.asize = c.asize
where a.id = 9917

;

select q.shopnum, q.id_shop, q.asize, q.matnr, q.kol_ost, f.postno postno from tem_bep_shop_ost q
left join tdv_shop_post_limit_result f on q.art = f.art and q.id_shop = f.id_shop
;


select nvl(v.shopnum, b.shopnum) shopmun, q.id_shop, q.art, q.asize, c.matnr, f.postno, q.kol_ost from (
  select id_shop, art, analog, asize, sum(kol) kol_ost from (
    select a2.id_shop, a2.art, nvl(d.analog, a2.art) analog, a2.asize, sum(a2.kol) kol, 'OST' ost_type
    from e_osttek_online a2
    inner join s_art c on a2.art = c.art
    left join pos_brak x on a2.scan = x.scan
                            and a2.id_shop = x.id_shop
    left join st_shop e on a2.id_shop =
    e.shopid
    left join TDV_MAP_NEW_ANAL d on d.art = a2.art
    -- фильтрация 
    where ',Зимняя,' like '%,' || c.season || ',%'
          and a2.scan != ' '
          and a2.procent = 0
          and x.scan is null
          --and z.art is null
          and c.groupmw in ('Мужские', 'Женские')
          and a2.art not like '%/О%'
          and a2.id_shop in (select shopid
                             from tdm_map_new_shop_group
                             WHERE s_group = 18001)
          and c.art not in (select art from t_map_new_art
              union select '1715000/1' from dual
              union select '1715001/1' from dual)
          --and a2.ex_time < trunc(sysdate) 
    group by a2.id_shop, a2.art, nvl(d.analog, a2.art), a2.asize
    having sum(kol) > 0
    
    union all 
    
    select id_shop, art, nvl(analog, art) analog, asize, sum(kol) kol, 'TRANS' ost_type
    --from TDV_NEW_SHOP_OST 
    from (
        select a.id_shop, a.art, nvl(a.analog, a.art) analog, a.asize, a.kol, 'TRANS' from ( 
          select distinct y.shopid id_shop, x3.art, a2.analog, to_number(asize, '9999.99') asize, 1 kol, 'TRANS'
          from tdv_map_new_sap_reserv2 x3
          left join s_shop y on x3.kunnr = y.shopnum
          left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
          where x3.boxty = ' '
                and to_number(asize, '9999.99') != 0 -- россыпь
                and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
                and verme > 0
                and shopid is not null
          
          union
          
          select distinct y.shopid id_shop, a.art, a2.analog, to_number(b.asize, '9999.99') asize, 1 kol, 'TRANS'
          from tdv_map_new_sap_reserv2 a
          inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
          inner join s_shop y on a.kunnr = y.shopnum
          left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
          where a.boxty != ' ' --короба
                and verme > 0
                and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
          
          union
          select distinct x.shopid id_shop, x3.art, a2.analog, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 1 kol, 'TRANS'
          from tdv_map_new_sap_reserv2 x3
          left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
          left join (select boxty, sum(kol) kol
                     from T_SAP_BOX_ROST
                     group by boxty) e on x3.boxty = e.boxty
          left join s_shop x on x3.kunnr = x.shopnum
          left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
          where kunnr not in ('X_NERASP', 'X_SD_USE')
                and trans = 'X'
          group by a2.analog, x3.art, nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid
        ) a inner join s_art c on a.art = c.art  
        where c.groupmw in ('Мужские', 'Женские') and a.art not like '%/О%' and ',Зимняя,' like '%,' || c.season || ',%'
        
        union
        select a1.skladid id_shop, a1.art, nvl(d.analog, a1.art) analog, asize, sum(kol), 'TRANS'
        from trans_in_way a1
        left join TDV_MAP_NEW_ANAL d on a1.art = d.art
        left join tdv_map_ex_transit e on a1.skladid = e.id_shop and a1.Postno_Lifex = e.id
        where scan != ' '
             and ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%'))
             and substr(a1.skladid, 1, 2) in ('36', '38', '39', '40')
        group by a1.skladid,a1.art, nvl(d.analog, a1.art), asize
        having sum(kol) > 0
        
        
        union
        -- остатки РЦ и СВХ
        select a2.id_shop, a2.art, d.analog, a2.asize , sum(kol) kol, 'TRANS'
        from (
        --СВХ
          select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
          from /*sklad_ost_owners*/
          (
            select shopid skladid,shopidto shopid,art,asize,scan
            from (select row_number() over(partition by b.scan order by b.dated desc) as nr,
                  b.shopid,b.shopidto,b.art,b.asize,b.scan
                  from kart_v b
                  inner join (select a.id_shop,a.art,a.asize,a.scan,sum(kol) kol
                              from e_osttek_online a
                              where substr(a.id_shop,1,1) = 'W' and a.scan != ' '
                              group by a.id_shop,a.art,a.asize,a.scan
                              having sum(kol) > 0) c on c.id_shop = b.shopid and c.art = b.art and c.asize = b.asize and c.scan = b.scan
                  where b.kvid in (0,2,1,5)
                  and substr(b.shopid,1,1) = 'W' and b.scan != ' ')
            where nr = 1
          )
          where shopid not in ('0', ' ')
                and shopid in (select shopid
                               from st_shop z
                               where z.org_kod = 'SHP') --and 1 = 0
                --and case when i_siberia_in = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
          group by skladid, shopid, art, asize
          
          union all
          --РЦ
          select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
          from dist_center.e_osttek
          where owner in (select shopid
                          from st_shop z
                          where z.org_kod = 'SHP')
          --and case when i_siberia_in = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
          group by DCID, owner, art, asize
          having sum(kol) > 0
        ) a2
        inner join s_art c on a2.art = c.art
        left join TDV_MAP_NEW_ANAL d on d.art = a2.art
        where ',Зимняя,' like '%,' || c.season || ',%'
              and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')
        and (a2.art like '%/О%' /*or j.art is null*/ -- только то, что плохо продавалось
        or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') --из сибири не забираем
        )
        group by a2.id_shop, a2.asize, d.analog, a2.art
        having sum(a2.kol) > 0
    ) group by id_shop, analog, art, asize having sum(kol) > 0
  ) group by id_shop, analog, art, asize having sum(kol) > 0
) q 
inner join (
  select id_Shop, art from t_map_new_poexalo where id = 9917 group by id_Shop, art
) z on q.id_shop = z.id_shop and q.art = z.art
left join s_all_mat c on q.art = c.art and q.asize = c.asize
left join tdv_map_shop_copy b on q.id_shop = b.shopid
left join t_map_shop_redirect v on q.id_shop = v.id_shop_virtual
left join tdv_shop_post_limit_result f on q.art = f.art and q.id_shop = f.id_shop
where q.id_Shop = '0001'
;