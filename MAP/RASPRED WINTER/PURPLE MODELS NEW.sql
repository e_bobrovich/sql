select a.id_shop, nvl(b.art, a.art), a1.mod  from tdv_test100 a
left join TDV_MAP_NEW_ANAL b on a.art = b.analog
left join s_art a1 on a1.art = nvl(b.art, a.art)
where a.id_shop = '0001';

select 
a.id_shop, a1.mod, a1.art, q.art_cnt, a.art analog /*analog*/, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, z.order_number
--a.id_shop, a1.mod, a1.art, q.art_cnt, a.art analog, a1.groupmw, z.order_number
from tdv_test100 a
left join TDV_MAP_NEW_ANAL a2 on a.art = a2.analog
left join s_art a1 on a1.art = nvl(a2.art, a.art)
left join t_map_assort_order z on a1.assort = z.assort
inner join tdm_map_new_shop_group x on x.s_group = 17901
                                       and a.id_shop = x.shopid
                                       and x.season_in like '%' || a1.season || '%'
left join tdv_map_shop_reit b on a.id_shop = b.id_shop
left join (
  select y1.mod, x1.art, sum(kol) art_cnt
  from T_MAP_NEW_OTBOR x1
  left join TDV_MAP_NEW_ANAL b on x1.art = b.art
  left join s_art y1 on x1.art = y1.art
  group by y1.mod, x1.art having sum(kol) > 0
) q on a1.mod = q.mod and a.art = q.art         
left join (select distinct analog
                 from tdv_map_stock2) c1 on a.art = c1.analog
left join (select id_shop
                 from tdv_map_without_stock s) d1 on a.id_shop = d1.id_shop
where a1.groupmw in ('Мужские', 'Женские')
and case when substr(a.id_shop,1,2) = '00' then ' ' else q.art end not like '%Р2%'
and not (c1.analog is not null and d1.id_shop is not null)
and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032')
and a.id_shop = '0001'

and (a1.mod, a.art,a1.art) in (select b.mod, a.text1, a.art from t_map_new_poexalo a
  left join s_art b on a.art = b.art
  where a.id = 9716 and a.id_shop = '0001' group by b.mod, a.text1, a.art
)
group by a.id_shop, a1.mod, a1.art, q.art_cnt, a.art, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, z.order_number
order by nvl(b.reit2, 0), z.order_number, /*a1.mod,*/ nvl(q.art_cnt,0) desc
;

select b.mod, a.text1, a.art from t_map_new_poexalo a
left join s_art b on a.art = b.art
where a.id = 9716 and a.id_shop = '0001' group by b.mod, a.text1, a.art;

--delete from t_map_new_poexalo where id = 9736 and id_shop = '2538' and art = '1831025/1';

select sum(kol) from t_map_new_otbor;
--art      analog    mod
--1831026	1831026	1831025
--1831026/1	1831026	1831025/1
--1831025	1831025	1831025
--1831025/1	1831025	1831025/1
--1831025/О	1831025	1831025/О
--1831025	1831025	1831025
--1831026	1831026	1831025
--1831027	1831027	1831025
--1831028	1831028	1831025
select distinct a.asize
from T_MAP_NEW_OTBOR a
where a.analog = '1831028' 
--a.art = '1831026'
     and a.kol > 0
     and ('1831025') not in (select distinct 
                            --a.art,a.text1,
                            c.mod -- которые не едут в магазин
                           from t_map_new_poexalo a
                           --left join TDV_MAP_NEW_ANAL b on a.art = b.art
                           left join s_art c on a.art = c.art
                           where a.id = 9736
                                 and id_shop = '2538'
                                 and (/*nvl(b.analog, a.art)a.art = '141003' or */c.mod = '1831025'))
     --and nvl(d.limit, 1000) > nvl(e.limit, 0)
     and case when substr('2538',1,2) = '00' then ' ' else a.art end not like '%Р2%'
order by a.asize
;

select '2538', art, 'Зимняя', 1, asize, 1, id_shop, 9736, systimestamp, null boxty, 
case when 5 is null then 6 else 5 end,
null owner,
       '141001' text1, null text2, null text3
from (select a.*
       from T_MAP_NEW_OTBOR a
       left join s_art b on a.art = b.art
       where a.analog = '141000'
             --and b.mod = '141000'
             --and asize = '37'
             and kol > 0
             and (nvl(null, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
             and case when substr('2538',1,2) = '00' then ' ' else a.art end not like '%Р2%')
where rownum = 1;