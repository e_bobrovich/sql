select * from T_MAP_BLOCK_ARTS_DOP;
select * from T_MAP_SAP_BLOCK_ART;

insert into T_MAP_SAP_BLOCK_ART
select a.art from (
  select distinct b.ART from T_MAP_BLOCK_ARTS_DOP a
  inner join s_all_mat b on a.ART = b.MATNR
) a
left join t_map_block_art_summer_raspred b on a.art = b.art
where b.art is null;

select distinct b.ART from T_MAP_BLOCK_ARTS_DOP a
inner join s_all_mat b on a.ART = b.MATNR;


select * from t_Map_new_poexalo where id = 16796
and art in (select art from T_MAP_SAP_BLOCK_ART);


select * from T_MAP_NEW_POEXALO where id = 16796 ;

select * from t_map_new_otbor_rasp;
select * from T_ALL_RASP_BOX_WORK_RASP;

--delete from t_map_new_otbor_rasp where art in (
--  select art from T_MAP_SAP_BLOCK_ART
--);

select * from t_map_multi_raspred_shop_limit;

--------------------------
-- ОБНОВЛЕНИЕ ЛИМИТОВ
delete from t_map_multi_raspred_shop_limit;

insert into t_map_multi_raspred_shop_limit 
select id_shop, sum(count_limit_in) count_limit_in, 0 from (
  select shopid id_shop, count_limit_in from tdm_map_new_shop_group where s_group = 47201
  union 
  select id_Shop,  sum(kol) * -1 count_limit_in from T_MAP_NEW_POEXALO 
  where id = 16796 and art not in (select art from T_MAP_SAP_BLOCK_ART) and id_shop not in ('fond', 'im', 'wb')
  group by id_shop
) group by id_shop
;
--------------------------

--------------------------
-- ОБНОВЛЕНИЕ ОСТАТКОВ
delete from t_map_new_otbor_rasp;

insert into t_map_new_otbor_rasp
select id_shop, art, season, kol, asize, analog, status_flag
from t_map_new_otbor_after where id = 16796;

delete from t_map_new_otbor_rasp where art in (
  select art from T_MAP_SAP_BLOCK_ART
);

select * from t_map_new_otbor_after where art in (
  select art from T_MAP_SAP_BLOCK_ART
) and id = 16796;

select sum(kol) from t_map_new_otbor_after where id = 16796;
select sum(kol) from t_map_new_otbor_after where id = 16838;
select sum(kol) from t_map_new_otbor_rasp;
--------------------------

--------------------------
-- ОБНОВЛЕНИЕ ОСТАТКОВ МАГАЗИНОВ + РАСПРЕДЕЛЕНИЕ

select * from TDV_NEW_SHOP_OST_RASP;

insert into TDV_NEW_SHOP_OST
select * from TDV_NEW_SHOP_OST_RASP;

--delete from TDV_NEW_SHOP_OST_RASP;

insert into TDV_NEW_SHOP_OST_RASP
--select id_shop, art, asize, verme, location from TDV_NEW_SHOP_OST
--union all
select a2.id_shop, a2.text1 analog, a2.asize, 1, 'TRANS'
FROM t_map_new_Poexalo a2
where id = 16796 and art not in (select art from T_MAP_SAP_BLOCK_ART) and id_shop not in ('fond', 'im', 'wb')
GROUP BY a2.id_shop, a2.text1, a2.asize
having sum(kol) > 0;

select a.art, b.normt from T_MAP_SAP_BLOCK_ART a
inner join s_art b on a.art = b.art