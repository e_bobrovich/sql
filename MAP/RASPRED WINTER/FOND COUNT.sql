select analog, sum(kol) from t_map_new_otbor_rasp
group by analog
having sum(kol) > 400;

select * from tdv_map_new_ost_sale_rasp;

select a.art analog, a.season, --a.id_Shop, 
sum(kol_sale) kol_sale,sum(kol_ost2),
sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef
from /*tdv_map_new_ost_sale_rasp*/ tdv_map_new_ost_sale_rasp a
inner join (
  select x.analog, sum(x.kol) kol
  from /*t_map_new_otbor_rasp*/ t_map_new_otbor_before x
  where x.id = 15342
  
  group by x.analog
  having sum(x.kol) >= 400
) b on a.art = b.analog
group by a.art, a.season--, a.id_Shop
having(sum(kol_sale) + sum(kol_ost) != 0) and (sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 >= 50) 
;

select boxty, sum(kol) from t_sap_box_rost group by boxty order by sum(kol);
select * from T_SAP_BOX_ROST order by boxty, asize;

select * 
from /*t_map_new_otbor_rasp*/ t_map_new_otbor_before a
where analog = '1818000' 
and a.id = 15342
order by asize
;

select x.boxty, y.count, y.kol_pair, y.min_asize, y.max_asize from (
  select boxty, count(*) count  from (
    select analog, asize
    from /*t_map_new_otbor_rasp*/ t_map_new_otbor_before
    where analog = '1818000'
    and id = 15342
    group by analog, asize
  )a
  inner join t_sap_box_rost b on a.asize = b.asize
  group by boxty           
) x
inner join (
  select boxty, count(*) count, sum(kol) kol_pair, min(asize) min_asize, max(asize) max_asize
  from t_sap_box_rost 
  group by boxty
  having sum(kol) = 5 and  count(*) = 5-- ростовки с 5 парами и ?5 размерами?
) y on x.boxty = y.boxty
where x.count = y.count
order by case when 'T' = 'T' then 100+y.min_asize else 100-y.max_asize end
--y.kol_pair desc, y.count desc

;

select boxty, count(*) count, case when count(*) = 5 then 'T' else 'F' end is_actual  from (
  select analog, asize
  from t_map_new_otbor_rasp
  where analog = '1818000'
  group by analog, asize
  having sum(kol) > 0
)a
inner join t_sap_box_rost b on a.asize = b.asize and b.boxty = 'B3'
group by boxty;

select * from t_sap_box_rost 
where boxty = 'A5' 
order by boxty, asize;

select distinct a.asize, b.boxty
from /*t_map_new_otbor_rasp*/ t_map_new_otbor_before a
inner join t_sap_box_rost b on /*r_boxty/bxoty*/ 'A9' = b.boxty and a.asize = b.asize
where /*a.art = r_row.art*/ a.analog = /*r_analog.analog*/ '1818000'
     and a.kol > 0
     and a.id = 15342
order by a.asize
;


select 'fond', art, /*r_row.season*/ 'Летняя', 1, asize, 1, id_shop, /*i_id*/ 15342, systimestamp, null boxty,
       0 block_no, null owner, /*r_analog.analog*/ '1818000' text1, /*r_boxty.boxty*/ 'A9' text2, 'ROS' text3
from (select a.*
       from /*t_map_new_otbor_rasp*/ t_map_new_otbor_before a
       where analog = /*r_analog.analog*/ '1818000'
             and asize = /*r_size.asize*/ 36
             and kol > 0
             
             and id = 15342
             order by a.kol)
where rownum = 1;
