--select count(*) from (

  select 
    distinct
    w.id_shop "ПОЛУЧАТЕЛЬ", 
    w.shopname "НАЗВАНИЕ",
    e.art "АРТИКУЛ",
    e.analog "АНАЛОГ",
    m.mod "МОДЕЛЬ",
    e.asize "РАЗМЕР",
    nvl(e.kol_ost,0) "ОСТАТОК РАЗМЕРА",
    nvl(e.kol_poex,0) "РАСПРЕДЕЛЕНО",
    nvl(e.kol_sale_02, 0) "ПРОДАЖИ РАЗМЕР 02",
    nvl(e.kol_sale_all, 0) "ПРОДАЖИ РАЗМЕР",
    e.block_no "ЭТАП"--,
  --  g.analog
  from 
  (
    select a.shopid id_shop, с.shopname  from tdm_map_new_shop_group a 
    left join s_shop b on a.shopid = b.shopid
    left join s_shop с on a.shopid = с.shopid
    where a.s_group = 17901
  ) w
  left join
  (
    select 
    coalesce(q.id_shop, p.id_shop, c1.id_shop) id_shop, 
    coalesce(q.analog, p.analog, c1.analog) analog, 
    coalesce(q.art, p.art, c1.art) art, 
    coalesce(q.asize, p.asize, c1.asize) asize, 
    q.kol_ost, 
    p.kol_poex, 
    c1.kol_sale kol_sale_all,
    d1.kol_sale kol_sale_02,
    p.block_no
    from 
    (
      select id_shop, art, analog, asize, sum(kol) kol_ost from (
        select a2.id_shop, a2.art, nvl(d.analog, a2.art) analog, a2.asize, sum(a2.kol) kol, 'OST' ost_type
        from e_osttek_online a2
        inner join s_art c on a2.art = c.art
        left join pos_brak x on a2.scan = x.scan
                                and a2.id_shop = x.id_shop
        left join st_shop e on a2.id_shop = e.shopid
        left join TDV_MAP_NEW_ANAL d on d.art = a2.art
        -- фильтрация 
        where ',Зимняя,' like '%,' || c.season || ',%'
              and a2.scan != ' '
              and a2.procent = 0
              and x.scan is null
              --and z.art is null
              and c.groupmw in ('Мужские', 'Женские')
              and a2.art not like '%/О%'
              and a2.id_shop in (select shopid
                                 from tdm_map_new_shop_group
                                 WHERE s_group = 17901)
              and c.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
              --and a2.ex_time < trunc(sysdate) 
        group by a2.id_shop, a2.art, nvl(d.analog, a2.art), a2.asize
        having sum(kol) > 0
        
        union all 
        
        select id_shop, art, nvl(analog, art) analog, asize, sum(kol) kol, 'TRANS' ost_type
        --from TDV_NEW_SHOP_OST 
        from (
            select a.id_shop, a.art, nvl(a.analog, a.art) analog, a.asize, a.kol, 'TRANS' from ( 
              select distinct y.shopid id_shop, x3.art, a2.analog, to_number(asize, '9999.99') asize, 1 kol, 'TRANS'
              from tdv_map_new_sap_reserv2 x3
              left join s_shop y on x3.kunnr = y.shopnum
              left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
              where x3.boxty = ' '
                    and to_number(asize, '9999.99') != 0 -- россыпь
                    and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
                    and verme > 0
                    and shopid is not null
              
              union
              
              select distinct y.shopid id_shop, a.art, a2.analog, to_number(b.asize, '9999.99') asize, 1 kol, 'TRANS'
              from tdv_map_new_sap_reserv2 a
              inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
              inner join s_shop y on a.kunnr = y.shopnum
              left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
              where a.boxty != ' ' --короба
                    and verme > 0
                    and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
              
              union
              select distinct x.shopid id_shop, x3.art, a2.analog, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 1 kol, 'TRANS'
              from tdv_map_new_sap_reserv2 x3
              left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
              left join (select boxty, sum(kol) kol
                         from T_SAP_BOX_ROST
                         group by boxty) e on x3.boxty = e.boxty
              left join s_shop x on x3.kunnr = x.shopnum
              left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
              where kunnr not in ('X_NERASP', 'X_SD_USE')
                    and trans = 'X'
              group by a2.analog, x3.art, nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid
            ) a inner join s_art c on a.art = c.art  
            where c.groupmw in ('Мужские', 'Женские') and a.art not like '%/О%' and ',Зимняя,' like '%,' || c.season || ',%'
            
            union
            select a1.skladid id_shop, a1.art, nvl(d.analog, a1.art) analog, asize, sum(kol), 'TRANS'
            from trans_in_way a1
            left join TDV_MAP_NEW_ANAL d on a1.art = d.art
            left join tdv_map_ex_transit e on a1.skladid = e.id_shop and a1.Postno_Lifex = e.id
            where scan != ' '
                 and ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%'))
                 and substr(a1.skladid, 1, 2) in ('36', '38', '39', '40')
            group by a1.skladid,a1.art, nvl(d.analog, a1.art), asize
            having sum(kol) > 0
            
            
            union
            -- остатки РЦ и СВХ
            select a2.id_shop, a2.art, d.analog, a2.asize , sum(kol) kol, 'TRANS'
            from (
            --СВХ
              select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
              from /*sklad_ost_owners*/
              (
                select shopid skladid,shopidto shopid,art,asize,scan
                from (select row_number() over(partition by b.scan order by b.dated desc) as nr,
                      b.shopid,b.shopidto,b.art,b.asize,b.scan
                      from kart_v b
                      inner join (select a.id_shop,a.art,a.asize,a.scan,sum(kol) kol
                                  from e_osttek_online a
                                  where substr(a.id_shop,1,1) = 'W' and a.scan != ' '
                                  group by a.id_shop,a.art,a.asize,a.scan
                                  having sum(kol) > 0) c on c.id_shop = b.shopid and c.art = b.art and c.asize = b.asize and c.scan = b.scan
                      where b.kvid in (0,2,1,5)
                      and substr(b.shopid,1,1) = 'W' and b.scan != ' ')
                where nr = 1
              )
              where shopid not in ('0', ' ')
                    and shopid in (select shopid
                                   from st_shop z
                                   where z.org_kod = 'SHP') --and 1 = 0
                    --and case when i_siberia_in = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
              group by skladid, shopid, art, asize
              
              union all
              --РЦ
              select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
              from dist_center.e_osttek
              where owner in (select shopid
                              from st_shop z
                              where z.org_kod = 'SHP')
              --and case when i_siberia_in = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
              group by DCID, owner, art, asize
              having sum(kol) > 0
            ) a2
            inner join s_art c on a2.art = c.art
            left join TDV_MAP_NEW_ANAL d on d.art = a2.art
            where ',Зимняя,' like '%,' || c.season || ',%'
                  and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')
            and (a2.art like '%/О%' /*or j.art is null*/ -- только то, что плохо продавалось
            or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') --из сибири не забираем
            )
            group by a2.id_shop, a2.asize, d.analog, a2.art
            having sum(a2.kol) > 0
        ) group by id_shop, analog, art, asize having sum(kol) > 0
      ) group by id_shop, analog, art, asize having sum(kol) > 0
    ) q 
    full join 
    (
      select id_shop, art,  text1 analog, asize, sum(kol) kol_poex, block_no from t_map_new_poexalo where id = 9917 
      group by id_shop, text1, art, asize, block_no having sum(kol) > 0
    ) p on q.id_shop = p.id_shop and q.art = p.art and q.analog = p.analog and q.asize = p.asize 
    full join
    (
      select y2.id_shop, nvl(z2.analog, y2.art) analog, y2.art, y2.asize, sum(y2.kol) kol_sale from pos_sale1 x2 
      inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
      left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
      where y2.art in (select art from s_art where season = 'Зимняя')
      and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
      and y2.scan != ' ' and y2.procent = 0
      and trunc(x2.sale_date) >= trunc(to_date('01.09.2018','dd.mm.yyyy'))
      and trunc(x2.sale_date) < trunc(sysdate)
      --and x2.id_shop = '0001' and  nvl(z2.analog, y2.art) = '141001'
      group by y2.id_shop, nvl(z2.analog, y2.art), y2.art, y2.asize
    ) c1 on coalesce(q.id_shop, p.id_shop) = c1.id_shop 
        and coalesce(q.analog, p.analog) = c1.analog 
        and coalesce(q.art, p.art) = c1.art 
        and coalesce(q.asize, p.asize) = c1.asize
    full join 
    (
      select y3.id_shop, nvl(z3.analog, y3.art) analog, y3.art, y3.asize, sum(y3.kol) kol_sale from pos_sale1 x3 
      inner join pos_sale2 y3 on x3.id_shop = y3.id_shop and x3.id_chek = y3.id_chek
      left join TDV_MAP_NEW_ANAL z3 on y3.art = z3.art
      where y3.art in (select art from s_art where season = 'Зимняя')
      and x3.bit_close = 'T' and x3.bit_vozvr = 'F'
      and y3.scan != ' ' and y3.procent = 0
      and trunc(x3.sale_date) >= trunc(to_date('01.09.2018','dd.mm.yyyy'))
      and trunc(x3.sale_date) < trunc(to_date('01.02.2019','dd.mm.yyyy'))
      group by y3.id_shop, nvl(z3.analog, y3.art), y3.art, y3.asize
    ) d1 on coalesce(q.id_shop, p.id_shop, c1.id_shop) = d1.id_shop 
        and coalesce(q.analog, p.analog, c1.analog) = d1.analog  
        and coalesce(q.art, p.art, c1.art) = d1.art 
        and coalesce(q.asize, p.asize, c1.asize) = d1.asize
    --where nvl(nvl(q.id_shop, p.id_shop), c1.id_shop) = '0001' and nvl(nvl(q.analog, p.analog), c1.analog) = '141001'
    --where (c1.analog != p.analog or c1.analog != q.analog)
  ) e on w.id_shop = e.id_shop
  left join T_MAP_ART_ANALOG_MOD m on e.art = m.art
  left join (
    select j.id_shop, j.text1 analog from t_map_new_poexalo j 
                where j.id = 9917
                group by j.id_shop, j.text1 having sum(j.kol) > 0
  ) g on w.id_shop = g.id_shop and e.analog = g.analog
--where 
--w.id_shop = '3623'
--and e.analog = '141001'
--and 
--g.analog is not null
--)
;


select id_shop, text1, art, asize, sum(kol) kol from t_map_new_poexalo where id = 9716 group by id_shop, text1, art, asize having sum(kol) > 0; 