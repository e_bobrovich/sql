create or replace package body bep_map_004 as

  --************** vaiable *****************************
  pv_season varchar2(100) default ',Летняя,';
  pv_filter_plan_in_shop varchar2(1) default 'F'; --исключать ли из остатков невыполненные планы на вывоз
  pv_assort_order varchar2(1) default 'F'; -- использовать ли сортировку по типу ассортимента
  --********************************************************************************************

  procedure writeLog(i_text varchar2,
                     i_proc varchar2,
                     i_id   in integer) is
  begin
    write_log(i_text, i_proc, i_id);
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2018 16:20:00
  -- Start Procedure:
  -- Заполняет таблицу данными с РЦ и СВХ
  --****************************************************************************************************************************************************
  procedure fill_rc_svx_data(i_siberia_in varchar2 default 'T') is
  begin
  
    execute immediate 'truncate table tdv_map_rc_data';
  
    -- Из СВХ Сибири ничего не забираем
  
    insert into tdv_map_rc_data
      --СВХ
      select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
      from /*sklad_ost_owners*/
      (
        select shopid skladid,shopidto shopid,art,asize,scan
        from (select row_number() over(partition by b.scan order by b.dated desc) as nr,
              b.shopid,b.shopidto,b.art,b.asize,b.scan
              from kart_v b
              inner join (select a.id_shop,a.art,a.asize,a.scan,sum(kol) kol
                          from e_osttek_online a
                          where substr(a.id_shop,1,1) = 'W' and a.scan != ' '
                          group by a.id_shop,a.art,a.asize,a.scan
                          having sum(kol) > 0) c on c.id_shop = b.shopid and c.art = b.art and c.asize = b.asize and c.scan = b.scan
              where b.kvid in (0,2,1,5)
              and substr(b.shopid,1,1) = 'W' and b.scan != ' ')
        where nr = 1
      )
      where shopid not in ('0', ' ')
           /*           
            and shopid in (select shopid
           from tdm_map_new_shop_group
           where s_group = i_s_group)
           */
            and shopid in (select shopid
                           from st_shop z
                           where z.org_kod = 'SHP') --and 1 = 0
            and case when i_siberia_in = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
      group by skladid, shopid, art, asize
      
      union all
      --РЦ
      select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
      from dist_center.e_osttek
      where owner in (select shopid
                      from st_shop z
                      where z.org_kod = 'SHP')
      and case when i_siberia_in = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
      group by DCID, owner, art, asize
      having sum(kol) > 0;
  
    commit;
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2019 16:00:00
  -- Comments: сбор всего что нужно распределить(короба)
  --
  --****************************************************************************************************************************************************
  procedure sum_all_rasp_box(i_id integer) is
  begin
    --execute immediate 'truncate table T_ALL_RASP'; -- здесь будет россыпь
  
    execute immediate 'truncate table t_all_rasp_box'; -- таблица с коробами
    execute immediate 'truncate table t_all_rasp_box_work';
  
    -- записываю короба к распределению
    insert into t_all_rasp_box
      select d.art, d.boxty, sum(d.verme) / f.kol, 0 flag, e.season, nvl(g.analog, d.art) analog
      from st_sap_ob_ost2 d
      left join s_art e on d.art = e.art
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                 group by boxty) f on d.boxty = f.boxty
      left join TDV_MAP_NEW_ANAL g on d.art = g.art
      where d.boxty not in (' ', 'N')
      and pv_season like '%,' || e.season || ',%'
      and e.groupmw in ('Мужские', 'Женские')
      and e.art not like '%/О%'
      group by d.art, d.boxty, f.kol, e.season, nvl(g.analog, d.art);
      
    -- от коробов отнимаю, что в резерве САПА
    update t_all_rasp_box x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, boxty, sum(verme) verme
                 from tdv_map_new_sap_reserv2 x3
                 where x3.boxty != ' '
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE')
                 group by art, boxty) x2
          where x1.art = x2.art
                and x1.boxty = x2.boxty)
    where exists (select x1.kol - x2.verme
           from (select art, boxty, sum(verme) verme
                  from tdv_map_new_sap_reserv2 x3
                  where x3.boxty != ' '
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE')
                  group by art, boxty) x2
           where x1.art = x2.art
                 and x1.boxty = x2.boxty);
  
    delete from t_all_rasp_box x
    where x.kol <= 0;
    
    delete from t_all_rasp_box
    where art in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual);
    
    commit;
  
    insert into t_all_rasp_box_work
    select a.*
    from t_all_rasp_box a;
    
    insert into t_all_rasp_box_before (id, art, boxty, kol, flag, season, analog)
    select i_id, art, boxty, kol, flag, season, analog from t_all_rasp_box_work;
      
    commit;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2019 14:00:00
  -- Comments: сбор всего что нужно распределить(россыпь)
  --
  --****************************************************************************************************************************************************
  procedure sum_all_rasp(i_id         integer,
                         i_s_group    integer,
                         i_siberia_in varchar2 default 'T') is
  begin
  
    execute immediate 'truncate table tdv_map_stock2';
    EXECUTE IMMEDIATE 'truncate table T_MAP_NEW_OTBOR';
    execute immediate 'truncate table e_osttek_online_short';
    execute immediate 'truncate table TDV_NEW_SHOP_OST'; -- таблица с транзитами и данными с заказами , лежащими на СГП
  
    -- сбор стоковых артикулов\аналогов
    insert into tdv_map_stock2
      select distinct a.art, nvl(b.analog, a.art)
      from cena_skidki_all_shops a
      left join TDV_MAP_NEW_ANAL b on a.art = b.art
      where substr(to_char(cena), -2) = '99';
  
    --перезаполняю таблицу остатков магазинов
    insert into e_osttek_online_short
      select a2.id_shop, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag,
             e.landid, a2.procent
      from e_osttek_online a2
      inner join s_art c on a2.art = c.art
      left join pos_brak x on a2.scan = x.scan
                              and a2.id_shop = x.id_shop
      --left join st_muya_art z on a2.art = z.art                        
      left join st_shop e on a2.id_shop = e.shopid
      left join TDV_MAP_NEW_ANAL d on d.art = a2.art
      -- фильтрация 
      where pv_season like '%,' || c.season || ',%'
            and a2.scan != ' '
            and a2.procent = 0 --уценка некондиция
            and x.scan is null -- брак
            --and z.art is null
            and c.groupmw in ('Мужские', 'Женские')
            and a2.art not like '%/О%'
            and a2.id_shop in (select shopid
                               from tdm_map_new_shop_group
                               WHERE s_group = i_s_group)
            --and substr(a.id_shop, 1, 2) not in ('36', '38', '39', '40')
      group by a2.id_shop, a2.art, c.season, a2.asize, nvl(d.analog, a2.art), e.landid, a2.procent
      having sum(kol) > 0;
      
    commit;
      
    map_fl.fillwarehousestock2();
    fill_rc_svx_data;
  
    -- заполнение доступного к распределению со склада  
    insert into T_MAP_NEW_OTBOR
    
      select id_shop, art, season, sum(kol), asize, analog, status_flag
      from (
             
             -- россыпь склада
             select '9999' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
             from tem_mapif_fill_warehouse_stock a
             inner join s_art b on a.art = b.art
             left join TDV_MAP_NEW_ANAL c on a.art = c.art
             where b.art not like '%/О%'
                   and pv_season like '%,' || b.season || ',%'
             group by a.art, b.season, a.asize, nvl(c.analog, b.art)
             
             UNION ALL
             -- транзиты без сибири
             select '9999', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag
             from trans_in_way a1
             left join TDV_MAP_NEW_ANAL d on a1.art = d.art
             left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                               and a1.Postno_Lifex = e.id
             inner join s_art c on a1.art = c.art
             where scan != ' '
                   and pv_season like '%,' || c.season || ',%'
                   and a1.art not like '%/О%'
                   and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
                   and not (
                      ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%')) 
                      and case when i_siberia_in = 'F' then substr(skladid, 1, 2) else ' ' end in ('36', '38', '39', '40')
                   )
             group by a1.art, c.season, asize, nvl(d.analog, a1.art)
             having sum(kol) > 0
             
             UNION ALL
             -- остатки РЦ и СВХ без сибири
             select '9999', a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
             from tdv_map_rc_data a2
             inner join s_art c on a2.art = c.art
             left join TDV_MAP_NEW_ANAL d on d.art = a2.art
             where pv_season like '%,' || c.season || ',%'
                   and a2.art not like '%/О%'
                   and substr(skladid, 1, 1) != 'W' --склады
                   and substr(skladid, 3, 1) != 'S' -- санг
                   and case when i_siberia_in = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
                   
             group by a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
             having sum(a2.kol) > 0)
      
      group by id_shop, art, season, asize, analog, status_flag;

    commit;
    
    -- очистка распределяемых от детской
    delete from T_MAP_NEW_OTBOR 
    where art in (
      select art from s_art where groupmw not in ('Мужские','Женские')
--      union all
--      select art from st_muya_art
--      union all
--      select art from t_map_new_art
    );
    commit;
    
    -- сохранение исходного состояния распределяемого
    insert into t_map_new_otbor_before (id, id_shop, art, season, kol, asize, analog, status_flag)
    select i_id, id_shop, art, season, kol, asize, analog, status_flag from t_map_new_otbor;
    commit;
    
    -- отдельно закидываю остатки магазинов, транзиты, резервы и остатки складов сибири, что бы потом каждый раз не пересчитывать {
		insert into TDV_NEW_SHOP_OST
      -- остатки магазинов 
--      select a2.id_shop, a2.analog, a2.asize, 1, 'TRANS'
--      FROM e_osttek_online_short a2
--      inner join s_art c on a2.art = c.art
--      GROUP BY a2.id_shop, a2.analog, a2.asize
--      having sum(kol) > 0
      
--      union
      select distinct y.shopid, nvl(a2.analog, x3.art), to_number(asize, '9999.99'), 1, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join s_shop y on x3.kunnr = y.shopnum
      LEFT JOIN TDV_MAP_NEW_ANAL a2 ON x3.art = a2.art
      inner join s_art c on x3.art = c.art
      where x3.boxty = ' '
            and to_number(asize, '9999.99') != 0 -- россыпь
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
            and verme > 0
            AND shopid IS NOT NULL
            and pv_season like '%,' || c.season || ',%'
    
      union
      select distinct y.shopid, nvl(a2.analog, a.art), to_number(b.asize, '9999.99'), 1, 'TRANS'
      from tdv_map_new_sap_reserv2 a
      inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
      inner join s_shop y on a.kunnr = y.shopnum
      LEFT JOIN TDV_MAP_NEW_ANAL a2 ON A.art = a2.art
      inner join s_art c on a.art = c.art
      where a.boxty != ' ' --короба
            and verme > 0
            AND substr(nvl(kunnr, ' '), 1, 2) != 'X_'
            and pv_season like '%,' || c.season || ',%'
      
      union
      select distinct x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 1, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST
                 group by boxty) e on x3.boxty = e.boxty
      left join s_shop x on x3.kunnr = x.shopnum
      LEFT JOIN TDV_MAP_NEW_ANAL a2 ON x3.art = a2.art
      inner join s_art c on x3.art = c.art
      where kunnr not in ('X_NERASP', 'X_SD_USE')
            AND trans = 'X'
            and pv_season like '%,' || c.season || ',%'
      GROUP BY nvl(a2.analog, x3.art), nvl(F.asize, to_number(x3.asize, '999.99')), x.shopid
      
      UNION
      -- транзиты (только сибири?)
      select a1.skladid id_shop, nvl(d.analog, a1.art) analog, asize, sum(kol), 'TRANS'
      from trans_in_way a1
      left join TDV_MAP_NEW_ANAL d on a1.art = d.art
      left join tdv_map_ex_transit e on a1.skladid = e.id_shop and a1.Postno_Lifex = e.id
      inner join s_art c on a1.art = c.art
      where scan != ' '
           and pv_season like '%,' || c.season || ',%'
           and a1.art not like '%/О%'
           and ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%'))
           and substr(a1.skladid, 1, 2) in ('36', '38', '39', '40')
      GROUP BY a1.skladid, nvl(D.analog, a1.art), asize
      having sum(kol) > 0
      
      union
      -- остатки РЦ и СВХ
      select a2.id_shop, nvl(d.analog, a2.art) analog, a2.asize, sum(kol), 'TRANS'
      from tdv_map_rc_data a2
      inner join s_art c on a2.art = c.art
      LEFT JOIN tdv_map_new_anal D ON D.art = a2.art
      where pv_season like '%,' || c.season || ',%'
            and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')
            --and substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40')
            and (a2.art like '%/О%'
              or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') -- для сибири учитываем остатки на РЦ и СВХ
            ) 
      group by a2.id_shop, a2.asize, nvl(d.analog, a2.art)
      having sum(a2.kol) > 0;
    
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2019 16:45:00
  -- Comments:
  --
  --****************************************************************************************************************************************************
  procedure fillBaseData2(i_s_group          integer,
                          i_sales_start_date date,
                          i_sales_end_date   date,
                          i_stock_doc_no     integer) is
  begin
    
    execute immediate 'truncate table TDV_MAP_NEW_OST_SALE'; -- - Остатки вместе с продажами в разрезе аналогов
    execute immediate 'truncate table tdv_map_osttek'; -- текущие остатки собраные в одной таблице (нужно было на случай, когда текущие
  
    -- формирую остатки на дату
    if i_stock_doc_no is null then
    
      insert into tdv_map_osttek
        select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
        from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
               from e_osttek_online a2
               inner join s_art c on a2.art = c.art
               left join pos_brak x on a2.scan = x.scan
                                       and a2.id_shop = x.id_shop
               --left join st_muya_art z on a2.art = z.art                        
                                       
               where a2.scan != ' '
                     and pv_season like '%,' || c.season || ',%' -- только нужный сезон
                     and a2.procent = 0 -- без некондиции
                     and c.groupmw in ('Мужские', 'Женские') -- без детской обуви
                     --and z.art is null -- без покупной
                     and x.scan is null -- без брака
                     and a2.art not like '%/О%'
                     and a2.id_shop in (select shopid
                                        from tdm_map_new_shop_group
                                        where s_group = i_s_group)
                     and a2.id_shop in (select shopid
                                        from st_shop z
                                        WHERE z.org_kod = 'SHP')
                     AND x.SCAN IS NULL
                     --and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40')
               group by a2.id_shop, a2.art, a2.asize, c.season
               having sum(kol) > 0)
        group by id_shop, art, asize, season;
    
    else
      
      insert into tdv_map_osttek
        select id_shop, art, asize, sum(kol) kol, season, 0 out_block, sum(kol_rc) kol_rc, sum(kol) kol
        from (select shopid id_shop, a.art, asize, sum(oste) kol, c.season, 0 out_block, 0 kol_rc
               from e_oborot2 a
               inner join s_art c on a.art = c.art
               --left join st_muya_art z on a.art = z.art    
               where id = i_stock_doc_no
                     and c.mtart != 'ZHW3'
                     and procent = 0 -- без некондиции
                     --and z.art is null -- без покупной
                     and c.groupmw in ('Мужские', 'Женские') -- без детской обуви
                     and pv_season like '%,' || c.season || ',%' -- только нужный сезон
                     and a.art not like '%/О%'
                     and a.shopid in (select shopid
                                      from tdm_map_new_shop_group
                                      WHERE s_group = i_s_group)
                     --and substr(a.shopid, 1, 2) not in ('36', '38', '39', '40')
               group by shopid, a.art, asize, c.season)
        group by id_shop, art, asize, season;
    
    end if;
    
    commit;
  
    -- Остатки вместе с продажами в разрезе аналогов
    insert into TDV_MAP_NEW_OST_SALE
      select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 0 out_block, sum(kol_rc) kol_rc,
             sum(kol2) kol2
      from (
             -- текущие остатки
             select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
             from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, z.season, null,
                            sum(kol_rc) kol_rc, sum(a2.kol2) kol2
                     from tdv_map_osttek a2
                     left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
                     left join s_art z on a2.art = z.art
                     group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize, z.season
                     having sum(kol) > 0) a
             where pv_season like '%,' || season || ',%' -- только нужный сезон        
             group by id_shop, a.art, asize, season
             
             union all
             
             -- все продажи данной обуви за выбранный период
             select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0, sum(b.kol) kol_sale, 0 kol_rc,
                     0 kol2
             from pos_sale1 a
             inner join pos_sale2 b on a.id_chek = b.id_chek
                                       and a.id_shop = b.id_shop
             inner join s_art c on b.art = c.art
             left join TDV_MAP_NEW_ANAL d on b.art = d.art
             where a.bit_close = 'T'
                   and a.bit_vozvr = 'F'
                   and b.scan != ' '
                   and b.procent = 0
                   and a.id_shop in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group)
                   and trunc(a.sale_date) >= trunc(i_sales_start_date)
                   and trunc(a.sale_date) <= trunc(i_sales_end_date)
                  AND pv_season LIKE '%,' || C.season || ',%' -- только нужный сезон  
                  --and substr(a.id_shop, 1, 2) not in ('36', '38', '39', '40')
             group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season) a
      
      group by id_shop, a.art, a.asize;
  
    commit;
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 24.09.2018 14:30:00
  -- Comments: 2) Если остаток артикула более 400 пар и его коэффициент реализации более 50 процентов, то из такого артикула формируем 10 коробов
  --
  --****************************************************************************************************************************************************
  procedure fond_count(i_id       integer, 
                       i_s_group  integer,
                       i_block_no number default null) is
                                
    v_all_size_count integer;
    v_cent_size_count integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
  begin
    null;
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 23.09.2018 14:30:00
  -- Comments: проверка на лимиты, центральные размеры, 
  -- обновление состояния распределяемого, отбора и лимитов
  --
  --****************************************************************************************************************************************************
  procedure ross_checks_and_updates(
                                  i_id                  integer,
                                  i_id_shop             varchar2,
                                  i_analog              varchar2,
                                  i_groupmw             varchar2,
                                  i_line                varchar2,
                                  i_shop_current_limit  integer default 0) is
                                
    v_all_size_count integer;
    v_cent_size_count integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
  begin
        v_shop_current_limit := i_shop_current_limit;
        
        -- запоминаем сколько пар добавляем магазину
        select sum(kol) into v_kol_add from t_map_new_poexalo 
        where id_shop = i_id_shop and id = i_id and  flag = 1;
        
        if v_kol_add is null then v_kol_add := 0; end if;
      
        -- сколько в итоге всего размеров и центральных размеров
        select count(asize),
        nvl(sum(
            case when asize in (4, 4.5, 5, 5.5)    and i_groupmw = 'Женские' and i_line != 'Sport' then 1
                 when asize in (8, 8.5, 9)         and i_groupmw = 'Мужские' and i_line != 'Sport' then 1
                 when asize in (5, 5.5, 6, 6.5)    and i_groupmw = 'Женские' and i_line = 'Sport' then 1
                 when asize in (8.5, 9, 9.5, 10)   and i_groupmw = 'Мужские' and i_line = 'Sport' then 1
                 when asize in (37, 38, 39)        and i_groupmw = 'Женские' then 1
                 when asize in (42, 43)            and i_groupmw = 'Мужские' then 1
                 else 0 end), 0) 
        into v_all_size_count, v_cent_size_count
        from (
            select distinct asize from e_osttek_online_short where id_shop = i_id_shop and art = i_analog and kol > 0
            union all
            select distinct asize from tdv_new_shop_ost where id_shop = i_id_shop and art = i_analog and verme > 0
            union all
            select distinct asize from t_map_new_poexalo where id = i_id and id_shop = i_id_shop and /*art*/ text1 = i_analog and kol > 0
        );
        
        --не менее 3 размеров, при этом 2 размера средних для женской и 1 средний для мужской
        if (v_all_size_count >= 3 
        and ((i_groupmw = 'Мужские' and v_cent_size_count >= 1) or (i_groupmw = 'Женские' and v_cent_size_count >= 2)) 
        and v_shop_current_limit - v_kol_add >= 0 )
        -- TODO проверка на лимит хранение
        then
          update T_MAP_NEW_OTBOR a
          SET kol = kol - 1
          where /*id_shop = '9999'
                and*/ analog = i_analog
                and (id_shop, art, asize) in (select id_shop_from, art, asize
                                              from t_map_new_poexalo
                                              where flag = 1
                                                    and id = i_id);
          -- вычитаем от лимита добавленное                                            
          v_shop_current_limit := v_shop_current_limit - v_kol_add;  
        else
          delete from t_map_new_poexalo
          where id = i_id
                and flag = 1;    
        end if; -- всего размеров и центральные
        
        update t_map_new_poexalo
          set flag = 0
          where flag = 1
                and id = i_id;
        
        -- обновляем текущий лимит магазина
        update t_map_multi_raspred_shop_limit set current_limit = v_shop_current_limit 
        where id_shop = i_id_shop;
  end;
  
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2018 11:00:00
  -- Comments: блок расчета распределения коробов россыпи на магазины
  --
  --****************************************************************************************************************************************************
  procedure box_count(i_id                  integer,
                       i_s_group             integer,
                       i_sales_start_date    date,
                       i_sales_end_date      date,
                       i_stock_doc_no        integer,
                       i_sale_pair_count     integer,
                       i_ost_pair_count      integer,
                       i_siberia_in          varchar2,
                       i_block_no            number default null,
                       i_sale_max_pair_count integer := 1000) is
  
    v_count integer;
    v_cent_size integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
    v_kol_in_box number := 0;
    
    v_is_add_corob varchar2(1) := 'F';
  begin
    dbms_output.put_line('  box_count i_block_no : ' || i_block_no || ' start time : ' || systimestamp);
    
    for r_row in (select x.*
                  from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, x.shop_in, sum(kol_sale) kol_sale,
                                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, x.count_limit_in,
                                D.release_year, D.kol kol_otbor, D.groupmw, d.line, d.order_number, C.group_name--, e1.min_year, e1.max_year
                       from TDV_MAP_NEW_OST_SALE a -- тут не должно быть отфильтрованной обуви и сибири
                       inner join (select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
                                         y.groupmw, y.line, z.order_number
                                  from t_all_rasp_box_work x -- доступные короба
                                  left join s_art y on x.art = y.art
                                  left join t_map_assort_order z on y.assort = z.assort
                                  -- nvl2(i_group_name, i_group_name, z.group_name)
                                  where y.groupmw in ('Мужские', 'Женские')
                                  group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw, y.line, z.order_number) d on a.art =
                                                                                                                    d.analog
                       inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                                and a.id_shop = x.shopid
                                                                and x.season_in like '%' || a.season || '%'
                        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                        left join (select distinct analog, group_name
                                   from t_map_group_ryb) c on a.art = c.analog
                        left join (select distinct analog
                                   from tdv_map_stock2) c1 on a.art = c1.analog -- стоковые артикула
                        left join (select id_shop
                                   from tdv_map_without_stock s
                                   where s.only_purple is null) d1 on a.id_shop = d1.id_shop -- магазины, кому не ввозится сток
                        left join T_MAP_SHOP_RELEASE_YEAR_LIMIT e1 on a.id_shop = e1.id_shop -- магазины с ограничениями по годам выпуска артикулов         
                        left join (select id_shop, text1 analog
                                   from t_map_new_poexalo where id = i_id 
                                   group by id_shop, text1 having sum(kol) > 0
                                   ) z  on a.id_shop = z.id_shop and a.art = z.analog   -- магазины с распределенными ранее аналогами
                        where kol_sale + kol_ost != 0
                                and z.analog is null -- не было распределено до этого
                                and case when substr(a.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%' -- для РФ фильтруется все артикула с Р
                               and not (c1.analog is not null and d1.id_shop is not null) -- ограничение на ввоз стока
                               and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032') -- если сток то только в 0032 магазин
                               and (d.release_year >= nvl(e1.min_year, d.release_year) and d.release_year <= nvl(e1.max_year, d.release_year)) -- ограничение по году выпуска артикула
                        group by a.id_shop, a.art, a.season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, d.release_year, d.kol,
                                d.groupmw, d.line, d.order_number, c.group_name--, e1.min_year, e1.max_year
                        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= i_sale_pair_count and sum(kol_sale) <= i_sale_max_pair_count and sum(kol_ost) <= i_ost_pair_count) x
                  -- Артикула распределяем начиная с наибольшего количества в остатках склада
                  order by  /*release_year,*/ order_number, kol_otbor desc, kol_sale desc, reit2)
    loop
      -- присвоение лимита к переменной
      select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_row.id_shop;
      
      if v_shop_current_limit > 0 then
      
        --  пытаюсь докинуть короб
        for r_boxty in (select art, a.boxty
                          from t_all_rasp_box_work a
                          left join T_SAP_BOX_ROST f on a.boxty = f.boxty
                          left join (select distinct asize
                                    from TDV_MAP_NEW_OST_SALE
                                    where id_shop = r_row.id_shop
                                          and art = r_row.art
                                          and kol_ost != 0) b on f.asize = b.asize
                          left join (select x.asize -- проверка на остатки в пути
                                    from TDV_NEW_SHOP_OST x
                                    where x.verme > 0
                                          and x.id_shop = r_row.id_shop
                                          and x.art = r_row.art) c on f.asize = c.asize
                          left join (select distinct asize
                                    from t_map_new_poexalo x
                                    where id_shop = r_row.id_shop
                                          and x.text1 = r_row.art
                                          and id = i_id) d on f.asize = d.asize
                          where analog = r_row.art
                                and a.kol > 0
                                and case when substr(r_row.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%'
                          group by art, a.boxty
                          having sum(case when b.asize is null and c.asize is null and d.asize is null and f.kol = 1 then 0  else 1 end) = 0
                          order by sum(a.kol) desc)
        loop
          -- запоминаем сколько пар в коробе, который добавляем магазину
          select sum(a.kol) into v_kol_in_box from t_sap_box_rost a where a.boxty = r_boxty.boxty;
          
          if v_kol_in_box is null then v_kol_in_box := 0; end if;
          --проверяем лимит
          if v_shop_current_limit - v_kol_in_box >= 0
          then
            
            insert into t_map_new_poexalo
            select r_row.id_shop, r_boxty.art, r_row.season, a.kol, a.asize, 0 flag, '9999', i_id,
                   systimestamp, r_boxty.boxty, i_block_no block_no, null owner, r_row.art text1, null text2, 'BOX' text3
            from T_SAP_BOX_ROST a
            where a.boxty = r_boxty.boxty;
  
            update t_all_rasp_box_work a
            set kol = kol - 1
            where art = r_boxty.art
                  and boxty = r_boxty.boxty;
  
            -- вычитаем от лимита короб
            v_shop_current_limit := v_shop_current_limit - v_kol_in_box;
            
            exit; -- выход из цикла при добавлении первого поместившегося короба
          end if;
          
        end loop;
        
        -- обновляем текущий лимит магазина
        update t_map_current_shop_limit set current_limit = v_shop_current_limit 
        where id_shop = r_row.id_shop;
      end if; -- проверка лимита
    end loop;
    
    commit;
    
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = i_id;
    
    dbms_output.put_line('    Поехало ' || v_count);
    
    select sum(kol)
    into v_count
    from t_all_rasp_box_work;
    
    dbms_output.put_line('    Отбор коробов после отдачи ' || v_count);
    
    dbms_output.put_line('  box_count i_block_no : ' || i_block_no || ' end time : ' || systimestamp);
  end;
  
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2018 11:00:00
  -- Comments: блок расчета распределения коробов россыпи на магазины
  --
  --****************************************************************************************************************************************************
  procedure blue_count(i_id                  integer,
                       i_s_group             integer,
                       i_sales_start_date    date,
                       i_sales_end_date      date,
                       i_stock_doc_no        integer,
                       i_sale_pair_count     integer,
                       i_ost_pair_count      integer,
                       i_siberia_in          varchar2,
                       i_block_no            number default null,
                       i_sale_max_pair_count integer := 1000) is
  
    v_count integer;
    v_all_size_count integer;
    v_cent_size_count integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
    v_kol_in_box number := 0;
    
    v_is_add_corob varchar2(1) := 'F';
  begin
    dbms_output.put_line('blue_count i_block_no : ' || i_block_no || ' start time : ' || systimestamp);
    -- перезаполняю таблицу остатков и продаж отделений
    fillBaseData2(i_s_group, i_sales_start_date, i_sales_end_date, i_stock_doc_no);
    commit;

    -- БЛОК КОРОБОВ
    box_count(i_id, i_s_group, i_sales_start_date, i_sales_end_date, i_stock_doc_no, i_sale_pair_count, i_ost_pair_count, i_siberia_in, i_block_no, i_sale_max_pair_count);
    
    -- БЛОК РОССЫПИ
    -- беру магазины и артикула в которые нужно добросить обувь -- обувь, которая там хорошо продается -- по заданным условиям
    for r_row in (select x.*
                  from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, x.shop_in, sum(kol_sale) kol_sale,
                                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, x.count_limit_in,
                                D.release_year, D.kol kol_otbor, D.groupmw, d.line, d.order_number, C.group_name--, e1.min_year, e1.max_year
                       from TDV_MAP_NEW_OST_SALE a -- тут не должно быть отфильтрованной обуви и сибири
                       inner join (select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
                                         y.groupmw, y.line, z.order_number
                                  from T_MAP_NEW_OTBOR x -- или тут не должно быть отвильтрованной обуви  и сибири
                                  left join s_art y on x.art = y.art
                                  left join t_map_assort_order z on y.assort = z.assort
                                  -- nvl2(i_group_name, i_group_name, z.group_name)
                                  where y.groupmw in ('Мужские', 'Женские')
                                  group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw, y.line, z.order_number) d on a.art =
                                                                                                                    d.analog
                       inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                                and a.id_shop = x.shopid
                                                                and x.season_in like '%' || a.season || '%'
                        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                        left join (select distinct analog, group_name
                                   from t_map_group_ryb) c on a.art = c.analog
                        left join (select distinct analog
                                   from tdv_map_stock2) c1 on a.art = c1.analog -- стоковые артикула
                        left join (select id_shop
                                   from tdv_map_without_stock s
                                   where s.only_purple is null) d1 on a.id_shop = d1.id_shop -- магазины, кому не ввозится сток
                        left join T_MAP_SHOP_RELEASE_YEAR_LIMIT e1 on a.id_shop = e1.id_shop -- магазины с ограничениями по годам выпуска артикулов         
                        left join (select id_shop, text1 analog
                                   from t_map_new_poexalo where id = i_id 
                                   group by id_shop, text1 having sum(kol) > 0
                                   ) z  on a.id_shop = z.id_shop and a.art = z.analog   -- магазины с распределенными ранее аналогами
                        where kol_sale + kol_ost != 0
                                and z.analog is null -- не было распределено до этого
                                and case when substr(a.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%' -- для РФ фильтруется все артикула с Р
                               and not (c1.analog is not null and d1.id_shop is not null) -- ограничение на ввоз стока
                               and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032') -- если сток то только в 0032 магазин
                               and (d.release_year >= nvl(e1.min_year, d.release_year) and d.release_year <= nvl(e1.max_year, d.release_year)) -- ограничение по году выпуска артикула
                        group by a.id_shop, a.art, a.season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, d.release_year, d.kol,
                                d.groupmw, d.line, d.order_number, c.group_name--, e1.min_year, e1.max_year
                        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= i_sale_pair_count and sum(kol_sale) <= i_sale_max_pair_count and sum(kol_ost) <= i_ost_pair_count) x
                  -- Артикула распределяем начиная с наибольшего количества в остатках склада
                  order by  /*release_year,*/ order_number, kol_otbor desc, kol_sale desc, reit2)
    loop
    
    
      -- присвоение лимита к переменной
      select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_row.id_shop;
      
      if v_shop_current_limit > 0 then
      
        for r_size in (select distinct a.asize
                        from T_MAP_NEW_OTBOR a
                        left join (select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol
                                 from TDV_MAP_NEW_OST_SALE
                                 where id_shop = r_row.id_shop
                                       and art = r_row.art
                                 group by asize) b on a.asize = b.asize
                        left join (select asize, sum(kol) kol
                                 from t_map_new_poexalo x
                                 where id_shop = r_row.id_shop
                                       and x.text1 = r_row.art
                                       and id = i_id
                                 group by asize) c on a.asize = c.asize
                        left join (select x.asize, sum(x.verme) kol
                                 from TDV_NEW_SHOP_OST x
                                 where x.verme > 0
                                       and x.art = r_row.art
                                       and id_shop = r_row.id_shop
                                 group by x.asize) d on a.asize = d.asize
                        
                        where a.analog = r_row.art
                             and a.kol > 0
                             and nvl(b.kol_sale, 0) >= 0
                             and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) = 0
                        order by a.asize)
        loop
          -- беру данные размеры из остатка
          insert into t_map_new_poexalo
            select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, i_id, systimestamp, null boxty,
                   case when i_block_no is null then i_sale_pair_count else i_block_no end block_no, null owner, r_row.art text1, null text2, 'ROS' text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   where analog = r_row.art
                         and asize = r_size.asize
                         and kol > 0
                         and case when substr(r_row.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%'
                         order by a.kol)
            where rownum = 1;
        end loop;
        
        -- проверки на лимиты, выполнение правила на 3 размера, 2(1) центральных
        -- обноление отбора, распределенного и лимитов
        ross_checks_and_updates(i_id => i_id,
                                i_id_shop => r_row.id_shop,
                                i_analog => r_row.art,
                                i_groupmw => r_row.groupmw,
                                i_line => r_row.line,
                                i_shop_current_limit => v_shop_current_limit);
      end if; -- лимиты
    end loop;
    
    commit;

    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = i_id;
    
    dbms_output.put_line('  Поехало ' || v_count);
    
    select sum(kol)
    into v_count
    from T_MAP_NEW_OTBOR;
    
    dbms_output.put_line('  Отбор россыпи после отдачи ' || v_count);
    
    dbms_output.put_line('blue_count i_block_no : ' || i_block_no || ' end time : ' || systimestamp);
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 20.09.2019 12:20:00
  -- Comments: распределение обуви на магазины где обуви не было вообще
  --
  --****************************************************************************************************************************************************
  procedure purple_count(i_id         in integer,
                         i_s_group    in integer,
                         i_siberia_in varchar2,
                         i_block_no   number default null) is
    v_count integer;
--    v_cent_size integer;
    
    v_shop_current_limit number := 0;
--    v_kol_add number := 0;
    v_kol_in_box number := 0;
    
    v_is_add_corob varchar2(1) := 'F';
  begin

    dbms_output.put_line('purple_count i_block_no : ' || i_block_no || ' start time : ' || systimestamp);

    execute immediate 'truncate table bep_test100';
    
    for r_shop in (select shopid
                   from tdm_map_new_shop_group x
                   where x.s_group = i_s_group)
    loop
      insert into bep_test100 (id_shop, art, boxty)
        select r_shop.shopid, x.analog, x.boxty
        from (select analog analog, sum(a.kol) kol, a.boxty
              from t_all_rasp_box_work a
              left join T_SAP_BOX_ROST f on a.boxty = f.boxty
              where a.kol > 0
              group by analog,a.boxty having sum(a.kol) > 0
              
              union all
        
              select nvl(b.analog, a.art) analog, count(distinct asize) kol/*asize_coun*/, null boxty -- если на остатке в отборе есть артикул с 3 и более размерами
              from T_MAP_NEW_OTBOR a
              left join TDV_MAP_NEW_ANAL b on a.art = b.art
              where a.kol > 0
              /*                     and a.art not in (select art
              from tdv_map_stock2) -- исключаю стоки*/
              group by nvl(b.analog, a.art)
              having count(distinct asize) >= 3) x 
        left join (select distinct nvl(b.analog, a.art) art -- которых никогда не было на остатках в магазине
                   from e_osttek a
                   left join TDV_MAP_NEW_ANAL b on a.art = b.art
                   where id_shop = r_shop.shopid) y on x.analog = y.art
        left join (select distinct nvl(b.analog, a.art) analog -- которые еще не распределили
                   from t_map_new_poexalo a
                   left join TDV_MAP_NEW_ANAL b on a.art = b.art
                   where a.id = i_id
                         and a.id_shop = r_shop.shopid) z on x.analog = z.analog
        left join (select distinct x.id_shop, x.art
                   from TDV_NEW_SHOP_OST x
                   where x.verme > 0
                         and x.id_shop = r_shop.shopid) r on r.art = x.analog
        where y.art is null
              and z.analog is null
              and r.art is null
              ;
    end loop;
    
    delete from bep_test100
    where id_shop = '0032';
    
    if i_siberia_in = 'F' then
      delete from bep_test100
      where substr(id_shop, 1, 2) in ('36', '38', '39', '40')
            and id_shop not in ('3918', '3814');
    end if;
      
    commit;
    
    -- доброс в магазины, в которых данной обуви не было вообще
    -- беру магазины и артикула(!!!не аналоги!!!) и модели  которые нужно добросить
    for r_row in (select
                  --a.id_shop, a1.mod, a1.art, q.art_cnt, a.art analog /*analog*/, max(a.boxty) boxty, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, z.order_number
                  a.id_shop, m1.mod, a1.art,  a.art analog /*analog*/, a.boxty, a1.season, a1.groupmw, a1.line, nvl(q.art_cnt,0), z.order_number    --, max(a.boxty) boxty

                  from bep_test100 a
                  left join TDV_MAP_NEW_ANAL a2 on a.art = a2.analog
                  left join s_art a1 on a1.art = nvl(a2.art, a.art)
                  left join T_MAP_ART_ANALOG_MOD m1 on m1.art = nvl(a2.art, a.art)
                  left join t_map_assort_order z on a1.assort = z.assort
                  inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                         and a.id_shop = x.shopid
                                                         and x.season_in like '%' || a1.season || '%'
                  left join tdv_map_shop_reit b on a.id_shop = b.id_shop
--                  left join (select distinct analog, group_name
--                            from t_map_group_ryb) c on a.art = c.analog
                  left join (
                    select z1.mod, a.art, nvl(b.analog, a.art) analog,  sum(a.kol) art_cnt, a.boxty
                    from t_all_rasp_box_work a
                    left join T_SAP_BOX_ROST f on a.boxty = f.boxty
                    left join TDV_MAP_NEW_ANAL b on a.art = b.art
                    left join T_MAP_ART_ANALOG_MOD z1 on z1.art = a.art
                    where a.kol > 0
                    group by z1.mod, a.art, nvl(b.analog, a.art), a.boxty having sum(a.kol) > 0
                    union all 
                    
                    select z1.mod, x1.art, nvl(b.analog, x1.art) analog,  sum(kol) art_cnt, null
                    from T_MAP_NEW_OTBOR x1
                    left join TDV_MAP_NEW_ANAL b on x1.art = b.art
                    left join T_MAP_ART_ANALOG_MOD z1 on z1.art = x1.art
                    group by z1.mod, x1.art, nvl(b.analog, x1.art) having sum(kol) > 0
                  ) q on m1.mod = q.mod and /*a.art*/ a1.art /*analog*/ = q.art and nvl(a.boxty, ' ') = nvl(q.boxty, ' ')       
                  left join (select distinct analog
                                   from tdv_map_stock2) c1 on a.art = c1.analog
                  left join (select id_shop
                                   from tdv_map_without_stock s) d1 on a.id_shop = d1.id_shop
                  left join T_MAP_SHOP_RELEASE_YEAR_LIMIT e1 on a.id_shop = e1.id_shop -- магазины с ограничениями по годам выпуска артикулов
                  where a1.groupmw in ('Мужские', 'Женские')
                  and case when substr(a.id_shop,1,2) = '00' then ' ' else a1.art end not like '%Р%'
                  and not (c1.analog is not null and d1.id_shop is not null)
                  and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032')
                  and (replace(a1.release_year, ' ', '2018') >= nvl(e1.min_year, replace(a1.release_year, ' ', '2018')) 
                    and replace(a1.release_year, ' ', '2018') <= nvl(e1.max_year, replace(a1.release_year, ' ', '2018')))
                  order by  nvl(b.reit2, 0), z.order_number, /*m1.mod,*/ 
                            case when a.boxty is not null then 1 else 2 end, 
                            nvl(q.art_cnt,0) desc, a.art, a1.art)
    loop
      -- присвоение лимита к переменной
      select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_row.id_shop;
      --проверка если лимит больше 0, то выполняем
      if v_shop_current_limit > 0 then
      
        v_is_add_corob := 'F';
        
        -- если в распределяемом был короб
        if r_row.boxty is not null then
          -- БЛОК ДОБАВЛЕНИЯ КОРОБА
          --  пытаюсь докинуть короб
          for r_boxty in (select distinct art, a.boxty
                          from t_all_rasp_box_work a
                          left join T_SAP_BOX_ROST f on a.boxty = f.boxty
                          where art = r_row.art --analog = r_row.analog
                                and a.boxty = r_row.boxty
                                and a.kol > 0
                                and (r_row.mod) not in (select distinct c.mod -- которые не едут в магазин
                                                        from t_map_new_poexalo a
                                                        left join TDV_MAP_NEW_ANAL b on a.art = b.art
                                                        left join s_art c on a.art = c.art
                                                        where a.id = i_id
                                                              and id_shop = r_row.id_shop
                                                              and (/*a.text1* = r_row.analog/ /*a.art = r_row.art or*/ c.mod = r_row.mod))
                                and case when substr(r_row.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%')
          loop
            -- запоминаем сколько пар в коробе, который добавляем магазину
            select sum(a.kol) into v_kol_in_box from t_sap_box_rost a where a.boxty = r_boxty.boxty;
          
            --проверяем лимит
            if v_kol_in_box is null then v_kol_in_box := 0; end if;
            if v_shop_current_limit - v_kol_in_box >= 0
            then
              
              insert into t_map_new_poexalo
              select r_row.id_shop, r_boxty.art, r_row.season, a.kol, a.asize, 0 flag, '9999', i_id,
                     systimestamp, r_boxty.boxty, i_block_no block_no, null owner, r_row.analog text1, r_row.mod text2, 'BOX' text3
              from T_SAP_BOX_ROST a
              where a.boxty = r_boxty.boxty;
    
              update t_all_rasp_box_work a
              set kol = kol - 1
              where art = r_boxty.art
                    and boxty = r_boxty.boxty;
    
              -- вычитаем от лимита короб
              v_shop_current_limit := v_shop_current_limit - v_kol_in_box;
            
              v_is_add_corob := 'T';
            end if;
            exit;
            
          end loop;
        
          -- обновляем текущий лимит магазина
          update t_map_current_shop_limit set current_limit = v_shop_current_limit 
          where id_shop = r_row.id_shop;  
        
        else
          -- БЛОК ДОБАВЛЕНИЯ РОССЫПИ      
          --if /*v_is_add_corob = 'F' and*/ r_row.boxty is not null then
          
          -- беру размеры которые могу докинуть
          for r_size in (select distinct a.asize
                         from T_MAP_NEW_OTBOR a
                         where /*a.analog = r_row.analog*/ a.art = r_row.art
                               and a.kol > 0
                               and (r_row.mod) not in (select c.mod -- которые не едут в магазин
                                                     from t_map_new_poexalo a
                                                     left join TDV_MAP_NEW_ANAL b on a.art = b.art
                                                     left join s_art c on a.art = c.art
                                                     where a.id = i_id
                                                           and id_shop = r_row.id_shop
                                                           and (/*a.text1* = r_row.analog/ /*a.art = r_row.art or*/ c.mod = r_row.mod))
                               --and nvl(d.limit, 1000) > nvl(e.limit, 0)
                               and case when substr(r_row.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%'
                         order by a.asize)
          loop
            ---- беру данные размеры из остатка
            insert into t_map_new_poexalo
              select r_row.id_shop, art, r_row.season, 1, asize, 1, '9999', i_id, systimestamp, null boxty, 
              i_block_no,
              null owner, r_row.analog text1, r_row.mod text2, 'ROS' text3
              from (select a.*
                     from T_MAP_NEW_OTBOR a
                     left join s_art b on a.art = b.art
                     where a.analog = r_row.analog 
                           --a.art = r_row.art
                           and b.mod = r_row.mod
                           and asize = r_size.asize
                           and kol > 0
                           --and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%')
                           and case when substr(r_row.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%')
              where rownum = 1;
          end loop;
          
          
          -- проверки на лимиты, выполнение правила на 3 размера, 2(1) центральных
          -- обноление отбора, распределенного и лимитов
          ross_checks_and_updates(i_id => i_id,
                                  i_id_shop => r_row.id_shop,
                                  i_analog => r_row.analog,
                                  i_groupmw => r_row.groupmw,
                                  i_line => r_row.line,
                                  i_shop_current_limit => v_shop_current_limit);
                                  
                                  
        end if; -- is boxty or ross
        
      end if; -- limit
      
    end loop;
    
    commit;
        
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = i_id;
    
    dbms_output.put_line('  Поехало ' || v_count);
    
    select sum(kol)
    into v_count
    from t_all_rasp_box_work;
    
    dbms_output.put_line('  Отбор коробов после отдачи ' || v_count);
    
    select sum(kol)
    into v_count
    from T_MAP_NEW_OTBOR;
    
    dbms_output.put_line('  Отбор россыпи после отдачи ' || v_count);
    
    dbms_output.put_line('purple_count i_block_no : ' || i_block_no || ' end time : ' || systimestamp);
  end;
  

  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 24.09.2018 09:00:00
  -- Comments: выполнение подсортировки до плоской ростовки остатками россыпи
  --
  --****************************************************************************************************************************************************
  procedure last_count(i_id          in integer,
                       i_s_group     in varchar2,
                       i_siberia_in  in varchar2,
                       i_block_no    in number default null) is
    v_count integer;
    v_cent_size integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
  begin
  
    dbms_output.put_line('last_count i_block_no : ' || i_block_no || ' start time : ' || systimestamp);
  
    --проход по аналогам остатков и сортировка по ассортименту
    for r_row in (select distinct analog, a.season, b.groupmw, b.line, z.order_number
                  from T_MAP_NEW_OTBOR a
                  left join s_art b on a.art = b.art
                  left join t_map_assort_order z on b.assort = z.assort
                  where kol > 0
                  order by z.order_number)
    loop
      -- проход по магазинам которые могут принять аналог
      for r_shop in (select distinct a.id_shop, reit2
                      from e_osttek_online_short a
                      inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                                and a.id_shop = x.shopid
                                                                and x.season_in like '%' || a.season || '%'
                      left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                      inner join s_art c on a.art = c.art
                      inner join t_map_multi_raspred_shop_limit d on a.id_shop = d.id_shop
                      left join (select distinct analog
                                 from tdv_map_stock2) c1 on a.art = c1.analog -- стоковые артикула
                      left join (select id_shop
                                   from tdv_map_without_stock s
                                   where s.only_purple is not null) d1 on a.id_shop = d1.id_shop -- магазины, кому не ввозится сток
                      left join t_map_shop_release_year_limit e1 on a.id_shop = e1.id_shop -- магазины с ограничениями по годам выпуска артикулов
                      where a.analog = r_row.analog
                        and not (c1.analog is not null and d1.id_shop is not null) -- ограничение на ввоз стока
                        and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032') -- если сток то только в 0032 магазин
                        and (c.release_year >= nvl(e1.min_year, c.release_year) and c.release_year <= nvl(e1.max_year, c.release_year)) -- ограничение по году выпуска артикула
                        and case when i_siberia_in = 'F' then substr(a.id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
                        and case when substr(a.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%'
                        and d.current_limit > 0
                      order by reit2)
      loop
        -- присвоение лимита к переменной
        select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_shop.id_shop;
        
        --проверка если лимит больше 0, то выполняем
        if v_shop_current_limit > 0 then
          -- по данному магазину и аналогу выбираем размеры , которые можем взять
          for r_size in (select distinct a.asize
                          from t_map_new_otbor a
                          left join (select asize, sum(kol) kol
                                   from e_osttek_online_short
                                   where id_shop = r_shop.id_shop
                                         and art = r_row.analog
                                   group by asize) b on a.asize = b.asize
                          left join (select asize, sum(kol) kol
                                   from t_map_new_poexalo x
                                   where id_shop = r_shop.id_shop
                                         and x.text1 = r_row.analog
                                         and id = i_id
                                   group by asize) c on a.asize = c.asize
                          left join (select x.asize, sum(x.verme) kol
                                   from TDV_NEW_SHOP_OST x
                                   where x.verme > 0
                                         and x.art = r_row.analog
                                         and id_shop = r_shop.id_shop
                                   group by x.asize) d on a.asize = d.asize
                          where a.analog = r_row.analog
                               and a.kol > 0
                               and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) = 0
                          order by a.asize)
          loop
--            dbms_output.put_line('analog : '||r_row.analog||' size : '||r_size.asize||' shop : '||r_shop.id_shop);
            
            insert into t_map_new_poexalo
              select r_shop.id_shop, art, r_row.season, 1 kol, asize, 1 flag, '9999', i_id, systimestamp, null boxty,
                     i_block_no block_no, null owner, r_row.analog text1, null text2, 'ROS' text3
              from (select a.*
                     from T_MAP_NEW_OTBOR a
                     where analog = r_row.analog
                           and asize = r_size.asize
                           and kol > 0
                           and case when substr(r_shop.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%')
              where rownum = 1;
          
          end loop;-- sizes
          
          -- проверки на лимиты, выполнение правила на 3 размера, 2(1) центральных
          -- обноление отбора, распределенного и лимитов
          ross_checks_and_updates(i_id => i_id,
                                  i_id_shop => r_shop.id_shop,
                                  i_analog => r_row.analog,
                                  i_groupmw => r_row.groupmw,
                                  i_line => r_row.line,
                                  i_shop_current_limit => v_shop_current_limit);
          
        end if;
      end loop; -- shops for analog
    end loop; -- analogs in otbor
    
    commit;
    
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = i_id;
    
    dbms_output.put_line('  Поехало ' || v_count);

    select sum(kol)
    into v_count
    from T_MAP_NEW_OTBOR;
    
    dbms_output.put_line('  Отбор россыпи после отдачи ' || v_count);
    
    dbms_output.put_line('last_count i_block_no : ' || i_block_no || ' end time : ' || systimestamp);
  end last_count;
  
  
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.09.2019 10:00:00
  -- Comments: тип расчета 1 распределение лета 2019
  --
  --****************************************************************************************************************************************************
  procedure go_count(i_id      in integer,
                      i_s_group in integer) is
  begin  

    --формирование фондов
    --fond_count(i_id => i_id, 
    --           i_s_group => i_s_group,
    --           i_block_no => '0');

    -- 1) продажи >= 5, по 1 сен 2018   
    blue_count(
              i_id => i_id, 
              i_s_group => i_s_group, 
              i_sales_start_date => to_date('20190201', 'yyyymmdd'), 
              i_sales_end_date => to_date('20190901', 'yyyymmdd'), 
              i_stock_doc_no => null, 
              i_sale_pair_count => 5, 
              i_ost_pair_count => 100, 
              i_siberia_in => 'T', 
              i_block_no => '1');
              
    -- 2) продажи = 4 на 1 авг 2018
    blue_count(
              i_id => i_id, 
              i_s_group => i_s_group, 
              i_sales_start_date => to_date('20190201', 'yyyymmdd'), 
              i_sales_end_date => to_date('20190801', 'yyyymmdd'), 
              i_stock_doc_no => null, 
              i_sale_pair_count => 4, 
              i_ost_pair_count => 100, 
              i_siberia_in => 'T', 
              i_block_no => '2',
              i_sale_max_pair_count => 4);
              
    --3) продажи = 3 на 1 авг 2018
    blue_count(
              i_id => i_id, 
              i_s_group => i_s_group, 
              i_sales_start_date => to_date('20190201', 'yyyymmdd'), 
              i_sales_end_date => to_date('20190801', 'yyyymmdd'), 
              i_stock_doc_no => null, 
              i_sale_pair_count => 3, 
              i_ost_pair_count => 100, 
              i_siberia_in => 'T', 
              i_block_no => '3',
              i_sale_max_pair_count => 3);
              
    -- 4) продажи >= 4 на сегодня
    blue_count(
              i_id => i_id, 
              i_s_group => i_s_group, 
              i_sales_start_date => to_date('20190201', 'yyyymmdd'), 
              i_sales_end_date => sysdate, 
              i_stock_doc_no => null, 
              i_sale_pair_count => 4, 
              i_ost_pair_count => 100, 
              i_siberia_in => 'T', 
              i_block_no => '4');
    -- 5) продажи >= 2 на сегодня
    blue_count(
              i_id => i_id, 
              i_s_group => i_s_group, 
              i_sales_start_date => to_date('20190201', 'yyyymmdd'), 
              i_sales_end_date => sysdate, 
              i_stock_doc_no => null, 
              i_sale_pair_count => 2, 
              i_ost_pair_count => 100, 
              i_siberia_in => 'T', 
              i_block_no => '5');
              
    -- 6) подсортировать до плоской ростовки остатки в магазинах
    last_count(i_id, i_s_group, 'F', '6');
    
    -- 7) фиолетовые (артикул, которого не было в данном магазине) не более 600 пар на магазины открытые после 1 июня 2019
    purple_count(i_id, i_s_group, 'F', '7');
    
  end;
  
  procedure go(i_s_group             number default null,
               i_no_data_refresh     varchar2 default 'F', --если НЕ нужно  пересчитывать текущие остатки и продажи то 'T'
               i_type_count          integer default 1, -- вид расчета
               i_refresh_shop_limit  varchar2 default 'T' -- если нужно обнулить лимиты на ввоз в магазины 
               ) as
    v_count integer;
    v_s_group integer;
               
    v_id integer;
    v_time timestamp;
    
		v_list_city varchar2(1000 char);
		v_list_filial varchar2(1000 char);
		v_cluster_name varchar2(1000 char);
		v_package_name varchar2(1000 char);
     
  begin
    execute immediate 'truncate table t_map_multi_raspred_shop_limit'; -- таблица с текущими значениями лимита на распределение в магазин
    
    if i_s_group is not null then
			v_s_group := i_s_group;
		end if;
    
    -- номер нового расчета
    v_id := map_fl.getnewcalculationid();
    dbms_output.put_line('номер расчета: ' || v_id);
    
    v_time := systimestamp;
    
    -- список городов
		v_list_city := map_fl.get_city_list(i_s_group, 1000);
		dbms_output.put_line('v_list_city: ' || v_list_city);
	
		-- список филиалов
		v_list_filial := map_fl.get_filial_list(i_s_group, 1000);
		dbms_output.put_line('v_list_filial: ' || v_list_filial);
	
		-- имя кластера
		v_cluster_name := map_fl.get_cluster_name(i_s_group);
		dbms_output.put_line('v_cluster_name: ' || v_cluster_name);
	
		-- имя пакета
		v_package_name := map_fl.get_package_name(dbms_utility.format_call_stack);
		dbms_output.put_line('v_package_name: ' || v_package_name);  

    -- вставка новой записи в таблицу расчетов    
    insert into TDV_MAP_13_RESULT
		values
			(v_id, v_time, null, v_s_group, null, null, null, 'F', v_list_city, v_list_filial, 1, v_time, null, 0, 'in progress', v_cluster_name,
			 userenv('sessionid'), 1, v_package_name);
		commit;
    
    update tdm_map_new_shop_group a
		set a.shop_in = ',' || replace(rtrim(ltrim(replace(a.shop_in, ' ', ''), ','), ','), '70', '00') || ','
		where a.s_group = v_s_group
					and a.shop_in is not null;
    
    -- собираю всё что можно для распределения      
    if i_no_data_refresh = 'F' then
      --fillBaseData(v_s_group, v_rules_last_count_id, i_is_sgp, 1);
      
      sum_all_rasp(v_id, v_s_group, 'F');
      sum_all_rasp_box(v_id);
      null;
    end if;
    
    dbms_output.put_line(sysdate || ' ' || 'summ_all_rasp complete');
    commit;
    
    -- инициализация исходных лимитов
    if i_refresh_shop_limit = 'T' then
      insert into t_map_multi_raspred_shop_limit 
      select shopid, count_limit_in from tdm_map_new_shop_group where s_group = v_s_group;
      commit;
    end if;
    
    -- запуск выбранного типа расчета
    if i_type_count = 1 then
      --purple_count(v_id, i_s_group, 'F', '7');
      
      go_count(i_id => v_id, i_s_group => v_s_group);
    end if;
    
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = v_id;
  
    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;
    commit;
    
    insert into t_map_new_otbor_after (id, id_shop, art, season, kol, asize, analog, status_flag)
    select v_id, id_shop, art, season, kol, asize, analog, status_flag from t_map_new_otbor;
    commit;
    
    insert into t_all_rasp_box_after (id, art, boxty, kol, flag, season, analog)
    select v_id, art, boxty, kol, flag, season, analog from t_all_rasp_box_work;
    commit;                                                                                                                                                                                      
  end go;

end bep_map_004;