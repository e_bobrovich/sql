select * from st_sap_ob_ost order by art, lgtyp;
select * from st_sap_ob_ost2 order by art, lgtyp;
select * from tdv_map_new_sap_reserv2;


select sum(kol) from (
select art, asize, sum(kol) kol, boxty
from ( -- короба
       select d.art, null asize, sum(d.verme) / f.kol kol, d.boxty
       from st_sap_ob_ost2 d
       left join s_art e on d.art = e.art
       left join (select boxty, sum(kol) kol
                  from T_SAP_BOX_ROST
                  group by boxty) f on d.boxty = f.boxty
       where d.boxty not in (' ', 'N')
             and lgtyp in ('R08', 'Z08', 'S08')
             and ',Летняя,' like '%,' || e.season || ',%'
       group by d.art, d.boxty, f.kol
       
       union all
       
       -- россыпь 
       select d.art, d1.asize, sum(d.verme) kol, null
       from st_sap_ob_ost2 d
       left join s_art e on d.art = e.art
       left join s_all_mat d1 on d.matnr = d1.matnr
       where d.boxty in (' ')
             and d1.asize != 0
             and lgtyp in ('R08', 'Z08', 'S08')
             and ',Летняя,' like '%,' || e.season || ',%'
       group by d.art, d1.asize
       
       union all
       
       -- транзиты
       select a1.art, asize, sum(kol) kol, null
       from trans_in_way a1
       left join TDV_MAP_NEW_ANAL d on a1.art = d.art
       left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                         and a1.Postno_Lifex = e.id
       inner join s_art c on a1.art = c.art
       where scan != ' '
             and ',Летняя,' like '%,' || c.season || ',%'
             and a1.art not like '%/О%'
             and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
       group by a1.art, asize
       having sum(kol) > 0
       
       union all
       -- остатки магазинов
       select a2.art, a2.asize, sum(a2.kol) kol, null
       from e_osttek_online_short a2
       left join st_muya_art f on a2.art = f.art
       left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
                                        and a2.analog = j.art
       where 1 = case
               when a2.landid = 'RU' and f.art is not null then
                0
               else
                1
             end -- исключаем для РФ вывоз покупной
             and j.art is not null -- только то, что плохо продавалось
             --and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40') --из сибири не забираем
             and procent = 0
       group by a2.art, a2.asize
       having sum(kol) > 0
       
       union all
       -- РЦ
       select a2.art, a2.asize, sum(a2.kol) kol, null
       from tdv_map_rc_data a2
       inner join s_art c on a2.art = c.art
       left join TDV_MAP_NEW_ANAL d on d.art = a2.art
       where ',Летняя,' like '%,' || c.season || ',%'
             and a2.art not like '%/О%'
             and substr(skladid, 1, 1) != 'W'
             and substr(skladid, 3, 1) != 'S'
       
       group by a2.art, a2.asize
       having sum(a2.kol) > 0
       )

group by art, asize, boxty
)