call map_fl.fillWarehouseStock();

delete from t_map_new_otbor_after where id = 1111;
delete from t_map_new_otbor_before where id = 9736;

select * from t_map_new_otbor_after where id = 9736;
select * from t_map_new_otbor_before where id = 9736;

select sum(kol) from t_map_new_otbor_after where id = 9775;
select sum(kol) from t_map_new_otbor_before where id = 9775;

select sum(kol) from t_map_new_otbor_after where id = 1112;

select count(*) from t_map_new_otbor_after where id = 0 and id_shop in ('TRANS', 'SGP');

select id_shop, sum(kol) from t_map_new_otbor_after where id = 0 group by id_shop;

--delete from t_map_new_otbor_after where id = 0;

select 9736, id_shop, art, season, kol, asize, analog, status_flag from t_map_new_otbor;

--delete from t_map_new_otbor_before where id = 9736 and id_shop != '9999';

insert into t_map_new_otbor_after (id, id_shop, art, season, kol, asize, analog, status_flag)
--select count(*) from (
  select 1112, id_shop, art, season, sum(kol) kol, asize, analog, status_flag
  from (
         
         -- россыпь склада
         select 'SGP' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
         from tem_mapif_fill_warehouse_stock a
         inner join s_art b on a.art = b.art
         left join TDV_MAP_NEW_ANAL c on a.art = c.art
         where b.art not like '%/О%'
               and ',Зимняя,' like '%,' || b.season || ',%'
         group by a.art, b.season, a.asize, nvl(c.analog, b.art)
         
         union all
         -- транзиты
         select 'TRANS', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag
         from trans_in_way a1
         left join TDV_MAP_NEW_ANAL d on a1.art = d.art
         left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                           and a1.Postno_Lifex = e.id
         inner join s_art c on a1.art = c.art
         where scan != ' '
               and ',Зимняя,' like '%,' || c.season || ',%'
               and a1.art not like '%/О%'
               and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
         group by a1.art, c.season, asize, nvl(d.analog, a1.art)
         having sum(kol) > 0
         
         union all
         -- остатки РЦ и СВХ
         select skladid, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
         from 
         (
            select skladid skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
            from 
            (
              select shopid skladid,shopidto shopid,art,asize,scan
              from (select row_number() over(partition by b.scan order by b.dated desc) as nr,
                    b.shopid,b.shopidto,b.art,b.asize,b.scan
                    from kart_v b
                    inner join (select a.id_shop,a.art,a.asize,a.scan,sum(kol) kol
                                from e_osttek_online a
                                where substr(a.id_shop,1,1) = 'W' and a.scan != ' '
                                group by a.id_shop,a.art,a.asize,a.scan
                                having sum(kol) > 0) c on c.id_shop = b.shopid and c.art = b.art and c.asize = b.asize and c.scan = b.scan
                    where b.kvid in (0,2,1,5)
                    and substr(b.shopid,1,1) = 'W' and b.scan != ' ')
              where nr = 1
            )
            where shopid not in ('0', ' ')
                  and shopid in (select shopid
                                 from st_shop z
                                 where z.org_kod = 'SHP') --and 1 = 0
                  and case when 'T' = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
            group by skladid, shopid, art, asize
            
            union all
            --РЦ
            select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
            from dist_center.e_osttek
            where owner in (select shopid
                            from st_shop z
                            where z.org_kod = 'SHP')
            and case when 'T' = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
            group by DCID, owner, art, asize
            having sum(kol) > 0
         )
         a2
         inner join s_art c on a2.art = c.art
         left join TDV_MAP_NEW_ANAL d on d.art = a2.art
         where ',Зимняя,' like '%,' || c.season || ',%'
               and a2.art not like '%/О%'
                     and substr(skladid, 1, 1) != 'W' --склады
                     and substr(skladid, 3, 1) != 'S' -- санг
               and case when 'F' = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
               
         group by skladid, a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
         having sum(a2.kol) > 0
  )
  group by id_shop, art, season, asize, analog, status_flag
--)
;

select sum(kol) from t_map_new_otbor_before where id = 9736;
select sum(kol) from t_map_new_otbor;

select sum(kol) from (
       -- россыпь склада
       select 'SGP' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
       from tem_mapif_fill_warehouse_stock a
       inner join s_art b on a.art = b.art
       left join TDV_MAP_NEW_ANAL c on a.art = c.art
       where b.art not like '%/О%'
             and ',Зимняя,' like '%,' || b.season || ',%'
       group by a.art, b.season, a.asize, nvl(c.analog, b.art)
);


select skladid, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
from 
(
  select skladid skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
  from sklad_ost_owners
  where shopid not in ('0', ' ')
        and shopid in (select shopid
                       from st_shop z
                       where z.org_kod = 'SHP') --and 1 = 0
        and case when 'T' = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
  group by skladid, shopid, art, asize
  
  union all
  --РЦ
  select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
  from dist_center.e_osttek
  where owner in (select shopid
                  from st_shop z
                  where z.org_kod = 'SHP')
  and case when 'T' = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
  group by DCID, owner, art, asize
  having sum(kol) > 0
)
a2
inner join s_art c on a2.art = c.art
left join TDV_MAP_NEW_ANAL d on d.art = a2.art
where ',Зимняя,' like '%,' || c.season || ',%'
     and a2.art not like '%/О%'
           and substr(skladid, 1, 1) != 'W' --склады
           and substr(skladid, 3, 1) != 'S' -- санг
     and case when 'F' = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
     
group by skladid, a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
having sum(a2.kol) > 0