select * from tdv_shop_post_limit_result;

delete from tdv_shop_post_limit_result;


insert into tdv_shop_post_limit_result
select  a.id_shop, a.art, sum(kol) kol, b.postgr
from t_map_new_poexalo a
inner join T_MAP_ART_PRIOR_RASP b on a.art = b.art
where id = 16840 and id_shop not in ('fond', 'im', 'wb')
group by a.id_shop, a.art, b.postgr;

------------------------------------


select * from tdv_map_warehouse_result;

delete from tdv_map_warehouse_result;

insert into tdv_map_warehouse_result
select a.id_shop, a.art, a.asize, sum(a.kol) kol, 8 lgtype, boxty boxty
from t_map_new_poexalo a
where id = 16840
group by a.id_shop, a.art, a.asize, boxty;



  --CREATE OR REPLACE FORCE VIEW "FIRM"."MAP_SAP_RESULT_W_8_ROSS_ADD" ("ART", "VERME", "BOXTY", "SHOPNUM", "MATNR", "ASIZE", "POSTGR") AS 
  select a.art, sum(a.kol) verme, ' ' boxty , b.shopnum, c.matnr, case when mod(a.asize,1)!=0 then to_char(a.asize) else to_char( a.asize)||',0' end asize, d.postno postgr
from (select case when id_shop = 'fond' then '2108' else id_shop end id_shop_result, x.* from tdv_map_warehouse_result x)  a
inner join /*tdv_map_shop_copy*/ s_shop b on a.id_shop_result = b.shopid
left join s_all_mat c on a.art = c.art and a.asize = c.asize
left join tdv_shop_post_limit_result d on a.art = d.art and a.id_shop = d.id_shop
where lgtype = 8 
and a.id_shop is not null
group by a.art, a.boxty,  b.shopnum, c.matnr, a.asize, d.postno;

