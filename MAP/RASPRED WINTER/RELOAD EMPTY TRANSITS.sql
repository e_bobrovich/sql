select distinct a.id_shop from tdv_map_ex_transit a 
left join (
  select skladid, postno_lifex from trans_in_way 
  group by skladid, postno_lifex
) b on b.skladid = a.id_shop and b.postno_lifex = a.id
left join d_rasxod1 c on a.id_shop = c.id_shop and a.id = c.id 
where b.skladid is null
and c.id is not null
and (a.id_shop, a.id) not in (
  select '0004', 1 from dual
)
order by a.id_shop;

select distinct a.id, a.id_shop from tdv_map_ex_transit a 
left join (
  select skladid, postno_lifex from trans_in_way 
  group by skladid, postno_lifex
) b on b.skladid = a.id_shop and b.postno_lifex = a.id
left join d_rasxod1 c on a.id_shop = c.id_shop and a.id = c.id 
where b.skladid is null
and c.id is not null
and (a.id_shop, a.id) not in (
  select '0004', 1 from dual
)
order by a.id_shop, a.id;

select * from tdv_map_ex_transit a
left join (
  select skladid, postno_lifex from trans_in_way 
  group by skladid, postno_lifex
) b on b.skladid = a.id_shop and b.postno_lifex = a.id
;

  select * from ri_stored_proc_error where trunc(er_time) = trunc(sysdate) and sp_name in( 'CREATE_TRANS_DOC_FROM_SHOP_ID', 'CREATE_TRANS_DOC_FROM_SHOP') order by er_time desc;
  
select * from d_sap_odgruz1 where skladid = '4604' and postno_lifex = '57';
select * from d_sap_odgruz2;
select * from d_planot1 order by date_s desc;
select * from d_rasxod1 where id_shop = '4604' order by date_s desc;

call CREATE_TRANS_DOC_FROM_SHOP('4516');

select shopid id_shop from tdm_map_new_shop_group where s_group = 43301 order by shopid;


select * from tdv_map_ex_transit a
left join (
  select skladid, postno_lifex from trans_in_way 
  group by skladid, postno_lifex
) b on b.skladid = a.id_shop and b.postno_lifex = a.id;

  
SET serveroutput ON  
begin
  for cur in (select distinct a.id_shop from tdv_map_ex_transit a 
            left join (
              select skladid, postno_lifex from trans_in_way 
              group by skladid, postno_lifex
            ) b on b.skladid = a.id_shop and b.postno_lifex = a.id
            left join d_rasxod1 c on a.id_shop = c.id_shop and a.id = c.id 
            where b.skladid is null
            and c.id is not null
            and (a.id_shop, a.id) not in (
              select '0004', 1 from dual
            )
            order by a.id_shop
      )
  loop

    dbms_output.put_line(cur.id_shop);
  end loop;
end;