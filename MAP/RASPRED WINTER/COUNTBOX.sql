truncate table T_MAP_NEW_OTBOR;

truncate table tdv_map_warehouse_stock;
truncate table tdv_map_warehouse_result;

select sum(kol) from t_map_new_poexalo where id = 9854;

select * from tdv_map_warehouse_stock a where a.boxty is null;
select sum(kol) from tdv_map_warehouse_stock;

select sum(kol) from (
  select 
    a.art, b.asize, sum(a.kol) kol
    from () a
  left join T_SAP_BOX_ROST b on  a.boxty = b.boxty
  where a.boxty != ' '
  group by a.art, b.asize having sum(a.kol) > 0
);

select a.id_shop, a.art, null, 1 kol, null lgtype, boxty
from t_map_new_poexalo a
where id = 9834
      and a.boxty in (select boxty from T_SAP_BOX_ROST)
      and (id_shop, art, boxty) not in (select id_shop, art, boxty
                                        from tdv_map_warehouse_result
                                        where boxty is not null)
group by a.id_shop, a.art, boxty;

select id_shop, art, boxty from t_map_new_poexalo where id = 9834 and boxty != ' ' group by id_shop, art, boxty;



select id_shop, a.art, a.asize, sum(a.kol) kol
from t_map_new_poexalo a
left join tdv_map_warehouse_stock b on a.art = b.art
                                        and a.asize = b.asize
where id = 9834
      and (a.boxty not in (select boxty from T_SAP_BOX_ROST) or a.boxty is null)
      and b.art is null
group by id_shop, a.art, a.asize
order by id_shop, a.art, a.asize;

select * from s_art where art = '013010';
--------------------------------------------------------------------------------
insert into tdv_map_warehouse_stock
select art, asize, sum(kol) kol, boxty
from ( -- короба
       select d.art, null asize, sum(d.verme) / f.kol kol, d.boxty
       from st_sap_ob_ost2 d
       left join s_art e on d.art = e.art
       left join (select boxty, sum(kol) kol
                  from T_SAP_BOX_ROST
                  group by boxty) f on d.boxty = f.boxty
       where d.boxty not in (' ', 'N')
             and lgtyp in ('R08', 'Z08', 'S08')
             and ',Зимняя,' like '%,' || e.season || ',%'
       group by d.art, d.boxty, f.kol
       union all
       
       -- россыпь 
       select d.art, d1.asize, sum(d.verme) kol, null
       from st_sap_ob_ost2 d
       left join s_art e on d.art = e.art
       left join s_all_mat d1 on d.matnr = d1.matnr
       where d.boxty in (' ')
             and d1.asize != 0
             and lgtyp in ('R08', 'Z08', 'S08')
             and ',Зимняя,' like '%,' || e.season || ',%'
       group by d.art, d1.asize
       
       union all
       
       -- транзиты
       select a1.art, asize, sum(kol) kol, null
       from trans_in_way a1
       left join TDV_MAP_NEW_ANAL d on a1.art = d.art
       left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                         and a1.Postno_Lifex = e.id
       inner join s_art c on a1.art = c.art
       where scan != ' '
             and ',Зимняя,' like '%,' || c.season || ',%'
             and a1.art not like '%/О%'
             and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
             and not (
                ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%')) 
                and substr(id_shop, 1, 2) in ('36', '38', '39', '40')
             )
       group by a1.art, asize
       having sum(kol) > 0
       
       union all
       -- РЦ
       select a2.art, a2.asize, sum(a2.kol) kol, null
       from tdv_map_rc_data a2
       inner join s_art c on a2.art = c.art
       left join TDV_MAP_NEW_ANAL d on d.art = a2.art
       where ',Зимняя,' like '%,' || c.season || ',%'
             and a2.art not like '%/О%'
             and substr(skladid, 1, 1) != 'W'
             and substr(skladid, 3, 1) != 'S'
             and case when 'F' = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
       group by a2.art, a2.asize
       having sum(a2.kol) > 0)

group by art, asize, boxty;
  
-- от россыпи 8-ого склада отнимаю резервы 8-ого склада
update tdv_map_warehouse_stock x1
set x1.kol =
     (select x1.kol - x2.verme
      from (select art, to_number(asize, '99999.99') asize, sum(verme) verme
             from tdv_map_new_sap_reserv2 x3
             where x3.boxty = ' '
                   and trans != 'X'
                   and kunnr not in ('X_NERASP', 'X_SD_USE')
                   and ABLAD = '08'
             group by art, to_number(asize, '99999.99')) x2
      where x1.art = x2.art
            and x1.asize = x2.asize)
where exists (select x1.kol - x2.verme
       from (select art, to_number(asize, '99999.99') asize, sum(verme) verme
              from tdv_map_new_sap_reserv2 x3
              where x3.boxty = ' '
                    and trans != 'X'
                    and kunnr not in ('X_NERASP', 'X_SD_USE')
                    and ABLAD = '08'
              group by art, to_number(asize, '99999.99')) x2
       where x1.asize = x2.asize
             and x1.art = x2.art);
  
delete from tdv_map_warehouse_stock
where kol <= 0;


-- остатки коробов забираем с других складов
insert into tdv_map_warehouse_result
  select a.id_shop, a.art, null, 1 kol, null lgtype, boxty
  from t_map_new_poexalo a
  where id = 9834
        and a.boxty in (select boxty
                        from T_SAP_BOX_ROST)
        and (id_shop, art, boxty) not in (select id_shop, art, boxty
                                          from tdv_map_warehouse_result
                                          where boxty is not null)
  group by a.id_shop, a.art, boxty;

delete from tdv_map_warehouse_result
where kol = 0;