select assort_group, assort, sum(kol) from (
  select 
  case when lower(assort) like '%ботинки%' or lower(assort) like '%полусапоги%' then 1
       when lower(assort) like '%сапоги%' and lower(assort) not like '%полусапоги%' then 2 
       else 2 end assort_group, 
  b.assort,
  a.* from t_map_new_poexalo a
  inner join s_art b on a.art = b.art
  where a.id = 9854
) group by assort_group, assort
;

select id_shop, art, sum(kol) kol, assort_group from (
  select a.id_shop, a.art, a.kol, 
  case when lower(assort) like '%ботинки%' or lower(assort) like '%полусапоги%' then 1
         when lower(assort) like '%сапоги%' and lower(assort) not like '%полусапоги%' then 2 
         else 2 end assort_group
  from t_map_new_poexalo a
  inner join s_art b on a.art = b.art
  where a.id = 9854
)
group by id_shop, art, assort_group
order by id_shop, art;

select assort_group, sum(kol) from (
  select id_shop, art, sum(kol) kol, assort_group from (
    select a.id_shop, a.art, a.kol, 
    case when lower(assort) like '%ботинки%' or lower(assort) like '%полусапоги%' then 1
           when lower(assort) like '%сапоги%' and lower(assort) not like '%полусапоги%' then 2 
           else 2 end assort_group
    from t_map_new_poexalo a
    inner join s_art b on a.art = b.art
    where a.id = 9876
  )
  group by id_shop, art, assort_group
) group by assort_group;


select * from tdv_shop_post_limit_result;

select x.*
from (select b.mod, count(distinct a.art) art_cnt
       from t_map_new_poexalo a
       left join s_art b on a.art = b.art
       where id = 9854
             and id_shop = '0001'
       --and mod = '1616025'
       group by b.mod) x
inner join s_art y on x.mod = y.mod 
group by art
order by case when lower(assort) like '%ботинки%' then 1
              when lower(assort) like '%полусапоги%' then 1
              when lower(assort) like '%сапоги%' and lower(assort) not like '%полусапоги%' then 2 end,       
         art_cnt desc
;