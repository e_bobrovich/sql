truncate table tdv_map_warehouse_stock;

-- ОСТАТКИ ПО ВСЕМ СКЛАДАМ
-- остаток россыпи на складе ДО расчета
select sum(kol) from T_MAP_NEW_OTBOR_BEFORE where id= 28320;
-- остаток россыпи на складе ПОСЛЕ расчета
select sum(kol) from T_MAP_NEW_OTBOR_AFTER where id= 28320;
-- остаток коробов на складе ДО расчета
select sum(kol) from t_all_rasp_box_before a where a.id = 28320;
-- остаток коробов на складе ПОСЛЕ расчета
select sum(kol) from t_all_rasp_box_after a where a.id = 28320;
-- остаток коробов в парах на складе ДО расчета
select sum(a.kol) kol from t_all_rasp_box_before a
left join (select boxty, sum(kol) kol from T_SAP_BOX_ROST group by boxty) f on a.boxty = f.boxty
where a.id = 9854;
-- остаток коробов в парах на складе ПОСЛЕ расчета
select sum(a.kol) kol from t_all_rasp_box_after a
left join (select boxty, sum(kol) kol from T_SAP_BOX_ROST group by boxty) f on a.boxty = f.boxty
where a.id = 9854;

-- ОСТАТКИ ПО 48(СГП)
-- остаток россыпи на складе ДО расчета

-- остаток коробов на складе ДО расчета
select sum(kol) from (
  select d.art, null asize, /*sum(d.verme), f.kol koll,*/  sum(d.verme) / f.kol kol, d.boxty
  from st_sap_ob_ost2 d
  left join s_art e on d.art = e.art
  left join (select boxty, sum(kol) kol
            from T_SAP_BOX_ROST
            group by boxty) f on d.boxty = f.boxty
  where d.boxty not in (' ', 'N')
       and lgtyp not in ('R08', 'Z08', 'S08')
       and ',Летняя,' like '%,' || e.season || ',%'
       and e.art not like '%/О%'
       and e.groupmw in ('Мужские', 'Женские')
       and d.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual)
  group by d.art, d.boxty, f.kol
);
-- остаток коробов в парах на складе ДО расчета
select sum(d.verme)
from st_sap_ob_ost2 d
left join s_art e on d.art = e.art
left join (select boxty, sum(kol) kol
            from T_SAP_BOX_ROST
            group by boxty) f on d.boxty = f.boxty
where d.boxty not in (' ', 'N')
     and lgtyp not in ('R08', 'Z08', 'S08')
     and ',Летняя,' like '%,' || e.season || ',%'
     and e.art not like '%/О%'
     and e.groupmw in ('Мужские', 'Женские')
     and d.art not in (select art from t_map_new_art
                  union select '1715000/1' from dual
                  union select '1715001/1' from dual);
                  
                  
                  
-- ОСТАТКИ ПО 8(ОСТАЛЬНОЕ)
-- остаток коробов на складе ДО расчета
select sum(kol) from (
  select d.art, null asize,/*sum(d.verme), f.kol koll, */ sum(d.verme) / f.kol kol, d.boxty
  from st_sap_ob_ost2 d
  left join s_art e on d.art = e.art
  left join (select boxty, sum(kol) kol
            from T_SAP_BOX_ROST
            group by boxty) f on d.boxty = f.boxty
  where d.boxty not in (' ', 'N')
       --and lgtyp in ('R08', 'Z08', 'S08')
       and ',Летняя,' like '%,' || e.season || ',%'
       and e.art not like '%/О%'
       and e.groupmw in ('Мужские', 'Женские')
  group by d.art, d.boxty, f.kol
);
-- остаток коробов в парах на складе ДО расчета
select sum(d.verme)
from st_sap_ob_ost2 d
left join s_art e on d.art = e.art
left join (select boxty, sum(kol) kol
            from T_SAP_BOX_ROST
            group by boxty) f on d.boxty = f.boxty
where d.boxty not in (' ', 'N')
     and lgtyp in ('R08', 'Z08', 'S08')
     and ',Летняя,' like '%,' || e.season || ',%'
     and e.art not like '%/О%'
     and e.groupmw in ('Мужские', 'Женские')
--     and d.art not in (select art from t_map_new_art
--                  union select '1715000/1' from dual
--                  union select '1715001/1' from dual)
;

select sum(a.kol_pair), sum(b.verme),  sum(a.kol_pair)-sum(b.verme)
from (
      select d.art, d.boxty, sum(d.verme) / f.kol kol_boxty, sum(d.verme) kol_pair, 0 flag, e.season, nvl(g.analog, d.art) analog
      from st_sap_ob_ost2 d
      left join s_art e on d.art = e.art
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                 group by boxty) f on d.boxty = f.boxty
      left join TDV_MAP_NEW_ANAL g on d.art = g.art
      where d.boxty not in (' ', 'N')
      and ',Летняя,' like '%,' || e.season || ',%'
      and e.groupmw in ('Мужские', 'Женские')
      and e.art not like '%/О%'
      group by d.art, d.boxty, f.kol, e.season, nvl(g.analog, d.art)
) a left join (
    select art, boxty, sum(verme) verme
    from tdv_map_new_sap_reserv2 x3
    where x3.boxty != ' '
         and trans != 'X'
         and kunnr not in ('X_NERASP', 'X_SD_USE')
    group by art, boxty
) b on a.art = b.art and a.boxty = b.boxty
;
