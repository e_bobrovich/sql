-- транзиты
select type_t, sum(kol) from 
(select '9999' id_shop, a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag,
  case when e.id_shop is not null then 1 when a1.shopid not like '%D%' then 2 when a1.skladid != 'KI1000' then 3 end type_t
  from trans_in_way a1
  left join TDV_MAP_NEW_ANAL d on a1.art = d.art
  left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                   and a1.Postno_Lifex = e.id
  inner join s_art c on a1.art = c.art
  where scan != ' '
       and ',Летняя,' like '%,' || c.season || ',%'
       and a1.art not like '%/О%'
--       and e.id_shop is null
--       and a1.skladid = '4602'
       and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')   
       and not (
          ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%')) 
          and case when 'F' = 'F' then substr(skladid, 1, 2) else ' ' end in ('36', '38', '39', '40')
       )
  group by a1.art, c.season, asize, nvl(d.analog, a1.art),
  case when e.id_shop is not null then 1 when a1.shopid not like '%D%' then 2 when a1.skladid != 'KI1000' then 3 end
  having sum(kol) > 0
  
  union all
  
  select '9999' id_shop, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag,
         case when e.id_shop is not null then 0 else 999 end
  from d_rasxod1 a1
  inner join d_rasxod2 a2 on a1.id_shop = a2.id_shop and a1.id = a2.id 
  left join tdv_map_new_anal d on a2.art = d.art
  inner join tdv_map_ex_transit e on a1.id_shop = e.id_shop and a1.id = e.id
  left join (
    select skladid, postno_lifex, shopid from trans_in_way 
    group by skladid, postno_lifex, shopid
  ) b on b.skladid = e.id_shop and b.postno_lifex = e.id   
  inner join s_art c on a2.art = c.art
  where scan != ' '
     and a2.art not like '%/О%'
     and ',Летняя,' like '%,' || c.season || ',%'
     --and e.id_shop = '4602'
     and b.skladid is null
     and not (case when 'F' = 'F' then substr(e.id_shop, 1, 2) else ' ' end in ('36', '38', '39', '40'))
     group by a2.art, c.season, a2.asize, nvl(d.analog, a2.art),
     case when e.id_shop is not null then 0 else 999 end
)  group by type_t;


select  distinct a1.skladid, a1.shopid, a1.Postno_Lifex
from trans_in_way a1
left join TDV_MAP_NEW_ANAL d on a1.art = d.art
left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                 and a1.Postno_Lifex = e.id
inner join s_art c on a1.art = c.art
where scan != ' '
     and ',Летняя,' like '%,' || c.season || ',%'
--       and a1.art not like '%/О%'
--       and e.id_shop is null
     and a1.skladid = '4602'
     and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')   
     and not (
        ((e.id_shop is not null and a1.shopid not like '%D%') or (e.id_shop is null and a1.shopid not like '%D%')) 
        and case when 'F' = 'F' then substr(skladid, 1, 2) else ' ' end in ('36', '38', '39', '40')
     )
     
;

  select distinct a1.id_shop, a1.kkl, a1.id
  from d_rasxod1 a1
  inner join d_rasxod2 a2 on a1.id_shop = a2.id_shop and a1.id = a2.id 
  left join tdv_map_new_anal d on a2.art = d.art
  inner join tdv_map_ex_transit e on a1.id_shop = e.id_shop and a1.id = e.id
  left join (
    select skladid, postno_lifex, shopid from trans_in_way 
    group by skladid, postno_lifex, shopid
  ) b on b.skladid = e.id_shop and b.postno_lifex = e.id   
  inner join s_art c on a2.art = c.art
  where scan != ' '
     and ',Летняя,' like '%,' || c.season || ',%'
     and e.id_shop = '4602'
     and b.skladid is null
     and not (case when 'F' = 'F' then substr(e.id_shop, 1, 2) else ' ' end in ('36', '38', '39', '40'))
  ;
  
select * from d_rasxod1 a1
inner join d_rasxod2 a2 on a1.id_shop = a2.id_shop and a1.id = a2.id
inner join s_art c on a2.art = c.art
where a1.id_shop = '4602' and a1.id = '83'
--and ',Летняя,' like '%,' || c.season || ',%';