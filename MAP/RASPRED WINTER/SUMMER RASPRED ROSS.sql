select * from TDV_MAP_13_RESULT order by id desc;

select * from e_osttek_online_short;

select * from tdv_map_osttek;

select * from TDV_MAP_NEW_OST_SALE;

select * from tdm_map_new_shop_group where s_group = '18009';

select * from T_MAP_SHOP_RELEASE_YEAR_LIMIT;

select * from TDV_MAP_NEW_OST_SALE a
inner join tdm_map_new_shop_group x on x.s_group = 18009
                                              and a.id_shop = x.shopid
                                              and x.season_in like '%' || a.season || '%'
;

select x.*
from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, x.shop_in, sum(kol_sale) kol_sale,
              sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, x.count_limit_in,
              D.release_year, D.kol kol_otbor, D.groupmw, d.line, d.order_number, C.group_name, e1.min_year, e1.max_year
     from TDV_MAP_NEW_OST_SALE a -- тут не должно быть отфильтрованной обуви и сибири
     inner join (select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
                       y.groupmw, y.line, z.order_number
                from T_MAP_NEW_OTBOR x -- или тут не должно быть отвильтрованной обуви  и сибири
                left join s_art y on x.art = y.art
                left join t_map_assort_order z on y.assort = z.assort
                -- nvl2(i_group_name, i_group_name, z.group_name)
                where y.groupmw in ('Мужские', 'Женские')
                group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw, y.line, z.order_number) d on a.art =
                                                                                                  d.analog
     inner join tdm_map_new_shop_group x on x.s_group = 18009
                                              and a.id_shop = x.shopid
                                              and x.season_in like '%' || a.season || '%'
      left join tdv_map_shop_reit b on a.id_shop = b.id_shop
      left join (select distinct analog, group_name
                 from t_map_group_ryb) c on a.art = c.analog
      left join (select distinct analog
                 from tdv_map_stock2) c1 on a.art = c1.analog -- стоковые артикула
      left join (select id_shop
                 from tdv_map_without_stock s
                 where s.only_purple is null) d1 on a.id_shop = d1.id_shop -- магазины, кому не ввозится сток
      left join T_MAP_SHOP_RELEASE_YEAR_LIMIT e1 on a.id_shop = e1.id_shop -- магазины с ограничениями по годам выпуска артикулов         
      left join (select id_shop, text1 analog
                 from t_map_new_poexalo where id = 14574 
                 group by id_shop, text1 having sum(kol) > 0
                 ) z  on a.id_shop = z.id_shop and a.art = z.analog   -- магазины с распределенными ранее аналогами
      where kol_sale + kol_ost != 0
              and z.analog is null -- не было распределено до этого
              and case when substr(a.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%' -- для РФ фильтруется все артикула с Р
             and not (c1.analog is not null and d1.id_shop is not null) -- ограничение на ввоз стока
             and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032') -- если сток то только в 0032 магазин
             and (d.release_year >= nvl(e1.min_year, d.release_year) and d.release_year <= nvl(e1.max_year, d.release_year)) -- ограничение по году выпуска артикула
      group by a.id_shop, a.art, a.season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, d.release_year, d.kol,
              d.groupmw, d.line, d.order_number, c.group_name, e1.min_year, e1.max_year
      having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= 5 and sum(kol_sale) <= 1000 and sum(kol_ost) <= 100) x
-- Артикула распределяем начиная с наибольшего количества в остатках склада
order by  /*release_year,*/ order_number, kol_otbor desc, kol_sale desc, reit2;


select distinct a.asize
from T_MAP_NEW_OTBOR a
left join (select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2
           from tdv_map_new_ost_sale
           where id_shop = /*r_row.id_shop*/'2224'
                 and art = /*r_row.art*/'2950'
           group by asize) b on a.asize = b.asize
left join (select asize, sum(kol) kol
         from t_map_new_poexalo x
         --left join TDV_MAP_NEW_ANAL y on x.art = y.art
         where id_shop = /*r_row.id_shop*/'2224'
              --and nvl(y.analog, x.art) = /*r_row.art*/'2950'
               and x.text1 = /*r_row.art*/'2950'
               and id = /*i_id*/14574
         group by asize) c on a.asize = c.asize
where a.analog = /*r_row.art*/'2950'
       and a.kol > 0
       --and nvl(d.limit, 1000) > nvl(e.limit, 0)
       --and c.asize is null
       and nvl(b.kol_sale, 0) >= 0
       and nvl(b.kol_ost2, 0) + nvl(c.kol, 0) <= 0
       and (/*r_row.id_shop*/'2224', /*r_row.art*/'2950', a.asize) not in
       (select x.id_shop, x.art, x.asize
            from TDV_NEW_SHOP_OST x
            where x.verme > 0
                  and x.art = /*r_row.art*/'2950'
                  and id_shop = /*r_row.id_shop*/'2224') -- которых нет в транзитах
and case when substr(/*r_row.id_shop*/'2224',1,2) = '00' then ' ' else a.art end not like '%Р%'
order by a.asize;

select distinct a.asize
from T_MAP_NEW_OTBOR a
left join (select asize, sum(kol_sale) kol_sale, sum(kol_ost2) kol
         from TDV_MAP_NEW_OST_SALE
         where id_shop = /*r_row.id_shop*/'2224'
               and art = /*r_row.art*/'2950'
         group by asize) b on a.asize = b.asize
left join (select asize, sum(kol) kol
         from t_map_new_poexalo x
         where id_shop = /*r_row.id_shop*/'2224'
               and x.text1 = /*r_row.art*/'2950'
               and id = /*i_id*/14574
         group by asize) c on a.asize = c.asize
left join (select x.asize, sum(x.verme) kol
         from TDV_NEW_SHOP_OST x
         where x.verme > 0
               and x.art = /*r_row.art*/'2950'
               and id_shop = /*r_row.id_shop*/'2224'
         group by x.asize) d on a.asize = d.asize

where a.analog = /*r_row.art*/'2950'
     and a.kol > 0
     and nvl(b.kol_sale, 0) >= 0
     and coalesce(b.kol, 0) + coalesce(c.kol, 0) + coalesce(d.kol, 0) = 0
order by a.asize;

select /*r_row.id_shop*/'2224', art, /*r_row.season*/ 'Летняя', 1, asize, 1, id_shop, /*i_id*/14574, systimestamp, null boxty,
       1 block_no, null owner, /*r_row.art*/'2950' text1, null text2, 'ROS' text3
from (select a.*
       from T_MAP_NEW_OTBOR a
       where analog = /*r_row.art*/'2950'
             and asize = /*r_size.asize*/ 40
             and kol > 0
             and case when substr(/*r_row.id_shop*/'2224',1,2) = '00' then ' ' else a.art end not like '%Р%'
      order by a.kol)
where rownum = 1;

select art, normt from s_art where art in ('2950', '8938915', '2662/1');

select art, normt, line, style from s_art;