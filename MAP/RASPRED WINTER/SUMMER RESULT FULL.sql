select sum(kol) from t_map_new_poexalo where id = 28608;
select block_no, sum(kol) from t_map_new_poexalo where id = 28608 group by block_no;


select * from t_map_new_poexalo where id = 28608 and block_no = 5 and boxty is not null;


select 
--  distinct
  a.id_shop "ПОЛУЧАТЕЛЬ", 
  с.shopname "НАЗВАНИЕ",
  a.art "АРТИКУЛ", 
  a.text1 "АНАЛОГ",
  a.text3 "КОРОБ/РОССЫПЬ",
  a.asize "РАЗМЕР",
  a.boxty "ГОТОВЫЙ КОРОБ",
  case when a.block_no = 0 then a.text2 else null end "РОСТОВКА В ФОНД",
  a.kol "КОЛИЧЕСТВО",
  nvl(ora.kol_ost, 0) "ОСТАТОК РАЗМЕРА",
  nvl(oa.kol_ost, 0) "ОСТАТОК АНАЛОГА",
  nvl(tr.kol_trans_ost, 0) "РЕЗЕРВ АНАЛОГА",
  nvl(sr8.kol_sale, 0) "ПРОДАЖИ РАЗМЕР 08",
  nvl(sa8.kol_sale, 0) "ПРОДАЖИ АНАЛОГ 08",
  nvl(sr9.kol_sale, 0) "ПРОДАЖИ РАЗМЕР 09",
  nvl(sa9.kol_sale, 0) "ПРОДАЖИ АНАЛОГ 09",
  nvl(sr.kol_sale, 0) "ПРОДАЖИ РАЗМЕР",
  nvl(sa.kol_sale, 0) "ПРОДАЖИ АНАЛОГ",
  case when a.block_no = -3 then 'FOND'
       when a.block_no = -2 then 'IM'
       when a.block_no = -1 then 'PODSORT'
       when a.block_no = 10 then 'WB'
       else to_char(a.block_no)
  end "ЭТАП"
from t_map_new_poexalo a
left join s_shop b on a.id_shop_from = b.shopid
left join s_shop с on a.id_shop = с.shopid
left join (
  select y2.id_shop, nvl(z2.analog, y2.art) analog, sum(y2.kol) kol_sale from pos_sale1 x2 
  inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
  left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
  where y2.art in (select art from s_art where season = 'Летняя')
  and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
  and y2.scan != ' ' and y2.procent = 0
  and trunc(x2.sale_date) >= trunc(to_date('01.02.2020','dd.mm.yyyy'))
  and trunc(x2.sale_date) < trunc(to_date('01.09.2020','dd.mm.yyyy'))
  group by y2.id_shop, nvl(z2.analog, y2.art)
) sa9 on a.id_shop = sa9.id_shop and a.text1 = sa9.analog 

left join (
  select y2.id_shop, nvl(z2.analog, y2.art) analog, y2.asize, sum(y2.kol) kol_sale from pos_sale1 x2 
  inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
  left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
  where y2.art in (select art from s_art where season = 'Летняя')
  and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
  and y2.scan != ' ' and y2.procent = 0
  and trunc(x2.sale_date) >= trunc(to_date('01.02.2020','dd.mm.yyyy'))
  and trunc(x2.sale_date) < trunc(to_date('01.09.2020','dd.mm.yyyy'))
  group by y2.id_shop, nvl(z2.analog, y2.art), y2.asize
) sr9 on a.id_shop = sr9.id_shop and a.text1 = sr9.analog and a.asize = sr9.asize

left join (
  select y2.id_shop, nvl(z2.analog, y2.art) analog, sum(y2.kol) kol_sale from pos_sale1 x2 
  inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
  left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
  where y2.art in (select art from s_art where season = 'Летняя')
  and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
  and y2.scan != ' ' and y2.procent = 0
  and trunc(x2.sale_date) >= trunc(to_date('01.02.2020','dd.mm.yyyy'))
  and trunc(x2.sale_date) < trunc(to_date('01.08.2020','dd.mm.yyyy'))
  group by y2.id_shop, nvl(z2.analog, y2.art)
) sa8 on a.id_shop = sa8.id_shop and a.text1 = sa8.analog 

left join (
  select y2.id_shop, nvl(z2.analog, y2.art) analog, y2.asize, sum(y2.kol) kol_sale from pos_sale1 x2 
  inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
  left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
  where y2.art in (select art from s_art where season = 'Летняя')
  and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
  and y2.scan != ' ' and y2.procent = 0
  and trunc(x2.sale_date) >= trunc(to_date('01.02.2020','dd.mm.yyyy'))
  and trunc(x2.sale_date) < trunc(to_date('01.08.2020','dd.mm.yyyy'))
  group by y2.id_shop, nvl(z2.analog, y2.art), y2.asize
) sr8 on a.id_shop = sr8.id_shop and a.text1 = sr8.analog and a.asize = sr8.asize

left join (
  select y2.id_shop, nvl(z2.analog, y2.art) analog, sum(y2.kol) kol_sale from pos_sale1 x2 
  inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
  left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
  where y2.art in (select art from s_art where season = 'Летняя')
  and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
  and y2.scan != ' ' and y2.procent = 0
  and trunc(x2.sale_date) >= trunc(to_date('01.02.2020','dd.mm.yyyy'))
  and trunc(x2.sale_date) < trunc(sysdate)
  group by y2.id_shop, nvl(z2.analog, y2.art)
) sa on a.id_shop = sa.id_shop and a.text1 = sa.analog 

left join (
  select y2.id_shop, nvl(z2.analog, y2.art) analog, y2.asize, sum(y2.kol) kol_sale from pos_sale1 x2 
  inner join pos_sale2 y2 on x2.id_shop = y2.id_shop and x2.id_chek = y2.id_chek
  left join TDV_MAP_NEW_ANAL z2 on y2.art = z2.art
  where y2.art in (select art from s_art where season = 'Летняя')
  and x2.bit_close = 'T' and x2.bit_vozvr = 'F'
  and y2.scan != ' ' and y2.procent = 0
  and trunc(x2.sale_date) >= trunc(to_date('01.02.2020','dd.mm.yyyy'))
  and trunc(x2.sale_date) < trunc(sysdate)
  group by y2.id_shop, nvl(z2.analog, y2.art), y2.asize
) sr on a.id_shop = sr.id_shop and a.text1 = sr.analog and a.asize = sr.asize

left join (
  select a2.id_shop, a2.analog, sum(a2.kol) kol_ost
--  from e_osttek_online_short_rasp_cp a2 
  from e_osttek_online_short_rasp a2 
  group by a2.id_shop, a2.analog having sum(kol) > 0  
) oa on a.id_shop = oa.id_shop and a.text1 = oa.analog

left join (
  select a2.id_shop, a2.analog, a2.asize, sum(a2.kol) kol_ost
--  from e_osttek_online_short_rasp_cp a2 
  from e_osttek_online_short_rasp a2 
  group by a2.id_shop, a2.analog, a2.asize having sum(kol) > 0  
) ora on a.id_shop = ora.id_shop and a.text1 = ora.analog and a.asize = ora.asize
left join (
  select id_shop, art, sum(verme) kol_trans_ost from tdv_new_shop_ost_rasp group by id_shop, art having sum(verme) > 0
) tr on a.id_shop = tr.id_shop and a.text1 = tr.art 
where a.id = 28608
--and block_no = 0
;

select * from e_osttek_online_short_rasp_cp;
select * from e_osttek_online_short_rasp;


select * from TDV_NEW_SHOP_OST_RASP;