select id_shop "МАГАЗИН", nvl(y1.analog, x1.art) "АНАЛОГ", x1.asize "РАЗМЕР", sum(kol) "ОСТАТОК"  from e_osttek_online x1 
left join TDV_MAP_NEW_ANAL y1 on x1.art = y1.art
where x1.art in (select art from s_art where season = 'Зимняя') and x1.ex_time < trunc(sysdate) 
and id_shop in (select shopid from tdm_map_new_shop_group where s_group = 17901)
group by id_shop, nvl(y1.analog, x1.art), x1.asize  having sum(kol) > 0;



--select * from (
select 
--count(*) 
sum(kol)
from (
  select a2.id_shop, /*a2.art,*/ nvl(d.analog, a2.art) analog, a2.asize, sum(a2.kol) kol, 'OST' ost_type
  from e_osttek_online a2
  inner join s_art c on a2.art = c.art
  left join pos_brak x on a2.scan = x.scan
                          and a2.id_shop = x.id_shop
  left join st_shop e on a2.id_shop = e.shopid
  left join TDV_MAP_NEW_ANAL d on d.art = a2.art
  -- фильтрация 
  where ',Зимняя,' like '%,' || c.season || ',%'
        and a2.scan != ' '
        and a2.procent = 0
        and x.scan is null
        --and z.art is null
        and c.groupmw in ('Мужские', 'Женские')
        and a2.art not like '%/О%'
        and a2.id_shop in (select shopid
                           from tdm_map_new_shop_group
                           WHERE s_group = 17901)
        and a2.ex_time < trunc(sysdate) 
  group by a2.id_shop, nvl(d.analog, a2.art), a2.asize
  having sum(kol) > 0
  
  union all 
  
  select id_shop, art, asize, sum(verme) kol, 'TRANS' ost_type
  from TDV_NEW_SHOP_OST 
  group by id_shop, art, asize having sum(verme) > 0

)
--) a
--left join (select distinct id_shop, art, asize, analog from e_osttek_online_short) b on a.id_shop = b.id_shop and a.art = b.art and a.asize = b.asize and a.analog = b.analog 
--where b.art is null
;

select sum(kol) from (
select id_shop, art, asize, sum(verme) kol, 'TRANS'
from TDV_NEW_SHOP_OST 
group by id_shop, art, asize having sum(verme) > 0
);