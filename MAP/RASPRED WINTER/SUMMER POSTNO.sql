select * from t_map_new_poexalo where id = 15739;

select * from t_map_shop_post_result_rasp;
select shopid, dtype from tdm_map_new_shop_group where s_group = 47201;

select * from t_map_shop_post_limit_rasp;

--insert into T_MAP_SHOP_POST_LIMIT_RASP
--select shopid, dtype from TDM_MAP_NEW_SHOP_GROUP where s_group = 47201;




select x.*
from (select b.mod, count(distinct a.art) art_cnt
       from t_map_new_poexalo a
       left join s_art b on a.art = b.art
       where id = 15739
             and id_shop = '0002'
       group by b.mod) x
inner join s_art y on x.mod = y.mod 
order by 
--case when lower(assort) like '%ботинки%' then 1
--    when lower(assort) like '%полусапоги%' then 1
--    when lower(assort) like '%сапоги%' and lower(assort) not like '%полусапоги%' then 2 end,       
         art_cnt desc
;

select a.art, sum(kol) kol
from t_map_new_poexalo a
inner join s_art c on a.art = c.art
where id = 15739
      and id_shop = '0002'
      and c.mod = '1638520'
      and a.art not in (select art
                        from t_map_shop_post_result_rasp
                        where id_shop = '0002')
group by a.art;

set serveroutput on;
declare
  i_id number := 15739;
  
  v_shop_limit t_map_shop_post_limit_rasp.kol%type;
  v_current_shop_kol number := 0;
  v_inc number := 0;
  v_post_count number := 0;
  v_art_count number := 0;
  v_count number := 0;
    
begin
  execute immediate 'truncate table t_map_shop_post_result_rasp';
  
  for r_shop in (select distinct id_shop
                 from t_map_new_poexalo
                 where id = i_id
                 and id_shop != 'fond'
                 order by id_shop)
  loop
    begin
      select kol
      into v_shop_limit
      from t_map_shop_post_limit_rasp
      where id_shop = r_shop.id_shop;
    exception
      when others then
        v_shop_limit := 300;
    end;
    
--    dbms_output.put_line('r_shop : '||r_shop.id_shop||' v_shop_limit : '||v_shop_limit);
    
    v_inc := 1 * 100;
    v_current_shop_kol := 0;
    --     dbms_output.put_line(v_inc);

    -- расчитываю, сколько поставок должно получится по итогу на данный магазин/группу
    begin
      select round_dig(sum(kol) / v_shop_limit, 1, 1)
      into v_post_count
      from t_map_new_poexalo a
      where id = i_id
            and id_shop = r_shop.id_shop;
    exception
      when others then
        v_post_count := 0;
    end;
    
    if v_post_count is null then
      v_post_count := 0;
    end if;
  
    dbms_output.put_line(r_shop.id_shop || ' ' || v_post_count);
    
    for r_post in (1) .. (v_post_count)
    loop
      -- перебираю все модели данной группы
      for r_mod in (select *
                    from (select b.mod, count(distinct a.art) art_cnt
                           from t_map_new_poexalo a
                           left join s_art b on a.art = b.art
                           inner join TDV_MAP_ART_PRIOR c on a.art = c.art
                           where id = i_id
                                 and id_shop = r_shop.id_shop
                                 --and c.postgr = r_prior.postgr
                           group by b.mod)
                    order by art_cnt desc)
      loop
        v_art_count := 0;
          
        if v_current_shop_kol >= v_shop_limit then
          v_current_shop_kol := 0;
          exit;
          --v_inc := v_inc + 1;
        end if;
        
        -- проход по моделям артикула
        for r_art in (select a.art, sum(kol) kol
                      from t_map_new_poexalo a
                      inner join s_art c on a.art = c.art
                      where id = i_id
                            and id_shop = r_shop.id_shop
                            and c.mod = r_mod.mod
                            and a.art not in (select art
                                              from t_map_shop_post_result_rasp
                                              where id_shop = r_shop.id_shop)
                      group by a.art)
        loop
          v_art_count := v_art_count + 1;
          
          insert into t_map_shop_post_result_rasp
              select r_shop.id_shop, r_art.art, r_art.kol, v_inc + r_post
              from dual;
              
          v_current_shop_kol := v_current_shop_kol + r_art.kol;
          dbms_output.put_line(v_art_count||' '||r_mod.art_cnt||' '||v_post_count||' '||r_post);   
          
          -- если
          if v_art_count = round_dig(r_mod.art_cnt / v_post_count, 1, 1) and r_post < v_post_count then
            exit;
          end if;
        end loop; -- артикула моделей
      end loop; -- модели
      
      
      if r_post = v_post_count then
        insert into t_map_shop_post_result_rasp
          select r_shop.id_shop, a.art, a.kol, v_inc + r_post
          from (select a.art, sum(kol) kol
                 from t_map_new_poexalo a
                 inner join s_art c on a.art = c.art
                 where id = i_id
                       and id_shop = r_shop.id_shop
                       and a.art not in (select art
                                         from t_map_shop_post_result_rasp
                                         where id_shop = r_shop.id_shop)
                 group by a.art) a;
      
        select sum(kol)
        into v_count
        from t_map_shop_post_result_rasp a
        where id_shop = r_shop.id_shop
              and a.postno = v_inc + r_post;
      
        if v_count <= 20 and r_post != 1 then
          update t_map_shop_post_result_rasp a
          set postno = v_inc + r_post - 1
          where id_shop = r_shop.id_shop
                and a.postno = v_inc + r_post;
        end if;
      end if;
      
    end loop; -- поставки
    commit;
  end loop; -- магазины
end;