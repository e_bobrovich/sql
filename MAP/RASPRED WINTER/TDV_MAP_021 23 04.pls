create or replace package body tdv_map_021 is

  --************** vaiable *****************************
  pv_rc varchar2(1) default 'F';
  pv_exclude_buy varchar2(1) default 'F'; -- если этот флаг установлен в 'T' , то исключаем из перебросок покупную, образцы, а также обувь на 'Р' и 'Р2'
  pv_sgp_rasp varchar2(1) default 'T'; -- является ли данный расчет распределением с СГП
  pv_removal_shoes_to_rc varchar2(1) default 'F'; -- является ли данный расчет отгрузкой на РЦ по лимиту
  pv_season varchar2(100) default ',Зимняя,';
  pv_filter_plan_in_shop varchar2(1) default 'F'; --исключать ли из остатков невыполненные планы на вывоз
  pv_assort_order varchar2(1) default 'F'; -- использовать ли сортировку по типу ассортимента
  --********************************************************************************************
  --  PROCEDURE
  -- запись в лог
  --********************************************************************************************

  procedure writeLog(i_text varchar2,
                     i_proc varchar2,
                     i_id   in integer) is
  begin
    write_log(i_text, i_proc, i_id);
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: 
  --
  --****************************************************************************************************************************************************
  procedure warehouse8countbox(i_id number) is
    v_kol number;
  begin
    execute immediate 'truncate table tdv_map_warehouse_stock';
    execute immediate 'truncate table tdv_map_warehouse_result';
  
    -- собираем остатки в парах по 8-му складу
    -- короба
    insert into tdv_map_warehouse_stock
      select art, asize, sum(kol) kol, boxty
      from ( -- короба
             select d.art, null asize, sum(d.verme) / f.kol kol, d.boxty
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join (select boxty, sum(kol) kol
                        from T_SAP_BOX_ROST
                        group by boxty) f on d.boxty = f.boxty
             where d.boxty not in (' ', 'N')
                   and lgtyp in ('R08', 'Z08', 'S08')
                   and pv_season like '%,' || e.season || ',%'
             group by d.art, d.boxty, f.kol
             union all
             
             -- россыпь 
             select d.art, d1.asize, sum(d.verme) kol, null
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join s_all_mat d1 on d.matnr = d1.matnr
             where d.boxty in (' ')
                   and d1.asize != 0
                   and lgtyp in ('R08', 'Z08', 'S08')
                   and pv_season like '%,' || e.season || ',%'
             group by d.art, d1.asize
             
             union all
             
             -- транзиты
             select a1.art, asize, sum(kol) kol, null
             from trans_in_way a1
             left join TDV_MAP_NEW_ANAL d on a1.art = d.art
             left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                               and a1.Postno_Lifex = e.id
             inner join s_art c on a1.art = c.art
             where scan != ' '
                   and pv_season like '%,' || c.season || ',%'
                   and a1.art not like '%/О%'
                   and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
             group by a1.art, asize
             having sum(kol) > 0
             
             union all
             -- остатки магазинов
             select a2.art, a2.asize, sum(a2.kol) kol, null
             from e_osttek_online_short a2
             left join st_muya_art f on a2.art = f.art
             left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
                                              and a2.analog = j.art
             where 1 = case
                     when a2.landid = 'RU' and f.art is not null then
                      0
                     else
                      1
                   end -- исключаем для РФ вывоз покупной
                   and j.art is not null -- только то, что плохо продавалось
                   and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40') --из сибири не забираем
                   and procent = 0
             group by a2.art, a2.asize
             having sum(kol) > 0
             
             union all
             -- РЦ
             select a2.art, a2.asize, sum(a2.kol) kol, null
             from tdv_map_rc_data a2
             inner join s_art c on a2.art = c.art
             left join TDV_MAP_NEW_ANAL d on d.art = a2.art
             where pv_season like '%,' || c.season || ',%'
                   and a2.art not like '%/О%'
                   and substr(skladid, 1, 1) != 'W'
                   and substr(skladid, 3, 1) != 'S'
             
             group by a2.art, a2.asize
             having sum(a2.kol) > 0)
      
      group by art, asize, boxty;
  
    -- от россыпи 8-ого склада отнимаю резервы 8-ого склада
    update tdv_map_warehouse_stock x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, asize, sum(verme) verme
                 from tdv_map_new_sap_reserv2 x3
                 where x3.boxty = ' '
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE')
                       and ABLAD = '08'
                 group by art, asize) x2
          where x1.art = x2.art
                and x1.asize = x2.asize)
    where exists (select x1.kol - x2.verme
           from (select art, asize, sum(verme) verme
                  from tdv_map_new_sap_reserv2 x3
                  where x3.boxty = ' '
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE')
                        and ABLAD = '08'
                  group by art, asize) x2
           where x1.asize = x2.asize
                 and x1.art = x2.art);
  
    delete from tdv_map_warehouse_stock
    where kol <= 0;
  
    -- остатки коробов забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, null, 1 kol, null lgtype, boxty
      from t_map_new_poexalo a
      where id = i_id
            and a.boxty in (select boxty
                            from T_SAP_BOX_ROST)
            and (id_shop, art, boxty) not in (select id_shop, art, boxty
                                              from tdv_map_warehouse_result
                                              where boxty is not null)
      group by a.id_shop, a.art, boxty;
  
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    -- перебираем все артикула по магазинам куда должна ехать обувь и соотносим с остатками склада
    for r_row in (select id_shop, a.art, a.asize, sum(a.kol) kol
                  from t_map_new_poexalo a
                  inner join tdv_map_warehouse_stock b on a.art = b.art
                                                          and a.asize = b.asize
                  where id = i_id
                        and (a.boxty not in (select boxty
                                             from T_SAP_BOX_ROST) or a.boxty is null)
                  group by id_shop, a.art, a.asize
                  order by id_shop, a.art, a.asize)
    loop
      select least(a.kol, r_row.kol) kol
      into v_kol
      from tdv_map_warehouse_stock a
      where art = r_row.art
            and asize = r_row.asize;
    
      insert into tdv_map_warehouse_result
        select r_row.id_shop, r_row.art, r_row.asize, v_kol, 8, null boxty
        from dual;
    
      update tdv_map_warehouse_stock
      set kol = kol - v_kol
      where art = r_row.art
            and asize = r_row.asize
            and boxty is null;
    end loop;
    commit;
  
    dbms_output.put_line('111');
  
    -- остатки россыпи забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, a.asize, sum(a.kol) - nvl(b.kol, 0) kol, null lgtype, null boxty
      from t_map_new_poexalo a
      left join (select *
                 from tdv_map_warehouse_result
                 where asize is not null) b on a.id_shop = b.id_shop
                                               and a.art = b.art
                                               and a.asize = b.asize
      where id = i_id
            and (a.boxty not in (select boxty
                                 from T_SAP_BOX_ROST) or a.boxty is null)
      group by a.id_shop, a.art, a.asize, nvl(b.kol, 0);
  
    dbms_output.put_line('1112');
  
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: 
  --
  --****************************************************************************************************************************************************
  procedure warehouse8countbox_invert(i_id number) is
    v_kol number;
  begin
    execute immediate 'truncate table tdv_map_warehouse_stock';
    execute immediate 'truncate table tdv_map_warehouse_result';
  
    -- собираем остатки в парах по 8-му складу
    -- короба
    insert into tdv_map_warehouse_stock
      select art, asize, sum(kol) kol, boxty
      from ( -- короба
             select d.art, null asize, sum(d.verme) / f.kol kol, d.boxty
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join (select boxty, sum(kol) kol
                        from T_SAP_BOX_ROST
                        group by boxty) f on d.boxty = f.boxty
             where d.boxty not in (' ', 'N')
                   and lgtyp not in ('R08', 'Z08', 'S08')
                   and pv_season like '%,' || e.season || ',%'
             group by d.art, d.boxty, f.kol
             union all
             
             -- россыпь 
             select d.art, d1.asize, sum(d.verme) kol, null
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join s_all_mat d1 on d.matnr = d1.matnr
             where d.boxty in (' ')
                   and d1.asize != 0
                   and lgtyp not in ('R08', 'Z08', 'S08')
                   and pv_season like '%,' || e.season || ',%'
             group by d.art, d1.asize)
      
      group by art, asize, boxty;
  
    -- от россыпи 8-ого склада отнимаю резервы 8-ого склада
    update tdv_map_warehouse_stock x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, asize, sum(verme) verme
                 from tdv_map_new_sap_reserv2 x3
                 where x3.boxty = ' '
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE')
                       and ABLAD != '08'
                 group by art, asize) x2
          where x1.art = x2.art
                and x1.asize = x2.asize)
    where exists (select x1.kol - x2.verme
           from (select art, asize, sum(verme) verme
                  from tdv_map_new_sap_reserv2 x3
                  where x3.boxty = ' '
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE')
                        and ABLAD != '08'
                  group by art, asize) x2
           where x1.asize = x2.asize
                 and x1.art = x2.art);
  
    delete from tdv_map_warehouse_stock
    where kol <= 0;
  
    -- остатки коробов забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, null, 1 kol, null lgtype, boxty
      from t_map_new_poexalo a
      where id = i_id
            and a.boxty in (select boxty
                            from T_SAP_BOX_ROST)
            and (id_shop, art, boxty) not in (select id_shop, art, boxty
                                              from tdv_map_warehouse_result
                                              where boxty is not null)
      group by a.id_shop, a.art, boxty;
  
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    -- перебираем все артикула по магазинам куда должна ехать обувь и соотносим с остатками склада
    for r_row in (select id_shop, a.art, a.asize, sum(a.kol) kol
                  from t_map_new_poexalo a
                  inner join tdv_map_warehouse_stock b on a.art = b.art
                                                          and a.asize = b.asize
                  where id = i_id
                        and (a.boxty not in (select boxty
                                             from T_SAP_BOX_ROST) or a.boxty is null)
                  group by id_shop, a.art, a.asize
                  order by id_shop, a.art, a.asize)
    loop
      select least(a.kol, r_row.kol) kol
      into v_kol
      from tdv_map_warehouse_stock a
      where art = r_row.art
            and asize = r_row.asize;
    
      insert into tdv_map_warehouse_result
        select r_row.id_shop, r_row.art, r_row.asize, v_kol, null lgtype, null boxty
        from dual;
    
      update tdv_map_warehouse_stock
      set kol = kol - v_kol
      where art = r_row.art
            and asize = r_row.asize
            and boxty is null;
    end loop;
    commit;
  
    dbms_output.put_line('111');
  
    -- остатки россыпи забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, a.asize, sum(a.kol) - nvl(b.kol, 0) kol, 8 lgtype, null boxty
      from t_map_new_poexalo a
      left join (select *
                 from tdv_map_warehouse_result
                 where asize is not null) b on a.id_shop = b.id_shop
                                               and a.art = b.art
                                               and a.asize = b.asize
      where id = i_id
            and (a.boxty not in (select boxty
                                 from T_SAP_BOX_ROST) or a.boxty is null)
      group by a.id_shop, a.art, a.asize, nvl(b.kol, 0);
  
    dbms_output.put_line('1112');
  
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: раскидывает получившееся распределение по складам
  --
  --****************************************************************************************************************************************************
  procedure warehouse8count(i_id number) is
    v_kol number;
  begin
    execute immediate 'truncate table tdv_map_warehouse_stock';
    execute immediate 'truncate table tdv_map_warehouse_result';
  
    -- собираем остатки в парах по 8-му складу
    -- короба
    insert into tdv_map_warehouse_stock
      select art, asize, sum(kol) kol, null
      from (select art, b.asize, b.kol * a.kol kol
             from (select d.art, d.boxty, sum(d.verme) / f.kol kol
                    from st_sap_ob_ost2 d
                    left join s_art e on d.art = e.art
                    left join (select boxty, sum(kol) kol
                              from T_SAP_BOX_ROST
                              group by boxty) f on d.boxty = f.boxty
                    where d.boxty not in (' ', 'N')
                          and lgtyp in ('R08', 'Z08')
                          and e.season in ('Зимняя')
                    group by d.art, d.boxty, f.kol) a
             inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
             union all
             select d.art, d1.asize, sum(d.verme) kol
             from st_sap_ob_ost2 d
             left join s_art e on d.art = e.art
             left join s_all_mat d1 on d.matnr = d1.matnr
             where d.boxty in (' ')
                   and d1.asize != 0
                   and lgtyp in ('R08', 'Z08')
                   and e.season in ('Зимняя')
             group by d.art, d1.asize)
      group by art, asize;
  
    -- перебираем все артикула по магазинам куда должна ехать обувь и соотносим с остатками склада
    for r_row in (select id_shop, a.art, a.asize, sum(a.kol) kol
                  from t_map_new_poexalo a
                  inner join tdv_map_warehouse_stock b on a.art = b.art
                                                          and a.asize = b.asize
                  where id = i_id
                  group by id_shop, a.art, a.asize
                  order by id_shop, a.art, a.asize)
    loop
      select least(a.kol, r_row.kol) kol
      into v_kol
      from tdv_map_warehouse_stock a
      where art = r_row.art
            and asize = r_row.asize;
    
      insert into tdv_map_warehouse_result
        select r_row.id_shop, r_row.art, r_row.asize, v_kol, 8, null boxty
        from dual;
    
      update tdv_map_warehouse_stock
      set kol = kol - v_kol
      where art = r_row.art
            and asize = r_row.asize;
    end loop;
  
    -- остатки забираем с других складов
    insert into tdv_map_warehouse_result
      select a.id_shop, a.art, a.asize, sum(a.kol) - nvl(b.kol, 0) kol, null lgtype, null boxty
      from t_map_new_poexalo a
      left join tdv_map_warehouse_result b on a.id_shop = b.id_shop
                                              and a.art = b.art
                                              and a.asize = b.asize
      where id = i_id
      group by a.id_shop, a.art, a.asize, nvl(b.kol, 0);
  
    delete from tdv_map_warehouse_result
    where kol = 0;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 16.11.2018 16:31:16
  -- Comments:
  --
  --****************************************************************************************************************************************************
  procedure shop_post_limit(i_id number) is
    v_shop_limit tdv_shop_post_limit.kol%type;
    v_current_shop_kol number := 0;
    v_inc number := 0;
    v_post_count number := 0;
    v_art_count number := 0;
    v_count number := 0;
  begin
  
    execute immediate 'truncate table tdv_shop_post_limit_result';
  
    for r_shop in (select distinct id_shop
                   from t_map_new_poexalo
                   where id = i_id
                   --and id_shop = '3623'
                   --and rownum < 10
                   order by id_shop)
    loop
    
      begin
        select kol
        into v_shop_limit
        from tdv_shop_post_limit
        where id_shop = r_shop.id_shop;
      exception
        when others then
          v_shop_limit := 300;
      end;
    
      --dbms_output.put_line('1 ' || r_shop.id_shop);
    
      for r_prior in (select distinct a.postgr
                      from TDV_MAP_ART_PRIOR a)
      loop
      
        v_inc := r_prior.postgr * 100;
        v_current_shop_kol := 0;
        --     dbms_output.put_line(v_inc);
      
        -- расчитываю, сколько поставок должно получится по итогу на данный магазин/группу
        begin
          select round_dig(sum(kol) / v_shop_limit, 1, 1)
          into v_post_count
          from t_map_new_poexalo a
          inner join TDV_MAP_ART_PRIOR b on a.art = b.art
          where id = i_id
                and id_shop = r_shop.id_shop
                and b.postgr = r_prior.postgr;
        exception
          when others then
            v_post_count := 0;
        end;
      
        if v_post_count is null then
          v_post_count := 0;
        end if;
      
        dbms_output.put_line(r_shop.id_shop || ' ' || v_post_count);
      
        for r_post in (1) .. (v_post_count)
        loop
        
          -- перебираю все модели данной группы
          for r_mod in (select *
                        from (select b.mod, count(distinct a.art) art_cnt
                               from t_map_new_poexalo a
                               left join s_art b on a.art = b.art
                               inner join TDV_MAP_ART_PRIOR c on a.art = c.art
                               where id = i_id
                                     and id_shop = r_shop.id_shop
                                     and c.postgr = r_prior.postgr
                               --and mod = '1616025'
                               group by b.mod)
                        order by art_cnt desc)
          loop
          
            v_art_count := 0;
          
            if v_current_shop_kol >= v_shop_limit then
              v_current_shop_kol := 0;
              exit;
              --v_inc := v_inc + 1;
            end if;
          
            for r_art in (select a.art, sum(kol) kol
                          from t_map_new_poexalo a
                          inner join TDV_MAP_ART_PRIOR b on a.art = b.art
                          inner join s_art c on a.art = c.art
                          where id = i_id
                                and id_shop = r_shop.id_shop
                                and b.postgr = r_prior.postgr
                                and c.mod = r_mod.mod
                                and a.art not in (select art
                                                  from tdv_shop_post_limit_result
                                                  where id_shop = r_shop.id_shop)
                          group by a.art)
            loop
              v_art_count := v_art_count + 1;
            
              /*              if v_current_shop_kol >= v_shop_limit then
                v_current_shop_kol := 0;
                v_inc := v_inc + 1;
              end if;*/
            
              insert into tdv_shop_post_limit_result
                select r_shop.id_shop, r_art.art, r_art.kol, v_inc + r_post
                from dual;
            
              v_current_shop_kol := v_current_shop_kol + r_art.kol;
              --dbms_output.put_line(v_art_count||' '||r_mod.art_cnt||' '||v_post_count||' '||r_post);
            
              if v_art_count = round_dig(r_mod.art_cnt / v_post_count, 1, 1) and r_post < v_post_count then
                exit;
              end if;
            end loop;
          end loop;
        
          if r_post = v_post_count then
            insert into tdv_shop_post_limit_result
              select r_shop.id_shop, a.art, a.kol, v_inc + r_post
              from (select a.art, sum(kol) kol
                     from t_map_new_poexalo a
                     inner join TDV_MAP_ART_PRIOR b on a.art = b.art
                     inner join s_art c on a.art = c.art
                     where id = i_id
                           and id_shop = r_shop.id_shop
                           and b.postgr = r_prior.postgr
                           and a.art not in (select art
                                             from tdv_shop_post_limit_result
                                             where id_shop = r_shop.id_shop)
                     group by a.art) a;
          
            select sum(kol)
            into v_count
            from tdv_shop_post_limit_result a
            where id_shop = r_shop.id_shop
                  and a.postno = v_inc + r_post;
          
            if v_count <= 20 and r_post != 1 then
              update tdv_shop_post_limit_result a
              set postno = v_inc + r_post - 1
              where id_shop = r_shop.id_shop
                    and a.postno = v_inc + r_post;
            end if;
          end if;
        
        end loop;
      
      -- если это последняя поставка и собрано меньше 20 пар, то добавляем её в предыдущую
      -- if r_post = v_post_count and 
      --dbms_output.put_line(r_shop.id_shop||' '||r_prior.postgr);
      end loop; --r_prior
      commit;
    end loop; -- r_shop
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 26.03.2018 10:02:32
  -- Comments: возвращает номер расчета на 1,2,3.. месяца назад
  --
  --****************************************************************************************************************************************************
  function getOstCalculationID(i_months_ago in integer) return e_oborot1.id%type is
    v_id e_oborot1.id%type;
  begin
  
    select id
    into v_id
    from e_oborot1
    where text like '%$MAP#%' || i_months_ago * 30 || '%'
          and date_r = (select max(date_r)
                        from e_oborot1
                        where text like '%$MAP#%' || i_months_ago * 30 || '%');
  
    return v_id;
  end;

  --********************************************************************************************
  --
  --
  --********************************************************************************************
  /*procedure blockStart(i_block_no in integer) is
    pragma autonomous_transaction;
  begin
    pv_CurBlockNo := i_block_no;
    pv_CurBlockStartTime := dbms_utility.get_time;
  
    update TDV_MAP_13_RESULT set ACTIVE_BLOCK = i_block_no, TIME_START_BLOCK = systimestamp where id = pv_id;
    commit;
  end ;
  
  procedure blockEnd(i_id number,
                     i_block_text varchar2) is
  begin
  
    if pv_CurBlockNo is null then
      return;
    end if;
  
    writeLog( 'Fill_ross3/Block: '|| pv_CurBlockNo ||'. '||i_block_text  , (dbms_utility.get_time - pv_CurBlockStartTime) / 100  , i_id );
    pv_CurBlockNo := null;
    pv_CurBlockStartTime := null;
  end ;    */

  --********************************************************************************************
  --
  --
  --********************************************************************************************
  procedure test(i_id               in number,
                 i_s_group          number,
                 i_test_no          in number default null,
                 i_show_only_failed in varchar2 default 'F') is
    pragma autonomous_transaction;
    v_count integer;
    v_test_no number default 0;
    v_start_time timestamp;
  begin
    v_test_no := 1;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы отметили к вывозу те сезоны, которые проставлены в tdm_map_new_shop_group.season_out *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, season
             from tdm_map_new_shop_group x
             inner join (select distinct season
                        from s_art
                        where season != ' ') y on upper(x.season_out) like '%' || upper(y.season) || '%'
             where s_group = i_s_group) a1
      full join (select distinct id_shop, season
                 from TDV_MAP_NEW_OST_SALE a
                 where a.out_block = 0) a2 on a1.id_shop = a2.id_shop
                                              and upper(a1.season) = upper(a2.season)
      where a1.id_shop is null --or a2.id_shop is null
      ;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы отметили к вывозу те сезоны, которые проставлены в tdm_map_new_shop_group.season_out', null,
           null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы отметили к вывозу те сезоны, которые проставлены в tdm_map_new_shop_group.season_out', null,
           null);
      end if;
    end if;
  
    v_test_no := 2;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не вывезли из магазина сезон который нельзя было вывозить *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, season
             from tdm_map_new_shop_group x
             inner join (select distinct season
                        from s_art
                        where season != ' ') y on upper(x.season_out) like '%' || upper(y.season) || '%'
             where s_group = i_s_group) a1
      full join (select distinct id_shop_from id_shop, season
                 from t_map_new_poexalo a
                 where a.id = i_id) a2 on a1.id_shop = a2.id_shop
                                          and upper(a1.season) = upper(a2.season)
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не вывезли из магазина сезон который нельзя было вывозить', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не вывезли из магазина сезон который нельзя было вывозить', null, null);
      end if;
    end if;
  
    v_test_no := 3;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не ввезли в магазина сезон который нельзя было ввозить *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, season
             from tdm_map_new_shop_group x
             inner join (select distinct season
                        from s_art
                        where season != ' ') y on upper(x.season_in) like '%' || upper(y.season) || '%'
             where s_group = i_s_group) a1
      full join (select distinct id_shop id_shop, season
                 from t_map_new_poexalo a
                 where a.id = i_id) a2 on a1.id_shop = a2.id_shop
                                          and upper(a1.season) = upper(a2.season)
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не ввезли в магазина сезон который нельзя было ввозить', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не ввезли в магазина сезон который нельзя было ввозить', null, null);
      end if;
    end if;
  
    v_test_no := 4;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не вывезли из магазина больше пар, чем разрешено *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, x.count_limit_out
             from tdm_map_new_shop_group x
             where s_group = i_s_group) a1
      full join (select distinct id_shop_from id_shop, sum(kol) kol
                 from t_map_new_poexalo a
                 where a.id = i_id
                 group by id_shop_from) a2 on a1.id_shop = a2.id_shop
                                              and a1.count_limit_out >= a2.kol
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не вывезли из магазина больше пар, чем разрешено', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не вывезли из магазина больше пар, чем разрешено', null, null);
      end if;
    end if;
  
    v_test_no := 5;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы не ввезли в магазина больше пар, чем разрешено *******');
      select count(*)
      into v_count
      from (select x.shopid id_shop, x.count_limit_in
             from tdm_map_new_shop_group x
             where s_group = i_s_group) a1
      full join (select distinct id_shop id_shop, sum(kol) kol
                 from t_map_new_poexalo a
                 where a.id = i_id
                 group by id_shop) a2 on a1.id_shop = a2.id_shop
                                         and a1.count_limit_in + 8 >= a2.kol
      where a1.id_shop is null;
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы не ввезли в магазина больше пар, чем разрешено', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы не ввезли в магазина больше пар, чем разрешено', null, null);
      end if;
    end if;
  
    v_test_no := 6;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что мы ввозим на магазин артикула только из разрешенного списка артикулов*******');
    
      -- заполнение блэклиста для магазинов, которые могут брать только определенные артикула {
      execute immediate 'truncate table tdv_map_black_list_all';
    
      insert into tdv_map_black_list_all
        select distinct x.shopid id_shop, art, analog
        from tdm_map_new_shop_group x
        left join (select bl1.name, bl2.art, nvl(an.analog, bl2.art) analog
                   from tdv_map_black_list1 bl1
                   left join tdv_map_black_list2 bl2 on bl1.id = bl2.id
                   left join TDV_MAP_NEW_ANAL an on an.art = bl2.art) y on ',' || y.name || ',' like
                                                                           '%,' || x.dtype_in || ',%'
        where x.s_group = i_s_group
              and rtrim(x.dtype_in, ' ') is not null
              and art is not null;
      -- }
    
      select count(*)
      into v_count
      from (select a.id_shop, a.art
             from t_map_new_poexalo a
             inner join tdm_map_new_shop_group b on a.id_shop = b.shopid
                                                    and b.s_group = i_s_group
                                                    and rtrim(b.dtype_in) is not null
             left join (select distinct id_shop, art, analog
                       from tdv_map_black_list_all) c on c.id_shop = a.id_shop
                                                         and (a.art = c.art or a.art = c.analog)
             where id = i_id
                   and c.art is null);
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что мы ввозим на магазин артикула только из разрешенного списка артикулов', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что мы ввозим на магазин артикула только из разрешенного списка артикулов', null, null);
      end if;
    end if;
  
    v_test_no := 7;
    if i_test_no is null or v_test_no = i_test_no then
      v_start_time := systimestamp;
      dbms_output.put_line('******* проверка, что магазин  получает обувь только от разрешенных отделений *******');
      select count(*)
      into v_count
      from (select x.id_shop
             from t_map_new_poexalo x
             inner join tdm_map_new_shop_group b on x.id_shop = b.shopid
                                                    and b.s_group = i_s_group
                                                    and rtrim(b.shop_in) is not null
             left join (select distinct id_shop, id_shop_from
                       from t_map_new_poexalo y
                       where id = i_id) y on x.id_shop = y.id_shop
                                             and ',' || b.shop_in || ',' like '%,' || y.id_shop || ',%'
             where id = i_id
                   and y.id_shop_from is null);
    
      if v_count = 0 then
        if i_show_only_failed = 'F' then
          dbms_output.put_line('test: ' || v_test_no || ' OK');
        end if;
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'OK',
           'проверка, что магазин  получает обувь только от разрешенных отделений', null, null);
      else
        dbms_output.put_line('test: ' || v_test_no || ' FAILED !');
        insert into RI_TEST_RESULT
        values
          (i_id, 'tdv_map_013', v_start_time, systimestamp, v_test_no, 'FAILED',
           'проверка, что магазин  получает обувь только от разрешенных отделений', null, null);
      end if;
    end if;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 28.02.2018 9:30:34
  -- Start Procedure:
  -- Заполняет таблицу данными с РЦ и СВХ
  --****************************************************************************************************************************************************
  procedure fill_rc_svx_data(i_siberia_in varchar2 default 'T') is
  begin
  
    execute immediate 'truncate table tdv_map_rc_data';
  
    -- Из СВХ Сибири ничего не забираем
  
    insert into tdv_map_rc_data
      --СВХ
      select skladid, shopid id_shop, art, asize, count(*) kol, 0 kol2
      from sklad_ost_owners
      where shopid not in ('0', ' ')
           /*           
            and shopid in (select shopid
           from tdm_map_new_shop_group
           where s_group = i_s_group)
           */
            and shopid in (select shopid
                           from st_shop z
                           where z.org_kod = 'SHP') --and 1 = 0
            and case when i_siberia_in = 'F' then substr(shopid, 1, 2) else ' ' end not in ('36', '38', '39', '40')
      group by skladid, shopid, art, asize
      
      union all
      --РЦ
      select DCID skladid, owner id_shop, art, asize, sum(kol) kol, 0 kol2
      from dist_center.e_osttek
      where owner in (select shopid
                      from st_shop z
                      where z.org_kod = 'SHP')
      and case when i_siberia_in = 'F' then substr(owner, 1, 2) else ' ' end not in ('36', '38', '39', '40')
      group by DCID, owner, art, asize
      having sum(kol) > 0;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 12:58:37
  -- Comments:
  --
  --****************************************************************************************************************************************************
  procedure fillBaseData2(i_s_group          integer,
                          i_sales_start_date date,
                          i_sales_end_date   date,
                          i_stock_doc_no     integer) is
  begin
    execute immediate 'truncate table TDV_NEW_SHOP_OST'; -- таблица с транзитами и данными с заказами , лежащими на СГП
    execute immediate 'truncate table TDV_MAP_NEW_OST_SALE'; -- - Остатки вместе с продажами в разрезе аналогов
    --execute immediate 'truncate table TDV_MAP_NEW_OST_FULL'; -- текущие остатки для расчета
    execute immediate 'truncate table tdv_map_osttek'; -- текущие остатки собраные в одной таблице (нужно было на случай, когда текущие
  
    /*    -- отдельно закидываю текущие остатки, что бы потом не лазить по этой большой таблице
    insert into TDV_MAP_NEW_OST_FULL
      select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0, 0 kol_rc
      from e_osttek a2
      left join s_art c on a2.art = c.art
      group by a2.id_shop, a2.art, a2.asize, c.season;
    --      having sum(kol) > 0 or sum(kol_rc) > 0
    
    commit;*/
  
    insert into TDV_NEW_SHOP_OST
    
    -- добавляю резервы из САПА
    
      select distinct y.shopid, nvl(a2.analog, x3.art), to_number(asize, '9999.99'), 1, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join s_shop y on x3.kunnr = y.shopnum
      left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
      where x3.boxty = ' '
            and to_number(asize, '9999.99') != 0 -- россыпь
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
            and verme > 0
            and shopid is not null
      
      union
      select distinct y.shopid, nvl(a2.analog, a.art), to_number(b.asize, '9999.99'), 1, 'TRANS'
      from tdv_map_new_sap_reserv2 a
      inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
      inner join s_shop y on a.kunnr = y.shopnum
      left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
      where a.boxty != ' ' --короба
            and verme > 0
            and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
      
      union
      select distinct x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 1, 'TRANS'
      from tdv_map_new_sap_reserv2 x3
      left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST
                 group by boxty) e on x3.boxty = e.boxty
      left join s_shop x on x3.kunnr = x.shopnum
      left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
      where kunnr not in ('X_NERASP', 'X_SD_USE')
            and trans = 'X'
      group by nvl(a2.analog, x3.art), nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid
      
      union
      -- остатки магазинов -- покупную для РФ оставляем в магазине -- и некондицию
      select a2.id_shop, a2.analog, a2.asize, 1, 'TRANS'
      FROM e_osttek_online_short a2
      inner join s_art c on a2.art = c.art
      --LEFT JOIN st_muya_art F ON a2.art = F.art
--      left join tdv_map_bad_sales j on a2.id_shop = j.id_shop and a2.analog = j.art
      where 
      (
--        1 = case when a2.landid = 'RU' and f.art is not null then 1 else 0 end 
          --f.art is not null
          --or 
          a2.procent != 0 -- некондицию оставляем
          or c.groupmw not in ('Мужские', 'Женские') --детскую оставляем
          --or j.art is null -- что хорошо продавалось не забираем
          --OR substr(a2.id_shop, 1, 2) IN ('36', '38', '39', '40') --из сибири не забираем
      ) 
      group by a2.id_shop, a2.analog, a2.asize
      having sum(kol) > 0
      
      union
      -- остатки РЦ и СВХ
      select a2.id_shop, nvl(d.analog, a2.art) analog, a2.asize, 1, 'TRANS'
      from tdv_map_rc_data a2
      inner join s_art c on a2.art = c.art
      left join TDV_MAP_NEW_ANAL d on d.art = a2.art
      where pv_season like '%,' || c.season || ',%'
            and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')
            --and substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40')
      and (a2.art like '%/О%' /*or j.art is null*/ -- только то, что плохо продавалось
      or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') --из сибири не забираем
--      and 1 = case when e.landid = 'RU' and f.art is not null then 1 else 0 end
      ) -- исключаем для РФ вывоз покупной   
    
      group by a2.id_shop, a2.asize, nvl(d.analog, a2.art)
      having sum(a2.kol) > 0;
  
    commit;
  
    
    if pv_filter_plan_in_shop = 'F' then
      -- формирую остатки на дату
      if i_stock_doc_no is null then
      
        insert into tdv_map_osttek
          select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
          from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
                 from e_osttek_online a2
                 inner join s_art c on a2.art = c.art
                 left join pos_brak x on a2.scan = x.scan
                                         and a2.id_shop = x.id_shop
                 --left join st_muya_art z on a2.art = z.art                        
                                         
                 where a2.scan != ' '
                       and pv_season like '%,' || c.season || ',%' -- только нужный сезон
                       and a2.procent = 0 -- без некондиции
                       and c.groupmw in ('Мужские', 'Женские') -- без детской обуви
                       --and z.art is null -- без покупной
                       and x.art is null -- без брака
                       and a2.id_shop in (select shopid
                                          from tdm_map_new_shop_group
                                          where s_group = i_s_group)
                       and a2.id_shop in (select shopid
                                          from st_shop z
                                          WHERE z.org_kod = 'SHP')
                       AND x.SCAN IS NULL
                       --and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40')
                 group by a2.id_shop, a2.art, a2.asize, c.season
                 having sum(kol) > 0)
          group by id_shop, art, asize, season;
      
      else
        
        insert into tdv_map_osttek
          select id_shop, art, asize, sum(kol) kol, season, 0 out_block, sum(kol_rc) kol_rc, sum(kol) kol
          from (select shopid id_shop, a.art, asize, sum(oste) kol, c.season, 0 out_block, 0 kol_rc
                 from e_oborot2 a
                 inner join s_art c on a.art = c.art
                 --left join st_muya_art z on a.art = z.art    
                 where id = i_stock_doc_no
                       and c.mtart != 'ZHW3'
                       and procent = 0 -- без некондиции
                       --and z.art is null -- без покупной
                       and c.groupmw in ('Мужские', 'Женские') -- без детской обуви
                       and pv_season like '%,' || c.season || ',%' -- только нужный сезон
                       and a.shopid in (select shopid
                                        from tdm_map_new_shop_group
                                        WHERE s_group = i_s_group)
                       --and substr(a.shopid, 1, 2) not in ('36', '38', '39', '40')
                 group by shopid, a.art, asize, c.season)
          group by id_shop, art, asize, season;
      
      end if;
    
    else
      -- формирую остатки на дату
      if i_stock_doc_no is null then
        
        insert into tdv_map_osttek
          select x.id_shop, x.art, x.asize, x.kol - nvl(y.kol_Plan,0) kol_ost, x.season, 1, 0 kol_rc, x.kol - nvl(y.kol_Plan,0) kol_ost2 
          from (
            select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
            from e_osttek_online a2
            inner join s_art c on a2.art = c.art
            left join pos_brak x on a2.scan = x.scan
                                   and a2.id_shop = x.id_shop
            --left join st_muya_art z on a2.art = z.art                        
            where a2.scan != ' '
                 and pv_season like '%,' || c.season || ',%' -- только нужный сезон
                 and a2.procent = 0 -- без некондиции
                 and c.groupmw in ('Мужские', 'Женские') -- без детской обуви
                 --and z.art is null -- без покупной
                 and x.art is null -- без брака
                 and a2.id_shop in (select shopid
                                    from tdm_map_new_shop_group
                                    where s_group = i_s_group)
                 and a2.id_shop in (select shopid
                                    from st_shop z
                                    WHERE z.org_kod = 'SHP')
                 AND x.SCAN IS NULL
                 --and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40')
            group by a2.id_shop, a2.art, a2.asize, c.season
            having sum(kol) > 0
          ) x
          left join (
            select b.kpodr id_shop, b.art, b.asize, sum(b.kol) kol_plan from d_planot1 a 
            inner join d_planot2 b on a.rel = b.rel and a.kpodr = b.kpodr
            where a.kpodr in (select shopid
                              from tdm_map_new_shop_group
                              where s_group = i_s_group)
            and a.rel in (select rel_planot from d_rasxod1 
                          where id_shop in (select shopid
                                            from tdm_map_new_shop_group
                                            where s_group = i_s_group) 
                          and bit_close = 'F')
            group by b.kpodr, b.art, b.asize
          ) y on x.id_shop = y.id_shop and x.art = y.art and x.asize = y.asize  
          where x.kol - nvl(y.kol_Plan,0) > 0;
      
      else
      
        insert into tdv_map_osttek
          select x.id_shop, x.art, x.asize, x.kol - nvl(y.kol_Plan,0) kol_ost, x.season, 0 out_block, 0 kol_rc, x.kol - nvl(y.kol_Plan,0) kol_ost2 
          from (
            select shopid id_shop, a.art, asize, sum(oste) kol, c.season, 0 out_block, 0 kol_rc
            from e_oborot2 a
            inner join s_art c on a.art = c.art
            --left join st_muya_art z on a.art = z.art    
            where id = i_stock_doc_no
                 and c.mtart != 'ZHW3'
                 and procent = 0 -- без некондиции
                 --and z.art is null -- без покупной
                 and c.groupmw in ('Мужские', 'Женские') -- без детской обуви
                 and pv_season like '%,' || c.season || ',%' -- только нужный сезон
                 and a.shopid in (select shopid
                                  from tdm_map_new_shop_group
                                  WHERE s_group = i_s_group)
                 --and substr(a.shopid, 1, 2) not in ('36', '38', '39', '40')
            group by shopid, a.art, asize, c.season
          ) x
          left join (
            select b.kpodr id_shop, b.art, b.asize, sum(b.kol) kol_plan from d_planot1 a 
            inner join d_planot2 b on a.rel = b.rel and a.kpodr = b.kpodr
            where a.kpodr in (select shopid
                              from tdm_map_new_shop_group
                              where s_group = i_s_group)
            and a.rel in (select rel_planot from d_rasxod1 
                          where id_shop in (select shopid
                                            from tdm_map_new_shop_group
                                            where s_group = i_s_group) 
                          and bit_close = 'F')
            group by b.kpodr, b.art, b.asize
          ) y on x.id_shop = y.id_shop and x.art = y.art and x.asize = y.asize  
          where x.kol - nvl(y.kol_Plan,0) > 0;  
      
      end if;
    
    end if;
    
    commit;
  
    -- Остатки вместе с продажами в разрезе аналогов
    insert into TDV_MAP_NEW_OST_SALE
      select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 0 out_block, sum(kol_rc) kol_rc,
             sum(kol2) kol2
      from (
             -- текущие остатки
             select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
             from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, z.season, null,
                            sum(kol_rc) kol_rc, sum(a2.kol2) kol2
                     from tdv_map_osttek a2
                     left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
                     left join s_art z on a2.art = z.art
                     group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize, z.season
                     having sum(kol) > 0) a
             where pv_season like '%,' || season || ',%' -- только нужный сезон        
             group by id_shop, a.art, asize, season
             
             union all
             
             -- все продажи данной обуви за выбранный период
             select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0, sum(b.kol) kol_sale, 0 kol_rc,
                     0 kol2
             from pos_sale1 a
             inner join pos_sale2 b on a.id_chek = b.id_chek
                                       and a.id_shop = b.id_shop
             inner join s_art c on b.art = c.art
             left join TDV_MAP_NEW_ANAL d on b.art = d.art
             where a.bit_close = 'T'
                   and a.bit_vozvr = 'F'
                   and b.scan != ' '
                   and b.procent = 0
                   and a.id_shop in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group)
                   and trunc(a.sale_date) >= trunc(i_sales_start_date)
                   and trunc(a.sale_date) <= trunc(i_sales_end_date)
                   and a.id_shop in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = i_s_group)
                  AND pv_season LIKE '%,' || C.season || ',%' -- только нужный сезон  
                  --and substr(a.id_shop, 1, 2) not in ('36', '38', '39', '40')
             group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season) a
      
      group by id_shop, a.art, a.asize;
  
    commit;
  end;

  --********************************************************************************************
  --
  --
  --********************************************************************************************

  --********************************************************************************************
  --
  --
  --********************************************************************************************
  procedure fillOborotDaysLast(i_day_last number) is
    v_id integer;
  begin
    select max(id) + 1
    into v_id
    from e_oborot1;
  
    insert into E_OBOROT1
      (ID, USERS, DATEB, DATEE, MOL_DOLG, MOL_FIO, MOL_TAB, TEXT)
    values
      (v_id, 'Только чтение', sysdate - i_day_last, sysdate - i_day_last, ' ', ' ', ' ',
       'Расчет ' || i_day_last || ' дней назад , $MAP#' || i_day_last || '#');
  
    insert into E_OBOROT3
      (ID, SHOPID, DATEN)
      select AA.*
      from (select v_id, BB.SHOPID,
                    (select max(DATEN)
                      from CONFIG
                      where CONFIG.SHOPID = BB.SHOPID) as DATEN
             from (select a.shopid
                    from s_shop a
                    left join st_shop b on a.shopid = b.shopid
                                          --where lower(a.cityname)  in ('краснодар', 'астрахань', 'ставрополь', 'ростов', 'волгоград', 'волжский', 'воронеж', 'липецк', 'тамбов' )
                                           and b.org_kod = 'SHP') BB) AA
      where not DATEN is null
      order by SHOPID;
  
    OBOROTN1(v_id, 'F');
  
    commit;
  
    dbms_output.put_line('Complete. DocNo: ' || v_id);
  end;

  /*procedure stopProcedure is
    v_count number;
  begin
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo where id = pv_id ;
  
    update TDV_MAP_13_RESULT set DATE_END = systimestamp,
                                                 COUNT_RESULT = 'complete',
                                                 SUM_KOL = v_count
    where id = pv_id;
  
    commit;
  
    RAISE_APPLICATION_ERROR (-20000, 'Stop procedure manual');
  
  end stopProcedure;*/

  --********************************************************************************************
  --
  --
  --********************************************************************************************
  procedure run_baby(i_id_hist number default null -- если требуется повторить один из прошлых расчетов
                     ) is
    v_id t_map_new_poexalo.id%type;
    v_str varchar2(2000);
    v_count number;
  
    v_min_no number default 12500;
    v_max_no number default 12600;
  begin
  
    -- сохраняем в истории данные по расчетам {
    if i_id_hist is null then
      delete from TDV_MAP_COLOR_PROC_HIST
      where count_id = v_id;
      insert into TDV_MAP_COLOR_PROC_HIST
        select v_id, a.*
        from TDV_MAP_COLOR_PROC a;
    
      delete from TDV_MAP_CLUSTER_PROP_HIST
      where count_id = v_id;
      insert into TDV_MAP_CLUSTER_PROP_HIST
        select v_id, a.*
        from TDV_MAP_CLUSTER_PROP a;
    else
      -- если нужно повторить расчет , то берем данные из истории
      -- проверяем был ли такой расчет:
      select count(id)
      into v_count
      from TDV_MAP_13_RESULT
      where id = i_id_hist;
    
      if v_count = 0 then
        return;
        -- нужно добавить запись ошибки в лог
      end if;
    
      delete from TDV_MAP_COLOR_PROC;
      insert into TDV_MAP_COLOR_PROC
        select x.id, x.color, SALE_OT, SALE_TO, PROC_OT, PROC_TO
        from TDV_MAP_COLOR_PROC_HIST x
        where count_id = i_id_hist;
    
      delete from TDV_MAP_CLUSTER_PROP;
      insert into TDV_MAP_CLUSTER_PROP
        select x.cluster_name, x.is_stage, x.stage_parent, x.id_color_proc, x.is_sgp, x.ost_month_last
        from TDV_MAP_CLUSTER_PROP_HIST x
        where count_id = i_id_hist;
    
      select substr(max(s_group), 1, length(max(s_group)) - 2) || '00',
             substr(max(s_group), 1, length(max(s_group)) - 2) || '00' + 100
      into v_min_no, v_max_no
      from TDV_MAP_13_RESULT
      where id = i_id_hist;
    end if;
  
    commit;
    -- }
  
    for r_row in (select distinct a.stage_parent
                  from TDV_MAP_CLUSTER_PROP a
                  where a.is_stage is not null
                        and a.stage_parent is not null)
    loop
    
      begin
        v_str := null;
      
        -- 1. запуск расчетов первого этапа с общим "родителем"
        for r_stage in (select distinct a.s_group, b.is_sgp, b.id_color_proc, b.ost_month_last
                        from tdm_map_new_shop_group a
                        inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) = trim(upper(b.cluster_name))
                        where b.stage_parent = r_row.stage_parent
                              and b.is_stage is not null
                              and a.s_group >= v_min_no
                              and a.s_group <= v_max_no)
        loop
        
          /*          go(r_stage.s_group, '1,2,3,4,9,10', 'F', null, r_stage.id_color_proc, r_stage.is_sgp, r_row.stage_parent,
          r_stage.ost_month_last, pv_sgp_rasp);*/
        
          go(r_stage.s_group, '!4', 'F', null, r_stage.id_color_proc, r_stage.is_sgp, r_row.stage_parent,
             r_stage.ost_month_last, pv_sgp_rasp);
        
          select max(id)
          into v_id
          from t_map_new_poexalo;
        
          v_str := v_str || ',' || v_id;
        
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_stage.s_group, r_row.stage_parent, v_id);
        end loop;
        --dbms_output.put_line(v_str);
      
        -- 2. Запуск расчета второго этапа ("родителя"), который учитывает расчеты всех своих первых этапов
        for r_parent_stage in (select distinct a.s_group, b.id_color_proc, b.is_sgp, b.ost_month_last
                               from tdm_map_new_shop_group a
                               inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) =
                                                                    trim(upper(b.cluster_name))
                               where b.cluster_name = r_row.stage_parent
                                     and b.is_stage is not null
                                     and a.s_group >= v_min_no
                                     and a.s_group <= v_max_no)
        loop
          if r_parent_stage.is_sgp = 'T' then
            go(r_parent_stage.s_group, '1,13', 'F', v_str, r_parent_stage.id_color_proc, r_parent_stage.is_sgp,
               r_row.stage_parent, r_parent_stage.ost_month_last, pv_sgp_rasp);
          else
            go(r_parent_stage.s_group, null, 'F', v_str, r_parent_stage.id_color_proc, r_parent_stage.is_sgp,
               r_row.stage_parent, r_parent_stage.ost_month_last, pv_sgp_rasp);
          end if;
        
          select max(id)
          into v_id
          from t_map_new_poexalo;
        
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_parent_stage.s_group, r_row.stage_parent,
                           v_id);
        end loop;
      
      exception
        when others then
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, ' ', 'расчет', ' ');
      end;
    
    end loop;
  
    -- 3. запуск безэтапных расчетов
    for r_row in (select distinct a.s_group, b.id_color_proc, b.is_sgp, b.ost_month_last
                  from tdm_map_new_shop_group a
                  inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) = trim(upper(b.cluster_name))
                  where b.is_stage is null
                        and a.s_group >= v_min_no
                        and a.s_group <= v_max_no)
    loop
      begin
        if r_row.is_sgp = 'T' then
          go(r_row.s_group, '1,13', 'F', null, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last,
             pv_sgp_rasp);
        else
          go(r_row.s_group, null, 'F', null, r_row.id_color_proc, r_row.is_sgp, null, r_row.ost_month_last, pv_sgp_rasp);
        end if;
      
        select max(id)
        into v_id
        from t_map_new_poexalo;
        dbms_output.put_line(v_id);
        WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_row.s_group, 'расчет', v_id);
      exception
        when others then
          WRITE_ERROR_PROC('run_baby', sqlcode, sqlerrm, 's_group = ' || r_row.s_group, 'расчет', ' ');
      end;
    end loop;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 27.02.2018 21:59:41
  -- Start Procedure:
  --
  --****************************************************************************************************************************************************
  procedure cut_shop_and_rc(i_id varchar2) is
    v_kol number default 0;
    v_art varchar2(50 char);
    v_asize number;
    v_skladid tdv_map_rc_data.skladid%type;
  begin
    -- возвращаю реальные остатки магазина
    update tdv_map_osttek
    set kol = kol - kol_rc, kol2 = kol2 - kol_rc;
  
    -- заношу данные по таблице СВХ-РЦ в расчитываемое поле
    update tdv_map_rc_data
    set kol2 = kol;
  
    -- прохожусь по каждому магазину в расчете
    -- и по каждому артикулу-размеру который был у этого магазина на РЦ
    for r_shop in (select distinct id_shop_from id_shop
                   from t_map_new_poexalo
                   where id = i_id)
    loop
      v_kol := 0;
      for r_row in (select a.*, b.kol kol_ost, b.kol_rc
                    from t_map_new_poexalo a
                    inner join tdv_map_osttek b on a.id_shop_from = b.id_shop
                                                   and a.art = b.art
                                                   and a.asize = b.asize
                                                   and b.kol_rc > 0
                    where a.id_shop_from = r_shop.id_shop
                          and id = i_id
                    order by a.art, a.asize)
      loop
        if r_row.art = v_art and r_row.asize = v_asize then
          v_kol := v_kol + r_row.kol;
        else
          v_art := r_row.art;
          v_asize := r_row.asize;
          v_kol := r_row.kol;
        end if;
      
        if v_kol > r_row.kol_ost then
          select skladid
          into v_skladid
          from (select skladid
                 from tdv_map_rc_data
                 where id_shop = r_shop.id_shop
                       and art = r_row.art
                       and asize = r_row.asize
                       and kol2 > 0
                 order by skladid)
          where rownum = 1;
        
          update t_map_new_poexalo a
          set id_shop_from = v_skladid, owner = r_shop.id_shop
          where id = i_id
                and a.id_shop = r_row.id_shop
                and a.art = r_row.art
                and a.asize = r_row.asize
                and a.id_shop_from = r_shop.id_shop
                and a.boxty = r_row.boxty;
        
          update tdv_map_rc_data
          set kol2 = kol2 - 1
          where id_shop = r_shop.id_shop
                and art = r_row.art
                and asize = r_row.asize
                and skladid = v_skladid;
        end if;
      end loop;
    end loop;
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 14.11.2018 7:57:14
  -- Comments: после расчета определяю откуда должна уехать каждая пара
  --
  --****************************************************************************************************************************************************
  procedure fill_raspr_data(i_id      integer,
                            i_s_group integer) is
  
    vr_t_map_new_otbor T_MAP_NEW_OTBOR%rowtype;
    v_id integer;
    v_ost number;
    v_otbor number;
    v_flag number;
    v_count number;
  begin
  
    -- номер нового расчета
    v_id := map_fl.getNewCalculationID();
  
    insert into TDV_MAP_13_RESULT
    values
      (v_id, systimestamp, null, i_s_group, 0, null, 0, 'F', null, null, 1, systimestamp, null, 0, 'in progress', null,
       USERENV('sessionid'), 1, 'tdv_map_021');
    commit;
  
    writeLog('начинаем формировать таблицу остатков СГП ', 'tdv_map-015', i_id);
  
    execute immediate 'truncate table tdv_map_stock2';
    execute immediate 'truncate table T_MAP_NEW_OTBOR';
    execute immediate 'truncate table tdv_map_bad_sales';
  
    insert into tdv_map_stock2
      select distinct a.art, nvl(c.analog, a.art)
      from cena_skidki_all_shops a
      left join TDV_MAP_NEW_ANAL c on a.art = c.art
      where substr(to_char(cena), -2) = '99';
  
    -- перезаполняю таблицу остатков и продаж для распределения
    -- и затем выбираю по каждому магазину аналоги, которые плохо продавались
    fillBaseData2(i_s_group, to_date('20180201', 'yyyymmdd'), to_date('20181219', 'yyyymmdd'), null);
    commit;
  
    insert into tdv_map_bad_sales
      select id_shop, art -- только аналоги  , которые плохо продавались
      from TDV_MAP_NEW_OST_SALE a4
      group by id_shop, art
      having sum(a4.kol_sale) <= 2;
    -- 
  
    commit;
  
    map_fl.fillWarehouseStock();
    fill_rc_svx_data();
  
    insert into T_MAP_NEW_OTBOR
    
      select id_shop, art, season, sum(kol), asize, analog, status_flag
      from (
             
             -- россыпь склада
             select 'SGP' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
             from tem_mapif_fill_warehouse_stock a
             inner join s_art b on a.art = b.art
             left join TDV_MAP_NEW_ANAL c on a.art = c.art
             where b.art not like '%/О%'
                   and pv_season like '%,' || b.season || ',%'
             group by a.art, b.season, a.asize, nvl(c.analog, b.art)
             
             union all
             -- транзиты
             select 'SGP', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag
             from trans_in_way a1
             left join TDV_MAP_NEW_ANAL d on a1.art = d.art
             left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                               and a1.Postno_Lifex = e.id
             inner join s_art c on a1.art = c.art
             where scan != ' '
                   and pv_season like '%,' || c.season || ',%'
                   and a1.art not like '%/О%'
                          and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
             group by a1.art, c.season, asize, nvl(d.analog, a1.art)
             having sum(kol) > 0
             
             union all
             -- остатки магазинов
             select a2.id_shop, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
             from e_osttek_online a2
             inner join s_art c on a2.art = c.art
             left join pos_brak x on a2.scan = x.scan
                                     and a2.id_shop = x.id_shop
             left join TDV_MAP_NEW_ANAL d on d.art = a2.art
             left join st_shop e on a2.id_shop = e.shopid
             left join st_muya_art f on a2.art = f.art
             left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
                                              and nvl(d.analog, a2.art) = j.art
             where a2.scan != ' '
                   and a2.procent = 0
                   and a2.art not like '%/О%'
                   and a2.id_shop in (select shopid
                                      from tdm_map_new_shop_group
                                      where s_group = i_s_group)
                   and a2.id_shop in (select shopid
                                      from st_shop z
                                      where z.org_kod = 'SHP')
                   and pv_season like '%,' || c.season || ',%'
                   and x.scan is null
                   and 1 = case
                     when e.landid = 'RU' and f.art is not null then
                      0
                     else
                      1
                   end -- исключаем для РФ вывоз покупной
                   and j.art is not null -- только то, что плохо продавалось
                   and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40') --из сибири не забираем
             group by a2.id_shop, a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
             having sum(kol) > 0
             
             union all
             -- остатки РЦ и СВХ
             select a2.id_shop, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
             from tdv_map_rc_data a2
             inner join s_art c on a2.art = c.art
             left join TDV_MAP_NEW_ANAL d on d.art = a2.art
             left join st_shop e on a2.id_shop = e.shopid
             left join st_muya_art f on a2.art = f.art
             left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
                                              and nvl(d.analog, a2.art) = j.art
             where pv_season like '%,' || c.season || ',%'
                   and a2.art not like '%/О%'
                  -- and substr(skladid, 1, 1) != 'W'
                  --and substr(skladid, 3, 1) != 'S'
                   and j.art is not null -- только то, что плохо продавалось
                   and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40') --из сибири не забираем
                   and 1 = case
                     when e.landid = 'RU' and f.art is not null then
                      0
                     else
                      1
                   end -- исключаем для РФ вывоз покупной       
             group by a2.id_shop, a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
             having sum(a2.kol) > 0)
      
      group by id_shop, art, season, asize, analog, status_flag;
  
    writeLog('закончили формировать таблицу остатков СГП ', 'tdv_map-015', i_id);
  
    commit;
    --return;
  
    -- теперь определяем откуда мы должны забрать каждую из пар для распределения
    for r_row in (select art, asize, sum(kol) kol
                  from t_map_new_poexalo a
                  where a.id = i_id
                  group by art, asize)
    loop
      v_ost := r_row.kol;
      v_otbor := 0;
    
      while (v_ost != 0)
      loop
      
        v_flag := 0;
      
        begin
          select *
          into vr_t_map_new_otbor
          from (select a.*
                 
                 from T_MAP_NEW_OTBOR a
                 left join t_map_shop_priority c on a.id_shop = c.id_shop
                 left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                 where a.art = r_row.art
                       and a.asize = r_row.asize
                       and a.kol > 0
                 order by case
                            when a.id_shop = 'SGP' then
                             0
                            else
                             1
                          end, c.priority, b.reit2 desc)
          where rownum = 1;
        exception
          when no_data_found then
            v_flag := 1;
        end;
      
        if v_flag = 1 then
          exit;
        end if;
      
        v_otbor := least(v_ost, vr_t_map_new_otbor.kol);
      
        update T_MAP_NEW_OTBOR
        set kol = vr_t_map_new_otbor.kol - v_otbor
        where id_shop = vr_t_map_new_otbor.id_shop
              and art = vr_t_map_new_otbor.art
              and asize = vr_t_map_new_otbor.asize;
      
        insert into t_map_new_poexalo
          select '9999', vr_t_map_new_otbor.art, vr_t_map_new_otbor.season, v_otbor kol, vr_t_map_new_otbor.asize,
                 0 flag, vr_t_map_new_otbor.id_shop, v_id, systimestamp, 'yellow', 0 block_no, null owner, null text1,
                 null text2, null text3
          from dual;
      
        v_ost := v_ost - v_otbor;
      
      end loop;
    end loop;
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = v_id;
  
    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;
  
    commit;
  end;

  --********************************************************************************************
  --  сбор всего что нужно распределить
  -- 
  --********************************************************************************************
  procedure sum_all_rasp_box is
  begin
    --writeLog('начинаем формировать таблицу остатков СГП ', 'tdv_map-015', i_id);
  
    --execute immediate 'truncate table T_ALL_RASP'; -- здесь будет россыпь
  
    execute immediate 'truncate table t_all_rasp_box'; -- таблица с коробами
  
    -- записываю короба к распределению
    insert into t_all_rasp_box
      select d.art, d.boxty, sum(d.verme) / f.kol, 0 flag, e.season, nvl(g.analog, d.art) analog
      from st_sap_ob_ost2 d
      left join s_art e on d.art = e.art
      left join (select boxty, sum(kol) kol
                 from T_SAP_BOX_ROST -- таблица с ростовками по коробам
                 group by boxty) f on d.boxty = f.boxty
      left join TDV_MAP_NEW_ANAL g on d.art = g.art
      where d.boxty not in (' ', 'N')
      group by d.art, d.boxty, f.kol, e.season, nvl(g.analog, d.art);
  
    -- от коробов отнимаю, что в резерве САПА
    update t_all_rasp_box x1
    set x1.kol =
         (select x1.kol - x2.verme
          from (select art, boxty, sum(verme) verme
                 from tdv_map_new_sap_reserv2 x3
                 where x3.boxty != ' '
                       and trans != 'X'
                       and kunnr not in ('X_NERASP', 'X_SD_USE')
                 group by art, boxty) x2
          where x1.art = x2.art
                and x1.boxty = x2.boxty)
    where exists (select x1.kol - x2.verme
           from (select art, boxty, sum(verme) verme
                  from tdv_map_new_sap_reserv2 x3
                  where x3.boxty != ' '
                        and trans != 'X'
                        and kunnr not in ('X_NERASP', 'X_SD_USE')
                  group by art, boxty) x2
           where x1.art = x2.art
                 and x1.boxty = x2.boxty);
  
    delete from t_all_rasp_box x
    where x.kol <= 0;
  
    commit;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 18.12.2018 9:06:33
  -- Comments:
  -- t_map_shop_priority
  -- TDV_NEW_SHOP_OST
  -- tdv_map_osttek
  -- TDV_MAP_NEW_ANAL
  --****************************************************************************************************************************************************
  procedure shop_getout(i_id      number,
                        i_s_group number) is
    v_count number := 0;
    v_ost number;
  begin
    --fillBaseData(i_s_group, null, 'F');
  
    -- из остатков сразу вычитаю то, что в пути, т.к. это забирать мы не можем
    delete from tdv_map_osttek
    where (id_shop, art) in (select b.id_shop, nvl(b.art, c.art)
                             from TDV_NEW_SHOP_OST b
                             left join TDV_MAP_NEW_ANAL c on b.art = c.analog);
  
    commit;
  
    for r_row in (select art, asize, sum(kol) kol
                  from t_map_new_poexalo
                  where id = i_id
                  group by art, asize)
    loop
      v_ost := r_row.kol;
    
      for r_shop in (select distinct a.id_shop
                     from tdv_map_osttek a
                     left join t_map_shop_priority b on a.id_shop = b.id_shop
                     left join tdv_map_shop_reit c on a.id_shop = c.id_shop
                     where a.art = r_row.art
                           and a.asize = r_row.asize
                           and a.kol > 0)
      loop
      
        null;
      end loop;
    
    end loop;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 30.10.2018 21:17:05
  -- Comments: сбор всего что нужно распределить
  --
  --****************************************************************************************************************************************************
  procedure sum_all_rasp(i_id         integer,
                         i_s_group    integer,
                         i_siberia_in varchar2 default 'T') is
  begin
    writeLog('начинаем формировать таблицу остатков СГП ', 'tdv_map-015', i_id);
  
    execute immediate 'truncate table tdv_map_stock2';
    execute immediate 'truncate table T_MAP_NEW_OTBOR';
    execute immediate 'truncate table t_map_group_ryb_limit_work';
    execute immediate 'truncate table tdv_map_bad_sales';
    execute immediate 'truncate table e_osttek_online_short';
  
    insert into tdv_map_stock2
      select distinct a.art, nvl(b.analog, a.art)
      from cena_skidki_all_shops a
      left join TDV_MAP_NEW_ANAL b on a.art = b.art
      where substr(to_char(cena), -2) = '99';
  
    -- обнуляю лимиты по группам в рабочей таблице
--    insert into t_map_group_ryb_limit_work
--      select id_shop, asize, 0 limit, group_name
--      from t_map_group_ryb_limit;
  
    --перезаполняю таблицу остатков
    insert into e_osttek_online_short
      select a2.id_shop, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag,
             e.landid, a2.procent
      from e_osttek_online a2
      inner join s_art c on a2.art = c.art
      left join pos_brak x on a2.scan = x.scan
                              and a2.id_shop = x.id_shop
      --left join st_muya_art z on a2.art = z.art                        
      left join st_shop e on a2.id_shop = e.shopid
      left join TDV_MAP_NEW_ANAL d on d.art = a2.art
      -- фильтрация 
      where pv_season like '%,' || c.season || ',%'
            and a2.scan != ' '
            and a2.procent = 0
            and x.scan is null
            --and z.art is null
            and c.groupmw in ('Мужские', 'Женские')
            and a2.art not like '%/О%'
            and a2.id_shop in (select shopid
                               from tdm_map_new_shop_group
                               WHERE s_group = i_s_group)
            --and substr(a.id_shop, 1, 2) not in ('36', '38', '39', '40')
      group by a2.id_shop, a2.art, c.season, a2.asize, nvl(d.analog, a2.art), e.landid, a2.procent
      having sum(kol) > 0;
  
    commit;
  
    -- перезаполняю таблицу остатков и продаж для распределения
    -- и затем выбираю по каждому магазину аналоги, которые плохо продавались
/*    fillBaseData2(i_s_group, to_date('20180201', 'yyyymmdd'), to_date('20181110', 'yyyymmdd'), 643);
    commit;
  
    insert into tdv_map_bad_sales
      select id_shop, art -- только аналоги  , которые плохо продавались
      from TDV_MAP_NEW_OST_SALE a4
      group by id_shop, art
      having sum(a4.kol_sale) <= 1;*/
    -- 
  
    commit;
      
    map_fl.fillWarehouseStock();
    fill_rc_svx_data;
  
    insert into T_MAP_NEW_OTBOR
    
      select id_shop, art, season, sum(kol), asize, analog, status_flag
      from (
             
             -- россыпь склада
             select '9999' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
             from tem_mapif_fill_warehouse_stock a
             inner join s_art b on a.art = b.art
             left join TDV_MAP_NEW_ANAL c on a.art = c.art
             where b.art not like '%/О%'
                   and pv_season like '%,' || b.season || ',%'
             group by a.art, b.season, a.asize, nvl(c.analog, b.art)
             
             union all
             -- транзиты
             select '9999', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag
             from trans_in_way a1
             left join TDV_MAP_NEW_ANAL d on a1.art = d.art
             left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                               and a1.Postno_Lifex = e.id
             inner join s_art c on a1.art = c.art
             where scan != ' '
                   and pv_season like '%,' || c.season || ',%'
                   and a1.art not like '%/О%'
                   and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
             group by a1.art, c.season, asize, nvl(d.analog, a1.art)
             having sum(kol) > 0
             
--             union all
--             -- остатки магазинов
--             -- TODO закомментировать
--             select /*9999*/ a2.id_shop, a2.art, a2.season, sum(a2.kol) kol, a2.asize, a2.analog, 0 status_flag
--             from e_osttek_online_short a2
--             left join st_muya_art f on a2.art = f.art
----             left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
----                                              and a2.analog = j.art
--             WHERE 
----             1 = case when a2.landid = 'RU' and f.art is not null then 0 else 1 end -- исключаем для РФ вывоз покупной
----               and j.art is not null -- только то, что плохо продавалось
--               f.art is null
--               AND procent = 0
--               AND substr(a2.id_shop, 1, 2) NOT IN ('36', '38', '39', '40') --из сибири не забираем
--             group by a2.id_shop,a2.art, a2.season, a2.asize, a2.analog
--             having sum(kol) > 0
             
             union all
             -- остатки РЦ и СВХ
             select '9999', a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
             from tdv_map_rc_data a2
             inner join s_art c on a2.art = c.art
             left join TDV_MAP_NEW_ANAL d on d.art = a2.art
             where pv_season like '%,' || c.season || ',%'
                   and a2.art not like '%/О%'
                   and substr(skladid, 1, 1) != 'W' --склады
                   and substr(skladid, 3, 1) != 'S' -- санг
                   and case when i_siberia_in = 'F' then substr(id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
                   
             group by a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
             having sum(a2.kol) > 0)
      
      group by id_shop, art, season, asize, analog, status_flag;
  
    --writeLog('закончили формировать таблицу остатков СГП ', 'tdv_map-015', i_id);
  
    commit;
    
    -- очистка распределяемых от детской
    delete from T_MAP_NEW_OTBOR 
    where art in (
      select art from s_art where groupmw not in ('Мужские','Женские')
--      union all
--      select art from st_muya_art
    );
    commit;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 21.12.2018 20:28:28
  -- Comments: доброс на магазины того что осталось на складе до плоской ростовки
  --
  --****************************************************************************************************************************************************
  procedure last_count(i_s_group in varchar2) is
    v_id number;
    v_count integer;
    v_cent_size integer;
  
    v_count1 number := 6655;
    v_count2 number := 8274;
  begin
  
    -- номер нового расчета
    v_id := map_fl.getNewCalculationID();
  
    insert into TDV_MAP_13_RESULT
    values
      (v_id, systimestamp, null, i_s_group, 0, null, 0, 'F', null, null, 1, systimestamp, null, 0, 'in progress', null,
       USERENV('sessionid'), 1, 'tdv_map_021');
    commit;
  
    execute immediate 'truncate table T_MAP_NEW_OTBOR';
    execute immediate 'truncate table e_osttek_online_short';
  
    --перезаполняю таблицу остатков
    -- прибаляю к ней всё что распределили на магазины в расчете 6655
    insert into e_osttek_online_short
      select id_shop, art, season, sum(kol) kol, asize, analog, status_flag, landid, procent
      from (select a2.id_shop, a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag,
                    e.landid, a2.procent
             from e_osttek_online a2
             inner join s_art c on a2.art = c.art
             left join pos_brak x on a2.scan = x.scan
                                     and a2.id_shop = x.id_shop
             left join st_shop e on a2.id_shop = e.shopid
             left join TDV_MAP_NEW_ANAL d on d.art = a2.art
             where pv_season like '%,' || c.season || ',%'
                   and a2.scan != ' '
                   and a2.procent = 0
                   and a2.art not like '%/О%'
                   and a2.id_shop in (select shopid
                                      from tdm_map_new_shop_group
                                      where s_group = i_s_group)
             group by a2.id_shop, a2.art, c.season, a2.asize, nvl(d.analog, a2.art), e.landid, a2.procent
             having sum(kol) > 0
             
             union all
             
             select x.id_shop, x.art, c.season, sum(x.kol), x.asize, nvl(d.analog, x.art) analog, 0 status_flag, e.landid,
                    0
             from t_map_new_poexalo x
             left join s_art c on x.art = c.art
             left join TDV_MAP_NEW_ANAL d on d.art = x.art
             left join st_shop e on x.id_shop = e.shopid
             where id = v_count1
                   and pv_season like '%,' || c.season || ',%'
             group by x.id_shop, x.art, c.season, x.asize, nvl(d.analog, x.art), e.landid)
      
      group by id_shop, art, season, asize, analog, status_flag, landid, procent;
  
    --8274
  
    commit;
  
    map_fl.fillWarehouseStock();
  
    insert into T_MAP_NEW_OTBOR
    
      select id_shop, a.art, season, sum(a.kol) - b.kol, a.asize, analog, status_flag
      from (
             
             -- россыпь склада ГП
             select '9999' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
             from tem_mapif_fill_warehouse_stock a
             inner join s_art b on a.art = b.art
             left join TDV_MAP_NEW_ANAL c on a.art = c.art
             where b.art not like '%/О%'
                   and pv_season like '%,' || b.season || ',%'
             group by a.art, b.season, a.asize, nvl(c.analog, b.art)
             
             union all
             -- транзиты
             select '9999', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag
             from trans_in_way a1
             left join TDV_MAP_NEW_ANAL d on a1.art = d.art
             left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                               and a1.Postno_Lifex = e.id
             inner join s_art c on a1.art = c.art
             where scan != ' '
                   and pv_season like '%,' || c.season || ',%'
                   and a1.art not like '%/О%'
                   and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
             group by a1.art, c.season, asize, nvl(d.analog, a1.art)
             having sum(kol) > 0) a
      left join (select art, asize, sum(kol) kol
                 from t_map_new_poexalo
                 where id = v_count2
                       and id_shop_from = 'SGP'
                 group by art, asize) b on a.art = b.art
                                           and a.asize = b.asize
      
      group by id_shop, a.art, season, a.asize, analog, status_flag, b.kol;
  
    --writeLog('закончили формировать таблицу остатков СГП ', 'tdv_map-015', i_id);
  
    commit;
  
    for r_row in (select distinct analog, a.season, b.groupmw
                  from T_MAP_NEW_OTBOR a
                  left join s_art b on a.art = b.art
                  where kol > 0)
    loop
    
      for r_shop in (select distinct a.id_shop, reit2
                     from e_osttek_online_short a
                     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                     where analog = r_row.analog
                           and a.id_shop not in (select distinct id_shop_from
                                                 from t_map_new_poexalo a
                                                 left join TDV_MAP_NEW_ANAL d on a.art = d.art
                                                 where id = v_count2
                                                       and id_shop_from != 'SGP'
                                                       and nvl(d.analog, a.art) = r_row.analog) -- не везу в те магазины из которых был вывоз
                     order by reit2)
      loop
      
        -- по данному магазину и аналогу выбираем размеры , которые можем взять
        for r_size in (select distinct a.asize
                       from t_map_new_otbor a
                       left join (select distinct asize
                                 from e_osttek_online_short
                                 where analog = r_row.analog
                                       and id_shop = r_shop.id_shop
                                       and kol > 0) b on a.asize = b.asize
                       where a.analog = r_row.analog
                             and b.asize is null
                             and a.kol > 0)
        loop
        
          insert into t_map_new_poexalo
            select r_shop.id_shop, art, r_row.season, 1 kol, asize, 1 flag, '9999', v_id, systimestamp, 'blue' boxty,
                   0 block_no, null owner, r_row.analog text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   where analog = r_row.analog
                         and asize = r_size.asize
                         and kol > 0)
            where rownum = 1;
        
        end loop;
      
        -- проверка что бы на магазине в итоге образовалось 3 размера, учитывая  товары в пути
        select count(distinct a.asize), nvl(count(distinct b.asize), 0)
        into v_count, v_cent_size
        from (select distinct asize
               from e_osttek_online_short
               where id_shop = r_shop.id_shop
                     and analog = r_row.analog
                     and kol > 0
               union all
               select distinct asize
               from t_map_new_poexalo x
               where id_shop = r_shop.id_shop
                     and x.text1 = r_row.analog
                     and id = v_id
                     and kol > 0) a
        left join t_map12_asize_type b on b.groupmw = r_row.groupmw
                                          and b.asize = a.asize
                                          and b.stype = 'cent';
      
        if v_count >= 3 and v_cent_size >= 1 then
          update T_MAP_NEW_OTBOR a
          set kol = kol - 1
          where analog = r_row.analog
                and (art, asize) in (select art, asize
                                     from t_map_new_poexalo
                                     where flag = 1
                                           and id = v_id);
        
        else
          delete from t_map_new_poexalo
          where id = v_id
                and flag = 1;
        end if;
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = v_id;
      end loop;
    
    end loop;
  
    commit;
  
    -- 689
    -- фиолетовый блок, раздаем остаток магазинам в которых не было данной обуви в 2018-ом году
  
    -- собираю в одну таблицу все что было на остатках магазинов в 2018
    execute immediate 'truncate table tdv_test100';
    insert into tdv_test100
      select distinct shopid, nvl(d.analog, a.art) analog
      from e_oborot2 a
      left join TDV_MAP_NEW_ANAL d on a.art = d.art
      where id = 689
            and oste > 0
            and nvl(d.analog, a.art) in (select distinct analog from T_MAP_NEW_OTBOR where kol>0)
      union
      select distinct a.id_shop, nvl(d.analog, b.art) analog
      from d_prixod1 a
      inner join d_prixod2 b on a.id_shop = b.id_shop
                                and a.id = b.id
      left join TDV_MAP_NEW_ANAL d on b.art = d.art
      where a.bit_close = 'T'
            and to_char(dated, 'yyyy') >= 2018
      			and nvl(d.analog, b.art) in (select distinct analog from T_MAP_NEW_OTBOR where kol>0)
      union
      select distinct id_shop, analog
      from e_osttek_online_short
      where kol > 0
      and analog in (select distinct analog from T_MAP_NEW_OTBOR where kol>0);
    --tdv_test100
  
  	commit;
  
    for r_row in (select distinct analog, a.season, b.groupmw
                  from T_MAP_NEW_OTBOR a
                  left join s_art b on a.art = b.art
                  where kol > 0)
    loop
    
      for r_shop in (select distinct a.id_shop, reit2
                     from e_osttek_online_short a
                     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                     left join (select distinct id_shop, art
                               from tdv_test100
                               where art = r_row.analog) c on a.id_shop = c.id_shop    
                     where c.id_shop is null -- не было остатков данного аналога в 2018 году
                           and a.id_shop not in (select distinct id_shop_from
                                                 from t_map_new_poexalo a
                                                 left join TDV_MAP_NEW_ANAL d on a.art = d.art
                                                 where id = v_count2
                                                       and id_shop_from != 'SGP'
                                                       and nvl(d.analog, a.art) = r_row.analog) -- не везу в те магазины из которых был вывоз
                     order by reit2)
      loop
        -- по данному магазину и аналогу выбираем размеры , которые можем взять
        for r_size in (select distinct a.asize
                       from t_map_new_otbor a
                       where a.analog = r_row.analog
                             and a.kol > 0)
        loop
        
          insert into t_map_new_poexalo
            select r_shop.id_shop, art, r_row.season, 1 kol, asize, 1 flag, '9999', v_id, systimestamp, 'purple' boxty,
                   1 block_no, null owner, r_row.analog text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   where analog = r_row.analog
                         and asize = r_size.asize
                         and kol > 0)
            where rownum = 1;
        end loop;
        -- проверка что бы на магазине в итоге образовалось 3 размера, учитывая  товары в пути
        select count(distinct a.asize), nvl(count(distinct b.asize), 0)
        into v_count, v_cent_size
        from (select distinct asize
               from t_map_new_poexalo x
               where id_shop = r_shop.id_shop
                     and x.text1 = r_row.analog
                     and id = v_id
                     and kol > 0) a
        left join t_map12_asize_type b on b.groupmw = r_row.groupmw
                                          and b.asize = a.asize
                                          and b.stype = 'cent';
      
        if v_count >= 3 and v_cent_size >= 1 then
          update T_MAP_NEW_OTBOR a
          set kol = kol - 1
          where analog = r_row.analog
                and (art, asize) in (select art, asize
                                     from t_map_new_poexalo
                                     where flag = 1
                                           and id = v_id);
        
        else
          delete from t_map_new_poexalo
          where id = v_id
                and flag = 1;
        end if;
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = v_id;
      end loop;
    end loop;
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = v_id;
  
    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;
  
    commit;
  
  end;

  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 17.04.2019 10:00:00
  -- Comments: доброс на магазины того что осталось на складе до плоской ростовки
  --
  --****************************************************************************************************************************************************
  procedure last_count2(i_id          in integer,
                        i_s_group     in varchar2,
                        i_siberia_in  in varchar2,
                        i_block_no    in number default null) is
    v_id number;
    v_count integer;
    v_cent_size integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
  begin
--    if i_siberia_in = 'F' then
--      delete from TDV_MAP_NEW_OST_SALE
--      where substr(id_shop, 1, 2) in ('36', '38', '39', '40');
--    end if;
  
    --проход по артикулам остатков и сортировка по ассортименту
    for r_row in (select distinct analog, a.season, b.groupmw, z.order_number
                  from T_MAP_NEW_OTBOR a
                  left join s_art b on a.art = b.art
                  left join t_map_assort_order z on b.assort = z.assort
                  where kol > 0
                  order by z.order_number)
    loop
    
    
      for r_shop in (select distinct a.id_shop, reit2
                     from e_osttek_online_short a
                     inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                                and a.id_shop = x.shopid
                                                                and x.season_in like '%' || a.season || '%'
                     left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                     left join (select id_shop
                                   from tdv_map_without_stock s
                                   where s.only_purple is not null) d1 on a.id_shop = d1.id_shop
                     where analog = r_row.analog
                     and d1.id_shop is null
                     and case when i_siberia_in = 'F' then substr(a.id_shop, 1, 2) else ' ' end not in ('36', '38', '39', '40')
                     and case when substr(a.id_shop,1,2) = '00' then ' ' else r_row.analog end not like '%Р2%'
                     order by reit2)
      loop
        -- присвоение лимита к переменной
        select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_shop.id_shop;
        
        --проверка если лимит больше 0, то выполняем
        if v_shop_current_limit > 0 then
        
          -- по данному магазину и аналогу выбираем размеры , которые можем взять
          for r_size in (select distinct a.asize
                         from t_map_new_otbor a
                         left join (select distinct asize
                                   from e_osttek_online_short
                                   where analog = r_row.analog
                                         and id_shop = r_shop.id_shop
                                         and kol > 0) b on a.asize = b.asize
                         where a.analog = r_row.analog
                               and b.asize is null
                               and a.kol > 0)
          loop
          
            insert into t_map_new_poexalo
              select r_shop.id_shop, art, r_row.season, 1 kol, asize, 1 flag, '9999', v_id, systimestamp, 'blue' boxty,
                     case when i_block_no is null then 0 else i_block_no end block_no, null owner, r_row.analog text1, null text2, null text3
              from (select a.*
                     from T_MAP_NEW_OTBOR a
                     where analog = r_row.analog
                           and asize = r_size.asize
                           and kol > 0)
              where rownum = 1;
          
          end loop;
          
          -- запоминаем сколько пар добавляем магазину
          select sum(kol) into v_kol_add from t_map_new_poexalo 
          where id_shop = r_shop.id_shop and id = i_id and  flag = 1;
          if v_kol_add is null then 
            v_kol_add := 0; 
          --else
          --dbms_output.put_line('v_kol_add : ' || v_kol_add || ' ' || r_row.id_Shop || ' ' || r_row.art);
          end if;
        
          -- проверка что бы на магазине в итоге образовалось 3 размера, учитывая  товары в пути
          select count(distinct a.asize), nvl(count(distinct b.asize), 0)
          into v_count, v_cent_size
          from (select distinct asize
                 from e_osttek_online_short
                 where id_shop = r_shop.id_shop
                       and analog = r_row.analog
                       and kol > 0
                 union all
                 select distinct asize
                 from t_map_new_poexalo x
                 where id_shop = r_shop.id_shop
                       and x.text1 = r_row.analog
                       and id = v_id
                       and kol > 0) a
          left join t_map12_asize_type b on b.groupmw = r_row.groupmw
                                            and b.asize = a.asize
                                            and b.stype = 'cent';
        
          --if v_count >= 3 and v_cent_size >= 1 then
          --не менее 3 размеров, при этом 2 размера средних для женской и 1 средний для мужской
          if v_count >= 3 
          and ((r_row.groupmw = 'Мужские' and v_cent_size >= 1) or (r_row.groupmw = 'Женские' and v_cent_size >= 2)) 
          then
            update T_MAP_NEW_OTBOR a
            set kol = kol - 1
            where analog = r_row.analog
                  and (art, asize) in (select art, asize
                                       from t_map_new_poexalo
                                       where flag = 1
                                             and id = v_id);
            -- вычитаем от лимита добавленное
            v_shop_current_limit := v_shop_current_limit - v_kol_add;   
          
          else
            delete from t_map_new_poexalo
            where id = v_id
                  and flag = 1;
          end if;
        
          update t_map_new_poexalo
          set flag = 0
          where flag = 1
                and id = v_id;
                
          -- обновляем текущий лимит магазина
          update t_map_multi_raspred_shop_limit set current_limit = v_shop_current_limit 
          where id_shop = r_shop.id_shop;
        
        end if;
      end loop;
    
    end loop;
  
    commit;
  
    -- 689
    -- фиолетовый блок, раздаем остаток магазинам в которых не было данной обуви в 2018-ом году
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = v_id;
  
    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;
  
    commit;
  
  end;


  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 29.08.2018 15:40:33
  -- Comments: процедура расчитывает вывоз обуви из кластера, согласно ограничениям вместимости на РЦ
  --
  -- 1.делаем обычный расчет для одного этапа
  -- 2. потом вызываем fillBaseData , для второго этапа, учитываем уже остатки переброшенной обуви как остатки отделений
  -- 3. имея остаки и продажи отделения начинаем вывоз обуви, пока не достигнем лимита на кластере
  -- при этом учитываем ростовки. Если без ростовки  - вывозим в первую очередь + по продажам + по рейтингу
  -- если не достигли лимита , начинаем вывозить уже то, что в ростовке
  --****************************************************************************************************************************************************

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 10:05:11
  -- Comments: блок расчета распределения россыпи на магазина
  --
  --****************************************************************************************************************************************************
  procedure blue_count(i_id                  integer,
                       i_s_group             integer,
                       i_sales_start_date    date,
                       i_sales_end_date      date,
                       i_stock_doc_no        integer,
                       i_sale_pair_count     integer,
                       i_ost_pair_count      integer,
                       i_siberia_in          varchar2,
                       i_block_no            number default null,
                       i_sale_max_pair_count integer := 1000) is
  
    v_count integer;
    v_cent_size integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
  begin
  
    -- перезаполняю таблицу остатков и продаж для распределения
    fillBaseData2(i_s_group, i_sales_start_date, i_sales_end_date, i_stock_doc_no);
    commit;
  
    if i_siberia_in = 'F' then
      delete from TDV_MAP_NEW_OST_SALE
      where substr(id_shop, 1, 2) in ('36', '38', '39', '40');
    end if;
  
    -- беру магазины и артикула в которые нужно добросить обувь -- обувь, которая там хорошо продается -- по заданным условиям
    for r_row in (select x.*
                  from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, x.shop_in, sum(kol_sale) kol_sale,
                                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, x.count_limit_in,
                                D.release_year, D.kol kol_otbor, D.groupmw, d.order_number, C.group_name
                         from TDV_MAP_NEW_OST_SALE a -- тут не должно быть отфильтрованной обуви и сибири
                         inner join (select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
                                           y.groupmw, z.order_number
                                    from T_MAP_NEW_OTBOR x -- или тут не должно быть отвильтрованной обуви  и сибири
                                    left join s_art y on x.art = y.art
                                    left join t_map_assort_order z on y.assort = z.assort
                                    -- nvl2(i_group_name, i_group_name, z.group_name)
                                    where y.groupmw in ('Мужские', 'Женские')
                                    group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw, z.order_number) d on a.art =
                                                                                                                      d.analog
                         inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                                and a.id_shop = x.shopid
                                                                and x.season_in like '%' || a.season || '%'
                         left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                         left join (select distinct analog, group_name
                                   from t_map_group_ryb) c on a.art = c.analog
                         left join (select distinct analog
                                   from tdv_map_stock2) c1 on a.art = c1.analog
--                         left join (select id_shop
--                                   from tdv_map_without_stock s
--                                   where s.only_purple is not null) d1 on a.id_shop = d1.id_shop
                        left join (select id_shop
                                   from tdv_map_without_stock s
                                   where s.only_purple is null) d1 on a.id_shop = d1.id_shop
                         where kol_sale + kol_ost != 0
                                --and d1.id_shop is null
                                and a.id_shop != '0032'
                                and case when substr(a.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р2%'
                               and not (c1.analog is not null and d1.id_shop is not null)
                               and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032')
                         group by a.id_shop, a.art, a.season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, d.release_year, D.kol,
                                  d.groupmw, d.order_number, c.group_name
                         having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= i_sale_pair_count and sum(kol_sale) <= i_sale_max_pair_count and sum(kol_ost) <= i_ost_pair_count) x
                  -- Артикула распределяем начиная с наибольшего количества в остатках склада
                  order by  release_year desc, order_number, kol_otbor desc, kol_sale desc, reit2)
    loop
      -- присвоение лимита к переменной
      select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_row.id_shop;
      --проверка если лимит больше 0, то выполняем
      if v_shop_current_limit > 0 then
        -- беру размеры которые могу докинуть
        for r_size in (select distinct a.asize
                       from T_MAP_NEW_OTBOR a
                       left join (select asize, sum(kol) kol
                                 from t_map_new_poexalo x
                                 --left join TDV_MAP_NEW_ANAL y on x.art = y.art
                                 where id_shop = r_row.id_shop
                                      --and nvl(y.analog, x.art) = r_row.art
                                       and x.text1 = r_row.art
                                       and id = i_id
                                 group by asize) c on a.asize = c.asize
                       -- проверка на лимит по группе
  --                     left join (select asize, limit
  --                               from t_map_group_ryb_limit a
  --                               where a.id_shop = r_row.id_shop
  --                                     and group_name = r_row.group_name) d on a.asize = d.asize
  --                     left join (select asize, limit
  --                               from t_map_group_ryb_limit_work a
  --                               where a.id_shop = r_row.id_shop
  --                                     and group_name = r_row.group_name) e on a.asize = e.asize
                       where a.analog = r_row.art
                             and a.kol > 0
                             --and nvl(d.limit, 1000) > nvl(e.limit, 0)
                             and c.asize is null
                             and (r_row.id_shop, r_row.art, a.asize) not in
                             (select x.id_shop, x.art, x.asize
                                  from TDV_NEW_SHOP_OST x
                                  where x.verme > 0
                                        and x.art = r_row.art
                                        and id_shop = r_row.id_shop) -- которых нет в транзитах
                       order by a.asize)
        loop
        
          -- беру данные размеры из магазина с худшими продажами и рейтингом
          insert into t_map_new_poexalo
            select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, i_id, systimestamp, null boxty,
                   case when i_block_no is null then i_sale_pair_count else i_block_no end block_no, null owner, r_row.art text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   where analog = r_row.art
                         and asize = r_size.asize
                         and kol > 0)
            where rownum = 1;
        
        end loop;
        
        -- запоминаем сколько пар добавляем магазину
        select sum(kol) into v_kol_add from t_map_new_poexalo 
        where id_shop = r_row.id_shop and id = i_id and  flag = 1;
        if v_kol_add is null then 
          v_kol_add := 0; 
        --else
        --dbms_output.put_line('v_kol_add : ' || v_kol_add || ' ' || r_row.id_Shop || ' ' || r_row.art);
        end if;
      
        -- проверка что бы на магазине в итоге образовалось 3 размера, учитывая  товары в пути при этом 1(2) размер средний
        select count(distinct a.asize), nvl(count(distinct b.asize), 0)
        into v_count, v_cent_size
        from (select distinct asize
               from TDV_NEW_SHOP_OST
               where id_shop = r_row.id_shop
                     and art = r_row.art
                     and verme > 0
               union all
               select distinct asize
               from t_map_new_poexalo x
               --left join TDV_MAP_NEW_ANAL y on x.art = y.art
               where id_shop = r_row.id_shop
                     and x.text1 = r_row.art
                     and id = i_id
                     and kol > 0) a
        left join t_map12_asize_type b on b.groupmw = r_row.groupmw
                                          and b.asize = a.asize
                                          and b.stype = 'cent';
      
        --не менее 3 размеров, при этом 2 размера средних для женской и 1 средний для мужской
        if v_count >= 3 
        and ((r_row.groupmw = 'Мужские' and v_cent_size >= 1) or (r_row.groupmw = 'Женские' and v_cent_size >= 2)) 
        and v_shop_current_limit - v_kol_add >= 0 then
          update T_MAP_NEW_OTBOR a
          SET kol = kol - 1
          where /*id_shop = '9999'
                and*/ analog = r_row.art
                and (id_shop, art, asize) in (select id_shop_from, art, asize
                                              from t_map_new_poexalo
                                              where flag = 1
                                                    and id = i_id);
        
          -- добавляем к остаткам данной группы в магазин
  --        update t_map_group_ryb_limit_work
  --        set limit = limit + 1
  --        where id_shop = r_row.id_shop
  --              and group_name = r_row.group_name
  --              and asize in (select asize
  --                            from t_map_new_poexalo
  --                            where flag = 1
  --                                  and id = i_id);
          
          -- вычитаем от лимита добавленное
          v_shop_current_limit := v_shop_current_limit - v_kol_add;    
        
        else
          delete from t_map_new_poexalo
          where id = i_id
                and flag = 1;
        end if;
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = i_id;
       
       -- обновляем текущий лимит магазина
        update t_map_multi_raspred_shop_limit set current_limit = v_shop_current_limit 
        where id_shop = r_row.id_shop;       
              
      end if;  
    end loop;
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = i_id;
  
    /*    map_fl.blockEnd(i_id, 'Поехало ' || v_count);
    
    dbms_output.put_line('Поехало ' || v_count);*/
  
    select sum(kol)
    into v_count
    from T_MAP_NEW_OTBOR;
  
    dbms_output.put_line('Отбор после отдачи ' || v_count);
  
    writeLog('Отбор после отдачи ' || v_count, 'fill_ross3', i_id);
  
    commit;
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 22:12:37
  -- Comments: распределение обуви на магазины где обуви не было вообще
  --
  --****************************************************************************************************************************************************
  procedure purple_count(i_id         in integer,
                         i_s_group    in integer,
                         i_siberia_in varchar2,
                         i_block_no   number default null) is
    v_count integer;
    v_cent_size integer;
    
    v_shop_current_limit number := 0;
    v_kol_add number := 0;
  begin
    ------------------------------------------------------------------
    -- фиолетовый блок по коробам
    execute immediate 'truncate table tdv_test100';
  
    for r_shop in (select shopid
                   from tdm_map_new_shop_group x
                   where x.s_group = i_s_group)
    loop
    
      insert into tdv_test100
        select r_shop.shopid, x.analog
        from (select nvl(b.analog, a.art) analog, count(distinct asize) asize_count -- если на остатке в отборе есть артикул с 3 и более размерами
               from T_MAP_NEW_OTBOR a
               left join TDV_MAP_NEW_ANAL b on a.art = b.art
               where a.kol > 0
               
               /*                     and a.art not in (select art
               from tdv_map_stock2) -- исключаю стоки*/
               group by nvl(b.analog, a.art)
               having count(distinct asize) >= 3) x 
        left join (select distinct nvl(b.analog, a.art) art -- которых никогда не было на остатках в магазине
                   from e_osttek a
                   left join TDV_MAP_NEW_ANAL b on a.art = b.art
                   where id_shop = r_shop.shopid) y on x.analog = y.art
        left join (select distinct nvl(b.analog, a.art) analog -- которые еще не распределили
                   from t_map_new_poexalo a
                   left join TDV_MAP_NEW_ANAL b on a.art = b.art
                   where a.id = i_id
                         and a.id_shop = r_shop.shopid) z on x.analog = z.analog
        left join (select distinct x.id_shop, x.art
                   from TDV_NEW_SHOP_OST x
                   where x.verme > 0
                         and x.id_shop = r_shop.shopid) r on r.art = x.analog
        where y.art is null
              and z.analog is null
              and r.art is null
              and case when substr(r_shop.shopid,1,2) = '00' then ' ' else x.analog end not like '%Р2%';
    end loop;
  
    delete from tdv_test100
    where id_shop = '0032';
    
--    delete from tdv_test100
--    where id_shop = '0032'
--          and art not in (select analog
--                          from tdv_map_stock2);
--  
--    delete from tdv_test100
--    where id_shop in (select id_shop
--                      from tdv_map_without_stock)
--          and art in (select analog
--                      from tdv_map_stock2);
  
    if i_siberia_in = 'F' then
      delete from tdv_test100
      where substr(id_shop, 1, 2) in ('36', '38', '39', '40');
    end if;
  
    commit;
  
    -- доброс в магазины, в которых данной обуви не было вообще
    -- беру магазины и артикула и модели  которые нужно добросить
    for r_row in (select a.id_shop, a1.mod, q.art_cnt, a.art /*analog*/, max(season) season, x.shop_in, 0, x.count_limit_in, a1.groupmw, z.order_number
                  from tdv_test100 a
                  left join s_art a1 on a1.art = a.art
                  left join t_map_assort_order z on a1.assort = z.assort
                  inner join tdm_map_new_shop_group x on x.s_group = i_s_group
                                                         and a.id_shop = x.shopid
                                                         and x.season_in like '%' || a1.season || '%'
                  left join tdv_map_shop_reit b on a.id_shop = b.id_shop
                  left join (select distinct analog, group_name
                            from t_map_group_ryb) c on a.art = c.analog
                  left join (
                    select y1.mod, x1.art, sum(kol) art_cnt
                    from T_MAP_NEW_OTBOR x1
                    left join TDV_MAP_NEW_ANAL b on x1.art = b.art
                    left join s_art y1 on x1.art = y1.art
                    group by y1.mod, x1.art having sum(kol) > 0
                  ) q on a1.mod = q.mod and a.art = q.art          
                  where a1.groupmw in ('Мужские', 'Женские')
                  group by a.id_shop, a1.mod, a.art, q.art_cnt, season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, a1.groupmw, z.order_number
                  order by nvl(b.reit2, 0), z.order_number, a1.mod, q.art_cnt desc)
    loop
      -- присвоение лимита к переменной
      select current_limit into v_shop_current_limit from t_map_multi_raspred_shop_limit where id_shop = r_row.id_shop;
      --проверка если лимит больше 0, то выполняем
      if v_shop_current_limit > 0 then
      
        -- беру размеры которые могу докинуть
        for r_size in (select distinct a.asize
                       from T_MAP_NEW_OTBOR a
                       -- проверка на лимит по группе
  --                     left join (select asize, limit
  --                               from t_map_group_ryb_limit a
  --                               where a.id_shop = r_row.id_shop
  --                                     and group_name = r_row.group_name) d on a.asize = d.asize
  --                     left join (select asize, limit
  --                               from t_map_group_ryb_limit_work a
  --                               where a.id_shop = r_row.id_shop
  --                                     and group_name = r_row.group_name) e on a.asize = e.asize
                       where a.analog = r_row.art
                             and a.kol > 0
                             and (r_row.mod, r_row.art) not in (select c.mod, nvl(b.analog, a.art) -- которые не едут в магазин
                                                   from t_map_new_poexalo a
                                                   left join TDV_MAP_NEW_ANAL b on a.art = b.art
                                                   left join s_art c on a.art = c.art
                                                   where a.id = i_id
                                                         and id_shop = r_row.id_shop
                                                         and nvl(b.analog, a.art) = r_row.art
                                                         and c.mod = r_row.mod)
                             --and nvl(d.limit, 1000) > nvl(e.limit, 0)
                       order by a.asize)
        loop
        
--          if r_row.count_limit_in != 10000 then
--            begin
--              select count(distinct a.id_shop)
--              into v_count
--              from t_map_new_poexalo a
--              inner join tdm_map_new_shop_group x on x.s_group = i_s_group
--                                                     and a.id_shop = x.shopid
--              left join tdv_map_shop_stage_count y on a.id_shop = y.id_shop
--              where id = i_id
--                    and a.id_shop = r_row.id_shop
--              group by a.id_shop, x.count_limit_in, y.kol_in
--              having sum(kol) + nvl(y.kol_in, 0) >= nvl(x.count_limit_in, 0);
--            
--              if v_count = 1 then
--                exit;
--              end if;
--            exception
--              when no_data_found then
--                null;
--            end;
--          end if;
        
          -- беру данные размеры из магазина с худшими продажами и рейтингом
          insert into t_map_new_poexalo
            select r_row.id_shop, art, r_row.season, 1, asize, 1, id_shop, i_id, systimestamp, null boxty, 
            case when i_block_no is null then 6 else i_block_no end,
            null owner,
                   r_row.art text1, null text2, null text3
            from (select a.*
                   from T_MAP_NEW_OTBOR a
                   where analog = r_row.art
                         and asize = r_size.asize
                         and kol > 0
                         and (nvl(r_row.shop_in, ',' || a.id_shop || ',') like '%,' || a.id_shop || ',%'))
            where rownum = 1;
        
        end loop;
        
        -- запоминаем сколько пар добавляем магазину
        select sum(kol) into v_kol_add from t_map_new_poexalo 
        where id_shop = r_row.id_shop and id = i_id and  flag = 1;
        if v_kol_add is null then 
          v_kol_add := 0; 
        --else
        --dbms_output.put_line('v_kol_add : ' || v_kol_add || ' ' || r_row.id_Shop || ' ' || r_row.art);
        end if;
      
        -- проверяю , что собрали минимум три размера. Если нет, то отменяю действие
        select count(distinct a.asize), nvl(count(distinct b.asize), 0)
        into v_count, v_cent_size
        from t_map_new_poexalo a
        left join s_art c on a.art = c.art
        left join t_map12_asize_type b on b.groupmw = c.groupmw
                                          and b.asize = a.asize
                                          and b.stype = 'cent'
        where id = i_id
              and id_shop = r_row.id_shop
              and flag = 1;
      
        -- Проверка 2 женских и 1 мужского, вместительность
        if v_count >= 3 
        and ((r_row.groupmw = 'Мужские' and v_cent_size >= 1) or (r_row.groupmw = 'Женские' and v_cent_size >= 2)) 
        and v_shop_current_limit - v_kol_add >= 0 then
          update T_MAP_NEW_OTBOR a
          set kol = kol - 1
          where (id_shop, art, asize) in (select id_shop_from, art, asize
                                          from t_map_new_poexalo
                                          where flag = 1
                                                and id = i_id);
        
          -- добавляем к остаткам данной группы в магазин
  --        update t_map_group_ryb_limit_work
  --        set limit = limit + 1
  --        where id_shop = r_row.id_shop
  --              and group_name = r_row.group_name
  --              and asize in (select asize
  --                            from t_map_new_poexalo
  --                            where flag = 1
  --                                  and id = i_id);
         
          -- вычитаем от лимита добавленное
          v_shop_current_limit := v_shop_current_limit - v_kol_add; 
        
        else
          delete from t_map_new_poexalo
          where id = i_id
                and id_shop = r_row.id_shop
                and flag = 1;
        end if;
      
        update t_map_new_poexalo
        set flag = 0
        where flag = 1
              and id = i_id;
              
        -- обновляем текущий лимит магазина
        update t_map_multi_raspred_shop_limit set current_limit = v_shop_current_limit 
        where id_shop = r_row.id_shop;   
      
      end if;
    
    end loop;
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = i_id;
  
    dbms_output.put_line('Этап 4 Отбор после отдачи ' || v_count);
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 23:34:42
  -- Comments: тип расчета 1
  --
  --****************************************************************************************************************************************************
  procedure go_count1(i_id      in integer,
                      i_s_group in integer) is
  begin
    -- 1) продажи >= 5, по 1 марта 2019 
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190301', 'yyyymmdd'), 824, 5, 100, 'T');
  
    -- 2) продажи = 4 на 1 фев 2019, остаток <= 1 на 1 фев 2019
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 4, 1, 'T', 4);
  
    -- 3) продажи = 3 на 1 фев 2019, остаток = 0 на 1 фев 2019
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 3, 0, 'T', 3);
  
    -- 4) продажи >= 4 на сегодня
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190306', 'yyyymmdd'), null, 4, 100, 'T');
  
    -- 5) фиолетовые
    purple_count(i_id, i_s_group, 'T');
  
    -- 6) продажи >= 2 на сегодня
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190301', 'yyyymmdd'), null, 2, 100, 'T');
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 31.10.2018 23:34:42
  -- Comments: тип расчета 2
  --
  --****************************************************************************************************************************************************
  procedure go_count2(i_id      in integer,
                      i_s_group in integer) is
  begin
    for r_row in (select distinct group_name
                  from T_MAP_GROUP_RYB
                  where group_name is not null)
    loop
      -- 1) продажи >= 5, по 1 сен 2018
      blue_count(i_id, i_s_group, to_date('20180201', 'yyyymmdd'), to_date('20180901', 'yyyymmdd'), 624, 5, 100,
                 r_row.group_name);
    
      -- 2) продажи = 4 на 1 авг 2018, остаток <= 1 на 1 авг 2018
      blue_count(i_id, i_s_group, to_date('20180201', 'yyyymmdd'), to_date('20180801', 'yyyymmdd'), 623, 4, 1,
                 r_row.group_name);
    
      -- 3) продажи = 3 на 1 авг 2018, остаток = 0 на 1 авг 2018
      blue_count(i_id, i_s_group, to_date('20180201', 'yyyymmdd'), to_date('20180801', 'yyyymmdd'), 623, 3, 0,
                 r_row.group_name);
    
      -- 4) фиолетовые
      purple_count(i_id, i_s_group, r_row.group_name);
    end loop;
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 01.04.2019 10:00:00
  -- Comments: тип расчета 3 распределение зимы
  --
  --****************************************************************************************************************************************************
  procedure go_count3(i_id      in integer,
                      i_s_group in integer) is
  begin  
    -- 1) продажи >= 5, по 1 марта 2019 
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190301', 'yyyymmdd'), 824, 5, 100, 'T');
  
    -- 2) продажи = 4 на 1 фев 2019, остаток <= 1 на 1 фев 2019
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 4, 1, 'T', 4);
  
    -- 3) продажи = 3 на 1 фев 2019, остаток = 0 на 1 фев 2019
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 3, 0, 'T', 3);
  
    -- 4) продажи >= 4 на сегодня
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), sysdate, null, 4, 100, 'T');
    
  end;
  
  --****************************************************************************************************************************************************
  -- Author:  Bobrovich EP
  -- Created: 16.04.2019 13:00:00
  -- Comments: тип расчета 4 распределение зимы
  --
  --****************************************************************************************************************************************************
  procedure go_count4(i_id      in integer,
                      i_s_group in integer) is
  begin  
    -- 1) продажи >= 5, по 1 марта 2019 
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190301', 'yyyymmdd'), /*824*/ null, 5, 100, 'T', '1');
  
    -- 2) продажи = 4 на 1 фев 2019, остаток <= 1 на 1 фев 2019
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 4, 1, 'T', '2', 4);
  
    -- 3) продажи = 3 на 1 фев 2019, остаток = 0 на 1 фев 2019
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), to_date('20190802', 'yyyymmdd'), 822, 3, 0, 'T', '3', 3);
  
    -- 4) продажи >= 4 на сегодня
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), sysdate, null, 4, 100, 'T', '4');
    
    -- 5) фиолетовые (артикул, которого не было в данном магазине) не более 600 пар на магазины открытые после 1 января 2019
    purple_count(i_id, i_s_group, 'F', '5');
    
    -- 6) продажи >= 2 на сегодня
    blue_count(i_id, i_s_group, to_date('20180901', 'yyyymmdd'), sysdate, null, 2, 100, 'F', '6');
    
    -- 7) подсортировать до плоской ростовки остатки в магазинах
    last_count2(i_id, i_s_group, 'F', '7');
  end;

  --****************************************************************************************************************************************************
  -- Author:  Tyshevich DV
  -- Created: 01.01.2018 21:59:18
  -- Start Procedure:
  --
  --****************************************************************************************************************************************************
  procedure go(i_s_group             number default null,
               i_block_no_run        varchar2 default null, --  -- перечисление блоков, которые должны быть выполнены в программе. если нужны все блоки то NULL
               i_no_data_refresh     varchar2 default 'F', --если НЕ нужно  пересчитывать текущие остатки и продажи то 'T'
               i_rules_last_count_id varchar2 default null, -- если нужно делать расчет с учетом предыдущих
               i_color_id            integer default null,
               i_is_sgp              varchar2 default 'F', -- является ли данный расчет возвратом на СГП
               i_stage               varchar2 default null,
               i_ost_month_last      integer default 3, -- на сколько месяцев назад учитывать остатки
               i_is_sgp_rasp         varchar2 default 'F', -- если данный расчет распределяет остатки с СГП на магазины
               i_type_count          integer default 1, -- вид расчета, 1- прогоняем весь объем по магазинам, 2 - прогоняем в разрезе групп из t_map_group_ryb
               i_refresh_shop_limit  varchar2 default 'F' -- если нужно обнулить лимиты на ввоз в магазины 
               ) is
    v_count integer;
    v_sqlrow integer := 0;
    v_s_group integer;
  
    v_id integer;
    v_time timestamp;
  
    v_color varchar2(20);
  
    v_limit_proc number;
    v_limit_pr number;
    v_limit_rs number;
    v_art s_art.art%type;
    v_limit_shop s_shop.shopid%type;
    v_limit_ost number;
  
    v_limit_pr_over varchar2(1) := 'F';
    v_limit_rs_over varchar2(1) := 'F';
  
    v_razn integer default 30000;
  
    v_best_shop varchar2(10 char);
    v_best_shop_in varchar2(500 char);
  
    v_block_no_run varchar2(50 char);
    v_rules_last_count_id varchar2(3000 char);
  
    v_color_id tdv_map_cluster_prop.id_color_proc%type;
  
    v_list_city varchar(5000 char);
    v_list_filial varchar2(2000 char);
  
    v_ost_calculation_id e_oborot1.id%type;
    v_cluster_name tdm_map_new_shop_group.cluster_name%type;
  begin
  
    execute immediate 'truncate table t_map_multi_raspred_shop_limit'; -- таблица с текущими значениями лимита на распределение в магазин
  
    --t_map_group_ryb
    if i_s_group is not null then
      v_s_group := i_s_group;
    end if;
  
    if i_block_no_run is not null then
      v_block_no_run := ',' || i_block_no_run || ',';
    end if;
  
    if i_rules_last_count_id is not null then
      v_rules_last_count_id := replace(',' || i_rules_last_count_id || ',', ' ', '');
    end if;
  
    -- номер нового расчета
    v_id := map_fl.getNewCalculationID();
  
    dbms_output.put_line('номер расчета: ' || v_id);
  
    v_time := systimestamp;
  
    if i_color_id is null then
      begin
        select nvl(max(b.id_color_proc), 1)
        into v_color_id
        from tdm_map_new_shop_group a
        inner join TDV_MAP_CLUSTER_PROP b on trim(upper(a.cluster_name)) = trim(upper(b.cluster_name))
        where a.s_group = v_s_group;
      exception
        when no_data_found then
          v_color_id := 1;
      end;
    else
      v_color_id := i_color_id;
    end if;
  
    select max(a.cluster_name)
    into v_cluster_name
    from tdm_map_new_shop_group a
    where a.s_group = v_s_group;
  
    -- вставка новой записи в таблицу расчетов {
    for r_row in (select distinct a.cityname
                  from s_shop a
                  where a.shopid in (select shopid
                                     from tdm_map_new_shop_group
                                     where s_group = v_s_group))
    loop
      v_list_city := v_list_city || ', ' || r_row.cityname;
    end loop;
    v_list_city := substr(ltrim(v_list_city, ','), 1, least(1000, length(v_list_city)));
  
    for r_row in (select distinct substr(shopid, 1, 2) fil
                  from tdm_map_new_shop_group
                  where s_group = v_s_group)
    loop
      v_list_filial := v_list_filial || ', ' || r_row.fil;
    end loop;
    v_list_filial := ltrim(v_list_filial, ',');
  
    insert into TDV_MAP_13_RESULT
    values
      (v_id, v_time, null, v_s_group, v_block_no_run, v_rules_last_count_id, v_color_id, i_is_sgp, v_list_city,
       v_list_filial, 1, v_time, i_stage, 0, 'in progress', v_cluster_name, USERENV('sessionid'), 1, 'tdv_map_021');
    commit;
    -- }
  
    update tdm_map_new_shop_group a
    set a.shop_in = ',' || replace(rtrim(ltrim(replace(a.shop_in, ' ', ''), ','), ','), '70', '00') || ','
    where a.s_group = v_s_group
          and a.shop_in is not null;
  
    dbms_output.put_line(sysdate || ' ' || 'begin');
    if i_no_data_refresh = 'F' then
      --fillBaseData(v_s_group, v_rules_last_count_id, i_is_sgp, 1);
    
      -- собираю всё что можно для распределения
      sum_all_rasp(v_id, v_s_group, 'F');
    
      --sum_all_rasp_box;
    end if;
  
    /*    execute immediate 'truncate table t_all_rasp_box_work';
    insert into t_all_rasp_box_work
      select a.*
      from t_all_rasp_box a;*/
  
    dbms_output.put_line(sysdate || ' ' || 'summ_all_rasp complete');
    commit;
    --return;
    /*    -- заполнение остатков магазина на кол-во месяцев назад соотвествующее настройкам {
    execute immediate 'truncate table tdv_map_oborot';
    
    v_ost_calculation_id := getOstCalculationID(i_ost_month_last);
    
    insert into tdv_map_oborot
      select distinct z.shopid, z.art, an.analog
      from e_oborot2 z
      left join TDV_MAP_NEW_ANAL an on an.art = z.art
      inner join tdm_map_new_shop_group x on x.shopid = z.shopid
                                             and x.s_group = v_s_group
                                             and z.id = v_ost_calculation_id
      where z.oste > 0;
    --}*/
  
    --test(v_id, v_s_group, 1);
  
    commit;
    
    if i_refresh_shop_limit = 'T' then
    -- инициализация исходных лимитов
      insert into t_map_multi_raspred_shop_limit 
      select shopid, count_limit_in from tdm_map_new_shop_group where s_group = v_s_group;
      commit;
    end if;
  
    if i_type_count = 1 then
      go_count1(i_id => v_id, i_s_group => v_s_group);
    elsif i_type_count = 2 then
      go_count2(i_id => v_id, i_s_group => v_s_group);
    elsif i_type_count = 3 then
      pv_filter_plan_in_shop := 'T';
      go_count3(i_id => v_id, i_s_group => v_s_group);
    elsif i_type_count = 4 then
      --pv_filter_plan_in_shop := 'T';
      pv_assort_order := 'T';
      go_count4(i_id => v_id, i_s_group => v_s_group);
    end if;
  
    select sum(kol)
    into v_count
    from t_map_new_poexalo
    where id = v_id;
  
    update TDV_MAP_13_RESULT
    set DATE_END = systimestamp, COUNT_RESULT = 'complete', SUM_KOL = v_count
    where id = v_id;
    commit;
  
    commit;
  
  end;

end tdv_map_021;