select * from t_all_rasp_box_work;

select a.art, a.boxty, count(*) from t_all_rasp_box_work a
left join t_sap_box_rost f on a.boxty = f.boxty
left join s_art c on a.art = c.art
--where a.art = '1ROOF106'
group by a.art, a.boxty
;

select art, boxty, sum(kol) kol from t_all_rasp_box_work
group by art, boxty;

select x.analog, sum(x.kol) kol, 
replace(y.release_year, ' ', '2018') release_year, y.groupmw, y.line
from t_all_rasp_box_work x
left join s_art y on x.art = y.art
group by x.analog;

select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
       y.groupmw, y.line, z.order_number
from t_all_rasp_box_work x -- доступные короба
left join s_art y on x.art = y.art
left join t_map_assort_order z on y.assort = z.assort
where y.groupmw in ('Мужские', 'Женские')
group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw, y.line, z.order_number;


select * from (select distinct art, id_shop from tdv_map_new_ost_sale) a
inner join (select distinct analog from t_all_rasp_box_work) b on a.art = b.analog;

select x.*
from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, x.shop_in, sum(kol_sale) kol_sale,
              sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, x.count_limit_in,
              D.release_year, D.kol kol_otbor, D.groupmw, d.line, d.order_number, C.group_name, e1.min_year, e1.max_year
     from TDV_MAP_NEW_OST_SALE a -- тут не должно быть отфильтрованной обуви и сибири
     inner join (select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
                         y.groupmw, y.line, z.order_number
                  from t_all_rasp_box_work x -- доступные короба
                  left join s_art y on x.art = y.art
                  left join t_map_assort_order z on y.assort = z.assort
                  where y.groupmw in ('Мужские', 'Женские')
                  group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw, y.line, z.order_number) d on a.art =
                                                                                                  d.analog
     inner join tdm_map_new_shop_group x on x.s_group = 18009
                                              and a.id_shop = x.shopid
                                              and x.season_in like '%' || a.season || '%'
      left join tdv_map_shop_reit b on a.id_shop = b.id_shop
      left join (select distinct analog, group_name
                 from t_map_group_ryb) c on a.art = c.analog
      left join (select distinct analog
                 from tdv_map_stock2) c1 on a.art = c1.analog -- стоковые артикула
      left join (select id_shop
                 from tdv_map_without_stock s
                 where s.only_purple is null) d1 on a.id_shop = d1.id_shop -- магазины, кому не ввозится сток
      left join T_MAP_SHOP_RELEASE_YEAR_LIMIT e1 on a.id_shop = e1.id_shop -- магазины с ограничениями по годам выпуска артикулов         
      left join (select id_shop, text1 analog
                 from t_map_new_poexalo where id = 14574 
                 group by id_shop, text1 having sum(kol) > 0
                 ) z  on a.id_shop = z.id_shop and a.art = z.analog   -- магазины с распределенными ранее аналогами
      where kol_sale + kol_ost != 0
              and z.analog is null -- не было распределено до этого
              and case when substr(a.id_shop,1,2) = '00' then ' ' else a.art end not like '%Р%' -- для РФ фильтруется все артикула с Р
             and not (c1.analog is not null and d1.id_shop is not null) -- ограничение на ввоз стока
             and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032') -- если сток то только в 0032 магазин
             and (d.release_year >= nvl(e1.min_year, d.release_year) and d.release_year <= nvl(e1.max_year, d.release_year)) -- ограничение по году выпуска артикула
      group by a.id_shop, a.art, a.season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, d.release_year, d.kol,
              d.groupmw, d.line, d.order_number, c.group_name, e1.min_year, e1.max_year
      having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= 5 and sum(kol_sale) <= 1000 and sum(kol_ost) <= 100
      ) x
-- Артикула распределяем начиная с наибольшего количества в остатках склада
order by  /*release_year,*/ order_number, kol_otbor desc, kol_sale desc, reit2;

select boxty, count(*) from T_SAP_BOX_ROST group by boxty;

select art, a.boxty
from t_all_rasp_box_work a
left join T_SAP_BOX_ROST f on a.boxty = f.boxty
left join (select distinct asize
          from TDV_MAP_NEW_OST_SALE
          where id_shop = /*r_row.id_shop*/'0056'
                and art = /*r_row.art*/'1ROOF106'
                and kol_ost != 0) b on f.asize = b.asize
left join (select x.asize -- проверка на остатки в пути
          from TDV_NEW_SHOP_OST x
          where x.verme > 0
                and x.id_shop = /*r_row.id_shop*/'0056'
                and x.art = /*r_row.art*/'1ROOF106') c on f.asize = c.asize
left join (select distinct asize
          from t_map_new_poexalo x
          where id_shop = /*r_row.id_shop*/'0056'
                and x.text1 = /*r_row.art*/'1ROOF106'
                and id = /*i_id*/14574) d on f.asize = d.asize
where analog = /*r_row.art*/'1ROOF106'
      and a.kol > 0
      and case when substr(/*r_row.id_shop*/'0056',1,2) = '00' then ' ' else a.art end not like '%Р%'
group by art, a.boxty
having sum(case when b.asize is null and c.asize is null and d.asize is null and f.kol = 1 then 0  else 1 end) = 0
order by sum(a.kol) desc, boxty;