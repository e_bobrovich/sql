select block_no, sum(kol), max(timed)  from t_map_new_poexalo where id = 15177 group by block_no order by block_no;
select sum(kol) from t_map_new_poexalo where id = 15177;

select * from t_ob_logs where trunc(timed) = trunc(sysdate);

select * from d_planot;

select * from d_planot1 
where 
--kpodr = '0076' and 
text = 'Всесезонная' 
and trunc(date_s) >= trunc(sysdate)-30
and kpodr like '00%'
order by rel desc;

select * from d_planot3 where kpodr = '0076' and rel = 518;

select * from d_planot2 a;

select 
--distinct kpodr 
*
from d_planot2 a
where (a.kpodr, a.rel) in (select kpodr, rel from d_planot3 where calc_id = 15043 and kpodr like '00%')
and (a.kpodr, a.art, a.asize) in (select id_shop, art, asize from t_map_bep_rb_nekond)
;

select x1.kol - x2.kol
from (select id_shop, art, asize, sum(kol) kol 
      from t_map_bep_rb_nekond
      group by id_shop, art, asize) x2
where x1.kpodr = x2.id_shop and x1.art = x2.art and x1.asize = x2.asize
;

update d_planot2 x1
set x1.kol =
     (select x1.kol - x2.kol
      from (select id_shop, art, asize, sum(kol) kol 
            from t_map_bep_rb_nekond
            group by id_shop, art, asize) x2
      where x1.kpodr = x2.id_shop and x1.art = x2.art and x1.asize = x2.asize)
where 
(x1.kpodr, x1.rel) in (select kpodr, rel from d_planot3 where calc_id = 15043 and kpodr like '00%')
and (x1.kpodr, x1.art, x1.asize) in (select id_shop, art, asize from t_map_bep_rb_nekond)
and
exists (select x1.kol - x2.kol
       from (select id_shop, art, asize, sum(kol) kol 
              from t_map_bep_rb_nekond
              group by id_shop, art, asize) x2
       where x1.kpodr = x2.id_shop and x1.art = x2.art and x1.asize = x2.asize);


update d_planot2@s0076 x1
set x1.kol =
     (select x1.kol - x2.kol
      from (select id_shop, art, asize, sum(kol) kol 
            from t_map_bep_rb_nekond
            group by id_shop, art, asize) x2
      where '0076' = x2.id_shop and x1.art = x2.art and x1.asize = x2.asize)
where 
('0076', x1.rel) in (select kpodr, rel from d_planot3 where calc_id = 15043 and kpodr = '0076')
and ('0076', x1.art, x1.asize) in (select id_shop, art, asize from t_map_bep_rb_nekond)
and
exists (select x1.kol - x2.kol
       from (select id_shop, art, asize, sum(kol) kol 
              from t_map_bep_rb_nekond
              group by id_shop, art, asize) x2
       where '0076' = x2.id_shop and x1.art = x2.art and x1.asize = x2.asize);


select * from d_planot2 x1
where 
(x1.kpodr, x1.rel) in (select kpodr, rel from d_planot3 where calc_id = 15043 and kpodr like '00%')
and (x1.kpodr, x1.art, x1.asize) in (select id_shop, art, asize from t_map_bep_rb_nekond)
and
exists (select x1.kol - x2.kol
       from (select id_shop, art, asize, sum(kol) kol 
              from t_map_bep_rb_nekond
              group by id_shop, art, asize) x2
       where x1.kpodr = x2.id_shop and x1.art = x2.art and x1.asize = x2.asize)
;


insert into t_map_bep_rb_nekond
select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
       from e_osttek_online a2
       inner join s_art c on a2.art = c.art
       inner join st_shop d on a2.id_shop = d.shopid
       left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
       left join st_muya_art z on a2.art = z.art
       left join st_sale_block y on a2.art = y.art and case when d.landid = 'BY' then 2 when d.landid = 'RU' then 1 end = y.numconf  
       left join (
            select id_shop, art from cena_skidki_all_shops
            where substr(to_char(cena), -2) = '99'
       ) w on a2.id_shop = w.id_shop and a2.art = w.art
       
       left join t_map_arts_out u on a2.art = u.art
       
       where a2.scan != ' '
           and a2.id_shop like '00%'
           and c.season = 'Всесезонная'

        -- 10.09.2019 С. Бунто:
        -- покупную также можно вывозить. Т.е. исключаем некондиция, уценка, брак, детская, заблокированная

--        and case when 'T' = 'T' then a2.procent else 0 end = 0 -- исключение некондиции
--        and case when 'T' = 'T' then x.scan else null end is null -- исключение брака
        and a2.procent = 0
        and x.scan is not null
--        and case when i_filter_muya = 'T' then 
--                                            case when (z.art is not null) then 0 else 1 end
--                                            else 1 end = 1 -- исключение покупной
--        and case when i_filter_kids = 'T' then c.groupmw else 'Мужские' end in ('Мужские', 'Женские') -- исключение детской
--        and case when i_filter_block = 'T' then y.art else null end is null -- исключение заблокированной
--        and case when i_filter_stock = 'T' then w.art else null end is null -- исключение стоковой
--        and case when i_filter_buy = 'T' then 
--                                            case when (c.facture = 'Покупная') then 0 else 1 end
--                                            else 1 end = 1 -- исключение покупной -- исключение стоковой
        and case when 'T' = 'T' then u.art else ' ' end is not null --только из списка
       group by a2.id_shop, a2.art, a2.asize, c.season
       having sum(kol) > 0)
group by id_shop, art, asize, season;


