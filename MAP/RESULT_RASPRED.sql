select id_shop_from, count(*) from t_map_new_poexalo where id = 9154
group by id_shop_from
;

select id_shop, count(*) from t_map_new_poexalo where id = 9154
group by id_shop
;

select * from t_map_new_poexalo 
where 
id = 9156 
and 
substr(id_shop,1,2) in ('36', '38', '39', '40')
;

select * from T_MAP_NEW_OTBOR  where substr(id_shop,1,2) in ('36', '38', '39', '40');

select * from t_map_new_poexalo where id = 9640 and block_no = 6;

select 
  a.id_shop_from "ОТПРАВИТЕЛЬ", 
  b.shopname "НАЗВАНИЕ ОТПР", 
  a.id_shop "ПОЛУЧАТЕЛЬ", 
  с.shopname "НАЗВАНИЕ",
  a.art "АРТИКУЛ", 
  a.asize "РАЗМЕР", 
  a.kol "КОЛИЧЕСТВО",
  case when a.block_no = '6' then 'фиолетовые' else to_char(a.block_no) end "УСЛОВИЕ ПРОДАЖ"
from t_map_new_poexalo a
inner join s_shop b on a.id_shop_from = b.shopid
inner join s_shop с on a.id_shop = с.shopid
where a.id = 9655
order by a.id_shop_from, a.art, a.asize
;

select id_shop, art, asize, sum(kol) from (
  select id_shop, art, asize, scan, sum(kol) kol from e_osttek_online where id_shop = '2802' and art = '141000' group by id_shop, art, asize, scan
) group by id_shop, art, asize;
