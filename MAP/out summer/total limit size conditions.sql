
select a.id_shop, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2
        --,sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef
        --, max(d.groupmw) groupmw, max(d.style) style
from tdv_map_new_ost_sale a
--left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
--                 from s_art
--                 group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
where kol_sale + kol_ost2 != 0
group by a.id_shop, a.art, a.season
having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0;

select * from tdv_map_osttek;


select season, text2, sum(kol), max(timed), min(timed)  from t_map_new_poexalo where id = 20218 group by season, text2 order by season, cast(text2 as number);
select season, sum(kol), max(timed), min(timed)  from t_map_new_poexalo where id = 20218 group by season order by season;

select distinct  sum(kol_sale) kol_sale -- a.id_shop, a.art analog, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2
from tdv_map_new_ost_sale a  
where kol_sale + kol_ost2 != 0
group by a.id_shop, a.art, a.season
having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
order by kol_sale;

select sum(kol) from (
  select 
    '9999' id_shop, a.art art, a.season season, kol2 kol, a.asize asize, 0 flag, a.id_shop id_shop_from, 
    7889978 id, systimestamp, null boxty, 1 block_no, null owner, 
    nvl(b.analog, a.art) text1, 0 text2, 0 text3
  from tdv_map_osttek a
  left join tdv_map_new_anal b on a.art = b.art
  left join (
  ----------------
    select 
      x.*, y.all_size_count, y.cent_size_count, 
      y.all_size_count - y.cent_size_count not_cent_size,
      case when y.cent_size_count = 0 and y.all_size_count > 0 then 'F' else 'T' end with_cent_sizes
    from (
      select a.id_shop, a.art analog, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2
      from tdv_map_new_ost_sale a  
      where kol_sale + kol_ost2 != 0
      group by a.id_shop, a.art, a.season
      having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
    ) x
    left join (
      select a.id_Shop, a.analog, count(asize) all_size_count,
      nvl(sum(
          case when asize in (4, 4.5, 5, 5.5)    and b.groupmw = 'Женские' and b.line != 'Sport' then 1
               when asize in (8, 8.5, 9)         and b.groupmw = 'Мужские' and b.line != 'Sport' then 1
               when asize in (5, 5.5, 6, 6.5)    and b.groupmw = 'Женские' and b.line = 'Sport' then 1
               when asize in (8.5, 9, 9.5, 10)   and b.groupmw = 'Мужские' and b.line = 'Sport' then 1
               when asize in (37, 38, 39)        and b.groupmw = 'Женские' then 1
               when asize in (42, 43)            and b.groupmw = 'Мужские' then 1
               else 0 end), 0
      ) cent_size_count
      from (
          select id_shop, nvl(b.analog, a.art) analog, asize from tdv_map_osttek a
          left join tdv_map_new_anal b on a.art = b.art
          where a.kol2 > 0
      ) a
      inner join (
        select case when normt != ' ' then art else normt end analog, 
        max(groupmw) groupmw, max(line) line 
        from s_art group by case when normt != ' ' then art else normt end
      ) b on a.analog = b.analog
      group by a.id_shop, a.analog
    ) y on x.id_shop = y.id_shop and x.analog = y.analog    
    
    where kol_Sale = 0
    
    --------------------
  ) q on a.id_shop = q.id_shop and nvl(b.analog, a.art) = q.analog -- and q.kol_sale = 0
  
  where a.kol >  0
  and q.analog is not null
);




select a.id_Shop, a.analog, count(asize) all_size_count,
nvl(sum(
    case when asize in (4, 4.5, 5, 5.5)    and b.groupmw = 'Женские' and b.line != 'Sport' then 1
         when asize in (8, 8.5, 9)         and b.groupmw = 'Мужские' and b.line != 'Sport' then 1
         when asize in (5, 5.5, 6, 6.5)    and b.groupmw = 'Женские' and b.line = 'Sport' then 1
         when asize in (8.5, 9, 9.5, 10)   and b.groupmw = 'Мужские' and b.line = 'Sport' then 1
         when asize in (37, 38, 39)        and b.groupmw = 'Женские' then 1
         when asize in (42, 43)            and b.groupmw = 'Мужские' then 1
         else 0 end), 0
) cent_size_count
from (
    select id_shop, nvl(b.analog, a.art) analog, asize from tdv_map_osttek a
    left join tdv_map_new_anal b on a.art = b.art
    where a.kol2 > 0
) a
inner join (
  select case when normt != ' ' then art else normt end analog, 
  max(groupmw) groupmw, max(line) line 
  from s_art group by case when normt != ' ' then art else normt end
) b on a.analog = b.analog
group by a.id_shop, a.analog