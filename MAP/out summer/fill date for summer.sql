select * from tdv_map_13_result where s_group = 28902;
select * from t_map_new_poexalo where id = 12663 and art = '1916085' and id_shop_from = '3319';

select * from tdv_map_13_result where lower(package_name) like '%bep_map_002' order by id desc;

select * from t_map_new_poexalo where id in (12754,12734,12694,12689,12634,12614,12434) and id_shop = '3319' and art = '1916085';

select * from s_art where art = '1916085';

select * from tdv_map_osttek where art in (select art from st_sale_block);

select a.art, a.id_shop, case when b.landid = 'BY' then 2 when b.landid = 'RU' then 1 end numconf, c.*, b.* from tdv_map_osttek a 
inner join st_shop b on a.id_shop = b.shopid
left join st_sale_block c on a.art = c.art and case when b.landid = 'BY' then 2 when b.landid = 'RU' then 1 end = c.numconf
;

--truncate table tdv_map_osttek;
select * from tdv_map_osttek ;
--insert into tdv_map_osttek
select id_shop, art, asize, sum(kol) kol, season, 1, sum(kol_rc) kol_rc, sum(kol) kol
from (select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season, 0 out_block, 0 kol_rc
       from e_osttek_online a2
       inner join s_art c on a2.art = c.art
       inner join st_shop d on a2.id_shop = d.shopid
       left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
       left join st_muya_art z on a2.art = z.art
       left join st_sale_block y on a2.art = y.art and case when d.landid = 'BY' then 2 when d.landid = 'RU' then 1 end = y.numconf  
       left join (
        select id_shop, art from cena_skidki_all_shops
        where substr(to_char(cena), -2) = '99'
       ) w on a2.id_shop = w.id_shop and a2.art = w.art
       where a2.scan != ' '
            --and pv_season like '%' || c.season || '%' -- только нужный сезон
             and (a2.id_shop, c.season)  in (select shopid, season_out
                                from tdm_map_new_shop_group
                                where s_group = /*i_s_group*/29801)
             and case when /*i_filter_nonstandart*/'T' = 'T' then a2.procent else 0 end = 0 -- исключение некондиции
             and case when /*i_filter_brak*/'T' = 'T' then x.scan else null end is null -- исключение брака
             and case when /*i_filter_muya*/'T' = 'T' then z.art else null end is null -- исключение покупной
             and case when /*i_filter_kids*/'T' = 'T' then c.groupmw else 'Мужские' end in ('Мужские', 'Женские') -- исключение детской
             and case when /*i_filter_stock*/'T' = 'T' then y.art else null end is null -- исключение заблокированной
             and case when /*i_filter_block*/'T' = 'T' then w.art else null end is null -- исключение стоковой
             
       group by a2.id_shop, a2.art, a2.asize, c.season
       having sum(kol) > 0)
group by id_shop, art, asize, season;
    


-------------------------------------------------------------------------------
--truncate table tdv_map_new_ost_sale;
select distinct (select groupmw from s_art where a.art = art) from tdv_map_new_ost_sale a;
select * from tdv_map_new_ost_sale where kol_ost != kol_ost2;

--insert into tdv_map_new_ost_sale
select id_shop, a.art, a.asize, max(a.season), sum(kol_ost), sum(kol_sale), 0 out_block, sum(kol_rc) kol_rc,
     sum(kol2) kol2
from (
     -- текущие остатки
     select id_shop, a.art, asize, season, sum(kol) kol_ost, 0 kol_sale, sum(kol_rc) kol_rc, sum(kol2) kol2
     from (select a2.id_shop, nvl(a3.analog, a2.art) art, a2.asize, sum(a2.kol) kol, z.season, null,
                    sum(kol_rc) kol_rc, sum(a2.kol2) kol2
             from tdv_map_osttek a2
             left join TDV_MAP_NEW_ANAL a3 on a2.art = a3.art
             left join s_art z on a2.art = z.art
             group by a2.id_shop, nvl(a3.analog, a2.art), a2.asize, z.season
             having sum(kol) > 0) a
     group by id_shop, a.art, asize, season
     
     union all
     
     -- все продажи данной обуви за выбранный период
     select a.id_shop id_shop, nvl(d.analog, b.art) art, b.asize, c.season, 0, sum(b.kol) kol_sale, 0 kol_rc,
             0 kol2
     from pos_sale1 a
     inner join pos_sale2 b on a.id_chek = b.id_chek
                               and a.id_shop = b.id_shop
     inner join s_art c on b.art = c.art
     left join TDV_MAP_NEW_ANAL d on b.art = d.art
     where a.bit_close = 'T'
           and a.bit_vozvr = 'F'
           and b.scan != ' '
           and b.procent = 0
           and (a.id_shop,c.season) in (select shopid, season_out
                             from tdm_map_new_shop_group
                             where s_group = /*i_s_group*/29801)
           and trunc(a.sale_date) >= trunc(/*i_sales_start_date*/to_date('01.03.2019', 'dd.mm.yyyy'))
           and trunc(a.sale_date) <= trunc(/*i_sales_end_date*/sysdate)
           --and c.groupmw in ('Мужские', 'Женские')
     group by a.id_shop, nvl(d.analog, b.art), b.asize, c.season
     ) a

group by id_shop, a.art, a.asize;

--------------------------------------------------------------------------------
select art, normt, style, season  from s_art;

select s_group, shopid id_shop, dtype, season_out limit from tdm_map_new_shop_group x where x.s_group = /*i_s_group*/ 29801;


select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
            sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, max(d.groupmw) groupmw,
            max(d.style) style
from tdv_map_new_ost_sale a
--inner join (select distinct analog
--                    from T_MAP_NEW_OTBOR
--                    where kol > 0) e on a.art = e.analog -- проходимся только по тем артикулам, которые есть на остатках распределения
left join tdv_map_shop_reit b on a.id_shop = b.id_shop
left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                 from s_art
                 group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
where a.id_shop = '2628'
    and kol_sale + kol_ost2 != 0
group by a.id_shop, a.art, a.season, nvl(b.reit2, 0)
having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
;

--------------------------------------------------------

select * from pos_sale2 where id_shop = '2628' and art = '8938170';
select * from kart_v where shopid = '2628' and art = '8938612'; 

select '9999' id_shop, '8938170' art, 'Летняя' season, 1 kol, '40' asize, 0 flag, '2628' id_shop_from, 12321 id, systimestamp, null boxty, 1 block_no,
             null owner, '8938170' text1, 0 text2, 0 text3 --i_kol_sale text3
from dual;

select 
'9999' id_shop, a.art art, a.season season, kol2 kol, a.asize asize, 0 flag, a.id_shop id_shop, 
1111 id, systimestamp, null boxty, 1 block_no, null owner, 
nvl(b.analog, a.art) text1, 0 text2, 0 text3
from tdv_map_osttek a
left join tdv_map_new_anal b on a.art = b.art
where a.id_shop = '2628' and nvl(b.analog, a.art) = '8938170';