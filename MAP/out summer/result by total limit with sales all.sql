select 
  a.id_shop, case when regexp_replace(a.id_shop, '[^[:digit:]]', '') = a.id_shop then 'МАГАЗИН' else 'СКЛАД' end type,
  a.season, a.kol_ost, nvl(b.kol_otbor, 0) kol_otbor, nvl(c.kol_out, 0) kol_out 
from (
  select id_shop, season, sum(kol) kol_ost from (
    select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season
    from (
        select dcid id_Shop, art, asize, scan, procent, sum(kol) kol FROM DIST_CENTER.E_OSTTEK 
        where (substr(dcid, 3, 1) = 'W'
        or substr(dcid, 3, 1) = 'S' )
        group by dcid, art, asize, scan, procent having sum(kol) > 0
        union all
        select id_Shop, art, asize, scan, procent , sum(kol) kol FROM e_osttek_online 
        where substr(id_Shop, 3, 1) != 'W'
        and substr(id_Shop, 3, 1) != 'S' 
        group by id_Shop, art, asize, scan, procent having sum(kol) > 0
     )a2
     inner join s_art c on a2.art = c.art
     inner join (select shopid, season_out
                 from tdm_map_new_shop_group
                 where s_group in (72401,72501,72601) and dtype != 0) w on a2.id_shop = w.shopid 
                                        and c.season = case when w.season_out != ' ' then w.season_out else c.season end      
     group by a2.id_shop, a2.art, a2.asize, c.season
     having sum(kol) > 0
  ) group by id_shop, season
) a
left join (
  select id_shop, season, sum(kol) kol_otbor from T_MAP_NEW_OTBOR_before where id in (20214) group by id_Shop, season
) b on a.id_shop = b.id_shop and a.season = b.season
left join (
  select id_shop_from, season, sum(kol) kol_out from t_map_new_poexalo where id in (20214) group by id_shop_from, season
) c on a.id_shop = c.id_shop_from and a.season = c.season
order by type desc, a.id_shop, a.season;

--------------------------------------------------------------------------------
-- ОТЧЕТ С ПРОДАЖАМИ
select 
  a.id_shop, case when regexp_replace(a.id_shop, '[^[:digit:]]', '') = a.id_shop then 'МАГАЗИН' else 'СКЛАД' end type,
  a.season, a.kol_ost, nvl(b.kol_otbor, 0) kol_otbor, c.sales, nvl(c.kol_out, 0) kol_out 
from (
  select id_shop, season, sum(kol) kol_ost from (
    select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season
     from (
              select dcid id_Shop, art, asize, scan, procent, sum(kol) kol FROM DIST_CENTER.E_OSTTEK 
              where (substr(dcid, 3, 1) = 'W'
              or substr(dcid, 3, 1) = 'S' )
              group by dcid, art, asize, scan, procent having sum(kol) > 0
              union all
              select id_Shop, art, asize, scan, procent , sum(kol) kol FROM e_osttek_online 
              where substr(id_Shop, 3, 1) != 'W'
              and substr(id_Shop, 3, 1) != 'S' 
              group by id_Shop, art, asize, scan, procent having sum(kol) > 0
     )a2
     inner join s_art c on a2.art = c.art
     inner join (select shopid, season_out
                 from tdm_map_new_shop_group
                 where s_group in (72401,72501,72601) and dtype != 0) w on a2.id_shop = w.shopid 
                                        and c.season = case when w.season_out != ' ' then w.season_out else c.season end      
     group by a2.id_shop, a2.art, a2.asize, c.season
     having sum(kol) > 0
  ) group by id_shop, season
) a
left join (
  select id_shop, season, sum(kol) kol_otbor from T_MAP_NEW_OTBOR_before where id in (20213) group by id_Shop, season
) b on a.id_shop = b.id_shop and a.season = b.season
left join (
  select id_shop_from, season, text2 sales, sum(kol) kol_out 
  from t_map_new_poexalo 
  where id in (20213) 
  and (text2 = 0 or (text2 in ('1', '2') and season = 'Зимняя'))
  group by id_shop_from, season, text2
) c on a.id_shop = c.id_shop_from and a.season = c.season
order by type desc, a.id_shop, a.season, sales;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- ОТЧЕТ С ПРОДАЖАМИ И АНАЛОГАМИ(АРТИКУЛАМИ)
select 
  a.id_shop, case when regexp_replace(a.id_shop, '[^[:digit:]]', '') = a.id_shop then 'МАГАЗИН' else 'СКЛАД' end type,
  a.season, c.sales, a.analog, a.art, a.kol_ost, nvl(b.kol_otbor, 0) kol_otbor, nvl(c.kol_out, 0) kol_out 
from (
  select id_shop, season, analog, art, sum(kol) kol_ost from (
    select a2.id_shop, c.normt analog, a2.art, a2.asize, sum(a2.kol) kol, c.season
     from (
              select dcid id_Shop, art, asize, scan, procent, sum(kol) kol FROM DIST_CENTER.E_OSTTEK 
              where (substr(dcid, 3, 1) = 'W'
              or substr(dcid, 3, 1) = 'S' )
              group by dcid, art, asize, scan, procent having sum(kol) > 0
              union all
              select id_Shop, art, asize, scan, procent , sum(kol) kol FROM e_osttek_online 
              where substr(id_Shop, 3, 1) != 'W'
              and substr(id_Shop, 3, 1) != 'S' 
              group by id_Shop, art, asize, scan, procent having sum(kol) > 0
     )a2
     inner join s_art c on a2.art = c.art
     inner join (select shopid, season_out
                 from tdm_map_new_shop_group
                 where s_group in (72401,72501,72601) and dtype != 0) w on a2.id_shop = w.shopid 
                                        and c.season = case when w.season_out != ' ' then w.season_out else c.season end      
     group by a2.id_shop, c.normt, a2.art, a2.asize, c.season
     having sum(kol) > 0
  ) group by id_shop, season, analog, art
) a
left join (
  select id_shop, season, analog, art, sum(kol) kol_otbor 
  from T_MAP_NEW_OTBOR_before 
  where id in (20216) 
  group by id_Shop, season, analog, art
) b on a.id_shop = b.id_shop and a.season = b.season and a.art = b.art and a.analog = b.analog
left join (
  select id_shop_from, season, text2 sales, text1 analog, art, sum(kol) kol_out 
  from t_map_new_poexalo 
  where id in (20216) 
--  and (text2 = 0 or (text2 in ('1', '2') and season = 'Зимняя'))
  group by id_shop_from, season, art, text1, text2
) c on a.id_shop = c.id_shop_from and a.season = c.season and a.art = c.art and a.analog = c.analog
where sales is not null
order by type desc, a.id_shop, a.season, c.sales, a.analog, a.art;
--------------------------------------------------------------------------------

select 
  a.id_shop, case when regexp_replace(a.id_shop, '[^[:digit:]]', '') = a.id_shop then 'МАГАЗИН' else 'СКЛАД' end type,
  a.season, a.kol_ost, nvl(b.kol_otbor, 0) kol_otbor, nvl(c.kol_out, 0) kol_out ,
  p.kol_sale, p.kol_sale_all, p.kol_ost, p.kol_ost_all, p.sale_koe, p.sale_koef_all
from (
  select id_shop, season, sum(kol) kol_ost from (
    select a2.id_shop, a2.art, a2.asize, sum(a2.kol) kol, c.season
     from e_osttek_online a2
     inner join s_art c on a2.art = c.art
     inner join (select shopid, season_out
                 from tdm_map_new_shop_group
                 where s_group in (/*72401,*/72501/*,72601*/) and dtype != 0) w on a2.id_shop = w.shopid 
                                        and c.season = case when w.season_out != ' ' then w.season_out else c.season end      
     group by a2.id_shop, a2.art, a2.asize, c.season
     having sum(kol) > 0
  ) group by id_shop, season
) a
left join (
  select id_shop, season, sum(kol) kol_otbor from T_MAP_NEW_OTBOR_before where id in (20160) group by id_Shop, season
) b on a.id_shop = b.id_shop and a.season = b.season
left join (
  select id_shop_from, season, sum(kol) kol_out from t_map_new_poexalo where id in (20160) group by id_shop_from, season
) c on a.id_shop = c.id_shop_from and a.season = c.season
inner join (

  select x.*, y.kol_sale_all, y.kol_ost_all, y.sale_koef_all 
  from (
          select a.id_shop, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost,
                  sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef, max(d.groupmw) groupmw,
                  max(d.style) style
          from tdv_map_new_ost_sale a
  --        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
          left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                           from s_art
                           group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
          where kol_sale + kol_ost2 != 0
          group by a.id_shop, a.art, a.season
          having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
  ) x
  left join (
    select a.art, sum(kol_sale) kol_sale_all, sum(kol_ost2) kol_ost_all,
                    sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef_all 
    from tdv_map_new_ost_sale a 
    group by a.art
    having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
  ) y on x.art = y.art
--  order by 
--    x.kol_sale, y.kol_sale_all, x.kol_ost 

) p on a.id_shop = p.id_Shop and a.art = p.art

order by a.id_shop, a.season;

--------------------------------------------------------------------------------

select id_shop, season, sum(kol) kol_otbor from T_MAP_NEW_OTBOR_before where id in (20197) group by id_Shop, season;

select id_shop_from, text1 analog, asize from T_MAP_NEW_poexalo where id in (20197) group by id_shop_from, text1, asize;

-- СКОЛЬКО ВСЕГО ПОЕХАЛО
select sum(kol) from T_MAP_NEW_poexalo where id = 20198;

-- КОЛИЧЕСТВО РАЗМЕРОВ НА АРТИКУЛ ВЫВОЗИМЫХ
select size_count, sum(kol) kol, count(*) art_count  from (
  select id_shop_from, analog, count(*) size_count, sum(kol) kol from (
    select id_shop_from, text1 analog, asize , sum(kol) kol
    from T_MAP_NEW_poexalo 
    where id in (20197) 
    group by id_shop_from, text1, asize
  ) group by id_shop_from, analog   
  --having count(*) = 1
) group by size_count
order by size_count
;

-- СКОЛЬКО СТОКА ПОЕХАЛО
select sum(kol) from t_map_new_poexalo a
left join (
  select distinct b.shopid id_shop, art from cena_skidki_all_shops a
  inner join (
    select shopid , priceid, case when landid = 'BY' then landid else priceid end priceid_list, landid 
    from st_shop --where org_kod = 'SHP'
  ) b on b.priceid = a.vidid and b.landid = case when b.priceid = '01' then a.reg else b.landid end
  where substr(to_char(cena), -2) = '99'
) b on a.id_shop_from = b.id_shop and a.art = b.art
where a.id = 20197
and b.art is not null;


-------------------------------------------------------------------------------

select season, cast(text2 as number(18)) sales , sum(kol) from t_map_new_poexalo where  id in (20203) group by season, cast(text2 as number(18)) order by season, cast(text2 as number(18));

select season, sales, size_count, sum(kol) kol  from (
  select season, sales, id_shop_from, analog, count(*) size_count, sum(kol) kol from (
    select season, text2 sales, id_shop_from, text1 analog, asize , sum(kol) kol
    from T_MAP_NEW_poexalo 
    where id in (20203, 20204) 
    group by season, id_shop_from, text1, asize, text2
    
  ) group by season, sales, id_shop_from, analog   
  
) group by season, sales, size_count
order by season, sales, size_count
;


select season, sales, size_count, sum(kol) kol  from (
  select season, sales, id_shop_from, analog, count(*) size_count, sum(kol) kol from (
    select season, text2 sales, id_shop_from, text1 analog, asize , sum(kol) kol
    from T_MAP_NEW_poexalo 
    where id in (20203, 20204) and text3 = 'F'
    group by season, id_shop_from, text1, asize, text2

  ) group by season, sales, id_shop_from, analog   
  
) group by season, sales, size_count
order by season, sales, size_count
;

-- ПАР НА СЕЗОН ПРОДАЖУ РАЗМЕРОВ В ОСТАТКЕ + БЕЗ ЦЕНТРАЛЬНЫХ ПАР
select 
x.season season, 
x.sales sales,
x.size_count analog_size_kol,
x.kol pair_kol,
nvl(y.count_analog, 0) without_cent_analog_kol,
nvl(y.kol, 0) without_cent_analog_pair_kol
from (
  select season, sales, size_count, sum(kol) kol  from (
    select season, sales, id_shop_from, analog, count(*) size_count, sum(kol) kol from (
      select season, text2 sales, id_shop_from, text1 analog, asize , sum(kol) kol
      from T_MAP_NEW_poexalo 
      where id in (20214) 
      group by season, id_shop_from, text1, asize, text2
      
    ) group by season, sales, id_shop_from, analog   
    
  ) group by season, sales, size_count
  order by season, sales, size_count
) x
left join (
  select season, sales, size_count, count(analog) count_analog, sum(kol) kol  from (
    select season, sales, id_shop_from, analog, count(*) size_count, sum(kol) kol from (
      select season, text2 sales, id_shop_from, text1 analog, asize , sum(kol) kol
      from T_MAP_NEW_poexalo 
      where id in (20214) and text3 = 'F'
      group by season, id_shop_from, text1, asize, text2
    ) group by season, sales, id_shop_from, analog   
  ) group by season, sales, size_count
  order by season, sales, size_count
) y on x.season = y.season and x.sales = y.sales and x.size_count = y.size_count
order by x.season, cast(x.sales as number(18)), cast(x.size_count as number(18));


-- ПАР НА СЕЗОН ПРОДАЖУ РАЗМЕРОВ В ОСТАТКЕ + БЕЗ ЦЕНТРАЛЬНЫХ ПАР ПО МАГАЗИНАМ С 0 ПРОДАЖ
select 
x.season season, 
x.sales sales,
x.size_count analog_size_kol,
x.kol pair_kol,
nvl(y.count_analog, 0) without_cent_analog_kol,
nvl(y.kol, 0) without_cent_analog_pair_kol
from (
  select season, sales, size_count, sum(kol) kol  from (
    select season, sales, id_shop_from, analog, count(*) size_count, sum(kol) kol from (
      select season, text2 sales, id_shop_from, text1 analog, asize , sum(kol) kol
      from T_MAP_NEW_poexalo 
      where id in (20208) 
      group by season, id_shop_from, text1, asize, text2
      
    ) group by season, sales, id_shop_from, analog   
    
  ) group by season, id_shop_from, sales, size_count
  order by season, sales, size_count
) x
left join (
  select season, sales, size_count, count(analog) count_analog, sum(kol) kol  from (
    select season, sales, id_shop_from, analog, count(*) size_count, sum(kol) kol from (
      select season, text2 sales, id_shop_from, text1 analog, asize , sum(kol) kol
      from T_MAP_NEW_poexalo 
      where id in (20208) and text3 = 'F'
      group by season, id_shop_from, text1, asize, text2
    ) group by season, sales, id_shop_from, analog   
  ) group by season, sales, size_count
  order by season, sales, size_count
) y on x.season = y.season and x.sales = y.sales and x.size_count = y.size_count
order by x.season, cast(x.sales as number(18)), cast(x.size_count as number(18));



select season, sales, size_count
, count(analog) cnt_analog
from (
  select season, sales, id_shop_from, analog, count(*) size_count
  from (
    select season, text2 sales, id_shop_from, text1 analog, asize
    from T_MAP_NEW_poexalo 
    where id in (20203, 20204) and text3 = 'F'
--    and text1 = '147010/Б'
    group by season, id_shop_from, text1, asize, text2
  ) group by season, sales, id_shop_from, analog
) group by season, sales, size_count
order by season, sales, size_count
;

--------------------------------------------------------------------------------
select b.shopid, count(*) from cena_skidki_all_shops a
inner join (
  select shopid , priceid, case when landid = 'BY' then landid else priceid end priceid_list, landid 
  from st_shop-- where org_kod = 'SHP'
) b on b.priceid = a.vidid and b.landid = case when b.priceid = '01' then a.reg else b.landid end
where substr(to_char(cena), -2) = '99'
group by b.shopid order by b.shopid
;

select sum(kol) from t_map_new_poexalo a
left join (
  select distinct art from cena_skidki_all_shops a
  where substr(to_char(cena), -2) = '99'
) b on a.art = b.art
where a.id = 20198
and b.art is not null;


select distinct b.shopid id_shop, art from cena_skidki_all_shops a
inner join (
  select shopid , priceid, case when landid = 'BY' then landid else priceid end priceid_list, landid 
  from st_shop where org_kod = 'SHP'
) b on b.priceid = a.vidid and b.landid = case when b.priceid = '01' then a.reg else b.landid end
where substr(to_char(cena), -2) = '99';



select id_shop_from, season, sum(kol) kol_out from t_map_new_poexalo where id in (20150, 20154) group by id_shop_from, season;
select id_shop, season, sum(kol) kol_otbor from T_MAP_NEW_OTBOR_before where id in (20150, 20154) group by id_Shop, season;

select a2.id_shop, c.season, sum(kol) from e_osttek_online a2
inner join s_art c on a2.art = c.art
inner join (
  select shopid, season_out
  from tdm_map_new_shop_group
  where s_group in (72401,72501/*,72601*/) and dtype != 0
) w on a2.id_shop = w.shopid and c.season = case when w.season_out != ' ' then w.season_out else c.season end
group by a2.id_shop,c.season
having sum(kol) > 0
;

