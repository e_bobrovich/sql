select * from tdv_map_osttek;

select * from TDV_MAP_NEW_OST_SALE;


select * from tdv_map_osttek where id_shop = '2828' order by art, asize;
select * from TDV_MAP_NEW_OST_SALE where id_shop = '2828' order by art, asize;

select id_shop, art  from TDV_MAP_NEW_OST_SALE where id_shop = '2828' group by id_shop, art;



select 
rank() over (partition by x.id_shop 
order by
x.kol_ost2 - y.asize_cnt desc,
case when y.asize_cnt > 3 and x.sale_koef between 75 and 100 then 0
     when y.asize_cnt > 3 and x.sale_koef between 50 and 75 then 1
     when y.asize_cnt > 3 and x.sale_koef between 25 and 50 then 2
     when y.asize_cnt > 3 and x.sale_koef between 0 and 25 then 3
     else 4
     end,
 x.sale_koef desc, y.asize_cnt desc) rnk,
x.*, y.asize_cnt
,case when y.asize_cnt > 3 and x.sale_koef between 75 and 100 then 0
     when y.asize_cnt > 3 and x.sale_koef between 50 and 75 then 1
     when y.asize_cnt > 3 and x.sale_koef between 25 and 50 then 2
     when y.asize_cnt > 3 and x.sale_koef between 0 and 25 then 3
     else 4
     end ordr
from (
  select a.id_shop, a.art, max(d.season) season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
          sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef, max(d.groupmw) groupmw,
          max(d.line) line, max(d.assort) assort , max(d.release_year) release_year --, max(d.mat_upper) mat_upper
  from tdv_map_new_ost_sale a
  left join (select max(groupmw) groupmw, max(season) season, max(line) line, decode(normt, ' ', art, normt) normt, max(assort) assort ,max(release_year) release_year --, max(mat_upper) mat_upper
             from s_art
             group by decode(normt, ' ', art, normt)
  ) d on d.normt = a.art     
  where a.id_shop = '2828' 
      and kol_sale + kol_ost2 != 0
  group by a.id_shop, a.art
  having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
) x
left join (
  select a.id_shop, a.art, count(asize) asize_cnt from tdv_map_new_ost_sale a
  where a.id_shop = '2828'   and kol_ost > 0
  group by a.id_shop, a.art
) y on x.id_shop = y.id_shop and x.art = y.art
--order by 
--x.kol_ost2 - y.asize_cnt desc,
--case when y.asize_cnt > 3 and x.sale_koef between 75 and 100 then 0
--     when y.asize_cnt > 3 and x.sale_koef between 50 and 75 then 1
--     when y.asize_cnt > 3 and x.sale_koef between 25 and 50 then 2
--     when y.asize_cnt > 3 and x.sale_koef between 0 and 25 then 3
--     else 4
--     end,
-- x.sale_koef desc, y.asize_cnt desc
;

select id_Shop, art, rnk from (
  select 
  rank() over (partition by x.id_shop 
  order by
  x.kol_ost2 - y.asize_cnt desc,
  case when y.asize_cnt > 3 then 0 else 1 end,
   x.sale_koef desc, y.asize_cnt desc) rnk,
  x.*, y.asize_cnt
  from (
    select a.id_shop, a.art, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
            sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef
    from tdv_map_new_ost_sale a
    where kol_sale + kol_ost2 != 0
    group by a.id_shop, a.art
    having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
  ) x
  left join (
    select a.id_shop, a.art, count(asize) asize_cnt from tdv_map_new_ost_sale a
    where kol_ost > 0
    group by a.id_shop, a.art
  ) y on x.id_shop = y.id_shop and x.art = y.art
) where rnk <= 18
;


select * from tdv_map_osttek where kol > 0;

update tdv_map_osttek set kol = kol -1, kol2 = kol2 -1
where (id_shop, art) in (
  select id_Shop, art from (
    select 
    rank() over (partition by x.id_shop 
    order by
    x.kol_ost2 - y.asize_cnt desc,
    case when y.asize_cnt > 3 then 0 else 1 end,
     x.sale_koef desc, y.asize_cnt desc, x.art) rnk,
    x.*, y.asize_cnt
    from (
      select a.id_shop, a.art, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
              sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef
      from tdv_map_new_ost_sale a
      where kol_sale + kol_ost2 != 0
      group by a.id_shop, a.art
      having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
    ) x
    left join (
      select a.id_shop, a.art, count(asize) asize_cnt from tdv_map_new_ost_sale a
      where kol_ost > 0
      group by a.id_shop, a.art
    ) y on x.id_shop = y.id_shop and x.art = y.art
  ) where rnk <= 18
);
