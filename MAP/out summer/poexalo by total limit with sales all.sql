select season, sum(kol_ost2) from (
--select max(kol_sale) from (
select x.*, y.*
from (
        select a.id_shop, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef, max(d.groupmw) groupmw,
                max(d.style) style
        from tdv_map_new_ost_sale a
--        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
        left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                         from s_art
                         group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
        where kol_sale + kol_ost2 != 0
        group by a.id_shop, a.art, a.season
        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
        
--        and sum(kol_sale) = 0
) x
left join (
  select a.art, sum(kol_sale) kol_sale_all, sum(kol_ost2) kol_ost2_all,
                  sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef_all 
  from tdv_map_new_ost_sale a 
  group by a.art
  having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
  
--  
  
) y on x.art = y.art
--where x.kol_sale = 0
order by 
  x.kol_sale, x.kol_ost2, y.kol_sale_all
)  group by season
  ;


select kol_sale, sum(kol_ost2) from (
select x.*, y.*, z.*
from (
        select a.id_shop, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef--, max(d.groupmw) groupmw,
                --max(d.style) style
        from tdv_map_new_ost_sale a
--        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
        left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                         from s_art
                         group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
        where kol_sale + kol_ost2 != 0
        group by a.id_shop, a.art, a.season
        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
) x
left join (
  select a.art, sum(kol_sale) kol_sale_all, sum(kol_ost2) kol_ost2_all,
                  sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef_all 
  from tdv_map_new_ost_sale a 
  group by a.art
  having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
) y on x.art = y.art

inner join (select distinct id_shop_from, text1 analog from t_map_new_poexalo where id = 20164) z on x.id_Shop = z.id_shop_from and x.art = z.analog
--where x.kol_sale = 0
order by 
  x.kol_sale, x.kol_ost2, y.kol_sale_all
)  group by kol_sale
  ;



select * from tdv_map_new_ost_sale;

select a.art analog, sum(kol_sale) kol_sale_all, sum(kol_ost2) kol_ost2_all,
                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef_all 
from tdv_map_new_ost_sale a 
group by a.art
having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
;

select * from pos_Sale1 a 
inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek 
where art = '308015/О';




--insert into t_map_new_poexalo
select 
'9999' id_shop, a.art art, a.season season, kol2 kol, a.asize asize, 0 flag, a.id_shop id_shop, 
77777777 id, systimestamp, null boxty, 1 block_no, null owner, 
nvl(b.analog, a.art) text1, 0 text2, 0 text3
from tdv_map_osttek a
left join tdv_map_new_anal b on a.art = b.art
where a.kol > 0;