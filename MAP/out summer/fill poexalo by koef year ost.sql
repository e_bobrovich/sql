select * from tdv_map_osttek;
select * from s_art;
select x.*, case when x.release_year <= 2018 then 0 else 1 end is_2019, case when kol_ost > 1 then 0 else 1 end  is_single,
case when x.art like '19%' then 1 else 0 end is_19,
case when x.art not like '19%' then 1 else 0 end is_not_19
from (
        select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost,
                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef, max(d.groupmw) groupmw, 
                max(d.line) line, max(d.release_year) release_year, max(d.facture) facture
        from tdv_map_new_ost_sale a
        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
        left join (select max(groupmw) groupmw, max(season) season, max(line) line, max(release_year) release_year, max(facture) facture, decode(normt, ' ', art, normt) normt
                         from s_art
                         group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
        where a.id_shop = /*r_shop.id_shop*/ '3454'
            and kol_sale + kol_ost2 != 0
        group by a.id_shop, a.art, a.season, nvl(b.reit2, 0)
        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
        and max(d.release_year) <= 2018 or (max(d.release_year) = 2019 and  sum(kol_ost2) = 1) -- до 2018 года все, 2019 год только одиночные пары
--        and (a.art not like '19%' /*or (a.art like '19%' and sum(kol_ost2) <3)*/)
--        and (max(d.facture) != 'Покупная' or (max(d.facture) = 'Покупная' and sum(kol_ost2) <3))
) x
where (x.art not like '19%' or (x.art like '19%' and kol_ost <3))
  and (x.facture != 'Покупная' or (x.facture = 'Покупная' and kol_ost <3))
order by 
  case when x.release_year <= 2018 then 0 else 1 end,
  case when kol_ost > 1 then 0 else 1 end,
  sale_koef, kol_sale, kol_ost;
  
  
select 
  x.*, 
  case when x.release_year <= 2018 then 0 else 1 end is_2019, 
  case when kol_ost > 1 then 0 else 1 end  is_single,
  y.kol_out
from (
        select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost,
                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_koef, max(d.groupmw) groupmw,
                max(d.line) line, max(d.release_year) release_year, max(facture) facture
        from tdv_map_new_ost_sale a
        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
        left join (select max(groupmw) groupmw, max(season) season, max(line) line, max(release_year) release_year, max(facture) facture, decode(normt, ' ', art, normt) normt
                         from s_art
                         group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
        --where a.id_shop = /*r_shop.id_shop*/ '3604'
            and kol_sale + kol_ost2 != 0
        group by a.id_shop, a.art, a.season, nvl(b.reit2, 0)
        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
        and max(d.release_year) <= 2018 or (max(d.release_year) = 2019 and  sum(kol_ost2) = 1) -- до 2018 года все, 2019 год только одиночные пары
) x
left join (
  select id_shop_from, text1 analog, sum(kol) kol_out from t_map_new_Poexalo where id = 20615 group by id_shop_from, text1
) y on x.id_shop = y.id_shop_from and x.art = y.analog
where (x.art not like '19%' or (x.art like '19%' and kol_ost <3))
  and (x.facture != 'Покупная' or (x.facture = 'Покупная' and kol_ost <3))
order by 
  x.id_shop,
  case when x.release_year <= 2018 then 0 else 1 end,
  case when kol_ost > 1 then 0 else 1 end,
  sale_koef, kol_sale, kol_ost;
