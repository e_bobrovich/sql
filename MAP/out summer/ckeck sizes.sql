select * from pos_sale2 where id_shop = '3710' and art = '229002/О';

select * from t_map_new_poexalo where id = 20137;
select * from tdv_map_osttek;
select * from tdv_map_new_ost_sale;
select art, normt from s_art where art = '1810027'; --1730120

select distinct asize from tdv_map_osttek a
left join tdv_map_new_anal b on a.art = b.art
where a.id_shop = '3710' and nvl(b.analog, a.art) = '1835225' and a.kol2 > 0
;


select x.*, y.*, case when y.all_size_count >= 3 and y.cent_size_count >= 2 then 1 else 0 end condition
from (
        select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, sum(kol_sale) kol_sale, sum(kol_ost2) kol_ost2,
                sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, max(d.groupmw) groupmw,
                max(d.style) style
        from tdv_map_new_ost_sale a
        --inner join (select distinct analog
        --                    from T_MAP_NEW_OTBOR
        --                    where kol > 0) e on a.art = e.analog -- проходимся только по тем артикулам, которые есть на остатках распределения
        left join tdv_map_shop_reit b on a.id_shop = b.id_shop
        left join (select max(groupmw) groupmw, max(season) season, max(style) style, decode(normt, ' ', art, normt) normt
                         from s_art
                         group by decode(normt, ' ', art, normt)) d on d.normt = a.art     
        where a.id_shop = '3710' 
            and kol_sale + kol_ost2 != 0
        group by a.id_shop, a.art, a.season, nvl(b.reit2, 0)
        having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_ost) != 0
                -- ТУТ УБИРАТЬ ПРИ СЛУЧАЕ
                and (('F' = 'T' and (sum(kol_ost) <= 4 or sum(kol_sale) = 0)) or 'F' = 'F') -- не вывозить 4 на остатке, если не было продаж
) x
left join (
  select a.id_Shop, a.analog, count(asize) all_size_count,
  nvl(sum(
      case when asize in (4, 4.5, 5, 5.5)    and b.groupmw = 'Женские' and b.line != 'Sport' then 1
           when asize in (8, 8.5, 9)         and b.groupmw = 'Мужские' and b.line != 'Sport' then 1
           when asize in (5, 5.5, 6, 6.5)    and b.groupmw = 'Женские' and b.line = 'Sport' then 1
           when asize in (8.5, 9, 9.5, 10)   and b.groupmw = 'Мужские' and b.line = 'Sport' then 1
           when asize in (37, 38, 39)        and b.groupmw = 'Женские' then 1
           when asize in (42, 43)            and b.groupmw = 'Мужские' then 1
           else 0 end), 0
  ) cent_size_count
  --into v_all_size_count, v_cent_size_count
  from (
      select id_shop, nvl(b.analog, a.art) analog, asize from tdv_map_osttek a
      left join tdv_map_new_anal b on a.art = b.art
      where a.id_shop = '3710' and a.kol2 > 0
      union
      select id_shop_from id_shop, text1 analog, asize from t_map_new_poexalo where id = 20145 and id_shop_from = '3710' and kol > 0
  ) a
  inner join (
    select case when normt != ' ' then art else normt end analog, 
    max(groupmw) groupmw, max(line) line 
    from s_art group by case when normt != ' ' then art else normt end
  ) b on a.analog = b.analog
  group by a.id_shop, a.analog
) y on x.id_shop = y.id_shop and x.art = y.analog
order by 
case when y.all_size_count >= 3 and y.cent_size_count >= 2 then 1 else 0 end, 
sale_kol, all_size_count, kol_sale, kol_ost2;


select * from tdv_map_osttek;

select sum(kol2) from tdv_map_osttek;

select a.analog, count(asize) all_size_count,
nvl(sum(
    case when asize in (4, 4.5, 5, 5.5)    and b.groupmw = 'Женские' and b.line != 'Sport' then 1
         when asize in (8, 8.5, 9)         and b.groupmw = 'Мужские' and b.line != 'Sport' then 1
         when asize in (5, 5.5, 6, 6.5)    and b.groupmw = 'Женские' and b.line = 'Sport' then 1
         when asize in (8.5, 9, 9.5, 10)   and b.groupmw = 'Мужские' and b.line = 'Sport' then 1
         when asize in (37, 38, 39)        and b.groupmw = 'Женские' then 1
         when asize in (42, 43)            and b.groupmw = 'Мужские' then 1
         else 0 end), 0
) cent_size_count
--into v_all_size_count, v_cent_size_count
from (
    select nvl(b.analog, a.art) analog, asize from tdv_map_osttek a
    left join tdv_map_new_anal b on a.art = b.art
    where a.id_shop = '3710' and a.kol2 > 0
    union
    select text1 analog, asize from t_map_new_poexalo where id = 20145 and id_shop_from = '3710' and kol > 0
) a
inner join (
  select case when normt != ' ' then art else normt end analog, 
  max(groupmw) groupmw, max(line) line 
  from s_art group by case when normt != ' ' then art else normt end
) b on a.analog = b.analog
group by a.analog
;

select art, normt from s_art where art = '2032150/1';