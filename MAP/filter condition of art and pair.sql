/*
1) Некондиция
2) Брак
3) Детская
4) Артикула заблокированные (покупные)
5) Сток

брак - ШК лежит в таблице pos_brak
некондиция - в таблице s_label поле procent > 0
сток - во вьювере cena_skidki_all_shops поле final_price кончается на 99

*/

select * from t_map_new_poexalo where id = 8874;

select * from s_art;

select groupmw from s_art group by groupmw;
select * from s_art where groupmw = ' ';
select * from s_art where mtart = 'ZHW3';

select * from e_osttek_online where procent != 0;

select * from pos_brak;
select * from s_label;
select * from cena_skidki_all_shops where substr(final_price,-2) = '99';

select a.art, a.manufactor from s_art a
right join st_muya_art b on a.art = b.art; 

--delete from t_map_new_poexalo
select * from t_map_new_poexalo
where id = '8874' and (id_shop, art) in (
  select a2.id_shop, a2.art from t_map_new_poexalo a2
  inner join s_art c on a2.art = c.art
  --left join st_muya_art z on a2.art = z.art
  --left join cena_skidki_all_shops y on a2.art = y.art
  where case when 'T' = 'T' then c.groupmw else 'Мужские' end not in ('Мужские', 'Женские') -- исключение детской обуви
    --and case when 'T' = 'T' then substr(to_char(y.cena), -2) else ' ' end = '99'        -- исключение стоковой
    --and case when 'T' = 'T' then z.art else null end is not null                            -- исключение покупной
)
;

-- исключение детской обуви
select * from t_map_new_poexalo a2
inner join s_art c on a2.art = c.art
where a2.id = 8874 and c.groupmw not in ('Мужские', 'Женские')
and case when 'F' = 'T' then substr(a2.id_shop, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири или остальным  ---3618
and case when 'F' = 'F' then substr(a2.id_shop, 1, 2) else '00' end not in ('36', '38', '39', '40') -- по магазинам сибири или остальным
;

--delete from t_map_new_poexalo 
select * from t_map_new_poexalo
where id = 8874 and (art) in (
  select c.art from s_art c
  where c.groupmw not in ('Мужские', 'Женские')
)
and case when 'F' = 'T' then substr(id_shop_from, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири 
and case when 'F' = 'F' then substr(id_shop_from, 1, 2) else '00' end not in ('36', '38', '39', '40'); -- по магазинам остальным


 -- исключение покупной
select * from t_map_new_poexalo a2
left join st_muya_art z on a2.art = z.art
where a2.id = 8874 and z.art is not null
and case when 'T' = 'T' then substr(a2.id_shop_from, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири   ---3618
and case when 'T' = 'F' then substr(a2.id_shop_from, 1, 2) else '00' end not in ('36', '38', '39', '40') -- по магазинам остальным
;

--delete from t_map_new_poexalo 
select * from t_map_new_poexalo
where id = 8874 and (art) in (
  select z.art from st_muya_art z 
  where z.art is not null
)
and case when 'F' = 'T' then substr(id_shop_from, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири 
and case when 'F' = 'F' then substr(id_shop_from, 1, 2) else '00' end not in ('36', '38', '39', '40'); -- по магазинам остальным;

-- исключение стоковой
select * from cena_skidki_all_shops a2
where substr(to_char(a2.cena), -2) = '99'
and case when 'F' = 'T' then substr(a2.id_shop, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири   ---3618
and case when 'F' = 'F' then substr(a2.id_shop, 1, 2) else '00' end not in ('36', '38', '39', '40') -- по магазинам остальным
;

--delete from t_map_new_poexalo 
select * from t_map_new_poexalo
where id = 8874 and (id_shop_from, art) in (
  select a2.id_shop, a2.art from cena_skidki_all_shops a2
  where substr(to_char(a2.cena), -2) = '99'
)
and case when 'F' = 'T' then substr(id_shop_from, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири 
and case when 'F' = 'F' then substr(id_shop_from, 1, 2) else '00' end not in ('36', '38', '39', '40'); -- по магазинам остальны;

-- исключение брака
--select a2.art, a2.id_shop, a2.asize  from e_osttek_online a2
select * from e_osttek_online a2
left join pos_brak x on a2.scan = x.scan and a2.id_shop = x.id_shop
where x.scan is not null
and case when 'T' = 'T' then substr(a2.id_shop, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири   ---3618
and case when 'T' = 'F' then substr(a2.id_shop, 1, 2) else '00' end not in ('36', '38', '39', '40') -- по магазинам остальным
;

--delete from t_map_new_poexalo 
select * from t_map_new_poexalo
where id = 8874 and (art) in (
  select z.art from pos_brack z 
  where z.art is not null
)
and case when 'F' = 'T' then substr(id_shop_from, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири 
and case when 'F' = 'F' then substr(id_shop_from, 1, 2) else '00' end not in ('36', '38', '39', '40'); -- по магазинам остальным;

update t_map_stay_in_shop_kol a
set a.kol_stay = nvl(kol_stay,0) + (select count(*) kol from pos_brak where id_shop = a.id_shop and art = a.art and asize = a.asize group by id_shop, art, asize) 
where (a.id_shop, a.art, a.asize) in (
  select id_shop, art, asize from pos_brak where id_shop = a.id_shop and art = a.art and asize = a.asize group by id_shop, art, asize having count(*) > 0
)
and a.art = '1616070' and a.id_shop = '4505'
and case when 'F' = 'T' then substr(a.id_shop, 1, 2) else '36' end in ('36', '38', '39', '40') -- по магазинам сибири 
and case when 'F' = 'F' then substr(a.id_shop, 1, 2) else '00' end not in ('36', '38', '39', '40'); -- по магазинам остальным;

select id_shop, art, asize, count(*) kol from pos_brak where (id_shop, art, asize) in (select id_shop, art, asize from t_map_stay_in_shop_kol) group by id_shop, art, asize having count(*) > 0;

select count(*) kol from pos_brak where '4505' = id_shop and '1616070' = art and 42 = asize group by id_shop, art, asize;
select * from t_map_stay_in_shop_kol a where a.art = '1616070' and a.id_shop = '4505';


select * from s_art where art = '9854360';


--TODO 
-- 1 фильтрация по условиям
-- 2 сортировка по продажам
-- 3 ограничение по коьисетсву вывозимых, проставленного в настройках и отличнго от дефолтного значения
--insert into t_map_new_poexalo
--(id_shop, art, season, kol, asize, flag, id_shop_from, id, timed, block_no)
select 9999 id_shop, a.art, c.season, a.kol, a.asize, 0 flag, a.id_shop id_shop_from, i_id id,  systimestamp, 1 block_no
from e_osttek_online a
inner join tdm_map_new_shop_group d on a.id_shop = d.shopid and i_s_group = d.s_group
left join (
  select a.id_shop, b.art from t_map_stay_in_shop a
  inner join tdv_map_new_anal b on a.art = b.analog
  group by a.id_shop, b.art 
) b on a.id_shop = b.id_shop and a.art = b.art
inner join s_art c on a.art = c.art
where b.art is null 
and a.kol > 0 and d.season_out like '%' || c.season || '%'
group by a.id_shop, a.art, c.season, a.kol, a.asize;