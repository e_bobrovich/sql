select count(*) from t_map_day_raspred_opo_inf;

select a.*, to_char(to_date('1899-12-30', 'YYYY-MM-DD') + a.erdat,
  'yyyymmdd') from t_map_day_raspred_opo_inf a;

select to_char(
  to_date('1899-12-30', 'YYYY-MM-DD') + 41293,
  'dd.mm.yyyy') from dual;
  
select erdat, to_date(erdat, 'mm/dd/yyyy') from t_map_day_raspred_opo_inf;
  
select erdat, length(erdat), substr(erdat,1,5)  from t_map_day_raspred_opo_inf where erdat is not null;

select zsize, zsize/10 from t_map_day_raspred_opo_inf;

update t_map_day_raspred_opo_inf
set erdat =  substr(erdat,1,5)
where erdat is not null;

update t_map_day_raspred_opo_inf
set aedat =  substr(aedat,1,5)
where aedat is not null;

--update t_map_day_raspred_opo_inf
--set erdat =  to_char(to_date(erdat, 'mm/dd/yyyy'),'yyyymmdd')
--where erdat is not null;
--
--update t_map_day_raspred_opo_inf
--set aedat =  to_char(to_date(aedat, 'mm/dd/yyyy'),'yyyymmdd')
--where aedat is not null;

update t_map_day_raspred_opo_inf
set erdat =  to_char(to_date('1899-12-30', 'YYYY-MM-DD') + erdat,'yyyymmdd')
where erdat is not null;

update t_map_day_raspred_opo_inf
set aedat =  to_char(to_date('1899-12-30', 'YYYY-MM-DD') + aedat,'yyyymmdd')
where aedat is not null;

update t_map_day_raspred_opo_inf
set zsize =  zsize/10
where zsize is not null;

select * from t_map_day_raspred_opo_inf;

select 
bismt art, 
kunnr shopnum,
zsize asize,
matnr,
menge,
menge_use,
menge_rest,
to_date(erdat, 'yyyymmdd') date_add,
to_date(aedat, 'yyyymmdd') date_edit,
aenam user_edit
from t_map_day_raspred_opo_inf a;

select 
b.shopid, nvl(c.analog, a.art) analog, a.art, d.asize, 
x.kol,
case when date_add > date_edit then a.menge_use  else a.menge end menge, 
a.menge_use, a.menge_rest, 
a.date_add, a.date_edit 
--,x.*
from (
    select 
    bismt art, 
    kunnr shopnum,
    zsize asize,
    matnr,
    menge,
    menge_use,
    menge_rest,
    to_date(erdat, 'yyyymmdd') date_add,
    to_date(aedat, 'yyyymmdd') date_edit,
    aenam user_edit
    from t_map_day_raspred_opo_inf a
) a
left join s_shop b on a.shopnum = b.shopnum
left join tdv_map_new_anal c on a.art = c.art
left join s_all_mat d on a.matnr = d.matnr

left join (
    select id_shop, text1, art, asize, kol from t_map_new_poexalo where id = 11234 and id_shop = '0001'
) x on b.shopid = x.id_shop and a.art = x.art and to_number(a.asize) = x.asize

where trunc(a.date_add) = trunc(sysdate)
--and b.shopid = '0001'
--and menge_use != 0
--and x.art is not null
order by a.date_add, b.shopid, nvl(c.analog, a.art), a.art, d.asize
;


-------------------


select * from tdv_map_13_result b order by id desc;

select id_shop, text1, art, to_char(asize) * 10, kol from t_map_new_poexalo where id = 11234;


--select id_shop, sum(menge) menge, sum(kol) kol from (
select id_shop "МАГАЗИН", analog "АНАЛОГ", art "АРТИКУЛ", asize "РАЗМЕР", sum(kol) "РАСПРЕДЕЛЕНО", sum(menge_use) "ОТПРАВЛЕНО" from (
    select 
    b.shopid id_shop, nvl(c.analog, a.art) analog, a.art, d.asize, 
    a.menge_use, 0 kol
    from (
        select 
        bismt art, 
        kunnr shopnum,
        zsize asize,
        matnr,
        menge,
        menge_use,
        menge_rest,
        to_date(erdat, 'yyyymmdd') date_add,
        to_date(aedat, 'yyyymmdd') date_edit,
        aenam user_edit
        from t_map_day_raspred_opo_inf a
    ) a
    left join s_shop b on a.shopnum = b.shopnum
    left join tdv_map_new_anal c on a.art = c.art
    left join s_all_mat d on a.matnr = d.matnr
    
    where trunc(a.date_add) = trunc(to_date('17.06.2019', 'dd.mm.yyyy'))--trunc(sysdate)-1
    and b.shopid = '0010'
    and a.menge_use != 0
    --order by a.date_add, b.shopid, nvl(c.analog, a.art), a.art, d.asize
    
    union all
    
    select id_shop, text1, art, asize, 0, kol from t_map_new_poexalo where id = 11234 and id_shop = '0010'
) group by id_shop, analog, art, asize order by id_shop, analog, art, asize
--) group by id_shop
;

select id_shop, text1, art, asize, 0, kol from t_map_new_poexalo where id = 11234 and id_shop = '0010';
--1916080 
--11234	17.06.19 04:50:48,348168000
--11214	16.06.19 06:19:41,969690000
--11194	15.06.19 05:58:54,857702000
--11174	14.06.19 06:24:49,902009000
--11154	13.06.19 07:22:15,638931000
--11114	12.06.19 07:45:07,884522000
--11074	11.06.19 07:03:32,775590000
--11054	10.06.19 07:14:04,712546000