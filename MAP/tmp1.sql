select x.*
from (select a.id_shop, nvl(b.reit2, 0) reit2, a.art, a.season, x.shop_in, sum(kol_sale) kol_sale,
              sum(kol_sale) / (sum(kol_sale) + sum(kol_ost2)) * 100 sale_kol, x.count_limit_in,
              d.release_year, sum(d.kol) kol_otbor, d.groupmw, c.group_name
       from TDV_MAP_NEW_OST_SALE a
       inner join (select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
                         y.groupmw
                  from T_MAP_NEW_OTBOR x
                  left join s_art y on x.art = y.art
                  -- nvl2(i_group_name, i_group_name, z.group_name)
                  group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw) d on a.art =
                                                                                           d.analog
       inner join tdm_map_new_shop_group x on x.s_group = '15836'
                                              and a.id_shop = x.shopid
                                              and x.season_in like '%' || a.season || '%'
       left join tdv_map_shop_reit b on a.id_shop = b.id_shop
       left join (select distinct analog, group_name
                 from t_map_group_ryb) c on a.art = c.analog
       left join (select distinct analog
                 from tdv_map_stock2) c1 on a.art = c1.analog
       left join (select id_shop
                 from tdv_map_without_stock s
                 where s.only_purple is null) d1 on a.id_shop = d1.id_shop
       where kol_sale + kol_ost != 0
             and not (c1.analog is not null and d1.id_shop is not null)
             and ((c1.analog is not null and a.id_shop = '0032') or a.id_shop != '0032')
       group by a.id_shop, a.art, a.season, x.shop_in, nvl(b.reit2, 0), x.count_limit_in, d.release_year,
                d.groupmw, c.group_name
       having(sum(kol_sale) + sum(kol_ost) != 0) and sum(kol_sale) >= 5/*i_sale_pair_count*/ and sum(kol_sale) <= 100/*i_sale_max_pair_count*/ and sum(kol_ost) <= 100/*i_ost_pair_count*/) x

order by release_year desc, kol_otbor desc, kol_sale desc, reit2
;


select x.analog, sum(x.kol) kol, replace(y.release_year, ' ', '2018') release_year,
       y.groupmw
from T_MAP_NEW_OTBOR x
left join s_art y on x.art = y.art
-- nvl2(i_group_name, i_group_name, z.group_name)
group by x.analog, replace(y.release_year, ' ', '2018'), y.groupmw;