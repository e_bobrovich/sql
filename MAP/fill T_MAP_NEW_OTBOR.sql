/*
сбор всего что нужно распределить
*/
--insert into T_MAP_NEW_OTBOR
    
select id_shop, art, season, sum(kol), asize, analog, status_flag
from (
     -- россыпь склада
     select '9999' id_shop, a.art, b.season, sum(kol) kol, a.asize, nvl(c.analog, b.art) analog, 0 status_flag
     from tem_mapif_fill_warehouse_stock a
     inner join s_art b on a.art = b.art
     left join TDV_MAP_NEW_ANAL c on a.art = c.art
     where b.art not like '%/О%'
           and ',Зимняя,' like '%,' || b.season || ',%'
     group by a.art, b.season, a.asize, nvl(c.analog, b.art)
     
     union all
     -- транзиты
     select '9999', a1.art, c.season, sum(kol) kol, asize, nvl(d.analog, a1.art) analog, 0 status_flag
     from trans_in_way a1
     left join TDV_MAP_NEW_ANAL d on a1.art = d.art
     left join tdv_map_ex_transit e on a1.skladid = e.id_shop
                                       and a1.Postno_Lifex = e.id
     inner join s_art c on a1.art = c.art
     where scan != ' '
           and ',Зимняя,' like '%,' || c.season || ',%'
           and a1.art not like '%/О%'
           and (e.id_shop is not null or a1.shopid not like '%D%' or a1.skladid != 'KI1000')
     group by a1.art, c.season, asize, nvl(d.analog, a1.art)
     having sum(kol) > 0
     
     union all
     -- остатки магазинов
     select /*'9999'*/a2.id_shop, a2.art, a2.season, sum(a2.kol) kol, a2.asize, a2.analog, 0 status_flag
     from e_osttek_online_short a2
     left join st_muya_art f on a2.art = f.art
--             left join tdv_map_bad_sales j on a2.id_shop = j.id_shop
--                                              and a2.analog = j.art
     where 
--             1 = case
--                     when a2.landid = 'RU' and f.art is not null then
--                      0
--                     else
--                      1
--                   end -- исключаем для РФ вывоз покупной
           --and j.art is not null -- только то, что плохо продавалось
           --and substr(a2.id_shop, 1, 2) not in ('36', '38', '39', '40') --из сибири не забираем
           f.art is null
           AND procent = 0
     group by a2.id_shop,a2.art, a2.season, a2.asize, a2.analog
     having sum(kol) > 0
     
     union all
     -- остатки РЦ и СВХ
     select '9999', a2.art, c.season, sum(a2.kol) kol, a2.asize, nvl(d.analog, a2.art) analog, 0 status_flag
     from tdv_map_rc_data a2
     inner join s_art c on a2.art = c.art
     left join TDV_MAP_NEW_ANAL d on d.art = a2.art
     where ',Зимняя,' like '%,' || c.season || ',%'
           and a2.art not like '%/О%'
           and substr(skladid, 1, 1) != 'W'
           and substr(skladid, 3, 1) != 'S'
     group by a2.art, c.season, a2.asize, nvl(d.analog, a2.art)
     having sum(a2.kol) > 0)

group by id_shop, art, season, asize, analog, status_flag;