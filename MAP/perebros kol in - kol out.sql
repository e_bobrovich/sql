select substr(c.shopnum,2) fil, substr(d.shopnum,2) shopnum, a.* from (
    select id_shop, season, sum(kol_in) kol_in, sum(kol_out) kol_out from (
        select id_shop id_shop, season, sum(kol) kol_in, 0 kol_out from t_map_new_poexalo a where id >= 11925 and id <= 11947 group by id_shop, season
        union all
        select id_shop_from id_shop, season, 0 kol_in, sum(kol) kol_out from t_map_new_poexalo a where id >= 11925 and id <= 11947 group by id_shop_from, season
    ) group by id_shop, season
) a
inner join st_retail_hierarchy b on a.id_shop = b.shopid
inner join s_shop c on b.shopparent = c.shopid
inner join s_shop d on a.id_shop = d.shopid
order by a.id_Shop, a.season
;

select id_shop id_shop, season, sum(kol) kol_in, 0 kol_out from t_map_new_poexalo a where id >= 11925 and id <= 11947 and id_shop = '0063' group by id_shop, season
union all
select id_shop_from id_shop, season, 0 kol_in, sum(kol) kol_out from t_map_new_poexalo a where id >= 11925 and id <= 11947 and id_shop_from = '0063' group by id_shop_from, season;

select * from t_map_new_poexalo where id >= 11925 and id <= 11947 and id_shop_from = '0063';
select * from t_map_new_poexalo where id >= 11925 and id <= 11947 and id_shop = '0063';