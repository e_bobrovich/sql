-- таблица с транзитами и данными с заказами , лежащими на СГП
-- что должно остаться, так же попадает в эту таблицу
--insert into TDV_NEW_SHOP_OST
select distinct y.shopid, nvl(a2.analog, x3.art), to_number(asize, '9999.99'), 1, 'TRANS'
from tdv_map_new_sap_reserv2 x3
left join s_shop y on x3.kunnr = y.shopnum
left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
where x3.boxty = ' '
      and to_number(asize, '9999.99') != 0 -- россыпь
      and substr(nvl(kunnr, ' '), 1, 2) != 'X_'
      and verme > 0
      and shopid is not null

union
select distinct y.shopid, nvl(a2.analog, a.art), to_number(b.asize, '9999.99'), 1, 'TRANS'
from tdv_map_new_sap_reserv2 a
inner join T_SAP_BOX_ROST b on a.boxty = b.boxty
inner join s_shop y on a.kunnr = y.shopnum
left join TDV_MAP_NEW_ANAL a2 on a.art = a2.art
where a.boxty != ' ' --короба
      and verme > 0
      and substr(nvl(kunnr, ' '), 1, 2) != 'X_'

union
select distinct x.shopid, nvl(a2.analog, x3.art) art, nvl(f.asize, to_number(x3.asize, '999.99')) asize, 1, 'TRANS'
from tdv_map_new_sap_reserv2 x3
left join T_SAP_BOX_ROST f on x3.boxty = f.boxty
left join (select boxty, sum(kol) kol
           from T_SAP_BOX_ROST
           group by boxty) e on x3.boxty = e.boxty
left join s_shop x on x3.kunnr = x.shopnum
left join TDV_MAP_NEW_ANAL a2 on x3.art = a2.art
where kunnr not in ('X_NERASP', 'X_SD_USE')
      and trans = 'X'
group by nvl(a2.analog, x3.art), nvl(f.asize, to_number(x3.asize, '999.99')), x.shopid

UNION
-- остатки магазинов -- покупную  оставляем в магазине -- и некондицию -- b и детскую
select a2.id_shop, a2.analog, a2.asize, 1, 'TRANS'
FROM e_osttek_online_short a2
inner join s_art c on a2.art = c.art
LEFT JOIN st_muya_art F ON a2.art = F.art
--      left join tdv_map_bad_sales j on a2.id_shop = j.id_shop and a2.analog = j.art
where 
(
--        1 = case when a2.landid = 'RU' and f.art is not null then 1 else 0 end 
    f.art is not null
    or a2.procent != 0 -- некондицию оставляем
    --or j.art is null -- что хорошо продавалось не забираем
    or c.groupmw not in ('Мужские', 'Женские') --детскую оставляем
    OR substr(a2.id_shop, 1, 2) IN ('36', '38', '39', '40') --из сибири не забираем
) 
group by a2.id_shop, a2.analog, a2.asize
having sum(kol) > 0

union
-- остатки РЦ и СВХ
select a2.id_shop, nvl(d.analog, a2.art) analog, a2.asize, 1, 'TRANS'
from tdv_map_rc_data a2
inner join s_art c on a2.art = c.art
left join TDV_MAP_NEW_ANAL d on d.art = a2.art
where ',Зимняя,' like '%,' || c.season || ',%'
      and (substr(skladid, 1, 1) = 'W' or substr(skladid, 3, 1) = 'S')

/* and (a2.art like '%/О%' or j.art is null -- только то, что плохо продавалось
or substr(a2.id_shop, 1, 2) in ('36', '38', '39', '40') --из сибири не забираем
and 1 = case when e.landid = 'RU' and f.art is not null then 1 else 0 end) -- исключаем для РФ вывоз покупной   
*/
group by a2.id_shop, a2.asize, nvl(d.analog, a2.art)
having sum(a2.kol) > 0
;