SELECT a.*,nvl2(a.fam,a.fam||nvl2(a.ima,' ',null),null)||nvl2(a.ima,a.ima||nvl2(a.otch,' ',null),null)||nvl2(a.otch,a.otch,null) fio_true,(select min(b.bit_accept) from d_inet_book2 b where a.posnr = b.posnr and a.serv_ip = b.serv_ip) bit_accept,
case when a.bit_book = 'T' then 'БРОНИРОВАНИЕ' when c.bit_move = 'T' then 'ПЕРЕМЕЩЕНИЕ' when a.bit_delivery_addr = 'T' then 'ДОСТАВКА НА АДРЕС' else 'ОФОРМЛЕННАЯ ПОКУПКА' end type_book,
to_timestamp(to_char(a.dated + ('-3'+to_char(systimestamp,'tzh'))/24,'dd.MM.yyyy HH24:mi:ss'),'dd.MM.yyyy HH24:mi:ss') timed,
floor(sysdate-(a.dated+ ('-3'+to_char(systimestamp,'tzh'))/24))||':'||to_char(trunc(sysdate) + floor((sysdate-(a.dated+ ('-3'+to_char(systimestamp,'tzh'))/24))*24*60) / 24/60, 'hh24:mi') timep,
case when a.date_rs is null or d.posnr is not null then 'ОБНОВЛЕН' else ' ' end new_st,z
c.bit_move, to_char(delivery_date,'dd.mm.yyyy') delivery_dates,
a.DELIVERY_TOWN, a.DELIVERY_ADDRESS, a.DELIVERY_BUILDING, a.DELIVERY_BLOCK, a.DELIVERY_APPARTMENT, 
nvl2(a.DELIVERY_FAM,a.DELIVERY_FAM||nvl2(a.DELIVERY_IMA,' ',null),null)||nvl2(a.DELIVERY_IMA,a.DELIVERY_IMA||nvl2(a.DELIVERY_OTCH,' ',null),null)||nvl2(a.DELIVERY_OTCH,a.DELIVERY_OTCH,null) delivery_fio
FROM D_INET_BOOK1 a
left join (select posnr,serv_ip,min(bit_sale) bit_sale,max(bit_block) bit_block,min(bit_move) bit_move,min(nvl(rx_id,0)) rx_id
            from d_inet_book2
            group by posnr,serv_ip) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip
left join d_inet_book_alert_shop d on a.posnr = d.posnr and a.serv_ip = d.serv_ip
where (a.bit_status = 'T' and c.bit_sale = 'F' and c.bit_block = 'T' and not (c.bit_move = 'T' and c.rx_id != 0)) or a.date_rs is null
order by new_st desc,a.dated desc,a.posnr desc;

alter table d_inet_book1 add (
"BIT_DELIVERY_ADDR" VARCHAR2(1 CHAR) DEFAULT 'F',
"DELIVERY_TOWN" VARCHAR2(100 CHAR) ,
"DELIVERY_ADDRESS" VARCHAR2(300 CHAR) ,
"DELIVERY_FAM" VARCHAR2(100 CHAR) ,
"DELIVERY_IMA" VARCHAR2(100 CHAR) ,
"DELIVERY_OTCH" VARCHAR2(100 CHAR) ,
"DELIVERY_APPARTMENT" VARCHAR2(300 CHAR) ,
"DELIVERY_BUILDING" VARCHAR2(300 CHAR) ,
"DELIVERY_BLOCK" VARCHAR2(300 CHAR)
)
;
