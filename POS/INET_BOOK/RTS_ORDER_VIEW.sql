select * from firm.RTS_ORDER_VIEW where status = 'SHIPPED';



  select b1.posnr, b1.serv_ip server, 
case when b2.id_shop='N/A' then case when ss.landid='RU' then 'ST_RU_2101' else 'ST_BY_0001' end else case when substr(b2.id_shop,0,1)='0' then 'ST_BY_'||b2.id_shop else 'ST_RU_'||b2.id_shop end end id_shop,  
0 deliveryCost,
b2.sum totalPrice, 
b2.discount totalDiscounts, 
b2.base_price subTotal, 
null deliveryStatus, 
null paymentStatus, 
case when b1.BIT_HYB_READY='T' then case when b1.bit_delivery_addr='F' then 'PICKUP_COMPLETE' else 'SHIPPED' end else case when b1.BIT_HYB_CANCEL='T' then 'CANCELLED' else case when b1.BIT_HYB_RECEIVE='T' then 'COMPLETED' else 'CREATED' end end end status,
b2.track trackingID, 
case when b1.BIT_HYB_READY='T' then case when b1.bit_delivery_addr='F' then 'PICKUP_COMPLETE' else 'SHIPPED' end else case when b1.BIT_HYB_CANCEL='T' then 'CANCELLED' else case when b1.BIT_HYB_RECEIVE='T' then 'COMPLETED' else 'CREATED' end end end consignmentStatus, 
null shippingAddressCode, 
case when b1.bit_delivery_addr = 'F' then case when b2.id_shop='N/A' then case when ss.landid='RU' then 'ST_RU_2101' else 'ST_BY_0001' end else case when substr(b2.id_shop,0,1)='0' then 'ST_BY_'||b2.id_shop else 'ST_RU_'||b2.id_shop end end else case when ss.landid='RU' then 'ST_RU_2101' else 'ST_BY_0001' end end deliveryPointOfServiceId, 
to_char(b1.dated+1, 'dd.mm.yyyy') statusDate 
from firm.d_inet_book1 b1 
inner join (
  select b2.posnr, b2.serv_ip, sum(case when b4.BIT_HYB_CANCEL='F' then b2.discount else 0 end) discount, 
  sum(case when b4.BIT_HYB_CANCEL='F' then b2.base_price else 0 end) base_price, 
  min(b2.id_shop) id_shop, 
  sum(case when b4.BIT_HYB_CANCEL='F' then b2.sum else 0 end) sum,
  max(case when b4.BIT_HYB_READY='T' then 'READY' else case when b4.BIT_HYB_CANCEL='T' then 'CANCELLED' else case when b4.BIT_HYB_RECEIVE='T' then 'RECEIVED' else 'CREATED' end end end) status,
  min(b4.dpd_id) track
  from firm.d_inet_book2 b2
  inner join firm.d_inet_book4 b4 on b2.posnr = b4.posnr and b2.serv_ip = b4.serv_ip and b2.rnum = b4.rnum and b2.bit_move = b4.bit_move
  where b2.bit_move='F' and b2.bit_delete = 'F'
  group by b2.posnr, b2.serv_ip
)b2 on b2.posnr = b1.posnr and b2.serv_ip = b1.serv_ip
inner join HYBRIS.st_server_for_servlet ss on b1.serv_ip=ss.server
where b1.bit_book='F';
