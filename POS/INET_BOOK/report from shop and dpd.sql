SELECT * FROM RI_REPLICATE@S0018 ORDER BY REL DESC;


/*
1. Сформировать отчёт по интернет-заказам.
2. Выбрать заказы 
  -отправитель является магазин
  -присвоен номер заказа ДПД

3. Отчёт отобразить в след виде:
Номер заказа/Дата заказа/Получатель/артикул/размер/статус в магазине/стоимость 


должно выполняться условие, 
если хотя бы одна позиции имеет отправителя магазин И к этой позиции привязан номер ДПД, 
то выгружаем весь заказ со всем позициями
*/

select b1.posnr,f.*  from d_inet_book1 b1
left join d_inet_book2 b2 on b2.posnr = b1.posnr	and b2.serv_ip = b1.serv_ip
left join D_INET_BOOK_COND_T f on f.posnr = b1.posnr and f.serv_ip = b1.serv_ip
--left join d_inet_book3 b3 on b3.posnr = b1.posnr	and b3.serv_ip = b1.serv_ip	--and b3.init = 'SHOP'	and b3.cond like 'ПОДТВЕРЖДЕН МАГАЗИНОМ%'
order by b1.dated desc;


select 