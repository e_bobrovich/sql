call SPA_MOVE_MATRIX_TEST.run_move_matrix('08173743', '192.168.22.252');

select a.posnr,a.art,a.asize,1 kol,a.id_shop_from,a.id_shop_to 
from tem_inet_move_matrix_result a
inner join (select level lvl from dual connect by level <= 1000) b on b.lvl <= a.kol
order by id_shop_from;

select * from d_inet_book2 where posnr = '09100621';
select * from d_inet_book1 where posnr = '00017214';

SELECT --DISTINCT POSNR, SERV_IP
POSNR, SERV_IP, ART, ASIZE, RNUM, SUM(KOL)
FROM (
       SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, b2.KOL -- позиции из заказа
       FROM D_INET_BOOK1 b1
                JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
       WHERE b1.BIT_BOOK = 'F'
         AND b2.BIT_MOVE = 'F'
         AND b2.bit_sale = 'F'
         AND b1.bit_status = 'T'
         AND b2.ID_SHOP_FROM IS NOT NULL
         AND b2.ID_SHOP_TO IS NOT NULL
         AND COALESCE(ID_CANCEL, 0) = 0
         and b1.BIT_DELIVERY_ADDR = 'T'
       UNION ALL
       SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL -- убирается все что отменили в заказе
       FROM D_INET_BOOK1 b1
                JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
       WHERE b1.BIT_BOOK = 'F'
         AND b2.BIT_MOVE = 'F'
         AND b1.BIT_STATUS = 'T'
         AND b2.ID_SHOP_FROM IS NOT NULL
         AND b2.ID_SHOP_TO IS NOT NULL
         AND (b2.ID_SHOP_FROM IN (b2.ID_SHOP_TO, 'NOT_OST') OR b2.ID_SHOP_TO = 'KI1000')
         AND COALESCE(ID_CANCEL, 0) = 0
         and b1.BIT_DELIVERY_ADDR = 'T'
       UNION ALL
       SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL -- убираем то что распределено
       FROM D_INET_BOOK1 b1
                JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
       WHERE b1.BIT_BOOK = 'F'
         AND b2.BIT_MOVE = 'T'
         AND b2.BIT_DELETE = 'F'
         AND b2.ID_SHOP_FROM IS NOT NULL
         AND b2.ID_SHOP_TO IS NOT NULL
         AND b1.bit_status = 'T'
         AND COALESCE(ID_CANCEL, 0) = 0
        and b1.BIT_DELIVERY_ADDR = 'T'   
      UNION ALL
      SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, 1 * b2.KOL AS KOL -- Добавляем то что отменено по на СГП
       FROM D_INET_BOOK1 b1
                JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                JOIN D_INET_BOOK_SAP_STATUS ss ON b2.RNUM = ss.RNUM AND b2.POSNR = ss.POSNR AND b2.SERV_IP = ss.SERV_IP
       WHERE b1.BIT_BOOK = 'F'
         AND b2.BIT_MOVE = 'F'
         AND b2.BIT_DELETE = 'F'
         AND b2.ID_SHOP_FROM IS NOT NULL
         AND b2.ID_SHOP_TO IS NOT NULL
         AND b1.BIT_STATUS = 'T'
         AND COALESCE(b2.ID_CANCEL, 0) = 0
         AND ss.STATUS_RNUM_SAP IN ('CL', 'DS', 'NS')
         AND b2.ID_SHOP_FROM = 'KI1000'
         and b1.BIT_DELIVERY_ADDR = 'T'
     )
WHERE SERV_IP IN (SELECT SERVER FROM HYBRIS.ST_SERVER_FOR_SERVLET WHERE LANDID = 'BY' AND TYPE = 'P')
GROUP BY POSNR, SERV_IP, ART, ASIZE, RNUM
--HAVING SUM(KOL) > 0
order by posnr desc, serv_ip, rnum
;


SELECT 
--b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL -- убираем то что распределено
*
FROM D_INET_BOOK1 b1
        JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
WHERE b1.BIT_BOOK = 'F'
and b1.posnr = '08184295'
 AND b2.BIT_MOVE = 'T'
 AND b2.BIT_DELETE = 'F'
 AND b2.ID_SHOP_FROM IS NOT NULL
 AND b2.ID_SHOP_TO IS NOT NULL
 AND b1.bit_status = 'T'
 AND COALESCE(ID_CANCEL, 0) = 0