select * from t_ob_logs where textd = 'SPA_MOVE_MATRIX'  and text1 = '08470782' order by timed desc;
select * from t_ob_logs where textd = 'KAA_CHECK_EMPTY_MOVE'  and text1 = '08532493' order by timed desc;

select * from d_inet_book1 where posnr = '08532493';
select * from d_inet_book2 where posnr = '08470782';
select * from d_inet_book3 where posnr = '08532493' order by date_s desc;

select * from history where sql_t like '%08470782%' order by date_s1 desc;




SELECT 
--CASE WHEN COALESCE(MAX(KOL), 0) > 0 THEN 'T' ELSE 'F' END AS IS_NEED
CASE WHEN MAX(BIT_DELIVERY_ADDR) = 'F' THEN -- если не доставка на адрес, то должна быть хоть одна позиция с положительной суммой
  CASE WHEN COALESCE(MAX(KOL), 0) > 0 THEN 'T' ELSE 'F' END 
ELSE  -- если доставка на адрес, то должна быть хоть одна позиция с положительной суммой и у всех позиций должна быть сумма = 0 (значит что все изначальные позиции отменены магазином)
  CASE WHEN COALESCE(MAX(KOL), 0) > 0 AND COALESCE(MAX(KOL2), 0) = 0 THEN 'T' ELSE 'F' END 
END 
AS IS_NEED
INTO V_RESULT
FROM (
         SELECT POSNR, SERV_IP, ART, ASIZE, RNUM, SUM(KOL) AS KOL, SUM(KOL2) AS KOL2, MAX(BIT_DELIVERY_ADDR) BIT_DELIVERY_ADDR
         FROM (
                  SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, b2.KOL, b1.BIT_DELIVERY_ADDR, -1*b2.KOL kol2
                  FROM BACKUP_FIRM_TRADE_0.D_INET_BOOK1 b1
                           JOIN BACKUP_FIRM_TRADE_0.D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                  WHERE b1.BIT_BOOK = 'F'
                    AND b1.POSNR = P_POSNR
                    AND b1.SERV_IP = P_SERVER_IP
                    AND b2.BIT_MOVE = 'F'
                    AND b2.bit_sale = 'F'
                    AND b1.bit_status = 'T'
                    AND b2.ID_SHOP_FROM IS NOT NULL
                    AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')--b2.ID_SHOP_TO IS NOT NULL
                    AND COALESCE(ID_CANCEL, 0) = 0
                  UNION ALL
                  SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2
                  FROM BACKUP_FIRM_TRADE_0.D_INET_BOOK1 b1
                           JOIN BACKUP_FIRM_TRADE_0.D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                  WHERE b1.BIT_BOOK = 'F'
                    AND b1.POSNR = P_POSNR
                    AND b1.SERV_IP = P_SERVER_IP
                    AND b2.BIT_MOVE = 'F'
                    AND b1.BIT_STATUS = 'T'
                    AND b2.ID_SHOP_FROM IS NOT NULL
                    AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')--b2.ID_SHOP_TO IS NOT NULL
                    AND (b2.ID_SHOP_FROM IN (b2.ID_SHOP_TO, 'NOT_OST') OR (b2.ID_SHOP_TO = 'KI1000' AND b1.BIT_DELIVERY_ADDR = 'F'))
                    AND COALESCE(ID_CANCEL, 0) = 0
                  UNION ALL
                  SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2
                  FROM BACKUP_FIRM_TRADE_0.D_INET_BOOK1 b1
                           JOIN BACKUP_FIRM_TRADE_0.D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                  WHERE b1.BIT_BOOK = 'F'
                    AND b1.POSNR = P_POSNR
                    AND b1.SERV_IP = P_SERVER_IP
                    AND b2.BIT_MOVE = 'T'
                    AND b2.BIT_DELETE = 'F'
                    AND b2.ID_SHOP_FROM IS NOT NULL
                    AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')--b2.ID_SHOP_TO IS NOT NULL
                    AND b1.bit_status = 'T'
                    AND COALESCE(ID_CANCEL, 0) = 0
                  UNION ALL
                  SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, 1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2
                  FROM BACKUP_FIRM_TRADE_0.D_INET_BOOK1 b1
                           JOIN BACKUP_FIRM_TRADE_0.D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                           JOIN D_INET_BOOK_SAP_STATUS ss
                                ON b2.RNUM = ss.RNUM AND b2.POSNR = ss.POSNR AND b2.SERV_IP = ss.SERV_IP
                  WHERE b1.BIT_BOOK = 'F'
                    AND b1.POSNR = P_POSNR
                    AND b1.SERV_IP = P_SERVER_IP
                    AND b2.BIT_MOVE = 'F'
                    AND b2.BIT_DELETE = 'F'
                    AND b2.ID_SHOP_FROM IS NOT NULL
                    AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')--b2.ID_SHOP_TO IS NOT NULL
                    AND b1.BIT_STATUS = 'T'
                    AND COALESCE(b2.ID_CANCEL, 0) = 0
                    AND ss.STATUS_RNUM_SAP IN ('CL', 'DS', 'NS')
                    AND b2.ID_SHOP_FROM = 'KI1000'
                  ------------------------------
                  UNION ALL  
                  SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, 0 KOL, b1.BIT_DELIVERY_ADDR, b2.kol kol2
                  FROM BACKUP_FIRM_TRADE_0.D_INET_BOOK1 b1
                           JOIN BACKUP_FIRM_TRADE_0.D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                  WHERE b1.BIT_BOOK = 'F'
                    AND b1.POSNR = P_POSNR
                    AND b1.SERV_IP = P_SERVER_IP
                    AND b2.BIT_MOVE = 'F'
                    AND b2.bit_sale = 'F'
                    AND b1.bit_status = 'T'
                    AND b2.ID_SHOP_FROM IS NOT NULL
--                      AND b2.ID_SHOP_TO IS NOT NULL
--                      AND COALESCE(ID_CANCEL, 0) = 0
                    AND b1.BIT_DELIVERY_ADDR = 'T'
              )
         WHERE SERV_IP IN (SELECT SERVER FROM HYBRIS.ST_SERVER_FOR_SERVLET WHERE LANDID = 'BY' AND TYPE = 'P')
         GROUP BY POSNR, SERV_IP, ART, ASIZE, RNUM
         HAVING (MAX(BIT_DELIVERY_ADDR) = 'F' AND SUM(KOL) > 0) OR (MAX(BIT_DELIVERY_ADDR) = 'T' AND SUM(KOL) >= 0 AND SUM(KOL2) >= 0)
--           HAVING SUM(KOL) > 0
     )
GROUP BY POSNR, SERV_IP;