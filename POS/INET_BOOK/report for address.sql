select cnt_rnum, count(*) from (
  select posnr, serv_ip, count(rnum) cnt_rnum from (  
    select x.posnr,x.serv_ip,x.rnum, max(shopid) shopid
    from (select posnr,serv_ip,rnum,
            Bit_Delivery_Addr,
            case when bit_move = 'F' then shopid else null end shopid,
            case when bit_move = 'F' then art else null end art,
            case when bit_move = 'F' then asize else null end asize,
            case when bit_move = 'F' then scan else null end scan,
            case when bit_move = 'F' then kol else null end kol,
            case when bit_move = 'F' then dpd_id else null end dpd_id,
            case when bit_move = 'F' then id_shop_to_num else null end id_shop_to_num,
            case when bit_move = 'F' then id_shop_from_num else null end id_shop_from_num
          from (
                SELECT a.posnr,a.serv_ip,b.rnum,
                b.art,b.asize,b.scan,b.kol,b.bit_move,g.dpd_id,
                b.id_shop_to id_shop_to_num, b.id_shop_from id_shop_from_num,
                case when b.id_shop = 'N/A' then 'ДОСТАВКА НА АДРЕС' when b.id_shop = 'KI1000' then 'СГП' else c.shopid end shopid,
                A.Bit_Delivery_Addr
                FROM d_inet_book1 a
                inner join D_INET_BOOK2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                left join (select bit_move,rnum,posnr,serv_ip,listagg(substr(shopnum,2)||' '||cityname||' '||address,', ') within group (order by id_shop) shopid
                      from (select distinct x.bit_move,x.rnum,x.posnr,x.serv_ip,x.id_shop,y.shopnum,y.cityname,y.address
                            from d_inet_book2 x
                            left join s_shop y on x.id_shop = y.shopid
                            where x.bit_delete = 'F')
                      group by bit_move,rnum,posnr,serv_ip) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip and c.rnum = b.rnum and c.bit_move = b.bit_move
                inner join d_inet_book4 g on b.posnr = g.posnr and b.serv_ip = g.serv_ip and b.rnum = g.rnum and b.bit_move = g.bit_move
                left join s_shop e on e.shopid = b.id_shop_to
                left join s_shop f on f.shopid = b.id_shop_from
                where b.bit_delete = 'F'             
          )
    ) x
    --where x.Bit_Delivery_Addr = 'T'
    group by x.posnr,x.serv_ip, x.rnum
    having max(x.shopid) = 'ДОСТАВКА НА АДРЕС'
  --  order by x.posnr desc, x.serv_ip, x.rnum
  )
  group by posnr, serv_ip
) group by cnt_rnum
order by cnt_rnum
;