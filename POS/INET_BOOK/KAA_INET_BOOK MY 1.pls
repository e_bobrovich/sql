create or replace package body KAA_INET_BOOK as
  -- константы
  C_CODE_SGP constant varchar2(10) := 'KI1000';


  --процедура обмена интернет-заказов
	procedure BOOK_EXCHANGE(SHOPIDIN    in varchar2,
																	 posnrin     in varchar2 default null,
																	 serv_ipin   in varchar2 default null,
																	 bit_bookin  in varchar2 default null,
																	 cur_posnrin in sys_refcursor default null,
																	 result      out varchar2) as
		shoplink_p varchar2(10);
		posnr_p varchar2(50);
		serv_ip_p varchar2(50);
		rnum_p number(10);
		count_p number(5);
		count_t_p number(5);
		count_f_p number(5);
		stat_p varchar2(100);
		id_stat_p number(5);
		send_count_p number(5);
		is_hyb_serv_prod_p varchar2(1);
		bit_delete_p varchar2(5);
		bit_cancel_reverse_p varchar2(5);
    bit_cancel_firm_p varchar2(5);
    art_p varchar2(100);
    asize_p number(5,1);
    bit_move_p varchar2(5);
    v_cnt number;
	
		cursor book is
			select distinct a.posnr, a.serv_ip, b.rnum
			from d_inet_book1 a
			inner join d_inet_book2_v b on a.posnr = b.posnr
																		 and a.serv_ip = b.serv_ip
			where b.id_shop = SHOPIDIN
						and (a.bit_send = 'F' or b.bit_send = 'F')
			order by a.posnr, a.serv_ip, b.rnum; --неотправленные заказы
		bookRow book%rowtype;
		--TYPE t_rec IS RECORD (
		--      posnr VARCHAR2(50 CHAR),
		--      dated DATE, 
		--      phone_number VARCHAR2(30 CHAR),
		--      id_dk VARCHAR2(20 CHAR),
		--      bit_status VARCHAR2(1 CHAR),
		--      bit_send VARCHAR2(1 CHAR),
		--      bit_online_pay VARCHAR2(1 CHAR),
		--      FAM VARCHAR2(100 CHAR),
		--      IMA VARCHAR2(100 CHAR),
		--      OTCH VARCHAR2(100 CHAR),
		--      BIT_BOOK VARCHAR2(1 CHAR),
		--      DATE_RS DATE,
		--      BOOK_NAME VARCHAR2(100 CHAR));
		--cur_rec t_rec;
		cur_rec d_inet_book1%rowtype;
		--PRAGMA AUTONOMOUS_TRANSACTION; 
	begin
--		delete from temp_kaatest;

    --проверка на закрытый магазин
    select count(*) into v_cnt
      from st_shop
      where org_kod = 'SHP' and bit_open = 'F'
      and trunc(sysdate-30) > trunc(nvl(date_closed,sysdate))
      and shopid = SHOPIDIN
    ;

		--если магазин N/A - доставка на адрес, то статус отправки ставлю TRUE
		if SHOPIDIN in ('N/A','KI1000','NOT_OST') or v_cnt > 0 then
			update d_inet_book2
				set bit_send = 'T'
				where (id_shop in ('N/A',C_CODE_SGP,'NOT_OST') or id_shop = SHOPIDIN)
								and bit_send = 'F';
			if cur_posnrin is null then
				update d_inet_book1 a
						set bit_send = (select nvl(min(b.bit_send),'F')
														from d_inet_book2 b
														where b.posnr = a.posnr
														and b.serv_ip = a.serv_ip)
						where (posnr,serv_ip) in (select distinct posnr,serv_ip
																					from d_inet_book2
																					where id_shop in ('N/A','KI1000','NOT_OST') or id_shop = SHOPIDIN);
				commit;
				return;
			else
				select nvl(min(b.bit_send),'F') into result
					from d_inet_book2 b
					where b.posnr = posnrin
					and b.serv_ip = serv_ipin
					and (b.id_shop in ('N/A','KI1000','NOT_OST') or b.id_shop = SHOPIDIN);
				commit;
				return;
			end if;
		end if;

		select nvl(max(db_link_name),'N/A')
		into shoplink_p
		from st_shop
		where shopid = SHOPIDIN;
	
		--общий обмен; posnrin is null - вызываем процедуру не для конкретного заказа
		if posnrin is null then
    
			open book; --прохожу по всем неотправленным заказам
			loop
				fetch book
					into posnr_p, serv_ip_p, rnum_p;
				exit when book%notfound;
				begin
					
					--удаляю из магазина данные по заказам на перемещение, если в центре по ним стоит метка удаления
					select min(bit_delete) into bit_delete_p
						from d_inet_book2_v
						where id_shop = SHOPIDIN and posnr = posnr_p and serv_ip = serv_ip_p and rnum = rnum_p and bit_move = 'T';
					
					if bit_delete_p = 'T' then
						execute immediate 'delete from d_inet_book2@' || shoplink_p || ' a
																where (a.POSNR,a.ID_SHOP,a.SERV_IP,a.RNUM) in (select b.POSNR,b.ID_SHOP,b.SERV_IP,b.RNUM
																																								from d_inet_book2 b
																																								where b.bit_send = ''F'' and b.bit_delete = ''T'' and b.bit_move = ''T''
																																								and b.POSNR = '''||posnr_p||''' and a.ID_SHOP = b.id_shop
																																								and b.SERV_IP = '''||serv_ip_p||''' and a.RNUM = b.rnum)';	
						execute immediate 'delete from d_inet_book1@' || shoplink_p || ' a
																where (a.POSNR,a.SERV_IP) in (select distinct b.POSNR,b.SERV_IP
																																								from d_inet_book2 b
																																								where b.bit_send = ''F'' and b.bit_delete = ''T'' and b.bit_move = ''T''
																																								and b.POSNR = '''||posnr_p||''' and b.id_shop = '''||SHOPIDIN||'''
																																								and b.SERV_IP = '''||serv_ip_p||''')
																and (select count(*)
																			from d_inet_book2@'||shoplink_p||'
																			where posnr = '''||posnr_p||''' and serv_ip = '''||serv_ip_p||''') = 0';
						update d_inet_book2 b
							set bit_send = 'T'
							where b.bit_send = 'F' and b.bit_delete = 'T' and b.bit_move = 'T'
							and b.POSNR = posnr_p 
							and case when is_hyb_serv_prod(b.serv_ip) = 'T' then b.id_shop else 'S888' end = SHOPIDIN
							and b.SERV_IP = serv_ip_p;
						update d_inet_book1 a
							set bit_send = (select nvl(min(b.bit_send),'F')
															from d_inet_book2 b
															where b.posnr = posnr_p
															and b.serv_ip = serv_ip_p)
							where 1 = 1
										and posnr = posnr_p
										and serv_ip = serv_ip_p;
						commit;
						continue;
					end if;
					
					--проверка на наличие заказа в магазине, чтобы знать, нужно ли в нем обновлять данные или этот заказ в магазин вообще не попал
					execute immediate 'select count(*),sum(case when bit_move = ''F'' then 1 else 0 end),sum(case when bit_move = ''T'' then 1 else 0 end)
															into :i,:j,:k from d_inet_book2@' || shoplink_p || ' 
															where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || '''
															and rnum = '||rnum_p
						into count_p,count_f_p,count_t_p;
						
					--если в центре стоит метка отката отмены позиции заказа, то убираем отмену в магазине
					select nvl(max(bit_cancel_reverse),'F') into bit_cancel_reverse_p
						from d_inet_book2_v
						where id_shop = SHOPIDIN and posnr = posnr_p and serv_ip = serv_ip_p and rnum = rnum_p and bit_move = 'F';
						
					if bit_cancel_reverse_p = 'T' and count_f_p > 0 then
						execute immediate 'update d_inet_book2@'||shoplink_p||'
																set date_cancel = null,bit_block = ''T'',id_cancel = null
																where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || '''
															and rnum = '||rnum_p;
						execute immediate 'insert into d_inet_book3@'||shoplink_p||' (POSNR,SERV_IP,COND,RNUM)
																select '''||posnr_p||''','''||serv_ip_p||''',''ПОЗИЦИЯ ЗАКАЗА ПЕРЕРЕЗЕРВИРОВАНА МАГАЗИНОМ'','||rnum_p||'
																from dual';
						update d_inet_book2
							set bit_cancel_reverse = 'F'
							where id_shop = SHOPIDIN and posnr = posnr_p and serv_ip = serv_ip_p and rnum = rnum_p and bit_move = 'F';
					end if;
          
          --если в центре стоит метка отмены позиции заказа, то отменяем ее в магазине
					select nvl(max(bit_cancel_firm),'F') into bit_cancel_firm_p
						from d_inet_book2_v
						where id_shop = SHOPIDIN and posnr = posnr_p and serv_ip = serv_ip_p and rnum = rnum_p;
						
					if bit_cancel_firm_p = 'T' and count_p > 0 then
						execute immediate 'update d_inet_book2@'||shoplink_p||'
																set date_cancel = sysdate,bit_block = ''F'',id_cancel = (select max(op_id_cancel) 
                                                                                                from d_inet_book4
                                                                                                where posnr = ''' || posnr_p || '''
                                                                                                and serv_ip = ''' || serv_ip_p || '''
															                                                                  and rnum = '||rnum_p||')
																where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || '''
															and rnum = '||rnum_p;
						update d_inet_book2
							set bit_cancel_firm = 'F'
							where id_shop = SHOPIDIN and posnr = posnr_p and serv_ip = serv_ip_p and rnum = rnum_p;
					end if;
          
          select nvl(max(art),' '),nvl(max(asize),0) into art_p,asize_p
            from d_inet_book2
            where posnr = posnr_p and serv_ip = serv_ip_p and rnum = rnum_p and bit_move = 'F';
						
					--проверка на остатки   				
					execute immediate 'select nvl(max(stat),''ПУСТОЙ ЗАКАЗ'') stat,nvl(max(id_stat),0) id_stat,nvl(min(bit_move),''F'') bit_move
														from (select bit_move,
																	case when bit_book = ''F'' and bit_move = ''F'' then ''ОК''
																	when bit_book = ''F'' and bit_move = ''T'' and bit_accept = ''T'' then ''ОК''
																	when kol <= 0 or kol < kolz then ''НЕТ В НАЛИЧИИ''
																	when kol <= kolb then ''В НАЛИЧИИ ТОЛЬКО БРАК''
																	when kol - kolb <= kolr then ''В НАЛИЧИИ ТОЛЬКО БРОНЬ''
																	else ''ОК'' end stat,
																	case when bit_book = ''F'' and bit_move = ''F'' then 4
																	when bit_book = ''F'' and bit_move = ''T'' and bit_accept = ''T'' then 4
																	when kol <= 0 or kol < kolz then 1
																	when kol <= kolb then 2
																	when kol - kolb <= kolr then 3
																	else 4 end id_stat
																	from (select k.bit_accept,k.bit_move,k.bit_book,k.rnum,k.kolz,nvl(z.kol,0) kol,nvl(z.kolb,0) kolb,nvl(z.kolr,0) kolr
																	from (select b.bit_accept,a.bit_book,b.bit_move,b.rnum,b.art,b.asize,sum(b.kol) kolz
																				from d_inet_book1 a
																				inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
																				where b.rnum = '||rnum_p||' and a.posnr = '''||posnr_p||''' and a.serv_ip = ''' || serv_ip_p || ''' and b.id_shop = '''||SHOPIDIN||''' 
																				--and (a.bit_book = ''T'' or (b.bit_move = ''T'' and b.bit_delete = ''F'') or (b.bit_move = ''F'' and (b.id_shop_from is null or nvl(b.id_shop_from,''null'') = nvl(b.id_shop_to,''null''))))
																				group by b.bit_accept,a.bit_book,b.bit_move,b.rnum,b.art,b.asize) k
																	left join (select m.art,m.asize,m.kol,m.kolb,nvl(r.kolr,0) kolr
																							from (select q.art,q.asize,sum(q.kol) kol,
																											sum(case when p.scan is null then 0 else 1 end) kolb
																											from (select art,asize,scan,sum(kol) kol
																														from (select x.art,x.asize,x.scan,
																																	sum(x.kol) kol
																																	from e_osttek@' || shoplink_p || ' x
																																	where x.procent = 0 and x.art = '''||art_p||''' and x.asize = '||asize_p||'
																																	group by x.art,x.asize,x.scan
																																	having sum(x.kol) > 0
																																	union all
																																	select art,asize,scan,kol
																																	from d_inet_book2@' || shoplink_p || '
																																	where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || ''' and bit_sale = ''T'')
																															group by art,asize,scan) q
																											left join (select art,asize,scan,1 kolb
																																	from pos_brak@' || shoplink_p || ') p on q.scan = p.scan
																											group by q.art,q.asize) m
																							left join (select b.art,b.asize,sum(b.kol) kolr
																													from d_inet_book1@' || shoplink_p || ' a
																													inner join d_inet_book2@' || shoplink_p || ' b on a.serv_ip = b.serv_ip and a.posnr = b.posnr
																													where a.bit_status = ''T'' and b.bit_block = ''T'' and b.bit_sale = ''F''
																													and not (b.bit_move = ''T'' and b.rx_id is not null)
																													and (a.posnr,a.serv_ip,b.rnum) not in (select ''' || posnr_p || ''',''' || serv_ip_p || ''','||rnum_p||' from dual@' || shoplink_p || ')
																													group by b.art,b.asize) r on r.art = m.art and r.asize = m.asize) z on k.art = z.art and k.asize = z.asize)
																	order by id_stat)  
														where rownum = 1'
						into stat_p, id_stat_p, bit_move_p;
					if id_stat_p < 4 then
						insert into d_inet_book3
							(POSNR, DATE_S, COND, SERV_IP, ID, INIT,rnum)
							select distinct posnr_p, sysdate, stat_p||' '||SHOPIDIN, serv_ip_p, a.idm + 1, 'SHOP_SERVER',rnum_p
							from (select nvl(max(id), 0) idm
										 from d_inet_book3
										 where posnr = posnr_p
													 and serv_ip = serv_ip_p
													 and init = 'SHOP_SERVER') a;
						if count_p = 0 then
							update d_inet_book2
								set bit_send = 'T'
								where posnr = posnr_p
											and serv_ip = serv_ip_p
											and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
											and rnum = rnum_p
											and bit_send = 'F';
							
							update d_inet_book1 a
								set bit_send = (select nvl(min(b.bit_send),'F')
																from d_inet_book2 b
																where b.posnr = posnr_p
																and b.serv_ip = serv_ip_p)
								where 1 = 1
											and posnr = posnr_p
											and serv_ip = serv_ip_p;
                      
              if bit_move_p = 'T' then
                update d_inet_book2
                  set id_cancel = 20,date_cancel = sysdate
                  where posnr = posnr_p
                        and serv_ip = serv_ip_p
                        and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
                        and rnum = rnum_p
                        and bit_move = 'T';
                update d_inet_book4
                  set op_id_cancel = 20,bit_hyb_ready = 'F',bit_hyb_cancel = 'T',bit_hyb_receive = 'F'
                  where posnr = posnr_p
                        and serv_ip = serv_ip_p
                        and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
                        and rnum = rnum_p
                        and bit_move = 'T';
              end if;
							continue;
						end if;
					end if;
				
					--обновляю статус просмотра заказа магазином  
					execute immediate 'update d_inet_book4 a
														set a.date_rs = nvl((select max(b.date_rs) from d_inet_book1@' || shoplink_p || ' b where posnr = ''' ||
														posnr_p || ''' and serv_ip = ''' || serv_ip_p || '''),a.date_rs)
														where (a.posnr,a.serv_ip,a.rnum,case when is_hyb_serv_prod(a.serv_ip) = ''T'' then a.id_shop else ''S888'' end) in (select distinct posnr,serv_ip,rnum,id_shop
																																	from d_inet_book2_v
																																	where id_shop = '''||SHOPIDIN||''' and posnr = '''||posnr_p||''' and serv_ip = '''||serv_ip_p||''' and rnum = '||rnum_p||')';
					execute immediate 'update d_inet_book1 a
														set a.date_rs = (select date_rs from (select date_rs from d_inet_book4 b where posnr = ''' ||
														posnr_p || ''' and serv_ip = ''' || serv_ip_p || ''' and id_shop = '''||SHOPIDIN||''' and rnum = '||rnum_p||' order by date_rs desc) where rownum = 1)
														where a.posnr = ''' || posnr_p || ''' and a.serv_ip = ''' || serv_ip_p || '''';
														
					--по первой таблице delete/insert   
					execute immediate 'delete from d_inet_book1@' || shoplink_p || ' where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || '''';
					execute immediate 'insert into d_inet_book1@' || shoplink_p || ' (POSNR,DATED,PHONE_NUMBER,ID_DK,BIT_STATUS,BIT_SEND,BIT_ONLINE_PAY,FAM,IMA,OTCH,BIT_BOOK,DATE_RS,BOOK_NAME,SERV_IP,BIT_HYB_READY,DELIVERY_DATE,  BIT_DELIVERY_ADDR,DELIVERY_TOWN,DELIVERY_ADDRESS,DELIVERY_FAM,DELIVERY_IMA,DELIVERY_OTCH,DELIVERY_APPARTMENT,DELIVERY_BUILDING,DELIVERY_BLOCK)
														select POSNR,DATED,PHONE_NUMBER,ID_DK,BIT_STATUS,BIT_SEND,BIT_ONLINE_PAY,FAM,IMA,OTCH,BIT_BOOK,DATE_RS,BOOK_NAME,SERV_IP,BIT_HYB_READY,DELIVERY_DATE,  BIT_DELIVERY_ADDR,DELIVERY_TOWN,DELIVERY_ADDRESS,DELIVERY_FAM,DELIVERY_IMA,DELIVERY_OTCH,DELIVERY_APPARTMENT,DELIVERY_BUILDING,DELIVERY_BLOCK
														from d_inet_book1 where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || '''';
				
					--по второй таблице проверяю, есть ли в магазине данные по заказу. если нет, то просто вставляю. если есть, ничего не меняю, а забираю данные от магазина в центр.
					if count_p = 0 then
						select is_hyb_serv_prod(serv_ip_p)
							into is_hyb_serv_prod_p
							from dual;
						execute immediate 'insert into d_inet_book2@' || shoplink_p || ' (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,
																																							SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,DATE_CANCEL,PROMOCODE,PROMO_DISCOUNT)
														select POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,
																		SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,DATE_CANCEL,PROMOCODE,PROMO_DISCOUNT
														from d_inet_book2 where case when '''||is_hyb_serv_prod_p||''' = ''T'' then id_shop else ''S888'' end = '''||SHOPIDIN||''' and posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || ''' and rnum = '||rnum_p;
					else
						if count_f_p > 0 then
							execute immediate 'update d_inet_book2@'||shoplink_p||' a
																	set (a.id_shop_to,a.id_shop_from) = (select nvl(max(b.id_shop_to),a.id_shop_to),nvl(max(b.id_shop_from),a.id_shop_from)
																																				from d_inet_book2 b
																																				where b.bit_delete = ''F'' and b.bit_move = a.bit_move
																																				and a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum)
																	where a.bit_move = ''F'' and a.posnr = '''||posnr_p||''' and a.serv_ip = '''||serv_ip_p||''' and a.rnum = '||rnum_p;
						
							delete from d_inet_book2
							where posnr = posnr_p
										and serv_ip = serv_ip_p
										and rnum = rnum_p
										and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
										and bit_delete = 'F'
										and bit_move = 'F';
						end if;
						if count_t_p > 0 then
							delete from d_inet_book2
							where posnr = posnr_p
										and serv_ip = serv_ip_p
										and rnum = rnum_p
										and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
										and bit_delete = 'F'
										and bit_move = 'T';
						end if;
						execute immediate 'insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,rx_id,DATE_CANCEL,PROMOCODE,PROMO_DISCOUNT)
														select POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,rx_id,DATE_CANCEL,PROMOCODE,PROMO_DISCOUNT
														from d_inet_book2@' || shoplink_p || ' where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p || ''' and rnum = '||rnum_p;
					end if;
					
					--обновляю в магазине номер заказа DPD, дату отмены и штрихкод на перемещение
					execute immediate 'update d_inet_book2@'||shoplink_p||' a
															set a.dpd_id = (select max(b.dpd_id)
																							from d_inet_book4 b
																							where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum and b.bit_move = ''F''),
                                  a.date_cancel = nvl((select max(c.date_cancel)
																							from d_inet_book2_v c
																							where a.posnr = c.posnr and a.serv_ip = c.serv_ip and a.rnum = c.rnum and c.id_shop = '''||SHOPIDIN||'''),a.date_cancel),
                                  a.scan_move = case when id_shop_from = ''KI1000'' then 
                                                     nvl((select max(d.scan)
                                                     from d_inet_book_sap_status d
                                                     where a.posnr = d.posnr and a.serv_ip = d.serv_ip and a.rnum = d.rnum),a.scan_move)
                                                else
                                                     nvl((select max(p.scan)
                                                     from d_inet_book2 p
                                                     where a.posnr = p.posnr and a.serv_ip = p.serv_ip and a.rnum = p.rnum and p.bit_move = ''T'' and p.bit_delete = ''F''),a.scan_move)
                                                end
															where a.posnr = '''||posnr_p||''' and a.serv_ip = '''||serv_ip_p||''' and a.rnum = '||rnum_p;
					
					update d_inet_book2
					set bit_send = 'T'
					where posnr = posnr_p
								and serv_ip = serv_ip_p
								and rnum = rnum_p
								and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
								and bit_send = 'F';
				
					update d_inet_book1 a
					set bit_send = (select nvl(min(b.bit_send),'F')
													from d_inet_book2 b
													where b.posnr = posnr_p
													and b.serv_ip = serv_ip_p)
					where 1 = 1
								and posnr = posnr_p
								and serv_ip = serv_ip_p;
				
					insert into d_inet_book3
						(POSNR, DATE_S, COND, SERV_IP, ID, INIT,rnum)
						select distinct posnr_p, sysdate, 'ОБМЕН СОВЕРШЕН '||SHOPIDIN, serv_ip_p, a.idm + 1, 'SHOP_SERVER',rnum_p
						from (select nvl(max(id), 0) idm
									 from d_inet_book3
									 where posnr = posnr_p
												 and serv_ip = serv_ip_p
												 and init = 'SHOP_SERVER') a;
				
					execute immediate 'insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT,rnum)
														select posnr,date_s,cond||'' ''||'''||SHOPIDIN||''',serv_ip,idm+rownum,''SHOP'',rnum
														from (select a.POSNR,a.DATE_S,a.COND,a.SERV_IP,b.idm,a.rnum
																	from d_inet_book3@' || shoplink_p || ' a
																	inner join (select nvl(max(id),0) idm,nvl(max(case when cond like ''%''||'''||SHOPIDIN||'''||''%'' then date_s else null end),to_date(''01012000'',''ddmmyyyy'')) datem
																							from d_inet_book3
																							where posnr = ''' || posnr_p || ''' and serv_ip = ''' || serv_ip_p ||
														''' and init = ''SHOP'') b on 1 = 1
																	where a.posnr = ''' || posnr_p || ''' and a.serv_ip = ''' || serv_ip_p ||
														''' and a.date_s > b.datem
																	order by a.date_s)';
				
					BOOK_SOLD(posnr_p, serv_ip_p);
          BOOK_SHOP_CANCELLED(posnr_p, serv_ip_p);
				end;
			end loop;
			--обмен по одному заказу
		else 
		
			--ставлю флаг отправки в false, если в первой таблице false		
			update d_inet_book2 a
						set a.bit_send = 'F'
						where a.posnr = posnrin and a.serv_ip = serv_ipin
						and case when is_hyb_serv_prod(a.serv_ip) = 'T' then a.id_shop else 'S888' end = SHOPIDIN
						and (a.posnr,a.serv_ip) in (select b.posnr,b.serv_ip
																				from d_inet_book1 b
																				where b.bit_send = 'F' and a.posnr = b.posnr and a.serv_ip = b.serv_ip);
		
			--удаляю из магазина данные по заказам на перемещение, если в центре по ним стоит метка удаления			
			execute immediate 'delete from d_inet_book2@' || shoplink_p || ' a
													where (a.POSNR,a.ID_SHOP,a.SERV_IP,a.RNUM) in (select b.POSNR,b.ID_SHOP,b.SERV_IP,b.RNUM
																																					from d_inet_book2 b
																																					where b.bit_send = ''F'' and b.bit_delete = ''T'' and b.bit_move = ''T''
																																					and b.POSNR = '''||posnrin||''' and a.ID_SHOP = b.id_shop
																																					and b.SERV_IP = '''||serv_ipin||''' and a.RNUM = b.rnum)';	
			execute immediate 'delete from d_inet_book1@' || shoplink_p || ' a
													where (a.POSNR,a.SERV_IP) in (select distinct b.POSNR,b.SERV_IP
																																					from d_inet_book2 b
																																					where b.bit_send = ''F'' and b.bit_delete = ''T'' and b.bit_move = ''T''
																																					and b.POSNR = '''||posnrin||''' and b.id_shop = '''||SHOPIDIN||'''
																																					and b.SERV_IP = '''||serv_ipin||''')
													and (select count(*)
																			from d_inet_book2@'||shoplink_p||'
																			where posnr = '''||posnrin||''' and serv_ip = '''||serv_ipin||''') = 0';	
			update d_inet_book2 b
				set bit_send = 'T'
				where b.bit_send = 'F' and b.bit_delete = 'T' and b.bit_move = 'T'
				and b.POSNR = posnrin 
				and case when is_hyb_serv_prod(b.serv_ip) = 'T' then b.id_shop else 'S888' end = SHOPIDIN
				and b.SERV_IP = serv_ipin;
				
			if cur_posnrin is null then
				update d_inet_book1 a
					set bit_send = (select nvl(min(b.bit_send),'F')
													from d_inet_book2 b
													where b.posnr = posnrin
													and b.serv_ip = serv_ipin)
					where 1 = 1
								and posnr = posnrin
								and serv_ip = serv_ipin;
			end if;
			
			select nvl(min(b.bit_send),'F') into result
				from d_inet_book2_v b
				where b.posnr = posnrin
				and b.serv_ip = serv_ipin;
			
			commit;

			for cur_book in (select distinct rnum
												from d_inet_book2_v
												where posnr = posnrin and serv_ip = serv_ipin
												and id_shop = SHOPIDIN and bit_send = 'F') loop
				begin
				
					--проверка на наличие заказа в магазине, чтобы знать, нужно ли в нем обновлять данные или этот заказ в магазин вообще не попал
					execute immediate 'select count(*),sum(case when bit_move = ''F'' then 1 else 0 end),sum(case when bit_move = ''T'' then 1 else 0 end)
															into :i,:j,:k from d_inet_book2@' || shoplink_p || ' 
															where posnr = ''' || posnrin || ''' 
															and serv_ip = ''' || serv_ipin || '''
															and rnum = '||cur_book.rnum
						into count_p,count_f_p,count_t_p;
						
					--если в центре стоит метка отката отмены позиции заказа, то убираем отмену в магазине
					select nvl(max(bit_cancel_reverse),'F') into bit_cancel_reverse_p
						from d_inet_book2_v
						where id_shop = SHOPIDIN and posnr = posnrin and serv_ip = serv_ipin and rnum = cur_book.rnum and bit_move = 'F';
						
					if bit_cancel_reverse_p = 'T' and count_f_p > 0 then
						execute immediate 'update d_inet_book2@'||shoplink_p||'
																set date_cancel = null,bit_block = ''T'',id_cancel = null
																where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || '''
															and rnum = '||cur_book.rnum;
						execute immediate 'insert into d_inet_book3@'||shoplink_p||' (POSNR,SERV_IP,COND,RNUM)
																select '''||posnrin||''','''||serv_ipin||''',''ПОЗИЦИЯ ЗАКАЗА ПЕРЕРЕЗЕРВИРОВАНА МАГАЗИНОМ'','||cur_book.rnum||'
																from dual';
						update d_inet_book2
							set bit_cancel_reverse = 'F'
							where id_shop = SHOPIDIN and posnr = posnrin and serv_ip = serv_ipin and rnum = cur_book.rnum and bit_move = 'F';
					end if;
          
          --если в центре стоит метка отмены позиции заказа, то отменяем ее в магазине
					select nvl(max(bit_cancel_firm),'F') into bit_cancel_firm_p
						from d_inet_book2_v
						where id_shop = SHOPIDIN and posnr = posnrin and serv_ip = serv_ipin and rnum = cur_book.rnum;
						
					if bit_cancel_firm_p = 'T' and count_p > 0 then
						execute immediate 'update d_inet_book2@'||shoplink_p||'
																set date_cancel = sysdate,bit_block = ''F'',id_cancel = (select max(op_id_cancel) 
                                                                                                from d_inet_book4
                                                                                                where posnr = ''' || posnrin || '''
                                                                                                and serv_ip = ''' || serv_ipin || '''
															                                                                  and rnum = '||cur_book.rnum||')
																where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || '''
															and rnum = '||cur_book.rnum;
						update d_inet_book2
							set bit_cancel_firm = 'F'
							where id_shop = SHOPIDIN and posnr = posnrin and serv_ip = serv_ipin and rnum = cur_book.rnum;
					end if;
          
          select nvl(max(art),' '),nvl(max(asize),0) into art_p,asize_p
            from d_inet_book2
            where posnr = posnrin and serv_ip = serv_ipin and rnum = cur_book.rnum and bit_move = 'F';
					
					--проверка на остатки
					execute immediate 'select nvl(max(stat),''ПУСТОЙ ЗАКАЗ'') stat,nvl(max(id_stat),0) id_stat,nvl(min(bit_move),''F'') bit_move
															from (select bit_move,
																		case when bit_book = ''F'' and bit_move = ''F'' then ''ОК''
																		when bit_book = ''F'' and bit_move = ''T'' and bit_accept = ''T'' then ''ОК''
																		when kol <= 0 or kol < kolz then ''НЕТ В НАЛИЧИИ''
																		when kol <= kolb then ''В НАЛИЧИИ ТОЛЬКО БРАК''
																		when kol - kolb <= kolr then ''В НАЛИЧИИ ТОЛЬКО БРОНЬ''
																		else ''ОК'' end stat,
																		case when bit_book = ''F'' and bit_move = ''F'' then 4
																		when bit_book = ''F'' and bit_move = ''T'' and bit_accept = ''T'' then 4
																		when kol <= 0 or kol < kolz then 1
																		when kol <= kolb then 2
																		when kol - kolb <= kolr then 3
																		else 4 end id_stat
																		from (select k.bit_accept,k.bit_move,k.bit_book,k.rnum,k.kolz,nvl(z.kol,0) kol,nvl(z.kolb,0) kolb,nvl(z.kolr,0) kolr
																		from (select b.bit_accept,a.bit_book,b.bit_move,b.rnum,b.art,b.asize,sum(b.kol) kolz
																					from (select ''' || posnrin || ''' posnr,''' || serv_ipin || ''' serv_ip,'''||bit_bookin||''' bit_book from dual) a
																					inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
																					where b.rnum = '||cur_book.rnum||' and a.posnr = ''' || posnrin || ''' and a.serv_ip = ''' || serv_ipin || ''' and b.id_shop = '''||SHOPIDIN||''' 
																					--and (a.bit_book = ''T'' or (b.bit_move = ''T'' and b.bit_delete = ''F'') or (b.bit_move = ''F'' and (b.id_shop_from is null or nvl(b.id_shop_from,''null'') = nvl(b.id_shop_to,''null''))))
																					group by b.bit_accept,a.bit_book,b.bit_move,b.rnum,b.art,b.asize) k
																		left join (select m.art,m.asize,m.kol,m.kolb,nvl(r.kolr,0) kolr
																								from (select q.art,q.asize,sum(q.kol) kol,
																												sum(case when p.scan is null then 0 else 1 end) kolb
																												from (select art,asize,scan,sum(kol) kol
																															from (select x.art,x.asize,x.scan,
																																	sum(x.kol) kol
																																	from e_osttek@' || shoplink_p || ' x
																																	where x.procent = 0 and x.art = '''||art_p||''' and x.asize = '||asize_p||'
																																	group by x.art,x.asize,x.scan
																																	having sum(x.kol) > 0
																																	union all
																																	select art,asize,scan,kol
																																	from d_inet_book2@' || shoplink_p || '
																																	where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || ''' and bit_sale = ''T'')
																															group by art,asize,scan) q
																												left join (select art,asize,scan,1 kolb
																																		from pos_brak@' || shoplink_p || ') p on q.scan = p.scan
																												group by q.art,q.asize) m
																								left join (select b.art,b.asize,sum(b.kol) kolr
																														from d_inet_book1@' || shoplink_p || ' a
																														inner join d_inet_book2@' || shoplink_p || ' b on a.serv_ip = b.serv_ip and a.posnr = b.posnr
																														where a.bit_status = ''T'' and b.bit_block = ''T'' and b.bit_sale = ''F''
																														and not (b.bit_move = ''T'' and b.rx_id is not null)
																														and (a.posnr,a.serv_ip,b.rnum) not in (select ''' || posnrin || ''',''' || serv_ipin ||	''','||cur_book.rnum||' from dual@' || shoplink_p || ')
																														group by b.art,b.asize) r on r.art = m.art and r.asize = m.asize) z on k.art = z.art and k.asize = z.asize)
																		order by id_stat)  
															where rownum = 1'
						into stat_p, id_stat_p, bit_move_p;
		
					if id_stat_p < 4 then
						insert into d_inet_book3
							(POSNR, DATE_S, COND, SERV_IP, ID, INIT,rnum)
							select distinct posnrin, sysdate, stat_p||' '||SHOPIDIN, serv_ipin, a.idm + 1, 'SHOP_SERVER',cur_book.rnum
							from (select nvl(max(id), 0) idm
										 from d_inet_book3
										 where posnr = posnrin
													 and serv_ip = serv_ipin
													 and init = 'SHOP_SERVER') a;
						if count_p = 0 then							
							update d_inet_book2
								set bit_send = 'T'
								where posnr = posnrin
											and serv_ip = serv_ipin
											and rnum = cur_book.rnum
											and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
											and bit_send = 'F';		
                      
              if bit_move_p = 'T' then
                update d_inet_book2
                  set id_cancel = 20,date_cancel = sysdate
                  where posnr = posnrin
                      and serv_ip = serv_ipin
                      and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
                      and rnum = cur_book.rnum
                      and bit_move = 'T';
                update d_inet_book4
                  set op_id_cancel = 20,bit_hyb_ready = 'F',bit_hyb_cancel = 'T',bit_hyb_receive = 'F'
                  where posnr = posnrin
                      and serv_ip = serv_ipin
                      and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
                      and rnum = cur_book.rnum
                      and bit_move = 'T';
              end if;			
						end if;
					end if;
				end;
			end loop;
			
			if cur_posnrin is null then
				update d_inet_book1 a
					set bit_send = (select nvl(min(b.bit_send),'F')
													from d_inet_book2 b
													where b.posnr = posnrin
													and b.serv_ip = serv_ipin)
					where 1 = 1
								and posnr = posnrin
								and serv_ip = serv_ipin;
			end if;
			
			select nvl(min(b.bit_send),'F') into result
				from d_inet_book2_v b
				where b.posnr = posnrin
				and b.serv_ip = serv_ipin;
			
			select count(*) into send_count_p
				from d_inet_book2_v b
				where b.posnr = posnrin
				and b.serv_ip = serv_ipin
				and b.id_shop = SHOPIDIN
				and b.bit_send = 'F';
			
			if send_count_p = 0 then
				commit;
        begin
          commit;
          DBMS_SESSION.CLOSE_DATABASE_LINK(shoplink_p);
          commit;

          EXCEPTION
            WHEN OTHERS THEN
            --WRITE_ERROR_PROC('KAA_INET_BOOK_EXCHANGE', sqlcode, sqlerrm, 'SHOPIDIN = ' || SHOPIDIN, 'link_close', ' ');
            rollback;
          end;
				return;
			end if;
					
			--//loop begin
			for cur_book in (select distinct rnum
												from d_inet_book2_v
												where posnr = posnrin and serv_ip = serv_ipin
												and id_shop = SHOPIDIN and bit_send = 'F') loop
				begin
				
					--проверка на наличие заказа в магазине, чтобы знать, нужно ли в нем обновлять данные или этот заказ в магазин вообще не попал
					execute immediate 'select count(*),sum(case when bit_move = ''F'' then 1 else 0 end),sum(case when bit_move = ''T'' then 1 else 0 end)
															into :i,:j,:k from d_inet_book2@' || shoplink_p || ' 
															where posnr = ''' || posnrin || ''' 
															and serv_ip = ''' || serv_ipin || '''
															and rnum = '||cur_book.rnum
						into count_p,count_f_p,count_t_p;
						
					--обновляю статус просмотра заказа магазином  
					execute immediate 'update d_inet_book4 a
														set a.date_rs = nvl((select max(b.date_rs) from d_inet_book1@' || shoplink_p || ' b where posnr = ''' ||
														posnrin || ''' and serv_ip = ''' || serv_ipin || '''),a.date_rs)
														where (a.posnr,a.serv_ip,a.rnum,case when is_hyb_serv_prod(a.serv_ip) = ''T'' then a.id_shop else ''S888'' end) in (select distinct posnr,serv_ip,rnum,id_shop
																																	from d_inet_book2_v
																																	where id_shop = '''||SHOPIDIN||''' and posnr = '''||posnrin||''' and serv_ip = '''||serv_ipin||''' and rnum = '||cur_book.rnum||')';
					execute immediate 'update d_inet_book1 a
														set a.date_rs = (select date_rs from (select date_rs from d_inet_book4 b where posnr = ''' ||
														posnrin || ''' and serv_ip = ''' || serv_ipin || ''' and id_shop = '''||SHOPIDIN||''' and rnum = '||cur_book.rnum||' order by date_rs desc) where rownum = 1)
														where a.posnr = ''' || posnrin || ''' and a.serv_ip = ''' || serv_ipin || '''';
														
					--по первой таблице delete/insert
					--если вызов из триггера и записи в d_inet_book1 еще нет, то передаем данные из курсора
					if cur_posnrin is not null then
		
						loop
							fetch cur_posnrin
								into cur_rec;
							exit when cur_posnrin%notfound;
							begin						
							
								execute immediate 'delete from d_inet_book1@' || shoplink_p || ' where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || '''';
								execute immediate 'insert into d_inet_book1@' || shoplink_p || ' (POSNR,DATED,PHONE_NUMBER,ID_DK,BIT_STATUS,BIT_SEND,BIT_ONLINE_PAY,FAM,IMA,OTCH,BIT_BOOK,DATE_RS,BOOK_NAME,SERV_IP,BIT_HYB_READY,DELIVERY_DATE,  BIT_DELIVERY_ADDR,DELIVERY_TOWN,DELIVERY_ADDRESS,DELIVERY_FAM,DELIVERY_IMA,DELIVERY_OTCH,DELIVERY_APPARTMENT,DELIVERY_BUILDING,DELIVERY_BLOCK)
																		select ''' || cur_rec.posnr || ''',to_date(''' || to_char(cur_rec.DATED, 'dd.MM.yyyy HH24:mi:ss') ||
																	''',''dd.MM.yyyy HH24:mi:ss''),''' || cur_rec.PHONE_NUMBER || ''',''' || cur_rec.ID_DK || ''',
																		''' || cur_rec.BIT_STATUS || ''',''' || cur_rec.BIT_SEND || ''',''' || cur_rec.BIT_ONLINE_PAY || ''',
																		''' || cur_rec.FAM || ''',''' || cur_rec.IMA || ''',''' || cur_rec.OTCH || ''',''' || cur_rec.BIT_BOOK ||
																	''',to_date(''' || to_char(cur_rec.DATE_RS, 'dd.MM.yyyy') || ''',''dd.MM.yyyy''),
																		''' || cur_rec.BOOK_NAME || ''',''' || cur_rec.SERV_IP || ''',''' || cur_rec.BIT_HYB_READY || ''',
                                    to_date(''' || to_char(cur_rec.DELIVERY_DATE, 'dd.MM.yyyy') || ''',''dd.MM.yyyy''),''' || cur_rec.BIT_DELIVERY_ADDR || ''',
                                    ''' || cur_rec.DELIVERY_TOWN || ''',''' || cur_rec.DELIVERY_ADDRESS || ''',''' || cur_rec.DELIVERY_FAM || ''',
                                    ''' || cur_rec.DELIVERY_IMA || ''',''' || cur_rec.DELIVERY_OTCH || ''',''' || cur_rec.DELIVERY_APPARTMENT || ''',
                                    ''' || cur_rec.DELIVERY_BUILDING || ''',''' || cur_rec.DELIVERY_BLOCK || '''
																		from dual';
							end;
						end loop;
					else
							
						execute immediate 'delete from d_inet_book1@' || shoplink_p || ' where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || '''';
						execute immediate 'insert into d_inet_book1@' || shoplink_p || ' (POSNR,DATED,PHONE_NUMBER,ID_DK,BIT_STATUS,BIT_SEND,BIT_ONLINE_PAY,FAM,IMA,OTCH,BIT_BOOK,DATE_RS,BOOK_NAME,SERV_IP,BIT_HYB_READY,DELIVERY_DATE,  BIT_DELIVERY_ADDR,DELIVERY_TOWN,DELIVERY_ADDRESS,DELIVERY_FAM,DELIVERY_IMA,DELIVERY_OTCH,DELIVERY_APPARTMENT,DELIVERY_BUILDING,DELIVERY_BLOCK)
																	select POSNR,DATED,PHONE_NUMBER,ID_DK,BIT_STATUS,BIT_SEND,BIT_ONLINE_PAY,FAM,IMA,OTCH,BIT_BOOK,DATE_RS,BOOK_NAME,SERV_IP,BIT_HYB_READY,DELIVERY_DATE,  BIT_DELIVERY_ADDR,DELIVERY_TOWN,DELIVERY_ADDRESS,DELIVERY_FAM,DELIVERY_IMA,DELIVERY_OTCH,DELIVERY_APPARTMENT,DELIVERY_BUILDING,DELIVERY_BLOCK 
																	from d_inet_book1 where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || '''';
					end if;
				
					--по второй таблице проверяю, есть ли в магазине данные по заказу. если нет, то просто вставляю. если есть, ничего не меняю, а забираю данные от магазина в центр.    
					if count_p = 0 then
						select is_hyb_serv_prod(serv_ipin)
							into is_hyb_serv_prod_p
							from dual;
						execute immediate 'insert into d_inet_book2@' || shoplink_p || ' (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,
																																							SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,rx_id,DATE_CANCEL,PROMOCODE,PROMO_DISCOUNT)
															select POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,
																			SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,rx_id,DATE_CANCEL,PROMOCODE,PROMO_DISCOUNT
															from d_inet_book2 where case when '''||is_hyb_serv_prod_p||''' = ''T'' then id_shop else ''S888'' end = '''||SHOPIDIN||''' and posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || ''' and rnum = '||cur_book.rnum;
					else
						if count_f_p > 0 then
							execute immediate 'update d_inet_book2@'||shoplink_p||' a
																	set (a.id_shop_to,a.id_shop_from) = (select nvl(max(b.id_shop_to),a.id_shop_to),nvl(max(b.id_shop_from),a.id_shop_from)
																																				from d_inet_book2 b
																																				where b.bit_delete = ''F'' and b.bit_move = a.bit_move
																																				and a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum)
																	where a.bit_move = ''F'' and a.posnr = '''||posnrin||''' and a.serv_ip = '''||serv_ipin||''' and a.rnum = '||cur_book.rnum;
						
							delete from d_inet_book2 a
							where a.posnr = posnrin
										and a.serv_ip = serv_ipin
										and rnum = cur_book.rnum
										and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
										and bit_delete = 'F'
										and bit_move = 'F';
						end if;		
						if count_t_p > 0 then
							delete from d_inet_book2 a
							where a.posnr = posnrin
										and a.serv_ip = serv_ipin
										and rnum = cur_book.rnum
										and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
										and bit_delete = 'F'
										and bit_move = 'T';
						end if;	
						execute immediate 'insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,DATE_CANCEL,rx_id,PROMOCODE,PROMO_DISCOUNT)
															select POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,SCAN,BIT_BLOCK,ID_CANCEL,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT,DATE_CANCEL,rx_id,PROMOCODE,PROMO_DISCOUNT
															from d_inet_book2@' || shoplink_p || ' where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin || ''' and rnum = '||cur_book.rnum;
					end if;
					
					--обновляю в магазине номер заказа DPD, дату отмены и шк перемещения
					execute immediate 'update d_inet_book2@'||shoplink_p||' a
															set a.dpd_id = (select max(b.dpd_id)
																							from d_inet_book4 b
																							where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum and b.bit_move = ''F''),
                                  a.date_cancel = nvl((select max(c.date_cancel)
																							from d_inet_book2_v c
																							where a.posnr = c.posnr and a.serv_ip = c.serv_ip and a.rnum = c.rnum and c.id_shop = '''||SHOPIDIN||'''),a.date_cancel),
                                  a.scan_move = case when id_shop_from = ''KI1000'' then 
                                                     nvl((select max(d.scan)
                                                     from d_inet_book_sap_status d
                                                     where a.posnr = d.posnr and a.serv_ip = d.serv_ip and a.rnum = d.rnum),a.scan_move)
                                                else
                                                     nvl((select max(p.scan)
                                                     from d_inet_book2 p
                                                     where a.posnr = p.posnr and a.serv_ip = p.serv_ip and a.rnum = p.rnum and p.bit_move = ''T'' and p.bit_delete = ''F''),a.scan_move)
                                                end
															where a.posnr = '''||posnrin||''' and a.serv_ip = '''||serv_ipin||''' and a.rnum = '||cur_book.rnum;
					
					commit;
				
					if cur_posnrin is null then
						--если вызов из триггера, то update bit_send не делаю, так как он идет в триггере
						update d_inet_book2
							set bit_send = 'T'
							where posnr = posnrin
										and serv_ip = serv_ipin
										and rnum = cur_book.rnum
										and case when is_hyb_serv_prod(serv_ip) = 'T' then id_shop else 'S888' end = SHOPIDIN
										and bit_send = 'F';
						
						update d_inet_book1 a
							set bit_send = (select nvl(min(b.bit_send),'F')
															from d_inet_book2 b
															where b.posnr = posnrin
															and b.serv_ip = serv_ipin)
							where 1 = 1
										and posnr = posnrin
										and serv_ip = serv_ipin;
					end if;
				
					insert into d_inet_book3
						(POSNR, DATE_S, COND, SERV_IP, ID, INIT,rnum)
						select distinct posnrin, sysdate, 'ОБМЕН СОВЕРШЕН '||SHOPIDIN, serv_ipin, a.idm + 1, 'SHOP_SERVER',cur_book.rnum
						from (select nvl(max(id), 0) idm
									 from d_inet_book3
									 where posnr = posnrin
												 and serv_ip = serv_ipin
												 and init = 'SHOP_SERVER') a;
				
					execute immediate 'insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT,rnum)
																select posnr,date_s,cond||'' ''||'''||SHOPIDIN||''',serv_ip,idm+rownum,''SHOP'',rnum
																from (select a.POSNR,a.DATE_S,a.COND,a.SERV_IP,b.idm,a.rnum
																			from d_inet_book3@' || shoplink_p || ' a
																			inner join (select nvl(max(id),0) idm,nvl(max(case when cond like ''%''||'''||SHOPIDIN||'''||''%'' then date_s else null end),to_date(''01012000'',''ddmmyyyy'')) datem
																									from d_inet_book3
																									where posnr = ''' || posnrin || ''' and serv_ip = ''' || serv_ipin ||
														''' and init = ''SHOP'') b on 1 = 1
																			where a.posnr = ''' || posnrin || ''' and a.serv_ip = ''' || serv_ipin || ''' and a.date_s > b.datem
																			order by a.date_s)';
				end;
			end loop;
			--//loop end
			
			if cur_posnrin is null then
				BOOK_SOLD(posnrin, serv_ipin);
        BOOK_SHOP_CANCELLED(posnrin, serv_ipin);
			end if;
		end if;
	
		select nvl(min(b.bit_send),'F') into result
			from d_inet_book2 b
			where b.posnr = posnrin
			and b.serv_ip = serv_ipin;
		commit;
	
	exception
		when others then
			dbms_output.put_line(sqlcode || sqlerrm);
			rollback;
      if to_char(sqlcode) not in ('-12170','-12543','-12541','-60','-12505','-2049','-1001') then
			  WRITE_ERROR_PROC('KAA_INET_BOOK_EXCHANGE', sqlcode, sqlerrm, 'SHOPIDIN = ' || SHOPIDIN, ' ', ' ');
      end if;
			if posnrin is null then
				close book;
				open book; --прохожу по всем неотправленным заказам
				loop
					fetch book
						into posnr_p, serv_ip_p, rnum_p;
					exit when book%notfound;
					begin
						insert into d_inet_book3
							(POSNR, DATE_S, COND, SERV_IP, ID, INIT,rnum)
							select distinct posnr_p, sysdate, 'ОШИБКА ОБМЕНА '||SHOPIDIN, serv_ip_p, a.idm + 1, 'SHOP_SERVER',rnum_p
							from (select nvl(max(id), 0) idm
										 from d_inet_book3
										 where posnr = posnr_p
													 and serv_ip = serv_ip_p
													 and init = 'SHOP_SERVER') a;
					end;
				end loop;
				commit;
			else
				insert into d_inet_book3
					(POSNR, DATE_S, COND, SERV_IP, ID, INIT,rnum)
					select distinct posnrin, sysdate, 'ОШИБКА ОБМЕНА '||SHOPIDIN, serv_ipin, a.idm + 1, 'SHOP_SERVER',b.rnum
					from (select nvl(max(id), 0) idm
								 from d_inet_book3
								 where posnr = posnrin
											 and serv_ip = serv_ipin
											 and init = 'SHOP_SERVER') a
					inner join d_inet_book2 b on b.posnr = posnrin and b.serv_ip = serv_ipin;
				commit;
			end if;
			result := 'F';
      
      begin
        commit;
        DBMS_SESSION.CLOSE_DATABASE_LINK(shoplink_p);
        commit;

        EXCEPTION
          WHEN OTHERS THEN
          --WRITE_ERROR_PROC('KAA_INET_BOOK_EXCHANGE', sqlcode, sqlerrm, 'SHOPIDIN = ' || SHOPIDIN, 'link_close', ' ');
          rollback;
      end;
		
	end BOOK_EXCHANGE;
	--END KAA_INET_BOOK_EXCHANGE

	--****************************************************************************************************************************************************
	-- Author:  Tyshevich DV
	-- Created: 14.03.2018 8:52:54
	-- Comments:
	--    МАТРИЦА ДОСТАВКИ
	--   расчет пунктов откуда везти товар
	--**************************************************************************************************************************************************** 
	procedure run_move_matrix(i_posnr       in varchar2, -- номер заказа
														i_serv_ip     in varchar2, -- IP сервера
														i_through_sgp in varchar2 default 'F') -- обязательно ли везти через СГП 
	 is
	
		v_id_shop_to d_inet_book2.id_shop%type;
		v_order_complete varchar2(1) := 'F';
		v_not_ost_flag varchar2(1) := 'F';
	  v_bit_delivery_addr d_inet_book1.bit_delivery_addr%type;
    v_landid hybris.st_server_for_servlet.landid%type;
  
		C_CODE_NOT_OST constant varchar2(10) := 'NOT_OST';
	
	begin
    
    -- определяю страну заказа
    select landid
    into v_landid
    from hybris.st_server_for_servlet
    where server = i_serv_ip  ;  
  
	  -- обновляю данные по товарам на СГП, для данного заказа
    GET_FREE_STOCK('select distinct matnr
      from d_inet_book2 a
      inner join s_all_mat b on a.art = b.art and a.asize = b.asize and b.trademark_sat != ''003''
      where a.posnr = '''||i_posnr||'''
            and a.serv_ip = '''||i_serv_ip||'''
            and a.bit_move = ''F'' ');
  
    -- определяю Самовывоз или Доставка на адрес
    select bit_delivery_addr
    into v_bit_delivery_addr
    from d_inet_book1 a
    where a.posnr = i_posnr
          and a.serv_ip = i_serv_ip;    
  
    if v_bit_delivery_addr = 'F' then
      -- магазин получатель
      select max(id_shop)
      into v_id_shop_to
      from d_inet_book2 a
      where a.posnr = i_posnr
            and a.serv_ip = i_serv_ip
            and a.bit_move = 'F';
    else
      v_id_shop_to := null;
    end if;        

    write_log('KAA_INET_BOOK', 'run_move_matrix', 0, i_posnr, i_serv_ip, i_through_sgp, 'расчет заказа ' , 
                case when v_bit_delivery_addr = 'F' then  'самовывоз из магазина '||v_id_shop_to else 'доставка на адрес' end);
	
		delete from tem_inet_move_matrix_result; -- итог расчета
		delete from tem_inet_move_matrix_ost; -- текущие отатки магазинов и СГП по артикулам из заказа
		delete from tem_inet_move_matrix_book; -- суммированный текущий заказ
	
		insert into tem_inet_move_matrix_book
			select a.art, a.asize, sum(a.kol) kol
			from d_inet_book2 a
			where a.posnr = i_posnr
						and a.serv_ip = i_serv_ip
						and a.bit_move = 'F'
			group by a.art, a.asize;

    -- сохраняю в лог , все что было в e_osttek_online на момент расчета {
   for r_row in (select id_shop, x. art, x.asize, sum(kol) kol
                    from e_osttek_online x
                    inner join (select distinct art, asize
                                from d_inet_book2 a
                                where a.posnr =i_posnr
                                      and a.serv_ip = i_serv_ip
                                      and a.bit_move = 'F') a on a.art = x.art
                                                                 and a.asize = x.asize
                    where id_shop in (select shopid from st_shop y where  y.org_kod = 'SHP' and bit_open = 'T')         --y.landid = 'BY' and                                     
                    group by id_shop, x. art, x.asize
                    having sum(kol) > 0)  loop
                    
      write_log('KAA_INET_BOOK', 'run_move_matrix', 0, i_posnr, i_serv_ip, i_through_sgp, 'было в e_osttek_online' ,r_row.id_shop||', '||r_row.art||', '||r_row.asize||', '||r_row.kol);                
    end loop;
	  -- }
  
  
    insert into tem_inet_move_matrix_ost
      select x.id_shop, x.art, x.asize, sum(x.kol) - nvl(b.kol, 0) - nvl(zp.kol, 0) kol
      from e_osttek_online x
      inner join tem_inet_move_matrix_book a on a.art = x.art
                                                and a.asize = x.asize
      inner join (select shopid
                  from st_shop y
                  where  y.org_kod = 'SHP' --landid = 'BY' 
                        and y.bit_open = 'T') y on x.id_shop = y.shopid
      left join pos_brak s on s.scan = x.scan
                              and s.id_shop = x.id_shop
      left join (select b.id_shop, b.art, b.asize, sum(b.kol) kol --бронь
                 from pos_reserve_v b 
                 where not (b.serv_ip = i_serv_ip and b.posnr = i_posnr)
                 group by b.id_shop, b.art, b.asize) b on x.id_shop = b.id_shop
                                                          and x.art = b.art
                                                          and x.asize = b.asize
    left join (select a.id_shop, art, asize, sum(kol) kol -- в счет ЗП, в незакрытом документе
               from d_rasxod1 a
               inner join d_rasxod2 b on a.id_shop = b.id_shop
                                         and a.id = b.id
               where idop = '35'
                     and bit_close = 'F'
                     and a.id_shop in (select shopid
                                       from st_shop x
                                       where x.org_kod = 'SHP')
               group by a.id_shop, art, asize) zp on zp.id_shop = x.id_shop
                                                     and zp.art = x.art
                                                     and zp.asize = x.asize                                                          
      where x.procent = 0 -- искл. некондицию
            and s.art is null -- искл. брак
            -- беру только магазины города Витебска и Магазин Самовывоза:
            and x.id_shop in (select shopid 
                                   from hyb_shop_stock_rb z 
                                   where landid = v_landid or landid is null -- если это заказ РФ, то не забираем с Витебских магазинов
                                   union 
                                   select shopid 
                                   from s_shop z
                                   where v_id_shop_to is not null and z.shopid = v_id_shop_to )
      group by x.id_shop, x.art, x.asize, b.kol, zp.kol
      having sum(x.kol) - nvl(b.kol, 0) - nvl(zp.kol, 0) > 0
      
      union all
      
      select C_CODE_SGP id_shop, x.art, x.asize, sum(x.verme) kol
      from rfc_free_stock x -- остатки СГП
      inner join tem_inet_move_matrix_book a on a.art = x.art
                                                and a.asize = x.asize
      group by x.art, x.asize
      having sum(x.verme) > 0;
	

      -- сохраняю всё что выбрал в ЛОГ
      for r_row in (select * from tem_inet_move_matrix_ost) loop
        write_log('KAA_INET_BOOK', 'run_move_matrix', 0, i_posnr, i_serv_ip, i_through_sgp, 'наличие в' ,r_row.id_shop||', '||r_row.art||', '||r_row.asize||', '||r_row.kol);
      end loop;  

		/*
    TODO: owner="asus" category="Optimize" priority="2 - Medium" created="13.03.2018" closed="13.03.2018"
    text="для ускорения можно сразу выбрать во временную таблицу из е_остек_онлайн только те позиции,
          которые есть в заказе. и дальше работать только с этой небольшой таблицей. тоже самое можно сделать и с остатками СГП. и
          затем объеденить это в одну таблицу"
    */
		-- 1. проверяю , могу ли я собрать заказ целиком (только для магазина САМОВЫВОЗА)
    if v_id_shop_to is not null then
      for r_all_order in (select b.id_shop, sum(decode(b.art, null, 1, 0)) all_order, sum(b.kol) kol, c.reit2
                          from tem_inet_move_matrix_book a
                          inner join (select *
                                     from tem_inet_move_matrix_ost) b on a.art = b.art
                                                                         and a.asize = b.asize
                                                                         and a.kol <= b.kol
                          left join tdv_map_shop_reit c on b.id_shop = c.id_shop, (select count(*) cnt
                                                            from tem_inet_move_matrix_book) d
                          where b.id_shop = v_id_shop_to   --(только для магазина САМОВЫВОЗА)                           
                          group by b.id_shop, c.reit2, d.cnt
                          having count(*) = d.cnt -- только если могу собрать весь заказ
/*                          order by -- 1. если весь заказ находится в пункте самовывоза
                                    decode(v_id_shop_to, b.id_shop, 0, 1),
                                   -- 2. если весь заказ на СГП
                                   decode(b.id_shop, C_CODE_SGP, 0, 1),
                                   -- 3. весь заказ на магазине в котором больше всего пар
                                   sum(b.kol) desc,
                                   -- 4. магазин с худшим рейтингом
                                   nvl(c.reit2, 1000) desc*/
                                   )
      loop
        for r_order_row in (select *
                            from tem_inet_move_matrix_book)
        loop
          insert into tem_inet_move_matrix_result
          values
            (i_posnr, r_order_row.art, r_order_row.asize, r_order_row.kol, r_all_order.id_shop, v_id_shop_to);
        end loop;
  		
        v_order_complete := 'T';
  		
        exit;
      end loop;
  	
      if v_order_complete = 'T' then
        commit;
        write_log('KAA_INET_BOOK', 'run_move_matrix', 0, i_posnr, i_serv_ip, i_through_sgp, 'заказ собран целиком на магазине Самовывоза' );
        return;
      end if;
		end if;
  
		-- 2. если заказ не собирается целиком, то собираю по частям
		while v_order_complete = 'F' and v_not_ost_flag = 'F'
		loop
			v_not_ost_flag := 'T';
			for r_all_order in (select b.id_shop, sum(decode(b.art, null, 0, 1)) max_order, sum(b.kol) kol, c.reit2
													from (select a.art, a.asize, a.kol - nvl(b.kol, 0) kol
																 from tem_inet_move_matrix_book a
																 left join (select art, asize, sum(kol) kol
																					 from tem_inet_move_matrix_result
																					 group by art, asize) b on a.art = b.art
																																		 and a.asize = b.asize
																 where a.kol - nvl(b.kol, 0) > 0) a
													inner join (select x.id_shop, x.art, x.asize, sum(x.kol) - sum(nvl(b.kol, 0)) kol
																		 from tem_inet_move_matrix_ost x
																		 left join tem_inet_move_matrix_result b on x.id_shop = b.id_shop_from
																																								and x.art = b.art
																																								and x.asize = b.asize
																		 group by x.id_shop, x.art, x.asize
																		 having sum(x.kol) - sum(nvl(b.kol, 0)) > 0) b on a.art = b.art
																																											and a.asize = b.asize
													-- здесь уже не соединяем по кол-ву
													left join tdv_map_shop_reit c on b.id_shop = c.id_shop
													group by b.id_shop, c.reit2
													order by -- 1. если  заказ находится в пункте самовывоза
																		decode(v_id_shop_to, b.id_shop, 0, 1),
																	 -- 2. если  заказ на СГП
																	 decode(b.id_shop, C_CODE_SGP, 0, 1),
																	 -- 3. где можно собрать максимальное число позиций
																	 sum(decode(b.art, null, 0, 1)) desc,
																	 -- 4.  заказ на магазине в котором больше всего пар
																	 sum(b.kol) desc,
																	 -- 5. магазин с худшим рейтингом
																	 nvl(c.reit2, 1000) desc)
			loop
				v_not_ost_flag := 'F';
			
				for r_part_of_order in (select a.art, a.asize, a.kol kol_order, b.kol kol_stock
																from (select a.art, a.asize, a.kol - nvl(b.kol, 0) kol
																			 from tem_inet_move_matrix_book a
																			 left join (select art, asize, sum(kol) kol
																								 from tem_inet_move_matrix_result
																								 group by art, asize) b on a.art = b.art
																																					 and a.asize = b.asize
																			 where a.kol - nvl(b.kol, 0) > 0) a
																inner join (select x.id_shop, x.art, x.asize, sum(x.kol) - sum(nvl(b.kol, 0)) kol
																					 from tem_inet_move_matrix_ost x
																					 left join tem_inet_move_matrix_result b on x.id_shop = b.id_shop_from
																																											and x.art = b.art
																																											and x.asize = b.asize
																					 where x.id_shop = r_all_order.id_shop
																					 group by x.id_shop, x.art, x.asize
																					 having sum(x.kol) - sum(nvl(b.kol, 0)) > 0) b on a.art = b.art
																																														and a.asize = b.asize)
				loop
					insert into tem_inet_move_matrix_result
					values
						(i_posnr, r_part_of_order.art, r_part_of_order.asize, least(r_part_of_order.kol_order, r_part_of_order.kol_stock), r_all_order.id_shop,
						 v_id_shop_to);
					commit;
          
          write_log('KAA_INET_BOOK', 'run_move_matrix', 0, i_posnr, i_serv_ip, i_through_sgp, 'заказ собран по частям в ',
                       r_all_order.id_shop||', '||r_part_of_order.art||', '||r_part_of_order.asize||', '||least(r_part_of_order.kol_order, r_part_of_order.kol_stock));
				end loop;
			
				exit;
			end loop;
		
			-- проверяю, собрался ли заказ целиком
			select decode(count(*), 0, 'T', 'F')
			into v_order_complete
			from (select a.art, a.asize, a.kol - nvl(b.kol, 0) kol
						 from tem_inet_move_matrix_book a
						 left join (select art, asize, sum(kol) kol
											 from tem_inet_move_matrix_result
											 group by art, asize) b on a.art = b.art
																								 and a.asize = b.asize
						 where a.kol - nvl(b.kol, 0) > 0);
		
			-- если программа не нашла откуда взять 
			if v_not_ost_flag = 'T' then
				for r_not_ost_order in (select a.art, a.asize, a.kol - nvl(b.kol, 0) kol
																from tem_inet_move_matrix_book a
																left join (select art, asize, sum(kol) kol
																					from tem_inet_move_matrix_result
																					group by art, asize) b on a.art = b.art
																																		and a.asize = b.asize
																where a.kol - nvl(b.kol, 0) > 0)
				loop
					insert into tem_inet_move_matrix_result
					values
						(i_posnr, r_not_ost_order.art, r_not_ost_order.asize, r_not_ost_order.kol, C_CODE_NOT_OST, v_id_shop_to);
            
          write_log('KAA_INET_BOOK', 'run_move_matrix', 0, i_posnr, i_serv_ip, i_through_sgp, 'не нашло остатков для ',
                       r_not_ost_order.art||', '|| r_not_ost_order.asize||', '||r_not_ost_order.kol);            
				end loop;
			end if;
		end loop;
	
		-- если перевозка через СГП, то в любом случае меняем получателя на СГП (кроме магазина самовывоза)
		if i_through_sgp = 'T' then
			update tem_inet_move_matrix_result a
			set a.id_shop_to = C_CODE_SGP
			where a.id_shop_from != C_CODE_SGP
						and nvl(a.id_shop_to,'XXX') != C_CODE_NOT_OST
            and a.id_shop_from != nvl(v_id_shop_to,'XXX');
		end if;
	
		commit;
	end run_move_matrix;


  
  --процедура обмена всех неотправленных заказов по всем магазинам
  PROCEDURE BOOK_EXCHANGE_SHOPS
   AS 
  cursor shop is 
  select distinct b.id_shop
		from d_inet_book1 a
		inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
		left join (select distinct shopid id_shop
								from st_shop_access
								where bit_access = 'T') c on c.id_shop = b.id_shop
		left join (select shopid id_shop,min(timews) timews,max(timewe) timewe,timecur
								from (select b.shopid,
											to_date(b.time_start,'HH24:mi')+(nvl(c.gmt,3)-3)/24 timews,
											to_date(b.time_end,'HH24:mi')+(nvl(c.gmt,3)-3)/24 timewe,
											to_date(to_char(sysdate,'HH24:mi'),'HH24:mi') timecur
											from st_shop_day_work_time b
											left join st_shop c on c.shopid = b.shopid
											where b.day_id = (1 + TRUNC (sysdate) - TRUNC (sysdate, 'IW')))
								where timecur between timews and timewe
								group by shopid,timecur) d on d.id_shop = b.id_shop
		where (a.bit_send = 'F' or b.bit_send = 'F')-- and b.id_shop not in ('0044')
			and (c.id_shop is not null or d.id_shop is not null or b.id_shop in ('KI1000','N/A','S888'))
    order by b.id_shop;
			
  shopRow shop%rowtype;
  p_shopID varchar2(10);
  result_p varchar2(1 CHAR);
  BEGIN
    
  --ставлю флаг отправки в false, если в первой таблице false
  update d_inet_book2 a
    set a.bit_send = 'F'
    where (a.posnr,a.serv_ip) in (select b.posnr,b.serv_ip
                                from d_inet_book1 b
                                where b.bit_send = 'F' and a.posnr = b.posnr and a.serv_ip = b.serv_ip);  
  commit;
   
  open shop;
    loop  
         FETCH shop INTO p_shopID;
         EXIT WHEN shop%NOTFOUND;  
            begin
						
							begin
								DBMS_SCHEDULER.drop_job( 'kaa_j_mass_book_exch_'||replace(p_shopID,'/','_'));
							exception when others then
								null;
							end;
							
							DBMS_SCHEDULER.CREATE_JOB (
													 job_name             => 'kaa_j_mass_book_exch_'||replace(p_shopID,'/','_'),
													 job_type             => 'PLSQL_BLOCK',
													 job_action           => q'[
								declare
								result_p varchar2(1 CHAR);
								BEGIN 
									kaa_inet_book.BOOK_EXCHANGE(']'||p_shopID||q'[',null,null,null,null,result_p);
									commit;
									
									EXCEPTION
										WHEN OTHERS THEN
										WRITE_ERROR_PROC('KAA_INET_BOOK_EXCHANGE_SHOPS', sqlcode, sqlerrm, 'SHOPIDIN = '||']'||p_shopID||q'[', ' ', ' ');
										rollback;
								END;
								]',
													 enabled              =>  false);
											
							DBMS_SCHEDULER.ENABLE ('kaa_j_mass_book_exch_'||replace(p_shopID,'/','_')); 						
            
            commit;
              EXCEPTION
              WHEN OTHERS THEN
              dbms_output.put_line(sqlcode||sqlerrm);
              dbms_output.put_line(p_shopID);
              WRITE_ERROR_PROC('KAA_INET_BOOK_EXCHANGE_SHOPS', sqlcode, sqlerrm, 'SHOPIDIN = '||p_shopID, ' ', ' ');
              rollback;
            end;
    end loop;
    
  END BOOK_EXCHANGE_SHOPS;
  
  --процедура вызова матрицы перемещения и дополнения заказа позициями для магазинов-отправителей
  PROCEDURE BOOK_CREATE_MOVE(posnrin in varchar2 default null, serv_ipin in varchar2 default null, testin in varchar2 default 'F')
   AS 
  
  p_result varchar2(500);
  p_rnum number(10);
	p_curcount number(5);
	p_syst varchar2(50);
  p_landid varchar2(20 CHAR);
  p_delivery_addr varchar2(10 CHAR);
  BEGIN
		p_curcount := 0;
    
    if is_hyb_order_prod(posnrin,serv_ipin) = 'F' and testin = 'F' then
       return;
    end if;
    
    select nvl(max(landid),'N/A') into p_landid
    from hybris.st_server_for_servlet
    where server = serv_ipin;
    
    if p_landid = 'N/A' then
      WRITE_ERROR_PROC('KAA_INET_BOOK_CREATE_MOVE', sqlcode, sqlerrm, 'p_landid = N/A', ' ', ' ');
      return;
    end if;
		
    select bit_delivery_addr into p_delivery_addr
    from d_inet_book1
    where posnr = posnrin and serv_ip = serv_ipin
    ;
    
    --ставлю метку удаления по предыдущему вызову матрицы
    update d_inet_book2
			set bit_delete = 'T'
      where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'T' and bit_delete = 'F'
    ;
		
		delete from d_inet_book4
			where bit_move = 'T' and posnr = posnrin and serv_ip = serv_ipin;
    
    update d_inet_book2
      set id_shop_from = null,id_shop_to = null
      where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F'
    ;
    
    --вызов матрицы
    if p_landid = 'BY' /*and p_delivery_addr = 'F'*/ then
      SPA_MOVE_MATRIX.run_move_matrix(posnrin, serv_ipin);
    else
      run_move_matrix(posnrin, serv_ipin, 'T');
    end if;
    for cur in (select a.posnr,a.art,a.asize,1 kol,a.id_shop_from,a.id_shop_to 
                    from tem_inet_move_matrix_result a
                    inner join (select level lvl from dual connect by level <= 1000) b on b.lvl <= a.kol
                    order by id_shop_from) loop
      begin
        select rnum into p_rnum
          from d_inet_book2
          where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F' and id_shop_from is null and id_shop_to is null
          and art = cur.art and asize = cur.asize
          and rownum = 1
        ;
      
        update d_inet_book2
          set id_shop_from = cur.id_shop_from,id_shop_to = cur.id_shop_to
          where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F' and id_shop_from is null and id_shop_to is null
          and art = cur.art and asize = cur.asize
          and rownum = 1
        ;
        
        if nvl(cur.id_shop_from,'null') != 'NOT_OST' and nvl(cur.id_shop_from,'null') != nvl(cur.id_shop_to,'null') then
					p_curcount := p_curcount + 1;
          insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,BIT_ACCEPT,BIT_SALE,SCAN,BIT_BLOCK,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT)
            select POSNR,ART,ASIZE,KOL,SUM,cur.id_shop_from ID_SHOP,'F' BIT_ACCEPT,'F' BIT_SALE,' ' SCAN,'T' BIT_BLOCK,SERV_IP,RNUM,'T' BIT_MOVE,cur.id_shop_to ID_SHOP_TO,'N/A' ID_SHOP_FROM,base_price,discount
            from d_inet_book2
            where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F' and rnum = p_rnum
          ;
        end if;
      
        EXCEPTION
        WHEN OTHERS THEN
        rollback;
        WRITE_ERROR_PROC('KAA_INET_BOOK_CREATE_MOVE', sqlcode, sqlerrm, ' ', ' ', ' ');
      end;
    end loop;
		
		--удаляю записи с меткой удаления, если с таким же ключом есть запись без метки удаления
		delete from d_inet_book2 a
			where a.posnr = posnrin and a.serv_ip = serv_ipin and a.bit_move = 'T' and a.bit_delete = 'T'
			and (a.POSNR,a.ID_SHOP,a.SERV_IP,a.RNUM) in (select b.POSNR,b.ID_SHOP,b.SERV_IP,b.RNUM
																										from d_inet_book2 b 
																										where b.bit_delete = 'F' and a.bit_move = b.bit_move and a.POSNR = b.posnr and a.ID_SHOP = b.id_shop
																										and a.SERV_IP = b.serv_ip and a.RNUM = b.rnum);
																										
		insert into d_inet_book4 (POSNR,SERV_IP,RNUM,BIT_MOVE,ID_SHOP)
						select posnr,serv_ip,rnum,bit_move,id_shop
						from d_inet_book2
						where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'T' and bit_delete = 'F';
						
		update d_inet_book4 a
			set a.dpd_id = (select max(b.dpd_id)
											from d_inet_book4 b
											where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum
												and b.bit_move = 'F')
			where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'T';
      
    if p_landid = 'BY' then
      for cur in (select x.rnum,x.bit_move,dense_rank() over (partition by posnr order by num,bit_move,nvl(id_shop_from,'nn')) rn
                  from (
                    select a.*,case when nvl(id_shop_from,'nn') = id_shop then 1 else 2 end num
                    from d_inet_book2 a
                    where posnr = posnrin and serv_ip = serv_ipin
                  ) x) loop
        begin
          update d_inet_book4
          set order_num = cur.rn
          where posnr = posnrin and serv_ip = serv_ipin and rnum = cur.rnum and bit_move = cur.bit_move
          ;
        end;
      end loop;
    end if;
			
		insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
			select posnrin,sysdate,'ВЫПОЛНЕНИЕ МАТРИЦЫ ПЕРЕМЕЩЕНИЯ. НОВЫХ ЗАПИСЕЙ - '||p_curcount,serv_ipin,a.idm+1,'SERVER'
			from (select nvl(max(id),0) idm
									from d_inet_book3
									where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
    
    commit;
		
		if is_hyb_serv_prod(serv_ipin) = 'T' then
			p_syst := 'BWP599';
		else
			p_syst := 'BWQ399';
		end if;
		
		RFC_ORDER_SEND(posnrin, serv_ipin, p_syst);
		
		EXCEPTION
		WHEN OTHERS THEN
		rollback;
		WRITE_ERROR_PROC('KAA_INET_BOOK_CREATE_MOVE', sqlcode, sqlerrm, ' ', ' ', ' ');
    
  END BOOK_CREATE_MOVE;
  
  --процедура автоотмены заказа в hybris по истечению срока бронирования
  PROCEDURE BOOK_PASSED_TIME
   AS 
  cursor book is 
  select distinct a.posnr,a.serv_ip,b.id_shop,c.db_link_name,a.bit_book,d.bit_sale
		from d_inet_book1 a
		inner join hybris.st_server_for_servlet s on s.server = a.serv_ip and s.landid = 'BY'
		inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
		inner join st_shop c on b.id_shop = c.shopid
		inner join (select posnr,serv_ip,max(bit_sale) bit_sale,sum(case when bit_sale = 'T' then 1 else 0 end) sale_cnt,
									sum(1) cnt,sum(case when bit_sale = 'T' or (id_cancel != 0 and id_cancel is not null) then 0 else 1 end) hang_cnt
									from d_inet_book2_v
									where bit_move = 'F'
									group by posnr,serv_ip) d on a.posnr = d.posnr and a.serv_ip = d.serv_ip
		left join (select posnr,serv_ip,max(date_s) date_s
								from d_inet_book3
								where cond like '%ПОДТВЕРЖДЕНО В HYBRIS%'
								group by posnr,serv_ip) e on e.posnr = a.posnr and e.serv_ip = a.serv_ip
		where is_hyb_serv_prod(a.serv_ip) = 'T' and a.bit_status = 'T' and a.serv_ip != ' ' and bit_move = 'F' and nvl(a.op_id_cancel,0) != 14
			and ((floor(sysdate-trunc(a.dated+1)) >= 1 and b.bit_sale = 'F' and a.bit_book = 'T')
						or (floor(sysdate-nvl(trunc(e.date_s+1),sysdate)) >= 3 and d.bit_sale = 'F' and a.bit_book = 'F' and a.bit_online_pay = 'F' and a.bit_delivery_addr = 'F')
						or (floor(sysdate-nvl(trunc(e.date_s+1),sysdate)) >= 3 and d.bit_sale = 'T' and d.hang_cnt > 0 and a.bit_book = 'F' and a.bit_online_pay = 'F' and a.bit_delivery_addr = 'F'))
		order by b.id_shop;
  bookRow book%rowtype;
  p_shopID varchar2(5);
  p_shopLink varchar2(10);
  p_posnr varchar2(20);
  p_serv_ip varchar2(50);
  p_bit_book varchar2(5);
	p_bit_sale varchar2(5);
  p_command varchar2(20);
  p_shopto varchar2(20);
  p_result varchar2(500);
  p_landid varchar(20);
  book_cur sys_refcursor;
  BEGIN
   
  open book;
    loop  
         FETCH book INTO p_posnr,p_serv_ip,p_shopID,p_shopLink,p_bit_book,p_bit_sale;
         EXIT WHEN book%NOTFOUND;  
            begin
							if p_bit_sale = 'F' then
                select nvl(max(landid),'BY') into p_landid
                  from hybris.st_server_for_servlet
                  where type = 'P' and server = p_serv_ip;
								p_command := case when p_bit_book = 'T' then 'reserv' else 'order' end;
								p_shopto := case when p_shopID like '00%' then 'ST_BY_'||p_shopID when p_shopID like 'N/A%' then 'ST_'||p_landid||'_0001' else 'ST_RU_'||p_shopID end;
								p_result := KAA_JAVA_HYBRIS_EXCH_FUN(p_serv_ip,p_command,p_posnr, p_shopto, 'CANCELLED');
								commit;
								
								if p_result != 'OK' then
									insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
										select p_posnr,sysdate,'ОШИБКА ОТМЕНЫ ИСТЕКШЕГО ЗАКАЗА В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
										from (select nvl(max(id),0) idm
																from d_inet_book3
																where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
									commit;
									continue;
								end if;
								
								update d_inet_book2
									set bit_send = 'F',date_cancel = sysdate
									where posnr = p_posnr and serv_ip = p_serv_ip;
                update d_inet_book4
                  set bit_hyb_cancel = 'T',bit_hyb_ready = 'F',bit_hyb_receive = 'F',op_id_cancel = 14
                  where posnr = p_posnr and serv_ip = p_serv_ip and bit_move = 'F';
								update d_inet_book1
                  set bit_hyb_cancel = 'T',bit_hyb_ready = 'F',bit_status = 'F',op_id_cancel = 14,bit_phone = 'T'
                  where posnr = p_posnr and serv_ip = p_serv_ip;
								insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
										select p_posnr,sysdate,'ОТМЕНЕН ИСТЕКШИЙ ЗАКАЗ В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
										from (select nvl(max(id),0) idm
																from d_inet_book3
																where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
								
								commit;
							else
								update d_inet_book2
									set bit_send = 'F',date_cancel = sysdate
									where posnr = p_posnr and serv_ip = p_serv_ip and bit_sale = 'F' and bit_move = 'F';
                update d_inet_book4 a
                  set (a.bit_hyb_cancel,a.bit_hyb_ready,a.bit_hyb_receive,a.op_id_cancel) = 
											(select 'T','F','F',14
												from dual)
                  where a.posnr = p_posnr and a.serv_ip = p_serv_ip and a.bit_move = 'F'
										and (a.posnr,a.serv_ip,a.rnum) in
													(select b.posnr,b.serv_ip,b.rnum
													from d_inet_book2 b
													where b.posnr = p_posnr and b.serv_ip = p_serv_ip and b.bit_move = 'F' and b.bit_sale = 'F');
								update d_inet_book1
																set bit_send = 'F', bit_phone = 'T',op_id_cancel = 14,bit_status = 'F'
																where posnr = p_posnr and serv_ip = p_serv_ip;
								insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
										select p_posnr,sysdate,'ОТМЕНЕН ИСТЕКШИЙ ЗАКАЗ В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
										from (select nvl(max(id),0) idm
																from d_inet_book3
																where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
								
								commit;
							end if;
              
              RFC_ORDER_CH_UPD(p_posnr, p_serv_ip, 'X', 'X');
              
              update_hybris_stock_full(p_posnr, p_serv_ip,'CANCEL');
              
              if p_bit_book = 'F' then
                 p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(p_serv_ip, p_posnr);
              end if;
							
              EXCEPTION
              WHEN OTHERS THEN
              rollback;
              WRITE_ERROR_PROC('KAA_INET_BOOK_PASSED_TIME', sqlcode, sqlerrm, 'SHOPIDIN = '||p_shopID, ' ', ' ');
              
              begin
                insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                  select p_posnr,sysdate,'ОШИБКА ОТМЕНЫ ИСТЕКШЕГО ЗАКАЗА В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
                  from (select nvl(max(id),0) idm
                              from d_inet_book3
                              where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                  commit;
              end;
            end;
    end loop;
    
  END BOOK_PASSED_TIME;
  
  --процедура отправки статуса "ПРОДАН" в hybris
  PROCEDURE BOOK_SOLD(posnrin in varchar2 default null, serv_ipin in varchar2 default null)
   AS 
  cursor book is 
  select distinct a.posnr,a.serv_ip,b.id_shop,c.db_link_name,a.bit_book
  from d_inet_book1 a
  inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
  inner join d_inet_book4 e on b.posnr = e.posnr and b.serv_ip = e.serv_ip and b.rnum = e.rnum and b.bit_move = e.bit_move
  inner join st_shop c on b.id_shop = c.shopid
  left join (select posnr,serv_ip,max(bit_sale) bit_sale
              from d_inet_book2 
							where bit_move = 'F'
              group by posnr,serv_ip) d on d.posnr = a.posnr and d.serv_ip = a.serv_ip
  where is_hyb_serv_prod(a.serv_ip) = 'T' and a.bit_delivery_addr = 'F' and b.bit_move = 'F' and a.serv_ip != ' ' and d.bit_sale = 'T' and b.bit_sale = 'T' and e.bit_hyb_receive = 'F'
  order by b.id_shop;
  bookRow book%rowtype;
  p_shopID varchar2(5);
  p_shopLink varchar2(10);
  p_posnr varchar2(20);
  p_serv_ip varchar2(50);
  p_bit_book varchar2(5);
  p_command varchar2(20);
  p_shopto varchar2(20);
  p_result varchar2(500);
	p_hyb_status varchar(50);
  p_landid varchar2(20);
  book_cur sys_refcursor;
  BEGIN
  
  if posnrin is null and serv_ipin is null then 
    open book;
      loop  
           FETCH book INTO p_posnr,p_serv_ip,p_shopID,p_shopLink,p_bit_book;
           EXIT WHEN book%NOTFOUND;  
              begin
                select nvl(max(landid),'BY') into p_landid
                  from hybris.st_server_for_servlet
                  where type = 'P' and server = p_serv_ip;
                p_command := case when p_bit_book = 'T' then 'reserv' else 'order' end;
                p_shopto := case when p_shopID like '00%' then 'ST_BY_'||p_shopID when p_shopID like 'N/A%' then 'ST_'||p_landid||'_0001' else 'ST_RU_'||p_shopID end;
								p_hyb_status := case when p_bit_book = 'T' then 'RECEIVED' else 'COMPLETED' end;
                p_result := KAA_JAVA_HYBRIS_EXCH_FUN(p_serv_ip,p_command,p_posnr, p_shopto, p_hyb_status);
                commit;
                
                if p_result != 'OK' then
                  insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                    select p_posnr,sysdate,'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
                    from (select nvl(max(id),0) idm
                                from d_inet_book3
                                where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                  commit;
                  continue;
                end if;
                
                update d_inet_book1
                                set bit_hyb_cancel = 'F',bit_hyb_ready = 'F',bit_hyb_receive = 'T',bit_phone = 'T',bit_status = 'T'
                                where posnr = p_posnr and serv_ip = p_serv_ip;
                update d_inet_book4 a
                  set (a.bit_hyb_cancel,a.bit_hyb_ready,a.bit_hyb_receive,a.op_id_cancel) = 
											(select 'F','F','T',null
												from dual)
                  where a.posnr = p_posnr and a.serv_ip = p_serv_ip and a.bit_move = 'F'
										and (a.posnr,a.serv_ip,a.rnum) in
													(select b.posnr,b.serv_ip,b.rnum
													from d_inet_book2 b
													where b.posnr = p_posnr and b.serv_ip = p_serv_ip and b.bit_move = 'F' and b.bit_sale = 'T');
                insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                    select p_posnr,sysdate,'ПРОДАЖА ПОДТВЕРЖДЕНА В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
                    from (select nvl(max(id),0) idm
                                from d_inet_book3
                                where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                
                commit;
                
                if p_bit_book = 'F' then
                   p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(p_serv_ip, p_posnr);
                end if;
                
                EXCEPTION
                WHEN OTHERS THEN
                rollback;
                WRITE_ERROR_PROC('KAA_INET_BOOK_SOLD', sqlcode, sqlerrm, 'SHOPIDIN = '||p_shopID, ' ', ' ');
                
                begin
                  insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                    select p_posnr,sysdate,'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
                    from (select nvl(max(id),0) idm
                                from d_inet_book3
                                where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                    commit;
                end;
              end;
      end loop;
  else
    for cur_row in (select distinct a.posnr,a.serv_ip,b.id_shop,c.db_link_name,a.bit_book
                    from d_inet_book1 a
                    inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                    inner join d_inet_book4 e on b.posnr = e.posnr and b.serv_ip = e.serv_ip and b.rnum = e.rnum and b.bit_move = e.bit_move
                    inner join st_shop c on b.id_shop = c.shopid
                    left join (select posnr,serv_ip,max(bit_sale) bit_sale
                                from d_inet_book2 
                                group by posnr,serv_ip) d on d.posnr = a.posnr and d.serv_ip = a.serv_ip
                    where is_hyb_serv_prod(a.serv_ip) = 'T' and b.bit_move = 'F' and a.serv_ip = serv_ipin and a.posnr = posnrin and a.bit_status = 'T' and d.bit_sale = 'T' and b.bit_sale = 'T' and e.bit_hyb_receive = 'F') loop
      begin
        p_command := case when cur_row.bit_book = 'T' then 'reserv' else 'order' end;
        p_shopto := case when cur_row.id_shop like '00%' then 'ST_BY_'||cur_row.id_shop else 'ST_RU_'||cur_row.id_shop end;
				p_hyb_status := case when cur_row.bit_book = 'T' then 'RECEIVED' else 'COMPLETED' end;
        p_result := KAA_JAVA_HYBRIS_EXCH_FUN(cur_row.serv_ip,p_command,cur_row.posnr, p_shopto, p_hyb_status);
        commit;
        
        if p_result != 'OK' then
          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
            select cur_row.posnr,sysdate,'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS',cur_row.serv_ip,a.idm+1,'SERVER'
            from (select nvl(max(id),0) idm
                        from d_inet_book3
                        where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and init = 'SERVER') a;
          commit;
          continue;
        end if;
        
        update d_inet_book1
                                  set bit_hyb_cancel = 'F',bit_hyb_ready = 'F',bit_hyb_receive = 'T',bit_phone = 'T',bit_status = 'T'
                                  where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip;
        update d_inet_book4 a
                  set (a.bit_hyb_cancel,a.bit_hyb_ready,a.bit_hyb_receive,a.op_id_cancel) = 
											(select 'F','F','T',null
												from dual)
                  where a.posnr = cur_row.posnr and a.serv_ip = cur_row.serv_ip and a.bit_move = 'F'
										and (a.posnr,a.serv_ip,a.rnum) in
													(select b.posnr,b.serv_ip,b.rnum
													from d_inet_book2 b
													where b.posnr = cur_row.posnr and b.serv_ip = cur_row.serv_ip and b.bit_move = 'F' and b.bit_sale = 'T');
        insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
            select cur_row.posnr,sysdate,'ПРОДАЖА ПОДТВЕРЖДЕНА В HYBRIS',cur_row.serv_ip,a.idm+1,'SERVER'
            from (select nvl(max(id),0) idm
                        from d_inet_book3
                        where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and init = 'SERVER') a;
        
        commit;
        
        if cur_row.bit_book = 'F' then
           p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(cur_row.serv_ip, cur_row.posnr);
        end if;
      
        EXCEPTION
        WHEN OTHERS THEN
        rollback;
        WRITE_ERROR_PROC('KAA_INET_BOOK_SOLD', sqlcode, sqlerrm, 'SHOPIDIN = '||p_shopID, ' ', ' ');
        
        begin
          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
            select cur_row.posnr,sysdate,'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS',cur_row.serv_ip,a.idm+1,'SERVER'
            from (select nvl(max(id),0) idm
                        from d_inet_book3
                        where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and init = 'SERVER') a;
            commit;
        end;
      end;
    end loop;
  end if;
    
  END BOOK_SOLD;
  
  --процедура вызывает rfc функцию в сап и забирает из нее данные по остаткам склада в таблицу RFC_FREE_STOCK
  --пример: select matnr from s_all_mat
  PROCEDURE GET_FREE_STOCK(p_query in varchar2 default null, p_syst in varchar2 default 'BWP599')
   AS 
  
  p_result varchar2(5000);
  p_id varchar2(1000);
  p_cur SYS_REFCURSOR;
  p_matnr varchar2(200);
	p_id_fun number(5);
	p_result_table varchar2(100);
  BEGIN
		select nvl(max(id_fun),-1) into p_id_fun
			from idocs.rfc_sap_fun
			where system = p_syst and fun_name = 'ZKM_GET_FREE_STOCK';
			
		if p_id_fun = -1 then
			WRITE_ERROR_PROC('KAA_GET_FREE_STOCK', '', '', 'Система не найдена', ' ', ' ');
			return;
		end if;
		
		select case when p_syst = 'BWP599' then 'RFC_FREE_STOCK' else 'RFC_FREE_STOCK_TEST' end into p_result_table
			from dual;
		
    select sys_context('USERENV', 'host')||'#'||sys_context('USERENV', 'os_user') into p_id
      from dual;
  
    if p_query is null then
      delete from rfc_get_free_stock where id = p_id and syst = p_syst;
      
      update idocs.rfc_struct_tables
        set table_name_in_system = null,
            table_query = null
      where id_fun = p_id_fun and table_name = 'ITR_MATNR';
      update idocs.rfc_struct_tables
        set table_query = 'begin
                            delete from FIRM.'||p_result_table||';
                            insert into FIRM.'||p_result_table||' (MEINS,VERME,MATNR)
                            select MEINS,VERME,MATNR from IDOCS.WR_ET_FREE_STOCK_'||p_id_fun||';
                            update FIRM.'||p_result_table||' a
                              set (art,asize) = (select nvl(max(b.art),a.art),nvl(max(b.asize),0)
                                                  from s_all_mat b
                                                  where a.matnr = b.matnr)
                              where art is null or asize is null;
                            end;'
        where id_fun = p_id_fun and table_name = 'ET_FREE_STOCK';  
      commit;
    else
      delete from rfc_get_free_stock where id = p_id and syst = p_syst;
      
      open p_cur for p_query;
      LOOP 
        FETCH p_cur
          INTO  p_matnr;
        EXIT WHEN p_cur%NOTFOUND;
        insert into rfc_get_free_stock (SIGN,OPT,LOW,HIGH,ID,SYST)
          select distinct 'I' SIGN,'EQ' OPT,p_matnr LOW,'' HIGH,p_id ID,p_syst
          from dual;
      END LOOP;
      CLOSE p_cur;
      
  --    execute immediate 'insert into rfc_get_free_stock (SIGN,OPT,LOW,HIGH,ID)
  --                        select distinct ''I'' SIGN,''EQ'' OPT,matnr LOW,'''' HIGH,'''||p_id||''' ID
  --                        from ('||p_query||')';
                          
      update idocs.rfc_struct_tables
          set table_name_in_system = 'FIRM.RFC_GET_FREE_STOCK',
              table_query = 'select HIGH,LOW,opt "OPTION",SIGN from FIRM.RFC_GET_FREE_STOCK where id = '''||p_id||''' and syst = '''||p_syst||''''
        where id_fun = p_id_fun and table_name = 'ITR_MATNR';
      update idocs.rfc_struct_tables
        set table_query = 'begin
                            --delete from FIRM.'||p_result_table||';
                            --insert into FIRM.'||p_result_table||' (MEINS,VERME,MATNR)
                            --select MEINS,VERME,MATNR from IDOCS.WR_ET_FREE_STOCK_'||p_id_fun||';
                            merge into FIRM.'||p_result_table||' a
                              using (select MEINS,VERME,MATNR 
                                      from IDOCS.WR_ET_FREE_STOCK_'||p_id_fun||') b on (a.matnr = b.matnr)
                              when matched then
                                update set a.verme = b.verme
                              when not matched then
                                insert (MEINS,VERME,MATNR)
                                  values (b.MEINS,b.VERME,b.MATNR);
                            update FIRM.'||p_result_table||' a
                              set (art,asize) = (select nvl(max(b.art),a.art),nvl(max(b.asize),0)
                                                  from s_all_mat b
                                                  where a.matnr = b.matnr)
                              where art is null or asize is null;
                            delete from FIRM.'||p_result_table||'
                              where verme = 0;
                            end;'
        where id_fun = p_id_fun and table_name = 'ET_FREE_STOCK';  
      
      commit;                        
    end if;
    
    select KAA_JAVA_CALL_RFC_FUN('service-bw.tomcat.vit.belwest.com','ZKM_GET_FREE_STOCK',p_syst) into p_result from dual;
    dbms_output.put_line(p_result);
    
    delete from rfc_get_free_stock where id = p_id and syst = p_syst;
  
    commit;
    
    if p_result != 'true' then
      WRITE_ERROR_PROC('KAA_GET_FREE_STOCK', sqlcode, sqlerrm, ' ', p_result, ' ');
    end if;
    
    delete from rfc_free_stock where art = '1937940';
    commit;
      
    EXCEPTION
    WHEN OTHERS THEN
    rollback;
    WRITE_ERROR_PROC('KAA_GET_FREE_STOCK', sqlcode, sqlerrm, ' ', ' ', ' ');
    raise;
    
  END GET_FREE_STOCK;
  
  --автоотмена заказов в центре при отмене заказа магазином
  PROCEDURE BOOK_SHOP_CANCELLED(posnrin in varchar2 default null, serv_ipin in varchar2 default null)
   AS 
  cursor book is 
  select distinct a.posnr,a.serv_ip,b.id_shop,c.db_link_name,a.bit_book,
		case when a.bit_book = 'T' then b.id_cancel else 16 end id_cancel,
		d.bit_phone_cancel,d.bit_sale
		from d_inet_book1 a
		inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
		inner join st_shop c on b.id_shop = c.shopid
		inner join (select x.posnr,x.serv_ip,max(bit_sale) bit_sale,min(bit_accept) bit_accept,
									min(case when bit_sale = 'F' then nvl(id_cancel,0) else null end) id_cancel, max(case when nvl(z.ccancel,0) > 1 then 'F' else y.bit_phone end) bit_phone_cancel,
									sum(case when bit_sale = 'T' then 1 else 0 end) sale_cnt,
									sum(1) cnt,sum(case when bit_sale = 'T' or (id_cancel != 0 and id_cancel is not null) then 0 else 1 end) hang_cnt
									from d_inet_book2_v x
									left join (select posnr,serv_ip,rnum,count(*) ccancel
															from d_inet_book2_v
															where nvl(id_cancel,0) > 0
															group by posnr,serv_ip,rnum) z on x.posnr = z.posnr and x.serv_ip = z.serv_ip and x.rnum = z.rnum
									left join st_book_cancel y on x.id_cancel = y.id
									where bit_move = 'F'
									group by x.posnr,x.serv_ip) d on a.posnr = d.posnr and a.serv_ip = d.serv_ip
		where is_hyb_serv_prod(a.serv_ip) = 'T' and a.serv_ip != ' ' and (a.bit_status = 'T' or (b.id_cancel = 17 and a.bit_hyb_cancel = 'F')) and bit_move = 'F'
		and ((b.bit_sale = 'F' and a.bit_book = 'T' and b.id_cancel is not null and b.id_cancel != 0)
					or (a.bit_book = 'F' and a.bit_online_pay = 'F' and a.bit_delivery_addr = 'F' and d.hang_cnt = 0 and nvl(d.id_cancel,0) > 0))
		order by b.id_shop;
  bookRow book%rowtype;
  p_shopID varchar2(5);
  p_shopLink varchar2(10);
  p_posnr varchar2(20);
  p_serv_ip varchar2(50);
  p_bit_book varchar2(5);
  p_command varchar2(20);
  p_shopto varchar2(20);
  p_result varchar2(500);
  p_id_cancel number(5);
  p_bit_phone varchar2(5);
	p_bit_sale varchar2(5);
	p_hyb_status varchar2(50);
  p_landid varchar2(20);
  book_cur sys_refcursor;
  BEGIN
   
    if posnrin is null and serv_ipin is null then 
      open book;
      loop  
           FETCH book INTO p_posnr,p_serv_ip,p_shopID,p_shopLink,p_bit_book,p_id_cancel,p_bit_phone,p_bit_sale;
           EXIT WHEN book%NOTFOUND;  
              begin
                select nvl(max(landid),'BY') into p_landid
                  from hybris.st_server_for_servlet
                  where type = 'P' and server = p_serv_ip;
                p_command := case when p_bit_book = 'T' then 'reserv' else 'order' end;
                p_shopto := case when p_shopID like '00%' then 'ST_BY_'||p_shopID when p_shopID like 'N/A%' then 'ST_'||p_landid||'_0001' else 'ST_RU_'||p_shopID end;
								p_hyb_status := case when p_bit_book = 'F' and p_bit_sale = 'T' then 'COMPLETED' else 'CANCELLED' end;
                p_result := KAA_JAVA_HYBRIS_EXCH_FUN(p_serv_ip,p_command,p_posnr, p_shopto, p_hyb_status);
                commit;
                
                if p_result != 'OK' then
                  insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                    select p_posnr,sysdate,'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
                    from (select nvl(max(id),0) idm
                                from d_inet_book3
                                where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                  commit;
                  continue;
                end if;
                
--                select case when bit_phone = 'T' then 'F' else 'T' end into p_bit_phone
--                  from st_book_cancel
--                  where id = p_id_cancel;
                
								update d_inet_book2
									set bit_send = 'F',date_cancel = sysdate
									where posnr = p_posnr and serv_ip = p_serv_ip and bit_move = 'F' and bit_sale = 'F';
                update d_inet_book4 a
                  set (a.bit_hyb_cancel,a.bit_hyb_ready,a.bit_hyb_receive,a.op_id_cancel) = 
                      (select case when c.bit_sale = 'T' then 'F' else 'T' end,
                        'F',
                        case when c.bit_sale = 'T' then 'T' else 'F' end,
                        case when c.bit_sale = 'T' then null else c.id_cancel end
                        from d_inet_book2 c
                          where c.posnr = p_posnr and c.serv_ip = p_serv_ip and a.rnum = c.rnum and c.bit_move = 'F')
                  where a.posnr = p_posnr and a.serv_ip = p_serv_ip and a.bit_move = 'F'
                    and (a.posnr,a.serv_ip,a.rnum) in
                          (select b.posnr,b.serv_ip,b.rnum
                          from d_inet_book2 b
                          where b.posnr = p_posnr and b.serv_ip = p_serv_ip and b.bit_move = 'F');
								if p_bit_book = 'F' and p_bit_sale = 'T' then
									update d_inet_book1
																	set bit_hyb_receive = 'T',bit_hyb_ready = 'F',bit_hyb_cancel = 'F',bit_status = 'F',bit_phone = case when p_bit_phone = 'T' then 'F' else 'T' end,op_id_cancel = p_id_cancel
																	where posnr = p_posnr and serv_ip = p_serv_ip;
								else
									update d_inet_book1
																	set bit_hyb_receive = 'F',bit_hyb_cancel = 'T',bit_hyb_ready = 'F',bit_status = 'F',bit_phone = case when p_bit_phone = 'T' then 'F' else 'T' end,op_id_cancel = p_id_cancel
																	where posnr = p_posnr and serv_ip = p_serv_ip;
								end if;
								insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
											select p_posnr,sysdate,'МАГАЗИНОМ ОТМЕНЕН ЗАКАЗ В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
											from (select nvl(max(id),0) idm
																	from d_inet_book3
																	where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                
                commit;
                
                RFC_ORDER_CH_UPD(p_posnr, p_serv_ip, 'X', 'X');
                
                update_hybris_stock_full(p_posnr, p_serv_ip,'CANCEL');
                
                if p_bit_book = 'F' then
                   p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(p_serv_ip, p_posnr);
                end if;
                
                EXCEPTION
                WHEN OTHERS THEN
                rollback;
                WRITE_ERROR_PROC('KAA_BOOK_SHOP_CANCELLED', sqlcode, sqlerrm, 'SHOPIDIN = '||p_shopID, ' ', ' ');
                
                begin
                  insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                    select p_posnr,sysdate,'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS',p_serv_ip,a.idm+1,'SERVER'
                    from (select nvl(max(id),0) idm
                                from d_inet_book3
                                where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
                    commit;
                end;
              end;
      end loop;
    else
      for cur_row in (select distinct a.posnr,a.serv_ip,b.id_shop,c.db_link_name,a.bit_book,
												case when a.bit_book = 'T' then b.id_cancel else 16 end id_cancel,
												d.bit_phone_cancel,d.bit_sale
												from d_inet_book1 a
												inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
												inner join st_shop c on b.id_shop = c.shopid
												inner join (select x.posnr,x.serv_ip,max(bit_sale) bit_sale,min(bit_accept) bit_accept,
                                    min(case when bit_sale = 'F' then nvl(id_cancel,0) else null end) id_cancel, max(case when nvl(z.ccancel,0) > 1 then 'F' else y.bit_phone end) bit_phone_cancel,
                                    sum(case when bit_sale = 'T' then 1 else 0 end) sale_cnt,
                                    sum(1) cnt,sum(case when bit_sale = 'T' or (id_cancel != 0 and id_cancel is not null) then 0 else 1 end) hang_cnt
                                    from d_inet_book2_v x
                                    left join (select posnr,serv_ip,rnum,count(*) ccancel
                                                from d_inet_book2_v
                                                where nvl(id_cancel,0) > 0
                                                group by posnr,serv_ip,rnum) z on x.posnr = z.posnr and x.serv_ip = z.serv_ip and x.rnum = z.rnum
                                    left join st_book_cancel y on x.id_cancel = y.id
                                    where bit_move = 'F'
                                    group by x.posnr,x.serv_ip) d on a.posnr = d.posnr and a.serv_ip = d.serv_ip
												where is_hyb_serv_prod(a.serv_ip) = 'T' and a.posnr = posnrin and a.serv_ip = serv_ipin
												and a.serv_ip != ' ' and a.bit_status = 'T' and bit_move = 'F'
												and ((b.bit_sale = 'F' and a.bit_book = 'T' and b.id_cancel is not null and b.id_cancel != 0)
															or (a.bit_book = 'F' and a.bit_online_pay = 'F' and a.bit_delivery_addr = 'F' and d.hang_cnt = 0 and nvl(d.id_cancel,0) > 0))
												order by b.id_shop) loop
        begin
          select nvl(max(landid),'BY') into p_landid
            from hybris.st_server_for_servlet
            where type = 'P' and server = cur_row.serv_ip;
          p_command := case when cur_row.bit_book = 'T' then 'reserv' else 'order' end;
          p_shopto := case when cur_row.id_shop like '00%' then 'ST_BY_'||cur_row.id_shop when cur_row.id_shop like 'N/A%' then 'ST_'||p_landid||'_0001' else 'ST_RU_'||cur_row.id_shop end;
					p_hyb_status := case when cur_row.bit_book = 'F' and cur_row.bit_sale = 'T' then 'COMPLETED' else 'CANCELLED' end;
          p_result := KAA_JAVA_HYBRIS_EXCH_FUN(cur_row.serv_ip,p_command,cur_row.posnr, p_shopto, p_hyb_status);
          commit;
          
          if p_result != 'OK' then
            insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
              select cur_row.posnr,sysdate,'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS',cur_row.serv_ip,a.idm+1,'SERVER'
              from (select nvl(max(id),0) idm
                          from d_inet_book3
                          where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and init = 'SERVER') a;
            commit;
            continue;
          end if;
          
--          select case when bit_phone = 'T' then 'F' else 'T' end into p_bit_phone
--            from st_book_cancel
--            where id = cur_row.id_cancel;
          
					update d_inet_book2
						set bit_send = 'F',date_cancel = sysdate
						where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and bit_move = 'F' and bit_sale = 'F';
          update d_inet_book4 a
            set (a.bit_hyb_cancel,a.bit_hyb_ready,a.bit_hyb_receive,a.op_id_cancel) = 
                (select case when c.bit_sale = 'T' then 'F' else 'T' end,
                  'F',
                  case when c.bit_sale = 'T' then 'T' else 'F' end,
                  case when c.bit_sale = 'T' then null else c.id_cancel end
                  from d_inet_book2 c
                    where c.posnr = cur_row.posnr and c.serv_ip = cur_row.serv_ip and a.rnum = c.rnum and c.bit_move = 'F')
            where a.posnr = cur_row.posnr and a.serv_ip = cur_row.serv_ip and a.bit_move = 'F'
              and (a.posnr,a.serv_ip,a.rnum) in
                    (select b.posnr,b.serv_ip,b.rnum
                    from d_inet_book2 b
                    where b.posnr = cur_row.posnr and b.serv_ip = cur_row.serv_ip and b.bit_move = 'F');
					if cur_row.bit_book = 'F' and cur_row.bit_sale = 'T' then
						update d_inet_book1
							set bit_hyb_receive = 'T',bit_hyb_cancel = 'F',bit_hyb_ready = 'F',bit_status = 'F',bit_phone = case when cur_row.bit_phone_cancel = 'T' then 'F' else 'T' end,op_id_cancel = cur_row.id_cancel
							where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip;
					else
						update d_inet_book1
							set bit_hyb_receive = 'F',bit_hyb_cancel = 'T',bit_hyb_ready = 'F',bit_status = 'F',bit_phone = case when cur_row.bit_phone_cancel = 'T' then 'F' else 'T' end,op_id_cancel = cur_row.id_cancel
							where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip;
					end if;
          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
              select cur_row.posnr,sysdate,'МАГАЗИНОМ ОТМЕНЕН ЗАКАЗ В HYBRIS',cur_row.serv_ip,a.idm+1,'SERVER'
              from (select nvl(max(id),0) idm
                          from d_inet_book3
                          where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and init = 'SERVER') a;
          
          commit;
          
          RFC_ORDER_CH_UPD(cur_row.posnr, cur_row.serv_ip, 'X', 'X');
          
          update_hybris_stock_full(cur_row.posnr, cur_row.serv_ip,'CANCEL');
          
          if cur_row.bit_book = 'F' then
             p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(cur_row.serv_ip, cur_row.posnr);
          end if;
        
          EXCEPTION
          WHEN OTHERS THEN
          rollback;
          WRITE_ERROR_PROC('KAA_BOOK_SHOP_CANCELLED', sqlcode, sqlerrm, 'SHOPIDIN = '||p_shopID, ' ', ' ');
          
          begin
            insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
              select cur_row.posnr,sysdate,'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS',cur_row.serv_ip,a.idm+1,'SERVER'
              from (select nvl(max(id),0) idm
                          from d_inet_book3
                          where posnr = cur_row.posnr and serv_ip = cur_row.serv_ip and init = 'SERVER') a;
              commit;
          end;
        end;
      end loop;
    end if;
    
  END BOOK_SHOP_CANCELLED;
	
	--передача заказа через rfc функцию в SAP
	PROCEDURE RFC_ORDER_SEND(p_posnr in varchar2, p_serv_ip in varchar2, p_syst in varchar2 default 'BWP599')
   AS 
  
		p_result varchar2(32767);
		p_id_fun number(5);
		p_struct_tables varchar2(32767);
		p_struct_field varchar2(32767);
		p_struct_tables_res varchar2(32767);
		p_struct_field_res varchar2(32767);
		BEGIN
      if is_hyb_order_prod(p_posnr,p_serv_ip) = 'F' and p_syst = 'BWP599' then
         return;
      end if;  
    
			select nvl(max(id_fun),-1) into p_id_fun
				from idocs.rfc_sap_fun
				where system = p_syst and fun_name = 'ZSD_CREATE_INET_REZERV';
				
			if p_id_fun = -1 then
				WRITE_ERROR_PROC('KAA_RFC_ORDER_SEND', p_posnr, '', 'Система не найдена', ' ', ' ');
				return;
			end if;
			
			--EXTID,KUNNR_IN,MANDT,MATNR,MEINS,NDS,NETWR,OMENG,ORDNR,PMENG,POSNR,STATUS,TMENG,WAERK
			p_struct_tables := 'select 
														null EXTID,case when nvl(b.id_shop_from,''null'') = ''KI1000'' then null 
															when substr(nvl(b.id_shop_from,''null''),1,2) = substr(nvl(b.id_shop_to,''null''),1,2) then e.shopnum
															else d.shopnum end KUNNR_IN,
														null MANDT,c.matnr MATNR,null MEINS,
														case when a.bit_delivery_addr = ''F'' then null else round_dig((b.sum*nvl(f.nds,0))/(100 + nvl(f.nds,0)), (case when g.landid = ''BY'' then 0.01 when g.landid = ''RU'' then 0.1 else 0.01 end),0) end NDS,
														case when a.bit_delivery_addr = ''F'' then b.sum else b.sum - round_dig((b.sum*nvl(f.nds,0))/(100 + nvl(f.nds,0)), (case when g.landid = ''BY'' then 0.01 when g.landid = ''RU'' then 0.1 else 0.01 end),0) end NETWR,
														null OMENG,
															--substr(''0000000000'',length(a.posnr)+1)||a.posnr ORDNR,
															to_number(case when g.landid = ''BY'' then ''2'' when g.landid = ''RU'' then ''1'' else ''0'' end || a.posnr) ORDNR,
															null PMENG,b.rnum POSNR,null STATUS,1 TMENG,
                              case when g.landid = ''BY'' then ''BYN'' when g.landid = ''RU'' then ''RUB'' else ''N\A'' end WAERK
														from d_inet_book1 a
														inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
														inner join s_all_mat c on b.art = c.art and b.asize = nvl(c.asize,0)
                            inner join HYBRIS.st_server_for_servlet g on g.server = a.serv_ip
														left join s_shop d on d.shopid = nvl(b.id_shop_from,''null'')
														left join firm.s_shop e on e.shopid = nvl(b.id_shop_to,''null'')
														left join s_nds f on b.art = f.art and b.asize = f.asize
														where c.trademark_sat != ''003'' and a.bit_book = ''F''
														and b.bit_move = ''F'' --and nvl(b.id_shop_to,''null'') != nvl(b.id_shop_from,''null'')
														--and not (nvl(b.id_shop_to,''null'') = ''NOT_OST'' or nvl(b.id_shop_from,''null'') = ''NOT_OST'')
														and a.posnr = '''||p_posnr||''' and a.serv_ip = '''||p_serv_ip||'''';
			
	--		p_struct_field := 'select distinct null "I_COMMIT",null "I_DO",null "IS_HEAD.BREAKDOWN",
	--												case when a.bit_delivery_addr = ''T'' then a.delivery_town else d.cityname end "IS_HEAD.CITY",
	--												null "IS_HEAD.ERDAT",
	--												null "IS_HEAD.ERDAT_ST",null "IS_HEAD.ERNAM",null "IS_HEAD.ERNAM_ST",null "IS_HEAD.ERZEZ",
	--												null "IS_HEAD.ERZEZ_ST",null "IS_HEAD.ISINWORK",
	--												case when a.bit_delivery_addr = ''T'' then null else d.shopnum end "IS_HEAD.KUNNR",
	--												''BY'' "IS_HEAD.LAND",null "IS_HEAD.MANDT",null "IS_HEAD.MBLNR_OUT",null "IS_HEAD.MJAHR_OUT",a.dated "IS_HEAD.ORDDAT"
	--												substr(''0000000000'',length(a.posnr)+1)||a.posnr "IS_HEAD.ORDNR",
	--												case when a.bit_online_pay = ''T'' then ''O'' else ''C'' end "IS_HEAD.PAY_METHOD",
	--												null "IS_HEAD.PROCESS",null "IS_HEAD.STATUS",
	--												case when a.bit_delivery_addr = ''T'' then a.delivery_address else d.address end  "IS_HEAD.STREET",
	--												null "IS_HEAD.VBELN_VA",null "IS_HEAD.VBELN_VL",
	--												case when a.bit_delivery_addr = ''T'' then ''A'' else ''P'' end "IS_HEAD.WAY_METHOD",
	--												null "IS_HEAD.XRET"
	--												from d_inet_book1 a
	--												inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
	--												left join s_shop d on d.shopid = b.id_shop
	--												where a.bit_book = ''F'' and b.bit_move = ''F''
	--												and a.posnr = '''||p_posnr||''' and a.serv_ip = '''||p_serv_ip||'''';
	
			--I_COMMIT,I_DO,IS_HEAD.BREAKDOWN,IS_HEAD.CITY,IS_HEAD.ERDAT,IS_HEAD.ERDAT_ST,IS_HEAD.ERNAM,
--			IS_HEAD.ERNAM_ST,IS_HEAD.ERZEZ,IS_HEAD.ERZEZ_ST,IS_HEAD.ISINWORK,IS_HEAD.KUNNR,IS_HEAD.LAND,
--			IS_HEAD.MANDT,IS_HEAD.MBLNR_OUT,IS_HEAD.MJAHR_OUT,IS_HEAD.ORDDAT,IS_HEAD.ORDNR,IS_HEAD.PAY_METHOD,
--			IS_HEAD.PROCESS,IS_HEAD.STATUS,IS_HEAD.STREET,IS_HEAD.VBELN_VA,IS_HEAD.VBELN_VL,IS_HEAD.WAY_METHOD,IS_HEAD.XRET
			p_struct_field := 'select distinct ''X'',''X'',null,
													case when a.bit_delivery_addr = ''T'' then a.delivery_town else d.cityname end,
													null,
													null,null,null,null,
													null,null,
													case when a.bit_delivery_addr = ''T'' then null else d.shopnum end,
													g.landid,null,null,null,a.dated,
													--substr(''0000000000'',length(a.posnr)+1)||a.posnr,
													to_number(case when g.landid = ''BY'' then ''2'' when g.landid = ''RU'' then ''1'' else ''0'' end || a.posnr),
													case when a.bit_online_pay = ''T'' then ''O'' else ''C'' end,
													null,null,
													case when a.bit_delivery_addr = ''T'' then a.delivery_address else d.address end,
													null,null,
													case when a.bit_delivery_addr = ''T'' then ''A'' else ''P'' end,
													null
													from d_inet_book1 a
													inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                          inner join HYBRIS.st_server_for_servlet g on g.server = a.serv_ip
													left join s_shop d on d.shopid = b.id_shop
													where a.bit_book = ''F'' and b.bit_move = ''F''
													and a.posnr = '''||p_posnr||''' and a.serv_ip = '''||p_serv_ip||'''';
													
			p_struct_tables_res := 'begin
																insert into firm.rfc_order_send_result_t (KUNNR_IN,MANDT,MATNR,MEINS,NETWR,OMENG,ORDNR,PMENG,POSNR,STATUS,TMENG,WAERK,SYST,POSNR_P,SERV_IP,NDS,EXTID,ID_FUN)
																	select KUNNR_IN,MANDT,MATNR,MEINS,NETWR,OMENG,ORDNR,PMENG,POSNR,STATUS,TMENG,WAERK,'''||p_syst||''' SYST,'''||p_posnr||''' POSNR_P,'''||p_serv_ip||''' SERV_IP,NDS,EXTID,'||p_id_fun||' ID_FUN
																		from idocs.WR_ET_DATA_'||p_id_fun||';
																end;';
																
			p_struct_field_res := 'begin
																insert into firm.rfc_order_send_result_s (BREAKDOWN,CITY,ERDAT,ERDAT_ST,ERNAM,
																																					ERNAM_ST,ERZEZ,ERZEZ_ST,ISINWORK,KUNNR,
																																					LAND,MANDT,MBLNR_OUT,MJAHR_OUT,ORDDAT,
																																					ORDNR,PAY_METHOD,PROCESS,STATUS,STREET,
																																					VBELN_VA,VBELN_VL,WAY_METHOD,XRET,SYST,POSNR_P,SERV_IP,ID_FUN)
																	select ''?ES_HEAD.BREAKDOWN?'',''?ES_HEAD.CITY?'',''?ES_HEAD.ERDAT?'',''?ES_HEAD.ERDAT_ST?'',''?ES_HEAD.ERNAM?'',''?ES_HEAD.ERNAM_ST?'',
																					''?ES_HEAD.ERZEZ?'',''?ES_HEAD.ERZEZ_ST?'',''?ES_HEAD.ISINWORK?'',''?ES_HEAD.KUNNR?'',''?ES_HEAD.LAND?'',
																					''?ES_HEAD.MANDT?'',''?ES_HEAD.MBLNR_OUT?'',''?ES_HEAD.MJAHR_OUT?'',''?ES_HEAD.ORDDAT?'',''?ES_HEAD.ORDNR?'',
																					''?ES_HEAD.PAY_METHOD?'',''?ES_HEAD.PROCESS?'',''?ES_HEAD.STATUS?'',''?ES_HEAD.STREET?'',''?ES_HEAD.VBELN_VA?'',
																					''?ES_HEAD.VBELN_VL?'',''?ES_HEAD.WAY_METHOD?'',''?ES_HEAD.XRET?'','''||p_syst||''' SYST, '''||p_posnr||''' POSNR_P, '''||p_serv_ip||''' SERV_IP,'||p_id_fun||' ID_FUN
																		from dual;
																commit;
																end;';																
													
			execute immediate 'update idocs.rfc_struct_tables
													set table_query = '''||replace(p_struct_tables,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and table_name = ''IT_DATA'' and in_or_out = ''IN''';
													
			execute immediate 'update idocs.rfc_struct_field
													set fields_query = '''||replace(p_struct_field,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and in_or_out = ''IN''';
													
			execute immediate 'update idocs.rfc_struct_tables
													set table_query = '''||replace(p_struct_tables_res,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and table_name = ''ET_DATA'' and in_or_out = ''OUT''';
													
			execute immediate 'update idocs.rfc_struct_field
													set fields_query = '''||replace(p_struct_field_res,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and in_or_out = ''OUT''';			
                          
      write_log('KAA_INET_BOOK', 'RFC_ORDER_SEND', 0, p_posnr, p_serv_ip, 'отправка заказа в SAP' , 'Начало');								
		
			commit;
      
      
			
			select KAA_JAVA_CALL_RFC_FUN('service-bw.tomcat.vit.belwest.com','ZSD_CREATE_INET_REZERV',p_syst) into p_result from dual;
			dbms_output.put_line(p_result);
      p_result := substr(p_result,1,200);
      
      write_log('KAA_INET_BOOK', 'RFC_ORDER_SEND', 0, p_posnr, p_serv_ip, 'отправка заказа в SAP' , 'Окончание', p_result);
			
			commit;
      
			if p_result not like '%IS_EXIST%' then
        if p_result != 'true' then
          WRITE_ERROR_PROC('KAA_RFC_ORDER_SEND', sqlcode, sqlerrm, p_posnr, p_result, ' ');
        else        
          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
            select p_posnr,sysdate,'ВЫПОЛНЕНА ПЕРЕДАЧА ЗАКАЗА В SAP '||p_syst,p_serv_ip,a.idm+1,'SERVER'
            from (select nvl(max(id),0) idm
                        from d_inet_book3
                        where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
        end if;
      end if;
      
      commit;
				
	    EXCEPTION
	    WHEN OTHERS THEN
	    rollback;
	    WRITE_ERROR_PROC('KAA_RFC_ORDER_SEND', sqlcode, sqlerrm, p_posnr, ' ', ' ');
	    raise;
			
		END RFC_ORDER_SEND;
		
		--изменение отправителя на id_shopin в заказе на перемещение
		PROCEDURE BOOK_UPDATE_MOVE(posnrin in varchar2, serv_ipin in varchar2, rnumin in number, id_shopin in varchar2)
		 AS 
		
		p_result varchar2(500);
		p_bit_book varchar2(5 CHAR);
		p_bit_delivery_addr varchar2(5 CHAR);
    p_bit_status varchar2(5 CHAR);
		p_main_shop varchar2(20 CHAR);
		p_sgp_perem varchar(5 CHAR);
		p_id_cancel number(5);
    p_perem_shop varchar2(20 CHAR);
    p_landid varchar2(20 CHAR);
    p_max_order_num number(5);
		BEGIN
		
			select bit_book,bit_delivery_addr,bit_status into p_bit_book,p_bit_delivery_addr,p_bit_status
				from d_inet_book1
				where posnr = posnrin and serv_ip = serv_ipin;
			
			--если бронирование, на выход	
			if p_bit_book = 'T' then
				return;
			end if;
      
      select nvl(max(landid),'BY') into p_landid
        from hybris.st_server_for_servlet
        where type = 'P' and server = serv_ipin;
        
      select nvl(max(order_num),0) + 1 into p_max_order_num
        from d_inet_book4
        where posnr = posnrin and serv_ip = serv_ipin;
			
			--записываю пункт назначения
			select max(id_shop) into p_main_shop
				from d_inet_book2
				where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F';
        
      --записываю магазин, на который создано текущее перемещение
      select nvl(max(id_shop),'null') into p_perem_shop
				from d_inet_book2
				where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move = 'T' and bit_delete = 'F';
			
			--помечаю флаг создания перемещение на СГП
			select nvl(case when max(lower(cityname)) like '%витебск%' then 'F' else 'F' end,'F') into p_sgp_perem
				from s_shop
				where shopid = id_shopin and id_shopin != 'KI1000';
			
			--ставлю метку удаления по позиции
			update d_inet_book2
				set bit_delete = 'T',bit_send = 'F'
				where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'T' and bit_delete = 'F' and rnum = rnumin
			;
			
			delete from d_inet_book4
				where bit_move = 'T' and posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin;
			
			--если доставка на адрес и пара поедет прямо с сгп
			if p_bit_delivery_addr = 'T' and id_shopin = 'KI1000' then
				update d_inet_book2
					set bit_send = 'F',id_shop_to = null,id_shop_from = 'KI1000'
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
				insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT)
					select POSNR,ART,ASIZE,KOL,SUM,id_shopin ID_SHOP,SERV_IP,RNUM,'T' BIT_MOVE,null ID_SHOP_TO,'N/A' ID_SHOP_FROM,BASE_PRICE,DISCOUNT
					from d_inet_book2
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
					
			--если доставка на адрес и пара едет на сгп с какого-то магазина
			elsif p_bit_delivery_addr = 'T' and id_shopin != 'KI1000' then
				update d_inet_book2
					set bit_send = 'F',id_shop_to = 'KI1000',id_shop_from = id_shopin
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
				insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT)
					select POSNR,ART,ASIZE,KOL,SUM,id_shopin ID_SHOP,SERV_IP,RNUM,'T' BIT_MOVE,'KI1000' ID_SHOP_TO,'N/A' ID_SHOP_FROM,BASE_PRICE,DISCOUNT
					from d_inet_book2
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
					
			--если самовывоз и пара в пункте назначения
			elsif p_bit_delivery_addr = 'F' and id_shopin = p_main_shop then
				update d_inet_book2
					set bit_send = 'F',id_shop_to = id_shopin,id_shop_from = id_shopin
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
					
			--если самовывоз и пара едет с СГП
			elsif p_bit_delivery_addr = 'F' and id_shopin = 'KI1000' then
				update d_inet_book2
					set bit_send = 'F',id_shop_to = p_main_shop,id_shop_from = 'KI1000'
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
				insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT)
					select POSNR,ART,ASIZE,KOL,SUM,id_shopin ID_SHOP,SERV_IP,RNUM,'T' BIT_MOVE,p_main_shop ID_SHOP_TO,'N/A' ID_SHOP_FROM,BASE_PRICE,DISCOUNT
					from d_inet_book2
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
					
			--если самовывоз и пара перемещается с витебского магазина на сгп
			elsif p_bit_delivery_addr = 'F' and id_shopin != 'KI1000' and p_sgp_perem = 'T' then
				update d_inet_book2
					set bit_send = 'F',id_shop_to = 'KI1000',id_shop_from = id_shopin
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
				insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT)
					select POSNR,ART,ASIZE,KOL,SUM,id_shopin ID_SHOP,SERV_IP,RNUM,'T' BIT_MOVE,'KI1000' ID_SHOP_TO,'N/A' ID_SHOP_FROM,BASE_PRICE,DISCOUNT
					from d_inet_book2
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
					
			--если самовывоз и пара перемещается из магазина РБ
			elsif p_bit_delivery_addr = 'F' and id_shopin != 'KI1000' and p_sgp_perem = 'F' then
				update d_inet_book2
					set bit_send = 'F',id_shop_to = p_main_shop,id_shop_from = id_shopin
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
				insert into d_inet_book2 (POSNR,ART,ASIZE,KOL,SUM,ID_SHOP,SERV_IP,RNUM,BIT_MOVE,ID_SHOP_TO,ID_SHOP_FROM,BASE_PRICE,DISCOUNT)
					select POSNR,ART,ASIZE,KOL,SUM,id_shopin ID_SHOP,SERV_IP,RNUM,'T' BIT_MOVE,p_main_shop ID_SHOP_TO,'N/A' ID_SHOP_FROM,BASE_PRICE,DISCOUNT
					from d_inet_book2
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
			else
				WRITE_ERROR_PROC('KAA_BOOK_UPDATE_MOVE', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin||', '||id_shopin, 'Пропущены все условия!');
				return;
			end if;
			
			if id_shopin != p_main_shop then
				insert into d_inet_book4 (POSNR,SERV_IP,RNUM,BIT_MOVE,ID_SHOP,DPD_ID)
						select POSNR,SERV_IP,RNUM,'T' BIT_MOVE,id_shopin ID_SHOP,DPD_ID
						from d_inet_book4
						where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
			end if;
      
      if p_landid = 'BY' then
        for cur in (select x.rnum,x.bit_move,(dense_rank() over (partition by posnr order by num,bit_move,nvl(id_shop_from,'nn')))+p_max_order_num rn
                    from (
                      select a.*,case when nvl(id_shop_from,'nn') = id_shop then 1 else 2 end num
                      from d_inet_book2 a
                      where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin
                    ) x) loop
          begin
            update d_inet_book4
            set order_num = cur.rn
            where posnr = posnrin and serv_ip = serv_ipin and rnum = cur.rnum and bit_move = cur.bit_move
            ;
          end;
        end loop;
      end if;
			
			--удаляю записи с меткой удаления, если с таким же ключом есть запись без метки удаления
			delete from d_inet_book2 a
				where a.posnr = posnrin and a.serv_ip = serv_ipin and a.rnum = rnumin and a.bit_move = 'T' and a.bit_delete = 'T'
				and (a.POSNR,a.ID_SHOP,a.SERV_IP,a.RNUM) in (select b.POSNR,b.ID_SHOP,b.SERV_IP,b.RNUM
																											from d_inet_book2 b 
																											where b.bit_delete = 'F' and a.bit_move = b.bit_move and a.POSNR = b.posnr and a.ID_SHOP = b.id_shop
																											and a.SERV_IP = b.serv_ip and a.RNUM = b.rnum);
				
			insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
				select posnrin,sysdate,'ПЕРЕФОРМИРОВАНО ПЕРЕМЕЩЕНИЕ',serv_ipin,rnumin,a.idm+1,'SERVER'
				from (select nvl(max(id),0) idm
										from d_inet_book3
										where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                    
      --для перемещений с сгп и заказов с доставкой на адрес восстанавливаю сразу
      update d_inet_book2
        set date_cancel = null,bit_block = 'T',id_cancel = null
        where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_delete = 'F'
          and id_shop in ('KI1000','N/A');
										
			--если позиция отменена в пункте самовывоза, ставлю метку отката отмены
			select nvl(max(id_cancel),0) into p_id_cancel
				from d_inet_book2
				where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
				
			if p_bit_delivery_addr = 'F' and p_id_cancel > 0 then
				update d_inet_book2
					set bit_cancel_reverse = 'T',bit_cancel_firm = 'F'
					where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F'
            and id_shop not in ('KI1000','N/A');
			end if;
      
      --если заказ отменен, восстанавливаю
      if p_bit_status = 'F' then
         update d_inet_book1
                set bit_hyb_cancel = 'F',bit_hyb_ready = 'F',bit_status = 'T',op_id_cancel = null
                where posnr = posnrin and serv_ip = serv_ipin;
         update d_inet_book4
                set bit_hyb_cancel = 'F',bit_hyb_ready = 'F',op_id_cancel = null
                where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F' and bit_hyb_receive = 'F';
         select KAA_JAVA_HYBRIS_EXCH_FUN(serv_ipin,'order',posnrin, 
                                          case when p_main_shop like '00%' then 'ST_BY_'||p_main_shop when p_main_shop like 'N/A%' then 'ST_'||p_landid||'_0001' else 'ST_RU_'||p_main_shop end,
                                          'CREATED')
                into p_result
                from dual;
         if p_result like '%OK%' then
            insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                select posnrin,sysdate,'ВОССТАНОВЛЕН В HYBRIS ',serv_ipin,a.idm+1,'FIRM'
                from (select nvl(max(id),0) idm
                from d_inet_book3
                where posnr = posnrin and serv_ip = serv_ipin and init = 'FIRM') a;
                
                p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(serv_ipin, posnrin);
         else
            insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                select posnrin,sysdate,'ОШИБКА СВЯЗИ С HYBRIS',serv_ipin,a.idm+1,'FIRM'
                from (select nvl(max(id),0) idm
                from d_inet_book3
                where posnr = posnrin and serv_ip = serv_ipin and init = 'FIRM') a;
         end if;
      end if;
			
			commit;
      
      --меняю остатки по заказу в hybris
      if p_perem_shop != 'null' then
         update_hybris_stock(posnrin, serv_ipin,rnumin,'REFORM_PLUS',p_perem_shop);
      end if;
      update_hybris_stock(posnrin, serv_ipin,rnumin,'REFORM_MINUS');
			
      --делаю обмен
			book_exchange(case when is_hyb_serv_prod(serv_ipin) = 'T' then p_main_shop else 'S888' end,posnrin,serv_ipin,'F',null,p_result);
			if p_main_shop != id_shopin then
				book_exchange(case when is_hyb_serv_prod(serv_ipin) = 'T' then id_shopin else 'S888' end,posnrin,serv_ipin,'F',null,p_result);
			end if;
			
			for cur_row in (select distinct id_shop
											from d_inet_book2_v
											where bit_delete = 'T' and posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin) loop
        begin
					book_exchange(cur_row.id_shop,posnrin,serv_ipin,'F',null,p_result);
				end;
			end loop;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_BOOK_UPDATE_MOVE', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END BOOK_UPDATE_MOVE;
		
		PROCEDURE RFC_ORDER_CH_UPD(p_posnr in varchar2, p_serv_ip in varchar2, p_check in varchar2, p_change in varchar2, p_syst in varchar2 default 'BWP599',p_error in varchar2 default 'F')
		AS 
	
		p_result varchar2(32767);
		p_id_fun number(5);
		p_struct_tables varchar2(32767);
		p_struct_field varchar2(32767);
		p_struct_tables_res varchar2(32767);
		p_struct_tables_res2 varchar2(32767);
		p_struct_field_res varchar2(32767);
    p_bit_book varchar2(5);
    p_cur_ret_nums varchar2(1000);
    p_otk varchar2(1000 CHAR);
		BEGIN
      if is_hyb_serv_prod(p_serv_ip) = 'F' or is_hyb_order_prod(p_posnr,p_serv_ip) = 'F' and p_syst = 'BWP599' then
         return;   
      end if;
      
      select nvl(max(bit_book),'T') into p_bit_book
        from d_inet_book1
        where posnr = p_posnr and serv_ip = p_serv_ip;
        
      if p_bit_book = 'T' then
         return;
      end if;
      
			select nvl(max(id_fun),-1) into p_id_fun
				from idocs.rfc_sap_fun
				where system = p_syst and fun_name = 'ZSD_CHECK_N_CHANGE_INET_ORD';
				
			if p_id_fun = -1 then
				WRITE_ERROR_PROC('KAA_RFC_ORDER_CH_UPD', p_posnr, '', 'Система не найдена', ' ', ' ');
				return;
			end if;
			
      --DPDNR,EXTID,KUNNR_IN,MANDT,MATNR,MEINS,NDS,NETWR,OMENG,ORDNR,PMENG,POSNR,STATUS,TMENG,WAERK
			p_struct_tables := 'select h.dpd_id DPDNR,
														case when nvl(b.id_shop_to,''null'') = ''KI1000'' then g.rx_id else null end EXTID,
                            case when nvl(b.id_shop_from,''null'') = ''KI1000'' then null 
															when substr(nvl(b.id_shop_from,''null''),1,2) = substr(nvl(b.id_shop_to,''null''),1,2) then e.shopnum
															else d.shopnum end KUNNR_IN,
														null MANDT,c.matnr MATNR,null MEINS,
														case when a.bit_delivery_addr = ''F'' then null else firm.round_dig((b.sum*nvl(f.nds,0))/(100 + nvl(f.nds,0)), (case when h.landid = ''BY'' then 0.01 when h.landid = ''RU'' then 0.1 else 0.01 end),0) end NDS,
														case when a.bit_delivery_addr = ''F'' then b.sum else b.sum - firm.round_dig((b.sum*nvl(f.nds,0))/(100 + nvl(f.nds,0)), (case when h.landid = ''BY'' then 0.01 when h.landid = ''RU'' then 0.1 else 0.01 end),0) end NETWR,
														null OMENG,
															--substr(''0000000000'',length(a.posnr)+1)||a.posnr ORDNR,
															case when to_number(a.posnr) = to_number(nvl(i.ordnr,''0'')) then to_number(a.posnr) else to_number(case when h.landid = ''BY'' then ''2'' when h.landid = ''RU'' then ''1'' else ''0'' end || a.posnr) end ORDNR,
															null PMENG,b.rnum POSNR,
                              case when h.bit_hyb_cancel = ''T'' then ''CL'' else null end STATUS,
                              1 TMENG,
                              case when h.landid = ''BY'' then ''BYN'' when h.landid = ''RU'' then ''RUB'' else ''N\A'' end WAERK
														from firm.d_inet_book1 a
														inner join firm.d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                            inner join firm.d_inet_book4 h on b.posnr = h.posnr and b.serv_ip = h.serv_ip and b.rnum = h.rnum and b.bit_move = h.bit_move
														inner join firm.s_all_mat c on b.art = c.art and b.asize = nvl(c.asize,0)
														inner join HYBRIS.st_server_for_servlet h on h.server = a.serv_ip
                            left join firm.s_shop d on d.shopid = nvl(b.id_shop_from,''null'')
														left join firm.s_shop e on e.shopid = nvl(b.id_shop_to,''null'')
														left join firm.s_nds f on b.art = f.art and b.asize = f.asize
                            left join firm.d_inet_book2 g on g.posnr = b.posnr and g.serv_ip = b.serv_ip and g.rnum = b.rnum and g.bit_delete = ''F'' and g.bit_move = ''T''
														left join (select posnr_p posnr,serv_ip,max(ordnr) ordnr
																				from rfc_order_send_result_s
																				where id_fun = 7
																				group by posnr_p,serv_ip) i on i.posnr = a.posnr and i.serv_ip = a.serv_ip
                            where c.trademark_sat != ''003'' and a.bit_book = ''F''
														and b.bit_move = ''F'' --and nvl(b.id_shop_to,''null'') != nvl(b.id_shop_from,''null'')
														--and not (nvl(b.id_shop_to,''null'') = ''NOT_OST'' or nvl(b.id_shop_from,''null'') = ''NOT_OST'')
														and a.posnr = '''||p_posnr||''' and a.serv_ip = '''||p_serv_ip||'''';
      --dbms_output.put_line(p_struct_tables);
	
      --I_CHANGE,I_CHECK,I_COMMIT,
      --IS_HEAD.BREAKDOWN,IS_HEAD.CITY,IS_HEAD.ERDAT,IS_HEAD.ERDAT_ST,IS_HEAD.ERNAM,IS_HEAD.ERNAM_ST,IS_HEAD.ERZEZ,
      --IS_HEAD.ERZEZ_ST,IS_HEAD.ISINWORK,IS_HEAD.KUNNR,IS_HEAD.LAND,IS_HEAD.MANDT,IS_HEAD.MBLNR_OUT,IS_HEAD.MJAHR_OUT,
      --IS_HEAD.ORDDAT,IS_HEAD.ORDNR,IS_HEAD.PAY_METHOD,IS_HEAD.PROCESS,IS_HEAD.STATUS,IS_HEAD.STREET,IS_HEAD.VBELN_VA,
      --IS_HEAD.VBELN_VL,IS_HEAD.WADAT_IST,IS_HEAD.WAY_METHOD,IS_HEAD.XCONFIRM,IS_HEAD.XOTKCONF,IS_HEAD.XPACK,IS_HEAD.XRET
			
			p_struct_field := 'select distinct '''||p_change||''','''||p_check||''','''||p_change||''',null,
													case when a.bit_delivery_addr = ''T'' then a.delivery_town else d.cityname end,
													null,
													null,null,null,null,
													null,null,
													case when a.bit_delivery_addr = ''T'' then null else d.shopnum end,
													g.landid,null,null,null,a.dated,
													--substr(''0000000000'',length(a.posnr)+1)||a.posnr,
													case when to_number(a.posnr) = to_number(nvl(i.ordnr,''0'')) then to_number(a.posnr) else to_number(case when g.landid = ''BY'' then ''2'' when g.landid = ''RU'' then ''1'' else ''0'' end || a.posnr) end,
													case when a.bit_online_pay = ''T'' then ''O'' else ''C'' end,
													null,null,
													case when a.bit_delivery_addr = ''T'' then a.delivery_address else d.address end,
													null,null,
                          case when a.bit_delivery_addr = ''T'' then trunc(rx.dated) else null end,
													case when a.bit_delivery_addr = ''T'' then ''A'' else ''P'' end,
													case when a.bit_phone = ''T'' then ''X'' else null end,
                          null,null,null
													from d_inet_book1 a
													inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                          inner join HYBRIS.st_server_for_servlet g on g.server = a.serv_ip
													left join s_shop d on d.shopid = b.id_shop
                          left join (select posnr_p posnr,serv_ip,max(ordnr) ordnr
																				from rfc_order_send_result_s
																				where id_fun = 7
																				group by posnr_p,serv_ip) i on i.posnr = a.posnr and i.serv_ip = a.serv_ip
                          left join (select posnr,serv_ip,id_shop,rx_id
                                      from (
                                            select posnr,serv_ip,id_shop,nvl(rx_id,0) rx_id,row_number() over (partition by posnr,serv_ip order by nvl(rx_id,0) desc) rn
                                            from d_inet_book2
                                            where bit_move = ''T'' and bit_delete = ''F'')
                                      where rn = 1) b2_rx on b2_rx.posnr = a.posnr and b2_rx.serv_ip = a.serv_ip
													left join d_rasxod1 rx on rx.id = b2_rx.rx_id and rx.id_shop = b2_rx.id_shop
													where a.bit_book = ''F'' and b.bit_move = ''F''
													and a.posnr = '''||p_posnr||''' and a.serv_ip = '''||p_serv_ip||'''';			
      --dbms_output.put_line(p_struct_field);							
													
			p_struct_tables_res := 'begin
																delete from firm.rfc_order_send_result_t
																	where syst = '''||p_syst||''' and posnr_p = '''||p_posnr||''' and serv_ip = '''||p_serv_ip||''' and id_fun = '||p_id_fun||';
																insert into firm.rfc_order_send_result_t (KUNNR_IN,MANDT,MATNR,MEINS,NETWR,OMENG,ORDNR,PMENG,POSNR,STATUS,TMENG,WAERK,SYST,POSNR_P,SERV_IP,NDS,EXTID,DPDNR,ID_FUN)
																	select KUNNR_IN,MANDT,MATNR,MEINS,NETWR,OMENG,ORDNR,PMENG,POSNR,STATUS,TMENG,WAERK,'''||p_syst||''' SYST,'''||p_posnr||''' POSNR_P,'''||p_serv_ip||''' SERV_IP,NDS,EXTID,DPDNR,'||p_id_fun||'
																		from idocs.WR_ET_DATA_'||p_id_fun||';
																commit;
																end;';
																
			p_struct_tables_res2 := 'begin
																delete from firm.rfc_order_ch_upd_result_t
																	where syst = '''||p_syst||''' and posnr_p = '''||p_posnr||''' and serv_ip = '''||p_serv_ip||''' and id_fun = '||p_id_fun||';
																insert into firm.rfc_order_ch_upd_result_t (CHARG,EXIDV,MANDT,MBLNR_2,MEINS,MJAHR_2,OMENG,ORDNR,POSNR,TMENG,VBELN_2,SYST,POSNR_P,SERV_IP,ID_FUN)
																	select CHARG,EXIDV,MANDT,MBLNR_2,MEINS,MJAHR_2,OMENG,ORDNR,POSNR,TMENG,VBELN_2,'''||p_syst||''' SYST,'''||p_posnr||''' POSNR_P,'''||p_serv_ip||''' SERV_IP,'||p_id_fun||'
																		from idocs.WR_ET_UNITS_'||p_id_fun||';
																commit;
																end;';											
																                              
			p_struct_field_res := 'begin
																delete from firm.rfc_order_send_result_s
																	where syst = '''||p_syst||''' and posnr_p = '''||p_posnr||''' and serv_ip = '''||p_serv_ip||''' and id_fun = '||p_id_fun||';
																insert into firm.rfc_order_send_result_s (BREAKDOWN,CITY,ERDAT,ERDAT_ST,ERNAM,
																																					ERNAM_ST,ERZEZ,ERZEZ_ST,ISINWORK,KUNNR,
																																					LAND,MANDT,MBLNR_OUT,MJAHR_OUT,ORDDAT,
																																					ORDNR,PAY_METHOD,PROCESS,STATUS,STREET,
																																					VBELN_VA,VBELN_VL,WADAT_IST,WAY_METHOD,XCONFIRM,XPACK,XRET,XOTKCONF,SYST,POSNR_P,SERV_IP,ID_FUN)
																	select ''?ES_HEAD.BREAKDOWN?'',''?ES_HEAD.CITY?'',''?ES_HEAD.ERDAT?'',''?ES_HEAD.ERDAT_ST?'',''?ES_HEAD.ERNAM?'',''?ES_HEAD.ERNAM_ST?'',
																					''?ES_HEAD.ERZEZ?'',''?ES_HEAD.ERZEZ_ST?'',''?ES_HEAD.ISINWORK?'',''?ES_HEAD.KUNNR?'',''?ES_HEAD.LAND?'',
																					''?ES_HEAD.MANDT?'',''?ES_HEAD.MBLNR_OUT?'',''?ES_HEAD.MJAHR_OUT?'',''?ES_HEAD.ORDDAT?'',''?ES_HEAD.ORDNR?'',
																					''?ES_HEAD.PAY_METHOD?'',''?ES_HEAD.PROCESS?'',''?ES_HEAD.STATUS?'',''?ES_HEAD.STREET?'',''?ES_HEAD.VBELN_VA?'',
																					''?ES_HEAD.VBELN_VL?'',''?ES_HEAD.WADAT_IST?'',''?ES_HEAD.WAY_METHOD?'',''?ES_HEAD.XCONFIRM?'',''?ES_HEAD.XPACK?'',''?ES_HEAD.XRET?'',''?ES_HEAD.XOTKCONF?'','''||p_syst||''' SYST, '''||p_posnr||''' POSNR_P, '''||p_serv_ip||''' SERV_IP,'||p_id_fun||' ID_FUN
																		from dual;
																commit;
																end;';																
													
			execute immediate 'update idocs.rfc_struct_tables
													set table_query = '''||replace(p_struct_tables,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and table_name = ''IT_DATA'' and in_or_out = ''IN''';
													
			execute immediate 'update idocs.rfc_struct_field
													set fields_query = '''||replace(p_struct_field,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and in_or_out = ''IN''';
													
			execute immediate 'update idocs.rfc_struct_tables
													set table_query = '''||replace(p_struct_tables_res,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and table_name = ''ET_DATA'' and in_or_out = ''OUT''';
													
			execute immediate 'update idocs.rfc_struct_tables
													set table_query = '''||replace(p_struct_tables_res2,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and table_name = ''ET_UNITS'' and in_or_out = ''OUT''';													
													
			execute immediate 'update idocs.rfc_struct_field
													set fields_query = '''||replace(p_struct_field_res,'''','''''')||'''
													where id_fun = '||p_id_fun||'
													and in_or_out = ''OUT''';													
		
      if p_change = 'X' then
         select nvl(max(rx),'N/A') into p_cur_ret_nums
          from (select listagg(rnum||' '||id_shop||' '||nvl(to_char(rx_id),'N/A'),' | ') within group (order by rnum) rx
                from d_inet_book2	
                where posnr = p_posnr and serv_ip = p_serv_ip and bit_move = 'T' and bit_delete = 'F');  
      
         write_log('KAA_INET_BOOK', 'RFC_ORDER_CH_UPD', 0, p_posnr, p_serv_ip, 'отправка изменений по заказу в SAP' , 'Начало',p_cur_ret_nums);
         
         merge into rfc_inet_order_edit a using
               (select p_posnr posnr,p_serv_ip serv_ip from dual) b
               on (a.posnr = b.posnr and a.serv_ip = b.serv_ip)
         when not matched then
           insert (posnr,serv_ip)
           values (b.posnr,b.serv_ip);
      end if;

			commit;
			
			select KAA_JAVA_CALL_RFC_FUN('service-bw.tomcat.vit.belwest.com','ZSD_CHECK_N_CHANGE_INET_ORD',p_syst) into p_result from dual;
      
      select nvl(max(xotkconf),'N/A') into p_otk
        from rfc_order_send_result_s
        where posnr_p = p_posnr and serv_ip = p_serv_ip and id_fun = p_id_fun;
        
      select p_otk||' - '||nvl(max(scans),'N/A') into p_otk
        from (select listagg(exidv,',') within group (order by posnr) scans
              from rfc_order_ch_upd_result_t
              where posnr_p = p_posnr and serv_ip = p_serv_ip and id_fun = p_id_fun);
      
      write_log('KAA_INET_BOOK', 'RFC_ORDER_CH_UPD', 0, p_posnr, p_serv_ip,'Проверка ОТК',p_otk);
			dbms_output.put_line(p_result);
      p_result := substr(p_result,1,200);
      
      if p_change = 'X' then
         write_log('KAA_INET_BOOK', 'RFC_ORDER_CH_UPD', 0, p_posnr, p_serv_ip, 'отправка изменений по заказу в SAP' , 'Окончание', p_result);
      end if;
			
			commit;
			
			if p_result != 'true' then
				WRITE_ERROR_PROC('KAA_RFC_ORDER_CH_UPD', sqlcode, sqlerrm, p_posnr, p_result, ' ');
        
        if p_change = 'X' then
          update rfc_inet_order_edit
            set status = 'E'
            where posnr = p_posnr and serv_ip = p_serv_ip;
        end if;
        
        if p_error = 'T' then
          commit;
          raise_application_error(-20000,'Ошибка при обмене с SAP - '||substr(p_result,1,30));
          return;
        end if;
			else
				if p_change = 'X' then
					insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
						select p_posnr,sysdate,'ВЫПОЛНЕНА ПЕРЕДАЧА ИЗМЕНЕНИЙ ПО ЗАКАЗУ В SAP '||p_syst,p_serv_ip,a.idm+1,'SERVER'
						from (select nvl(max(id),0) idm
												from d_inet_book3
												where posnr = p_posnr and serv_ip = p_serv_ip and init = 'SERVER') a;
          delete from rfc_inet_order_edit
                 where posnr = p_posnr and serv_ip = p_serv_ip;
				end if;
			end if;
      
      commit;
				
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_RFC_ORDER_CH_UPD', sqlcode, sqlerrm, p_posnr, ' ', ' ');
			raise;
			
		END RFC_ORDER_CH_UPD;
		
		--обновление статусов по заказу в сапе по активным заказам
		PROCEDURE ORDER_STATUS_UPDATE
		 AS 
		cursor orders is 
		select distinct a.posnr,a.serv_ip
			from d_inet_book1 a
			inner join d_inet_book_active b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
			where a.bit_book = 'F' and is_hyb_serv_prod(a.serv_ip) = 'T' and is_hyb_order_prod(a.posnr,a.serv_ip) = 'T'
			order by a.posnr;
				
		ordersRow orders%rowtype;
		p_posnr varchar2(20);
		p_serv_ip varchar2(50);
		result_p varchar2(5000 CHAR);
		BEGIN
		 
		open orders;
			loop  
					 FETCH orders INTO p_posnr,p_serv_ip;
					 EXIT WHEN orders%NOTFOUND;  
							begin
							RFC_ORDER_CH_UPD(p_posnr, p_serv_ip, 'X', null, 'BWP599');
							commit;
								EXCEPTION
								WHEN OTHERS THEN
								dbms_output.put_line(sqlcode||sqlerrm);
								WRITE_ERROR_PROC('KAA_ORDER_STATUS_UPDATE', sqlcode, sqlerrm, 'POSNR = '||p_posnr, ' ', ' ');
								rollback;
							end;
			end loop;
			
		END ORDER_STATUS_UPDATE;
    
    --отправка в сап заказов, которые не отправились автоматом
		PROCEDURE ORDER_SAP_EMPTY_EXCHANGE
		 AS 
		cursor orders is 
		select distinct a.posnr,a.serv_ip
			from d_inet_book_sap_empty a
      where is_hyb_order_prod(a.posnr,a.serv_ip) = 'T'
			order by a.posnr;
				
		ordersRow orders%rowtype;
		p_posnr varchar2(20);
		p_serv_ip varchar2(50);
		result_p varchar2(5000 CHAR);
		BEGIN
		 
		open orders;
			loop  
					 FETCH orders INTO p_posnr,p_serv_ip;
					 EXIT WHEN orders%NOTFOUND;  
							begin
							RFC_ORDER_SEND(p_posnr, p_serv_ip);
							commit;
								EXCEPTION
								WHEN OTHERS THEN
								dbms_output.put_line(sqlcode||sqlerrm);
								WRITE_ERROR_PROC('KAA_ORDER_SAP_EMPTY_EXCHANGE', sqlcode, sqlerrm, 'POSNR = '||p_posnr, ' ', ' ');
								rollback;
							end;
			end loop;
			
		END ORDER_SAP_EMPTY_EXCHANGE;
    
    --отмена или восстановление позиции в заказе
    PROCEDURE ORDER_CANC_REST_POSITION(commandin in varchar2, posnrin in varchar2, serv_ipin in varchar2, rnumin in number, id_cancelin in number default 0)
		 AS 
		
		p_result varchar2(500);
		p_bit_book varchar2(5 CHAR);
    p_bit_status varchar2(5 CHAR);
    p_bit_delivery_addr varchar2(5 CHAR);
		p_main_shop varchar2(20 CHAR);
		p_id_cancel number(5);
    p_landid varchar2(20);
		BEGIN
		
			select bit_book,bit_delivery_addr,bit_status into p_bit_book,p_bit_delivery_addr,p_bit_status
				from d_inet_book1
				where posnr = posnrin and serv_ip = serv_ipin;
			
			--если бронирование, на выход	
			if p_bit_book = 'T' then
				return;
			end if;
			
			--записываю пункт назначения
			select max(id_shop),nvl(max(id_cancel),0) into p_main_shop,p_id_cancel
				from d_inet_book2
				where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move = 'F';
        
      if commandin = 'CANCEL' then
         GOTO CANCEL;
      elsif commandin = 'RESTORE' then
         GOTO RESTORE;
      else
        WRITE_ERROR_PROC('KAA_ORDER_CANC_REST_POSITION', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Пропущены все условия!');
				return;
      end if;
      
      <<CANCEL>>
        if id_cancelin = 0 then
           return;
        end if;  
      
        update d_inet_book4
          set op_id_cancel = id_cancelin,bit_hyb_cancel = 'T',bit_hyb_ready = 'F'
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin;
          
        --для перемещений с сгп и заказов с доставкой на адрес отменяю сразу
        update d_inet_book2
          set date_cancel = sysdate,bit_block = 'F',id_cancel = id_cancelin
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_delete = 'F' and nvl(id_cancel,0) = 0
            and id_shop in ('KI1000','N/A');
        
        --ставлю флаг отмены позиции заказа в центре, чтобы при обмене отменить ее в магазине
        update d_inet_book2
          set bit_cancel_firm = 'T',bit_cancel_reverse = 'F',bit_send = 'F'
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_delete = 'F' and nvl(id_cancel,0) = 0
            and id_shop not in ('KI1000','N/A');
          
        insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
          select posnrin,sysdate,'ОТМЕНЕНА ПОЗИЦИЯ',serv_ipin,rnumin,a.idm+1,'SERVER'
          from (select nvl(max(id),0) idm
                      from d_inet_book3
                      where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                      
        update_hybris_stock(posnrin, serv_ipin,rnumin,'CANCEL');
        
        GOTO QUIT;
      
      <<RESTORE>>
        update d_inet_book4
          set op_id_cancel = null,bit_hyb_cancel = 'F',bit_hyb_ready = 'F'
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin;
          
        --для перемещений с сгп и заказов с доставкой на адрес восстанавливаю сразу
        update d_inet_book2
          set date_cancel = null,bit_block = 'T',id_cancel = null
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_delete = 'F'
            and id_shop in ('KI1000','N/A');
        
        --убираю флаг отмены позиции заказа в центре
        update d_inet_book2
          set bit_cancel_firm = 'F',bit_send = 'F'
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_delete = 'F'
            and id_shop not in ('KI1000','N/A');
      
        --если позиция отменена в пункте самовывоза, ставлю метку отката отмены
        select nvl(max(id_cancel),0) into p_id_cancel
          from d_inet_book2
          where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F';
  				
        if p_bit_delivery_addr = 'F' and p_id_cancel > 0 then
          update d_inet_book2
            set bit_cancel_reverse = 'T'
            where posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin and bit_move= 'F'
              and id_shop not in ('KI1000','N/A');
        end if;
        
        --если заказ отменен, восстанавливаю
        if p_bit_status = 'F' then
           update d_inet_book1
                  set bit_hyb_cancel = 'F',bit_hyb_ready = 'F',bit_status = 'T',op_id_cancel = null
                  where posnr = posnrin and serv_ip = serv_ipin;
           update d_inet_book4
                  set bit_hyb_cancel = 'F',bit_hyb_ready = 'F',op_id_cancel = null
                  where posnr = posnrin and serv_ip = serv_ipin and bit_move = 'F' and bit_hyb_receive = 'F';
           select nvl(max(landid),'BY') into p_landid
            from hybris.st_server_for_servlet
            where type = 'P' and server = serv_ipin; 
           select KAA_JAVA_HYBRIS_EXCH_FUN(serv_ipin,'order',posnrin, 
                                            case when p_main_shop like '00%' then 'ST_BY_'||p_main_shop when p_main_shop like 'N/A%' then 'ST_'||p_landid||'_0001' else 'ST_RU_'||p_main_shop end,
                                            'CREATED')
                  into p_result
                  from dual;
           if p_result like '%OK%' then
              insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                  select posnrin,sysdate,'ВОССТАНОВЛЕН В HYBRIS ',serv_ipin,a.idm+1,'FIRM'
                  from (select nvl(max(id),0) idm
                  from d_inet_book3
                  where posnr = posnrin and serv_ip = serv_ipin and init = 'FIRM') a;
                  
              delete from d_inet_book3
                where posnr = posnrin and serv_ip = serv_ipin
                and cond like '%ПОДТВЕРЖДЕНО В HYBRIS%';
                  
              p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(serv_ipin, posnrin);
           else
              insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                  select posnrin,sysdate,'ОШИБКА СВЯЗИ С HYBRIS',serv_ipin,a.idm+1,'FIRM'
                  from (select nvl(max(id),0) idm
                  from d_inet_book3
                  where posnr = posnrin and serv_ip = serv_ipin and init = 'FIRM') a;
           end if;
        end if;
        
        insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
          select posnrin,sysdate,'ВОССТАНОВЛЕНА ПОЗИЦИЯ',serv_ipin,rnumin,a.idm+1,'SERVER'
          from (select nvl(max(id),0) idm
                      from d_inet_book3
                      where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                      
        update_hybris_stock(posnrin, serv_ipin,rnumin,'RESTORE');
      
        GOTO QUIT;
      
      <<QUIT>>
			
			commit;
			
			book_exchange(case when is_hyb_serv_prod(serv_ipin) = 'T' then p_main_shop else 'S888' end,posnrin,serv_ipin,'F',null,p_result);			
			for cur_row in (select distinct id_shop
											from d_inet_book2_v
											where bit_move = 'T' and posnr = posnrin and serv_ip = serv_ipin and rnum = rnumin) loop
        begin
					book_exchange(cur_row.id_shop,posnrin,serv_ipin,'F',null,p_result);
				end;
			end loop;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_ORDER_CANC_REST_POSITION', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END ORDER_CANC_REST_POSITION;
    
    --обновление остатков в hybris по конкретной позиции заказа
    PROCEDURE UPDATE_HYBRIS_STOCK(posnrin in varchar2, serv_ipin in varchar2, rnumin in number, triggerin in varchar2, id_shop_delin in varchar2 default null)
		 AS 
		
		p_result varchar2(500);
		p_bit_book varchar2(5 CHAR);
    p_bit_status varchar2(5 CHAR);
    p_bit_delivery_addr varchar2(5 CHAR);
		BEGIN
      
      if is_hyb_serv_prod(serv_ipin) = 'F' or is_hyb_order_prod(posnrin,serv_ipin) = 'F' then
         return;
      end if;
		
			select bit_book,bit_delivery_addr,bit_status into p_bit_book,p_bit_delivery_addr,p_bit_status
				from d_inet_book1
				where posnr = posnrin and serv_ip = serv_ipin;
        
      if triggerin = 'BOOK_INSERT' then
         GOTO BOOK_INSERT;
      elsif triggerin = 'REFORM_PLUS' then
         GOTO REFORM_PLUS;
      elsif triggerin = 'REFORM_MINUS' then
         GOTO REFORM_MINUS;
      elsif triggerin = 'CANCEL' then
         GOTO CANCEL;
      elsif triggerin = 'RESTORE' then
         GOTO RESTORE;
      else
        WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Пропущены все условия!');
				return;
      end if;
      
      --вставка заказа или бронирования в пос (резудьтат матрицы уже должен учтен)
      <<BOOK_INSERT>>
          
        for cur_row in (select 
                        case 
                          when a.id_shop = 'KI1000' then 'BWH_'||s.landid||'_SGP'
                          else 'BWH_'||s.landid||'_'||id_shop
                        end id_shop,
                        b.matnr,a.kol,a.bit_move,a.bit_delete
                        from d_inet_book2 a
                        inner join s_all_mat b on a.art = b.art and a.asize = nvl(b.asize,0) and b.trademark_sat != '003'
                        inner join hybris.st_server_for_servlet s on s.server = a.serv_ip
                        where a.posnr = posnrin and a.serv_ip = serv_ipin and a.rnum = rnumin
                        and a.id_shop not in ('N/A')
                        and not (p_bit_book = 'F' and a.bit_move = 'F' and a.id_shop != nvl(a.id_shop_from,'null'))) loop
            begin
              
                        p_result := KAA_JAVA_HYBRIS_STOCK_FUN(serv_ipin, cur_row.id_shop, cur_row.matnr, to_char(cur_row.kol), '-1');
                          
                        if p_result like '%OK%' then
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
                            select posnrin,sysdate,'ИЗМЕНЕНЫ ОСТАТКИ В HYBRIS ПРИ ВСТАВКЕ ЗАКАЗА В ПОС',serv_ipin,rnumin,a.idm+1,'SERVER'
                            from (select nvl(max(id),0) idm
                                        from d_inet_book3
                                        where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                        else
                          WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Не удалось изменить остатки в hybris при вставке заказа в пос!');
                        end if;
            end;
        end loop;
      
        GOTO QUIT;
        
        
      --переформировка перемещения - восстановление на остатке
      <<REFORM_PLUS>>
        
        if id_shop_delin is null then
           GOTO QUIT;
        end if;
      
        for cur_row in (select 
                        case 
                          when a.id_shop = 'KI1000' then 'BWH_'||s.landid||'_SGP'
                          else 'BWH_'||s.landid||'_'||id_shop
                        end id_shop,
                        b.matnr,a.kol,a.bit_move,a.bit_delete
                        from d_inet_book2 a
                        inner join s_all_mat b on a.art = b.art and a.asize = nvl(b.asize,0) and b.trademark_sat != '003'
                        inner join hybris.st_server_for_servlet s on s.server = a.serv_ip
                        where a.posnr = posnrin and a.serv_ip = serv_ipin and a.rnum = rnumin and a.id_shop = id_shop_delin
                        and a.id_shop not in ('N/A') and bit_move = 'T' and bit_delete = 'T') loop
            begin
              
                        p_result := KAA_JAVA_HYBRIS_STOCK_FUN(serv_ipin, cur_row.id_shop, cur_row.matnr, to_char(cur_row.kol), '1');
                          
                        if p_result like '%OK%' then
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
                            select posnrin,sysdate,'ИЗМЕНЕНЫ ОСТАТКИ В HYBRIS ПРИ ПЕРЕФОРМИРОВКЕ ПЕРЕМЕЩЕНИЯ+',serv_ipin,rnumin,a.idm+1,'SERVER'
                            from (select nvl(max(id),0) idm
                                        from d_inet_book3
                                        where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                        else
                          WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Не удалось изменить остатки в hybris при переформировке перемещения+!');
                        end if;
            end;
        end loop;
        
        GOTO QUIT;
        
      --переформировка перемещения - списание с остатка
      <<REFORM_MINUS>>
        
        for cur_row in (select 
                        case 
                          when a.id_shop = 'KI1000' then 'BWH_'||s.landid||'_SGP'
                          else 'BWH_'||s.landid||'_'||id_shop
                        end id_shop,
                        b.matnr,a.kol,a.bit_move,a.bit_delete
                        from d_inet_book2 a
                        inner join s_all_mat b on a.art = b.art and a.asize = nvl(b.asize,0) and b.trademark_sat != '003'
                        inner join hybris.st_server_for_servlet s on s.server = a.serv_ip
                        where a.posnr = posnrin and a.serv_ip = serv_ipin and a.rnum = rnumin
                        and a.id_shop not in ('N/A') and bit_move = 'T' and bit_delete = 'F') loop
            begin
              
                        p_result := KAA_JAVA_HYBRIS_STOCK_FUN(serv_ipin, cur_row.id_shop, cur_row.matnr, to_char(cur_row.kol), '-1');
                          
                        if p_result like '%OK%' then
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
                            select posnrin,sysdate,'ИЗМЕНЕНЫ ОСТАТКИ В HYBRIS ПРИ ПЕРЕФОРМИРОВКЕ ПЕРЕМЕЩЕНИЯ-',serv_ipin,rnumin,a.idm+1,'SERVER'
                            from (select nvl(max(id),0) idm
                                        from d_inet_book3
                                        where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;

                        else
                          WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Не удалось изменить остатки в hybris при переформировке перемещения-!');

                        end if;

            end;

        end loop;  
            
        GOTO QUIT;
        
      --отмена позиции - восстановление на остатке
      <<CANCEL>>
        
        for cur_row in (select 
                        case 
                          when a.id_shop = 'KI1000' then 'BWH_'||s.landid||'_SGP'
                          else 'BWH_'||s.landid||'_'||id_shop
                        end id_shop,
                        b.matnr,a.kol,a.bit_move,a.bit_delete
                        from d_inet_book2 a
                        inner join s_all_mat b on a.art = b.art and a.asize = nvl(b.asize,0) and b.trademark_sat != '003'
                        inner join hybris.st_server_for_servlet s on s.server = a.serv_ip
                        where a.posnr = posnrin and a.serv_ip = serv_ipin and a.rnum = rnumin
                        and a.id_shop not in ('N/A') and bit_sale = 'F'
                        and not (p_bit_book = 'F' and a.bit_move = 'F' and a.id_shop != nvl(a.id_shop_from,'null'))) loop
            begin
              
                        p_result := KAA_JAVA_HYBRIS_STOCK_FUN(serv_ipin, cur_row.id_shop, cur_row.matnr, to_char(cur_row.kol), '1');
                          
                        if p_result like '%OK%' then
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
                            select posnrin,sysdate,'ИЗМЕНЕНЫ ОСТАТКИ В HYBRIS ПРИ ОТМЕНЕ ПОЗИЦИИ',serv_ipin,rnumin,a.idm+1,'SERVER'
                            from (select nvl(max(id),0) idm
                                        from d_inet_book3
                                        where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                        else
                          WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Не удалось изменить остатки в hybris при отмене позиции!');
                        end if;
            end;
        end loop;
      
        GOTO QUIT;
        
      --восстановление позиции - списание с остатка
      <<RESTORE>>
      
        for cur_row in (select 
                        case 
                          when a.id_shop = 'KI1000' then 'BWH_'||s.landid||'_SGP'
                          else 'BWH_'||s.landid||'_'||id_shop
                        end id_shop,
                        b.matnr,a.kol,a.bit_move,a.bit_delete
                        from d_inet_book2 a
                        inner join s_all_mat b on a.art = b.art and a.asize = nvl(b.asize,0) and b.trademark_sat != '003'
                        inner join hybris.st_server_for_servlet s on s.server = a.serv_ip
                        where a.posnr = posnrin and a.serv_ip = serv_ipin and a.rnum = rnumin
                        and a.id_shop not in ('N/A') and bit_sale = 'F'
                        and not (p_bit_book = 'F' and a.bit_move = 'F' and a.id_shop != nvl(a.id_shop_from,'null'))) loop
            begin
              
                        p_result := KAA_JAVA_HYBRIS_STOCK_FUN(serv_ipin, cur_row.id_shop, cur_row.matnr, to_char(cur_row.kol), '-1');
                          
                        if p_result like '%OK%' then
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,RNUM,ID,INIT)
                            select posnrin,sysdate,'ИЗМЕНЕНЫ ОСТАТКИ В HYBRIS ПРИ ВОССТАНОВЛЕНИИ ПОЗИЦИИ',serv_ipin,rnumin,a.idm+1,'SERVER'
                            from (select nvl(max(id),0) idm
                                        from d_inet_book3
                                        where posnr = posnrin and serv_ip = serv_ipin and init = 'SERVER') a;
                        else
                          WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', posnrin||', '||serv_ipin||', '||rnumin, 'Не удалось изменить остатки в hybris при восстановлении позиции!');
                        end if;
            end;
        end loop;
      
        GOTO QUIT;
			
      <<QUIT>>
      
			commit;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END UPDATE_HYBRIS_STOCK;
    
    --обновление остатков в hybris по всему заказу
    PROCEDURE UPDATE_HYBRIS_STOCK_FULL(posnrin in varchar2, serv_ipin in varchar2, triggerin in varchar2)
		 AS 
		
		p_result varchar2(500);
		BEGIN
		
      for cur_row in (select distinct rnum
                             from d_inet_book2
                             where posnr = posnrin and serv_ip = serv_ipin) loop
          begin
                 UPDATE_HYBRIS_STOCK(posnrin, serv_ipin, cur_row.rnum, triggerin); 
          end;
      end loop;
      
			commit;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_UPDATE_HYBRIS_STOCK_FULL', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END UPDATE_HYBRIS_STOCK_FULL;
    
    --обновление статусов dpd по заказу по активным заказам
		PROCEDURE ORDER_DPDSTATE_UPDATE
		 AS 
		cursor orders is 
		select distinct case when s.landid = 'BY' then '2'||a.posnr else '1'||a.posnr end posnr,a.serv_ip,case when a.bit_delivery_addr = 'F' then 'true' else 'false' end pos,c.dpd_id
			from d_inet_book1 a
			inner join d_inet_book_active b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
      inner join d_inet_book4 c on a.posnr = c.posnr and a.serv_ip = c.serv_ip and c.bit_move = 'F'
      inner join hybris.st_server_for_servlet s on s.server = a.serv_ip
			where a.bit_book = 'F' and is_hyb_serv_prod(a.serv_ip) = 'T' and is_hyb_order_prod(a.posnr,a.serv_ip) = 'T'
      and s.landid = 'BY'
			order by posnr;
				
		ordersRow orders%rowtype;
		p_posnr varchar2(20);
		p_serv_ip varchar2(50);
    p_pos varchar2(20);
    p_dpd_id varchar2(50);
		result_p varchar2(5000 CHAR);
		BEGIN
		 
		open orders;
			loop  
					 FETCH orders INTO p_posnr,p_serv_ip,p_pos,p_dpd_id;
					 EXIT WHEN orders%NOTFOUND;  
							begin
							result_p := KAA_JAVA_HYBRIS_DPDSTATE_FUN(p_posnr, p_pos, p_dpd_id, p_serv_ip);
							commit;
              /*if result_p not like '%state%' then
                 WRITE_ERROR_PROC('KAA_ORDER_DPDSTATE_UPDATE', sqlcode, sqlerrm, 'POSNR = '||p_posnr, result_p, ' ');
              end if;*/
								EXCEPTION
								WHEN OTHERS THEN
								dbms_output.put_line(sqlcode||sqlerrm);
								WRITE_ERROR_PROC('KAA_ORDER_DPDSTATE_UPDATE', sqlcode, sqlerrm, 'POSNR = '||p_posnr, ' ', ' ');
								rollback;
							end;
			end loop;
			
		END ORDER_DPDSTATE_UPDATE;
    
    --отправка изменений в сап по заказам, по которым не удалось отправить при изменении
		PROCEDURE ORDER_SAP_EDIT_EXCHANGE
		 AS 
		cursor orders is 
		select distinct a.posnr,a.serv_ip
			from rfc_inet_order_edit a
			order by a.posnr;
				
		ordersRow orders%rowtype;
		p_posnr varchar2(20);
		p_serv_ip varchar2(50);
		result_p varchar2(5000 CHAR);
		BEGIN
		 
		open orders;
			loop  
					 FETCH orders INTO p_posnr,p_serv_ip;
					 EXIT WHEN orders%NOTFOUND;  
							begin
							RFC_ORDER_CH_UPD(p_posnr, p_serv_ip, 'X', 'X', 'BWP599');
							commit;
								EXCEPTION
								WHEN OTHERS THEN
								dbms_output.put_line(sqlcode||sqlerrm);
								WRITE_ERROR_PROC('KAA_ORDER_SAP_EDIT_EXCHANGE', sqlcode, sqlerrm, 'POSNR = '||p_posnr, ' ', ' ');
								rollback;
							end;
			end loop;
			
		END ORDER_SAP_EDIT_EXCHANGE;
    
    --внесение продаж и отмена по заказам с доставкой на адрес
    PROCEDURE ORDER_DPD_DELIVERED(i_country in varchar2)
     AS 
  --  p_command varchar2(20);
  --  p_shopto varchar2(20);
  --  p_result varchar2(500);
  --	p_hyb_status varchar2(50);
  --	p_history_text varchar2(500);
  --	p_last_id number(20);
  --	p_last_id_sale number(20);
  --	p_sale_date date;
      p_shopid varchar2(20 CHAR);
      p_shopbase varchar2(20 CHAR);
    BEGIN
        if i_country = 'BY' then
          p_shopid := 'I002';
          p_shopbase := 'SHOP_0I002';
        elsif i_country = 'RU' then
          p_shopid := '4699';
          p_shopbase := 'SHOP_26099';
        else
          WRITE_ERROR_PROC('KAA_ORDER_DPD_DELIVERED', sqlcode, sqlerrm, 'Пройдены все условия!', ' ', ' ');
          return;
        end if;
  	
        --проставляю в транзитах на виртуальную базу по интернет-заказам номер этой базы (по РБ)
        update d_sap_odgruz1
          set shopid = 'I002'
          where partnerid = '0090000359' and shopid is null;
        commit;
        
        execute immediate q'[
          declare
            p_command varchar2(20);
            p_shopto varchar2(20);
            p_result varchar2(500);
            p_hyb_status varchar2(50);
            p_history_text varchar2(500);
            p_last_id number(20);
            p_last_id_sale number(20);
            p_sale_date date;
            p_shopid_s varchar2(20 CHAR);
					  p_rx_code varchar2(10 CHAR);
						p_rx_name varchar2(300 CHAR);
          begin 
            p_shopid_s := ']'||p_shopid||q'[';
  				
            --приходую в виртуальной базе непринятые поставки
            TDV_EX_FIRM_TO_SHOP.copy_table_to_shop(p_shopid_s); 
  					
            delete from ]'||p_shopbase||q'[.d_prixod2
              where id in (select id
                            from ]'||p_shopbase||q'[.d_prixod1
                            where bit_close = 'F');
            delete from ]'||p_shopbase||q'[.d_prixod1
              where bit_close = 'F';
            commit;
  					
            for curOdgr in (select distinct a.postno,d.posnr,to_date(a.prdate,'yyyymmdd') dated
                              from ]'||p_shopbase||q'[.d_sap_odgruz1 a
                              inner join ]'||p_shopbase||q'[.d_sap_odgruz2 b on a.postno = b.postno
                              left join ]'||p_shopbase||q'[.d_prixod2 c on c.postno = a.postno and c.art = b.art and c.asize = b.asize and c.scan = b.scan
                              left join d_inet_book_sap_status d on d.vbeln_vl = a.postno
                              where c.id is null) loop
              begin
                select nvl(max(id)+1,1) into p_last_id
                  from ]'||p_shopbase||q'[.d_prixod1;
                insert into ]'||p_shopbase||q'[.d_prixod1 (ID,DATED,NDOC,KKL,NKL,TEXT,DATE_S,BIT_CLOSE,IDOP,NAMEOP,POSTNO,TIMED,REL,SHOPID,ENET_POSNR)
                  select p_last_id ID,curOdgr.dated DATED,' ' NDOC,'KI1000' KKL,'СООО "Белвест"' NKL,' ' TEXT,sysdate DATE_S,'F' BIT_CLOSE,
                    '01' IDOP,'Приход от Белвест' NAMEOP,curOdgr.POSTNO,curOdgr.dated TIMED,p_last_id REL,p_shopid_s SHOPID,curOdgr.posnr ENET_POSNR
                    from dual;
  									
                commit;
  									
                ]'||p_shopbase||q'[.IMPORT_PRIXOD(curOdgr.postno,p_last_id);
  							
                update ]'||p_shopbase||q'[.d_prixod2
                  set kol = koli
                  where id = p_last_id;
  								
                commit;
  								
                update ]'||p_shopbase||q'[.d_prixod1
                  set bit_close = 'T'
                  where id = p_last_id and id not in (select id
                                                  from ]'||p_shopbase||q'[.d_prixod2
                                                  where asize != 0 and scan = ' ');
  								
                commit;
                dbms_output.put_line('Создан приход по заказу - '||curOdgr.posnr);
              end;
            end loop;
  					
            ]'||p_shopbase||q'[.OSTTEK_EVAL('KAA');
  					
            --ставлю статус продажи/отмены позиции заказа по статусам dpd
            for curUpd in (select distinct a.posnr,a.serv_ip,b.dpd_id,b.state_en state,b.return,d.rnum
                            from d_inet_book_active a
                            inner join d_inet_book_dpd_status b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                            inner join d_inet_book1 c on a.posnr = c.posnr and a.serv_ip = c.serv_ip
                            inner join d_inet_book4 d on a.posnr = d.posnr and a.serv_ip = d.serv_ip and b.dpd_id = d.dpd_id and d.bit_move = 'F'
                            where b.dpd_id is not null and c.bit_delivery_addr = 'T' and d.bit_hyb_cancel = 'F' and d.bit_hyb_receive = 'F' 
                              and a.serv_ip in (select server from HYBRIS.ST_SERVER_FOR_SERVLET where type = 'P' and landid = ']'||i_country||q'[') 
                            --and a.posnr = '00038869'
                            order by d.rnum) loop
              begin
                dbms_output.put_line('Найден заказ,позиция - '||curUpd.posnr||' - '||curUpd.rnum);
                --доставлено обратно с возвратом. ставлю отмену и проставляю штрихкод из перемещения
                if curUpd.state in ('Delivered','NotDone') and curUpd.return = 'true' then
                  update d_inet_book2 a
                    set id_cancel = 16,date_cancel = sysdate,bit_block = 'F',
                      scan = nvl((select max(b.scan)
                                    from d_inet_book2 b
                                    where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum and b.bit_move = 'T' and b.bit_delete = 'F'),a.scan)
                    where posnr = curUpd.posnr and serv_ip = curUpd.serv_ip and rnum = curUpd.rnum and bit_move = 'F';
                  update d_inet_book2 a
                    set scan = nvl((select max(b.scan)
                                    from d_inet_book_sap_status b
                                    where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum and b.scan != ' '),a.scan)
                    where posnr = curUpd.posnr and serv_ip = curUpd.serv_ip and rnum = curUpd.rnum and bit_move = 'F' and nvl(id_shop_from,'null') = 'KI1000' and a.scan = ' ';
                  update d_inet_book2
                    set bit_accept = case when scan is not null and scan != ' ' then 'T' else bit_accept end
                    where posnr = curUpd.posnr and serv_ip = curUpd.serv_ip and rnum = curUpd.rnum and bit_move = 'F';
                  dbms_output.put_line('Отмена заказа,позиции - '||curUpd.posnr||' - '||curUpd.rnum);
                end if;
  							
                --доставлено клиенту. ставлю продажу и проставляю штрихкод из перемещения
                if curUpd.state = 'Delivered' and curUpd.return = 'false' then
                  update d_inet_book2 a
                    set bit_sale = 'T',
                      scan = nvl((select max(b.scan)
                                    from d_inet_book2 b
                                    where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum and b.bit_move = 'T' and b.bit_delete = 'F'),a.scan)
                    where posnr = curUpd.posnr and serv_ip = curUpd.serv_ip and rnum = curUpd.rnum and bit_move = 'F';
                  update d_inet_book2 a
                    set scan = nvl((select max(b.scan)
                                    from d_inet_book_sap_status b
                                    where a.posnr = b.posnr and a.serv_ip = b.serv_ip and a.rnum = b.rnum and b.scan != ' '),a.scan)
                    where posnr = curUpd.posnr and serv_ip = curUpd.serv_ip and rnum = curUpd.rnum and bit_move = 'F' and nvl(id_shop_from,'null') = 'KI1000' and a.scan = ' ';
                  update d_inet_book2
                    set bit_accept = case when scan is not null and scan != ' ' then 'T' else bit_accept end
                    where posnr = curUpd.posnr and serv_ip = curUpd.serv_ip and rnum = curUpd.rnum and bit_move = 'F';
                  dbms_output.put_line('Подтверждение продажи заказа,позиции - '||curUpd.posnr||' - '||curUpd.rnum);
                end if;
              end;
            end loop;
            commit;
  			
            --прохожу по каждому заказу
            for cur in (select distinct a.posnr,a.serv_ip,c.bit_sale_ch,c.id_cancel_ch,a.bit_online_pay
                          from d_inet_book1 a
                          inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                          inner join (select posnr,serv_ip,max(bit_sale) bit_sale_ch,min(nvl(id_cancel,0)) id_cancel_ch
                                      from d_inet_book2_v
                                      where bit_move = 'F'
                                      group by posnr,serv_ip) c on a.posnr = c.posnr and a.serv_ip = c.serv_ip
                          inner join d_inet_book4 d on b.posnr = d.posnr and b.serv_ip = d.serv_ip and b.rnum = d.rnum and d.bit_move = 'F'
                          inner join d_inet_book_sap_status e on e.posnr = d.posnr and e.serv_ip = d.serv_ip and e.rnum = d.rnum
                          inner join d_sap_odgruz2 f on f.postno = e.vbeln_vl and f.art = b.art and f.asize = b.asize and f.scan = b.scan and not (f.scan = ' ' and f.asize > 0)
                          where is_hyb_serv_prod(a.serv_ip) = 'T' and a.serv_ip != ' ' and (a.bit_status = 'T' or (b.id_cancel = 17 and a.bit_hyb_cancel = 'F')) and b.bit_move = 'F'
                          and a.bit_book = 'F' and a.bit_delivery_addr = 'T' and (nvl(b.id_cancel,0) > 0 or b.bit_sale = 'T') 
                            and a.serv_ip in (select server from HYBRIS.ST_SERVER_FOR_SERVLET where type = 'P' and landid = ']'||i_country||q'[')
                          and d.bit_hyb_cancel = 'F' and d.bit_hyb_receive = 'F'
                          --and a.posnr = '00038869'
                          union
                          --тестовые заказы
                          select distinct a.posnr,a.serv_ip,c.bit_sale_ch,c.id_cancel_ch,a.bit_online_pay
                            from d_inet_book1 a
                            inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                            inner join (select posnr,serv_ip,max(bit_sale) bit_sale_ch,min(nvl(id_cancel,0)) id_cancel_ch
                                        from d_inet_book2_v
                                        where bit_move = 'F'
                                        group by posnr,serv_ip) c on a.posnr = c.posnr and a.serv_ip = c.serv_ip
                            inner join d_inet_book4 d on b.posnr = d.posnr and b.serv_ip = d.serv_ip and b.rnum = d.rnum and d.bit_move = 'F'
                            where is_hyb_serv_prod(a.serv_ip) = 'T' and is_hyb_order_prod(a.posnr,a.serv_ip) = 'F' and a.serv_ip != ' ' and (a.bit_status = 'T' or (b.id_cancel = 17 and a.bit_hyb_cancel = 'F')) and b.bit_move = 'F'
                            and a.bit_book = 'F' and a.bit_delivery_addr = 'T' and (nvl(b.id_cancel,0) > 0 or b.bit_sale = 'T') 
                              and a.serv_ip in (select server from HYBRIS.ST_SERVER_FOR_SERVLET where type = 'P' and landid = ']'||i_country||q'[')
                            and d.bit_hyb_cancel = 'F' and d.bit_hyb_receive = 'F'
                            --and a.posnr = '00038869'
                          order by posnr) loop  
                    begin
                      dbms_output.put_line('Поехали - '||cur.posnr);
                      p_command := 'order';
                      p_shopto := 'ST_]'||i_country||q'[_0001';
                      p_hyb_status := case when cur.bit_sale_ch = 'T' then 'COMPLETED' else 'CANCELLED' end;
                      if cur.bit_sale_ch = 'T' or cur.id_cancel_ch > 0 then
                        --отправляю в hybris
                        p_result := KAA_JAVA_HYBRIS_EXCH_FUN(cur.serv_ip,p_command,cur.posnr, p_shopto, p_hyb_status);
                        commit;
  											
                        if p_result != 'OK' then
                          p_history_text := case when cur.bit_sale_ch = 'T' then 'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS' else 'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS' end;
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                            select cur.posnr,sysdate,p_history_text,cur.serv_ip,a.idm+1,'SERVER'
                            from (select nvl(max(id),0) idm
                                        from d_inet_book3
                                        where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
                          commit;
                          continue;
                        else
                          p_history_text := case when cur.bit_sale_ch = 'T' then 'ПРОДАЖА ПОДТВЕРЖДЕНА В HYBRIS' else 'ОТМЕНЕН ЗАКАЗ В HYBRIS' end;
                          insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                                select cur.posnr,sysdate,p_history_text,cur.serv_ip,a.idm+1,'SERVER'
                                from (select nvl(max(id),0) idm
                                            from d_inet_book3
                                            where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
                        end if;		
  											
                      end if;
  										
                      --прохожу по позиции заказа и проставляю статусы
                      for curPos in (select distinct a.posnr,a.serv_ip,b.rnum,
                          b.id_cancel,b.bit_sale,d.dpd_id
                          from d_inet_book1 a
                          inner join d_inet_book2_v b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                          inner join d_inet_book4 d on a.posnr = d.posnr and a.serv_ip = d.serv_ip and b.rnum = d.rnum and d.bit_move = 'F'
                          where is_hyb_serv_prod(a.serv_ip) = 'T' and a.serv_ip != ' ' and (a.bit_status = 'T' or (b.id_cancel = 17 and a.bit_hyb_cancel = 'F')) and b.bit_move = 'F'
                          and a.bit_book = 'F' and a.bit_delivery_addr = 'T' and (nvl(b.id_cancel,0) > 0 or b.bit_sale = 'T') and a.serv_ip = cur.serv_ip and a.posnr = cur.posnr
                          and d.bit_hyb_cancel = 'F' and d.bit_hyb_receive = 'F'
                          order by a.posnr,b.rnum) loop  
                        begin
                          dbms_output.put_line('Поехали - '||cur.posnr||' - '||curPos.rnum);
                          if curPos.bit_sale = 'T' or curPos.id_cancel > 0 then
                            update d_inet_book4 a
                              set (a.bit_hyb_cancel,a.bit_hyb_ready,a.bit_hyb_receive,a.op_id_cancel) = 
                                  (select case when curPos.bit_sale = 'T' then 'F' else 'T' end,
                                    'F',
                                    case when curPos.bit_sale = 'T' then 'T' else 'F' end,
                                    case when curPos.id_cancel = 0 then null else curPos.id_cancel end
                                    from dual)
                              where a.posnr = cur.posnr and a.serv_ip = cur.serv_ip and a.rnum = curPos.rnum and a.bit_move = 'F';
  																		
                            if cur.bit_sale_ch = 'T' then
                              update d_inet_book1
                                              set bit_hyb_receive = 'T',bit_hyb_ready = 'F',bit_hyb_cancel = 'F',bit_phone = 'T',op_id_cancel = cur.id_cancel_ch
                                              where posnr = cur.posnr and serv_ip = cur.serv_ip;
                            elsif cur.id_cancel_ch > 0 then
                              update d_inet_book1
                                              set bit_hyb_receive = 'F',bit_hyb_cancel = 'T',bit_hyb_ready = 'F',bit_status = 'F',bit_phone = 'T',op_id_cancel = cur.id_cancel_ch
                                              where posnr = cur.posnr and serv_ip = cur.serv_ip;
                            end if;
  													
                            commit;
  													
                            select nvl(max(dated),sysdate) into p_sale_date
                              from d_inet_book_dpd_status
                              where posnr = curPos.posnr and serv_ip = curPos.serv_ip and dpd_id = curPos.dpd_id;
  													
                            --создаю продажу в виртиуальной базе интернет магазина
                            if curPos.bit_sale = 'T' then
                              if cur.bit_online_pay = 'F' then
  															
                                insert into ]'||p_shopbase||q'[.pos_sale1 (SALE_DATE,ID_DK,INF1,SHOP_ID,KASSIR_ID,KASSIR_FIO,SELLER_ID,SELLER_FIO,
                                                        PRICE_SUM,DISCOUNT_SUM,SALE_SUM,INF2,INF3,BIT_CLOSE,BIT_VOZVR,KASSA_ID,FR_ID_CHEK,
                                                        SELLER_TAB,BIT_ANNUL,BIT_RETURN_BLOCK)
                                select p_sale_date SALE_DATE,' ' ID_DK,' ' INF1,p_shopid_s SHOP_ID,0 KASSIR_ID,' ' KASSIR_FIO,0 SELLER_ID,' ' SELLER_FIO,
                                  b.base_price PRICE_SUM,b.discount DISCOUNT_SUM,b.sum SALE_SUM,' ' INF2,' ' INF3,'F' BIT_CLOSE,'F' BIT_VOZVR,1 KASSA_ID,1 FR_ID_CHEK,
                                  ' ' SELLER_TAB,'F' BIT_ANNUL,'F' BIT_RETURN_BLOCK
                                  from d_inet_book1 a
                                    inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                    where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  																	
                                select max(id_chek) into p_last_id
                                  from ]'||p_shopbase||q'[.pos_sale1;
  																
                                insert into ]'||p_shopbase||q'[.pos_sale2 (ID_CHEK,ART,ASIZE,PROCENT,EAN,SCAN,ASSORT,KOL,CENA1,CENA2,CENA3,
                                                            PORTION,NEI,DISCOUNT,DISCOUNT_SUM,DISCOUNT_INF,ID_CHEK_VOZVR,ID_SALE_VOZVR,
                                                            PARTNO,CENA2S,CENA2SW,BIT_ENET,ENET_POSNR)
                                select p_last_id ID_CHEK,b.ART,b.ASIZE,0 PROCENT,d.EAN,b.SCAN,c.ASSORT,b.KOL,g.price CENA1,b.base_price CENA2,b.sum CENA3,
                                  0 PORTION,e.name1 NEI,0 DISCOUNT,b.discount DISCOUNT_SUM,' ' DISCOUNT_INF,0 ID_CHEK_VOZVR,0 ID_SALE_VOZVR,
                                  g.partno PARTNO,b.sum CENA2S,b.sum CENA2SW,'T' BIT_ENET,cur.posnr ENET_POSNR
                                  from d_inet_book1 a
                                    inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                    inner join s_art c on b.art = c.art
                                    left join s_ean d on b.art = d.art and b.asize = d.asize
                                    left join s_edizm e on c.meins = e.meins
                                    left join d_inet_book_sap_status f on f.posnr = b.posnr and f.serv_ip = b.serv_ip and f.rnum = b.rnum
                                    left join d_sap_odgruz2 g on f.vbeln_vl = g.postno and g.art = b.art and g.scan = b.scan and g.asize = b.asize
                                    where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  																	
                                select max(id_sale) into p_last_id_sale
                                  from ]'||p_shopbase||q'[.pos_sale2;
  																
                                insert into ]'||p_shopbase||q'[.pos_sale3 (ID_CHEK,VID_OP_ID,SUM)
                                select p_last_id ID_CHEK,1 VID_OP_ID,b.SUM
                                  from d_inet_book1 a
                                    inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                    where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  																	
                                insert into ]'||p_shopbase||q'[.pos_sale8 (ID_CHEK,VID_OP_ID,PODVID_OP_ID,SUM)
                                select p_last_id ID_CHEK,1 VID_OP_ID,1 PODVID_OP_ID,b.SUM
                                  from d_inet_book1 a
                                    inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                    where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  																	
                                insert into ]'||p_shopbase||q'[.pos_sale4 (ID_SALE,DISC_TYPE_ID,DISCOUNT,DISCOUNT_SUM,DISCOUNT_INF,SHOP_ID)
                                select p_last_id_sale ID_SALE,'ENET000001' DISC_TYPE_ID,0 DISCOUNT,b.discount DISCOUNT_SUM,' ' DISCOUNT_INF,p_shopid_s SHOP_ID
                                  from d_inet_book1 a
                                    inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                    where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  															
                                update ]'||p_shopbase||q'[.pos_sale1
                                  set bit_close = 'T'
                                  where id_chek = p_last_id;
  																	
                                commit;
  															
                                dbms_output.put_line('Внесена продажа - '||cur.posnr||' - '||curPos.rnum);
  															
                                ]'||p_shopbase||q'[.OSTTEK_EVAL('KAA');
                              else
                                --если оплата онлайн - создаю расход
                                select nvl(max(id)+1,1) into p_last_id
                                  from ]'||p_shopbase||q'[.d_rasxod1;
                                insert into ]'||p_shopbase||q'[.d_rasxod1 (ID,DATED,NDOC,KKL,NKL,TEXT,DATE_S,BIT_CLOSE,IDOP,NAMEOP,POSTNO,KKL_VIRTUAL,TIMED,REL,SHOPID)
                                  select p_last_id ID, p_sale_date DATED,' ' NDOC,' ' KKL,' ' NKL,' ' TEXT,sysdate DATE_S,'F' BIT_CLOSE,
                                    '44' IDOP,'Реализация по безналичному расчету (интернет заказ)' NAMEOP,' ' POSTNO,' ' KKL_VIRTUAL,
                                    p_sale_date TIMED,p_last_id REL,p_shopid_s SHOPID
                                    from dual;
  																	
                                commit;
  															
                                insert into ]'||p_shopbase||q'[.d_rasxod2 (ID,ART,ASIZE,PROCENT,EAN,SCAN,ASSORT,KOL,CENA1,CENA2,CENA3,
                                                                    PORTION,NEI,DATE_S,VID_INS,PARTNO,KOROB,REL,NDS,TEXT,TABNO,SKIDKA,SELLER_TAB,
                                                                    SELLER_FIO,ID_DK,BIT_ENET,ENET_POSNR)
                                select p_last_id ID,b.ART,b.ASIZE,0 PROCENT,d.EAN,b.SCAN,c.ASSORT,b.KOL,g.price CENA1,b.base_price CENA2,b.sum CENA3,
                                  0 PORTION,e.name1 NEI,sysdate DATE_S,0 VID_INS,g.partno PARTNO,' ' KOROB,p_last_id REL,0 NDS,' ' TEXT,' ' TABNO,0 SKIDKA,' ' SELLER_TAB,
                                  ' ' SELLER_FIO,' ' ID_DK,'T' BIT_ENET,a.posnr ENET_POSNR
                                from d_inet_book1 a
                                  inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                  inner join s_art c on b.art = c.art
                                  left join s_ean d on b.art = d.art and b.asize = d.asize
                                  left join s_edizm e on c.meins = e.meins
                                  left join d_inet_book_sap_status f on f.posnr = b.posnr and f.serv_ip = b.serv_ip and f.rnum = b.rnum
                                  left join d_sap_odgruz2 g on f.vbeln_vl = g.postno and g.art = b.art and g.scan = b.scan and g.asize = b.asize
                                  where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  															
                                update ]'||p_shopbase||q'[.d_rasxod1
                                  set bit_close = 'T'
                                  where id = p_last_id;
  															
                                commit;
  															
                                dbms_output.put_line('Внесен расход - '||cur.posnr||' - '||curPos.rnum);
  															
                                ]'||p_shopbase||q'[.OSTTEK_EVAL('KAA');
  															
                              end if;
														else
														  --если возврат, создаем расход на белвест. если было перемещение с магазина, то расход с 59 кодом, иначе - 21
															select case when nvl(max(b.id_shop),'N') in ('KI1000','N') or to_char(a.dated,'yyyy') < '2020' then '21' else '59' end into p_rx_code
															from d_inet_book2 b
                              inner join d_inet_book1 a on a.posnr = b.posnr and a.serv_ip = b.serv_ip
															where b.posnr = curPos.posnr and b.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'T' and b.bit_delete = 'F'
                              group by to_char(a.dated,'yyyy')
															;
															
															select nvl(max(nameop),'Не определено') into p_rx_name
															from st_op
															where idop = p_rx_code
															;
															
															select nvl(max(id)+1,1) into p_last_id
                                  from ]'||p_shopbase||q'[.d_rasxod1;
                                insert into ]'||p_shopbase||q'[.d_rasxod1 (ID,DATED,NDOC,KKL,NKL,TEXT,DATE_S,BIT_CLOSE,IDOP,NAMEOP,POSTNO,KKL_VIRTUAL,TIMED,REL,SHOPID)
                                  select p_last_id ID, p_sale_date DATED,' ' NDOC,' ' KKL,' ' NKL,' ' TEXT,sysdate DATE_S,'F' BIT_CLOSE,
                                    p_rx_code IDOP,p_rx_name NAMEOP,' ' POSTNO,' ' KKL_VIRTUAL,
                                    p_sale_date TIMED,p_last_id REL,p_shopid_s SHOPID
                                    from dual;
  																	
                                commit;
  															
                                insert into ]'||p_shopbase||q'[.d_rasxod2 (ID,ART,ASIZE,PROCENT,EAN,SCAN,ASSORT,KOL,CENA1,CENA2,CENA3,
                                                                    PORTION,NEI,DATE_S,VID_INS,PARTNO,KOROB,REL,NDS,TEXT,TABNO,SKIDKA,SELLER_TAB,
                                                                    SELLER_FIO,ID_DK,BIT_ENET,ENET_POSNR)
                                select p_last_id ID,b.ART,b.ASIZE,0 PROCENT,d.EAN,b.SCAN,c.ASSORT,b.KOL,g.price CENA1,b.base_price CENA2,b.sum CENA3,
                                  0 PORTION,e.name1 NEI,sysdate DATE_S,0 VID_INS,g.partno PARTNO,' ' KOROB,p_last_id REL,0 NDS,' ' TEXT,' ' TABNO,0 SKIDKA,' ' SELLER_TAB,
                                  ' ' SELLER_FIO,' ' ID_DK,'T' BIT_ENET,a.posnr ENET_POSNR
                                from d_inet_book1 a
                                  inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                                  inner join s_art c on b.art = c.art
                                  left join s_ean d on b.art = d.art and b.asize = d.asize
                                  left join s_edizm e on c.meins = e.meins
                                  left join d_inet_book_sap_status f on f.posnr = b.posnr and f.serv_ip = b.serv_ip and f.rnum = b.rnum
                                  left join d_sap_odgruz2 g on f.vbeln_vl = g.postno and g.art = b.art and g.scan = b.scan and g.asize = b.asize
                                  where a.posnr = curPos.posnr and a.serv_ip = curPos.serv_ip and b.rnum = curPos.rnum and b.bit_move = 'F';
  															
                                update ]'||p_shopbase||q'[.d_rasxod1
                                  set bit_close = 'T'
                                  where id = p_last_id;
  															
                                commit;
  															
                                dbms_output.put_line('Внесен расход по возврату - '||cur.posnr||' - '||curPos.rnum);
  															
                                ]'||p_shopbase||q'[.OSTTEK_EVAL('KAA');
                            end if;
                          end if;
  												
                          EXCEPTION
                          WHEN OTHERS THEN
                          rollback;
                          WRITE_ERROR_PROC('KAA_ORDER_DPD_DELIVERED', sqlcode, sqlerrm, cur.posnr, curPos.rnum, ' ');
  												
                          begin
                            p_history_text := case when cur.bit_sale_ch = 'T' then 'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS' else 'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS' end;
                            insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT,rnum)
                              select cur.posnr,sysdate,p_history_text,cur.serv_ip,a.idm+1,'SERVER',curPos.rnum
                              from (select nvl(max(id),0) idm
                                          from d_inet_book3
                                          where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
                              commit;
                          end;
                        end;
                      end loop;
  										
                      if cur.bit_sale_ch = 'T' or cur.id_cancel_ch > 0 then
                        kaa_inet_book.RFC_ORDER_CH_UPD(cur.posnr, cur.serv_ip, 'X', 'X');
  											
                        if cur.bit_sale_ch = 'F' and cur.id_cancel_ch > 0 then
                          kaa_inet_book.update_hybris_stock_full(cur.posnr, cur.serv_ip,'CANCEL');
                        end if;
  											
                        p_result := KAA_JAVA_HYBRIS_UDPINFO_FUN(cur.serv_ip, cur.posnr);
                      end if;
  										
                      EXCEPTION
                      WHEN OTHERS THEN
                      rollback;
                      WRITE_ERROR_PROC('KAA_ORDER_DPD_DELIVERED', sqlcode, sqlerrm, cur.posnr, ' ', ' ');
  										
                      begin
                        p_history_text := case when cur.bit_sale_ch = 'T' then 'ОШИБКА ПОДТВЕРЖДЕНИЯ ПРОДАЖИ В HYBRIS' else 'ОШИБКА ОТМЕНЫ ЗАКАЗА В HYBRIS' end;
                        insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                          select cur.posnr,sysdate,p_history_text,cur.serv_ip,a.idm+1,'SERVER'
                          from (select nvl(max(id),0) idm
                                      from d_inet_book3
                                      where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
                          commit;
                      end;
                    end;
            end loop;
					
            commit;
  					
            RKV_ALL(p_shopid_s,'010~KAA~2',p_result);
          end;
        ]';	--]'
  			
        commit;
  			
        EXCEPTION
          WHEN OTHERS THEN
          rollback;
          WRITE_ERROR_PROC('KAA_ORDER_DPD_DELIVERED', sqlcode, sqlerrm, ' ', ' ', ' ');
      
    END ORDER_DPD_DELIVERED;
    
    --обновление статуса по заказу для КЦ
    PROCEDURE UPDATE_COND(posnrin in varchar2 default null, serv_ipin in varchar2 default null)
		 AS 
		
		p_result varchar2(500);
		BEGIN
		
      if posnrin is not null then
        delete from d_inet_book_cond_t
          where posnr = posnrin and serv_ip = serv_ipin;
          
        insert into d_inet_book_cond_t (posnr,serv_ip,shopcond,shopperemcond,callcond)
          select posnr,serv_ip,shopcond,shopperemcond,callcond
          from d_inet_book_cond
          where posnr = posnrin and serv_ip = serv_ipin;
      else
        delete from d_inet_book_cond_tem;
        
        insert into d_inet_book_cond_tem (posnr,serv_ip,shopcond,shopperemcond,callcond)
          select posnr,serv_ip,shopcond,shopperemcond,callcond
          from d_inet_book_cond;        
        
        delete from d_inet_book_cond_t;
        
        insert into d_inet_book_cond_t (posnr,serv_ip,shopcond,shopperemcond,callcond)
          select posnr,serv_ip,shopcond,shopperemcond,callcond
          from d_inet_book_cond_tem;
      end if;
      
			commit;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_UPDATE_COND', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END UPDATE_COND;
    
    --создание перемещений по заказам, где оно не отработало автоматом
    PROCEDURE CHECK_EMPTY_MOVE
		 AS 
		
		p_result varchar2(500);
		BEGIN
		
      for cur in (select a.posnr,a.serv_ip
                  from d_inet_book1 a
                  inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                  where is_hyb_order_prod(a.posnr,a.serv_ip) = 'T'
                  and a.bit_book = 'F' and b.id_shop_from is null) loop
        begin
          BOOK_CREATE_MOVE(cur.posnr, cur.serv_ip);
          --RFC_ORDER_CH_UPD(cur.posnr, cur.serv_ip,'X','X');
        end;
      end loop;
      
			commit;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_CHECK_EMPTY_MOVE', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END CHECK_EMPTY_MOVE;
    
    --изменение статуса SMS по заказу
    PROCEDURE SMS_STATUS_UPDATE(typein in varchar2, idin in varchar2, statusin in number, 
      responsein in varchar2, posnrin in varchar2 default null, serv_ipin in varchar2 default null, 
      phone_numberin in varchar2 default null, txtin in varchar2 default null, filin in varchar2 default null,
      txtviberin in varchar2 default null, typemin in varchar2 default null, channelin in varchar2 default null,
      smskindin in varchar2 default 'ready')
		 AS 
		
		p_result varchar2(2000);
		BEGIN
		
    	if typein = 'stat' then
        update d_inet_book_sms a
          set a.status = STATUSIN,
              a.resp_stat = responsein,
              a.dates = sysdate,
              a.channel = channelin
        where id = idin and MESS_KIND = smskindin;
      else
        merge into d_inet_book_sms a using
                 (select idin as "ID", smskindin as "MESS_KIND" from dual) b
                 on (a.id = b.id and a.MESS_KIND = b.MESS_KIND)
           when not matched then
             insert (POSNR,SERV_IP,ID,STATUS,PHONE_NUMBER,TXT,FIL,RESPONSE,TXTVIBER,TYPEM,MESS_KIND)
             values (POSNRIN,SERV_IPIN,IDIN,STATUSIN,PHONE_NUMBERIN,TXTIN,FILIN,RESPONSEIN,TXTVIBERIN,TYPEMIN,SMSKINDIN)
           when matched then
             update
               set a.status = STATUSIN,
                   a.response = responsein,
                   a.dates = sysdate;
      end if;
      
			commit;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_SMS_STATUS_UPDATE', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END SMS_STATUS_UPDATE;
    
    --проверка статуса по непроверенным sms
    PROCEDURE SMS_STATUS_CHECK
		 AS 
		
		p_result varchar2(2000);
    p_days number;
    p_hours number;    
    v_mess VARCHAR2(1024);
    v_channel VARCHAR2(1024);
    N NUMBER;
    I NUMBER;
    TYPE arr_type IS TABLE OF VARCHAR2(250) INDEX BY BINARY_INTEGER;
    ARR arr_type;
		BEGIN
		
      for cur in (select distinct posnr,serv_ip,id,mess_kind,regexp_replace(response , '[^0-9]', '') num
                  from d_inet_book_sms
                  where status = 1 and (upper(response) like '%NONE%' or upper(response) like '%OK%')) loop
        begin
          select kaa_java_sms_status_fun(cur.num) into p_result
            from dual;
            
          insert into d_inet_book_sms_status_t (posnr, kind, answer) values(cur.posnr, cur.mess_kind, p_result);
          --разбираю ответ и закидываю статус и канал получения в массив
          if nvl(p_result,'N/A') != 'N/A' then
	
            I:=1;
            LOOP --
              if p_result is null then
                ARR(I):='N/A';
                I:=I+1;
                exit;
              end if;
              N:=INSTR(p_result,';');
              IF N=0 THEN
                ARR(I):=p_result;
                I:=I+1;
                EXIT;
              END IF;
              if N != 1 then 
                ARR(I):=SUBSTR(p_result,1,N-1);
                p_result:=SUBSTR(p_result,N+1);
              else
                ARR(I):='N/A';
                p_result:=SUBSTR(p_result,N+1);
              end if;
              I:=I+1;
            END LOOP;
        		
            I:=I-1;
        		
            for ind in 1..ARR.COUNT
            loop
              dbms_output.put_line(ind||' - '||ARR(ind));
            end loop;
        		
            begin
              v_mess := ARR(1);
        		
              EXCEPTION
              WHEN OTHERS THEN
              dbms_output.put_line(sqlcode||sqlerrm);
              v_mess := 'N/A';
            end;
        		
            begin
              v_channel := ARR(2);
        		
              EXCEPTION
              WHEN OTHERS THEN
              dbms_output.put_line(sqlcode||sqlerrm);
              v_channel := 'N/A';
            end;
        		
          else
            v_mess := 'N/A';
            v_channel := 'N/A';
          end if;
          
          p_result := v_mess;
            
          if nvl(p_result,'N/A') in ('Доставлено','Прочитано') then
            SMS_STATUS_UPDATE('stat', cur.id, 2,p_result,null,null,null,null,null,null,null,v_channel,cur.mess_kind);
            if (cur.mess_kind='ready') then
                update d_inet_book1
                set bit_sms_send = 'T'
                where posnr = cur.posnr and serv_ip = cur.serv_ip;
            else 
                update d_inet_book1
                set bit_sms_send_assembly = 'T'
                where posnr = cur.posnr and serv_ip = cur.serv_ip;
                insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                  select cur.posnr,sysdate,'SMS О КОМПЛЕКТОВАНИИ ДОСТАВЛЕНО',cur.serv_ip,a.idm+1,'SERVER'
                  from (select nvl(max(id),0) idm
                         from d_inet_book3
                         where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
            end if;
          else
            
            if (cur.mess_kind='ready') then 
              select floor(sysdate-e.date_s) days,
              case when to_char(date_s,'yyyy') = '9999' then 0 else to_number(to_char(trunc(sysdate) + floor((sysdate-e.date_s)*24*60) / 24/60, 'hh24')) end hours
              into p_days,p_hours
                from (select nvl(max(date_s),to_date('31129999','ddmmyyyy')) date_s
                      from d_inet_book3
                      where cond like '%ПОДТВЕРЖДЕНО В HYBRIS%'
                      and posnr = cur.posnr and serv_ip = cur.serv_ip) e;
              
              if (p_days >= 0 and p_hours >= 3) or nvl(p_result,'N/A') in ('Отклонено','Ошибка при отправке сообщения/получении отчета(API)','Не доставлено (просрочено)','Не доставлено','Ошибка отправки','Отклонено/заблокировано') then
                SMS_STATUS_UPDATE('stat', cur.id, 3,p_result,null,null,null,null,null,null,null,v_channel,cur.mess_kind);
                update d_inet_book1
                  set bit_sms_send = 'F',bit_phone = 'F',bit_hyb_ready = 'F'
                  where posnr = cur.posnr and serv_ip = cur.serv_ip and bit_hyb_ready = 'T' and bit_hyb_receive = 'F';
                insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                  select cur.posnr,sysdate,'ОШИБКА SMS',cur.serv_ip,a.idm+1,'SERVER'
                  from (select nvl(max(id),0) idm
                         from d_inet_book3
                         where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
              else
                SMS_STATUS_UPDATE('stat', cur.id, 1,p_result,null,null,null,null,null,null,null,v_channel,cur.mess_kind);
              end if; 
            else
              select floor(sysdate-e.date_s) days,
              case when to_char(date_s,'yyyy') = '9999' then 0 else to_number(to_char(trunc(sysdate) + floor((sysdate-e.date_s)*24*60) / 24/60, 'hh24')) end hours
              into p_days,p_hours
                from (select nvl(max(date_s),to_date('31129999','ddmmyyyy')) date_s
                      from d_inet_book3
                      where cond like '%ЗВОНОК КЛИЕНТУ СОВЕРШЕН%'
                      and posnr = cur.posnr and serv_ip = cur.serv_ip) e;
              
              if (p_days >= 0 and p_hours >= 3) or nvl(p_result,'N/A') in ('Отклонено','Ошибка при отправке сообщения/получении отчета(API)','Не доставлено (просрочено)','Не доставлено','Ошибка отправки','Отклонено/заблокировано') then
                SMS_STATUS_UPDATE('stat', cur.id, 3,p_result,null,null,null,null,null,null,null,v_channel,cur.mess_kind);
                update d_inet_book1
                  set bit_sms_send_assembly = 'F',bit_phone = 'F'
                  where posnr = cur.posnr and serv_ip = cur.serv_ip ;
                insert into d_inet_book3 (POSNR,DATE_S,COND,SERV_IP,ID,INIT)
                  select cur.posnr,sysdate,'ОШИБКА SMS О КОМПЛЕКТОВАНИИ',cur.serv_ip,a.idm+1,'SERVER'
                  from (select nvl(max(id),0) idm
                         from d_inet_book3
                         where posnr = cur.posnr and serv_ip = cur.serv_ip and init = 'SERVER') a;
              else
                SMS_STATUS_UPDATE('stat', cur.id, 1,p_result,null,null,null,null,null,null,null,v_channel,cur.mess_kind);
              end if;
            end if;
          end if;
          
          commit;
        
          EXCEPTION
          WHEN OTHERS THEN
          rollback;
          WRITE_ERROR_PROC('KAA_SMS_STATUS_CHECK', sqlcode, sqlerrm, p_result, cur.id, ' ');
        end;
      end loop;
      
			commit;
			
			EXCEPTION
			WHEN OTHERS THEN
			rollback;
			WRITE_ERROR_PROC('KAA_SMS_STATUS_CHECK', sqlcode, sqlerrm, ' ', ' ', ' ');
			
		END SMS_STATUS_CHECK;

end KAA_INET_BOOK;
