SELECT  * FROM D_RASXOD_DET WHERE  REL=3162;
SELECT  * FROM D_RASXOD1 WHERE  id=1479;

select * FROM D_RASXOD_DET;

select 
  nvl2(a.DELIVERY_FAM,a.DELIVERY_FAM||nvl2(a.DELIVERY_IMA,' ',null),null)||nvl2(a.DELIVERY_IMA,a.DELIVERY_IMA||nvl2(a.DELIVERY_OTCH,' ',null),null)||nvl2(a.DELIVERY_OTCH,a.DELIVERY_OTCH,null) delivery_fio,
  nvl2(a.DELIVERY_TOWN, a.DELIVERY_TOWN, null) || 
  nvl2(a.DELIVERY_ADDRESS, ', '||a.DELIVERY_ADDRESS, null) || 
  nvl2(a.DELIVERY_BUILDING, ', д.'||a.DELIVERY_BUILDING, null) || 
  nvl2(a.DELIVERY_BLOCK, ', корп.'||a.DELIVERY_BLOCK, null) || 
  nvl2(a.DELIVERY_APPARTMENT, ', кв.'||a.DELIVERY_APPARTMENT, null) delivery_addr2
--  a.DELIVERY_TOWN, a.DELIVERY_ADDRESS, a.DELIVERY_BUILDING, a.DELIVERY_BLOCK, a.DELIVERY_APPARTMENT
from d_inet_book1 a
inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
inner join d_rasxod2 c on b.scan = c.scan
where c.rel = 1479
;


select * from D_RASXOD2_V;

SELECT A."ID",A."ART",A."ASIZE",A."PROCENT",A."EAN",A."SCAN",A."ASSORT",A."KOL",A."CENA1",A."CENA2",A."CENA3",A."PORTION",A."NEI",A."DATE_S",A."VID_INS",A."PARTNO",A."KOROB",A."REL",A."NDS",A."TEXT",A."TABNO",A."SKIDKA",A."SELLER_TAB",A."SELLER_FIO"
,CASE VID_INS   WHEN 0 THEN 'РУЧНОЙ'   WHEN 1 THEN 'СКАНЕР'   WHEN 2 THEN 'ИМПОРТ'   ELSE ' '  END AS NAME_INS
,CAST(CENA1*KOL AS NUMBER(18,2)) AS SUM1,CAST(CENA3*KOL AS NUMBER(18,2)) AS SUM3,B.PRODH,C.VTEXT
,B.VOLUM,B.BRGEW
,A.TABNO||' '||A.TEXT||' СКИДКА:'||TO_CHAR(A.SKIDKA,'99') AS TABNO_TEXT,A.ID_DK,A.BIT_ENET,A.ENET_POSNR, A.COMMENTS,A.KRED_MON
FROM D_RASXOD2 A
LEFT JOIN S_ART B ON A.ART=B.ART
LEFT JOIN S_PRODH C ON SUBSTR(B.PRODH,1,4)=C.PRODH
where rel = 1479
;

SELECT A.*,ROUND(SUM1*(1.00+NDS/100.00),2)-SUM1 AS SUM_NDS,ROUND(SUM1*(1.00+NDS/100.00),2) AS SUM2
,0 AS NAC,0 AS NACP
FROM (
SELECT ART,ASSORT,ASIZE,PROCENT,NEI,SUM(KOL) AS KOL,CENA1,SUM(KOL*CENA1) AS SUM1,CENA2,SUM(TO_NUMBER(BRGEW,'99.999'))/1000  AS BRGEW
,CASE WHEN NDS=0 THEN GET_NDS(ART,ASIZE) ELSE NDS END AS NDS
 FROM D_RASXOD2_V
WHERE REL=1479
GROUP BY ASSORT,ART,ASIZE,PROCENT,CENA1,CENA2,CENA3,NEI,NDS
) A
ORDER BY ART,ASIZE,PROCENT,CENA1,CENA2,NEI                 
;


SELECT A.*,ROUND(SUM1*(1.00+NDS/100.00),2)-SUM1 AS SUM_NDS,ROUND(SUM1*(1.00+NDS/100.00),2) AS SUM2
,0 AS NAC,0 AS NACP
FROM (
  SELECT A.ART,A.ASSORT,A.ASIZE,A.PROCENT,A.NEI,SUM(B.KOL) AS KOL,
  ROUND(SUM(B.SUM-(B.SUM/120)*20),2)/SUM(B.KOL) CENA1,
  ROUND(SUM(B.SUM-(B.SUM/120)*20),2) AS SUM1,A.CENA2,
  
  SUM(TO_NUMBER(A.BRGEW,'99.999'))/1000  AS BRGEW
  ,CASE WHEN A.NDS=0 THEN GET_NDS(A.ART,A.ASIZE) ELSE A.NDS END AS NDS
   FROM D_RASXOD2_V A
   INNER JOIN D_INET_BOOK2 B ON A.SCAN = B.SCAN
  WHERE REL=1479
  GROUP BY A.ASSORT,A.ART,A.ASIZE,A.PROCENT,A.CENA2,A.CENA3,A.NEI,A.NDS, B.SUM, B.KOL

) A
ORDER BY ART,ASIZE,PROCENT,CENA1,CENA2,NEI                 
;

SELECT A.ART,A.ASSORT,A.ASIZE,A.PROCENT,A.NEI,
SUM(B.KOL) AS KOL,
SUM(B.SUM/B.KOL) CENA1,
B.SUM AS SUM1,
A.CENA2,SUM(TO_NUMBER(A.BRGEW,'99.999'))/1000  AS BRGEW
,CASE WHEN A.NDS=0 THEN GET_NDS(A.ART,A.ASIZE) ELSE A.NDS END AS NDS
,round(SUM(B.SUM-(B.SUM/120)*20),2) without_nds
 FROM D_RASXOD2_V A
 INNER JOIN D_INET_BOOK2 B ON A.SCAN = B.SCAN
 INNER JOIN D_INET_BOOK1 C ON B.POSNR = C.POSNR AND B.SERV_IP = C.SERV_IP
WHERE REL=1479 
AND c.BIT_BOOK = 'F' AND B.BIT_MOVE = 'F' AND c.BIT_DELIVERY_ADDR = 'T' AND COALESCE(B.ID_CANCEL, 0) = 0
GROUP BY A.ASSORT,A.ART,A.ASIZE,A.PROCENT,A.CENA2,A.CENA3,A.NEI,A.NDS, B.SUM, B.KOL
;

select * from D_RASXOD2_V a 
inner join d_inet_book2 b on a.scan = b.scan
WHERE  a.REL=1479 
;

select 
b.*
from d_inet_book1 a
inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
inner join d_rasxod2 c on b.scan = c.scan
where c.rel = 1479
;


select a.posnr,to_char(a.dated,'dd.mm.yyyy') dated,a.phone_number phone,
nvl2(a.delivery_fam,a.delivery_fam||nvl2(a.delivery_ima,' ',null),null)||nvl2(a.delivery_ima,a.delivery_ima||nvl2(a.delivery_otch,' ',null),null)||nvl2(a.delivery_otch,a.delivery_otch,null) fio,
  nvl2(a.DELIVERY_TOWN, a.DELIVERY_TOWN, null) || 
  nvl2(a.DELIVERY_ADDRESS, ', '||a.DELIVERY_ADDRESS, null) || 
  nvl2(a.DELIVERY_BUILDING, ', д.'||a.DELIVERY_BUILDING, null) || 
  nvl2(a.DELIVERY_BLOCK, ', корп.'||a.DELIVERY_BLOCK, null) || 
  nvl2(a.DELIVERY_APPARTMENT, ', кв.'||a.DELIVERY_APPARTMENT, null) delivery_addr,
'Адрес магазина: г. Витебск, ул. 3-я Суражская, 6' addr,' ' curdate,' ' seller_fio
from d_inet_book1 a
inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
inner join d_rasxod2 c on b.scan = c.scan
where c.rel = 1479
and a.bit_book = 'F' and b.bit_move = 'F' and a.bit_delivery_addr = 'T' and coalesce(b.id_cancel, 0) = 0
;

select b.art,c.mod,c.assort,b.asize,b.kol,b.base_price,b.discount,b.sum
from d_inet_book1 a
inner join d_inet_book2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
inner join s_art c on b.art = c.art
where  b.scan = '00000000001111182372' 
and a.bit_book = 'F' and b.bit_move = 'F' and a.bit_delivery_addr = 'T' and coalesce(b.id_cancel, 0) = 0
order by b.rnum,b.art,b.asize;

select * from d_inet_book1 where posnr = '00002078';
select * from d_inet_book2 where posnr = '08372403';
select * from d_inet_book2 where scan = '00000000001111182372';

select * from d_inet_book2 where bit_move = 'F';