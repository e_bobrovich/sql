
SELECT DISTINCT POSNR, SERV_IP, sum(kol) kol, sum(kol2) kol2, MAX(BIT_DELIVERY_ADDR) BIT_DELIVERY_ADDR
FROM (
         SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, b2.KOL, b1.BIT_DELIVERY_ADDR, b2.kol kol2 -- позиции из заказа
         FROM D_INET_BOOK1 b1
                  JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
         WHERE b1.BIT_BOOK = 'F'
           AND b2.BIT_MOVE = 'F'
           AND b2.bit_sale = 'F'
           AND b1.bit_status = 'T'
--           AND b2.ID_SHOP_FROM IS NOT NULL
           AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')
           AND b2.ID_SHOP_TO IS NOT NULL
           AND COALESCE(ID_CANCEL, 0) = 0
         UNION ALL
         SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2 -- убирается все что отменили в заказе
         FROM D_INET_BOOK1 b1
                  JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
         WHERE b1.BIT_BOOK = 'F'
           AND b2.BIT_MOVE = 'F'
           AND b1.BIT_STATUS = 'T'
--           AND b2.ID_SHOP_FROM IS NOT NULL
           AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')
           AND b2.ID_SHOP_TO IS NOT NULL
           AND (b2.ID_SHOP_FROM IN (b2.ID_SHOP_TO, 'NOT_OST') OR b2.ID_SHOP_TO = 'KI1000')
           AND COALESCE(ID_CANCEL, 0) = 0
         UNION ALL
         SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2 -- убираем то что распределено
         FROM D_INET_BOOK1 b1
                  JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
         WHERE b1.BIT_BOOK = 'F'
           AND b2.BIT_MOVE = 'T'
           AND b2.BIT_DELETE = 'F'
--           AND b2.ID_SHOP_FROM IS NOT NULL
           AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')
           AND b2.ID_SHOP_TO IS NOT NULL
           AND b1.bit_status = 'T'
           AND COALESCE(ID_CANCEL, 0) = 0
        UNION ALL
        SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, 1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2 -- Добавляем то что отменено по на СГП
         FROM D_INET_BOOK1 b1
                  JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                  JOIN D_INET_BOOK_SAP_STATUS ss ON b2.RNUM = ss.RNUM AND b2.POSNR = ss.POSNR AND b2.SERV_IP = ss.SERV_IP
         WHERE b1.BIT_BOOK = 'F'
           AND b2.BIT_MOVE = 'F'
           AND b2.BIT_DELETE = 'F'
--           AND b2.ID_SHOP_FROM IS NOT NULL
           AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')
           AND b2.ID_SHOP_TO IS NOT NULL
           AND b1.BIT_STATUS = 'T'
           AND COALESCE(b2.ID_CANCEL, 0) = 0
           AND ss.STATUS_RNUM_SAP IN ('CL', 'DS', 'NS')
           AND b2.ID_SHOP_FROM = 'KI1000'
     )
WHERE SERV_IP IN (SELECT SERVER FROM HYBRIS.ST_SERVER_FOR_SERVLET WHERE /*LANDID = 'BY' AND*/ TYPE = 'P')
and BIT_DELIVERY_ADDR = 'T'
GROUP BY POSNR, SERV_IP, ART, ASIZE, RNUM
--HAVING (MAX(BIT_DELIVERY_ADDR) = 'F' AND SUM(KOL) > 0) OR (MAX(BIT_DELIVERY_ADDR) = 'T' AND SUM(KOL) >= SUM(KOL2) AND SUM(KOL) > 0)
order by POSNR desc, SERV_IP desc
;


SELECT b2.*
--b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2 -- убираем то что распределено
FROM D_INET_BOOK1 b1
        JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
WHERE b1.BIT_BOOK = 'F'
 AND b2.BIT_MOVE = 'T'
 AND b2.BIT_DELETE = 'F'
--           AND b2.ID_SHOP_FROM IS NOT NULL
 AND (b2.ID_SHOP_FROM IS NOT NULL OR b1.BIT_DELIVERY_ADDR = 'T')
 AND b2.ID_SHOP_TO IS NOT NULL
 AND b1.bit_status = 'T'
 AND COALESCE(ID_CANCEL, 0) = 0
 order by b2.POSNR desc, b2.SERV_IP desc, b2.RNUM;
 
 select * from d_inet_book1@s3508;