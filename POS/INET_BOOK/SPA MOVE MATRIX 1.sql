SELECT * FROM TEM_INET_MOVE_MATRIX_RESULT; -- итог расчета
SELECT * FROM TEM_INET_MOVE_MATRIX_RESULT2; -- итог расчета
SELECT * FROM TEM_INET_MOVE_MATRIX_OST3; -- текущие оcтатки магазинов и СГП по артикулам из заказа
SELECT * FROM TEM_INET_MOVE_MATRIX_BOOK2; -- суммированный текущий заказ
SELECT * FROM TEM_INET_MOVE_MATRIX_SETTINGS2; -- настройки
select * from SPA_MOVE_MATRIX_TRANSPORT_COST;
select * from SPA_MOVE_MATRIX_DELIVERY_COST;

DELETE FROM TEM_INET_MOVE_MATRIX_RESULT; -- итог расчета
DELETE FROM TEM_INET_MOVE_MATRIX_RESULT2; -- итог расчета
DELETE FROM TEM_INET_MOVE_MATRIX_OST3; -- текущие оcтатки магазинов и СГП по артикулам из заказа
DELETE FROM TEM_INET_MOVE_MATRIX_BOOK2; -- суммированный текущий заказ
DELETE FROM TEM_INET_MOVE_MATRIX_SETTINGS2; -- настройки

select * from d_inet_book1 where bit_delivery_addr = 'T' order by dated desc;
select * from d_inet_book2 where posnr = '00341494' and serv_ip = '192.168.22.252';


call spa_move_matrix.run_move_matrix('00341494', '192.168.22.252');
select * from tem_inet_move_matrix_result2;
select * from tem_inet_move_matrix_result;

-- определяю страну заказа
SELECT lower(LANDID) AS LAND_ID
--INTO v_land_id
FROM HYBRIS.ST_SERVER_FOR_SERVLET
WHERE SERVER = '192.168.22.252';
--by

-- определяю Самовывоз или Доставка на адрес
SELECT BIT_DELIVERY_ADDR, DELIVERY_TOWN
--INTO v_is_delivery
FROM D_INET_BOOK1 a
WHERE a.POSNR = '00341494'
AND a.SERV_IP = '192.168.22.252';
--T

SELECT max(id_shop)
--INTO v_shop_to -- магазин получатель
FROM D_INET_BOOK2 a
WHERE a.POSNR = '00341494'
AND a.SERV_IP = '192.168.22.252'
and a.BIT_MOVE = 'F'; -- позиция перемещения заказа между магазинами и СГП, если едет - "T"
--null N/A
--ELSE v_shop_to := 'KI1000'; KI1000

INSERT INTO TEM_INET_MOVE_MATRIX_SETTINGS2(LAND_ID, IS_DELIVERY, SHOP_TO, POSNR, SERVER_IP, DELIVERY_TOWN)
VALUES ('by', 'T', 'KI1000', '00341494', '192.168.22.252', 'г. Витебск,');

--INSERT INTO TEM_INET_MOVE_MATRIX_BOOK2(ART, ASIZE, KOL, RNUM)
 SELECT ART, ASIZE, SUM(KOL) AS KOL, RNUM--,  SUM(KOL2) AS KOL2
        FROM (
                 SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, b2.KOL, b1.BIT_DELIVERY_ADDR, -1*b2.kol kol2
                 FROM D_INET_BOOK1 b1
                          JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                 WHERE b1.BIT_BOOK = 'F'
                   AND b1.POSNR = '00341494'
                   AND b1.SERV_IP = '192.168.22.252'
                   AND b2.BIT_MOVE = 'F'
                   AND b2.bit_sale = 'F'
                   AND b1.bit_status = 'T'
                   AND COALESCE(ID_CANCEL, 0) = 0
                 UNION ALL
                 SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2
                 FROM D_INET_BOOK1 b1
                          JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                 WHERE b1.BIT_BOOK = 'F'
                   AND b1.POSNR = '00341494'
                   AND b1.SERV_IP = '192.168.22.252'
                   AND b2.BIT_MOVE = 'F'
                   AND b1.BIT_STATUS = 'T'
                   AND b2.ID_SHOP_FROM IS NOT NULL
                   AND b2.ID_SHOP_TO IS NOT NULL
                   AND (b2.ID_SHOP_FROM IN (b2.ID_SHOP_TO, 'NOT_OST') OR (b2.ID_SHOP_TO = 'KI1000'  AND b1.BIT_DELIVERY_ADDR = 'F'))
                   AND COALESCE(ID_CANCEL, 0) = 0
                 UNION ALL
                 SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, -1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2
                 FROM D_INET_BOOK1 b1
                          JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                 WHERE b1.BIT_BOOK = 'F'
                   AND b1.POSNR = '00341494'
                   AND b1.SERV_IP = '192.168.22.252'
                   AND b2.BIT_MOVE = 'T'
                   AND b2.BIT_DELETE = 'F'
                   AND b2.ID_SHOP_FROM IS NOT NULL
                   AND b2.ID_SHOP_TO IS NOT NULL
                   AND b1.bit_status = 'T'
                   AND COALESCE(ID_CANCEL, 0) = 0
                UNION ALL
                SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, 1 * b2.KOL AS KOL, b1.BIT_DELIVERY_ADDR, 0 kol2
                 FROM D_INET_BOOK1 b1
                          JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                          JOIN D_INET_BOOK_SAP_STATUS ss ON b2.RNUM = ss.RNUM AND b2.POSNR = ss.POSNR AND b2.SERV_IP = ss.SERV_IP
                 WHERE b1.BIT_BOOK = 'F'
                   AND b1.POSNR = '00341494'
                   AND b1.SERV_IP = '192.168.22.252'
                   AND b2.BIT_MOVE = 'F'
                   AND b2.BIT_DELETE = 'F'
                   AND b2.ID_SHOP_FROM IS NOT NULL
                   AND b2.ID_SHOP_TO IS NOT NULL
                   AND b1.BIT_STATUS = 'T'
                   AND COALESCE(b2.ID_CANCEL, 0) = 0
                   AND ss.STATUS_RNUM_SAP IN ('CL', 'DS', 'NS')
                   AND b2.ID_SHOP_FROM = 'KI1000'
                ------------------------------
                UNION ALL  
                SELECT b2.POSNR, b2.SERV_IP, b2.ART, b2.ASIZE, b2.RNUM, 0 KOL, b1.BIT_DELIVERY_ADDR, b2.kol kol2
                FROM D_INET_BOOK1 b1
                         JOIN D_INET_BOOK2 b2 ON b1.SERV_IP = b2.SERV_IP AND b1.POSNR = b2.POSNR
                WHERE b1.BIT_BOOK = 'F'
                  AND b1.POSNR = '00341494'
                  AND b1.SERV_IP = '192.168.22.252'
                  AND b2.BIT_MOVE = 'F'
                  AND b2.bit_sale = 'F'
                  AND b1.bit_status = 'T'
                  AND b2.ID_SHOP_FROM IS NOT NULL
                  AND b2.ID_SHOP_TO IS NOT NULL
                  AND COALESCE(ID_CANCEL, 0) = 0
                  AND b1.BIT_DELIVERY_ADDR = 'T'
             )
        GROUP BY ART, ASIZE, RNUM
        HAVING (MAX(BIT_DELIVERY_ADDR) = 'F' AND SUM(KOL) > 0) OR (MAX(BIT_DELIVERY_ADDR) = 'T' AND SUM(KOL) >= 0 AND SUM(KOL2) >= 0)
--        HAVING SUM(KOL) > 0
        ;


call KAA_INET_BOOK.GET_FREE_STOCK('SELECT distinct matnr
              FROM d_inet_book2 a
              inner join s_all_mat b on a.art = b.art and a.asize = b.asize
              WHERE a.posnr = ''' || '00341494' || '''
                    and a.serv_ip = ''' || '192.168.22.252' || '''
                    and b. trademark_sat != ''003''
                    and a.bit_move = ''F'' ');
                    
SELECT LAND_ID, SHOP_TO, IS_DELIVERY
--INTO v_land_id, v_shop_to, v_is_delivery
FROM TEM_INET_MOVE_MATRIX_SETTINGS2;

select * from TEM_INET_MOVE_MATRIX_SETTINGS2;

SELECT x.id_shop, x.art, x.asize, sum(x.kol) kol
FROM e_osttek_online x
      JOIN TEM_INET_MOVE_MATRIX_BOOK2 a on a.art = x.art and a.asize = x.asize
      JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 b ON b.IS_DELIVERY = 'F' AND (('BY' = UPPER(b.LAND_ID) AND INSTR(x.ID_SHOP, '00') = 1) OR ('RU' = UPPER(b.LAND_ID) AND INSTR(x.ID_SHOP, '00') != 1))
      JOIN ST_SHOP s ON x.ID_SHOP = s.SHOPID AND s.ORG_KOD = 'SHP' and s.BIT_OPEN = 'T'
      LEFT JOIN POS_BRAK brak on x.SCAN = brak.SCAN AND x.id_shop = brak.id_shop
WHERE brak.SCAN IS NULL
  AND x.PROCENT = 0
group by x.id_shop, x.art, x.asize
having sum(x.kol) > 0;


delete from TEM_INET_MOVE_MATRIX_OST3 where id_shop not in ('KI1000','NOT_OST');

select * from TEM_INET_MOVE_MATRIX_BOOK2;

select * from TEM_INET_MOVE_MATRIX_OST3;

INSERT INTO TEM_INET_MOVE_MATRIX_OST3 (ID_SHOP, ART, ASIZE, SIZES, KOL, PRIORITY, RNUM, COST)
SELECT a.*, DECODE(a.ID_SHOP, 'KI1000', 0, 'NOT_OST', 0, DECODE(a.PRIORITY, 4, 0, nvl(d.COST, 8.7264))) COST--, d.city_from, d.city_to
--LENGTH(a.SIZES) - LENGTH(REPLACE(a.SIZES, '&', ''))
FROM (
        SELECT a.ID_SHOP,
               a.ART,
               a.ASIZE,
               LISTAGG(a.ASIZE, '&') WITHIN GROUP ( ORDER BY a.ASIZE ) OVER ( partition by  a.ID_SHOP, a.ART, a.PRIORITY ) SIZES,
               SUM(a.KOL) KOL,
               a.PRIORITY,
               a.RNUM
        FROM (
                 SELECT o.ID_SHOP,
                        o.ART,
                        o.ASIZE,
                        o.KOL AS KOL,
                        -- DECODE(o.ID_SHOP, s.SHOP_TO, 4, DECODE(GREATEST(TRUNC(sh.DATE_OPEN) - TRUNC(ADD_MONTHS(SYSDATE,-2)), 0), 0, 2, 1)) PRIORITY
                        CASE WHEN (o.ID_SHOP = s.SHOP_TO)
                            THEN 4
                            ELSE (
                                CASE WHEN sh.DATE_OPEN < ADD_MONTHS(SYSDATE,-2)
                                    THEN 2
                                    ELSE 1
                                END
                            )
                        END PRIORITY,
                        b.RNUM
                 FROM E_OSTTEK_ONLINE o
                          JOIN TEM_INET_MOVE_MATRIX_BOOK2 b ON o.ART = b.ART
                          JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON (('BY' = UPPER(s.LAND_ID) AND INSTR(o.ID_SHOP, '00') = 1) OR
                                                                   ('RU' = UPPER(s.LAND_ID) AND INSTR(o.ID_SHOP, '00') != 1))
                          JOIN ST_SHOP sh ON o.ID_SHOP = sh.SHOPID AND sh.ORG_KOD = 'SHP' AND sh.BIT_OPEN = 'T' --AND
                                             --(s.IS_DELIVERY = 'F' OR UPPER(SPA_CITYNAME_WITHOUT_DOT(sh.CITYNAME)) = 'ВИТЕБСК')
                          LEFT JOIN POS_BRAK brak on o.SCAN = brak.SCAN AND o.id_shop = brak.id_shop
                 WHERE brak.SCAN IS NULL
                   AND o.PROCENT = 0

                 UNION ALL

                 SELECT b.ID_SHOP,
                        b.ART,
                        b.ASIZE,
                        -1 * b.KOL AS KOL,
                        CASE WHEN (b.ID_SHOP = s.SHOP_TO)
                            THEN 4
                            ELSE (
                                CASE WHEN sh.DATE_OPEN < ADD_MONTHS(SYSDATE,-2)
                                    THEN 2
                                    ELSE 1
                                END
                            )
                        END PRIORITY,
                        b2.RNUM
                 FROM POS_RESERVE_V b
                          JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s
                               ON b.ID_SHOP = s.SHOP_TO AND b.SERV_IP = s.SERVER_IP AND b.POSNR != s.POSNR
                          JOIN TEM_INET_MOVE_MATRIX_BOOK2 b2 ON b2.ART = b.ART
                          JOIN ST_SHOP sh ON b.ID_SHOP = sh.SHOPID AND sh.ORG_KOD = 'SHP' --AND
                                             --(s.IS_DELIVERY = 'F' OR UPPER(SPA_CITYNAME_WITHOUT_DOT(sh.CITYNAME)) = 'ВИТЕБСК')

                 UNION ALL

                  -- в счет ЗП, в незакрытом документе
                 SELECT r1.ID_SHOP,
                        r2.ART,
                        r2.ASIZE,
                        (-1 * r2.KOL) KOL,
                        -- DECODE(o.ID_SHOP, s.SHOP_TO, 4, DECODE(GREATEST(TRUNC(sh.DATE_OPEN) - TRUNC(ADD_MONTHS(SYSDATE,-2)), 0), 0, 2, 1)) PRIORITY
                        CASE WHEN (r1.ID_SHOP = s.SHOP_TO)
                            THEN 4
                            ELSE (
                                CASE WHEN sh.DATE_OPEN < ADD_MONTHS(SYSDATE,-2)
                                    THEN 2
                                    ELSE 1
                                END
                            )
                        END PRIORITY,
                        b.RNUM
                 FROM D_RASXOD1 r1
                          JOIN D_RASXOD2 r2 on r1.ID_SHOP = r2.ID_SHOP and r1.ID = r2.ID
                          JOIN TEM_INET_MOVE_MATRIX_BOOK2 b ON r2.ART = b.ART
                          JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON (('BY' = UPPER(s.LAND_ID) AND INSTR(r1.ID_SHOP, '00') = 1) OR
                                                                   ('RU' = UPPER(s.LAND_ID) AND INSTR(r1.ID_SHOP, '00') != 1))
                          JOIN ST_SHOP sh ON r1.ID_SHOP = sh.SHOPID AND sh.ORG_KOD = 'SHP' --AND
                                             --(s.IS_DELIVERY = 'F' OR UPPER(SPA_CITYNAME_WITHOUT_DOT(sh.CITYNAME)) = 'ВИТЕБСК')
                 WHERE r1.IDOP = '35'
                   AND r1.BIT_CLOSE = 'F'

                UNION ALL

                SELECT 'KI1000' ID_SHOP,
                       x.ART,
                       x.ASIZE,
                       SUM(x.VERME) KOL,
                       3 PRIORITY,
                       a.RNUM
                FROM RFC_FREE_STOCK x -- остатки СГП
                     JOIN TEM_INET_MOVE_MATRIX_BOOK2 a on a.ART = x.ART and a.ASIZE = x.ASIZE
                     JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON 'BY' = UPPER(s.LAND_ID)
                     LEFT JOIN D_INET_BOOK_SAP_STATUS ss ON ss.RNUM = a.RNUM AND ss.POSNR = s.POSNR AND ss.SERV_IP = s.SERVER_IP AND ss.STATUS_RNUM_SAP IN ('CL', 'DS', 'NS')
                WHERE ss.RNUM IS NULL
                group by x.ART, x.ASIZE, a.RNUM
                having sum(x.VERME) > 0

                UNION ALL

                SELECT 'NOT_OST' ID_SHOP, ART, ASIZE, kol, 0 PRIORITY, RNUM
                FROM TEM_INET_MOVE_MATRIX_BOOK2
                WHERE KOL > 0

                UNION ALL

                SELECT
                        z.ID_SHOP AS ID_SHOP,
                        z.ART,
                        z.ASIZE,
                        -10000 AS KOL,
                        -- DECODE(o.ID_SHOP, s.SHOP_TO, 4, DECODE(GREATEST(TRUNC(sh.DATE_OPEN) - TRUNC(ADD_MONTHS(SYSDATE,-2)), 0), 0, 2, 1)) PRIORITY
                        CASE WHEN (z.ID_SHOP_FROM = s.SHOP_TO)
                            THEN 4
                            ELSE (
                                CASE WHEN sh.DATE_OPEN < ADD_MONTHS(SYSDATE,-2)
                                    THEN 2
                                    ELSE 1
                                END
                            )
                        END PRIORITY,
                        b.RNUM
                 FROM D_INET_BOOK2 z
                          JOIN TEM_INET_MOVE_MATRIX_BOOK2 b ON b.ART = z.ART AND b.ASIZE = z.ASIZE AND b.RNUM = z.RNUM
                          JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON (('BY' = UPPER(s.LAND_ID) AND INSTR(z.ID_SHOP, '00') = 1) OR
                                                                   ('RU' = UPPER(s.LAND_ID) AND INSTR(z.ID_SHOP, '00') != 1))
                                                                   AND z.POSNR = s.POSNR
                                                                   AND z.SERV_IP = s.SERVER_IP
                          JOIN ST_SHOP sh ON z.ID_SHOP = sh.SHOPID AND sh.ORG_KOD = 'SHP' AND sh.BIT_OPEN = 'T' --AND
                                             --(s.IS_DELIVERY = 'F' OR UPPER(SPA_CITYNAME_WITHOUT_DOT(sh.CITYNAME)) = 'ВИТЕБСК')
                 WHERE z.BIT_MOVE = 'T'
             ) a
        GROUP BY a.ID_SHOP, a.ART, a.ASIZE, a.PRIORITY, a.RNUM
        HAVING SUM(a.KOL) > 0
    ) a
JOIN TEM_INET_MOVE_MATRIX_BOOK2 b ON a.ART = b.ART AND a.ASIZE =  b.ASIZE
JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 c ON 1=1
LEFT JOIN ST_SHOP e ON a.ID_SHOP = e.SHOPID
--LEFT JOIN FIRM.SPA_MOVE_MATRIX_TRANSPORT_COST d ON a.ID_SHOP = d.SHOP_FROM AND c.SHOP_TO = d.SHOP_TO
LEFT JOIN SPA_MOVE_MATRIX_DELIVERY_COST d ON lower(e.CITYNAME) LIKE '%'||lower(d.CITY_FROM) AND lower(c.DELIVERY_TOWN) like '%'||lower(d.city_to)||',%' 
ORDER BY a.PRIORITY DESC, d.COST, LENGTH(a.SIZES) - LENGTH(REPLACE(a.SIZES, '&', '')) DESC
;

select * from SPA_MOVE_MATRIX_DELIVERY_COST where lower('Витебск') LIKE '%'||lower(CITY_FROM) --AND lower(c.DELIVERY_TOWN) like '%'||lower(d.city_to)||',%' 
;
select * from TEM_INET_MOVE_MATRIX_OST3 order by priority desc, cost,id_shop, art, asize;

--INSERT INTO TEM_INET_MOVE_MATRIX_RESULT2 (POSNR, ART, ASIZE, KOL, ID_SHOP_FROM, ID_SHOP_TO, RNUM)
SELECT POSNR, ART, ASIZE, KOL, ID_SHOP, (CASE WHEN ID_SHOP = 'KI1000' AND 'T' = 'T' THEN NULL ELSE SHOP_TO END) SHOP_TO, RNUM
FROM (
        SELECT a.POSNR,
               a.ART,
               a.ASIZE,
               a.KOL,
               a.ID_SHOP,
               a.SHOP_TO,
               PRIORITY,
               RNUM,
               ROW_NUMBER() over (PARTITION BY a.ART, a.ASIZE, RNUM ORDER BY a.PRIORITY DESC, a.SHOP_KOL DESC, a.COST, a.SIZES_KOL DESC, a.RAND) AS ID
        FROM (
                SELECT s.POSNR,
                       o.ART,
                       o.ASIZE,
                       (CASE WHEN z.KOL > o.KOL THEN o.KOL ELSE z.KOL END) AS KOL,
                       o.ID_SHOP,
                       s.SHOP_TO,
                       o.PRIORITY,
                       SUM(CASE WHEN z.KOL > o.KOL THEN o.KOL ELSE z.KOL END) OVER (partition by o.ID_SHOP) AS SHOP_KOL,
                       o.COST,
                       SUM(LENGTH(o.SIZES) - LENGTH(REPLACE(o.SIZES, '&', ''))) OVER (partition by o.ID_SHOP) SIZES_KOL,
                       SUM(round(dbms_random.value() * 1000)) OVER (partition by o.ID_SHOP) rand,
                       z.RNUM
                FROM TEM_INET_MOVE_MATRIX_OST3 o
                         JOIN TEM_INET_MOVE_MATRIX_BOOK2 z ON z.ART = o.ART AND z.ASIZE = o.ASIZE AND o.RNUM = z.RNUM
                         JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON 1 = 1
                         LEFT JOIN TEM_INET_MOVE_MATRIX_RESULT r ON r.ART = z.ART AND r.ASIZE = z.ASIZE
                WHERE o.KOL > 0
                  AND z.KOL - COALESCE(r.KOL, 0) > 0
        ) a
        ORDER BY a.PRIORITY DESC, a.SHOP_KOL DESC, a.COST, a.SIZES_KOL DESC, a.RAND
)
WHERE id = 1;


SELECT a.POSNR,
       a.ART,
       a.ASIZE,
       a.KOL,
       a.ID_SHOP,
       a.SHOP_TO,
       PRIORITY,
       RNUM,
       ROW_NUMBER() over (PARTITION BY a.ART, a.ASIZE, RNUM ORDER BY a.PRIORITY DESC, a.SHOP_KOL DESC, a.COST, a.SIZES_KOL DESC, a.RAND) AS ID,
       ROW_NUMBER() over (PARTITION BY a.ART, a.ASIZE, RNUM ORDER BY a.PRIORITY DESC, a.SHOP_KOL DESC, a.COST, a.SIZES_KOL DESC, a.RAND) AS ID
FROM (
        SELECT s.POSNR,
               o.ART,
               o.ASIZE,
               (CASE WHEN z.KOL > o.KOL THEN o.KOL ELSE z.KOL END) AS KOL,
               o.ID_SHOP,
               s.SHOP_TO,
               o.PRIORITY,
               SUM(CASE WHEN z.KOL > o.KOL THEN o.KOL ELSE z.KOL END) OVER (partition by o.ID_SHOP) AS SHOP_KOL,
               o.COST,
               SUM(LENGTH(o.SIZES) - LENGTH(REPLACE(o.SIZES, '&', ''))) OVER (partition by o.ID_SHOP) SIZES_KOL,
               SUM(round(dbms_random.value() * 1000)) OVER (partition by o.ID_SHOP) rand,
               z.RNUM
        FROM TEM_INET_MOVE_MATRIX_OST3 o
                 JOIN TEM_INET_MOVE_MATRIX_BOOK2 z ON z.ART = o.ART AND z.ASIZE = o.ASIZE AND o.RNUM = z.RNUM
                 JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON 1 = 1
                 LEFT JOIN TEM_INET_MOVE_MATRIX_RESULT r ON r.ART = z.ART AND r.ASIZE = z.ASIZE
        WHERE o.KOL > 0
          AND z.KOL - COALESCE(r.KOL, 0) > 0
) a
ORDER BY a.PRIORITY DESC, a.SHOP_KOL DESC, a.COST, a.SIZES_KOL DESC, a.RAND
;

select b.id_shop--, sum(b.kol) kol, count(*) cnt, d.cnt
from tem_inet_move_matrix_book2 a
inner join (select * from tem_inet_move_matrix_ost3) b on a.art = b.art
                                                       and a.asize = b.asize
                                                       and a.kol <= b.kol
left join tdv_map_shop_reit c on b.id_shop = c.id_shop, 
(select count(*) cnt from tem_inet_move_matrix_book2) d
group by b.id_shop, d.cnt
--having count(*) = d.cnt
order by max(b.cost)
;

select * 
from tem_inet_move_matrix_book2 a
inner join (select *
           from tem_inet_move_matrix_ost3) b on a.art = b.art
                                               and a.asize = b.asize
                                               and a.kol <= b.kol
;


select * from TEM_INET_MOVE_MATRIX_OST3;
SELECT POSNR, ART, ASIZE, KOL, ID_SHOP, (CASE WHEN ID_SHOP = 'KI1000' AND 'T' = 'T' THEN NULL ELSE SHOP_TO END) SHOP_TO, RNUM
FROM (
      SELECT a.POSNR,
             a.ART,
             a.ASIZE,
             a.KOL,
             a.ID_SHOP,
             a.SHOP_TO,
             PRIORITY,
             RNUM,
             a.COST,
             ROW_NUMBER() over (PARTITION BY a.ART, a.ASIZE, RNUM ORDER BY a.PRIORITY DESC,/* a.SHOP_KOL DESC,*/ a.COST, a.SIZES_KOL DESC, a.RAND) AS ID
      FROM (
            SELECT s.POSNR,
                   o.ART,
                   o.ASIZE,
                   z.KOL AS KOL,
                   o.ID_SHOP,
                   s.SHOP_TO,
                   o.PRIORITY,
            --       SUM(CASE WHEN z.KOL > o.KOL THEN o.KOL ELSE z.KOL END) OVER (partition by o.ID_SHOP) AS SHOP_KOL,
                   o.COST,
                   SUM(LENGTH(o.SIZES) - LENGTH(REPLACE(o.SIZES, '&', ''))) OVER (partition by o.ID_SHOP) SIZES_KOL,
                   SUM(round(dbms_random.value() * 1000)) OVER (partition by o.ID_SHOP) rand,
                   z.RNUM,
                   c.CNT_RNUM,
                   COUNT(o.RNUM) OVER (partition by o.ID_SHOP) AS cnt_rnum_shop
            FROM TEM_INET_MOVE_MATRIX_OST3 o
                     JOIN TEM_INET_MOVE_MATRIX_BOOK2 z ON z.ART = o.ART AND z.ASIZE = o.ASIZE AND o.RNUM = z.RNUM
                     JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON 1 = 1
                     LEFT JOIN TEM_INET_MOVE_MATRIX_RESULT r ON r.ART = z.ART AND r.ASIZE = z.ASIZE,
                     (SELECT COUNT(*) CNT_RNUM FROM TEM_INET_MOVE_MATRIX_BOOK2) c
            WHERE o.KOL > 0 
              AND o.KOL >= z.KOL
              AND z.KOL - COALESCE(r.KOL, 0) > 0
      ) a
      WHERE CNT_RNUM = cnt_rnum_shop
      ORDER BY a.PRIORITY DESC,/* a.SHOP_KOL DESC,*/ a.COST, a.SIZES_KOL DESC, a.RAND
)
WHERE id = 1;



SELECT 
s.POSNR, o.ART, o.ASIZE, z.KOL, o.ID_SHOP, (CASE WHEN o.ID_SHOP = 'KI1000' AND 'T' = 'T' THEN NULL ELSE s.SHOP_TO END) SHOP_TO, z.RNUM
,
o.*, z.*, s.*, r.*
FROM TEM_INET_MOVE_MATRIX_OST3 o
JOIN TEM_INET_MOVE_MATRIX_BOOK2 z ON z.ART = o.ART AND z.ASIZE = o.ASIZE AND o.RNUM = z.RNUM
JOIN TEM_INET_MOVE_MATRIX_SETTINGS2 s ON 1 = 1
LEFT JOIN TEM_INET_MOVE_MATRIX_RESULT r ON r.ART = z.ART AND r.ASIZE = z.ASIZE
WHERE o.KOL > 0
  AND z.KOL - COALESCE(r.KOL, 0) > 0
  AND o.ID_SHOP = 'NOT_OST'
;


select * from TEM_INET_MOVE_MATRIX_OST3;
select * from TEM_INET_MOVE_MATRIX_SETTINGS2;
select * from SPA_MOVE_MATRIX_DELIVERY_COST;
select * from st_shop;


select a.*, b.cityname
from TEM_INET_MOVE_MATRIX_OST3 a
left join st_shop b on a.id_shop = b.shopid;


-- определяю Самовывоз или Доставка на адрес
SELECT a.DELIVERY_TOWN, b.*, a.*

FROM D_INET_BOOK1 a
left join SPA_MOVE_MATRIX_DELIVERY_COST b on lower(a.DELIVERY_TOWN) like '%'||lower(b.city_to)||',%' 
WHERE
BIT_DELIVERY_ADDR = 'T'
--and a.POSNR = '00341494'
--AND a.SERV_IP = '192.168.22.252'
order by dated desc
;

select '%'||lower(city_to)||',%' from SPA_MOVE_MATRIX_DELIVERY_COST;