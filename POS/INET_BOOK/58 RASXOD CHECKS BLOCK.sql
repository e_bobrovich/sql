select * from d_inet_book1 where posnr = '01986007';
select * from d_inet_book2 where posnr = '01986007';

select * from e_osttek where art = '8815095'; --and asize = 43


select scan from d_rasxod2 where id = 619;
select * from d_inet_book2 where scan = '00000000001006343519';


select x.art,x.asize,x.scan,x.kol,k.book_kol
from d_rasxod2 x
inner join (
        select x.art,x.asize,x.scan,x.bit_accept,sum(x.kol) book_kol
        from d_inet_book2 x
        inner join (
          SELECT distinct a.posnr,a.serv_ip
          FROM D_INET_BOOK1 a
          left join (
              select posnr,serv_ip,min(bit_sale) bit_sale,max(bit_block) bit_block,min(bit_move) bit_move,min(nvl(rx_id,0)) rx_id
              from d_inet_book2
              group by posnr,serv_ip
          ) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip
          where a.bit_status = 'T' and c.bit_sale = 'F' and c.bit_block = 'T' and not (c.bit_move = 'T' and c.rx_id != 0)
          and c.bit_move = 'T'
        ) y on x.posnr = y.posnr and x.serv_ip = y.serv_ip
        group by x.art,x.asize,x.scan,x.bit_accept
) k on k.art = x.art and k.asize = x.asize and k.scan = x.scan and k.bit_accept = 'T'
where x.id = 619 and k.book_kol >= x.kol;

select count(posnr) from (
  select
  x.scan, y.posnr
  from d_rasxod2 x
  
  left join d_inet_book2 y on x.scan = y.scan
  where x.id = 619
)
;


select b.posnr, count(*)
FROM D_RASXOD2 A
 INNER JOIN D_INET_BOOK2 B ON A.SCAN = B.SCAN
 INNER JOIN D_INET_BOOK1 C ON B.POSNR = C.POSNR AND B.SERV_IP = C.SERV_IP
WHERE id = 619 
AND C.BIT_BOOK = 'F' AND B.BIT_MOVE = 'T' AND C.BIT_DELIVERY_ADDR = 'T' AND COALESCE(B.ID_CANCEL, 0) = 0
group by b.posnr
;

select b.posnr, a.scan
FROM D_RASXOD2 A
 INNER JOIN D_INET_BOOK2 B ON A.SCAN = B.SCAN
 INNER JOIN D_INET_BOOK1 C ON B.POSNR = C.POSNR AND B.SERV_IP = C.SERV_IP
WHERE id = 619 
AND C.BIT_BOOK = 'F' AND B.BIT_MOVE = 'T' AND C.BIT_DELIVERY_ADDR = 'T' AND COALESCE(B.ID_CANCEL, 0) = 0
;
--where a.bit_status = 'T' and c.bit_sale = 'F' and c.bit_block = 'T' and not (c.bit_move = 'T' and c.rx_id != 0)


select b.posnr, count(b.scan) count_scan, sum(a.kol) rx_kol, sum(b.kol) book_kol
--b.posnr, a.*, b.*,c.*
from d_rasxod2 a
 inner join d_inet_book2 b on a.scan = b.scan
 inner join d_inet_book1 c on b.posnr = c.posnr and b.serv_ip = c.serv_ip
where a.id = 619 
and c.bit_status = 'T' and b.bit_sale = 'F' and c.bit_book = 'F' 
and b.bit_move = 'T' and c.bit_delivery_addr = 'T' and coalesce(b.id_cancel, 0) = 0
and b.dpd_id is not null
--and not (b.bit_move = 'T' and b.rx_id != 0)
group by b.posnr
having sum(a.kol) >= sum(b.kol)
;
