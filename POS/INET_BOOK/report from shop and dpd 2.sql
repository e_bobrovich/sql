select x.posnr,x.serv_ip,x.rnum,
  max(x.art) art,max(x.asize) asize,
	max(x.scan) scan,max(x.kol) kol,
	max(x.dpd_id) dpd_id,
  max(x.shopid) shopid,
  max(x.id_shop_to_num) id_shop_to_num,max(x.id_shop_from_num) id_shop_from_num
from (select posnr,serv_ip,rnum,
        case when bit_move = 'F' then shopid else null end shopid,
				case when bit_move = 'F' then art else null end art,
				case when bit_move = 'F' then asize else null end asize,
				case when bit_move = 'F' then scan else null end scan,
				case when bit_move = 'F' then kol else null end kol,
				case when bit_move = 'F' then dpd_id else null end dpd_id,
				case when bit_move = 'F' then id_shop_to_num else null end id_shop_to_num,
				case when bit_move = 'F' then id_shop_from_num else null end id_shop_from_num
			from (SELECT a.posnr,a.serv_ip,b.rnum,
						b.art,b.asize,b.scan,b.kol,b.bit_move,g.dpd_id,
						b.id_shop_to id_shop_to_num, b.id_shop_from id_shop_from_num,
            case when b.id_shop = 'N/A' then 'ДОСТАВКА НА АДРЕС' when b.id_shop = 'KI1000' then 'СГП' else c.shopid end shopid
						FROM d_inet_book1 a
						inner join D_INET_BOOK2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
						left join (select bit_move,rnum,posnr,serv_ip,listagg(substr(shopnum,2)||' '||cityname||' '||address,', ') within group (order by id_shop) shopid
									from (select distinct x.bit_move,x.rnum,x.posnr,x.serv_ip,x.id_shop,y.shopnum,y.cityname,y.address
												from d_inet_book2 x
												left join s_shop y on x.id_shop = y.shopid
												where x.bit_delete = 'F')
									group by bit_move,rnum,posnr,serv_ip) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip and c.rnum = b.rnum and c.bit_move = b.bit_move
						inner join d_inet_book4 g on b.posnr = g.posnr and b.serv_ip = g.serv_ip and b.rnum = g.rnum and b.bit_move = g.bit_move
						left join s_shop e on e.shopid = b.id_shop_to
						left join s_shop f on f.shopid = b.id_shop_from
						where b.bit_delete = 'F' and a.posnr = '00024240'
						and a.serv_ip = '192.168.130.170')) x
group by x.posnr,x.serv_ip,x.rnum
order by x.rnum;

-- поулчение заказов у которых хотя бы одна позиции имеет отправителя магазин И к этой позиции привязан номер ДПД
select x.posnr,x.serv_ip--,x.rnum,
from (select posnr,serv_ip,rnum,
				case when bit_move = 'F' then art else null end art,
				case when bit_move = 'F' then asize else null end asize,
				case when bit_move = 'F' then scan else null end scan,
				case when bit_move = 'F' then kol else null end kol,
				case when bit_move = 'F' then dpd_id else null end dpd_id,
				case when bit_move = 'F' then id_shop_to_num else null end id_shop_to_num,
				case when bit_move = 'F' then id_shop_from_num else null end id_shop_from_num
			from (
            SELECT a.posnr,a.serv_ip,b.rnum,
						b.art,b.asize,b.scan,b.kol,b.bit_move,g.dpd_id,
						b.id_shop_to id_shop_to_num, b.id_shop_from id_shop_from_num
						FROM d_inet_book1 a
						inner join D_INET_BOOK2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
						left join (select bit_move,rnum,posnr,serv_ip,listagg(substr(shopnum,2)||' '||cityname||' '||address,', ') within group (order by id_shop) shopid
									from (select distinct x.bit_move,x.rnum,x.posnr,x.serv_ip,x.id_shop,y.shopnum,y.cityname,y.address
												from d_inet_book2 x
												left join s_shop y on x.id_shop = y.shopid
												where x.bit_delete = 'F')
									group by bit_move,rnum,posnr,serv_ip) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip and c.rnum = b.rnum and c.bit_move = b.bit_move
						inner join d_inet_book4 g on b.posnr = g.posnr and b.serv_ip = g.serv_ip and b.rnum = g.rnum and b.bit_move = g.bit_move
						left join s_shop e on e.shopid = b.id_shop_to
						left join s_shop f on f.shopid = b.id_shop_from
						where b.bit_delete = 'F'             
      )
) x
group by x.posnr,x.serv_ip
having max(x.dpd_id) != ' ' and regexp_replace(max(x.id_shop_to_num), '[^[:digit:]]', '') = max(x.id_shop_to_num)
order by x.posnr desc, x.serv_ip
;

--Номер заказа/Дата заказа/Получатель/артикул/размер/статус в магазине/стоимость 
select 
  a.posnr "Номер заказа", 
  a.dated "Дата заказа",
  a.id_shop_from_num "Отправитель",
  a.id_shop_to_num "Получатель",
  a.dpd_id "Номер DPD",
  a.art "Артикул",
  a.asize "Размер",
--  f.shopcond "Статус в маг.",
  g.shopcond "Статус в маг.",
  a.sum "Стоимость"
  from (
    select x.posnr,x.serv_ip,x.rnum,
    max(x.dated) dated,
    max(x.art) art,max(x.asize) asize,
    max(x.sum) sum,
    max(x.dpd_id) dpd_id,
    max(x.id_shop_to_num) id_shop_to_num,
    max(x.id_shop_from_num) id_shop_from_num
    from (select posnr,serv_ip,rnum,dated, sum,
          case when bit_move = 'F' then art else null end art,
          case when bit_move = 'F' then asize else null end asize,
          case when bit_move = 'F' then dpd_id else null end dpd_id,
          case when bit_move = 'F' then id_shop_to_num else null end id_shop_to_num,
          case when bit_move = 'F' then id_shop_from_num else null end id_shop_from_num
        from (SELECT a.posnr,a.serv_ip,b.rnum,
              a.dated, b.sum,
              b.art,b.asize,b.kol,b.bit_move,g.dpd_id,
              b.id_shop_to id_shop_to_num, b.id_shop_from id_shop_from_num
              FROM d_inet_book1 a
              inner join D_INET_BOOK2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
              left join (select bit_move,rnum,posnr,serv_ip,listagg(substr(shopnum,2)||' '||cityname||' '||address,', ') within group (order by id_shop) shopid
                    from (select distinct x.bit_move,x.rnum,x.posnr,x.serv_ip,x.id_shop,y.shopnum,y.cityname,y.address
                          from d_inet_book2 x
                          left join s_shop y on x.id_shop = y.shopid
                          where x.bit_delete = 'F')
                    group by bit_move,rnum,posnr,serv_ip) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip and c.rnum = b.rnum and c.bit_move = b.bit_move
              inner join d_inet_book4 g on b.posnr = g.posnr and b.serv_ip = g.serv_ip and b.rnum = g.rnum and b.bit_move = g.bit_move
              left join s_shop e on e.shopid = b.id_shop_to
              left join s_shop f on f.shopid = b.id_shop_from
              where b.bit_delete = 'F' 
        )
  ) x
  group by x.posnr,x.serv_ip,x.rnum
) a
inner join (
    select x.posnr,x.serv_ip--, max(x.dated)--,x.rnum,
    from (select posnr,serv_ip,rnum, dated,
            case when bit_move = 'F' then art else null end art,
            case when bit_move = 'F' then asize else null end asize,
            case when bit_move = 'F' then scan else null end scan,
            case when bit_move = 'F' then kol else null end kol,
            case when bit_move = 'F' then dpd_id else null end dpd_id,
            case when bit_move = 'F' then id_shop_to_num else null end id_shop_to_num,
            case when bit_move = 'F' then id_shop_from_num else null end id_shop_from_num
          from (
                SELECT a.posnr,a.serv_ip,b.rnum,
                a.dated,
                b.art,b.asize,b.scan,b.kol,b.bit_move,g.dpd_id,
                b.id_shop_to id_shop_to_num, b.id_shop_from id_shop_from_num
                FROM d_inet_book1 a
                inner join D_INET_BOOK2 b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
                left join (select bit_move,rnum,posnr,serv_ip,listagg(substr(shopnum,2)||' '||cityname||' '||address,', ') within group (order by id_shop) shopid
                      from (select distinct x.bit_move,x.rnum,x.posnr,x.serv_ip,x.id_shop,y.shopnum,y.cityname,y.address
                            from d_inet_book2 x
                            left join s_shop y on x.id_shop = y.shopid
                            where x.bit_delete = 'F')
                      group by bit_move,rnum,posnr,serv_ip) c on c.posnr = a.posnr and c.serv_ip = a.serv_ip and c.rnum = b.rnum and c.bit_move = b.bit_move
                inner join d_inet_book4 g on b.posnr = g.posnr and b.serv_ip = g.serv_ip and b.rnum = g.rnum and b.bit_move = g.bit_move
                left join s_shop e on e.shopid = b.id_shop_to
                left join s_shop f on f.shopid = b.id_shop_from
                where b.bit_delete = 'F'             
          )
    ) x
    group by x.posnr,x.serv_ip
    having max(x.dpd_id) != ' ' and regexp_replace(max(x.id_shop_from_num), '[^[:digit:]]', '') = max(x.id_shop_from_num)
            and to_char(max(x.dated), 'yyyymmdd') >= '20200101' and to_char(max(x.dated), 'yyyymmdd') <= '20200409'
) b on a.posnr = b.posnr and a.serv_ip = b.serv_ip
--left join D_INET_BOOK_COND_T f on f.posnr = a.posnr and f.serv_ip = a.serv_ip
left join D_INET_BOOK_COND_POS g on g.posnr = a.posnr and g.serv_ip = a.serv_ip and g.rnum = a.rnum
--where to_char(a.dated, 'yyyymmdd') >= '20200101' and to_char(a.dated, 'yyyymmdd') <= '20200409'
order by a.dated desc, a.posnr desc,a.serv_ip, a.rnum;