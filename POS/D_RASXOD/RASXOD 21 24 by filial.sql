--Страна, номер магазина, код операции (21 и 24), номер расхода, дата расхода, номер задания, дата задания, сезон, артикул, размер, штрихкод, количество.
select 
  S.landid "Страна", 
  substr(s.shopnum,2,5) "Номер магазина",
  a.idop "Номер операции",
  a.id "Номер расхода", 
  to_char(a.dated, 'dd.mm.yyyy') "Дата расхода", 
  a.rel_planot "Номер задания",
  to_char(p.dated, 'dd.mm.yyyy') "Дата задания",
  c.season "Сезон",
  b.art "Артикул",
  b.asize "Размер",
  b.scan "Штрихкод",
  b.kol "Количество"
--  a.*, b.* 
from d_rasxod1 a
inner join d_rasxod2 b on a.id = b.id and a.id_Shop = b.id_shop  
left join s_art c on b.art = c.art
left join s_shop s on a.id_Shop = s.shopid
left join Prem_Retail_Hierarchy x on a.id_shop = x.shopid
left join d_planot1 p on a.rel_planot = p.rel and a.id_Shop = p.kpodr
where X.Shopparent = '27F1' and a.id_shop != '2731' and a.idop in ('24', '21')

and to_char(a.dated, 'yyyymmdd') >= 20200101 and to_char(a.dated, 'yyyymmdd') <= 20200331
order by a.dated, a.id_Shop, a.id, b.art, b.asize
;

select * from Prem_Retail_Hierarchy;

select * from d_planot1;
select * from s_shop;
select * from s_art where art = '9С199К45 КОР';