----вот так вызываем функцию
set serveroutput on
begin
kaa_sebest_eq.ZFM_STOCK_END_PERIOD('46F1', '20200430', 'R4P300');
end;


select shopid, min (datec) from rfc_seb_eq_et_list_t group by shopid;

--delete from KAA_SEBEST_SAP_OST where bwkey like '43%' and dated = 20200501;


--только перед перекидыванием нужно по свх сделать манипуляции

update rfc_seb_eq_et_list_t
set shopid = 'W431'
where shopid = '43F1' and datec = '20200501' and lgort = '9001'
;

update rfc_seb_eq_et_list_t
set shopid = 'W432'
where shopid = '43F1' and datec = '20200501' and lgort = '9003'
;


--а так перекидываем данные в kaa_sebest_sap_ost
set serveroutput on
begin
kaa_sebest_eq.STOCK_END_PERIOD_TO_SAP_OST('W391', '20200430');
end;

select * from rfc_seb_eq_et_list_t w where shopid like '39F1%';



select shopid 
from st_shop 
where substr(shopid,3,2) not in ('F2', 'D0')
  and bit_open = 'T' 
  and org_kod in ('SHP','WRH','RCT', 'FIL') 
  and shopid not like '00%' 
  and (shopid like '46'||'%' or shopid like 'W'||'46'||'%')
  order by shopid
;


select shopid from rfc_seb_eq_et_list_t w 
where (shopid like '46'||'%' or shopid like 'W'||'46'||'%')
and datec = 20200430
group by shopid 
order by shopid
;


SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor cur1 is 
  select shopid 
  from st_shop 
  where substr(shopid,3,2) not in ('F2', 'D0')
    and bit_open = 'T' 
    and org_kod in ('SHP','WRH','RCT', 'FIL') 
    and shopid not like '00%' 
    and (shopid like '46'||'%' or shopid like 'W'||'46'||'%')
    order by shopid
  ;
cursor cur2 is 
  select shopid from rfc_seb_eq_et_list_t w 
  where (shopid like '46'||'%' or shopid like 'W'||'46'||'%')
  and datec = 20200430
  group by shopid 
  order by shopid
  ;
curRow1 cur1%rowtype;
curRow2 cur2%rowtype;
p_shopid varchar2(20 CHAR);
--**********************
type_load number := 2;
--**********************
BEGIN

if type_load = 1 then 
  open cur1;
  loop      
       FETCH cur1 INTO p_shopid;
       EXIT WHEN cur1%NOTFOUND;  
          begin
            dbms_output.put_line('p_shopid - '|| p_shopid);
            kaa_sebest_eq.ZFM_STOCK_END_PERIOD(p_shopid, '20200430', 'R4P300');
            
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line('p_shopid - '|| p_shopid);
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
else
  open cur2;
  loop      
       FETCH cur2 INTO p_shopid;
       EXIT WHEN cur2%NOTFOUND;  
          begin
            dbms_output.put_line('p_shopid - '|| p_shopid);
            kaa_sebest_eq.STOCK_END_PERIOD_TO_SAP_OST(p_shopid, '20200430');
            
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line('p_shopid - '|| p_shopid);
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
end if;
END ;

--begin KAA_SEBEST_EQ.SAP_EQ_PROC('20200501','2827'); end;