select * from FIRM.KAA_SEBEST_SAP_OST where dated = 20200501 and bwkey like '39F1%';
select count(*) from FIRM.KAA_SEBEST_SAP_OST where dated = 20200501 and bwkey like '39S4%';
select bwkey, count(*) from FIRM.KAA_SEBEST_SAP_OST where dated = 20200501 and bwkey like '22%' group by bwkey;

--------------------------------------------------------------------------------

select bwkey, count(*) from KAA_SEBEST_SAP_OST where dated = 20200501 and (bwkey like '22%' or bwkey like 'W'||'22%') group by bwkey order by bwkey;
select bwkey, count(*) from KAA_SEBEST_SAP_EQ where (bwkey like '22%' or bwkey like 'W'||'22%') group by bwkey order by bwkey;
select bwkey, count(*) from WR_TABLE_EXPORT_PARAM_1_TEST where (bwkey like '22%' or bwkey like 'W'||'22%') group by bwkey order by bwkey;

--------------------------------------------------------------------------------
select * from KAA_SEBEST_SAP_OST where dated = 20200501 and verpr like '%,%' and bwkey like '39%';

-- ЗАмена ',' на '.'
update KAA_SEBEST_SAP_OST set verpr = replace(verpr, ',','.'), salk3 = replace(salk3, ',','.') where dated = 20200501 and bwkey like '39%';
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Проверка на 2 знака после точки
select a.*, 
instr(verpr,'.',1) + 2,
substr(verpr, 0, instr(verpr,'.',1) + 2) a1
from FIRM.KAA_SEBEST_SAP_OST a where dated = 20200501
and length(substr(verpr,instr(verpr,'.',1)+1)) > 2 and verpr like '%.%' and bwkey like '39%'
;

update KAA_SEBEST_SAP_OST set verpr = substr(verpr, 0, instr(verpr,'.',1) + 2)
where dated = 20200501
and length(substr(verpr,instr(verpr,'.',1)+1)) > 2 and verpr like '%.%' and bwkey like '26%';
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Проверка на сумму = количество * цену
select a.*,
to_number(verpr) * to_number(kol) a1,
to_number(salk3) a2,
(to_number(verpr) * to_number(kol)) - to_number(salk3) a3,
to_number(salk3)/ to_number(kol) a4
from FIRM.KAA_SEBEST_SAP_OST a where dated = 20200501 and bwkey like '29%'
and to_number(verpr) * to_number(kol) != to_number(salk3);
--------------------------------------------------------------------------------

select * from kaa_sebest_sap_ost where  bwkey like '39%';
--------------------------------------------------------------------------------
-- Очистка склада -0001
update kaa_sebest_sap_ost
set bwkey = substr(bwkey,1,4)
where dated = '20200501' and bwkey like '22%' and bwkey like '%-0001'; -- для магазинов
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Проверка соответсвия складов СВХ

select * from rfc_seb_eq_et_list_t where shopid like '46F1';

select * from rfc_seb_eq_et_list_t where shopid like '39F1';

select lgort, min(b.art) min_matnr, max(b.art) max_matnr from rfc_seb_eq_et_list_t a
inner join s_all_mat b on a.matnr = b.matnr
where shopid like '39F1' and datec = 20200430 group by lgort;

select * from rfc_seb_eq_et_list_t;

select * from s_all_mat where matnr = 'F17373957-F390';

select 'SAP' tip,a.werks||'-'||a.lgort shopid,b.art,sum(to_number(a.quantity_end_of_period)) kol
from rfc_seb_eq_et_list_t a
inner join s_all_mat b on a.matnr = b.matnr and b.trademark_sat != '003'
where 
datec = '20200430' and werks like '39F%' and b.art = '013004'
group by a.werks||'-'||a.lgort,b.art
union all
select 'POS' tip,shopid,art,sum(kol*koef)
from kart_v
where shopid in ('W391','W392', 'W393') and art = '013004' 
and to_char(dated,'yyyymmdd') < '20200501'
group by shopid,art
having sum(kol*koef) > 0;

update rfc_seb_eq_et_list_t
set shopid = 'W391'
where shopid = '39F1' and datec = '20200430' and lgort = '9001'
;

update rfc_seb_eq_et_list_t
set shopid = 'W392'
where shopid = '39F1' and datec = '20200390' and lgort = '9003'
;
--------------------------------------------------------------------------------

--begin KAA_SEBEST_EQ.SAP_EQ_PROC('20200501','W391'); end;

--------------------------------------------------------------------------------
-- Изменение статуса для работы RFC
select * from KAA_SEBEST_SAP_EQ where bwkey like '39S4%';

--update KAA_SEBEST_SAP_EQ set bit_exp = 'F' where bwkey like '30%' and bit_exp = 'T';
update KAA_SEBEST_SAP_EQ set bit_exp = 'T' where bwkey like '2227%' and bit_exp = 'F';

select count(*) from KAA_SEBEST_SAP_EQ where (bwkey like '39%' or bwkey like 'W39%') and  bit_exp = 'F';
select count(*) from KAA_SEBEST_SAP_EQ where (bwkey like '39%' or bwkey like 'W39%') and  bit_exp = 'T';
--------------------------------------------------------------------------------
select count(*) from KAA_SEBEST_SAP_EQ where bit_exp = 'T';

--------------------------------------------------------------------------------
-- Запуск и отслеживание RFC функции
--select KAA_JAVA_CALL_SERVICE_FUN() into v_result from dual;

select * from WR_TABLE_EXPORT_PARAM_1_TEST where bwkey like '39%';
select count(*) from WR_TABLE_EXPORT_PARAM_1_TEST where bwkey like '39S4%';

select * from IDOCS.RFC_STAT where id_fun = 1 order by time_start desc; --ZPRICE_SEB
select * from IDOCS.RFC_STAT where id_fun = 24 order by time_start desc; --ZPRICE_SEB
select * from IDOCS.RFC_ERRORS where fun_name = 'ZPRICE_SEB' order by time_in desc;

--begin KAA_SEBEST_EQ.RFC_EXPORT('20200501'); end;
select * from kaa_sebest_errors order by id_shop, art;

select * from kaa_sebest_errors where id_shop like '%' order by id_shop, art;

select * from temp_reports order by dated desc;

select * from temp_reports where id_shop like '33%' order by dated desc;
select * from temp_reports where prim = 'MOVEMENT_EQ_COMPLETE' and id_shop like '25%' order by dated desc;
select bwkey from KAA_SEBEST_SAP_OST where dated = 20200501 and bwkey like '31%' group by bwkey;

--begin KAA_SEBEST_EQ.ODGRUZ_EQ('31','20200205'); end; --(по всему 1 филиалу).

select * from firm.kaa_sebest_trans_eq where id_shop like '22%' order by id_shop;

