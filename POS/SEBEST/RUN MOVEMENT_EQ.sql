select shopid 
from st_shop 
where substr(shopid,1,2) in ('35')
	and bit_open = 'T' and org_kod = 'SHP'
;


select * from st_shop_Access where shopid = '2221';

select * from temp_reports where prim like '39%MOVEEQ';

SET serveroutput ON
BEGIN 
KAA_SEBEST_EQ.MOVEMENT_EQ('3528',null,'F','20200501');
commit;
end;

SET serveroutput ON
BEGIN 
KAA_SEBEST_EQ.MOVEMENT_EQ_DC('28W1',null,'F','20200501');
commit;
end;

SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor cur is 
select shopid 
from st_shop 
where substr(shopid,1,2) in ('35')
	and bit_open = 'T' and org_kod = 'SHP'
;
curRow cur%rowtype;
p_shopid varchar2(20 CHAR);
BEGIN
 
open cur;
  loop  
       FETCH cur INTO p_shopid;
       EXIT WHEN cur%NOTFOUND;  
          begin
          
					begin
						DBMS_SCHEDULER.drop_job( 'kaa_j_mov_eq_seb_'||p_shopid);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'kaa_j_mov_eq_seb_'||p_shopid,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
BEGIN 
KAA_SEBEST_EQ.MOVEMENT_EQ(']'||p_shopid||q'[',null,'F','20200501');
commit;
END;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('kaa_j_mov_eq_seb_'||p_shopid); 
					
          commit;
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
  
END ;