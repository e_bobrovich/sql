--2208
--если было перемещение с одного магазина на другой, то товар пропадет с остатка этого магазина в сапе только в момент оприходования товара другим магазином
--поэтому перемещения, приходы по которым были после нужной даты, вычитаем из оборотки сапа

--bwkey in ('2209') and matnr in ('F18220810-F220')
--bwkey in ('2208') and matnr in ('86101_00000088', '86101_00000090')
--bwkey in ('2203') and matnr in ('F12222220-F220','F12222220-F220')

select * from kaa_sebest_errors where (id_shop like '22%' or id_shop like 'W'||'22%') order by id_shop, art, matnr, err;
select * from kaa_sebest_errors where (id_shop like '22%' ) order by id_shop, art, matnr, err;

select * from kaa_sebest_errors where id_shop in ('2201') and matnr in ('86001_00001221', '86223_00000047');

select * from kaa_sebest_sap_ost where bwkey = '2208';

select * from KAA_SEBEST_SAP_EQ where bwkey = '2208';

select * from WR_TABLE_EXPORT_PARAM_1_TEST where bwkey = '2208';

select count(*) from BACKUP_FIRM_TRADE_1.WR_TABLE_EXPORT_PARAM_1_TEST where bwkey = '2201';
select count(*) from WR_TABLE_EXPORT_PARAM_1_TEST where bwkey = '2201';

select * from d_prixod1@s2224 where idop = '50';
select * from d_prixod1@sdc where idop = '50';

select * from BACKUP_FIRM_TRADE_1.WR_TABLE_EXPORT_PARAM_1_TEST where bwkey = '2201'
and (bwkey, bwtar, charg, exidv, mandt, matnr, peinh, verpr, scan, partno, lbkum, salk3, waers_s, waers_v, kol, cena1, kol_ch, bit_sop, date_s) not in (
  select bwkey, bwtar, charg, exidv, mandt, matnr, peinh, verpr, scan, partno, lbkum, salk3, waers_s, waers_v, kol, cena1, kol_ch, bit_sop, date_s
  from WR_TABLE_EXPORT_PARAM_1_TEST where bwkey = '2201'
)
;

select shop,err,matnr,art,asize,sum(kol),sum(cena1)
from (
  select '1', '2208' shop,'SAP_OST_EQ_ERROR' err,q.matnr,p.art,p.asize,sum(kol) kol,sum(cena1) cena1,0 rxkol
  from (select y.art,nvl(y.asize,0) asize,sum(to_number(kol)) kol,sum(to_number(salk3)) cena1
        from kaa_sebest_sap_ost x
        inner join s_all_mat y on x.matnr = y.matnr
        where x.bwkey = '2208' and x.dated = '20200501'
        group by y.art,nvl(y.asize,0)
        union all
        
--        select art,asize,-sum(kol),-sum(kol*cena1)
--        from d_prixod2@S2208
--        where id in (select id from d_prixod1@S2208 where idop = '50' and to_char(dated,'yyyyMMdd') = '20200501')
--        group by art,asize
        
        select art,asize,-sum(kol),-sum(kol*cena1)
        from d_prixod2
        where id_shop = '2208' and id in (select id from d_prixod1 where id_shop = '2208' and idop = '50' and to_char(dated,'yyyyMMdd') = '20200501')
        group by art,asize
        ) p
  inner join s_all_mat q on p.art = q.art and p.asize = nvl(q.asize,0)
  group by q.matnr,p.art,p.asize
  having sum(kol) != 0 or sum(cena1) != 0
  
  union all
  
  select '2', '2208','SAP_OST_EQ_ERROR',matnr,art,asize,-sum(kol),-sum(cena1),-1 rxkol
  from (
    select e.matnr,b.art,b.asize,b.scan,b.partno,b.kol,b.cena1
    from d_rasxod1 a
    inner join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop
    inner join d_sap_odgruz1 c on c.postno_lifex = to_char(a.id) and c.pr_ud = 0 and c.skladid = a.id_shop
    inner join d_prixod1 d on d.postno = c.postno and d.id_shop = a.kkl and to_char(d.dated,'yyyymmdd') >= '20200501'
    inner join s_all_mat e on e.art = b.art and nvl(e.asize,0) = b.asize and e.trademark_sat != '003'
    where a.bit_close = 'T' and a.idop in ('22','22','22','22','53') and a.id_shop = '2208'
    and to_char(a.dated,'yyyymmdd') < '20200501'
  )
  group by matnr,art,asize
)
group by shop,err,matnr,art,asize
having sum(kol) != 0 or (sum(cena1) != 0 and sum(rxkol) = 0)
;


select '2222','BAD_RX_CENA1',scan,partno,art,asize,ID
from d_rasxod2
where id in (select id from d_rasxod1 where idop = '51' and to_char(dated,'yyyyMMdd') = '20200501' and id_shop = '2222')
and id_shop = '2222' and cena1 <= 0;

select * from (
select y.art,nvl(y.asize,0) asize,sum(to_number(kol)) kol,sum(to_number(salk3)) cena1
        from kaa_sebest_sap_ost x
        inner join s_all_mat y on x.matnr = y.matnr
        where x.bwkey = '2208' and x.dated = '20200501'
        group by y.art,nvl(y.asize,0)
        union all
        select art,asize,-sum(kol),-sum(kol*cena1)
        from d_prixod2@S2208
        where id in (select id from d_prixod1@S2208 where idop = '50' and to_char(dated,'yyyyMMdd') = '20200501')
        group by art,asize
) where art in ('9221-014', '9221-019');


select kol v_kol_ob,cena2 v_sum_ob from temp_reports where id_shop = '2222' and prim = 'KAA_SEBEST_XXX' and art = 'Обувь';
select kol v_kol_sop,cena2 v_sum_sop from temp_reports where id_shop = '2222' and prim = 'KAA_SEBEST_XXX' and art = 'Сопутствующие товары';