select * from kaa_sebest_sap_ost where bwkey = 'W401' and matnr in ( 'F17382255-F370');

select * from s_shop where shopid = '2221';
select * from st_shop_access where shopid = '3814';

select * from d_prixod1 a where a.id_shop = '3810' and a.idop = '50';

select * from E_OBOROT1@s3814 order by id desc;
select * from E_OBOROT1@sdc order by id desc;

select * from d_prixod1@s3814 where idop = 50;
select * from d_rasxod1@sdc where idop = 51;

select * from d_rasxod1 where id_shop = '3814' and idop = 51;

--insert into temp_reports (id_shop,art,kol,cena2,prim)
select '3814',ggroup,art,sum(ostb) ostb,sum(sostb1) sostb1,'KAA_SEBEST_XXX'
from (SELECT 'Оборотка' PLACE,case when GRUP = 'Обувь' then grup else 'Сопутствующие товары' end AS GGROUP, b.art, SUM(B.OSTB) AS OSTB,SUM(B.SOSTB1) AS SOSTB1
      FROM (
        SELECT A.*,B.VTEXT,B.SEASON,B.MANUFACTOR,GRUP1(A.ART,1) AS GRUP  FROM E_OBOROT_V@S3814 A LEFT JOIN S_ART_V@S3814 B ON A.ART=B.ART 
                WHERE A.ID='27'
        ) B  GROUP BY GRUP, b.art
        union all
        select 'Расход','Обувь', art, -sum(kol), -sum(kol*cena1)
        from d_rasxod2@S3814 where id = '79'
        group by art
        union all
        select 'Расход','Сопутствующие товары', art, -sum(kol), -sum(kol*cena1)
        from d_rasxod2@S3814 where id = '80'
        group by art
      )
      
where ggroup = 'Сопутствующие товары'
group by ggroup, art
having sum(ostb) != 0 or sum(sostb1) != 0
;



select * from E_OBOROT_V@S3814 where ID='27' and art = '9С3421 27-29';
select * from E_OBOROT_V@scd where ID='27' and art = '9С3421 27-29';
select * from d_rasxod2@s3814 where id = 80 and art = '9С3421 27-29';
select * from d_rasxod2@sdc where id = 80 and art = '9С3421 27-29';

select * from s_all_mat where art = '9С3421 27-29';

SELECT 'Оборотка' PLACE,case when GRUP = 'Обувь' then grup else 'Сопутствующие товары' end AS GGROUP,  b.art, SUM(B.OSTB) AS OSTB,SUM(B.SOSTB1) AS SOSTB1
FROM (
SELECT A.*,B.VTEXT,B.SEASON,B.MANUFACTOR,GRUP1(A.ART,1) AS GRUP  FROM E_OBOROT_V@S3814 A LEFT JOIN S_ART_V@S3814 B ON A.ART=B.ART 
        WHERE A.ID='27'
) B  GROUP BY GRUP, b.art;


select '3814',ggroup,sum(ostb) ostb,sum(sostb1) sostb1,'KAA_SEBEST_XXX', art
from (SELECT 'Оборотка' PLACE,case when GRUP = 'Обувь' then grup else 'Сопутствующие товары' end AS GGROUP, SUM(B.OSTB) AS OSTB,
    SUM(B.SOSTB1) AS SOSTB1, art FROM (
        SELECT A.*,B.VTEXT,B.SEASON,B.MANUFACTOR,GRUP1(A.ART,1) AS GRUP  FROM E_OBOROT_V@S3814 A LEFT JOIN S_ART_V@S3814 B ON A.ART=B.ART 
            WHERE A.ID='27'
        ) B GROUP BY GRUP, art
        union all
        select 'Расход','Обувь', -sum(kol), -sum(kol*cena1), art
        from d_rasxod2@S3814 where id = '79' group by art
        union all
        select 'Расход','Сопутствующие товары', -sum(kol), -sum(kol*cena1), art
        from d_rasxod2@S3814 where id = '80' group by art)
group by ggroup, art;


select * from E_OBOROT_V@sdc order by rel desc;
select * from E_OBOROT1@sdc where dcid = '28W1' order by rel desc;
select * from d_rasxod1@sdc where dcid = '28W1' order by id desc; 

select '28W1',ggroup, art, sum(ostb) ostb,sum(sostb1) sostb1,'KAA_SEBEST_XXX'
from (SELECT 'Оборотка' PLACE,case when GRUP = 'Обувь' then grup else 'Сопутствующие товары' end AS GGROUP, art, SUM(B.OSTB) AS OSTB,SUM(B.SOSTB1) AS SOSTB1
      FROM (
        SELECT A.*,B.VTEXT,B.SEASON,B.MANUFACTOR,GRUP1(A.ART,1) AS GRUP  FROM E_OBOROT_V@sdc A LEFT JOIN S_ART_V@sdc B ON A.ART=B.ART 
                WHERE A.REL='329'
        ) B  GROUP BY GRUP, art
        union all
        select 'Расход','Обувь', art, -sum(kol), -sum(kol*cena1)
        from d_rasxod2@sdc where rel = '2961'
        group by art
        union all
        select 'Расход','Сопутствующие товары', art, -sum(kol), -sum(kol*cena1)
        from d_rasxod2@sdc where rel = '2962'
        group by art
      )
group by ggroup, art
having sum(ostb) != 0
order by art, ostb
;


select distinct id,rel,postno,art
from (
  select id,rel,postno,art,partno,sum(kol) kol,sum(cena1) cena1, sum(cnt) cnt,sum(fim)
  from (
    select 'pr' tip,a.id,a.rel,a.postno,b.art,b.partno,b.kol,b.cena1,1 cnt,b.kol - b.koli fim
    from d_prixod1 a
    inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop and b.scan = ' '
    inner join temp_reports t on t.id_shop = a.id_shop and t.prim = '3814'||'MOVEEQ' and t.id = a.id
    where a.id_shop = '3814' and a.bit_close = 'T'
    union all
    select 'trans' tip,c.id,c.rel,a.postno,b.art,b.partno,-b.kol,-b.price,-1 cnt,0 fim
    from d_sap_odgruz1 a
    inner join d_sap_odgruz2 b on a.postno = b.postno and b.scan = ' '
    inner join d_prixod1 c on c.postno = a.postno and c.id_shop = '3814' and c.bit_close = 'T'
    inner join temp_reports t on t.id_shop = c.id_shop and t.prim = '3814'||'MOVEEQ' and t.id = c.id
    where a.pr_ud = 0
  )
  group by id,rel,art,partno,postno
  having sum(kol) = 0 and sum(fim) = 0 and sum(cena1) != 0
);


select matnr, count(*) from ( 
  select a.* from kaa_sebest_sap_ost a
  left join kaa_sebest_errors b on a.bwkey = b.id_shop and a.matnr = b.matnr
  where b.id_shop = '3814' and b.err = 'SAP_OST_EQ_ERROR'
  and a.dated = '20200501' and a.kol != b.kol
  order by a.matnr
) group by matnr
having count(*) > 1
;

insert into kaa_sebest_sap_ost
select a.matnr, a.bwkey, a.charg, -a.verpr, -a.kol, -a.salk3, a.dated from kaa_sebest_sap_ost a
left join kaa_sebest_errors b on a.bwkey = b.id_shop and a.matnr = b.matnr
where b.id_shop = '3814' and b.err = 'SAP_OST_EQ_ERROR'
and a.dated = '20200501'
order by a.matnr;

--insert into kaa_sebest_sap_ost 
select * from kaa_sebest_sap_ost a where a.dated = '20200501' and a.bwkey = '3814' and matnr = 'F18301901-F380';
select * from kaa_sebest_errors a where a.id_shop = '3814' and matnr = 'F18301901-F380';



select matnr from kaa_sebest_errors where id_shop = '3814' and err = 'SAP_OST_EQ_ERROR';


select shop,err,matnr,art,asize,sum(kol),sum(cena1)
from (
  select '1', '3814' shop,'SAP_OST_EQ_ERROR' err,q.matnr,p.art,p.asize,sum(kol) kol,sum(cena1) cena1,0 rxkol
  from (select y.art,nvl(y.asize,0) asize,sum(to_number(kol)) kol,sum(to_number(salk3)) cena1
        from kaa_sebest_sap_ost x
        inner join s_all_mat y on x.matnr = y.matnr
        where x.bwkey = '3814' and x.dated = '20200501'
        group by y.art,nvl(y.asize,0)
        union all

        select art,asize,-sum(kol),-sum(kol*cena1)
        from d_prixod2
        where id_shop = '3814' and id in (select id from d_prixod1 where id_shop = '3814' and idop = '50' and to_char(dated,'yyyyMMdd') = '20200501')
        group by art,asize
        ) p
  inner join s_all_mat q on p.art = q.art and p.asize = nvl(q.asize,0)
  group by q.matnr,p.art,p.asize
  having sum(kol) != 0 or sum(cena1) != 0
  
  union all
  
  select '2', '3814','SAP_OST_EQ_ERROR',matnr,art,asize,-sum(kol),-sum(cena1),-1 rxkol
  from (
    select e.matnr,b.art,b.asize,b.scan,b.partno,b.kol,b.cena1
    from d_rasxod1 a
    inner join d_rasxod2 b on a.id = b.id and a.id_shop = b.id_shop
    inner join d_sap_odgruz1 c on c.postno_lifex = to_char(a.id) and c.pr_ud = 0 and c.skladid = a.id_shop
    inner join d_prixod1 d on d.postno = c.postno and d.id_shop = a.kkl and to_char(d.dated,'yyyymmdd') >= '20200501'
    inner join s_all_mat e on e.art = b.art and nvl(e.asize,0) = b.asize and e.trademark_sat != '003'
    where a.bit_close = 'T' and a.idop in ('38','38','38','38','53') and a.id_shop = '3814'
    and to_char(a.dated,'yyyymmdd') < '20200501'
  )
  group by matnr,art,asize
)
group by shop,err,matnr,art,asize
having sum(kol) != 0 or (sum(cena1) != 0 and sum(rxkol) = 0)
;


select * from d_prixod1 a 
inner join d_prixod2 b on a.id = b.id and a.id_shop = b.id_shop 
where a.id_shop = '3814' and a.idop = '50' and text = 'Сопутка' and art in ('9С3421 27-29','2с4103 р.29','2с4103 р.25') order by art, partno;

select * from E_OSTTEK_OWNER_V@sdc where dcid = '28W1' and rel is null;
