select bwkey, count(*) from WR_TABLE_EXPORT_PARAM_1_TEST where (bwkey like '37%' or bwkey like 'W'||'37%') group by bwkey order by bwkey;
select bwkey, count(*) from KAA_SEBEST_SAP_OST where dated = 20200501 and (bwkey like '37%' or bwkey like 'W'||'37%') group by bwkey order by bwkey;
select bwkey, count(*) from KAA_SEBEST_SAP_EQ where (bwkey like '37%' or bwkey like 'W'||'37%') group by bwkey order by bwkey;

select * from st_shop where shopid = '35D0';
select * from st_shop_access where shopid = '3523';
select * from st_shop_access where shopid like '35%';
select shopid, max(bit_access) bit_access from st_shop_access where shopid like '23%' group by shopid order by shopid;

select shopid, max(bit_access) bit_access from st_shop_access where shopid in (select distinct id_shop from kaa_sebest_errors where (id_shop like '38%' )) group by shopid;

select * from pos_sale1 where id_shop = '2709' order by sale_date desc;

SET serveroutput ON
BEGIN 
KAA_SEBEST_EQ.PR_RX_CREATE('3523','01.05.2020');
commit;
END;

SET serveroutput ON
BEGIN 
KAA_SEBEST_EQ.PR_RX_CREATE_DC('39S4','01.05.2020');
commit;
END;


select shopid 
from st_shop 
where substr(shopid,1,2) in ('35')
  and bit_open = 'T' and org_kod = 'SHP'
--  and shopid = '3510'
;

SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor cur is 
  select shopid bit_access from st_shop_access 
  where shopid in (select distinct id_shop from kaa_sebest_errors where (id_shop like '35%' )) group by shopid
  ;
curRow cur%rowtype;
p_shopid varchar2(20 CHAR);
BEGIN
 
open cur;
  loop  
       FETCH cur INTO p_shopid;
       EXIT WHEN cur%NOTFOUND;  
          begin
          
					begin
						DBMS_SCHEDULER.drop_job( 'kaa_j_pr_rx_seb_'||p_shopid);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'kaa_j_pr_rx_seb_'||p_shopid,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
BEGIN 
KAA_SEBEST_EQ.PR_RX_CREATE(']'||p_shopid||q'[','01.05.2020');
commit;
END;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('kaa_j_pr_rx_seb_'||p_shopid); 
					
          commit;
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
  
END ;


---
SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
ddl_ins1 varchar(1000);
cursor cur is 
  select shopid 
  from st_shop 
  where substr(shopid,1,2) in ('44')
    and bit_open = 'T' and org_kod = 'SHP'
--    and shopid = '3510'
  ;
curRow cur%rowtype;
p_shopid varchar2(20 CHAR);
BEGIN
 
open cur;
  loop  
       FETCH cur INTO p_shopid;
       EXIT WHEN cur%NOTFOUND;  
          begin
          
					begin
						DBMS_SCHEDULER.drop_job( 'kaa_j_pr_rx_seb_'||p_shopid);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'kaa_j_pr_rx_seb_'||p_shopid,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
BEGIN 
KAA_SEBEST_EQ.PR_RX_CREATE(']'||p_shopid||q'[','01.05.2020');
commit;
END;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('kaa_j_pr_rx_seb_'||p_shopid); 
					
          commit;
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            rollback;
          end;
  end loop;
  
END ;