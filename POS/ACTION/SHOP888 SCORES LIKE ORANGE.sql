select * from d_kassa_cur;
select * from d_kassa_disc_cur;

select * from d_kassa_disc_cur
where posnr in (
  select posnr
  from d_kassa_disc_cur a
  inner join pos_skidki1 b on a.discount_type_id = b.docid
  where trunc (sysdate) between b.dates and b.datee
  and b.like_orange = 'T' and a.kass_id = '1#lacit010-m'
) and  discount_type_id = 'A000000001'
;




select coalesce(max(docid), ' ') --into 'B200000879'
  from
    (
      select docid
      from
        (
          select docid, ROW_NUMBER() OVER (ORDER BY priority DESC) AS rn
          from pos_skidki1
          where disc_type_id = 'ZK38' AND TRUNC(SYSDATE) BETWEEN TRUNC(DATES) AND TRUNC(DATEE)
        )
    where rn = 1
    );


select 
get_system_val('DK_SCORES_WITH_DISCOUNT_PRICE') sys_dk_with_disc,
get_dk_sc_sys_str( 'DOCID') docid,
get_dk_sc_sys_int( 'SCORES_ROUND')  round,
get_dk_orange_discount('0000000087995') DK_ORANGE_DISCOUNT
from dual;
select NUMCONF from config; -- 2

select * from d_kassa_cur;
select * from d_kassa_disc_cur;


select get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP') from dual;

select 

sum(kol * ((case when br.scan is null then price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                            where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001')
                                            ,0) else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end)/100*5)) a,
sum(case when (ACTION_PRICELIST(b.art, case when 'F' = 'T' then get_order_date(b.enet_posnr) else sysdate end, b.procent) = 'T' or zk38.ART IS NOT NULL or orange.posnr IS NOT NULL) then GREATEST(
                                case when b.procent != 0 -- для некондиции и брака РФ берется половина от цены на 90
                                  then ( cena2s(sysdate,b.art,0,1,/*b.procent*/0,'00',/*noncond_proc*/0) - cena2p(sysdate,b.art,0,1)*0.7 ) * kol
                                  else (b.PRICE_SUM + nvl(a.DISCOUNT_SUM, 0) - b.PRICE * 0.7)*kol
                                end, 0/*0*/) else 999999999 end) d,
sum(case when (ACTION_PRICELIST(b.art,  case when 'F' = 'T' then get_order_date(b.enet_posnr) else sysdate end, b.procent) = 'T' or zk38.ART IS NOT NULL  or orange.posnr IS NOT NULL)/* and is_child_prod(p_ART) = 'F'*/ -- проверка на оранжевый ценник -- и не детская продукция
                              and (baggins(b.art) = 'T' or substr(c.prodh, 0, 4) = '0001' or c.prodh = '000200586308' or orange.posnr IS NOT NULL) then 1 else 0 end) e,


sum(round_dig(LEAST(case when (ACTION_PRICELIST(b.art, case when 'F' = 'T' then get_order_date(b.enet_posnr) else sysdate end, b.procent) = 'T' or zk38.ART IS NOT NULL or orange.posnr IS NOT NULL) then GREATEST(
                                case when b.procent != 0 -- для некондиции и брака РФ берется половина от цены на 90
                                --then (cena2p(sysdate,b.art,0,1)*0.7 - (cena2p(sysdate,b.art,0,1) - cena2s(sysdate,b.art,0,1,/*b.procent*/0,'00',/*noncond_proc*/0)) ) * kol
                                  then ( cena2s(sysdate,b.art,0,1,/*b.procent*/0,'00',/*noncond_proc*/0) - cena2p(sysdate,b.art,0,1)*0.7 ) * kol
                                  else (b.PRICE_SUM + nvl(a.DISCOUNT_SUM, 0) - b.PRICE * 0.7)*kol
                                end, 0) else 999999999 end/*максимум с ограничениями*/,(
                    case when (ACTION_PRICELIST(b.art,  case when 'F' = 'T' then get_order_date(b.enet_posnr) else sysdate end, b.procent) = 'T' or zk38.ART IS NOT NULL or orange.posnr IS NOT NULL)/* and is_child_prod(p_ART) = 'F'*/ -- проверка на оранжевый ценник -- и не детская продукция
                              and (baggins(b.art) = 'T' or substr(c.prodh, 0, 4) = '0001' or orange.posnr IS NOT NULL) -- проверка на сумку или обувь
                    then
                          
                          kol * ((case when br.scan is null then price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                            where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001')
                                            ,0) else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end)/100*5)
                    when (action_pricelist(b.art, case when 'F' = 'T' then get_order_date(b.enet_posnr) else sysdate end) = 'F' AND zk38.ART IS NULL  AND orange.posnr IS NULL) and
                              IS_ORDER_WITH_DISC(b.enet_posnr,discount_type_id) = 'F'
                    then
                        case when  ((get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null) /*or is_child_prod(p_ART) = 'T'*/) -- проверка на сопутку
                          then kol * ((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001'),0))/100*
                                                                                                                  get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP'))
                        when is_sport_prod(b.art) = 'T' and is_child_prod(b.art) = 'T'
                          then kol * ((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001'),0))/100*
                                                                                                                  get_dk_sc_sys_int('USE_SCR', 'OB_CHILD'))
                        when is_sport_prod(b.art) = 'T'
                          then kol * ((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001'),0))/100*
                                                                                                                  get_dk_sc_sys_int('USE_SCR', 'OB_SPORT'))
                        when is_belwest_prod(b.art) = 'T' and 2 = 1
                          then kol * ((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001'),0))/100*
                                                                                                                  get_dk_sc_sys_int('USE_SCR', 'OB_BELWEST'))
                        when is_child_prod(b.art) = 'T'
                          then kol * ((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001'),0))/100*
                                                                                                                  get_dk_sc_sys_int('USE_SCR', 'OB_CHILD'))
                        else kol * ((case when br.scan is null then price else cena2s(sysdate,b.art,0,1,b.procent,'00',noncond_proc) end-nvl((select sum(discount_sum) from d_kassa_disc_cur t1
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!='A000000001'),0))/100*
                                                                                                                  get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB'))
                        end
                    else 0
                    end)), 0.01, -1 /*0*/)
)  scores, sum(orange.posnr)
from d_kassa_cur b
left join pos_skidki2 zk38 ON zk38.docid = 'B200000879' AND b.art = zk38.art
left join d_kassa_disc_cur a on a.posnr = b.posnr and a.kass_id = b.kass_id
left join pos_skidki1 s1 ON s1.DOCID = a.DISCOUNT_TYPE_ID AND s1.WITHOUT_DK = 'F'
left join s_art c on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id IN ('ZK23','ZK01','ZK02','ZK10','ZK25','ZK33','ZK34','ZK19','ZK35','ZK37') or (x.disc_type_id in ('ZK29') and 2 in (1,2))) AND x.docid not in ('B200000856', 'B200000855')) d on d.docid=discount_type_id
left join pos_brak br on br.scan = nvl(b.scan,' ') and br.scan != ' '
left join (
  select distinct posnr
  from d_kassa_disc_cur a
  inner join pos_skidki1 b on a.discount_type_id = b.docid
  where trunc (sysdate) between b.dates and b.datee
  and b.like_orange = 'T' and a.kass_id = '1#lacit010-m'
) orange on a.posnr = orange.posnr
where (s1.DOCID IS NOT NULL OR a.DISCOUNT_TYPE_ID IS NULL) AND (discount_type_id = 'A000000001'
or (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and not (2 = 2 and b.posnr in (select posnr
                                                                                                                      from d_kassa_disc_cur a
                                                                                                                      inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                      where trunc (sysdate) between b.dates and b.datee
                                                                                                                      and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                              )))
)
and b.kass_id = '1#lacit010-m';