select * from pos_skidki1 order by docid;
select * from pos_skidki2 where docid = '0200001903';

select * from st_dk where dk_level = 1;

-- B200000886
select max(priority) from pos_skidki1;

-- 8С-61СП Р3/40/ЧЕРН
-- 8С-64СП Р4/40/БРОН

select * from e_osttek where art in (select art from pos_skidki2 where docid = '0200001903') and kol > 0;
select * from e_osttek where art not in (select art from pos_skidki2 where docid = '0200001903') and kol > 0;

select * from d_kassa_disc_cur;
select * from d_kassa_cur;
delete from d_kassa_disc_cur;

select max(zk10),max(zk25),max(zk29),max(zk33),max(zk34),max(zk19),max(zk37),max(zk38),max(zk39),max(zk70)
from (select
      case when disc_type_id = 'ZK10' then docid else ' ' end zk10,
      case when disc_type_id = 'ZK25' then docid else ' ' end zk25,
      case when disc_type_id = 'ZK29' then docid else ' ' end zk29,
      case when disc_type_id = 'ZK33' then docid else ' ' end zk33,
      case when disc_type_id = 'ZK34' then docid else ' ' end zk34,
      case when disc_type_id = 'ZK19' then docid else ' ' end zk19,
      case when disc_type_id = 'ZK37' then docid else ' ' end zk37,
      case when disc_type_id = 'ZK38' then docid else ' ' end zk38,
      case when disc_type_id = 'ZK39' then docid else ' ' end zk39,
      case when disc_type_id = 'ZK70' then docid else ' ' end zk70
      from (
            select disc_type_id,max(docid) docid
            from pos_skidki1
            where disc_type_id in ('ZK10','ZK25','ZK29','ZK19','ZK37','ZK38','ZK39') and trunc(sysdate) >= trunc(dates)
            and trunc(sysdate) <= trunc(datee)
            group by disc_type_id
            
            union all
            select disc_type_id,max(docid) docid
            from pos_skidki1 a
            inner join d_kassa_cur b on b.kass_id = '1#lacit010-m' and b.bit_enet = 'T'
            inner join d_inet_book2 c on b.enet_posnr = c.posnr and b.art = c.art and b.asize = c.asize and nvl(b.scan,' ') = c.scan
            inner join d_inet_book1 d on c.posnr = d.posnr and c.serv_ip = d.serv_ip
            where disc_type_id in ('ZK33','ZK34') and trunc(d.dated) >= trunc(dates)
            and trunc(d.dated) <= trunc(datee)
            group by disc_type_id
            
            union all
            select disc_type_id,max(docid) docid
            from d_kassa_disc_cur a
            JOIN pos_skidki1 b ON a.DISCOUNT_TYPE_ID = b.docid
            where a.kass_id = '1#lacit010-m' and b.disc_type_id in ('ZK70') and trunc(sysdate) between trunc(b.dates) and trunc(b.datee)
            group by disc_type_id
          ));

select * from tem_action_calculate;

delete from tem_action_calculate;

--insert into tem_action_calculate (POSNR,ART,BIT_HAND,PROCENT,KOL,TMC_TYPE,COLOR,PRICE,PRICE_SK,ZIM,LET,UTEP,VSESEZ,ZK10,ZK25,ZK29,PRICE_SUM,BIT_ENET,BIT_BOOK,ZK33,ZK34,ZK19,ZK37,RNUM)   
select x.*,rownum rnum from (select a.posnr,a.art,a.bit_hand,a.procent,a.kol,
case when baggins(a.art) = 'T' then 'BAG' when (scan = ' ' or scan is null) then 'SOP' else 'OB' end tmc_type,
case when action_pricelist(a.art,sysdate) = 'T' then 'T' else 'F' end color,
cena2p(sysdate,a.art,0,1,a.procent) price,
cena2s(sysdate,a.art,0,1,a.procent) price_sk,
case when upper(b.season) like '%ЗИМ%' then 1 else 0 end zim,
case when upper(b.season) like '%ЛЕТ%' then 1 else 0 end let,
case when upper(b.season) like '%УТЕП%' then 1 else 0 end utep,
case when upper(b.season) like '%ВСЕСЕЗ%' then 1 else 0 end vsesez,
nvl(c.zk10,' '),nvl(c.zk25,' '),nvl(c.zk29,' '),
a.price_sum,a.bit_enet,a.bit_book,
nvl(c.zk33,' '),nvl(c.zk34,' '),nvl(c.zk19,' '),nvl(c.zk37,' ')
from d_kassa_cur a
inner join s_art b on a.art = b.art
left join (select posnr,
            max(case when disc_type_id = 'ZK10' then docid else ' ' end) zk10,
            max(case when disc_type_id = 'ZK25' then docid else ' ' end) zk25,
            max(case when disc_type_id = 'ZK29' then docid else ' ' end) zk29,
            max(case when disc_type_id = 'ZK33' then docid else ' ' end) zk33,
            max(case when disc_type_id = 'ZK34' then docid else ' ' end) zk34,
            max(case when disc_type_id = 'ZK19' then docid else ' ' end) zk19,
            max(case when disc_type_id = 'ZK37' then docid else ' ' end) zk37
            from (select posnr,y.disc_type_id,max(y.docid) docid
                  from d_kassa_disc_cur x
                  inner join pos_skidki1 y on x.discount_type_id = y.docid
                  where x.kass_id = '1#lacit010-m' and y.disc_type_id in ('ZK10','ZK25','ZK29','ZK33','ZK34','ZK19','ZK37') and PRIORITY > 5
                  group by posnr,y.disc_type_id)
            group by posnr) c on c.posnr = a.posnr
where a.kass_id = '1#lacit010-m'
order by color,a.kol desc,cena2s(sysdate,a.art,0,1,a.procent),a.posnr desc) x;



select count(*) reccount,

sum(case when bit_hand = 'T' then 1 else 0 end) handcount,
sum(case when bit_enet = 'T' then 1 else 0 end) enetcount,
sum(case when bit_book = 'T' then 1 else 0 end) bookcount,
sum(case when tmc_type = 'OB' then 1 else 0 end) obcount,
sum(case when tmc_type = 'BAG' then kol else 0 end) bagcount,
sum(case when tmc_type = 'SOP' then kol else 0 end) sopcount,
sum(case when color = 'T' then kol else 0 end) orangepcount,
sum(e.kolnotcur) kolnotcur,
sum(case when kol < 2 and (tmc_type in ('BAG','OB') or d.prodh in ('000400185001')) then kol else 0 end) curactpcount,
sum(case when color = 'F' and ((tmc_type = 'OB') or (tmc_type = 'BAG' and upper(d.country_prod) like '%ИТАЛИЯ%' and kol < 4)) then kol else 0 end) curactpwhitecount,
sum(case when d.prodh in ('000400185001') then kol else 0 end) jeanscount,
sum(case when d.prodh in ('000400185002') then kol else 0 end) jacketcount,
sum(case when tmc_type = 'BAG' and substr(cena2s(sysdate,a.art,0,1),-2) != '99' then a.kol else 0 end) zk19_bag_count,
sum(case when a.procent = 40 then kol else 0 end) nek40count,
sum(case when a.procent > 0 then kol else 0 end) nekcount,
sum(a.kol) kol,
sum(case when color = 'F' then a.kol else 0 end) whitekol,
sum(case when color = 'T' then 0 else zim end) zimcountw,
sum(case when color = 'T' then 0 else let end) letcountw,
sum(case when color = 'T' then 0 else utep end) utepcountw,
sum(case when color = 'T' then 0 else vsesez end) vsesezcountw,
sum(case when color = 'F' then 0 else zim end) zimcounto,
sum(case when color = 'F' then 0 else let end) letcounto,
sum(case when color = 'F' then 0 else utep end) utepcounto,
sum(case when color = 'F' then 0 else vsesez end) vsesezcounto,
sum(case when zk10 = ' ' then 0 else 1 end) zk10count,
sum(case when zk25 = ' ' then 0 else 1 end) zk25count,
sum(case when zk29 = ' ' then 0 else 1 end) zk29count,
sum(case when zk33 = ' ' then 0 else 1 end) zk33count,
sum(case when zk34 = ' ' then 0 else 1 end) zk34count,
sum(case when zk19 = ' ' then 0 else 1 end) zk19count,
sum(case when zk37 = ' ' then 0 else 1 end) zk37count,
--        sum((case when utep = 1 and c.kolch = 1 and a.color = 'F' and upper(d.assort) = 'САПОГИ' then b.price_sum else price_sk end)*kol) chsum
sum((case when a.curact = 'T' then a.price_sum else price_sk*kol end)) chsum,
sum(case when tmc_type = 'OB' and (substr(cena2s(sysdate,a.art,0,1),-2) = '99' or s2.art is not null) then kol else 0 end) stockcount,
sum(case when tmc_type = 'BAG' and (substr(cena2s(sysdate,a.art,0,1),-2) = '99' or s2.art is not null) then kol else 0 end) stockbagcount,
sum(case when a.art in (select art from pos_skidki2 where docid in ('B200000336','B200000300','B200000287','B200000381','B200000289')) then kol else 0 end) lastpcount,
sum(case when a.art in (select art from pos_skidki2 where docid in ('B200000555')) then a.kol else 0 end) actiondoccount,
sum(case when a.kol > 1 then a.kol else 0 end) twocount,
sum(case when is_child_prod(a.art) = 'T' and tmc_type = 'OB' then a.kol else 0 end) childcount,
sum(case when is_sport_prod(a.art) = 'T' and tmc_type = 'OB' then a.kol else 0 end) sportcount,
sum(case when a.art in (select art from pos_skidki2 where docid in ('B200000640','B200000641','B200000642','B200000643')) then a.kol else 0 end) lottocount,
sum(case when d.mtart not in ('ZHW3','ZROH') --and d.manufactor != 'СООО БЕЛВЕСТ'
         and d.groupmw in ('Для школьников мальч','Дошкольные для мальч','Мальчиковые','Малодетские','Дошкольные','Мужские')
         then a.kol else 0 end) men_count,
sum(case when d.mtart not in ('ZHW3','ZROH') and d.groupmw = 'Женские' then a.kol else 0 end) zhen_count,
sum(case when a.art in (select art from pos_skidki2 where docid = 'B200000784') then a.kol else 0 end) beregBelt_count,
sum(case when a.art in (select art from pos_skidki2 where docid = 'B200000921') or 
              ('B200000921' = 'B200000921' and a.art in (select art from s_art where prodh like '00020018%')) then a.kol else 0 end) zk10art_count,
sum(case when tmc_type = 'OB' and substr(cena2s(sysdate,a.art,0,1),-2) != '99' then a.kol else 0 end) zk19_shoes_count,
sum(case when a.art in (select art from pos_skidki2 where docid in ('B200000838','B200000839')) then a.kol else 0 end) exc_stockcount
from tem_action_calculate a
inner join (select posnr,price_sum
            from d_kassa_cur
            where kass_id = '1#lacit010-m') b on a.posnr = b.posnr
inner join (select sum(kol) kolch from tem_action_calculate) c on 1 = 1
inner join (select art,assort,country_prod,prodh,manufactor,groupmw,mtart from s_art) d on d.art = a.art
inner join (select count(*) kolnotcur
              from d_kassa_disc_cur
              where DISCOUNT_TYPE_ID in ('B200000394','B200000395')
                and kass_id = '1#lacit010-m') e on 1 = 1
left join pos_skidki2 s2 on s2.docid = '0200001903' and s2.art = a.art
;


SELECT z.POSNR, 20, prices / KOL * (80 / KOL / 100), PRICES
--INTO v_zk10_posnr, v_zk10_discount, v_zk10_discount_sum, v_zk10_price_sum
FROM (
    SELECT cena2p(sysdate,a.art,0,1,a.procent) price, cena2s(sysdate,a.art,0,1,a.procent,'00',a.noncond_proc) * KOL prices, a.POSNR, a.KOL, ROW_NUMBER() over (ORDER BY cena2s(sysdate,a.art,0,1,a.procent,'00',a.noncond_proc), KOL) AS nr
    FROM D_KASSA_CUR a
    WHERE a.KASS_ID = '1#lacit010-m'
) z
--WHERE z.nr = 1
;

SELECT DISTINCT a.POSNR FROM D_KASSA_CUR a WHERE a.KASS_ID = '1#lacit010-m' AND a.POSNR != 1;



select posnr,cast(price as number(18,2)) price,discount,
case when rownum = 1 then price - case when round_dig(prices*kol - (kol*(prices*discount/100)),0.01,0) = 0 then 0.01 else round_dig(prices*kol - (kol*(prices*discount/100)),0.01,0) end/kol else 0 end discount_r,
--round_dig(prices*discount/100,0.01,0) + (price - prices) discount_r,
case when round_dig(prices*kol - (kol*(prices*discount/100)),0.01,0) = 0 then 0.01 else round_dig(prices*kol - (kol*(prices*discount/100)),0.01,0) end price_sum
from (select c.posnr,c.kol,cena2p(sysdate,c.art,0,1,c.procent) price,cena2s(sysdate,c.art,0,1,c.procent) prices,
      case when 'B200000886' = 'B200000886' then 20 else case when rownum = 1 then 20 else 0 end end discount,
      c.discount_r,c.price_sum
      from (select a.*
            from d_kassa_cur a
            inner join (select posnr,rownum rnum
                        from (select posnr
                              from (select case when d.mtart not in ('ZROH','ZHW3') then 'T' else 'F' end ob_ch,x.*,sum(kol) over (order by x.rnum) kols
                                    from (select t.*,row_number() over(partition by t.tmc_type order by zim,t.price_sk,t.posnr) as nr
                                            from tem_action_calculate t) x
                                          inner join s_art d on x.art = d.art
                                    --where kol <= 2 and (tmc_type = 'BAG' or tmc_type = 'OB' or d.prodh in ('000200586308','000400185001'))
                                    order by x.rnum)
                              --where kols <= cur.kol
                              order by price_sk,posnr desc)) f on f.posnr = a.posnr
            where a.kass_id = '1#lacit010-m'
--                          and f.rnum <= v_recc
            order by rnum
            ) c
            /*left join (select kass_id,posnr,sum(discount_sum) discount_sum
                        from d_kassa_disc_cur
                      where discount_type_id = 'A000000001'
                      group by kass_id,posnr) d on c.kass_id = d.kass_id and c.posnr = d.posnr*/
      )
      ;
      
SELECT z.POSNR zk10_posnr, 20 /* / KOL*/ zk10_discount,prices, kol, prices / KOL * (20 / 100) zk10_discount_sum, PRICES zk10_price_sum
FROM (
    SELECT cena2p(sysdate,a.art,0,1,a.procent) price, cena2s(sysdate,a.art,0,1,a.procent,'00',a.noncond_proc) * KOL prices, a.POSNR, a.KOL, ROW_NUMBER() over (ORDER BY cena2s(sysdate,a.art,0,1,a.procent,'00',a.noncond_proc), KOL) AS nr
    FROM D_KASSA_CUR a
    WHERE a.KASS_ID = '1#lacit010-m'      
) z;      
      
select
cena2p(sysdate,art,0,1,procent) PRICE,
8.02 / KOL - ROUND((1.604 * KOL), 0) PRICE_SUM,
(cena2p(sysdate,art,0,1,procent) - cena2s(sysdate,art,0,1,procent,'00',noncond_proc) + 1.604) / cena2p(sysdate,art,0,1,procent)/kol * 100 DISCOUNT,
cena2p(sysdate,art,0,1,procent) - cena2s(sysdate,art,0,1,procent,'00',noncond_proc) + 1.604 DISCOUNT_R
from D_KASSA_CUR where posnr = 1 and KASS_ID = '1#lacit010-m';


select x.posnr
from d_kassa_cur x
left join d_kassa_disc_cur z on x.posnr = z.posnr and z.discount_type_id = 'A000000001' and z.kass_id = x.kass_id
where x.kass_id = '1#lacit010-m' and z.posnr is null
;


select nvl(max(kol),0)
          from (select *
                from tem_action_calculate
                where curact = 'T'
                order by price_sk,posnr desc)
          where rownum = 1