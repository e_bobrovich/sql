select * from pos_brak order by date_s desc;

select * from e_osttek where scan = '00000000001103160723';
select * from s_label where scan = '00000000001103160723';

select * from D_CENNIK2_V where rel = 87;
select * from D_CENNIK1 where rel = 87;
select * from D_CENNIK2 where rel = 87;

  SELECT 
  B."REL",B."NPP",B."ART",B."ASSORT",B."STRR",B."SCAN",B."NEI",B."CENA2",B."CENA3",B."DATE_S",
  B."PC_S",B."KOL",B."OST",B."PORTION",B."EAN",B."SKIDKA",B."NDOCP",B."DATEP",B."NDOCS",B."DATES",
  B."BIT_SELE",B."PROCENT",B."TIP_SKIDKA",B."TIP_R",B."CENA2W",B."CENA2S",B."ART_REAL",B."VID_LOAD",B."CENA_PREV_DAY",
  CASE  WHEN TIP_SKIDKA=1 THEN 'БЕЛЫЙ' WHEN TIP_SKIDKA=2 THEN 'ОРАНЖЕВЫЙ' ELSE ' ' END AS TIP_SKIDKA_N,
  CASE  WHEN TIP_R=0 THEN ' ' WHEN TIP_R=1 THEN 'РАССРОЧКА' ELSE ' ' END AS TIP_R_N 
  FROM D_CENNIK2 B
;

--SCAN_CENNIK();
--SCAN_CENNIK_UPD(V_REL);

--BEGIN
-- DELETE TMP_ART  WHERE SESSIONID='443608792';
--INSERT INTO TMP_ART (SESSIONID,ART,PROCENT,VID_LOAD) 
 SELECT '443608792',a.ART,
 case when a.procent > 0 then a.procent else b.PROCENT end procent,
 --a.PROCENT,
 'БРАК' 
 FROM POS_BRAK a
 inner join (select art,asize,scan,procent
	      from e_osttek
	      where scan != ' '
	      group by art,asize,scan,procent
	      having sum(kol) > 0) b on a.art = b.art and a.asize = b.asize and a.scan = b.scan
 inner join (select distinct art
		from pos_brak
		where scan != ' '
		and trunc(dated) between to_date('22.01.2020','dd.mm.yyyy') and to_date('22.03.2020','dd.mm.yyyy')
		union all
		select distinct art
		from kart_v
		where scan != ' '
		and trunc(dated) between to_date('22.01.2020','dd.mm.yyyy') and to_date('22.03.2020','dd.mm.yyyy')) c on c.art = a.art
 WHERE BIT_ALLOW='T' AND BIT_CLOSE='T' 
 GROUP BY a.art,case when a.procent > 0 then a.procent else b.PROCENT end-- a.procent
 ORDER BY a.ART,PROCENT;
--END;

select * from pos_brak where art = '013010';

--SCAN_CENNIK_UPD(V_REL);
select CENA2P(sysdate,'1738786',0,1, 40) from dual;

select * from TMP_ART where SESSIONID in ('443731778','443608792','443617969') order by date_s desc;

select art,scan,procent,vid_load
from (SELECT a.ART,a.SCAN,case when a.vid_load = 'ТСД' then nvl(c.PROCENT,0) else a.procent end procent,a.VID_LOAD,max(a.date_s) date_s
			FROM TMP_ART a --СОЗДАНИЕ КУРСОРА
			left join (select ean,max(art) art,max(asize) asize from s_ean group by ean) b on a.scan = b.ean
			left join (select art,procent,sum(kol) kol
									from e_osttek
									group by art,procent
									having sum(kol) > 0) c on b.art = c.art
			WHERE a.SESSIONID='446374248'--443731778--443617969, 443608792
			group by a.ART,a.SCAN,a.VID_LOAD,case when a.vid_load = 'ТСД' then nvl(c.PROCENT,0) else a.procent end
			order by date_s
);

select art,scan,procent,vid_load
from (SELECT a.ART,a.SCAN,case when a.vid_load = 'ТСД' then nvl(c.PROCENT,0) else a.procent end procent,a.VID_LOAD,max(a.date_s) date_s
			FROM TMP_ART a --СОЗДАНИЕ КУРСОРА
			left join (select ean,max(art) art,max(asize) asize from s_ean group by ean) b on a.scan = b.ean
			left join (select art,procent,sum(kol) kol
									from e_osttek
									group by art,procent
									having sum(kol) > 0) c on b.art = c.art
			WHERE a.SESSIONID='445526870'
			group by a.ART,a.SCAN,a.VID_LOAD,case when a.vid_load = 'ТСД' then nvl(c.PROCENT,0) else a.procent end

SELECT NVL(MAX(ART),' '),MAX(ASSORT),MAX(NEI),MAX(PROCENT) /*INTO V_ART,V_ASSORT,V_NEI,V_PROCENT*/ FROM S_LABEL_V WHERE SCAN='00000000001102479129';

SELECT CASE WHEN COUNT(*)=0 /*AND 'БРАК' != 'БРАК'*/ THEN 'T' ELSE 'F' END, count(*) FROM POS_BRAK WHERE SCAN=' '; --00000000001002614725

select * from D_CENNIK2 where rel = 87;

SELECT ART,SCAN,PROCENT,IS_NONCOND,VID_LOAD FROM D_CENNIK2 --СОЗДАНИЕ КУРСОРА
WHERE REL=87 ORDER BY NPP;

SELECT DATED FROM D_CENNIK1 WHERE REL=87; -- ДАТА РАСЧЕТА
SELECT MAX(ASSORT),MAX(NEI),MAX(ART_REAL),MAX(MTART) AS MTART FROM S_ART_V WHERE ART='1738786';
SELECT SUM(KOL) FROM E_OSTTEK WHERE ART='1738786' AND PROCENT='40';
SELECT NVL(MAX(EAN),' ') FROM S_EAN WHERE ART='1738786';
select CENA2P_PROC(to_date('22.03.20', 'dd.mm.yy'),'1738786',0,0, V_CENA =>V_CENA2,V_DATEB =>V_DATEP,V_NDOC => V_NDOCP) from dual;-- 780 -- 2019-10-21 -- Приход N.327
select CENAPS_SKIDKA_PROC(V_DATED,V_ART,V_PROCENT,V_CENAS => V_CENA3,V_SKIDKA => V_SKIDKA,V_DATES => V_DATES,V_NDOCS => V_NDOCS) from dual; -- 3797 -- 0 -- 2019-03-01 -- 0200001165