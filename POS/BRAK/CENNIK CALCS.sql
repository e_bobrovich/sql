select * from pos_brak;
select * from s_label;
select * from pos_brak_v;
select * from d_cennik1 where rel = 87;
select * from d_cennik2 where rel = 87;
select * from D_CENNIK2_V;
select * from st_brak_uc;


select * from d_kassa_cur;

select CENA2P(sysdate,'1648001',0,1) from dual;
select CENA2P(sysdate,'1648001',0,1,20) from dual;
select CENA2S(sysdate,'1648001',0,1,60, '00', 20) from dual;


SELECT cena2s(sysdate, art, asize, 1, 40, 0, 20), procent , art FROM S_LABEL WHERE SCAN = '00000000001100222821';

select * from pos_brak where scan = '00000000001100222821';

select * from S_LABEL_V  where scan = '00000000001100222821';

SELECT
NVL(MAX(CASE WHEN DISC_TYPE_ID in ('ZK02','ZK22') THEN CENA ELSE 0 END),0) AS CENA   -- ФИКСИРОВАННАЯ ЦЕНА НА СКИДКЕ
,NVL(MAX(CASE WHEN DISC_TYPE_ID in ('ZK01','ZK31','ZK21') THEN PROCENT ELSE 0 END),0) AS PROCENT -- ПРОЦЕНТ СКИДКИ ОТ ПРАЙСОВОЙ ЦЕНЫ
FROM (
SELECT 0 nek,A.DISC_TYPE_ID,A.PRIORITY,B.*
       FROM  POS_SKIDKI2 B
       INNER JOIN POS_SKIDKI1 A ON B.DOCID = A.DOCID
         WHERE A.DISC_TYPE_ID IN ('ZK01','ZK02')
               AND  B.ART = /*/*i_art*/'013010'
               AND  to_number(to_char(/*/*i_date*/sysdate,'yyyymmdd')) between to_number(to_char(dates,'yyyymmdd')) and to_number(to_char(datee,'yyyymmdd'))
union all--для некондиции в РБ ZK31 может быть разный процент скидки на один артикул в зависимости от процента уценки 05092017
SELECT 1 nek,A.DISC_TYPE_ID,A.PRIORITY,B.*
       FROM  POS_SKIDKI2 B
       INNER JOIN POS_SKIDKI1 A ON B.DOCID = A.DOCID
         WHERE A.DISC_TYPE_ID IN ('ZK31')
               AND  B.ART = /*/*i_art*/'013010'
               AND  to_number(to_char(/*/*i_date*/sysdate,'yyyymmdd')) between to_number(to_char(dates,'yyyymmdd')) and to_number(to_char(datee,'yyyymmdd'))
               AND  B.CENA = /*i_procent*/ 40
union all--для некондиции РФ
SELECT 1 nek,A.DISC_TYPE_ID,A.PRIORITY,B.*
       FROM  POS_SKIDKI2 B
       INNER JOIN POS_SKIDKI1 A ON B.DOCID = A.DOCID
         WHERE /*V_NUMCONF*/1 = 1 AND A.DISC_TYPE_ID IN ('ZK21','ZK22')
               AND  B.ART = /*/*i_art*/'013010'
               AND  to_number(to_char(/*/*i_date*/sysdate,'yyyymmdd')) between to_number(to_char(dates,'yyyymmdd')) and to_number(to_char(datee,'yyyymmdd'))
               and /*I_NONCOND_PROC*/ 40 != 0
               and /*i_procent*/ 40 > 0
           ORDER BY nek desc,PRIORITY DESC ) a
WHERE ROWNUM=1
group by DOCID, disc_type_id;

select nvl(procent,0) procent
--into  v_discount_open
from pos_skidki1 a
inner join pos_skidki2 b on B.DOCID = A.DOCID
where a.openshop_flag = 'T'
  and B.ART = /*i_art*/'1738786'
  AND  to_number(to_char(/*i_date*/sysdate,'yyyymmdd')) between to_number(to_char(dates,'yyyymmdd')) and to_number(to_char(datee,'yyyymmdd'));