SELECT *  FROM D_CENNIK2_V  WHERE REL=87;

 SELECT '446385437',a.ART, a.SCAN, 
 a.PROCENT PROCENT_YCEN, 
 a.PROCENT_NONCOND PROCENT_NONCOND, 
 'БРАК' 
 FROM POS_BRAK a
 inner join (select art,asize,scan,procent
	      from e_osttek
	      where scan != ' '
	      group by art,asize,scan,procent
	      having sum(kol) > 0) b on a.art = b.art and a.asize = b.asize and a.scan = b.scan
 inner join (select distinct art
		from pos_brak
		where scan != ' '
		and trunc(dated) between to_date('26.01.2020','dd.mm.yyyy') and to_date('26.03.2020','dd.mm.yyyy')
		union all
		select distinct art
		from kart_v
		where scan != ' '
		and trunc(dated) between to_date('26.01.2020','dd.mm.yyyy') and to_date('26.03.2020','dd.mm.yyyy')) c on c.art = a.art
 WHERE BIT_ALLOW='T' AND BIT_CLOSE='T' 
 GROUP BY a.ART, a.SCAN,  
 a.PROCENT_NONCOND, 
 a.PROCENT 
 ORDER BY a.ART,a.PROCENT;



select art,scan,procent/*PROCENT_YCEN*/, vid_load, PROCENT_NONCOND/*PROCENT_NONCOND*/
from (SELECT a.ART,a.SCAN,case when a.vid_load = 'ТСД' then nvl(c.PROCENT,0) else a.procent end procent, PROCENT_NONCOND,a.VID_LOAD,max(a.date_s) date_s
			FROM TMP_ART a --СОЗДАНИЕ КУРСОРА
			left join (select ean,max(art) art,max(asize) asize from s_ean group by ean) b on a.scan = b.ean
			left join (select art,procent,sum(kol) kol
									from e_osttek
									group by art,procent
									having sum(kol) > 0) c on b.art = c.art
			WHERE a.SESSIONID=446455127
			group by a.ART,a.SCAN,a.VID_LOAD,case when a.vid_load = 'ТСД' then nvl(c.PROCENT,0) else a.procent end, PROCENT_NONCOND
			order by date_s);
      
select * from d_cennik2 where rel = 87;
      
select GET_STRR_BRAK('1738786',/*V_PROCENT*/'52') from dual;
select * from st_brak_uc where vid_brak = '61';

select * from s_label_v where scan = '00000000001102479129';

select 'Код дефекта: '||b.vid_brak vid_brak, 'Дефект: '||b.brak_name brak_name, ceil(length('Дефект: '||b.brak_name)/34) leng, 'nek' tip 
from s_label_v a 
left join st_brak_uc b on nvl(a.code_def,'0') = b.vid_brak 
where a.scan = '00000000001001445158' and a.procent > 0 
union all 
select 'Код дефекта: '||vid_brak vid_brak, 'Дефект: '||brak_name brak_name, ceil(length('Дефект: '||brak_name)/34) leng,'brak' tip 
from pos_brak where scan = '00000000001001445158';