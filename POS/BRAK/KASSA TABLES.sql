select * from pos_sale1;
select * from pos_sale2;
select * from pos_sale3;
select * from pos_sale4;
select * from pos_sale5;
select * from pos_sale6;
select * from pos_sale9;


select c.art,C.ASIZE,C.PROCENT,C.ASSORT,C."KOL",C.PRICE,C."DISCOUNT_R",C.DISCOUNT,C."PRICE_SUM",C.SCAN,C.EAN,C.NEI,C.ID_CHEK,C.ID_SALE,C.IS_SERTIF,C.POSNR,C.DISCOUNT_DK,c.bit_enet,c.bit_book,c.enet_posnr 
,c.discount_r-c.discount_DK discount_other,case when d.art is null then 'F' else 'T' end COLOR_FLAG,c.noncond_proc,
case when c.procent > 0 then sb.vid_brak||' '||sb.brak_name when e.procent > 0 then e.code_def||' '||s.brak_name else ' ' end brak_name
 from (
  select ART, ASIZE, PROCENT, ASSORT, SUM(KOL) "KOL" , to_char(PRICE) price, to_char(SUM(DISCOUNT_R)) "DISCOUNT_R", DISCOUNT, to_char(SUM(PRICE_SUM)) "PRICE_SUM", 
  SCAN, EAN, NEI, ID_CHEK, ID_SALE, IS_SERTIF, POSNR,  
  (select sum(b.discount_sum) from d_kassa_disc_cur b where b.kass_id = '1#lacit010-m' and b.posnr = a.posnr and B.DISCOUNT_TYPE_ID = 'A000000001') DISCOUNT_DK,
  a.bit_enet,a.bit_book,a.enet_posnr,a.noncond_proc 
  from D_KASSA_CUR a 
  where  KASS_ID = '1#lacit010-m' group by ART, ASIZE, PROCENT, ASSORT, PRICE, DISCOUNT, SCAN, POSNR, EAN, NEI, ID_CHEK, ID_SALE, IS_SERTIF,bit_enet,bit_book,enet_posnr,noncond_proc
) c
left join (select distinct art from st_prem_art) d on c.art = d.art
left join s_label_v e on e.scan = nvl(c.scan,'n/a')
left join (select distinct vid_brak,brak_name from st_brak_uc) s on s.vid_brak = nvl(e.code_def,'0')
left join pos_brak f on f.scan = nvl(c.scan,'n/a')
left join (select distinct vid_brak,brak_name from st_brak_uc) sb on sb.vid_brak = nvl(f.vid_brak,'0')
 order by c.POSNR;
 
 select distinct A.ART, A.assort||' '||A.groupmw as "NAME", ost_art_asize_procent(A.ART, null, null) as "KOL", 
 (select sum(kol) from e_osttek where scan in (select x.scan from pos_brak x where x.art = a.art)) KOL_B,
 (select sum(kol) from ENET_BOOK_SCANS e where e.art = a.art and get_system_val('INET_SALE') = 'T') KOL_E,
 get_cena_p(to_date('27.03.2020','dd.MM.rrrr'), A.ART, null, 1) as "PRICE", 
 get_cena_s(sysdate,a.art,b.asize,1) as PRICES,
 size_range(a.ART) as "SIZE_RANGE", A.prodh,case when d.art is null then 'F' else 'T' end COLOR_FLAG 
 from s_art_v A 
 left join S_SIZE_SCALE B on a.art = b.art 
 left join (select distinct art from st_prem_art) d on a.art = d.art 
 where UPPER(a.ART) like '%1738786%' and ost_art_asize_procent(A.ART, null, null) > 0
 ;
 
select distinct A.ART, b.asize, C.procent, ost_art_asize_procent(A.ART, b.asize, c.procent) as "KOL", 
nvl((select sum(kol) from e_osttek where scan in (select x.scan from pos_brak x where x.art = a.art and x.asize = b.asize)),0) KOL_B,
(select sum(kol) from ENET_BOOK_SCANS e where e.art = a.art and e.asize = b.asize and get_system_val('INET_SALE') = 'T') KOL_E,
cena2p(to_date('27.03.2020','dd.MM.rrrr'), 
A.ART, b.asize, 1,c.procent) as "PRICE", 
cena2s(sysdate,a.art,b.asize,1, c.procent,'00',c.procent) as PRICES,
'F' color_flag 
from s_art_v A 
inner join S_SIZE_SCALE B on a.art = b.art 
left join s_label_v C on a.art = c.art 
where a.ART = '1738786' 
and ost_art_asize_procent(A.ART, b.asize, c.procent) > 0 order by b.asize
;


SELECT distinct    a.ART,    a.ASIZE,   
case when nvl(d.procent_ycen,0) > 0 then cast(d.procent_ycen AS varchar2(3)) else cast(a.procent AS varchar2(3)) end procent,
--(CASE WHEN a.procent > 0 THEN cast(a.procent AS varchar2(3)) 
--ELSE (
--  select cast(nvl(max(case when y.bit_allow = 'T' then y.procent else (case when y.procent > 20 then 20 else y.procent end) end),0) as varchar2(3)) from pos_brak y where y.scan = '00000000001102479129'
--) 
--END) procent,    
a.ASSORT,    
get_cena_p(to_date('27.03.2020','dd.MM.yyyy'),A.ART, a.asize, 1) "PRICE",    
ost_art_asize_procent(A.ART, a.asize, a.procent) "COUNT",   
a.EAN,    b.NEI,  a.scan, a.procent noncond
FROM S_LABEL_V  
a INNER JOIN S_ART_V b on a.ART = b.ART 
inner join e_osttek c on c.scan = a.scan  
left join (
  select y.scan,cast(nvl(max(case when y.bit_allow = 'T' then y.procent else (case when y.procent > 20 then 20 else y.procent end) end),0) as varchar2(3)) procent_ycen 
  from pos_brak y where y.scan = '00000000001102479129'
  group by scan
) d on a.scan = d.scan
WHERE a.SCAN = '00000000001102479129' and 
(select nvl(sum(kol),0) from e_osttek where scan = '00000000001102479129') > 0;


SELECT distinct a.ART, a.ASIZE,
case when nvl(d.procent_ycen,0) > 0 then cast(d.procent_ycen AS varchar2(3)) else cast(a.procent AS varchar2(3)) end procent,
a.ASSORT, 
get_cena_p(to_date('28.03.2020','dd.MM.yyyy'), A.ART, a.asize, 1) "PRICE", 
ost_art_asize_procent(A.ART, a.asize, a.procent) "COUNT" , 
a.EAN, b.NEI, a.scan, a.procent noncond
FROM S_LABEL_V  a 
INNER JOIN S_ART_V b on a.ART = b.ART 
inner join e_osttek c on c.scan = a.scan  
left join (
  select scan, cast(nvl(max(case when y.procent > 20 and (y.bit_allow = 'F' or y.bit_close = 'F') then 20 else y.procent end),0) as varchar2(3)) procent_ycen
  from pos_brak y
  where y.scan = '00000000001106907112'
  group by scan
) d on a.scan = d.scan
WHERE a.SCAN = '00000000001106907112' and 
(select nvl(sum(kol),0) from e_osttek where scan = '00000000001106907112') > 0;


SELECT distinct a.ART, a.ASIZE,
case when a.procent > 0 then cast(a.procent as varchar2(3))
 case when nvl(d.procent_ycen,0) > 0 then cast(d.procent_ycen AS varchar2(3)) else cast(a.procent AS varchar2(3)) end procent,
 a.ASSORT,  get_cena_p(to_date('28.03.2020','dd.MM.yyyy'), A.ART, a.asize, 1) "PRICE",  ost_art_asize_procent(A.ART, a.asize, a.procent) "COUNT" , a.EAN,  b.NEI,  a.scan, a.procent noncond 
FROM S_LABEL_V  a INNER JOIN S_ART_V b on a.ART = b.ART inner join e_osttek c on c.scan = a.scan left join (   select y.scan,cast(nvl(max(case when y.procent > 20 and (y.bit_allow = 'F' or y.bit_close = 'F') then 20 else y.procent end),0) as varchar2(3)) procent_ycen   from pos_brak y where y.scan = '00000000001106907112' group by scan ) d on a.scan = d.scan 
 WHERE a.SCAN = '00000000001106907112' and (select nvl(sum(kol),0) from e_osttek where scan = '00000000001106907112') > 0