select nvl(min(case when procent > 0 then 'T' else 'F' end),'F')
--into v_nekond
from s_label
where scan = nvl('00000000001102479129',' ');

SELECT PROCENT, DOCID, a.disc_type_id
--INTO  v_discount, v_skidki_docid, v_disc_type_id
FROM (
  SELECT A.DISC_TYPE_ID,A.PRIORITY,B.*
       FROM  POS_SKIDKI2 B
       INNER JOIN POS_SKIDKI1 A ON B.DOCID = A.DOCID
         WHERE A.DISC_TYPE_ID IN ('ZK21')
               AND  B.ART = '1931150'
               AND  to_number(to_char(systimestamp,'yyyymmdd')) between to_number(to_char(dates,'yyyymmdd')) and to_number(to_char(datee,'yyyymmdd'))
  union all--для некондиции в РБ ZK31 может быть разный процент скидки на один артикул в зависимости от процента уценки 05092017
  SELECT A.DISC_TYPE_ID,A.PRIORITY,B.*
       FROM  POS_SKIDKI2 B
       INNER JOIN POS_SKIDKI1 A ON B.DOCID = A.DOCID
         WHERE A.DISC_TYPE_ID IN ('ZK31')
               AND  B.ART = '1931150'
               AND  to_number(to_char(systimestamp,'yyyymmdd')) between to_number(to_char(dates,'yyyymmdd')) and to_number(to_char(datee,'yyyymmdd'))
               AND  B.CENA = 20--52
           ORDER BY PRIORITY DESC 
) a
WHERE ROWNUM=1
group by procent, DOCID, disc_type_id;


