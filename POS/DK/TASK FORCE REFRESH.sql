--1, 2, 5, 8, 9, 24, 26
--
--добрый день. сегодня планируем в филиалах 1, 2, 5, 8, 9, 24, 26 подключить принудительное обновление бонусных карт при их сканировании. если в течение 2 недель критических проблем не будет, то подключим все остальные филиалы РФ.
--принудительное обновление включается\выключается через настройки параметров отделения(FORCE_REFRESH_DK	принудительное обновление карты при сканировании). при проблемах с интернетом\сетью у отделения можно отключить параметр.
--через 2 недели сообщите, пожалуйста, информацию были ли какие-нибудь проблемы\обращения связанные с этим. если 


--SELECT shopid FROM S_SHOP WHERE SUBSTR(SHOPNUM, 2, 5) IN ('24111', '03130', '03102', '11132', '11124', '07130', '09132', '12125', '10124', '00068', '06140', '00071', '00063', '06136', '00067', '06146', '00069', '07111', '03107', '00064', '00070', '02119', '02121', '04130', '04131');


/*
SET serveroutput ON
declare
p_shopLink varchar(10);
var number(5);
varc varchar(200);
ddl_ins1 varchar(1000);
cursor shop is SELECT shopid FROM S_SHOP WHERE SUBSTR(SHOPNUM, 2, 5) IN ('24111', '03130', '03102', '11132', '11124', '07130', '09132', '12125', '10124', '00068', '06140', '00071', '00063', '06136', '00067', '06146', '00069', '07111', '03107', '00064', '00070', '02119', '02121', '04130', '04131')
order by shopid;
shopRow shop%rowtype;
p_shopID varchar2(5);
BEGIN
 
open shop;
  loop  
       FETCH shop INTO p_shopID;
       EXIT WHEN shop%NOTFOUND;  
          begin
          BEGIN
    DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"FIRM"."SPA_ON_SPORT_QUEST_' || p_shopID || '_5"',
            program_name => '"FIRM"."SPA_ON_SHOP_PARAM"',
            start_date => TO_TIMESTAMP_TZ('2019-04-20 10:05:01.000000000 EUROPE/MINSK','YYYY-MM-DD HH24:MI:SS.FF TZR'),
            repeat_interval => NULL,
            end_date => NULL,
            enabled => FALSE,
            auto_drop => TRUE,
            comments => '',
              
            job_style => 'REGULAR');

    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE( 
             job_name => '"FIRM"."SPA_ON_SPORT_QUEST_' || p_shopID || '_5"', 
             argument_position => 1, 
             argument_value => p_shopID);
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE( 
             job_name => '"FIRM"."SPA_ON_SPORT_QUEST_' || p_shopID || '_5"', 
             argument_position => 2, 
             argument_value => 'SPORT_QUEST');
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE( 
             job_name => '"FIRM"."SPA_ON_SPORT_QUEST_' || p_shopID || '_5"', 
             argument_position => 3, 
             argument_value => 'T');
         
     
 
    DBMS_SCHEDULER.SET_ATTRIBUTE( 
             name => '"FIRM"."SPA_ON_SPORT_QUEST_' || p_shopID || '_5"', 
             attribute => 'logging_level', value => DBMS_SCHEDULER.LOGGING_OFF);
      
  
    
    DBMS_SCHEDULER.enable(
             name => '"FIRM"."SPA_ON_SPORT_QUEST_' || p_shopID || '_5"');
END;

          dbms_output.put_line(p_shopID||' OK ');
            EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line(sqlcode||sqlerrm);
            dbms_output.put_line(p_shopID||' ERRRRRR ');
            commit;
          end;
  end loop;
  
END ;
*/

--ЗАДАНИЯ НА ВЫВОЗ НА БЕЛВЕСТ

select distinct shopid from st_retail_hierarchy a where shopparent in ('21F1', '22F1', '25F1', '28F1', '29F1', '44F1', '46F1') order by shopid;

select * from ri_system@s0031 where sys_var = 'FORCE_REFRESH_DK';

SET serveroutput ON
DECLARE
V_SELE VARCHAR2(1024):='';
V_SHOPID VARCHAR2(21):='';  
V_RESULTP VARCHAR2(99):='OK'; 
CURSOR1 sys_refcursor;
V_KOL NUMBER(3,0):=0;
V_LINK VARCHAR2(20) ; 
V_BIT_CLOSE VARCHAR2(1) ;
V_ID NUMBER(10,0):=0;

BEGIN
	 --ID=9054 - номер расчета
	 --to_char(date_s,'yyyyMMdd') >= '20190306' - датами фильтровать уже созданные задания
   OPEN CURSOR1 FOR  select distinct shopid from st_retail_hierarchy a where shopparent in ('21F1', '22F1', '25F1', '28F1', '29F1', '44F1', '46F1') order by shopid
; 

 LOOP --
   <<LAB>>
   FETCH CURSOR1 INTO V_ID,V_SHOPID; --
   EXIT WHEN CURSOR1%NOTFOUND; --

   SELECT NVL(MAX(DB_LINK_NAME),' ') INTO V_LINK FROM ST_SHOP WHERE SHOPID=V_SHOPID;  
	 
	 begin
						DBMS_SCHEDULER.drop_job( 'script_param_'||V_SHOPID);
					exception when others then
						null;
					end;
					
					DBMS_SCHEDULER.CREATE_JOB (
					 job_name             => 'script_param_'||V_SHOPID,
					 job_type             => 'PLSQL_BLOCK',
					 job_action           => q'[
declare
V_RES  VARCHAR2(99):='OK';
begin
BEGIN

EXECUTE IMMEDIATE  'select * from config@]'||V_LINK||q'[';
  EXCEPTION 
  WHEN OTHERS THEN
  return;
END;    

  INSERT INTO RI_SYSTEM (SYS_VAR, SYS_VAL, TEXT)
  VALUES ('FORCE_REFRESH_DK', 'T','принудительное обновление карты при сканировании');
  
  COMMIT;
end;
]',
					 enabled              =>  false);
			
					DBMS_SCHEDULER.ENABLE ('script_param_'||V_SHOPID); 

 commit;
 dbms_output.put_line(V_SHOPID);
 V_KOL:=V_KOL+1;
 
 
END LOOP; --

  EXCEPTION
  WHEN OTHERS THEN
  dbms_output.put_line('Ошибка '||V_SHOPID||' '||sqlcode||' '||sqlerrm);
  
END
;