select * from t_ob_logs where textd = 'SPA_MOVE_MATRIX'  and text1 = '10755188' order by timed desc;

select * from d_inet_book1 where posnr = '10755188';
select * from d_inet_book2 where posnr = '10755188';
select * from d_inet_book3 where posnr = '10755188' order by date_s;
select * from d_inet_book4 where posnr = '10755188';


select * from pos_dk_scores where id_dk = '0000020916114';

select * from pos_dk_scores@s0070;

select * from (
  select
  a.id_dk_from, a.id_dk_to,
  rank() over (partition by id_dk_from order by ddate desc) rnk
  from POS_DK_COPY_LOGS a
  where trunc(ddate) > trunc(sysdate-10) - 2 -- в течении последних 2 дней
) where rnk = 1;



select distinct id_dk from pos_dk_scores@s0070 a
where id_dk in (
  select id_dk_from from (
    select
    a.id_dk_from, a.id_dk_to,
    rank() over (partition by id_dk_from order by ddate desc) rnk
    from POS_DK_COPY_LOGS a
    where trunc(ddate) > trunc(sysdate-10) - 2 -- в течении последних 2 дней
  ) where rnk = 1
);

update pos_dk_scores@s0070 x
set x.id_dk = (
        select max(y.id_dk_to) from POS_DK_COPY_LOGS y
        where y.id_dk_from = x.id_dk
        and trunc(y.ddate) > trunc(sysdate-10) - 2
)
where x.id_dk = (
  select distinct y.id_dk_from from POS_DK_COPY_LOGS y
  where y.id_dk_from = x.id_dk
  and trunc(y.ddate) > trunc(sysdate-10) - 2
);

merge into pos_dk_scores@s0070 x
using (
        select id_dk_from, id_dk_to from (
          select
          id_dk_from, id_dk_to,
          rank() over (partition by id_dk_from order by ddate desc) rnk
          from POS_DK_COPY_LOGS
          where trunc(ddate) > trunc(sysdate-10) - 2 -- в течении последних 2 дней
        ) where rnk = 1
) y on (x.id_dk = y.id_dk_from)
when matched then update set x.id_dk = y.id_dk_to;



select distinct id_dk from pos_dk_scores@S0070 a
where id_dk in (
  select id_dk_from from (
    select a.id_dk_from, a.id_dk_to, rank() over (partition by id_dk_from order by ddate desc) rnk
    from POS_DK_COPY_LOGS a
    where trunc(ddate) > trunc(sysdate-10) - 2 -- в течении последних 2 дней
  ) where rnk = 1
);

select distinct a.id_Dk, b.id_dk_to from pos_dk_scores@S0070 a
inner join (
 select id_dk_from, id_dk_to from (
    select a.id_dk_from, a.id_dk_to, rank() over (partition by id_dk_from order by ddate desc) rnk
    from POS_DK_COPY_LOGS a
    where trunc(ddate) > trunc(sysdate-10) - 2 -- в течении последних 2 дней
  ) where rnk = 1
) b on a.id_dk = b.id_dk_from;


SET serveroutput ON
DECLARE
  I_ID_SHOP VARCHAR2(4) := '0070';
  v_str_dk VARCHAR2(2000);
  v_str_update VARCHAR2(2000);
  V_LINK VARCHAR2(20);
  
  TYPE DkCurTyp  IS REF CURSOR;
  v_dk_cursor    DkCurTyp;
  dk_from_record      varchar2(20);
  dk_to_record      varchar2(20);
BEGIN
  v_link := get_shop_link(i_id_shop); 
  v_str_dk := 
     'select distinct a.id_Dk, b.id_dk_to from pos_dk_scores@'||V_LINK||' a
      inner join (
       select id_dk_from, id_dk_to from (
          select a.id_dk_from, a.id_dk_to, rank() over (partition by id_dk_from order by ddate desc) rnk
          from POS_DK_COPY_LOGS a
          where trunc(ddate) > trunc(sysdate-10) - 2 -- в течении последних 2 дней
        ) where rnk = 1
      ) b on a.id_dk = b.id_dk_from';
--  dbms_output.put_line(v_str_dk);
  OPEN v_dk_cursor FOR v_str_dk;
  LOOP
    FETCH v_dk_cursor INTO dk_from_record, dk_to_record;
    EXIT WHEN v_dk_cursor%NOTFOUND;
    dbms_output.put_line(dk_from_record||' '||dk_to_record);
    
    v_str_update := 'update pos_dk_scores@'||v_link||' set id_dk = '''||dk_to_record||''' where id_dk = '''||dk_from_record||'''';
    dbms_output.put_line(v_str_update);
    
  END LOOP;
END;

