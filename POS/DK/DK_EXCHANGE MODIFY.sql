select a.shopid,max(BIT_ACCESS)
                        from FIRM.ST_SHOP_Access  a
                        inner join FIRM.s_shop b on a.shopid = b.shopid
                        inner join FIRM.st_shop c on a.shopid = c.shopid
                        where --b.landid!='BY' and 
                                 c.org_kod = 'SHP'
                                 and a.shopid = case when '0001' is not null then '0001' else a.shopid end 
                        group by a.shopid having max(BIT_ACCESS) = 'T'
                        order by a.shopid;

select  *
  from dba_scheduler_jobs
  where owner = 'SYSTEM' and upper(job_name) like 'J_DK_%';

SET serveroutput ON
declare
  i_shop varchar2(4) := '0001';
  v_exchange_end number default 1;
  v_Sproc_time number;
BEGIN
  for r_shop in (select a.shopid,max(BIT_ACCESS)
                        from FIRM.ST_SHOP_Access  a
                        inner join FIRM.s_shop b on a.shopid = b.shopid
                        inner join FIRM.st_shop c on a.shopid = c.shopid
                        where --b.landid!='BY' and 
                                 c.org_kod = 'SHP'
                                 and a.shopid = case when i_shop is not null then i_shop else a.shopid end 
                        group by a.shopid having max(BIT_ACCESS) = 'T'
                        order by a.shopid) loop
  
    begin
      DBMS_SCHEDULER.drop_job( 'j_dk_rplc_'||r_shop.shopid);
    exception when others then
      null;
    end;
  
    DBMS_SCHEDULER.CREATE_JOB
    ( job_name      => 'j_dk_rplc_'||r_shop.shopid
    , program_name  => 'DK_REPLACE_IN_SHOP'
    --, schedule_name => 'simple_schedule'
    , enabled       => FALSE
  --  ,AUTO_DROP => false
    );
  
    DBMS_SCHEDULER.SET_JOB_ANYDATA_VALUE
    ( job_name      => 'j_dk_rplc_'||r_shop.shopid
    , argument_name  => 'i_id_shop'
    , argument_value => ANYDATA.ConvertVarchar2(r_shop.shopid) );
  
    DBMS_SCHEDULER.ENABLE ( 'j_dk_rplc_'||r_shop.shopid );
  end loop;
  
  v_Sproc_time := dbms_utility.get_time;
  v_exchange_end := 1;
  
  -- ждем пока завершатся задания по замене
  while v_exchange_end != 0 loop
    dbms_lock.sleep(60);  
  
    select  count(*)
    into v_exchange_end
    from dba_scheduler_jobs
    where owner = 'SYSTEM' and upper(job_name) like 'J_DK_RPLC_%';
    
    if (dbms_utility.get_time - v_Sproc_time) / 100 > 360 then -- если больше 10 минут выполняется, то нужно прибить оставшиеся задания и выйти
     dbms_output.put_line('прошло больше 10 минут');
     
     for r_sid in (select distinct sid,serial#
                      from v$session s,v$sql q
                      where s.sql_address = q.address
                               and s.sql_hash_value = q.hash_value and s.action like 'J_DK_RPLC_%') loop
        begin
          execute immediate 'ALTER SYSTEM KILL SESSION '''||r_sid.sid||','||r_sid.serial#||''' IMMEDIATE';                   
        exception when others then
          null;
        end ;             
      end loop; 
      exit;
    end if;  
  end loop;   
END;
/