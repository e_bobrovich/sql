select * from st_shop;

select art, sum(kol_by) kol_by, sum(kol_ru) kol_ru 
from (
  select art, sum(kol) kol_by, 0 kol_ru from (
    select a.*, substr(b.shopnum, 2,5) shopnum, b.shopname, b.landid from ( 
      select art, id_shop, sum(kol) kol
      from (
        select /*a.scan,*/ a.art, a.asize, a.id_shop, sum(a.kol) kol from e_osttek_online a
        inner join EVGENIJ_BOBROVICH.Map_List_Arts b on a.art = b.art and b.id_list = 187
        group by /*a.scan,*/ a.art, a.asize, a.id_shop
        having sum(a.kol) > 0
      )
      group by art, id_shop
    ) a
    inner join s_shop b on a.id_Shop = b.shopid
    inner join st_shop c on a.id_shop = c.shopid
    where id_shop not in ('I002') and c.bit_open = 'T'
    and b.landid = 'BY'
    order by art, id_shop
  )
  group by art
  
  union all
  
  select art, 0 kol_by, sum(kol) kol_ru from (
    select a.*, substr(b.shopnum, 2,5) shopnum, b.shopname, b.landid from ( 
      select art, id_shop, sum(kol) kol
      from (
        select /*a.scan,*/ a.art, a.asize, a.id_shop, sum(a.kol) kol from e_osttek_online a
        inner join EVGENIJ_BOBROVICH.Map_List_Arts b on a.art = b.art and b.id_list = 187
        group by /*a.scan,*/ a.art, a.asize, a.id_shop
        having sum(a.kol) > 0
      )
      group by art, id_shop
    ) a
    inner join s_shop b on a.id_Shop = b.shopid
    inner join st_shop c on a.id_shop = c.shopid
    where id_shop not in ('I002') and c.bit_open = 'T'
    and b.landid = 'RU'
    order by art, id_shop
  )
  group by art
)
group by art
order by art
;


select a.scan, a.art, a.asize, a.id_shop, sum(a.kol) kol from e_osttek_online a
    inner join EVGENIJ_BOBROVICH.Map_List_Arts b on a.art = b.art and b.id_list = 187
    where id_shop = '1U01'
    group by a.scan, a.art, a.asize, a.id_shop
    having sum(a.kol) > 0;
    
select * from e_osttek_online where scan = '00000000001007252811';