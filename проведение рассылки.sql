------------------------------------
-- ОБНОВЛЕНИЕ ДАТЫ ПОСЛЕДНЕЙ ПОКУПКИ
------------------------------------
call fill_last_sale_date();
------------------------------------
-- ВСПОМОГАТЕЛЬНОЕ
------------------------------------
insert into delivery_filters 
select 1067, 'ID_SHOP', value1, null from delivery_filters where id_del = 1022 and field_name = 'ID_SHOP';

insert into delivery_filters
select 1024, 'ID_SHOP', a.shopid, null 
from firm.st_shop a
inner join firm.st_retail_hierarchy b on a.shopid = b.shopid where b.shopparent in (
'30F1',
'31F1',
'34F1',
'35F1'
) order by shopid;


insert into delivery_dk
select 1025, id_dk from v_st_dk
where
v_st_dk.id_shop in (select a.value1 from delivery_filters a where a.id_del = 1025 and a.field_name = 'ID_SHOP')
and v_st_dk.birthday >= to_date('10.08.1972', 'DD.MM.YYYY')
and v_st_dk.last_buy_date >= to_date('01.01.2016', 'DD.MM.YYYY')
group by id_dk;

delete 
--select *
from delivery_dk dd
where dd.id_del = 1025 
and dd.id_dk in (
    select id_dk from firm.pos_sale1
    inner join firm.pos_sale2 on firm.pos_sale1.id_chek = firm.pos_sale2.id_chek and firm.pos_sale1.id_shop = firm.pos_sale2.id_shop                
    where firm.pos_sale1.id_dk != ' ' 
    and firm.pos_sale2.art in (
      select a.value1 from delivery_filters a where a.id_del = 1025 and a.field_name = 'FIRM.POS_SALE2.ART')
    and (firm.pos_sale1.sale_date >= to_date('01.06.2017', 'dd.mm.yyyy') 
    and firm.pos_sale1.sale_date <= to_date('09.08.2017', 'DD.MM.YYYY'))
    group by id_dk
    );
    
    select firm.st_dk.id_dk from firm.st_dk
where 
id_dk not in (
  select ps1.id_dk from firm.pos_sale1 ps1 
  inner join firm.pos_sale2 ps2 on ps1.id_shop = ps2.id_shop and ps1.id_chek = ps2.id_chek
  left join firm.s_art sa on ps2.art = sa.art
  where ps1.id_dk != ' '
  and sa.color != ' '
  and ps2.art in (
    select s2.art from FIRM.POS_SKIDKI2 s2
    left join firm.pos_skidki1 s1 on s2.docid = s1.docid
    where s1.dates <= ps1.sale_date
    and s1.datee >= ps1.sale_date
    and s2.procent != 0
  )
  and ps1.sale_date >= to_date('01.06.2017', 'DD.MM.YYYY') 
  and ps1.sale_date <= to_date('09.08.2017', 'DD.MM.YYYY')
);
    
    
------------------------------------
-- ИСПОЛЬЗОВАНИЕ ФИЛЬТРОВ
------------------------------------
---call fill_dk_table(id)
call use_filters(1063);
------------------------------------
-- Исключение черного списка
------------------------------------
delete from delivery_dk dd
where dd.id_dk in (
  select bl.id_dk from black_list bl
) 
and dd.id_del = i_id_del;

------------------------------------
-- УДАЛЕНИЕ ДУБЛЕЙ
------------------------------------
delete from delivery_dk where id_dk in (
  select a.id_dk from delivery_dk  a
  inner join firm.st_dk b on a.id_dk = b.id_dk
  where get_im_phone(b.phone_number) in (
    select  get_im_phone(b.phone_number)
    from delivery_dk a
    inner join firm.st_dk b on a.id_dk = b.id_dk
    where id_del = 1067
    group by id_del, get_im_phone(b.phone_number) having count(*)>1 
  ) and id_del = 1067
) and id_del = 1067;

delete from delivery_dk_temp where id_dk in (
  select a.id_dk from delivery_dk  a
  inner join firm.st_dk b on a.id_dk = b.id_dk
  where get_im_phone(b.phone_number) in (
    select  get_im_phone(b.phone_number)
    from delivery_dk a
    inner join firm.st_dk b on a.id_dk = b.id_dk
    where id_del = ID_DEL
    group by id_del, get_im_phone(b.phone_number) having count(*)>1 
  ) and id_del = ID_DEL
) and id_del = ID_DEL;
------------------------------------
-- УДАЛЕНИЕ ОШИБОК
------------------------------------
delete from delivery_dk_temp where get_dk_error(id_dk) is not null and  id_del=i_id_del;
select count(*) into v_count from delivery_dk where get_dk_error(id_dk) is not null and  id_del=i_id_del;
delete from delivery_dk where get_dk_error(id_dk) is not null and  id_del=i_id_del;
------------------------------------
-- ДОБАВЛЕНИЕ В ПУЛ
------------------------------------
call add_delivery_pool(ID_DEL, 'NEW');
------------------------------------
-- ЧИСЛО ПОЛУЧАТЕЛЕЙ
------------------------------------
select id_del, count(id_dk) from delivery_dk where id_del >= 1060
group by id_del;

select fl.login, count(dd.id_dk) from delivery_dk dd
left join firm.st_dk sd on dd.id_dk = sd.id_dk
inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
inner join s_filial_logins fl on b.shopparent = fl.shopid
where dd.id_del >= id_del and dd.id_del1 <= id_del2
group by fl.login;
------------------------------------
-- БАЗА РБ
------------------------------------
select dd.id_dk, 3||get_im_phone(sd.phone_number) from delivery_dk dd
left join firm.st_dk sd on dd.id_dk = sd.id_dk
where dd.id_del = 1067;
------------------------------------
-- РЕЗУЛЬТАТЫ РАССЫЛКИ ПО ФИЛИАЛАМ
------------------------------------
select dh.id_del, fl.login, count(dh.id_dk) from delivery_history dh
left join firm.st_dk sd on dh.id_dk = sd.id_dk
inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
inner join s_filial_logins fl on b.shopparent = fl.shopid
where dh.id_del >= 1061 and dh.id_del <= 1069
group by dh.id_del, fl.login
order by dh.id_del;

select id_del, count(id_dk) from delivery_history where id_del >= 1060
group by id_del;

------------------------------------------------
-- КОПИРОВАНИЕ ДК ИЗ ДРУГОЙ РАССЫЛКИ ПО ФИЛИАЛАМ
------------------------------------------------
insert into delivery_dk
select GOAL_ID_DEL, dd.id_dk from delivery_dk dd
left join firm.st_dk sd on dd.id_dk = sd.id_dk
inner join firm.st_retail_hierarchy b on sd.id_shop = b.shopid
inner join s_filial_logins fl on b.shopparent = fl.shopid
where dd.id_del = from_id_del and regexp_replace(fl.login, '[^[:digit:]]') > num_fil

------------------------------------------------
-- РЕЗУЛЬТАТ - ПЛАН - ПРОГРЕСС РАССЫЛКИ
------------------------------------------------
select a.id_del,b.result, a.plan, round((b.result/a.plan)*100, 2) progress from (
    select id_del, count(id_dk) plan from delivery_dk where id_del >=1380 group by id_del
) a
left join (
    select id_del, count(id_dk) result from delivery_history where id_del >= 1380 group by id_del
) b on a.id_del = b.id_del;