select replace(x.shopparent, '25F2', '44F1') as shopparent, nvl(y.cnt,0) as cnt
from
(select replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy 
where shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
group by shopparent
order by shopparent
) x
left join
(
    select replace(b.shopparent, '25F2', '44F1') as shopparent, count(dd.id_dk) as cnt from delivery_dk dd
    left join firm.st_dk sd on dd.id_dk = sd.id_dk
    inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
    where dd.id_del in (2621,2601,2586,2585,2584,2583,2582,2581)
    and
    b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
 and dd.groupt = 'K'
    group by b.shopparent
) y on x.shopparent = y.shopparent
order by x.shopparent;


select replace(fil.shopparent, '25F2', '44F1') as shopparent, (nvl(vd.colvo, 0) + nvl(vr.colvo, 0)) as viber_d, nvl(vr.colvo, 0) as viber_r, nvl(sd.colvo, 0) as sms_d from 
(
    select replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy 
    where shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
    group by shopparent
    order by shopparent
) fil 
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 5)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) vd on fil.shopparent = vd.shopparent
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 6)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) vr on fil.shopparent = vr.shopparent
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 5)
  and dh.id_channel = 2
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) sd on fil.shopparent = sd.shopparent order by replace(vd.shopparent, '25F2', '44F1');


  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
and
b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 5)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent;
  
select dh.* from delivery_history dh
inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk 
where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
--and (dh.id_status = 5)
and dh.id_channel = 2
;

select replace(fil.shopparent, '25F2', '44F1') as shopparent, (nvl(vd.colvo, 0) + nvl(vr.colvo, 0)) as viber_d, nvl(vr.colvo, 0) as viber_r, nvl(sd.colvo, 0) as sms_d from
(select replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy 
where shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
group by shopparent
order by shopparent
) fil 
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 5)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) vd on fil.shopparent = vd.shopparent
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 6)
  and dh.id_channel = 1
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) vr on fil.shopparent = vr.shopparent
left join (
  select b.shopparent, count(*) as colvo from delivery_history dh
 inner join delivery_dk dd on dh.id_del = dd.id_del and dh.id_dk = dd.id_dk
  left join firm.st_dk sd on dh.id_dk = sd.id_dk
  inner join (select shopid, replace(shopparent, '25F2', '44F1') as shopparent from firm.st_retail_hierarchy) b on sd.id_shop = b.shopid
  inner join s_filial_logins fl on b.shopparent = fl.shopid
   where dh.id_del in (2621,2601,2586,2585,2584,2583,2582,2581) 
   and
   b.shopparent in ('21F1','22F1','23F1','24F1','25F1','26F1','27F1','28F1','29F1','30F1','31F1','32F1','33F1','34F1','35F1','36F1','37F1','38F1','39F1','40F1','41F1','42F1','43F1','44F1','45F1','46F1')
  and (dh.id_status = 5)
  and dh.id_channel = 2
 and (dd.groupt = 'O' or dd.groupt is null)
  group by b.shopparent
  order by b.shopparent
) sd on fil.shopparent = sd.shopparent order by replace(fil.shopparent, '25F2', '44F1')
