-- PREM_SHOP_PLAN_TO

--GRID SELECT
select id_shop, nvl(shopnum,id_shop) SHOP, year YEAR, month MONTH, 
plan_sum PLAN_SUM, nvl(fact_sum, 0) FACT_SUM, case when plan_sum = 0 then 0 else round(100*nvl(fact_sum, 0)/plan_sum, 2) end PROGRESS_SUM, 
plan_pair PLAN_PAIR, nvl(fact_pair, 0) FACT_PAIR, case when plan_pair = 0 then 0 else round(100*nvl(fact_pair, 0)/plan_pair, 2) end PROGRESS_PAIR 
from bep_prem_shop_plan a 
left join (select shopid, substr(shopnum,2, 5) shopnum from s_shop) b on a.id_shop = b.shopid
order by year desc, month desc, id_shop
;

--PLAN LIST MERGE
merge into bep_prem_shop_plan a
using (
    select nvl(b.shopid, lpad('10', 5, '0')) id_shop,  2020  year ,  1  month,  11111  plan_sum , 0 fact_sum,  222  plan_pair , 0 fact_pair from dual
    left join (select shopid, shopnum from s_shop) b on 'S'||lpad('10', 5, '0') = b.shopnum
) b on (a.id_shop = b.id_shop and a.year = b.year and a.month = b.month) when matched 
then update set a.plan_sum = b.plan_sum, a.plan_pair = b.plan_pair
when not matched 
then insert (id_shop, year, month, plan_sum, fact_sum, plan_pair, fact_pair) values (b.id_shop, b.year, b.month, b.plan_sum, b.fact_sum, b.plan_pair, b.fact_pair);


select substr(shopnum,2,5) shopnum, nvl(b.shopid, lpad('10', 5, '0')) id_shop ,  2020  year ,  1  month,  11111  plan_sum , 0 fact_sum,  222  plan_pair , 0 fact_pair from dual
left join (select shopid, shopnum from s_shop) b on lpad('10', 5, '0') = substr(b.shopnum,2,5)
;

-- BEP_PREM_SHOP_PLAN_DAY_TO
select distinct a.shopid, substr(c.shopnum, 2, 5) shopnum, a.shopname from st_shop a
inner join s_shop c on a.shopid = c.shopid
inner join st_retail_hierarchy b on a.shopid = b.shopid where b.shopparent in ('00F1') and a.org_kod = 'SHP' and a.bit_open = 'T'
and a.shopid in 
(select id_shop from bep_prem_shop_plan where year = 2019 and month = 1)
 ORDER BY a.shopid;

 
select date_n, b.id_shop shop, substr(f.shopnum, 2, 5) shopnum, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
nvl(e.plan_sum, d.plan_sum) plan_sum,
nvl(e.plan_pair, d.plan_pair) plan_pair
from 
(
    select to_date(( 1 + level - 1)||'.'||1||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, 1 month, 2019 year 
    from dual connect by level <= extract(day from last_day(to_date(1||'.'||2019,'mm.yyyy')))
) a 

inner join bep_prem_shop_plan b on lpad('0001', 4, '0') = b.id_shop and a.month = b.month and a.year = b.year
left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
inner join s_shop f on b.id_shop = f.shopid
where b.id_shop =  '0001' and b.year = 2019 and b.month =1
order by date_n
;


-----------

SELECT DISTINCT (CASE LANDID WHEN 'RU' THEN 'РФ' WHEN 'BY' THEN 'РБ' ELSE LANDID END) AS LAND, LANDID
FROM ST_SHOP
where org_kod = 'SHP'
ORDER BY LANDID
;

SELECT DISTINCT shopid, shopname FROM ST_SHOP 
WHERE LANDID IN ('BY') AND ORG_KOD = 'FIL' AND SUBSTR(shopid, 3, 1) = 'F' 
and shopid in (
    select distinct b.shopparent from bep_prem_average_pair_ranges a 
    inner join st_retail_hierarchy b on a.id_shop = b.shopid 
    where a.year =  2020  and a.month =  1 
)
ORDER BY shopid;

------------------------------------ 

select shopid, substr(shopnum,-5) shopnum from s_shop a;
select * from s_shop where 'S'||lpad('10', 5, '0') = shopnum;
select * from s_shop;

select shopid, shopnum from s_shop where '00010' = substr(shopnum,2,5);

call bep_calc_def_day_shop_plan(2020, 1, '0002');