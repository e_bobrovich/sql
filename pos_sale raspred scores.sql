select a.*, rownum from (select b.*,
                         a.discount_type_id,
                         a.discount_sum,
                         c.mtart, case when BEP_DK_MAX_SCORES_CHECK('1#lacit010-m','0000000000125')!=0 then
                         round_dig(LEAST(GREATEST(b.PRICE_SUM + nvl(a.DISCOUNT_SUM, 0) - b.PRICE * 0.5, 0),(
                CASE WHEN get_system_val('DK_SCORES_WITH_DISCOUNT_PRICE') = 'T' THEN 

                          (case when 1 = 1 
                                    and ACTION_PRICELIST(b.art, SYSDATE, b.procent) = 'T' -- проверка на оранжевый ценник
                                    and (baggins(b.art) = 'T' or substr(c.prodh, 0, 4) = '0001') -- проверка на сумку или обувь
                          then
                                kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                                  where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!= get_dk_sc_sys_str( 'DOCID'))
                                                  ,0))/100*get_dk_orange_discount('0000000000125'))
                          when action_pricelist(b.art,get_order_date(b.enet_posnr)) = 'F' and 
                               IS_ORDER_WITH_DISC(b.enet_posnr,discount_type_id) = 'F'
                          then
                              case when  (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null ) -- проверка на сопутку
                              then kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                                        where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!= get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                                        get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
                              else kol * ((price-nvl((select sum(discount_sum) from d_kassa_disc_cur t1 
                                                        where t1.posnr = b.posnr  and t1.kass_id = b.kass_id and t1.discount_type_id!= get_dk_sc_sys_str( 'DOCID')),0))/100*
                                                                                                                        get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
                              end 
                          end)
                    ELSE
                            (case when ACTION_PRICELIST(b.art, SYSDATE, b.procent) = 'T' -- проверка на оранжевый ценник
                         then kol * (price/100*get_dk_orange_discount('0000000000125'))
                         else
                            case when   (get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null )
                            then kol * (price/100*get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'SOP')) 
                            else kol * (price/100*get_dk_sc_sys_int( case when 'F' = 'T' then 'USE_SCR_SELLER' else 'USE_SCR' end, 'OB')) 
                            end
                         end) 
                    END)),get_dk_sc_sys_int( 'SCORES_ROUND'),0) / (BEP_DK_MAX_SCORES_CHECK('1#lacit010-m', '0000000000125') / 100)  else 0 end scores_proc
                    from d_kassa_cur b
                    left join d_kassa_disc_cur a
                      on a.posnr = b.posnr
                     and a.kass_id = b.kass_id
                    left join s_art c
                      on b.art = c.art
                  left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id in ('ZK23','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1)) ) d on d.docid=discount_type_id
                  where (nvl(discount_type_id, ' ') = get_dk_sc_sys_str( 'DOCID') or (get_dk_bag_as_sop(b.art, 'F') = 'T'   and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
                                                                                                                                            from d_kassa_disc_cur a
                                                                                                                                            inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                                                            where trunc (sysdate) between b.dates and b.datee
                                                                                                                                            and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                                                    ))))
                     and b.kass_id = '1#lacit010-m' 
                     and b.procent = 0
                     and (action_pricelist(b.art,sysdate,b.procent) in ('F', 'T') or 'F' = 'T') order by kol) a
                   order by rownum desc;
                   
                   
                   
                   
                   
select a.*, rownum from (select b.*,
 a.discount_type_id,
 a.discount_sum,
 c.mtart
from d_kassa_cur b
left join d_kassa_disc_cur a
on a.posnr = b.posnr
and a.kass_id = b.kass_id
left join s_art c
on b.art = c.art
left join  (select x.docid from pos_skidki1 x where trunc(sysdate) between x.dates and x.datee and (x.disc_type_id in ('ZK23','ZK10','ZK25','ZK33','ZK34','ZK35') or (x.disc_type_id in ('ZK29') and 1 = 1)) ) d on d.docid=discount_type_id
where (nvl(discount_type_id, ' ') = get_dk_sc_sys_str( 'DOCID') 
--or (get_dk_bag_as_sop(b.art, 'F') = 'T'   and d.docid is null and not (1 = 2 and b.posnr in (select posnr 
--                                                                                                from d_kassa_disc_cur a
--                                                                                                inner join pos_skidki1 b on a.discount_type_id = b.docid
--                                                                                                where trunc (sysdate) between b.dates and b.datee
--                                                                                                and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
--                                                                                        )))
or ((get_dk_bag_as_sop(b.art, 'F') = 'T'  and d.docid is null and 1 = 2 and b.posnr not in (select posnr 
                                                                                                            from d_kassa_disc_cur a
                                                                                                            inner join pos_skidki1 b on a.discount_type_id = b.docid
                                                                                                            where trunc (sysdate) between b.dates and b.datee
                                                                                                            and b.disc_type_id = 'ZK25' and a.kass_id = '1#lacit010-m'
                                                                                                            )
) or (1 = 1 and discount_type_id = get_dk_sc_sys_str( 'DOCID')  ))
)
and b.kass_id = '1#lacit010-m' 
and b.procent = 0
and (action_pricelist(b.art,sysdate,b.procent) in ('F', 'T') or 'F' = 'T') order by kol) a
order by rownum desc;