SELECT seller_tab, max(seller_fio), shop_id as id_shop, year, month, group_t, cast(sum(sump-sumv) as number(18,2)) as fact_sum, 0, 0, 0, 0
FROM (
    SELECT 
    AA."DATED"
    ,AA."YEAR"
    ,AA."MONTH"
    ,AA."VOP"
    ,AA."KOP"
    ,AA."KOEF"
    ,AA."SHOP_ID"
    ,AA."SELLER_FIO"
    ,AA."SELLER_TAB"
    ,AA."SUMP1"
    ,AA."SUMV1"
    ,AA."SUMP"
    ,AA."SUMV"
    ,AA."KOLP"
    ,AA."KOLV"
    ,AA."ART"
    ,case when c.SEASON = ' ' then 'Сопутка' else 'Обувь' end "GROUP_T"
    
    FROM (
        SELECT 
        TRUNC(A.SALE_DATE) AS DATED
        ,EXTRACT(YEAR FROM A.SALE_DATE) as year
        ,EXTRACT(MONTH FROM A.SALE_DATE) as month
        ,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN 1 ELSE 2 END AS KOP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  1 ELSE -1 END AS KOEF 
        ,A.ID_CHEK
        ,A.SHOP_ID
        ,A.SELLER_FIO
        ,A.SELLER_TAB
        ,B.ART
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
        ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
        FROM POS_SALE2 B
        INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
        WHERE A.BIT_CLOSE = 'T'
        and A.SHOP_ID = COALESCE('0004', A.SHOP_ID)
        and EXTRACT(YEAR FROM a.sale_date) = COALESCE(2018, EXTRACT(YEAR FROM a.sale_date))
        and EXTRACT(MONTH FROM A.SALE_DATE) = COALESCE(8, EXTRACT(MONTH FROM A.SALE_DATE))
        
        UNION ALL
        
        SELECT 
                COALESCE(C2.DATES, A2.dated) as dated
                ,EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)) as year
                ,EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) as month
                ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
                ,3 AS KOP
                ,-1 AS KOEF
                ,A2.ID AS ID_CHEK
                ,A2.ID_SHOP
                ,B2.SELLER_FIO
                ,B2.SELLER_TAB
                ,B2.ART
                ,0 AS SUMP1
                ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
                , CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
                , CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
                , 0  AS KOLP 
                , B2.KOL AS KOLV
                FROM D_PRIXOD2 B2
                INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
                --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
                LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                                SELECT p3.ID, p3.ID_SHOP, TRUNC(MAX(p3.DATE_S)) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = COALESCE('0004', p3.ID_SHOP) GROUP BY p3.ID, p3.ID_SHOP
                                UNION ALL
                                SELECT ID_PRIXOD AS ID, ID_SHOP, TRUNC(MAX(DATE_S)) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP = COALESCE('0004', ID_SHOP) GROUP BY ID_PRIXOD, ID, ID_SHOP) WHERE ID != 0 GROUP BY ID, ID_SHOP
                        ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
                LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = COALESCE('0004', c.ID_SHOP) WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
                WHERE
                ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19','42')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in ('03','14','19','42'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.DATED) IS NOT NULL)))
                AND A2.BIT_CLOSE='T' 
                and A2.ID_SHOP = COALESCE('0004', A2.ID_SHOP)
                and EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)) = COALESCE(2018, EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)))
                and EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) = COALESCE(8, EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)))
        
        
        UNION ALL
        
        select 
        a.DATED
        ,EXTRACT(YEAR FROM a.dated) as year
        ,EXTRACT(MONTH FROM a.dated) as month
        ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
        ,4 AS KOP
        ,1 AS KOEF
        ,a.ID AS ID_CHEK
        ,b.id_shop
        ,b.SELLER_FIO
        ,b.seller_tab
        ,b.art ART
        ,0 AS SUMP1
        ,0 AS SUMV1 
        ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
        ,0 AS SUMV 
        ,0  AS KOLP
        , 0 AS KOLV
        from d_rasxod1 a
        inner join (
                select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2
                from (
                    select id, art, seller_fio, seller_tab, id_shop, sum(kol*cena3) sum from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop
                ) x
                left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
                ) y on x.id = y.id and x.id_shop = y.id_shop
        ) b on a.id = b.id and a.id_shop = b.id_shop
        
        where a.bit_close = 'T' and a.idop in ('34','35','37','38','39','40')
        and b.id_shop = COALESCE('0004', b.id_shop) 
        and EXTRACT(YEAR FROM a.dated) = COALESCE(2018, EXTRACT(YEAR FROM a.dated))
        and TO_CHAR(a.dated, 'mm') = COALESCE(to_char(8, 'FM09'), TO_CHAR(a.dated, 'mm'))
    ) AA
    LEFT JOIN S_ART C ON AA.ART=C.ART  
)
group by seller_tab, shop_id, year,month, group_t;
--------------------------------------------------------------------------------

SELECT 
A.SALE_DATE AS DATED
,EXTRACT(YEAR FROM A.SALE_DATE) as year
,EXTRACT(MONTH FROM A.SALE_DATE) as month
,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
,CASE WHEN A.BIT_VOZVR='F' THEN 1 ELSE 2 END AS KOP
,CASE WHEN A.BIT_VOZVR='F' THEN  1 ELSE -1 END AS KOEF 
,A.ID_CHEK
,A.SHOP_ID
,A.SELLER_FIO
,A.SELLER_TAB
,B.ART
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
FROM POS_SALE2 B
INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
WHERE A.BIT_CLOSE = 'T'
and A.SHOP_ID = COALESCE('0004', A.SHOP_ID)
and EXTRACT(YEAR FROM a.sale_date) = COALESCE(2018, EXTRACT(YEAR FROM a.sale_date))
and EXTRACT(MONTH FROM A.SALE_DATE) = COALESCE(8, EXTRACT(MONTH FROM A.SALE_DATE))

and A.SELLER_FIO = 'Гоосен Юлия Юрьевна'
and A.ID_CHEK not in (
    select v.id_chek 
    FROM POS_SALEA_V_PREM@s0004 v
    WHERE v.DATED>=to_date('01.08.2018','dd.MM.yyyy') 
    AND v.DATED<=to_date('31.08.2018','dd.MM.yyyy') AND v.BIT_CLOSE='T' 
    and v.SELLER_FIO = 'Гоосен Юлия Юрьевна'
    and v.vop ='ПРОДАЖА'
)
;

select * from pos_users@s0003;
select * from s_seller@s0059 where tabno = '00022542';
select * from s_seller@s0008 where tabno like '%6317';

select * from s_seller@s0008 where fio like '%Рудлевская Оксана Владимировна%';

select * from s_seller where tabno like '%00006019';
select * from s_seller where fio like '%Рудлевская Оксана Владимировна%';
--------------------------------------------------------------------------------  
    
SELECT 
COALESCE(C2.DATES, A2.dated) as dated
,EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)) as year
,EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) as month
,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
,3 AS KOP
,-1 AS KOEF
,A2.ID AS ID_CHEK
,A2.ID_SHOP
,B2.SELLER_FIO
,B2.SELLER_TAB
,B2.ART
,0 AS SUMP1
,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
, CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
, CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
, 0  AS KOLP 
, B2.KOL AS KOLV
FROM D_PRIXOD2 B2
INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
--INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                SELECT p3.ID, p3.ID_SHOP, TRUNC(MAX(p3.DATE_S)) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = COALESCE('0061', p3.ID_SHOP) GROUP BY p3.ID, p3.ID_SHOP
                UNION ALL
                SELECT ID_PRIXOD AS ID, ID_SHOP, TRUNC(MAX(DATE_S)) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP = COALESCE('0068', ID_SHOP) GROUP BY ID_PRIXOD, ID, ID_SHOP) WHERE ID != 0 GROUP BY ID, ID_SHOP
        ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = COALESCE('0061', c.ID_SHOP) WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
WHERE
((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19','42')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in ('03','14','19','42'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.DATED) IS NOT NULL)))
AND A2.BIT_CLOSE='T' 
and A2.ID_SHOP = COALESCE('0061', A2.ID_SHOP)
and EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)) = COALESCE(2018, EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)))
and EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) = COALESCE(8, EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)))

and B2.SELLER_FIO = 'Лабоцкая Юлия Дмитриевна'
--and a2.id not in (
--    select v.id_chek 
--    FROM POS_SALEA_V_PREM@s0068 v
--    WHERE v.DATED>=to_date('01.08.2018','dd.MM.yyyy') 
--    AND v.DATED<=to_date('31.08.2018','dd.MM.yyyy') AND v.BIT_CLOSE='T' 
--    and v.SELLER_FIO = 'Качан Дарья Викторовна'
--)
;
-------------------------------------------------------------------------------- 

select 
a.DATED
,EXTRACT(YEAR FROM A.DATED)  as year
,EXTRACT(MONTH FROM A.DATED) as month
,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
,4 AS KOP
,1 AS KOEF
,a.ID AS ID_CHEK
,b.id_shop
,b.SELLER_FIO
,b.seller_tab
,b.art ART
,0 AS SUMP1
,0 AS SUMV1 
,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
,0 AS SUMV 
,0  AS KOLP
, 0 AS KOLV
from d_rasxod1 a
inner join (
        select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2
        from (
            select id, art, seller_fio, seller_tab, id_shop, sum(kol*cena3) sum from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop
        ) x
        left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
        ) y on x.id = y.id and x.id_shop = y.id_shop
) b on a.id = b.id and a.id_shop = b.id_shop

where a.bit_close = 'T' and a.idop in ('34','35','37','38','39','40')
and b.id_shop = COALESCE('0004', b.id_shop) 
and EXTRACT(YEAR FROM a.dated) = COALESCE(2018, EXTRACT(YEAR FROM a.dated))
and TO_CHAR(a.dated, 'mm') = COALESCE(to_char(8, 'FM09'), TO_CHAR(a.dated, 'mm'))

--and B.SELLER_TAB = '00000822'
;
--------------------------------------------------------------------------------                
SELECT MAX(IDOP),MAX(DATED) FROM D_PRIXOD1 WHERE ID=768;    
SELECT MAX(TIP) FROM ST_OP WHERE IDOP=14 AND (NUMCONF=2 OR NUMCONF=0);

SELECT MAX(NUMCONF) FROM CONFIG@s4306;

select FIRM_DEV.BEP_GET_VOZVRAT(797, '3103') from dual;


select *
FROM POS_SALEA_V_PREM@s0068 v
WHERE extract(month from v.DATED) = 8 AND v.BIT_CLOSE='T' 
and v.SELLER_FIO = 'Мороз Анжела Геннадьевна' and
v.id_chek not in (
SELECT 
a.id_chek
FROM POS_SALE2 B
INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
WHERE A.BIT_CLOSE = 'T'
and A.SHOP_ID = COALESCE('0068', A.SHOP_ID)
and EXTRACT(YEAR FROM a.sale_date) = COALESCE(2018, EXTRACT(YEAR FROM a.sale_date))
and EXTRACT(MONTH FROM A.SALE_DATE) = COALESCE(8, EXTRACT(MONTH FROM A.SALE_DATE))

and A.SELLER_FIO = 'Мороз Анжела Геннадьевна'
)
;