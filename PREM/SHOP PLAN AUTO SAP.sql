select * from bep_prem_shop_plan where id_shop = '0001' and year = 2019 and month = 8;
select * from bep_prem_day_shop_plan;
select * from bep_prem_day_shop_plan_auto;
select * from BEP_PREM_shop_plan_test;

select * from st_shop where shopid = '0053';

select distinct id_shop from BEP_PREM_shop_plan_test where year = 2019 and month = 8;
select distinct id_shop from bep_prem_day_shop_plan_auto where year = 2019 and month = 8;

select * from (
    select distinct id_shop from BEP_PREM_shop_plan_test where year = 2019 and month = 8
) a full join (
    select distinct id_shop from BEP_PREM_shop_plan where year = 2019 and month = 8
) b on a.id_shop = b.id_shop
where a.id_shop is null or b.id_shop is null;


select * from (
    select distinct id_shop from BEP_PREM_shop_plan_test where year = 2019 and month = 8
) a full join (
    select distinct id_shop from bep_prem_day_shop_plan_auto where year = 2019 and month = 8
) b on a.id_shop = b.id_shop
where a.id_shop is null or b.id_shop is null;

select 
--*
sum("/BIC/ZAMOUNTDC") plan_to,
sum("/BIC/ZQTY") plan_pair 
from userbw."/BIC/OHZBW_PD" 
where "/BIC/ZVERSION" = 'OPLAN01' 
and plant = '7001' 
--and "/BIC/ZKPIIND" = 'KPI000020' 
--and "/BIC/ZKPIIND" = 'KPI000008' 
and calyear = 2019 
and calmonth2 = lpad(8, 2, '0') 
--and calday = to_char(sysdate, 'yyyymmdd')

;

select id_shop, plan_date, plan_day, plan_month, plan_year, sum(day_plan_sum) day_plan_sum, sum(day_plan_pair) day_plan_pair from (
    select 
    case when substr(plant,1,2) = '70' then '00'||substr(plant,3,2) else plant end id_shop,
    to_date(calday, 'yyyymmdd') plan_date,
    extract(day from to_date(calday, 'yyyymmdd')) plan_day,
    extract(month from to_date(calday, 'yyyymmdd')) plan_month,
    extract(year from to_date(calday, 'yyyymmdd')) plan_year,
    sum("/BIC/ZAMOUNTDC") day_plan_sum, 
    0 day_plan_pair
    from userbw."/BIC/OHZBW_PD" 
    where "/BIC/ZVERSION" = 'OPLAN01' 
    and plant = '7001' 
    and "/BIC/ZKPIIND" = 'KPI000020'
    and  (extract(year from sysdate)*12+extract(month from sysdate))-(extract(year from to_date(calday, 'yyyymmdd'))*12+extract(month from to_date(calday, 'yyyymmdd'))) <= 2
    group by plant, calyear, calmonth, calday
    
    union all
    
    select 
    case when substr(plant,1,2) = '70' then '00'||substr(plant,3,2) else plant end id_shop,
    to_date(calday, 'yyyymmdd') plan_date,
    extract(day from to_date(calday, 'yyyymmdd')) plan_day,
    extract(month from to_date(calday, 'yyyymmdd')) plan_month,
    extract(year from to_date(calday, 'yyyymmdd')) plan_year,
    0 day_plan_sum, 
    sum("/BIC/ZQTY") day_plan_pair
    from userbw."/BIC/OHZBW_PD" 
    where "/BIC/ZVERSION" = 'OPLAN01' 
    and plant = '7001' 
    and "/BIC/ZKPIIND" = 'KPI000008' 
    and  (extract(year from sysdate)*12+extract(month from sysdate))-(extract(year from to_date(calday, 'yyyymmdd'))*12+extract(month from to_date(calday, 'yyyymmdd'))) <= 2
    group by plant, calyear, calmonth, calday
) group by id_shop, plan_date, plan_day, plan_month, plan_year
order by id_shop, plan_date
;


select id_shop, plan_date, plan_day, plan_month, plan_year, sum(day_plan_sum) day_plan_sum, sum(day_plan_pair) day_plan_pair from (
    select 
    case when substr(plant,1,2) = '70' then '00'||substr(plant,3,2) else plant end id_shop,
    to_date(calday, 'yyyymmdd') plan_date,
    extract(day from to_date(calday, 'yyyymmdd')) plan_day,
    extract(month from to_date(calday, 'yyyymmdd')) plan_month,
    extract(year from to_date(calday, 'yyyymmdd')) plan_year,
    sum("/BIC/ZAMOUNTDC") day_plan_sum, 
    sum("/BIC/ZQTY") day_plan_pair
    from userbw."/BIC/OHZBW_PD" 
    where "/BIC/ZVERSION" = 'OPLAN01' 
    and plant = '7001' 
    and  (extract(year from sysdate)*12+extract(month from sysdate))-(extract(year from to_date(calday, 'yyyymmdd'))*12+extract(month from to_date(calday, 'yyyymmdd'))) <= 2
    group by plant, calyear, calmonth, calday
) group by id_shop, plan_date, plan_day, plan_month, plan_year
order by id_shop, plan_date
;

select (extract(year from sysdate)*12+extract(month from sysdate)), (2018*12+12), (extract(year from sysdate)*12+extract(month from sysdate))-(2018*12+12) res from dual;

select id_shop, year, month, sum(plan_sum) plan_sum, 0, sum(plan_pair) plan_pair, 0 from bep_prem_day_shop_plan_auto
where id_shop = case when '0001' is null then id_shop else '0001' end
and year = case when 2019 is null then year else 2019 end
and month = case when 5 is null then month else 5 end
and 2 <= 
    case when 2019 is null and 5 is null then 
        (extract(year from sysdate)*12+extract(month from sysdate)) - (year*12+month)
    else 2 end
group by id_shop, year, month
;