--    insert into bep_prem_shop_plan/*bep_prem_shop_plan_test*/ (id_shop, year, month, plan_sum, fact_sum, plan_pair, fact_pair)
    select id_shop, year, month, sum(plan_sum) plan_sum, 0, sum(plan_pair) plan_pair, 0 from bep_prem_day_shop_plan_auto
    where id_shop = case when /*i_id_shop*/ '0001' is null then id_shop else /*i_id_shop*/ '0001' end
    and year = case when /*i_year*/ 2020 is null then year else /*i_year*/ 2020 end
    and month = case when /*i_month*/ 3 is null then month else /*i_month*/ 3 end
    and /*v_month_count*/ 0 >= 
        case when /*i_year*/ 2020 is null and /*i_month*/ 3 is null then 
            (extract(year from sysdate)*12+extract(month from sysdate)) - (year*12+month)
        else /*v_month_count*/ 0 end
    group by id_shop, year, month;
    


select id_shop, year, month, max(plan_sum) plan_sum, max(plan_pair) plan_pair,
case when max(plan_sum) = 0 and max(plan_pair) = 0 then 'T' else 'F' end is_delete 
from (
  select id_shop, year, month, sum(plan_sum) plan_sum, sum(plan_pair) plan_pair, 'F' is_delete 
  from bep_prem_day_shop_plan_auto
  where id_shop = case when /*i_id_shop*/ '0001' is null then id_shop else /*i_id_shop*/ '0001' end
  and year = case when /*i_year*/ 2020 is null then year else /*i_year*/ 2020 end
  and month = case when /*i_month*/ 3 is null then month else /*i_month*/ 3 end
  and /*v_month_count*/ 0 >= 
      case when /*i_year*/ 2020 is null and /*i_month*/ 3 is null then 
          (extract(year from sysdate)*12+extract(month from sysdate)) - (year*12+month)
      else /*v_month_count*/ 0 end
  group by id_shop, year, month
  
  union all 
  
  select '0001' id_Shop, 2020 year, 3 month, 0 plan_sum, 0 plan_pair, 'T' is_delete from dual
)group by id_shop, year, month
;

    
select * from bep_prem_shop_plan where year = 2020 and month = 3 and id_Shop = '0001';

merge into bep_prem_shop_plan a using (
  select id_shop, year, month, max(plan_sum) plan_sum, max(plan_pair) plan_pair,
  case when max(plan_sum) = 0 and max(plan_pair) = 0 then 'T' else 'F' end is_delete 
  from (
    select id_shop, year, month, sum(plan_sum) plan_sum, sum(plan_pair) plan_pair, 'F' is_delete 
    from bep_prem_day_shop_plan_auto
    where id_shop = case when /*i_id_shop*/ '0001' is null then id_shop else /*i_id_shop*/ '0001' end
    and year = case when /*i_year*/ 2021 is null then year else /*i_year*/ 2021 end
    and month = case when /*i_month*/ 3 is null then month else /*i_month*/ 3 end
    and /*v_month_count*/ 0 >= 
        case when /*i_year*/ 2020 is null and /*i_month*/ 3 is null then 
            (extract(year from sysdate)*12+extract(month from sysdate)) - (year*12+month)
        else /*v_month_count*/ 0 end
    group by id_shop, year, month
    union all 
    select /*i_id_shop*/ '0001' id_Shop, /*i_year*/2020 year, /*i_month*/ 3 month, 0 plan_sum, 0 plan_pair, 'T' is_delete from dual
  )group by id_shop, year, month
) b on (a.id_shop = b.id_shop and a.year = b.year and a.month = b.month)
when matched
  then update set a.plan_sum = b.plan_sum, a.plan_pair = b.plan_pair
       delete where b.is_delete = 'T'
when not matched 
  then insert (a.id_shop, a.year, a.month, a.plan_sum, a.fact_sum, a.plan_pair, a.fact_pair)
       values (b.id_shop, b.year, b.month, b.plan_sum, 0, b.plan_pair, 0)
;