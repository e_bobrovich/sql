select * from bep_prem_s_stock;
select * from BEP_PREM_TMP_STOCK;
select * from BEP_PREM_MONTH_STOCK;

select distinct
   sid
from v$mystat;

select 
 case when s1.art is not null then 'СТОК' else 'НЕ СТОК' end is_stock_new, 
 AA."DATED"
,AA."YEAR"
,AA."MONTH"
,AA."SHOP_ID"
,AA."VOP"
,lpad(AA.SELLER_TAB, 8, '0') "SELLER_TAB"
,AA."SELLER_FIO"
,cast((AA."SUMP"-AA."SUMV") as number(18,2)) as fact_sale_sum
,case when c.mtart not in ('ZROH','ZHW3') then cast((KOLP-KOLV) as number(18)) else 0 end as FACT_PAIR_KOL
--        ,AA."PROCENT"
,AA."ART"
, case when c.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутка' end as "SH_SOP"
, case when c.facture = ' ' and c.manufactor != 'СООО БЕЛВЕСТ' then 'Покупная'
       when c.facture = ' ' and c.manufactor = 'СООО БЕЛВЕСТ' then 'Собственная' 
       else c.facture 
  end as "OWN_PURCHASE"
, c.manufactor as "BRAND"
, case when "ACTION" = 'F' then 'Белый' else 'Оранжевый' end as "CENNIK" --CHANGE AFTER
, c.assort_torg as "ASSORT_TORG"
--        , c.assort as "ASSORT"
, c.groupmw as "GROUPMW"
, c.season "SEASON"
, case when s.art is not null then 'Сток' else 'Не сток' end "IS_STOCK"
, case when c.manufactor in ('ADIDAS INTERNATIONAL TRADING','LOTTO SPORT ITALIA SPA','PUMA SE RUDOLF DASSLER SPORT','SKECHERS SARL') then 'Спорт' else 'Не спорт' end "IS_SPORT"
, case when c.manufactor in ('ООО ФИДО') then 'ФИДО' else 'Не ФИДО' end "IS_FIDO"
, case when c.groupmw not in ('Мужские', 'Женские', 'N/A', ' ') then 'Десткие' else 'Не детские' end "IS_KIDS"
, m.mat_upper "MAT_UPPER"
, case when m.mat_upper in (
    'Н.Вел/Н.Кож с Во.Пок','Н.Вел/Н.Кожа Воск.от','Н.Кож Вел/Н.кож Нуб',
    'НатВелюр/НатКожа','НатВелюр/НатЛак','НатВелюр/НатНуб В.от',
    'НатКожа Велюр','НатКожа Велюр/Текст','НатКожа Спилок'
  ) then 'Ворсовая кожа (велюр\спилок)' else 'Не ворсовая кожа (велюр\спилок)' end "IS_VORS_LEATHER"
,AA."PROCENT"
, c.assort as "ASSORT"
, case when AA."PROCENT" > 0 then 'Некондиция' else 'Не некондиия' end "IS_NECOND"
, case when sum(case when c.mtart not in ('ZROH','ZHW3') then 1 else 0 end) over (partition by aa.shop_id, aa.id_chek) > 0 then 'Обувь в чеке' else 'Без обуви в чеке' end IS_SH_CHEK
, case when SA.ART is not null then 'На скидке' else 'Не на скидке' end "IS_SKIDKA"
FROM (
    SELECT 
    TRUNC(A.SALE_DATE) AS DATED
    ,EXTRACT(YEAR FROM A.SALE_DATE) as year
    ,EXTRACT(MONTH FROM A.SALE_DATE) as month
    ,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
    ,CASE WHEN A.BIT_VOZVR='F' THEN 1 ELSE 2 END AS KOP
    ,CASE WHEN A.BIT_VOZVR='F' THEN  1 ELSE -1 END AS KOEF 
    ,A.ID_CHEK
    ,A.SHOP_ID
    ,A.SELLER_FIO
    ,A.SELLER_TAB
    ,B.ART
    ,B.asize
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
    ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
    ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
    ,B.ACTION
    ,B.PROCENT
    FROM POS_SALE2 B
    INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
    WHERE A.BIT_CLOSE = 'T'
    and A.SHOP_ID = COALESCE('2101', A.SHOP_ID)
    and EXTRACT(YEAR FROM a.sale_date) = COALESCE(2020, EXTRACT(YEAR FROM a.sale_date))
    and EXTRACT(MONTH FROM A.SALE_DATE) = COALESCE(1, EXTRACT(MONTH FROM A.SALE_DATE))

    UNION ALL

    SELECT 
    (case when a2.idop in ('14') then c2.dates else a2.dated end) as dated
    ,EXTRACT(YEAR FROM (case when a2.idop in ('14') then c2.dates else a2.dated end)) as year
    ,EXTRACT(MONTH FROM (case when a2.idop in ('14') then c2.dates else a2.dated end)) as month
    ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
    ,3 AS KOP
    ,-1 AS KOEF
    ,A2.ID AS ID_CHEK
    ,A2.ID_SHOP
    ,B2.SELLER_FIO
    ,B2.SELLER_TAB
    ,B2.ART
    ,B2.asize
    ,0 AS SUMP1
    ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
    ,CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
    ,CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
    ,0  AS KOLP 
    ,B2.KOL AS KOLV
    ,B2.ACTION
    ,B2.PROCENT
    FROM D_PRIXOD2 B2
    INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
    --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
    LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                    SELECT p3.ID, p3.ID_SHOP, TRUNC(MAX(p3.DATE_S)) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = COALESCE('2101', p3.ID_SHOP) GROUP BY p3.ID, p3.ID_SHOP
                    --UNION ALL
                    --SELECT ID_PRIXOD AS ID, ID_SHOP, TRUNC(MAX(DATE_S)) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP = COALESCE('2101', ID_SHOP) GROUP BY ID_PRIXOD, ID, ID_SHOP
                    ) WHERE ID != 0 GROUP BY ID, ID_SHOP
            ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
    LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = COALESCE('2101', c.ID_SHOP) WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
    where
    ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19',/*'42',*/'45', '55')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in (/*21.11.2019 Цыбин не считать 20*/'20','03','14','19',/*'42',*/'45', '55'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.dated) IS NOT NULL)))
    AND A2.BIT_CLOSE='T' 
    and A2.ID_SHOP = COALESCE('2101', A2.ID_SHOP)
--            and EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)) = COALESCE(2020, EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)))
--            and EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) = COALESCE(1, EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)))
    and extract(year from case when a2.idop in ('14') then c2.dates else a2.dated end) = 2020
    and EXTRACT(MONTH FROM case when A2.IDOP in ('14') then C2.DATES else a2.dated end) = 1
    UNION ALL

    select 
    a.DATED
    ,EXTRACT(YEAR FROM a.dated) as year
    ,EXTRACT(MONTH FROM a.dated) as month
    ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
    ,4 AS KOP
    ,1 AS KOEF
    ,a.ID AS ID_CHEK
    ,b.id_shop
    ,b.SELLER_FIO
    ,b.seller_tab
    ,b.art ART
    ,b.asize
    ,0 AS SUMP1
    ,0 AS SUMV1 
    ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
    ,0 AS SUMV 
    ,b.kol AS KOLP
    ,0 AS KOLV
    ,b.ACTION
    ,B.PROCENT
    from d_rasxod1 a
    inner join (
            select x.id, x.art, x.asize, x.seller_fio, seller_tab, x.id_shop, x.action, x.procent, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2, nvl(x.kol,0) kol
            from (
                select id, art, asize, seller_fio, seller_tab, id_shop, action, procent, sum(kol*cena3) sum, sum(kol) kol from d_rasxod2 group by id, art, asize, seller_fio, seller_tab, id_shop, action, procent
            ) x
            left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
            ) y on x.id = y.id and x.id_shop = y.id_shop
    ) b on a.id = b.id and a.id_shop = b.id_shop
    
    where a.bit_close = 'T' and a.idop in ('34','35',/*'37',*/'38','39','40', '44')
    and b.id_shop = COALESCE('2101', b.id_shop)
    and EXTRACT(YEAR FROM a.dated) = COALESCE(2020, EXTRACT(YEAR FROM a.dated))
    and EXTRACT(MONTH FROM a.dated) = COALESCE(1, EXTRACT(MONTH FROM a.dated))
) AA
LEFT JOIN S_ART C ON AA.ART=C.ART  
left join (select distinct shopid id_shop, priceid priceid2, case when landid = 'BY' then landid else priceid end priceid, landid from st_shop where org_kod = 'SHP') sh on aa.shop_id = sh.id_shop 
left join (select art, asize, status_art from s_all_mat where status_art = 'S' group by art, asize, status_art) s on aa.art = s.art and aa.asize = s.asize
left join (select art, asize, mat_upper from s_all_mat group by art, asize, mat_upper) m on aa.art = m.art and aa.asize = m.asize
left join (select art, dates, case when reg = 'BY' then reg else vidid end vidid, reg from BEP_PREM_TMP_STOCK) s1 on sh.priceid = s1.vidid and aa.art = s1.art and trunc(aa.dated) = trunc(s1.dates) 
left join bep_prem_criterion_art_list sa 
  on sa.priceid = sh.priceid
  and aa.art = sa.art and trunc(aa.dated) >= trunc(dateb) and trunc(aa.dated) <= trunc(datee)   
;

select * from (
  select distinct shopid id_shop, priceid priceid2, case when landid = 'BY' then landid else priceid end priceid, landid from st_shop where org_kod = 'SHP'
) a
inner join (
  select art, dates, case when reg = 'BY' then reg else vidid end vidid, reg from BEP_PREM_TMP_STOCK
) b on --a.priceid = b.vidid and a.landid = case when a.priceid = '01' then b.reg else a.landid end 
a.priceid = b.vidid
and b.dates = to_date('20200115', 'yyyymmdd') --and b.art = 'B0019-1-12'
;

SET serveroutput ON
DECLARE
  v_date date := to_date('20200101','yyyymmdd');
  v_prev_day varchar2(1) := 'F';
  i_year integer := 2020;
  i_month integer := 1;
  v_count integer := 0;
BEGIN
  delete from BEP_PREM_TMP_STOCK 
  where extract(year from dates) = i_year
    and extract(month from dates) = i_month;

  v_date := to_date(lpad(i_year, 4, '0')||lpad(i_month, 2, '0')||'01','yyyymmdd');
  while (extract(month from v_date) = i_month and extract(year from v_date) = i_year and v_date <= sysdate)
  loop
    dbms_output.put_line(v_date);
    for r_cur_stock in (
        select distinct a.art, v_date curr_date, s.priceid vidid, s.landid reg  from (
              select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
              ,trunc(a.dates) dates, trunc(a.datee) datee
              from firm.pos_skidki1 a
              left join firm.pos_skidki2 b on a.docid = b.docid
              inner join firm.pos_skidki3 c on a.docid = c.docid
              inner join table(sys.odcivarchar2list('2101','0031','4407')) d on c.shopid = d.column_value
              where trunc(v_date) between trunc(a.dates) and trunc(a.datee)  and a.disc_type_id in (/*'ZK01',*/'ZK02') 
        ) a
        inner join st_shop s on s.shopid = a.shopid
        where a.rn = 1
        and substr(to_char(a.cena), -2) = '99'
        
--        select distinct art, v_date date_cur, vidid, reg
--        from table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4407'), v_date))
--        where substr(to_char(cena), -2) = '99'
      )
    loop
      -- проверка наличия этого артикула в стоке на день до рассматриваемого
      insert into BEP_PREM_TMP_STOCK (art, dates, vidid, reg)
      values (r_cur_stock.art, v_date, r_cur_stock.vidid, r_cur_stock.reg);
    end loop;
    commit;   
    
    v_date := v_date + 1;
  end loop;
  commit;
               
exception when others then
  dbms_output.put_line(sqlcode||sqlerrm);
  rollback;   
END ;