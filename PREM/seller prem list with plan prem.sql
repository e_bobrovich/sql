select nvl(a.id_shop, b.id_shop) id_shop,
       c.tabno                   seller_tab,
       c.fio                     seller_fio,
       sum(nvl(a.month_prem, 0)) month_prem,
       sum(nvl(b.day_prem, 0))   day_prem
from (select id_shop, seller_tab, sum(prem) month_prem
      from bep_prem_month_sales
      where id_shop = '0001'
      group by id_shop, seller_tab) a
full join (select id_shop, seller_tab, sum(prem) day_prem
          from bep_prem_month_sales
          where id_shop = '0001'
            and trunc(dates) = trunc(sysdate)
          group by id_shop, seller_tab) b
         on a.id_shop = b.id_shop and a.seller_tab = b.seller_tab
inner join (select distinct tabno, fio
           from s_seller
           where to_char(dateb, 'yyyymm') <= to_char(sysdate, 'yyyymm')
             and to_char(dated, 'yyyymm') >= to_char(sysdate, 'yyyymm')) c
          on nvl(a.seller_tab, b.seller_tab) = c.tabno
group by nvl(a.id_shop, b.id_shop), c.tabno, c.fio;


select id_shop, seller_tab, sum(prem) month_prem,

SUM(prem) OVER(PARTITION BY id_shop) prem_part

from bep_prem_month_sales
where id_shop = '0001'
group by id_shop, seller_tab;


select nvl(a.id_shop, b.id_shop) id_shop,
       c.tabno                   seller_tab,
       c.fio                     seller_fio,
       sum(nvl(a.month_prem, 0)) month_prem,
       sum(nvl(b.day_prem, 0))   day_prem,
      bep_prem_get_plan_percent(
        extract(year from sysdate),
        extract(month from sysdate),
        '0001',
        100 * sum(sum(nvl(a.month_sum, 0))) over (partition by nvl(a.id_shop, b.id_shop)) / max(plan_sum)
      ) plan_prem
       
from (select id_shop, seller_tab, sum(prem) month_prem, sum(fact_sale_sum) month_sum -- премии за меся и сумма ТО
      from bep_prem_month_sales
      where id_shop = '0001'
      group by id_shop, seller_tab) a
full join (select id_shop, seller_tab, sum(prem) day_prem -- премии за день
          from bep_prem_month_sales
          where id_shop = '0001'
            and trunc(dates) = trunc(sysdate)
          group by id_shop, seller_tab) b
         on a.id_shop = b.id_shop and a.seller_tab = b.seller_tab
inner join (select distinct tabno, fio -- спсиок продавцов магазина за месяц
           from s_seller
           where to_char(dateb, 'yyyymm') <= to_char(sysdate, 'yyyymm')
             and to_char(dated, 'yyyymm') >= to_char(sysdate, 'yyyymm')) c
          on nvl(a.seller_tab, b.seller_tab) = c.tabno
inner join bep_prem_shop_plan p on '0001' = p.id_Shop and extract(year from sysdate) = p.year and  extract(month from sysdate) = p.month --  план на месяц
group by nvl(a.id_shop, b.id_shop), c.tabno, c.fio
order by seller_tab;





select a.id_shop,
sum(a.fact_sale_sum) * case 
  when 100 * sum(a.fact_sale_sum)/b.plan_sum >= 100 then max(c.percent_100)
  when 100 * sum(a.fact_sale_sum)/b.plan_sum >= 90 then max(c.percent_90)
  else 0
end plan_prem
from bep_prem_month_sales a
inner join bep_prem_shop_plan b on a.id_shop = b.id_Shop and 2020 = b.year and  3 = b.month
left join (
  select '0001' id_shop, year, month, percent_90, percent_100  from bep_prem_plan_percent 
  where month = 3 and year = 2020
  and region in (select region from bep_prem_filial_region a 
                  inner join st_retail_hierarchy b on a.filial = b.shopparent
                  where b.shopid = '0001'
      )
) c on b.id_shop = c.id_Shop and b.year = c.year and b.month = c.month
where a.id_shop = '0001'
group by a.id_shop, b.plan_sum;


select * from bep_prem_plan_percent;


select percent_90, percent_100  from bep_prem_plan_percent 
where month = 3 and year = 2020
and region in (select region from bep_prem_filial_region a 
                inner join st_retail_hierarchy b on a.filial = b.shopparent
                where b.shopid = '0001'
    )
;