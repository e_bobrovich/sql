select * from s_all_mat where art = '1914115';

select * from CENA_SKIDKI_ALL_SHOPS;

select a.id_shop, c.art, c.action, sa.art art_list from (
    select distinct(gs.id_shop) from bep_prem_group_shop gs
    inner join bep_prem_s_group sg on gs.id_group = sg.id_group
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
) a
inner join (select shopid , priceid, case when landid = 'BY' then landid else priceid end priceid_list, landid from st_shop where org_kod = 'SHP'
) b on a.id_shop = b.shopid
--inner join cena_skidki_all_shops c on b.priceid = c.vidid
inner join cena_skidki_all_shops c on b.priceid = c.vidid and b.landid = case when b.priceid = '01' then c.reg else b.landid end
left join bep_prem_criterion_art_list sa 
  on sa.priceid = b.priceid
  and c.art = sa.art and trunc(sysdate) >= trunc(dateb) and trunc(sysdate) <= trunc(datee);  
  
  
select shopid , priceid, case when landid = 'BY' then landid else priceid end priceid_list, landid from st_shop where org_kod = 'SHP';  
  
select art, to_date('20200101','yyyymmdd') datec, vidid, reg
from table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4407'), to_date('20200101','yyyymmdd')))
where substr(to_char(cena), -2) = '99'
;

select * from bep_prem_s_stock;

select count(*) from bep_prem_s_stock;

select * from bep_prem_s_stock where to_char(date_from, 'yyyymmdd') = '20200101' and to_char(date_to, 'yyyymmdd') = '20200101';
select * from bep_prem_s_stock where to_char(date_from, 'yyyymmdd') = '20200101' and to_char(date_to, 'yyyymmdd') = '20200102';
select * from bep_prem_s_stock where to_char(date_from, 'yyyymmdd') = '20200102' and to_char(date_to, 'yyyymmdd') = '20200102';

select * from bep_prem_s_stock where to_char(date_to, 'yyyymmdd') > '20200103';
select * from bep_prem_s_stock where to_char(date_from, 'yyyymmdd') > '20200103';

select vidid, reg, count(*) from bep_prem_s_stock group by vidid, reg order by vidid, reg;

merge into bep_prem_s_stock x
using (
  select distinct art, to_date('20200106','yyyymmdd') date_cur, vidid, reg
  from table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4407'), to_date('20200106','yyyymmdd')))
  where substr(to_char(cena), -2) = '99'
) y on (x.art = y.art and x.vidid = y.vidid and x.reg = y.reg and x.date_to = y.date_cur-1) 
when matched 
then
  update set x.date_to = case 
                          when x.date_to = y.date_cur-1 then y.date_cur 
                          else x.date_to 
                         end
when not matched 
then 
  insert (x.art, x.date_from, x.date_to, x.vidid, x.reg) 
  values (y.art, y.date_cur, y.date_cur, y.vidid, y.reg) 
;
  
  
insert into bep_prem_s_stock
select distinct art, to_date('20200101','yyyymmdd') date_from, to_date('20200101','yyyymmdd') date_to, vidid, reg
from table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4407'), to_date('20200101','yyyymmdd')))
where substr(to_char(cena), -2) = '99';


select distinct art, to_date('20200106','yyyymmdd') date_cur, vidid, reg
  from table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4407'), to_date('20200106','yyyymmdd')))
  where substr(to_char(cena), -2) = '99'
;

select case when count(*) = 0 then 'F' else 'T' end from bep_prem_s_stock a 
      where a.art = 'B0031-11'
        and a.vidid = '50' 
        and a.reg = 'BY'
        and a.date_to = to_date('20200106','yyyymmdd') - 1;
        
select count(*) 
      from bep_prem_s_stock
      where extract(year from date_from) = 2020
        and extract(month from date_from) = 1;