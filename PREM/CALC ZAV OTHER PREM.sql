select a.*, 100 * a.fact_sum/a.plan_sum progress, b.* from bep_prem_shop_plan a
left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                and a.fact_sum > b.min_sum
                                and a.fact_sum <= b.max_sum
where a.year = 2020 and a.month = 1
and a.id_shop = '0001'

;

select * from bep_Prem_seller a
where a.year = 2020 and a.month = 1
and a.id_shop = '0001'
;

select * from bep_prem_zav_percent;

select * from bep_prem_shop_plan where year = 2020 and month = 3 and id_Shop = '0001'; 

select * from BEP_PREM_DOP_ZAVMAG_PODSOB;


select 
  a.tabno, a.fio, b.shopid id_shop, nvl(c.fact_sum, 0) fact_sum, 
  case when c.seller_tab is null then 'F' else 'T' end is_prem,
  stext2
  case when lower(stext2) like '%подсобный рабочий%' then nvl(z.other_prem_percent,0)
       else 0
  end other_prem_percent,
  case when lower(stext2) like '%заведующий%' then nvl(z.zavmag_prem_percent,0) 
     else 0
  end zavmag_prem_percent
--  ,z.*
--,c.*,a.*
from s_seller a
left join s_shop b on a.orgno = substr(b.shopnum, 2, 5)
left join (
    select seller_tab, id_shop, year, month, sum(fact_sum) fact_sum  from bep_prem_seller  
    where year = 2020 and month = 1 and id_shop = '0004'
    group by seller_tab, id_shop, year, month
) c on a.tabno = c.seller_tab

left join (
  select a.id_shop, a.year, a.month, 
  b.prem_percent zavmag_prem_percent,
  case 
    when 100 * a.fact_sum/a.plan_sum <= 80 then 30
    else 50
  end other_prem_percent
  /*, a.*, 100 * a.fact_sum/a.plan_sum progress, b.*/ from bep_prem_shop_plan a
  left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                  and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                  and a.fact_sum > b.min_sum
                                  and a.fact_sum <= b.max_sum
  where a.year = 2020 and a.month = 1   and a.id_shop = '0004'
) z on c.id_shop = z.id_shop and c.year = z.year and c.month = z.month

where 
shopid = '0004' 
and (lower(stext2) like '%заведующий%' or lower(stext2) like '%подсобный рабочий%')
and ((extract(month from dateb) <=1 and extract(year from dateb) = 2020) or extract(year from dateb) < 2020)
and ((extract(month from dated) >= 1 and extract(year from dated) = 2020) or extract(year from dated) > 2020)
and ((a.prozt != '0' and a.prozt != ' ') or a.stat2 = '3')
order by stext2 desc, dated desc
;


select * from (
 select a.id_shop, a.year, a.month, 
  b.prem_percent zavmag_prem_percent,
  case 
    when 100 * a.fact_sum/a.plan_sum <= 80 then 30
    else 50
  end other_prem_percent
--  , a.*
  /*, a.*, 100 * a.fact_sum/a.plan_sum progress, b.*/ from bep_prem_shop_plan a
  left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                  and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                  and a.fact_sum > b.min_sum
                                  and a.fact_sum <= b.max_sum
  where a.year = 2020 and a.month = 1   and a.id_shop = '0004'
) x;

select * from (
  select 
     b.shopid id_shop, 2020 year, 1 month, a.tabno seller_tab, a.fio, stext2,
    case when lower(stext2) like '%подсобный рабочий%' then 'T' else 'F' end is_other,
    case when lower(stext2) like '%заведующий%' then 'T' else 'F' end is_zavmag
  from s_seller a
  left join s_shop b on a.orgno = substr(b.shopnum, 2, 5)
  where 
  shopid = '0004' 
  and (lower(stext2) like '%заведующий%' or lower(stext2) like '%подсобный рабочий%')
  and ((extract(month from dateb) <= 1 and extract(year from dateb) = 2020) or extract(year from dateb) < 2020)
  and ((extract(month from dated) >= 1 and extract(year from dated) = 2020) or extract(year from dated) > 2020)
  and ((a.prozt != '0' and a.prozt != ' ') or a.stat2 = '3')
  order by stext2 desc, dated desc
) y;

select 0/0 from dual;

select 
  x.id_shop, x.year, x.month,
  y.seller_tab, y.fio,
  case when is_zavmag = 'T' then zavmag_prem_percent else 0 end zavmag_prem_percent,
  case when is_other = 'T' then other_prem_percent else 0 end other_prem_percent
from (
 select a.id_shop, a.year, a.month, 
  nvl(b.prem_percent, 0) zavmag_prem_percent,
  case when 100 * a.fact_sum/a.plan_sum <= 80 then 30 else 50 end other_prem_percent
  from bep_prem_shop_plan a
  left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                  and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                  and a.fact_sum > b.min_sum
                                  and a.fact_sum <= b.max_sum
  where a.year = 2020 and a.month = 1 and a.id_shop = '0004'
) x
inner join (
  select 
     b.shopid id_shop, 2020 year, 1 month, a.tabno seller_tab, a.fio, stext2,
    case when lower(stext2) like '%подсобный рабочий%' then 'T' else 'F' end is_other,
    case when lower(stext2) like '%заведующий%' then 'T' else 'F' end is_zavmag
  from s_seller a
  left join s_shop b on a.orgno = substr(b.shopnum, 2, 5)
  where 
  shopid = '0004' 
  and (lower(stext2) like '%заведующий%' or lower(stext2) like '%подсобный рабочий%')
  and ((extract(month from dateb) <= 1 and extract(year from dateb) = 2020) or extract(year from dateb) < 2020)
  and ((extract(month from dated) >= 1 and extract(year from dated) = 2020) or extract(year from dated) > 2020)
  and ((a.prozt != '0' and a.prozt != ' ') or a.stat2 = '3')
  order by stext2 desc, dated desc
) y on x.id_shop = y.id_shop and x.year = y.year and x.month = y.month
;

select * from BEP_PREM_DOP_ZAVMAG_PODSOB;

select 
  x.id_shop, x.year, x.month,
  max(zavmag_prem_percent) zavmag_prem_percent,
  max(podsob_prem_percent) podsob_prem_percent,
  case when max(zavmag_prem_percent) = 0 and  max(podsob_prem_percent) = 0 then 'T' else 'F' end is_delete 
from (
  select a.id_shop, a.year, a.month, 
  nvl(b.prem_percent, 0) zavmag_prem_percent,
  case when 100 * a.fact_sum/a.plan_sum <= 80 then 30 else 50 end podsob_prem_percent,
  'F' is_delete 
  from bep_prem_shop_plan a
  left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                  and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                  and a.fact_sum > b.min_sum
                                  and a.fact_sum <= b.max_sum
  where a.year = 2020 and a.month = 1 and a.id_shop = '0004'
  union all 
  select /*i_id_shop*/ '0004' id_Shop, /*i_year*/2020 year, /*i_month*/ 1 month, 0 zavmag_prem_percent, 0 podsob_prem_percent, 'T' is_delete from dual
) x
group by x.id_shop, x.year, x.month
;

---------------------------------------------------------------------------------
merge into bep_prem_seller a
using (
  select 
    x.id_shop, x.year, x.month,
    y.seller_tab, y.fio seller_fio,
    case when is_zavmag = 'T' then zavmag_prem_percent else null end zavmag_prem_percent,
    case when is_other = 'T' then other_prem_percent else null end other_prem_percent
  from (
   select a.id_shop, a.year, a.month, 
    nvl(b.prem_percent, 0) zavmag_prem_percent,
    case when 100 * a.fact_sum/a.plan_sum <= 80 then 30 else 50 end other_prem_percent
    from bep_prem_shop_plan a
    left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                    and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                    and a.fact_sum > b.min_sum
                                    and a.fact_sum <= b.max_sum
    where a.year = 2020 and a.month = 2 and a.id_shop = '0004'
  ) x
  inner join (
    select 
       b.shopid id_shop, 2020 year, 2 month, a.tabno seller_tab, a.fio, stext2,
      case when lower(stext2) like '%подсобный рабочий%' then 'T' else 'F' end is_other,
      case when lower(stext2) like '%заведующий%' then 'T' else 'F' end is_zavmag
    from s_seller a
    left join s_shop b on a.orgno = substr(b.shopnum, 2, 5)
    where 
    shopid = '0004' 
    and (lower(stext2) like '%заведующий%' or lower(stext2) like '%подсобный рабочий%')
    and ((extract(month from dateb) <= 2 and extract(year from dateb) = 2020) or extract(year from dateb) < 2020)
    and ((extract(month from dated) >= 2 and extract(year from dated) = 2020) or extract(year from dated) > 2020)
    and ((a.prozt != '0' and a.prozt != ' ') or a.stat2 = '3')
    order by stext2 desc, dated desc
  ) y on x.id_shop = y.id_shop and x.year = y.year and x.month = y.month
) b on (a.id_Shop = b.id_shop and a.year = b.year and a.month = b.month and a.seller_tab = b.seller_tab)
when matched
  then update set a.zavmag_prem_percent = b.zavmag_prem_percent, a.other_prem_percent = b.other_prem_percent
when not matched 
  then insert (a.seller_tab, a.seller_fio, a.id_shop, year, month, id_combination, group_t, fact_sum, fact_pair_kol, prem_individ_group, prem_individ_full, prem_plan_group, prem_plan_full, dateb, datee, id_group, other_prem_percent, zavmag_prem_percent)
        values(b.seller_tab,b.seller_fio, b.id_shop, b.year, b.month, 0, 'Остальное',  0, 0, 0, 0, 0, 0, null, null, null, b.other_prem_percent, b.zavmag_prem_percent);
                
;

select * from bep_Prem_seller where id_shop = '0004' and year = 2020 and month = 2;

merge into bep_prem_dop_zavmag_podsob a
  using (
  select 
    x.id_shop, x.year, x.month,
    max(zavmag_prem_percent) zavmag_prem_percent,
    max(podsob_prem_percent) podsob_prem_percent,
    case when max(zavmag_prem_percent) = 0 and  max(podsob_prem_percent) = 0 then 'T' else 'F' end is_delete 
  from (
    select a.id_shop, a.year, a.month, 
    nvl(b.prem_percent, 0) zavmag_prem_percent,
    case when 100 * a.fact_sum/a.plan_sum <= 80 then 30 else 50 end podsob_prem_percent,
    'F' is_delete 
    from bep_prem_shop_plan a
    left join bep_prem_zav_percent b on 100 * a.fact_sum/a.plan_sum >= b.min_progress 
                                    and 100 *a.fact_sum/a.plan_sum < b.max_progress
                                    and a.fact_sum > b.min_sum
                                    and a.fact_sum <= b.max_sum
    where a.year = /*i_year*/ 2020 and a.month = /*i_month*/ 1 and a.id_shop = /*i_id_shop*/ '0004'
    union all 
    select /*i_id_shop*/ '0004' id_Shop, /*i_year*/2020 year, /*i_month*/ 1 month, 0 zavmag_prem_percent, 0 podsob_prem_percent, 'T' is_delete from dual
  ) x
  group by x.id_shop, x.year, x.month
) b on (a.id_shop = b.id_shop and a.year = b.year and a.month = b.month)
when matched 
  then update set a.zavmag_prem_percent = b.zavmag_prem_percent, a.podsob_prem_percent = b.podsob_prem_percent
       delete where b.is_delete = 'T'
when not matched
  then insert (a.id_shop, a.year, a.month, a.zavmag_prem_percent, a.podsob_prem_percent)
       values (b.id_shop, b.year, b.month, b.zavmag_prem_percent, b.podsob_prem_percent)
;

select * from bep_prem_dop_zavmag_podsob;