
select * from (
select b.tabno, b.fio,  b.stext2, b.stext3, b.dateb, b.dated, b.prozt, b.stat2
--, d.massn, d.massn_name,d.stat2, d.stat5, d.begda, d.endda 
--a.*, 
,a.seller_tab, a.seller_fio, a.id_shop, a.year, a.month, a.group_t, a.fact_sum, a.prem_individ_group, a.prem_individ_full, a.prem_plan_group, a.prem_plan_full, a.id_combination, a.id_group, a.fact_pair_kol, a.dateb as group_dateb, a.datee as group_datee
--,d.fact_total_sum, d.fact_total_pair
,c.tabno zavmag_tab, c.fio zavmag, c.fact_sum zavmag_fact_sum from bep_prem_seller a
left join (
    select a1.tabno, a1.fio, a1.stext2, a1.stext3, a1.dateb, a1.dated, b1.id_shop, a1.prozt, a1.stat2 from s_seller a1
    inner join (
        select tabno, a.orgno, shopid id_shop, min(dated) dated from s_seller a
        left join s_shop b on a.orgno = substr(b.shopnum, 2, 5)
        where  ((extract(month from dateb) <= 12 and extract(year from dateb) = 2019) or extract(year from dateb) < 2019)
        and    ((extract(month from dated) >= 12 and extract(year from dated) = 2019) or extract(year from dated) > 2019)
        and shopid = '4316'
        and ((a.prozt != '0' and a.prozt != ' ') or a.stat2 = '3')
        group by tabno, a.orgno, shopid
    ) b1 on a1.tabno = b1.tabno and a1.dated = b1.dated and a1.orgno = b1.orgno
    and ((a1.prozt != '0' and a1.prozt != ' ') or a1.stat2 = '3')
) b on a.seller_tab = b.tabno 
----
and a.id_shop =  b.id_shop
----
left join (
  select seller_tab, id_shop, sum(fact_sum) fact_total_sum, sum(fact_pair_kol) fact_total_pair 
  from bep_prem_seller
  where year = 2019 and month = 12 and id_shop = '4316'
  group by seller_tab, id_shop
) d on a.id_shop = d.id_shop and a.seller_tab = d.seller_tab 
left join (
    select * from (
        select a.tabno, a.fio, b.shopid id_shop, nvl(c.fact_sum, 0) fact_sum 
        ,extract(month from dateb) monthb, extract(year from dateb) yearb
        ,extract(month from dated) monthd, extract(year from dated) yeard
        from s_seller a
        left join s_shop b on a.orgno = substr(b.shopnum, 2, 5)
        left join (
            select seller_tab, id_shop, sum(fact_sum) fact_sum  from bep_prem_seller  
            where year = 2019 and month = 12 and id_shop = '4316'
            group by seller_tab, id_shop
        ) c on a.tabno = c.seller_tab
        where 
        shopid = '4316' and
        (lower(stext2) like '%заведующий%' or lower(stext2) like '%администратор%' /*or lower(stext3) like '%заведующий%' or lower(stext3) like '%администратор%'*/)
        and ((extract(month from dateb) <=12 and extract(year from dateb) = 2019) or extract(year from dateb) < 2019)
        and ((extract(month from dated) >= 12 and extract(year from dated) = 2019) or extract(year from dated) > 2019)
        and ((a.prozt != '0' and a.prozt != ' ') or a.stat2 = '3')
        order by stext2 desc, dated desc
    ) where rownum = 1
) c on a.id_shop = c.id_shop
where a.year = 2019 and a.month = 12
and a.id_shop = '4316'
and a.seller_tab != c.tabno
) cur_seller
--where cur_seller.zavmag_fact_sum != 0 and cur_seller.fact_sum < 0  --and v_is_deleted = 'F'
;

select * from bep_prem_seller where year = 2019 and month = 12 and id_shop = '4316';


update bep_prem_seller 
set fact_sum = fact_sum + -8470, fact_pair_kol = fact_pair_kol + -2
where seller_tab = '02330248' and group_t = 'Обувь' and year = 2019 and month = 12 and id_shop = '4316';

delete from bep_prem_seller 
where seller_tab = '02330237' and group_t = 'Обувь' and year = 2019 and month = 12 and id_shop = '4316';


delete from bep_prem_seller
where year = 2019 and month = 12 and id_shop = '4316' 
and fact_sum = 0 and seller_tab not in (
  select seller_tab from bep_prem_seller
  where year = 2019 and month = 12 and id_shop = '4316' 
  and fact_sum != 0
);       
       
select distinct seller_tab from bep_prem_seller
where year = 2019 and month = 12 and id_shop = '4316' 
and fact_sum = 0 and seller_tab not in (
  select seller_tab from bep_prem_seller
  where year = 2019 and month = 12 and id_shop = '4316' 
  and fact_sum != 0
);

select 
distinct a.seller_tab
from (            
  select * from bep_prem_seller 
  where year = 2019 and month = 12 and id_shop = '4316'
  and fact_sum = 0
) a
left join (            
  select * from bep_prem_seller 
  where year = 2019 and month = 12 and id_shop = '4316'
  and fact_sum != 0
) b on a.seller_tab = b.seller_tab
;    