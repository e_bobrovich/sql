select distinct a.art, to_date('20200201','yyyymmdd') curr_date, s.priceid vidid, s.landid  from (
      select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
      ,trunc(a.dates) dates, trunc(a.datee) datee
      from firm.pos_skidki1 a
      left join firm.pos_skidki2 b on a.docid = b.docid
      inner join firm.pos_skidki3 c on a.docid = c.docid
      inner join table(sys.odcivarchar2list('2101','0031','4407')) d on c.shopid = d.column_value --- This is restriction view shops
      where trunc(to_date('20200201','yyyymmdd')) between trunc(a.dates) and trunc(a.datee)  and a.disc_type_id in (/*'ZK01',*/'ZK02') 
) a
inner join st_shop s on s.shopid = a.shopid
where a.rn = 1
and substr(to_char(a.cena), -2) = '99'
;

select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
,trunc(a.dates) dates, trunc(a.datee) datee
from firm.pos_skidki1 a
left join firm.pos_skidki2 b on a.docid = b.docid
inner join firm.pos_skidki3 c on a.docid = c.docid
inner join table(sys.odcivarchar2list('2101','0031','4407')) d on c.shopid = d.column_value --- This is restriction view shops
where 
trunc(to_date('20200201','yyyymmdd')) between trunc(a.dates) and trunc(a.datee)  
--'202002' >= to_char(a.dates, 'yyyymm') and '202002' <= to_char(a.datee, 'yyyymm')
and a.disc_type_id in (/*'ZK01',*/'ZK02') 
and art = '1914115';


select count(*), max(date_to)
from bep_prem_s_stock
where extract(year from date_from) = 2020
  and extract(month from date_from) = 2
  ;
  
  
select * from bep_prem_s_stock where date_from >= to_date('20200201','yyyymmdd');