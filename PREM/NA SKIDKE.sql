
--  CREATE OR REPLACE FORCE VIEW "FIRM"."BEP_PREM_MONTH_SALES" ("HOUR", "DATES", "ID_CHEK", "ID_SHOP", "VOP", "SELLER_TAB", "SELLER_FIO", "FACT_SALE_SUM", "FACT_PAIR_KOL", "PROCENT", "ART", "SH_SOP", "OWN_PURCHASE", "BRAND", "CENNIK", "ASSORT_TORG", "ASSORT", "GROUPMW", "SEASON", "IS_STOCK", "IS_NECOND", "IS_SPORT", "IS_FIDO", "IS_KIDS", "MAT_UPPER", "IS_VORS_LEATHER", "IS_SH_CHEK", "IS_SKIDKA", "PREM") AS 
  select a.hour, a.dates, a.id_chek id_chek, a.shop_id id_shop, a.vop, a.seller_tab, a.seller_fio, a.fact_sale_sum, a.fact_pair_kol, a.procent, a.art, a.sh_sop, a.own_purchase, a.brand, a.cennik, a.assort_torg, a.assort, a.groupmw, a.season, a.is_stock, a.is_necond, a.is_sport, a.is_fido, a.is_kids, a.mat_upper, a.is_vors_leather, is_sh_chek, is_skidka,
a.fact_sale_sum * (bep_prem_get_combination(extract(year from sysdate), extract(month from sysdate), a.shop_id, a.sh_sop, a.cennik, a.own_purchase, a.brand, a.assort_torg, a.groupmw,a.season, a.is_stock, a.is_sport, a.is_fido, a.is_kids, a.mat_upper, a.is_vors_leather, a.is_necond, a.assort, is_sh_chek, is_skidka, a.dates)) / 100 as prem from(
    select 
     --AA."HOUR"
    to_number(to_char(AA."DATES", 'HH24')) "HOUR"
    ,aa."DATES"
    ,AA."ID_CHEK"
    ,AA."SHOP_ID"
    ,AA."VOP"
    ,lpad(AA.SELLER_TAB, 8, '0') "SELLER_TAB"
    ,AA."SELLER_FIO"
    ,cast((AA."SUMP"-AA."SUMV") as number(18,2)) as fact_sale_sum
    ,case when c.mtart not in ('ZROH','ZHW3') then cast((KOLP-KOLV) as number(18)) else 0 end as FACT_PAIR_KOL
    ,AA."ART"
    ,AA."PROCENT"
    , case when c.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутка' end as "SH_SOP"
    , case when c.facture = ' ' and c.manufactor != 'СООО БЕЛВЕСТ' then 'Покупная'
           when c.facture = ' ' and c.manufactor = 'СООО БЕЛВЕСТ' then 'Собственная' 
           else c.facture 
      end as "OWN_PURCHASE"
    , c.manufactor as "BRAND"
    , case when "ACTION" = 'F' then 'Белый' else 'Оранжевый' end as "CENNIK" --CHANGE AFTER
    , c.assort_torg as "ASSORT_TORG"
    , c.assort as "ASSORT"
    , c.groupmw as "GROUPMW"
    , c.season "SEASON"
    , case when s.art is not null then 'Сток' else 'Не сток' end "IS_STOCK"
    , case when c.manufactor in ('ADIDAS INTERNATIONAL TRADING','LOTTO SPORT ITALIA SPA','PUMA SE RUDOLF DASSLER SPORT','SKECHERS SARL') then 'Спорт' else 'Не спорт' end "IS_SPORT"
    , case when c.manufactor in ('ООО ФИДО') then 'ФИДО' else 'Не ФИДО' end "IS_FIDO"
    , case when c.groupmw not in ('Мужские', 'Женские', 'N/A', ' ') then 'Десткие' else 'Не детские' end "IS_KIDS"
    , m.mat_upper "MAT_UPPER"
    , case when m.mat_upper in (
            'Н.Вел/Н.Кож с Во.Пок','Н.Вел/Н.Кожа Воск.от','Н.Кож Вел/Н.кож Нуб',
            'НатВелюр/НатКожа','НатВелюр/НатЛак','НатВелюр/НатНуб В.от',
            'НатКожа Велюр','НатКожа Велюр/Текст','НатКожа Спилок'
          ) then 'Ворсовая кожа (велюр\спилок)' else 'Не ворсовая кожа (велюр\спилок)' end "IS_VORS_LEATHER"
    , case when AA."PROCENT" > 0 then 'Некондиция' else 'Не некондиия' end "IS_NECOND"
    , case when sum(case when c.mtart not in ('ZROH','ZHW3') then 1 else 0 end) over (partition by aa.shop_id, aa.id_chek) > 0 then 'Обувь в чеке' else 'Без обуви в чеке' end is_sh_chek
    , case when SA.ART is not null then 'На скидке' else 'Не на скидке' end "IS_SKIDKA"
    FROM (
        SELECT 
        extract(hour from A.SALE_DATE) AS HOUR
        ,A.SALE_DATE AS DATES
        ,case when a.bit_vozvr='F' then 'ПРОДАЖА' else 'ВОЗВРАТ' end as vop 
        ,A.ID_CHEK
        ,A.SHOP_ID
        ,A.SELLER_FIO
        ,A.SELLER_TAB
        ,B.ART
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
        ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
        ,B.ACTION
        ,B.PROCENT
        FROM POS_SALE2 B
        INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
        WHERE A.BIT_CLOSE = 'T'
        --and A.SHOP_ID = COALESCE('0008', A.SHOP_ID)
        --and trunc(a.sale_date) = trunc(sysdate-1)
        and to_char(a.sale_date, 'yyyymm') = to_char(sysdate, 'yyyymm')

        UNION ALL

        SELECT 
        0 as hour
        --extract(hour from COALESCE(C2.DATES, A2.dated)) as HOUR
        ,COALESCE(C2.DATES, A2.date_s) as DATES
        ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' as vop
        ,A2.ID AS ID_CHEK
        ,A2.ID_SHOP
        ,B2.SELLER_FIO
        ,B2.SELLER_TAB
        ,B2.ART
        ,0 AS SUMP1
        ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
        ,CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
        ,CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
        ,0  AS KOLP 
        ,B2.KOL AS KOLV
        ,B2.ACTION
        ,B2.PROCENT
        FROM D_PRIXOD2 B2
        INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
        --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
        LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                        SELECT p3.ID, p3.ID_SHOP, MAX(p3.DATE_S) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = p3.ID_SHOP GROUP BY p3.ID, p3.ID_SHOP
                        --UNION ALL
                        --SELECT ID_PRIXOD AS ID, ID_SHOP, MAX(DATE_S) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP =  ID_SHOP GROUP BY ID_PRIXOD, ID, ID_SHOP
                        ) WHERE ID != 0 GROUP BY ID, ID_SHOP
                ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
        LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = c.ID_SHOP WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
        WHERE
        ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19',/*'42',*/'45','55')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in (/*21.11.2019 Цыбин не считать 20*/'20','03','14','19',/*'42',*/'45','55'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.DATED) IS NOT NULL)))
        AND A2.BIT_CLOSE='T' 
        --and A2.ID_SHOP = COALESCE('0008', A2.ID_SHOP)
        --and trunc( A2.dated) = trunc(sysdate-1)
        --and to_char(A2.dated, 'yyyymm') = to_char(sysdate, 'yyyymm')
        and to_char(case when A2.IDOP in ('14') then C2.DATES else a2.dated end, 'yyyymm') = to_char(sysdate, 'yyyymm')
        UNION ALL

        select 
        --extract(hour from a.DATED) as HOUR
        0 as hour
        ,a.DATED as DATES
        ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end as vop
        ,a.ID AS ID_CHEK
        ,b.id_shop
        ,b.SELLER_FIO
        ,b.seller_tab
        ,b.art ARTl
        ,0 AS SUMP1
        ,0 AS SUMV1 
        ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
        ,0 AS SUMV 
        ,b.kol AS KOLP
        ,0 AS KOLV
        ,b.ACTION
        ,b.procent
        from d_rasxod1 a
        inner join (
                select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, x.action, x.procent, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2, nvl(x.kol,0) kol
                from (
                    select id, art, seller_fio, seller_tab, id_shop, action, procent, sum(kol*cena3) sum, sum(kol) kol from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop, action, procent
                ) x
                left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
                ) y on x.id = y.id and x.id_shop = y.id_shop
        ) b on a.id = b.id and a.id_shop = b.id_shop

        where a.bit_close = 'T' and a.idop in ('34','35',/*'37',*/'38','39','40', '44')
        --and b.id_shop = COALESCE('0008', b.id_shop)
        --and trunc(a.dated) = trunc(sysdate-1)
        and to_char(a.dated, 'yyyymm') = to_char(sysdate, 'yyyymm')
    ) AA
    left join s_art c on aa.art = c.art
    left join (select distinct shopid id_shop, priceid, landid from st_shop where org_kod = 'SHP') sh on aa.shop_id = sh.id_shop 
    left join (select art, status_art from s_all_mat where status_art = 'S' group by art, status_art) s on aa.art = s.art
    left join (select art, mat_upper from s_all_mat group by art, mat_upper) m on aa.art = m.art
    left join bep_prem_criterion_art_list sa 
      on sa.priceid = case when sa.priceid = 'BY' then sh.landid else sh.priceid end
      and aa.art = sa.art and trunc(aa.dates) >= trunc(dateb) and trunc(aa.dates) <= trunc(datee)    
) a;

select * from bep_prem_criterion_art_list where id_list in (1,2,3);

update bep_prem_criterion_art_list set dateb = to_date('01.12.2019');
update bep_prem_criterion_art_list set datee = to_date('17.01.2020') where id_list in (1,2,3);

select sa.* ,aa.* from bep_Prem_month_sales aa
left join (select distinct shopid id_shop, priceid, landid from st_shop where org_kod = 'SHP') sh on aa.id_shop = sh.id_shop 
left join bep_prem_criterion_art_list sa 
      on sa.priceid = case when sa.priceid = 'BY' then sh.landid else sh.priceid end
      and aa.art = sa.art and trunc(aa.dates) >= trunc(dateb) and trunc(aa.dates) <= trunc(datee)  
where aa.id_shop = '0001'
and sa.art is not null
;

select * from bep_Prem_month_sales aa
where art in (select art from bep_prem_criterion_art_list where priceid = 'BY');