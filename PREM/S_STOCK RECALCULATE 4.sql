select * from IDOCS.idocs_struct where lower(plsql_query) like '%status_art%';

select * from IDOCS.idocs_stat where system in ('BRP','BWP599','R4P300') and idoctype = 'ZBW_ART_POS' order by time_in desc;

select * from bep_prem_s_stock where extract(month from date_from) = 2;


  select * from bep_prem_s_stock 
  where extract(year from date_from) = 2020
    and extract(month from date_from) = 2
    and extract(day from date_from) = case -- если не весь месяц и задан день,то эта дата, иначе весь месяц
                                          when 'F' = 'F' and 1 is not null then 1 
                                          else extract(day from date_from) 
                                        end;


select * from (
  select distinct a.art, to_date('20200201','yyyymmdd') curr_date, s.priceid vidid, s.landid  from (
        select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
        ,trunc(a.dates) dates, trunc(a.datee) datee
        from firm.pos_skidki1 a
        left join firm.pos_skidki2 b on a.docid = b.docid
        inner join firm.pos_skidki3 c on a.docid = c.docid
        inner join table(sys.odcivarchar2list('2101','0031','4407')) d on c.shopid = d.column_value --- This is restriction view shops
        where trunc(to_date('20200206','yyyymmdd')) between trunc(a.dates) and trunc(a.datee)  and a.disc_type_id in (/*'ZK01',*/'ZK02') 
  ) a
  inner join st_shop s on s.shopid = a.shopid
  where a.rn = 1
  and substr(to_char(a.cena), -2) = '99'
) a
full join (
  select distinct a.art, to_date('20200201','yyyymmdd') curr_date, s.priceid vidid, s.landid  from (
        select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
        ,trunc(a.dates) dates, trunc(a.datee) datee
        from firm.pos_skidki1 a
        left join firm.pos_skidki2 b on a.docid = b.docid
        inner join firm.pos_skidki3 c on a.docid = c.docid
        inner join table(sys.odcivarchar2list('2101','0031','4407')) d on c.shopid = d.column_value --- This is restriction view shops
        where trunc(to_date('20200106','yyyymmdd')) between trunc(a.dates) and trunc(a.datee)  and a.disc_type_id in (/*'ZK01',*/'ZK02') 
  ) a
  inner join st_shop s on s.shopid = a.shopid
  where a.rn = 1
  and substr(to_char(a.cena), -2) = '99'
) b on a.art = b.art and a.vidid = b.vidid and a.landid = b.landid
--where a.art is null or b.art is null
where a.art ='689368/1' or b.art = '689368/1'
;