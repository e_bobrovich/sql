select * from bep_prem_s_stock;
select * from BEP_PREM_TMP_STOCK;
select dates, count(*) from BEP_PREM_TMP_STOCK group by dates order by dates;

select art, count(*) from BEP_PREM_TMP_STOCK group by art having count(*) < 21;


select * from bep_prem_tmp_stock where art = '9830520' order by dates;

select * from bep_prem_tmp_stock where art = '9830520' order by dates;

select level day from dual connect by level <= extract(day from last_day(to_date('202002','yyyymm')));


select a.*, b.art, b.vidid, b.reg, b.dates,  
lag (a.dates,1) over (partition by b.art, b.vidid, b.reg order by a.dates) as prev_dates
from (
  select to_date(lpad(2020, 4, '0')||lpad(1, 2, '0')||level,'yyyymmdd') dates 
  from dual 
  connect by level <= extract(day from last_day(to_date('202001','yyyymm')))
) a
left join (
  select q.* 
  from bep_prem_tmp_stock q 
  where art = '9830520' and to_char(dates, 'yyyymm') =  '202001' 
  order by dates
) b on a.dates = b.dates
order by a.dates
;

select art, vidid, reg, dates,
lag (dates,1) over (partition by art, vidid, reg order by dates) as prev_dates
from bep_prem_tmp_stock
where art = '9830520' and to_char(dates, 'yyyymm') =  '202001'
;

select * from (
  select a.dates dates1, b.art, b.vidid, b.reg, b.dates,  
  lag (a.dates,1) over (partition by b.art, b.vidid, b.reg order by a.dates) as prev_dates
  from (
    select to_date(lpad(2020, 4, '0')||lpad(1, 2, '0')||level,'yyyymmdd') dates 
    from dual 
    connect by level <= extract(day from last_day(to_date('202001','yyyymm')))
    --order by to_date(lpad(2020, 4, '0')||lpad(1, 2, '0')||level,'yyyymmdd')
  ) a
  left join (
    select q.* 
    from bep_prem_tmp_stock q 
    where art = '9830520' and to_char(dates, 'yyyymm') =  '202001' 
    order by dates
  ) b on a.dates = b.dates
  where b.art is not null 
  order by a.dates
) group by art, vidid, reg--, case when dates - prev_dates = 1 then 
;

SET serveroutput ON
DECLARE
  v_date date := to_date('20200101','yyyymmdd');
  v_prev_day varchar2(1) := 'F';
  i_year integer := 2020;
  i_month integer := 1;
  v_count integer := 0;
BEGIN
  delete from BEP_PREM_TMP_STOCK 
  where extract(year from dates) = i_year
    and extract(month from dates) = i_month;

  v_date := to_date(lpad(i_year, 4, '0')||lpad(i_month, 2, '0')||'01','yyyymmdd');
  while (extract(month from v_date) = i_month and extract(year from v_date) = i_year and v_date <= sysdate)
  loop
    dbms_output.put_line(v_date);
    for r_cur_stock in (
        select distinct a.art, v_date curr_date, s.priceid vidid, s.landid reg  from (
              select row_number() over(partition by c.shopid,b.art order by a.priority desc) rn, b.art,b.procent,b.docid,b.cena, a.priority, a.disc_type_id, c.shopid,a.doc_exception
              ,trunc(a.dates) dates, trunc(a.datee) datee
              from firm.pos_skidki1 a
              left join firm.pos_skidki2 b on a.docid = b.docid
              inner join firm.pos_skidki3 c on a.docid = c.docid
              inner join table(sys.odcivarchar2list('2101','0031','4407')) d on c.shopid = d.column_value
              where trunc(v_date) between trunc(a.dates) and trunc(a.datee)  and a.disc_type_id in (/*'ZK01',*/'ZK02') 
        ) a
        inner join st_shop s on s.shopid = a.shopid
        where a.rn = 1
        and substr(to_char(a.cena), -2) = '99'
        
--        select distinct art, v_date date_cur, vidid, reg
--        from table(GET_CENA_SKIDKI_ALL_SHOPS(sys.odcivarchar2list('2101','0031','4407'), v_date))
--        where substr(to_char(cena), -2) = '99'
      )
    loop
      -- проверка наличия этого артикула в стоке на день до рассматриваемого
      insert into BEP_PREM_TMP_STOCK (art, dates, vidid, reg)
      values (r_cur_stock.art, v_date, r_cur_stock.vidid, r_cur_stock.reg);
    end loop;
    commit;   
    
    v_date := v_date + 1;
  end loop;
  commit;
               
exception when others then
  dbms_output.put_line(sqlcode||sqlerrm);
  rollback;   
END ;