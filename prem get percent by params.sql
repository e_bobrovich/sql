
select distinct(gs.id_shop) from bep_prem_group_shop gs
inner join bep_prem_s_group sg on gs.id_group = sg.id_group

where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate);


select shopid,priceid
from st_shop
where org_kod = 'SHP' and bit_open = 'T';

select * from CENA_SKIDKI_ALL_SHOPS;

select a.id_shop, b.priceid, c.art, c.action from (
    select distinct(gs.id_shop) from bep_prem_group_shop gs
    inner join bep_prem_s_group sg on gs.id_group = sg.id_group
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
) a
inner join (
    select shopid , priceid from st_shop
    where org_kod = 'SHP'
) b on a.id_shop = b.shopid
inner join cena_skidki_all_shops c on b.priceid = c.vidid
--where a.id_shop = '0071'
;

select 
--* 
a.id_shop, a.art, max(percent) percent, systimestamp update_date
from (
    select id_shop, art, 
    case when action = 'F' then 'Белый' else 'Оранжевый' end as cennik
    --from table(get_cena_skidki_all_shops_prem())
    from (
        select a.id_shop, c.art, c.action from (
            select distinct(gs.id_shop) from bep_prem_group_shop gs
            inner join bep_prem_s_group sg on gs.id_group = sg.id_group
            where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
        ) a
        inner join (select shopid , priceid from st_shop where org_kod = 'SHP'
        ) b on a.id_shop = b.shopid
        inner join cena_skidki_all_shops c on b.priceid = c.vidid
    )

--    select '0001' id_shop, '0109000/О' art,	'Оранжевый' CENNIK from dual
) a
inner join (
    select 
    a.art, 
    case when a.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутка' end as "SH_SOP",
    case when a.facture = ' ' and a.manufactor != 'СООО БЕЛВЕСТ' then 'Покупная'
               when a.facture = ' ' and a.manufactor = 'СООО БЕЛВЕСТ' then 'Собственная' 
               else a.facture 
          end as "OWN_PURCHASE",
    a.manufactor as "BRAND",
    a.assort_torg as "ASSORT_TORG",
    a.groupmw as "GROUPMW"
    from s_art a
) b on a.art = b.art

inner join (
    select * from (
        select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
        from bep_prem_combination_percent cp
        inner join bep_prem_s_group sg on cp.id_group = sg.id_group
        inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
        left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
        left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
        where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
        --and sg.dateb is not null and sg.datee is not null
        and cp.id_combination != 0
        --and gs.id_shop = '0001'--'3622'
        group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
    ) a
    where 
        trunc(sysdate) >= case when a.dateb is not null then a.dateb else trunc(sysdate) end
    and trunc(sysdate) <= case when a.datee is not null then a.datee else trunc(sysdate) end
) c on
a.id_shop = c.id_shop
    and case when c.ids_criterion like '%'||1||'%' then c.combination else b.sh_sop end like '%'||b.sh_sop||'%' 
    and case when c.ids_criterion like '%'||2||'%' then c.combination else a.cennik end like '%'||a.cennik||'%'  
    and case when c.ids_criterion like '%'||3||'%' then c.combination else b.own_purchase  end like '%'||b.own_purchase ||'%'  
    and case when c.ids_criterion like '%'||4||'%' then c.combination else b.brand end like '%'||b.brand||'%'
    and case when c.ids_criterion like '%'||5||'%' then c.combination else b.ASSORT_TORG end like '%'||b.ASSORT_TORG||'%'
    and case when c.ids_criterion like '%'||6||'%' then c.combination else b.groupmw end like '%'||b.groupmw||'%'

group by a.id_shop, a.art
;
