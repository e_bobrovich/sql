
--номер отд, время продажи , группа, артикул, наименование ТМЦ, кол-во

select 
a.id_shop, 
to_char(a.sale_date, 'dd.mm.yyyy HH24:MI:SS') sale_date,
case when c.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутствующие товары' end "group",
b.art,
c.assort,
b.kol,
b.cena3
from pos_sale1 a
inner join pos_sale2 b on a.id_shop = b.id_shop and a.id_chek = b.id_chek
inner join s_art c on b.art = c.art
where a.sale_date >= to_date('20191201','yyyymmdd') and a.id_Shop not in ('S777', 'S888')
order by id_shop, sale_date
;