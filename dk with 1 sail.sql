select 'карт с продажей до декабря', count(*) from st_dk where bit_block = 'F' and id_dk in (select id_dk from pos_sale1 where sale_date < to_date('01.12.2018', 'dd.mm.yyyy') group by id_dk having count(*) = 1);


select * from st_dk where bit_block = 'F';
select * from pos_sale1 where sale_date < to_date('01.12.2018', 'dd.mm.yyyy');

select 'карт с продажей по РФ', count(*) from st_dk a left join pos_sale1 b on a.id_dk = b.id_dk  where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с продажей по РБ', count(*) from st_dk a left join pos_sale1 b on a.id_dk = b.id_dk  where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null;

select id_dk, count(*) from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk;

select 'карт с 1 продажей до мая BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk having count(*) = 1) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт с 1 продажей до июня BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.06.2019', 'dd.mm.yyyy') group by id_dk having count(*) = 1) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт с 1 продажей до мая RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk having count(*) = 1) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт с 1 продажей до июня RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.06.2019', 'dd.mm.yyyy') group by id_dk having count(*) = 1) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is not null
;



select 'карт активированных до мая RU', count(*) from st_dk where dk_country = 'RU' and bit_block = 'F' and activation_date < to_date('01.05.2019', 'dd.mm.yyyy')
union all
select 'карт активированных до июня RU',count(*) from st_dk where dk_country = 'RU' and bit_block = 'F' and activation_date < to_date('01.06.2019', 'dd.mm.yyyy')
union all
select 'карт активированных до мая BY', count(*) from st_dk where dk_country = 'BY' and bit_block = 'F' and activation_date < to_date('01.05.2019', 'dd.mm.yyyy')
union all
select 'карт активированных до июня BY',count(*) from st_dk where dk_country = 'BY' and bit_block = 'F' and activation_date < to_date('01.06.2019', 'dd.mm.yyyy')
--union all
--select 'карт активированных до февраля',count(*) from st_dk where dk_country = 'RU' and bit_block = 'F' and activation_date < to_date('01.02.2019', 'dd.mm.yyyy')
--union all
--select 'карт активированных до марта',count(*) from st_dk where dk_country = 'RU' and bit_block = 'F' and activation_date < to_date('01.03.2019', 'dd.mm.yyyy')
--union all
--select 'карт активированных до апреля',count(*) from st_dk where dk_country = 'RU' and bit_block = 'F' and activation_date < to_date('01.04.2019', 'dd.mm.yyyy')
;


select 'карт с 0 продаж по РФ', count(*) from st_dk a left join pos_sale1 b on a.id_dk = b.id_dk  where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с 0 продаж по РБ', count(*) from st_dk a left join pos_sale1 b on a.id_dk = b.id_dk  where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null;

select 'карт с 0 продаж до мая BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с 0 продаж до июня BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.06.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с 0 продаж до мая RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с 0 продаж до июня RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.06.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is null
--union all
--select 'карт с 0 продаж до апреля', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.04.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
--where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null
;

select 'карт с продажей по РФ', count(*) from st_dk a left join pos_sale1 b on a.id_dk = b.id_dk  where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с продажей по РБ', count(*) from st_dk a left join pos_sale1 b on a.id_dk = b.id_dk  where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null;

select 'карт с продажей до мая BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт с продажей до июня BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.06.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт с продажей до мая RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.05.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт с продажей до июня RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.06.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is not null
;


select 'карт активированных до декабря RU',count(*) from st_dk where dk_country = 'RU' and bit_block = 'F' and activation_date < to_date('01.12.2019', 'dd.mm.yyyy')
union all
select 'карт с 0 продаж до декабря RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.12.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с 1 продажей до декабря RU', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.12.2019', 'dd.mm.yyyy') group by id_dk having count(*) = 1) b on a.id_dk = b.id_dk 
where a.dk_country = 'RU' and a.bit_block = 'F' and b.id_dk is not null
union all
select 'карт активированных до декабря BY', count(*) from st_dk where dk_country = 'BY' and bit_block = 'F' and activation_date < to_date('01.12.2019', 'dd.mm.yyyy')
union all
select 'карт с 0 продаж до декабря BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.12.2019', 'dd.mm.yyyy') group by id_dk) b on a.id_dk = b.id_dk
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is null
union all
select 'карт с 1 продажей до декабря BY', count(*) from st_dk a left join (select id_dk from pos_sale1 where sale_date < to_date('01.12.2019', 'dd.mm.yyyy') group by id_dk having count(*) = 1) b on a.id_dk = b.id_dk 
where a.dk_country = 'BY' and a.bit_block = 'F' and b.id_dk is not null
;

