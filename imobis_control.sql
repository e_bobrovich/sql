select * from imob.s_error_code;

select * from ri_stored_proc_error order by er_time desc;

select * from firm.st_shop where shopid = '0003';

select b.id_imobis, a.* from api_messages a left join DELIVERY_HISTORY b on a.id_message = b.id_del||'-'||id_dk where trunc(DATE_CALL) = trunc(sysdate) order by a.date_call desc;

select b.id_imobis, a.* from api_messages a left join DELIVERY_HISTORY b on a.id_message = b.id_del||'-'||id_dk where phone_number = '79219580485' order by a.date_call desc;

select * from S_CHANNEL;

select to_char(date_call,'day','NLS_DATE_LANGUAGE=RUSSIAN') day, trunc(date_call) "DATE", count(*) "COUNT" from pos_dk_codes 
group by to_char(date_call,'day','NLS_DATE_LANGUAGE=RUSSIAN'), trunc(date_call)
order by  trunc(date_call) desc;

select a.id_dk, id_shop, id_pos, id_imobis, code, phone_number , 
case when status = 'ERROR' then nvl(b.text, 'ERROR') 
     else case when status = 'STATUS' then nvl(b.description, 'STATUS') else status end end as "STATUS",  
to_char(date_call, 'dd.mm.yy hh24:mi:ss') date_call, to_char(date_last, 'dd.mm.yy hh24:mi:ss') date_last 
from pos_dk_codes  a
left join
(
    select a.id_dk, a.id_imobis, b.text, c.description
    from delivery_history a
    left join s_error_code b on a.error = b.code 
    left join s_status c on a.id_status = c.id_status
    where a.id_del in (0,1)
) b on a.id_pos = b.id_dk
where trunc(date_call) = trunc(systimestamp)
order by date_call desc, date_last desc;

--------------------------------------------------


select b.cnt, a.* from st_delivery a
left join (select id_del,count(1) cnt from delivery_dk group by id_del) b on a.id_del = b.id_del
order by a.id_del desc;

select * from v_pool_with_date where id_del >= 7600 order by start_date desc;

select a.id_del "id_del",nvl(b.result, 0) result, a.plan, nvl(round((b.result/a.plan)*100, 2), 0) progress from (
    select a.id_del, nvl(count(id_dk), 0) plan from delivery_dk a inner join tech_data b on a.id_del = b.id_del 
    where a.id_del >=6304 and groupt = 'O' group by a.id_del
) a
left join (
    select id_del , nvl(count(id_dk), 0) result from delivery_history 
    where id_del >= 6304 group by id_del
) b on a.id_del = b.id_del
where a.id_del not in (select id_del from st_delivery where flag_delete = 1)
union all

select 99999 as "id_del",nvl(d.result, 0), c.plan
, case when c.plan = 0 then 0 else nvl(round((d.result/c.plan)*100, 2), 0) end
from (
    select 99999 as "id_del", nvl(count(id_dk), 0) plan from delivery_dk a inner join tech_data b on a.id_del = b.id_del 
    where a.id_del >= 6304 and groupt = 'O'
    and a.id_del not in (select id_del from st_delivery where flag_delete = 1)
) c
left join (
    select 99999 as "id_del",  nvl(count(id_dk), 0) result from delivery_history 
    where id_del >= 6304
    and id_del not in (select id_del from st_delivery where flag_delete = 1)
) d on c."id_del" = d."id_del"
order by 1
;


select a.id_del, available,round(available/(available+nvl(no_available, 0)),2)*100||'%' "1%",
'  ',
nvl(no_available, 0) as no_available,   round(nvl(no_available, 0)/(available+nvl(no_available, 0)),2)*100||'%' "2%" ,
available+no_available "all"
from 
(
    select id_del, count(*) as available from delivery_history 
    where id_del >= 6304 and id_status not in (21,22) group by id_del order by id_del
) a
left join 
(
    select id_del, count(*) as no_available from delivery_history 
    where id_del >= 6304 and id_status in (21,22) group by id_del order by id_del
) b on a.id_del = b.id_del
order by id_del;

select a.id_status,b.description, count(*) 
from delivery_history a
left join s_status b on a.id_status = b.id_status
where a.id_del >= 4663 group by a.id_status, b.description order by id_status;
--------------------------------------------------
--------------------------------------------------

select a.id_status, a.name_status, c.name_channel, a.sms_state_devino, a.viber_status_devino, a.description,  b.final from s_status a
inner join channel_status b on a.id_status = b.id_status
inner join s_channel c on b.id_channel = c.id_channel;

select * from s_error_code;
select * from service_settings;

select a.description, c.name_channel, case when b.final = 1 then 'Да' else 'Нет' end from s_status a
inner join channel_status b on a.id_status = b.id_status
inner join s_channel c on b.id_channel = c.id_channel
;

--------------------------------------------------
call set_delivery_pool_status(4544, 'DONE');

select a.id_dk, id_shop, LISTAGG(a.phone_number, ', ') WITHIN GROUP (order by a.date_call) as phone_numbers, count(*)
from pos_dk_codes  a
where trunc(date_call) = trunc(systimestamp)
group by a.id_dk, id_shop
order by id_shop
;
--------------------------------------------------

select a.id_message, a.user_id_message,'    ', b.id_imobis, b.id_status from api_messages a
left join delivery_history b on get_iddel(a.id_message) = b.id_del and get_idpos(a.id_message) = b.id_dk
where 
a.user_id_message in (
'201094865',
'101424545',
'201094751',
'101356154',
'101424344'
);

select "user" , "password" , "sender" , "phone" , "messageId" , "text" , "messengerText" ,
                            "messengerImage" , "messengerActionTitle" , "messengerActionURL" , "order" ,
                            "messengerSentTTL" , "messengerModeTTL"
                            from v_messages where get_iddel("messageId") = 4145 and rownum = 1;


call firm.calculation_day_tables.t_day_st_dk_imob1();

select distinct fl.login as "login", fl.password as "password",fsd.phone_number as "phone", fsd.id_dk, nvl(bl.phone_list, 'F') as black_list, nvl(wl.white_list, 'F') as white_list
from firm.st_dk fsd
left join firm.st_shop fsts on fsd.id_shop = fsts.shopid
left join firm.s_shop fsh on substr(fsts.shopid,1,2) = substr(fsh.shopid,1,2) and substr(fsh.shopid,3,2) = 'F1'
left join s_filial_logins fl on fsh.shopid=fl.shopid
left join 
(
    select id_dk, 
    case when v_st_dk = 'T' then 'T'
        else 'F' end as phone_list
    from black_list
) bl on fsd.id_dk = bl.id_dk
left join 
(
    select id_dk,
    case when count(*) != 0 then 'T'
        else 'F' end as white_list
    from white_list
    group by id_dk
) wl
on fsd.id_dk = wl.id_dk
where fsd.id_dk = '0000027666715'
;

select * from t_day_st_dk1;

select * from black_list where id_dk = '0000028904298';

select * from delivery_history where id_dk = '0000014460012' order by date_sent desc;

select get_imob_from_dk('0000007387654') as info from dual;

select get_imob_from_phone('0000010365151') as info from dual;

--8 931 356 93 98
--8 981 946 40 95
select id_dk, phone_number 
from firm.st_dk
--from t_day_st_dk1
where 
substr(replace(replace(' 8 981 197 65 69', ' ', ''), '+', ''),2) = 
substr(replace(replace(phone_number, ' ', ''), '+', ''),2);

call delete_black_list('0000014337796', 'PHONE');

call add_white_list('0000000000000', '', sysdate);

call add_black_list('0000021666339', ' ', sysdate, 'PHONE');

select * from black_list;
-------

select * from firm.st_dk where fio like '%Новоженина Татьяна%';
select * from delivery_history where id_dk = '0000000233392';

select a.id_del "id_del",nvl(b.result, 0), a.plan, nvl(round((b.result/a.plan)*100, 2), 0) progress from (
    select a.id_del, nvl(count(id_dk), 0) plan from delivery_dk a inner join tech_data b on a.id_del = b.id_del 
    where a.id_del >=2042 and groupt = 'O' group by a.id_del
) a
left join (
    select id_del , nvl(count(id_dk), 0) result from delivery_history 
    where id_del >= 2520 group by id_del
) b on a.id_del = b.id_del
--where a.id_del not in (select id_del from st_delivery where flag_delete = 1)
union all

select 99999 as "id_del",nvl(d.result, 0), c.plan, nvl(round((d.result/c.plan)*100, 2), 0) progress from (
    select 99999 as "id_del", nvl(count(id_dk), 0) plan from delivery_dk a inner join tech_data b on a.id_del = b.id_del 
    where a.id_del >=2042 and groupt = 'O'
    --and a.id_del not in (select id_del from st_delivery where flag_delete = 1)
) c
left join (
    select 99999 as "id_del",  nvl(count(id_dk), 0) result from delivery_history 
    where id_del >= 2520
    --and id_del not in (select id_del from st_delivery where flag_delete = 1)
) d on c."id_del" = d."id_del"
order by 1

;

select * from delivery_history where id_dk = '0000020518608-1521614658';


select a.id_dk, id_shop, id_pos, code, phone_number , 
case when status = 'ERROR' then nvl(b.text, 'ERROR') 
     else status end as "STATUS",  
to_char(date_call, 'dd.mm.yy hh24:mi:ss') date_call, to_char(date_last, 'dd.mm.yy hh24:mi:ss') date_last 
from pos_dk_codes  a
left join
(
    select a.id_dk, b.text
    from delivery_history a
    left join s_error_code b on a.error = b.code 
    where a.id_del = 0
) b on a.id_pos = b.id_dk
where trunc(date_call) = trunc(systimestamp)
order by date_call desc, date_last desc;

select * from delivery_history where id_dk = '0000015051578-1522828842';
select * from delivery_history where id_imobis = '322794658';

select * from firm.st_dk where id_dk ='0000017060547';
select * from delivery_dk where id_dk in ('0000017060547', '0000017060547');

select * from pos_dk_codes where id_dk = '0000008914552';

select * from pos_dk_codes where 
substr(replace(replace('+79263738883', ' ', ''), '+', ''),2) = 
substr(replace(replace(phone_number, ' ', ''), '+', ''),2);

select * from delivery_history
where id_dk in ('0000010466087','0000010968437','0000015101679','0000015825315','0000016547544','0000020580919','0000018319958','0000021626821','0000021666339','0000022031747','0000023458963','0000022301215','0000023996359')
;

select * from delivery_history
where get_iddk(id_dk) in ('0000008914552','0000008914552')
;


select count(*) 
from delivery_history a
where 
(a.DATE_DONE >= trunc(to_date('01.12.2018 00:00:00', 'dd.mm.yyyy hh24:mi:ss')) or a.DATE_SENT >= trunc(to_date('01.12.2018 00:00:00', 'dd.mm.yyyy hh24:mi:ss')))
and a.id_del not in (1,3)
;