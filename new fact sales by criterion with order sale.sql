delete from tem_bep_prem_sales_crit_test;
select * from tem_bep_prem_sales_crit_test;

select id_chek, sh_sop from tem_bep_prem_sales_crit_test where sh_sop = 'Обувь' group by id_chek, sh_sop having count(*) > 1;


select seller_tab, id_chek, count(*), sum(fact_sale_sum),
(0.2 * (count(*) - 1) + 1)*1.25 as prem_perc,
sum(fact_sale_sum) * ((0.2 * (count(*) - 1) + 1)*1.25)/100 as comb_check_prem
from tem_bep_prem_sales_crit_test 
where sh_sop = 'Обувь' --and vop in ('БЕЗНАЛ','ПРОДАЖА') 
and year = 2019 and month = 5 and id_shop = '0001' --and seller_tab = '00000182'
group by seller_tab, id_chek
;

select vop, seller_tab, seller_fio, fact_sale_sum, art, id_chek, percent, order_sale,
(0.2 * (order_sale-1) + 1) * percent percent_comb
from tem_bep_prem_sales_crit_test
where order_sale != 0
order by id_chek, order_sale
;

--row_number() OVER (PARTITION BY date_part('year',spravka.dateend) ORDER BY spravka.dateend)
select w.*, row_number() over (partition by w.id_chek order by w.fact_sale_sum) rn 
from tem_bep_prem_sales_crit_test w where sh_sop = 'Обувь'
;

select a.* from tem_bep_prem_sales_crit_test a 
where (a.id_chek, a.sh_sop) in (
    select id_chek, sh_sop from tem_bep_prem_sales_crit_test where sh_sop = 'Обувь' group by id_chek, sh_sop having count(*) > 1
)
order by a.id_chek, a.fact_sale_sum
;



insert into tem_bep_prem_sales_crit_test(dated, year, month, id_shop, vop, seller_tab, seller_fio, fact_sale_sum, fact_pair_kol, art, sh_sop, own_purchase, brand, cennik, assort_torg, groupmw, id_chek, percent, order_sale, PERCENT_COMBO, PREM_SALE)
select 
    a."DATED", 
    a."YEAR", 
    a."MONTH", 
    a."SHOP_ID", 
    a."VOP", 
    a."SELLER_TAB", 
    a."SELLER_FIO", 
    a."FACT_SALE_SUM",
    a."FACT_PAIR_KOL" , 
    a."ART", 
    a."SH_SOP" , 
    a."OWN_PURCHASE" , 
    a."BRAND", 
    a."CENNIK" , 
    a."ASSORT_TORG", 
    a."GROUPMW" , 
    a."ID_CHEK" ,
    max(nvl(c.percent,d.percent)) percent_result,
    case when a.sh_sop = 'Обувь' and vop in ('ПРОДАЖА', 'БЕЗНАЛ') then row_number() over (partition by a.id_chek, a.sh_sop order by a.fact_sale_sum desc) else 0 end rn,
    case when a.sh_sop = 'Обувь' and vop in ('ПРОДАЖА', 'БЕЗНАЛ')
        then 
            (0.2 * (row_number() over (partition by a.id_chek, a.sh_sop order by a.fact_sale_sum desc) - 1) + 1) * max(nvl(c.percent,d.percent)) 
        else max(nvl(c.percent,d.percent)) end percent_combo,
    fact_sale_sum * (
        case when a.sh_sop = 'Обувь' and vop in ('ПРОДАЖА', 'БЕЗНАЛ')
            then 
                (0.2 * (row_number() over (partition by a.id_chek, a.sh_sop order by a.fact_sale_sum desc) - 1) + 1) * max(nvl(c.percent,d.percent)) 
            else max(nvl(c.percent,d.percent)) end / 100
        ) prem_pair
--    ,
--    c.dateb, c.datee, 
--    nvl(c.id_group, d.id_group) id_group,
--    nvl(c.id_combination,d.id_combination) id_combination, 
--    nvl(c.combination, 'Остальное') combination,
--    nvl(c.percent,d.percent) percent -- процент исходный
    
from (
    select 
     AA."DATED"
    ,AA."YEAR"
    ,AA."MONTH"
    ,aa."SHOP_ID"
    ,AA."ID_CHEK"
    ,AA."VOP"
    ,lpad(AA.SELLER_TAB, 8, '0') "SELLER_TAB"
    ,AA."SELLER_FIO"
    ,cast((AA."SUMP"-AA."SUMV") as number(18,2)) as fact_sale_sum
    ,case when c.mtart not in ('ZROH','ZHW3') then cast((KOLP-KOLV) as number(18)) else 0 end as FACT_PAIR_KOL
    ,AA."ART"
    , case when c.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутка' end as "SH_SOP"
    , case when c.facture = ' ' and c.manufactor != 'СООО БЕЛВЕСТ' then 'Покупная'
           when c.facture = ' ' and c.manufactor = 'СООО БЕЛВЕСТ' then 'Собственная' 
           else c.facture 
      end as "OWN_PURCHASE"
    , c.manufactor as "BRAND"
    , case when "ACTION" = 'F' then 'Белый' else 'Оранжевый' end as "CENNIK" --CHANGE AFTER
    , c.assort_torg as "ASSORT_TORG"
    , c.groupmw as "GROUPMW"
    FROM (
        SELECT 
        TRUNC(A.SALE_DATE) AS DATED
        ,EXTRACT(YEAR FROM A.SALE_DATE) as year
        ,EXTRACT(MONTH FROM A.SALE_DATE) as month
        ,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN 1 ELSE 2 END AS KOP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  1 ELSE -1 END AS KOEF 
        ,A.ID_CHEK
        ,A.SHOP_ID
        ,A.SELLER_FIO
        ,A.SELLER_TAB
        ,B.ART
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
        ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
        ,B.ACTION
        FROM POS_SALE2 B
        INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
        WHERE A.BIT_CLOSE = 'T'
        and A.SHOP_ID = '0001'
        and extract(year from a.sale_date) = 2019
        and EXTRACT(MONTH FROM A.SALE_DATE) = 5
    
        UNION ALL
    
        SELECT 
        COALESCE(C2.DATES, A2.dated) as dated
        ,EXTRACT(YEAR FROM COALESCE(C2.DATES, A2.dated)) as year
        ,EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) as month
        ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
        ,3 AS KOP
        ,-1 AS KOEF
        ,A2.ID AS ID_CHEK
        ,A2.ID_SHOP
        ,B2.SELLER_FIO
        ,B2.SELLER_TAB
        ,B2.ART
        ,0 AS SUMP1
        ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
        ,CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
        ,CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
        ,0  AS KOLP 
        ,B2.KOL AS KOLV
        ,B2.ACTION
        FROM D_PRIXOD2 B2
        INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
        --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
        LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                        SELECT p3.ID, p3.ID_SHOP, TRUNC(MAX(p3.DATE_S)) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = '0001' GROUP BY p3.ID, p3.ID_SHOP
                        --UNION ALL
                        --SELECT ID_PRIXOD AS ID, ID_SHOP, TRUNC(MAX(DATE_S)) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP = COALESCE('0001', ID_SHOP) GROUP BY ID_PRIXOD, ID, ID_SHOP
                        ) WHERE ID != 0 GROUP BY ID, ID_SHOP
                ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
        LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = COALESCE('0001', c.ID_SHOP) WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
        where
        ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19',/*'42',*/'45', '55')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in ('03','14','19',/*'42',*/'45', '55'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.dated) IS NOT NULL)))
        and a2.bit_close='T' 
        and A2.ID_SHOP = '0001'
        and extract(year from coalesce(c2.dates, a2.dated)) = 2019
        and EXTRACT(MONTH FROM COALESCE(C2.DATES, A2.dated)) = 5
    
        UNION ALL
    
        select 
        a.DATED
        ,EXTRACT(YEAR FROM a.dated) as year
        ,EXTRACT(MONTH FROM a.dated) as month
        ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
        ,4 AS KOP
        ,1 AS KOEF
        ,a.ID AS ID_CHEK
        ,b.id_shop
        ,b.SELLER_FIO
        ,b.seller_tab
        ,b.art ART
        ,0 AS SUMP1
        ,0 AS SUMV1 
        ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
        ,0 AS SUMV 
        ,b.kol AS KOLP
        ,0 AS KOLV
        ,b.ACTION
        from d_rasxod1 a
        inner join (
                select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, x.action, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2, nvl(x.kol,0) kol
                from (
                    select id, art, seller_fio, seller_tab, id_shop, action, sum(kol*cena3) sum, sum(kol) kol from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop, action
                ) x
                left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
                ) y on x.id = y.id and x.id_shop = y.id_shop
        ) b on a.id = b.id and a.id_shop = b.id_shop
    
        where a.bit_close = 'T' and a.idop in ('34','35',/*'37',*/'38','39','40', '44')
        and b.id_shop = '0001'
        and extract(year from a.dated) = 2019
        and EXTRACT(MONTH FROM a.dated) = 5
    ) aa
    left join s_art c on aa.art=c.art
) a

left join (
    select * from (
        select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
        from bep_prem_combination_percent cp
        inner join bep_prem_s_group sg on cp.id_group = sg.id_group
        inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
        inner join st_retail_hierarchy rh on gs.id_shop = rh.shopid
        left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
        left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
        where sg.year = 2019 and sg.month = 5 and id_shop = '0001'
        and cp.id_combination != 0
        group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
    ) a
) c on
    a.shop_id = c.id_shop
    and trunc(a.dated) >= case when c.dateb is not null then c.dateb else trunc(a.dated) end
    and trunc(a.dated) <= case when c.datee is not null then c.datee else trunc(a.dated) end
    and case when c.ids_criterion like '%'||1||'%' then c.combination else a.sh_sop end like '%'||a.sh_sop||'%' 
    and case when c.ids_criterion like '%'||2||'%' then c.combination else a.cennik end like '%'||a.cennik||'%'  
    and case when c.ids_criterion like '%'||3||'%' then c.combination else a.own_purchase  end like '%'||a.own_purchase ||'%'  
    and case when c.ids_criterion like '%'||4||'%' then c.combination else a.brand end like '%'||a.brand||'%'
    and case when c.ids_criterion like '%'||5||'%' then c.combination else a.assort_torg end like '%'||a.assort_torg||'%'
    and case when c.ids_criterion like '%'||6||'%' then c.combination else a.groupmw end like '%'||a.groupmw||'%'

left join (
    select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    inner join st_retail_hierarchy rh on gs.id_shop = rh.shopid
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
    where sg.year = 2019 and sg.month = 5 and id_shop = '0001'
    and sg.dateb is null and sg.datee is null
    and cp.id_combination = 0
    group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
) d on a.shop_id = d.id_shop

group by a."DATED", a."YEAR", a."MONTH", a."SHOP_ID", a."VOP", a."SELLER_TAB", 
    a."SELLER_FIO", a."FACT_SALE_SUM", a."ART", a."SH_SOP", a."OWN_PURCHASE" , 
    a."BRAND", a."CENNIK", a."FACT_PAIR_KOL", a."ASSORT_TORG", a."GROUPMW", 
    a."ID_CHEK" 

--order by  a."ID_CHEK", a."FACT_SALE_SUM"
;

select * from tem_bep_prem_sales_crit_test;

select 
seller_tab, 
max(seller_fio) seller_fio, 
sum(fact_sale_sum) fact_sale_sum, 
sum(fact_pair_kol) fact_pair_kol,
sum(prem_sale) prem_sum
from tem_bep_prem_sales_crit_test 
where id_shop = '0001' and year = 2019 and month = 5 
group by seller_tab;


select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, sg.region, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
from bep_prem_combination_percent cp
inner join bep_prem_s_group sg on cp.id_group = sg.id_group
inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
where sg.year = 2019
and sg.month = 5
--and sg.dateb is not null and sg.datee is not null
and cp.id_combination != 0
and gs.id_shop = '0001'
group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, sg.region, cp.percent
order by sg.dateb, percent desc;


select * from bep_prem_seller where id_shop = '0001' and year = 2019 and month = 5;

select sum(fact_sale_sum), sum(fact_pair_kol), sum(prem_sale) --into v_seller_sales_sum, v_seller_pair_kol, v_seller_prem
from tem_bep_prem_sales_crit_test 
where id_shop = '0001' and year = 2019 and month = 5 and seller_tab = '00000181'
and trunc(dated) >= to_date('20.05.19','dd.mm.yy') and trunc(dated) <= to_date('31.05.19','dd.mm.yy')
and case when '5/6' like '%'||1||'%' then 'Полуботинки/Мужские' else SH_SOP end like '%'||SH_SOP||'%' 
and case when '5/6' like '%'||2||'%' then 'Полуботинки/Мужские' else CENNIK end like '%'||CENNIK||'%'  
and case when '5/6' like '%'||3||'%' then 'Полуботинки/Мужские' else OWN_PURCHASE end like '%'||OWN_PURCHASE||'%'  
and case when '5/6' like '%'||4||'%' then 'Полуботинки/Мужские' else BRAND end like '%'||BRAND||'%' 
and case when '5/6' like '%'||5||'%' then 'Полуботинки/Мужские' else assort_torg end like '%'||assort_torg||'%'
and case when '5/6' like '%'||6||'%' then 'Полуботинки/Мужские' else GROUPMW end like '%'||GROUPMW||'%';