
  CREATE OR REPLACE FORCE VIEW "FIRM"."BEP_PREM_DAY_SHOP_PLAN" ("DATE_N", "ID_SHOP", "YEAR", "MONTH", "DAY", "PLAN_SUM", "PLAN_PAIR") AS 
  select date_n, b.id_shop id_shop, extract(year from date_n) year, extract(month from date_n) month, extract(day from date_n) day,
        nvl(e.plan_sum, d.plan_sum) plan_sum,
        nvl(e.plan_pair, d.plan_pair) plan_pair
        from 
        (
            select to_date(( 1 + level - 1)||'.'||1||'.'||2019, 'dd.mm.yyyy') date_n,  1 + level - 1 day, extract(month from sysdate) month, extract(year from sysdate) year 
            from dual connect by level <= extract(day from last_day(to_date('1'||'.'||'2019','mm.yyyy')))
        ) a 
        inner join bep_prem_shop_plan b on b.id_shop = b.id_shop and a.month = b.month and a.year = b.year
        left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and a.month = d.month and a.year = d.year
        left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and a.date_n = trunc(e.edit_date)
        --order by shop, date_n
;
