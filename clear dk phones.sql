select count(id_dk) from delivery_dk where id_del = 3142;

delete from delivery_dk a
where a.id_dk in( select a.id_dk from delivery_dk a
  inner join t_day_st_dk1 b on a.id_dk = b.id_dk
  where 
  a.id_dk not in (select id_dk from white_list) 
  and a.id_del = 3142 
  and substr(b.phone_number,2) in (
        select  substr(b.phone_number,2)
        from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk
        where id_del = 3142
        group by id_del, substr(b.phone_number,2) having count(1)>1 
  )
  and (substr(b.phone_number,2), b.sale_date/*b.activation_date*/) not in(
        select substr(b.phone_number,2), max(b.sale_date)/*min(b.activation_date) as first_date*/ from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk
        where a.id_del = 3142
        group by substr(b.phone_number,2) having count(1)>1
  )
);

select a.id_dk from delivery_dk a
inner join t_day_st_dk1 b on a.id_dk = b.id_dk
where 
a.id_dk not in (select id_dk from white_list) 
and a.id_del = 3142 
and substr(b.phone_number,2) in (
    select  substr(b.phone_number,2)
    from delivery_dk a
    inner join t_day_st_dk1 b on a.id_dk = b.id_dk
    where id_del = 3142
    group by id_del, substr(b.phone_number,2) having count(1)>1 
)
    and (substr(b.phone_number,2), b.activation_date) not in(
    select substr(b.phone_number,2), min(b.activation_date) as first_date from delivery_dk a
    inner join t_day_st_dk1 b on a.id_dk = b.id_dk
    where a.id_del = 3142
    group by substr(b.phone_number,2) having count(1)>1
)
;

select rowid, id_dk from delivery_dk where id_del = 3142;
select b.id_dk, substr(b.phone_number,2), b.sale_date, b.activation_date from delivery_dk a inner join t_day_st_dk1 b on a.id_dk = b.id_dk where a.id_del = 3142;

delete from delivery_dk where id_del = 3142
and id_dk in (
    select a.id_dk from delivery_dk a
    inner join t_day_st_dk1 b on a.id_dk = b.id_dk 
    where (substr(b.phone_number,2), b.sale_date) in (
        select substr(b.phone_number,2), b.sale_date from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk 
        where a.id_del = 3142
    ) and (substr(b.phone_number,2), b.sale_date) not in ( 
        select substr(b.phone_number,2), max(b.sale_date)  from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk 
        where a.id_del = 3142
        group by substr(b.phone_number,2)
    )
    
)
;

delete from delivery_dk where id_del = 3142
and id_dk in (
    select a.id_dk from delivery_dk a
    inner join t_day_st_dk1 b on a.id_dk = b.id_dk 
    where (substr(b.phone_number,2), b.sale_date) in (
        select substr(b.phone_number,2), b.sale_date from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk 
        where a.id_del = 3142
        
        minus
        
        select substr(b.phone_number,2), max(b.sale_date)  from delivery_dk a
        inner join t_day_st_dk1 b on a.id_dk = b.id_dk 
        where a.id_del = 3142
        group by substr(b.phone_number,2)
    )
)
;


select count(1) from (
    select id_dk, phone_number from t_day_st_dk1 where
    case 
    when phone_number is null or phone_number = ' ' then 'Не заполнен номер телефона.'
    when regexp_replace(phone_number, '[^[:digit:]]')  != phone_number then 'В номере телефона недопустимые символы.'
    when length(phone_number) != 11 and dk_country = 'RU' then 'Номер телефона не равен 11 цифрам.'
    when length(phone_number) != 12 and dk_country = 'BY' then 'Номер телефона не равен 11 цифрам.'
    when (substr(phone_number,1,1) not in ('8','7')  ) and dk_country = 'RU' then 'В номере телефона неверный код страны(Необходим 7 или 8).'
    when substr(phone_number,1,3) != '375' and dk_country = 'BY' then 'В номере телефона неверный код страны(Необходим 375).'
    when instr(phone_number, '1234567') !=0 then 'В немере телефона содержатся последовательно идущие цифры 12345678.'
    when instr(phone_number, '7654321') !=0 then 'В немере телефона содержатся последовательно идущие цифры  7654321.'
    when (select MAX(length(phone_number) - length(replace(phone_number, level-1,''))) FROM dual connect by 1=1 and level <= 10) > 7 then 'В номере телефона содержится много одинаковых цифр.'
    end is not null
)
;

select count(1) from t_day_st_dk1;


call use_filters_new(3142, 'F');
call clear_deliv_black_list(3142);
call clear_double_phones(3142);