-- ДАННЫЕ ПО ДК
select distinct a."ID_DK",a."FIO",a."BIRTHDAY",a."SEX",a."COUNTRY",a."CITY",
a."STREET", 
get_clean_phone(a."PHONE_NUMBER") PHONE_NUMBER ,
a."EMAIL",
a."DISCOUNT",
      a."ACTIVATION_DATE",a."SUMM",a."ID_SHOP",
      --a."DKVID_ID",a."EDIT_DATE",a."BIT_BLOCK",a."SENT_SAP",a."DISC_SCORES_START",
      a."SCORES",
      --a."DK_TYPE",
      a."DK_LEVEL",
      --a."SCORES_DATE",
      --a."SENT_SAP_TIME",
      a."DK_COUNTRY",a."INF1",
      --a."INF2",a."INF3",
      a."FAM",
      a."OTCH",
      get_russian_name(a.ima) "IMA", b.sale_date,
      --nvl(d.name, 'Уважаемый покупатель') IMA, --a."IMA",
      upper( get_last_sms_status(a.id_dk, 'description')) "SMS_STATUS",
      upper( get_last_viber_status(a.id_dk, 'description')) "VIBER_STATUS",
      --d.name, 
      ' ' as "DOP_INFO"
from firm.st_dk a
left join DK_LAST_SALE b on a.id_dk = b.id_dk
where a.bit_block = 'F'
;
    
-- ДАННЫЕ ПО POS_SALE1
select count(*) from firm.pos_sale1 where id_dk is not null and id_dk != ' '
;
    
select sale_date from dk_last_sale where id_dk = '0000012533251';
                                
                    
select a.id_dk, b.name_channel, c.id_status,  c.name_status, description from DELIVERY_HISTORY a
inner join s_channel b on a.id_channel = b.id_channel
inner join s_status c on a.id_status = c.id_status
where (a.id_dk, a.date_sent) in
(
    select x.id_dk, max(x.date_sent) from delivery_history x 
    where x.id_channel is not null 
    and x.id_del in (select id_del from delivery_pool where status = 'DONE')
    group by id_dk
)
and id_dk = '0000012533251';

select a.*, get_last_sms_status(a.id_dk, 'description'), get_last_viber_status(a.id_dk, 'description') from delivery_dk a where a.id_del = 2449;
