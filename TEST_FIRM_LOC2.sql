-- ДК : 0000021470264 ||| 0000012122509 || 0000015767196 || 0000020341824
-- ДАТА : to_timestamp('13.04.17 23:59:59','dd.mm.yy. hh24:mi:ss')

select * from pos_dk_stock1 order by dateb;

select * from tem_dk_scores_info;

select sum(cnt) from (  
    select  a.id_chek, a.id_ac, a.scores_type, count(a.id_dk) as cnt from pos_dk_scores_firm a
    where
    a.dk_sum > 0 
    and doc_type = 'ac'
    group by a.id_chek, a.id_ac, a.scores_type
    order by id_chek
) b where b.id_ac is null
;

select a.*
from pos_dk_scores a
where a.id_dk = '0000018751031'
and a.dates <= trunc(to_timestamp('30.04.17 23:59:59','dd.mm.yy. hh24:mi:ss'), 'ddd') 
order by a.dates
;

select a.*, case when a.id_ac is not null then a.id_ac else a.scores_type end scores_type123
from pos_dk_scores_firm a
where a.id_dk = '0000015767196'
and a.dates <= trunc(to_timestamp('30.07.17 23:59:59','dd.mm.yy. hh24:mi:ss'), 'ddd') 
order by a.dates
;

select a.*
from pos_dk_scores_firm a
where a.id_dk = '0000012122509'
and a.dates <= trunc(to_timestamp('31.10.17 23:59:59','dd.mm.yy. hh24:mi:ss'), 'ddd') 
order by a.dates
;

-- ТИПЫ НАЧИСЛЯВШИХСЯ БАЛЛОВ У ДК К ЗАДАННОМУ ВРЕМЕНИ
select nvl(a.id_ac, 'stnd')  from pos_dk_scores_firm a 
where a.id_dk = '0000012122509'
and a.dk_sum > 0
and a.dates <= to_timestamp('31.10.17', 'dd.mm.yy')
group by a.id_ac
order by a.id_ac
;

-- СУММА НАЧИСЛЕНИЙ ДО ЭТОГО ДНЯ ПО СТАНДАРТНЫМ
select 
*
--nvl(sum(dk_sum), 0) 
from pos_dk_scores_firm a
where id_dk = '0000018212877'
and dk_sum > 0
and doc_type = 'ck'
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(dates, 'ddd')
;

-- СУММА СПИСАНИЙ ДО ЭТОГО ДНЯ ПО СТАНДАРТНЫМ
select 
*
--nvl(sum(dk_sum), 0) 
from pos_dk_scores_firm a
where a.id_dk = '0000018212877'
and a.dk_sum < 0
and a.doc_type = 'ck'
and (a.id_ac is null or a.id_ac = 'TEST0009') -- учесть "акцию" списание через год
and a.scores_type = 'stnd'
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(a.dates, 'ddd')
;

-- СУММА НАЧИСЛЕНИЙ ДО ЭТОГО ДНЯ ПО АКЦИОННЫМ
select 
*
--nvl(sum(dk_sum), 0) 
from pos_dk_scores_firm a
where a.id_dk = '0000018212877'
and a.dk_sum > 0
and a.doc_type = 'ac'
and  (a.id_ac is not null or a.id_chek = 'SC100001')
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(a.dates, 'ddd')
order by dates
;

-- СУММА СПИСАНИЙ ДО ЭТОГО ДНЯ ПО АКЦИООННЫМ
select 
*
--nvl(sum(dk_sum), 0) 
from pos_dk_scores_firm a
where a.id_dk = '0000018212877'
and a.dk_sum < 0
and
(   
    (
        a.doc_type = 'ac' 
        --and a.id_ac is not null
        and a.scores_type = 'stnd'
        and a.id_shop = 'FIRM'
        and a.id_ac != 'TEST0009' -- учесть "акцию" списание через год
    )
    or
    (
        a.doc_type = 'ck'
        --and a.id_ac is null
        and a.scores_type != 'stnd'
    )
)  
and to_timestamp('31.10.17', 'dd.mm.yy') >= trunc(a.dates, 'ddd')
;
  
select * from pos_dk_scores_firm a
where a.id_dk = '0000012122509'
and a.dk_sum > 0
and a.doc_type = 'ac'
and a.id_ac = 'AC030001'
order by a.dates;

select * from pos_dk_scores_firm a
where a.id_dk = '0000012122509'
and a.dk_sum <= 0
and a.doc_type = 'ac'
and a.id_shop = 'FIRM'
and a.id_chek = 'AC030001'
order by a.dates;
