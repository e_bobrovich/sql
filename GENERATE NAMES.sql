-- РАЗДЕЛЕНИЕ НА ЧАСТИ

select * from words_source;

insert into words_part(id_word, part)
select 1, regexp_substr('ма ма','[^ ]+', 1, level) from dual
connect by regexp_substr('ма ма', '[^ ]+', 1, level) is not null;

select substr(word, instr(word, ' ')) from words_source;

select case when instr('фаб ри ка', ' ', 1, 1) = 0 then replace('фаб ри ка',' ') else substr('фаб ри ка',0, instr('фаб ри ка', ' ')-1) end from dual;
select case when instr('фаб ри ка', ' ', 1, 2) = 0 then replace('фаб ри ка',' ') else replace(substr('фаб ри ка',0, instr('фаб ри ка', ' ', 1, 2)-1),' ') end from dual;
select case when instr('фаб ри ка', ' ', 1, 3) = 0 then replace('фаб ри ка',' ') else replace(substr('фаб ри ка',0, instr('фаб ри ка', ' ', 1, 3)-1),' ') end from dual;

select word_result as "РЕЗУЛЬТАТ", words "ИСХОДНЫЕ СЛОВА" from words_result order by words;


select a.*, replace(b.word, ' ', '') word from words_part a
                            inner join words_source b on a.id_word = b.id_word;
                            
                            
select a1.id_part part1, b1.id_part part2, a1.part||b1.part new_word, a1.word||','||b1.word source_words from 
(
    select a.id_part, a.part, b.word from words_part a
    inner join words_source b on a.id_word = b.id_word
)a1,(
    select a.id_part, a.part, b.word from words_part a
    inner join words_source b on a.id_word = b.id_word
) b1
;

select * from (
    select replace(a2.new_word||b2.part, ' ', '') new_word3, replace(a2.source_words||','||b2.source_word,', ', '') source_words3 from 
    (
        select a.part, b.word source_word from words_part a
        inner join words_source b on a.id_word = b.id_word
        union
        select ' ' part, ' ' word from dual
    ) b2,
    (
        (
            select a1.part||b1.part new_word, a1.word||','||b1.word source_words from 
            (
                select a.id_part, a.part, b.word from words_part a
                inner join words_source b on a.id_word = b.id_word
            )a1,(
                select a.id_part, a.part, b.word from words_part a
                inner join words_source b on a.id_word = b.id_word
            ) b1
        
        )
    )a2
)
;


select a.part, b.word from words_part a
inner join words_source b on a.id_word = b.id_word
union
select ' ' part, ' ' word from dual
;


--------------------------------------------------------------------------------



select * from (
    select replace(a2.new_word||b2.part, ' ', '') new_word3, replace(a2.source_words||','||b2.source_word,', ', '') source_words3 from 
    (
        select a.part, b.word source_word from words_part a
        inner join words_source b on a.id_word = b.id_word
        union
        select ' ' part, ' ' word from dual
    ) b2,
    (
        (
            select a1.part||b1.part new_word, a1.word||','||b1.word source_words from 
            (
                select a.id_part, a.part, b.word from words_part a
                inner join words_source b on a.id_word = b.id_word
            )a1,(
                select a.id_part, a.part, b.word from words_part a
                inner join words_source b on a.id_word = b.id_word
            ) b1
        
        )
    )a2
) where length(new_word3) <= 9
;


truncate table word_res_temp;


insert into word_res_temp
select * from (
    select 
        --replace(a2.new_word||b2.part, ' ', '') new_word3, 
        --replace(a2.source_words||','||b2.source_word,', ', '') source_words3 
        
        replace(b2.part||a2.new_word, ' ', '') new_word3, 
        replace(b2.source_word||','||a2.source_words,' ,', '') source_words3 
    from 
    (
        select a.part, b.word source_word from words_part a
        inner join words_source b on a.id_word = b.id_word
        --where length(a.part) < 5
        union
        select ' ' part, ' ' word from dual
    ) b2,
    (
        (
            select a1.part||b1.part new_word, a1.word||','||b1.word source_words from 
            (
                select  a.part, b.word from words_part a
                inner join words_source b on a.id_word = b.id_word
                --where length(a.part) >= 5
            )a1,(
                select a.part, b.word from words_part a
                inner join words_source b on a.id_word = b.id_word
                --where length(a.part) < 5
            ) b1
            where  length(a1.part||b1.part) <= 9 and length(a1.part||b1.part) >= 5
        )
    )a2
    where  length(replace(a2.new_word||b2.part, ' ', '')) <= 9
    --and substr(a2.source_words, instr(a2.source_words, ',')+1) != b2.part
    and substr(a2.source_words, 0, instr(a2.source_words, ',')-1) != b2.part
) where length(new_word3) <= 9
;

 select distinct * from word_res_temp 
 where length(word_result) <= 8
 order by 
 length(word_result)
 , 
 word_result
 ;

select count(*) from (
    select distinct * from word_res_temp where length(word_result) <= 8
)
;     