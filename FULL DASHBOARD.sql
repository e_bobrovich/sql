--------------------------------------------------------------------------------
-- РЕЙТИНГ
--В отчетах ТОПов (дирекция, оперативная информация) есть файл реализация. Он каждый день обновляется данными всей сети. 
--Там ежедневно присваивается рейтинг отделениям. Т,е. эти данные есть в системе.
--В дашборде на кассе каждый следующий день они должны видеть, какие они были в рейтинге вчера.
--------------------------------------------------------------------------------

select id_shop, reit2 from tdv_map_shop_reit where id_shop = '0008';

select a.id_shop, rownum, a.reit2 from
(
    select a.id_shop, reit2 from tdv_map_shop_reit a
    inner join spa_fil_shop b on a.id_shop = b.shp
    where b.fil = (select fil from spa_fil_shop where shp = '0008')
    order by reit2
) a;


select id_shop, rating
from (
    select a.id_shop, rownum rating
      from (select a.id_shop, reit2
      from tdv_map_shop_reit a
      inner join st_shop b on a.id_shop = b.shopid
      where b.landid = (select landid from st_shop where shopid = '0063')
      order by reit2
  ) 
a)
where id_shop = '0063';

select fil from spa_fil_shop where shp = '0008';
select landid from st_shop where shopid = '0008';


select id_shop, sum(fact_sale_sum), sum(fact_pair_kol)  from bep_prem_month_sales 
group by id_shop;



select id_shop, sum(fact_sale_sum) sale_sum from bep_prem_month_sales group by id_shop order by sum(fact_sale_sum) desc;

select * from bep_prem_month_sales where id_shop = '3917' and trunc(dates) = trunc(sysdate);

--------------------------------------------------------------------------------
-- РЕЙТИНГ ЗА МЕСЯЦ
--------------------------------------------------------------------------------
select id_shop, rating from (    
    select id_shop, rownum rating from (
        select id_shop, fact_sum from bep_Prem_shop_plan a
        inner join st_shop b on a.id_shop = b.shopid 
        where year = extract(year from sysdate) and month = extract(month from sysdate)
        and b.landid = (select landid from st_shop where shopid = '0001')
        order by fact_sum desc
    )
)  where id_shop = '0001'
;

--------------------------------------------------------------------------------
-- РЕЙТИНГ ЗА ГОД
--------------------------------------------------------------------------------
select id_shop, rating from (    
    select id_shop, year_sum, rownum rating from ( 
        select a.id_shop,a.year, a.month, a.year_sum from (
            select id_shop,year, month,fact_sum, 
            sum(fact_sum) over (partition by id_shop,year order by id_shop,year, month rows unbounded preceding) year_sum 
            from bep_prem_shop_plan 
            --where year = extract(year from sysdate) and month <= extract(month from sysdate)
            where year = 2018 and month <= 9
        ) a
        inner join st_shop b on a.id_shop = b.shopid 
        --where year = extract(year from sysdate) and month = extract(month from sysdate)
        where a.year = 2018 and a.month = 9
        and b.landid = (select landid from st_shop where shopid = '0001')
        order by year_sum desc
    ) 
)where id_shop = '0001'
;

select id_shop, rating from (    
    select id_shop, year_sum, rownum rating from ( 
        select a.id_shop,a.year, a.month, a.year_sum from (
            select id_shop,year, month,fact_sum, 
            sum(fact_sum) over (partition by id_shop,year order by id_shop,year, month rows unbounded preceding) year_sum 
            from bep_prem_shop_plan 
            where year = extract(year from sysdate) and month <= extract(month from sysdate)
        ) a
        inner join st_shop b on a.id_shop = b.shopid 
        where year = extract(year from sysdate) and month = extract(month from sysdate)
        and b.landid = (select landid from st_shop where shopid = '0001')
        order by year_sum desc
    ) 
)where id_shop = '0001'
;


--------------------------------------------------------------------------------
-- КОНВЕРСИЯ OLD
--конверсия - как в отчете 9205 (конверсия по обмену). 
--Конверсия продаж – соотношение между числом потенциальных покупателей и покупателей, совершивших покупку товара.
--------------------------------------------------------------------------------
select id_shop, kol from pos_shedule1 where trunc(dated) = trunc(sysdate)-1 and id_shop = '0001'; 

select 
nvl(a.id_shop, b.id_shop) id_shop, 
nvl(a.sales_count_other, 0) sales_count_other, 
nvl(b.customer_count_dk, 0) customer_count_dk,
nvl(c.kol, 0),
case when c.kol = 0 or c.kol is null then 0 else cast(((nvl(b.customer_count_dk, 0) + nvl(a.sales_count_other, 0)) /c.kol)*100 as number(18,2)) end conversion
from (
    select id_shop, count(*) sales_count_other  from pos_sale1 
    where id_shop = '0004' and trunc(sale_date) = trunc(sysdate)-1 and id_dk = ' '
    group by id_shop, id_dk
) a
full join 
(
    select id_shop, count(id_dk) customer_count_dk  from pos_sale1 
    where id_shop = '0004' and trunc(sale_date) = trunc(sysdate)-1 and id_dk != ' '
    group by id_shop
) b on a.id_shop = b.id_shop
full join
(
    select id_shop, kol from pos_shedule1 where trunc(dated) = trunc(sysdate)-1 and id_shop = '0004'
) c on case when a.id_shop is not null then a.id_shop else b.id_shop end = c.id_shop
;

--------------------------------------------------------------------------------
-- КОНВЕРСИЯ NEW
-- считаем ее как соотношение продаж по обуви к посетителям
--------------------------------------------------------------------------------

select sum(kol) from pos_shedule1 where to_char(dated, 'yyyymm') = to_char(sysdate, 'yyyymm') and id_shop = '0001'; 

select count(1) from bep_prem_month_sales where id_shop = '0001' and vop = 'ПРОДАЖА' and sh_sop = 'Обувь';

select nvl(a.id_shop, b.id_shop) id_shop, 
cast(case when b.cnt_visitors = 0 or b.cnt_visitors is null then 0 else (nvl(a.cnt_sales, 0)/b.cnt_visitors)*100 end as number(18,2)) conversion ,
A.cnt_sales, b.cnt_visitors  FROM 
(select id_shop,count(1) cnt_sales from bep_prem_month_sales where id_shop = '0001' and vop = 'ПРОДАЖА' and sh_sop = 'Обувь' and sale_date < sysdate group by id_shop) a
inner join
(select id_shop,sum(kol) cnt_visitors from pos_shedule1 where to_char(dated, 'yyyymm') = to_char(sysdate, 'yyyymm') and id_shop = '0001' group by id_shop) b
on a.id_shop = b.id_shop
;
--------------------------------------------------------------------------------
-- СРЕДНЯЯ ПАРА
--сумма товарооборота по обуви/ к-во проданных пар по обуви
--Обновлять его надо в режиме онлайн с каждым пробитым чеком

--------------------------------------------------------------------------------
select id_shop, 
case 
    when sum(fact_pair_kol) = 0 then 0 
    else cast(nvl(sum(fact_sale_sum), 0)/sum(fact_pair_kol) as number(18,2)) end average_pair  
from bep_prem_month_sales  where id_shop = '0010' and vop = 'ПРОДАЖА' and sh_sop = 'Обувь' group by id_shop;

select * from bep_prem_month_sales  where id_shop = '0001' and vop = 'ПРОДАЖА' and sh_sop = 'Обувь';
select id_shop, sum(fact_sale_sum) to_sum from bep_prem_month_sales  where id_shop = '0001' and vop = 'ПРОДАЖА' and sh_sop = 'Обувь' group by id_shop;
select id_shop, sum(fact_pair_kol) pair_sum from bep_prem_month_sales  where id_shop = '0001' and vop = 'ПРОДАЖА' and sh_sop = 'Обувь' group by id_shop;



select * from  bep_prem_month_sales where id_shop = '0001' and vop = 'ПРОДАЖА';

select id_shop, cast(avg(chek_sum) as number(18,2)) average_check from (
    select id_shop, id_chek, sum(fact_sale_sum) chek_sum from  bep_prem_month_sales where id_shop = '0001' and vop = 'ПРОДАЖА' group by id_shop, id_chek
) group by id_shop
;

select 
nvl(a.id_shop, b.id_shop) id_shop, 
case when b.pair_sum = 0 then 0 else cast(nvl(a.to_sum, 0)/b.pair_sum as number(18,2)) end average_pair
from 
(select id_shop, sum(fact_sale_sum) to_sum from bep_prem_month_sales  where vop = 'ПРОДАЖА' and sh_sop = 'Обувь' group by id_shop) a
full join
(select id_shop, sum(fact_pair_kol) pair_sum from bep_prem_month_sales  where vop = 'ПРОДАЖА' and sh_sop = 'Обувь' group by id_shop) b on a.id_shop = b.id_shop;


--------------------------------------------------------------------------------
-- СРЕДНИЙ ЧЕК
--редний чек это отношение суммы товарооборота в руб к количеству пробитых чеков. 
--Обновлять его надо в режиме онлайн с каждым пробитым чеком

-- ??? СЧИТАТЬ ЛИ ЧЕКОМ ТОЛЬКО ПРОДАЖИ, ЛИБО БЕЗНАЛ ТОЖЕ ???
--------------------------------------------------------------------------------
select sum(fact_sale_sum), count(1) to_sum from bep_prem_month_sales a 
where a.id_shop = '0008' 
;

select 
nvl(a.id_shop, b.id_shop) id_shop, 
nvl(a.to_sum, 0) to_sum, 
nvl(b.count_check,0) count_check,
case when b.count_check = 0 then 0 else cast(nvl(a.to_sum, 0)/b.count_check as number(18,2)) end average_check
from 
(select id_shop, sum(fact_sale_sum) to_sum from bep_prem_month_sales  where id_shop = '0008' and vop = 'ПРОДАЖА' group by id_shop) a
full join
(select id_shop, count(1) count_check from bep_prem_month_sales where id_shop = '0008' and vop = 'ПРОДАЖА' group by id_shop) b on a.id_shop = b.id_shop
;


select id_shop, cast(avg(fact_sale_sum) as number(18,2)) average_check from bep_prem_month_sales where id_shop = '0001' and vop = 'ПРОДАЖА' group by id_shop;

select id_shop, cast(avg(chek_sum) as number(18,2)) average_check from (
    select id_shop, id_chek, sum(fact_sale_sum) chek_sum from  bep_prem_month_sales where id_shop = '0001' and vop = 'ПРОДАЖА' group by id_shop, id_chek
) group by id_shop
;

--------------------------------------------------------------------------------
-- СРЕДНЯЯ ПАРА
--------------------------------------------------------------------------------
select a.id_shop, average_pair, red_from, red_to_yellow_from, yellow_to_green_from, green_to  from
(
    select
    id_shop,
    case 
    when sum(fact_pair_kol) = 0 then 0 
    else cast(nvl(sum(fact_sale_sum), 0)/sum(fact_pair_kol) as number(18,2)) end average_pair 
    from bep_prem_month_sales 
    where id_shop = '0001' and upper(vop) = upper('продажа') and upper(sh_sop) = upper('обувь') group by id_shop
) a
left join 
(
    select 
    a.id_shop, 
    nvl(b.red_from, a.red_from) red_from ,
    nvl(b.red_to_yellow_from, a.red_to_yellow_from) red_to_yellow_from ,
    nvl(b.yellow_to_green_from, a.yellow_to_green_from) yellow_to_green_from ,
    nvl(b.green_to, a.green_to) green_to
    from (select '0063' id_shop, 50 red_from,110 red_to_yellow_from, 130 yellow_to_green_from, 200 green_to from dual) a
    left join
    bep_prem_average_pair_ranges b on a.id_shop = b.id_shop and b.year = extract(year from sysdate) and b.month = extract(month from sysdate)
) b on a.id_shop = b.id_shop
    
;

select * from bep_prem_month_sales  where id_shop = '0001' and upper(vop) = upper('продажа') and upper(sh_sop) = upper('обувь');

select 
a.id_shop, 
nvl(b.red_from, a.red_from) red_from ,
nvl(b.red_to_yellow_from, a.red_to_yellow_from) red_to_yellow_from ,
nvl(b.yellow_to_green_from, a.yellow_to_green_from) yellow_to_green_from ,
nvl(b.green_to, a.green_to) green_to
from (select '0063' id_shop, 50 red_from,110 red_to_yellow_from, 130 yellow_to_green_from, 200 green_to from dual) a
left join
bep_prem_average_pair_ranges b on a.id_shop = b.id_shop and b.year = extract(year from sysdate) and b.month = extract(month from sysdate)
;
--------------------------------------------------------------------------------
-- ВЫПОЛНЕНИЕ ПРОДАЖ, ПАР ДЕНЬ
--------------------------------------------------------------------------------
select  
nvl(a.id_shop, b.id_shop) id_shop, 
nvl(day_fact_sale, 0) fact_sale,
nvl(day_plan_sale, 0) plan_sale, 
nvl(day_fact_pair, 0) fact_pair, 
nvl(day_plan_pair, 0) plan_pair 
from 
(select id_shop, sum(fact_sale_sum) day_fact_sale, sum(fact_pair_kol) day_fact_pair from bep_prem_month_sales where id_shop = '0008' and trunc(dates) = trunc(sysdate) group by id_shop) a 
full join
(select id_shop, cast(plan_sum/extract(day from LAST_DAY(sysdate)) as number(18,2)) day_plan_sale, cast(plan_pair/extract(day from LAST_DAY(sysdate)) as number(18,2)) day_plan_pair from bep_prem_shop_plan where id_shop = '0008' and year = extract(year from sysdate) and month = extract(month from sysdate)) b on a.id_shop = b.id_shop
;

select id_shop, cast(plan_sum/extract(day from LAST_DAY(sysdate)) as number(18,2)) day_plan_sale, cast(plan_pair/extract(day from LAST_DAY(sysdate)) as number(18,2)) day_plan_pair from bep_prem_shop_plan where id_shop = '0008' and year = extract(year from sysdate) and month = extract(month from sysdate);

select * from bep_prem_day_shop_plan where id_shop = '0001' and trunc(date_n) = trunc(sysdate);

select  
nvl(a.id_shop, b.id_shop) id_shop, 
nvl(day_fact_sale, 0) fact_sale,
nvl(day_plan_sale, 0) plan_sale, 
nvl(day_fact_pair, 0) fact_pair, 
nvl(day_plan_pair, 0) plan_pair 
from 
(select id_shop, sum(fact_sale_sum) day_fact_sale, sum(fact_pair_kol) day_fact_pair from bep_prem_month_sales where id_shop = '0001' and trunc(dates) = trunc(sysdate) group by id_shop) a 
full join
(select id_shop, plan_sum day_plan_sale, plan_pair day_plan_pair from bep_prem_day_shop_plan where id_shop = '0001' and trunc(date_n) = trunc(sysdate)) b  on a.id_shop = b.id_shop
;

select nvl(a.id_shop, b.id_shop) id_shop,
     nvl(day_fact_sale, 0)     fact_sale,
     nvl(day_plan_sale, 0)     plan_sale,
     nvl(day_fact_pair, 0)     fact_pair,
     nvl(day_plan_pair, 0)     plan_pair
from (
    select id_shop, sum(fact_sale_sum) day_fact_sale, sum(fact_pair_kol) day_fact_pair
    from bep_prem_month_sales
    where id_shop = '0001'
    and trunc(dates) = trunc(sysdate)
    group by id_shop
) a
full join (
    select trunc(sysdate) date_n, b.id_shop id_shop,
    nvl(e.plan_sum, d.plan_sum) day_plan_sale,
    nvl(e.plan_pair, d.plan_pair) day_plan_pair
    from
    bep_prem_shop_plan b
    left join bep_prem_def_day_shop_plan d on b.id_shop = d.id_shop and b.month = d.month and b.year = d.year
    left join bep_prem_edit_day_shop_plan e on b.id_shop = e.id_shop and trunc(sysdate) = trunc(e.edit_date)
    where b.id_shop = '0001' and extract(month from sysdate) = b.month and extract(year from sysdate) = b.year
) b on a.id_shop = b.id_shop
;
--------------------------------------------------------------------------------
-- ВЫПОЛНЕНИЕ ПРОДАЖ, ПАР МЕСЯЦ
--------------------------------------------------------------------------------
select 
nvl(a.id_shop, b.id_shop) id_shop, 
nvl(month_fact_sale, 0) fact_sale, 
nvl(month_plan_sale, 0) plan_sale, 
nvl(month_fact_pair, 0) fact_pair, 
nvl(month_plan_pair, 0) plan_pair
from
(select id_shop, sum(fact_sale_sum) month_fact_sale, sum(fact_pair_kol) month_fact_pair from bep_prem_month_sales where id_shop = '0008' group by id_shop) a
full join
(select id_shop, plan_sum month_plan_sale, plan_pair month_plan_pair from bep_prem_shop_plan where id_shop = '0008' and year = extract(year from sysdate) and month = extract(month from sysdate)) b on a.id_shop = b.id_shop
;

--------------------------------------------------------------------------------
-- ГРАФИК
--10:00-22:00; один час
--------------------------------------------------------------------------------

--factPointGraphList
select hours.id_shop, hours.n, sum(fact_sale_sum) over(partition by hours.id_shop order by hours.n rows unbounded preceding) fact_sale_sum  from
(
    select a.id_shop, a.n from
    (select '0063' id_shop, 1 + level - 1 n from dual connect by level <= 24 - 1 + 1) a
    full join
    (select '0063' id_shop, hourb, houre  from st_shop_work_time where id_shop =  '7'||substr('0063',2)) b on a.id_shop = b.id_shop
    where a.n >= b.hourb and a.n <= b.houre order by a.n
) hours
left join
(
    select id_shop, hour, sum(fact_sale_sum) fact_sale_sum from bep_prem_month_sales where trunc(dates) = trunc(sysdate) and id_shop = '0063' group by id_shop, hour order by hour
) sales on hours.id_shop = sales.id_shop and hours.n = sales.hour
;

--planPointGraphList
select * from
(select a.id_shop, a.n from
    (select '0008' id_shop, 1 + level - 1 n from dual connect by level <= 24 - 1 + 1) a
    full join
    (select '0008' id_shop, hourb, houre  from st_shop_work_time where id_shop =  '7'||substr('0008',2)) b on a.id_shop = b.id_shop
    where a.n >= b.hourb and a.n <= b.houre order by a.n
)
cross join 
(select 2926.9 from dual)
;

--full graph points
select plan.id_shop, plan.n, plan.plan_value, nvl(fact.fact_sale_sum, 0) fact_sale_sum from
(    select id_shop, n, plan_value from
    (select a.id_shop, a.n from
        (select '0063' id_shop, 1 + level - 1 n from dual connect by level <= 24 - 1 + 1) a
        left join
        (select a.shopid id_shop, cast(substr(time_start, 0, instr(time_start, ':')-1) as number) hourb, cast(substr(time_end, 0, instr(time_start, ':')-1) as number) houre from st_shop_day_work_time a where shopid = '0063' and day_id = to_char(sysdate,'d')) b on a.id_shop = case when b.id_shop is null then a.id_shop else b.id_shop end
        where a.n >= case when b.hourb is null then a.n else b.hourb end and a.n <= case when b.houre is null then a.n else b.houre end  order by a.n
    )
    cross join (select 2926.9 plan_value from dual)
) plan
left join 
(   select id_shop, hour, sum(fact_sale_sum) over(partition by id_shop order by hour rows unbounded preceding) fact_sale_sum from
    (select id_shop, hour, sum(fact_sale_sum) fact_sale_sum from bep_prem_month_sales where trunc(dates) = trunc(sysdate) and id_shop = '0063' group by id_shop, hour order by hour)
) fact on plan.id_shop = fact.id_shop and plan.n = fact.hour
;

select id_shop, hour, sum(fact_sale_sum) over(partition by id_shop order by hour rows unbounded preceding) fact_sale_sum from
(select id_shop, hour, sum(fact_sale_sum) fact_sale_sum from bep_prem_month_sales where trunc(dates) = trunc(sysdate) and id_shop = '0036' group by id_shop, hour order by hour)
;



select id_shop, hour, sum(fact_sale_sum) over(partition by id_shop, hour order by hour rows unbounded preceding) fact_sale_sum from bep_prem_month_sales where trunc(dates) = trunc(sysdate) and id_shop = '0008'  order by hour;

select * from bep_prem_month_sales where trunc(dates) = trunc(sysdate) and id_shop = '0036' order by hour;

select * from st_shop_work_time where id_shop = '7008';
select rownum from dual where ruwnum >= 10 and rownum <= 18;

select level from dual connect by level  <= 10;


select a.id_shop, a.n
from
(select '0008' id_shop, 1 + level - 1 n from dual connect by level <= 24 - 1 + 1) a
full join
(select '0008' id_shop, hourb, houre  from st_shop_work_time where id_shop =  '7'||substr('0008',2)) b on a.id_shop = b.id_shop
where a.n >= b.hourb and a.n <= b.houre
order by a.n
;

select hourb, houre  from st_shop_work_time where id_shop =  '7'||substr('0008',2);
--------------------------------------------------------------------------------
-- ПРЕМИИ ПРОДАВЦОВ
--------------------------------------------------------------------------------

select nvl(a.id_shop, b.id_shop)  id_shop,
       c.tabno                    seller_tab,
       c.fio                      seller_fio,
       nvl(a.month_prem, 0)       month_prem,
       nvl(b.day_prem, 0)         day_prem
from (
  select id_shop, seller_tab, seller_fio, sum(prem) month_prem
  from bep_prem_month_sales
  where id_shop = '0001'
  group by id_shop, seller_tab, seller_fio
) a
full join (
  select id_shop, seller_tab, seller_fio, sum(prem) day_prem
  from bep_prem_month_sales
  where id_shop = '0001'
    and trunc(dates) = trunc(sysdate)
  group by id_shop, seller_tab, seller_fio
) b on a.id_shop = b.id_shop and a.seller_tab = b.seller_tab
inner join (
  select distinct tabno, fio from s_seller 
  where to_char(dateb, 'yyyymm') <= to_char(sysdate, 'yyyymm') and to_char(dated, 'yyyymm') >= to_char(sysdate, 'yyyymm')
) c on  nvl(a.seller_tab, b.seller_tab) = c.tabno
;


select 
nvl(a.id_shop, b.id_shop) id_shop, 
nvl(a.seller_tab, b.seller_tab) seller_tab,
nvl(a.seller_fio, b.seller_fio) seller_fio,
nvl(a.month_prem, 0) month_prem, 
nvl(b.day_prem, 0) day_prem
from 
(select id_shop, seller_tab, seller_fio, sum(prem) month_prem from bep_prem_month_sales where id_shop = '0008' group by id_shop, seller_tab, seller_fio) a
full join
(select id_shop, seller_tab, seller_fio, sum(prem) day_prem from bep_prem_month_sales where id_shop = '0008' and trunc(dates) = trunc(sysdate)-1 group by id_shop, seller_tab, seller_fio) b on a.id_shop = b.id_shop and a.seller_tab = b.seller_tab
;

select id_shop, seller_tab, seller_fio, sum(prem) month_prem from bep_prem_month_sales where id_shop = '0008' group by id_shop, seller_tab, seller_fio;
select id_shop, seller_tab, seller_fio, sum(prem) day_prem from bep_prem_month_sales where id_shop = '0008' and trunc(dates) = trunc(sysdate)-1 group by id_shop, seller_tab, seller_fio;



--------------------------------------------------------------------------------
select sum(fact_pair_kol) from bep_prem_month_sales where id_shop = '0008' and trunc(dates) = trunc(sysdate);



