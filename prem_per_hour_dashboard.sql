select
  systimestamp
, to_char(systimestamp, 'HH24') ch
, extract(timezone_hour from systimestamp) etzh
, extract(hour from systimestamp) eh
, extract(hour from cast(systimestamp as timestamp)) ehc
, to_number(to_char(systimestamp, 'HH24')) nmb 
from dual;

select
  systimestamp
, to_char(systimestamp, 'HH24') ch
, extract(timezone_hour from systimestamp) etzh
, extract(hour from systimestamp) eh
, extract(hour from cast(systimestamp as timestamp)) ehc
from dual;

select shop_id, hour, sum(fact_sale_sum), sum(fact_pair_kol) from
(
    select 
     AA."HOUR"
    ,AA."SHOP_ID"
    ,AA."VOP"
    ,lpad(AA.SELLER_TAB, 8, '0') "SELLER_TAB"
    ,AA."SELLER_FIO"
    ,cast((AA."SUMP"-AA."SUMV") as number(18,2)) as fact_sale_sum
    ,case when c.mtart not in ('ZROH','ZHW3') then cast((KOLP-KOLV) as number(18)) else 0 end as FACT_PAIR_KOL
    FROM (
        SELECT 
        extract(hour from cast(A.SALE_DATE as timestamp)) AS HOUR
        --,A.SALE_DATE
        ,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
        ,A.SHOP_ID
        ,A.SELLER_FIO
        ,A.SELLER_TAB
        ,B.ART
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
        ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
        FROM POS_SALE2 B
        INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
        WHERE A.BIT_CLOSE = 'T'
        and A.SHOP_ID = COALESCE('0034', A.SHOP_ID)
        and trunc(a.sale_date) = trunc(sysdate)
    
        UNION ALL
    
        SELECT 
        0 as hour
        --extract(hour from COALESCE(C2.DATES, A2.dated)) as HOUR
        --,COALESCE(C2.DATES, A2.dated),C2.DATES,A2.dated
        ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
        ,A2.ID_SHOP
        ,B2.SELLER_FIO
        ,B2.SELLER_TAB
        ,B2.ART
        ,0 AS SUMP1
        ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
        ,CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
        ,CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
        ,0  AS KOLP 
        ,B2.KOL AS KOLV
        FROM D_PRIXOD2 B2
        INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
        --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
        LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                        SELECT p3.ID, p3.ID_SHOP, TRUNC(MAX(p3.DATE_S)) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = COALESCE('0034', p3.ID_SHOP) GROUP BY p3.ID, p3.ID_SHOP
                        UNION ALL
                        SELECT ID_PRIXOD AS ID, ID_SHOP, TRUNC(MAX(DATE_S)) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP = COALESCE('0034', ID_SHOP) GROUP BY ID_PRIXOD, ID, ID_SHOP) WHERE ID != 0 GROUP BY ID, ID_SHOP
                ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
        LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = COALESCE('0034', c.ID_SHOP) WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
        WHERE
        ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19',/*'42',*/'45')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in ('03','14','19',/*'42',*/'45'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.DATED) IS NOT NULL)))
        AND A2.BIT_CLOSE='T' 
        and A2.ID_SHOP = COALESCE('0034', A2.ID_SHOP)
        and trunc( A2.dated) = trunc(sysdate)
    
        UNION ALL
    
        select 
        --extract(hour from a.DATED) as HOUR
        0 as hour
        ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
        ,b.id_shop
        ,b.SELLER_FIO
        ,b.seller_tab
        ,b.art ART
        ,0 AS SUMP1
        ,0 AS SUMV1 
        ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
        ,0 AS SUMV 
        ,b.kol AS KOLP
        ,0 AS KOLV
        from d_rasxod1 a
        inner join (
                select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, x.action, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2, nvl(x.kol,0) kol
                from (
                    select id, art, seller_fio, seller_tab, id_shop, action, sum(kol*cena3) sum, sum(kol) kol from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop, action
                ) x
                left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
                ) y on x.id = y.id and x.id_shop = y.id_shop
        ) b on a.id = b.id and a.id_shop = b.id_shop
        
        where a.bit_close = 'T' and a.idop in ('34','35',/*'37',*/'38','39','40', '44')
        and b.id_shop = COALESCE('0034', b.id_shop)
        and trunc(a.dated) = trunc(sysdate)
    ) AA
    left join s_art c on aa.art = c.art 
) 
group by shop_id, hour
order by hour
;