--begin
--    for i_month in 11..11 loop
--        for item in (select shp from spa_fil_shop where fil = '00F1' order by shp)
--        loop
--            bep_calculate_prem_full(2018, i_month, item.shp);
--            --bep_calculate_prem_plan(2018, 8, item.shp);
--        end loop;
--    end loop;
--end;

set serveroutput on;

begin
    --for item in (select shp from spa_fil_shop where fil = '00F1' order by shp)
    for item in (select shp from spa_fil_shop where shp in ('0001', '0002', '0003') order by shp)
    loop
        bep_calculate_prem_full(2018, 11, item.shp);
        --bep_calculate_prem_plan(2018, 8, item.shp);
    end loop;
end;