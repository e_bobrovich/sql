CREATE MATERIALIZED VIEW LOG ON BEP_PREM_S_CRITERION
WITH ROWID;


CREATE MATERIALIZED VIEW BEP_PREM_MONTH_SALES
BUILD IMMEDIATE
REFRESH FAST ON COMMIT
AS 
select a.hour, a.dates, a.shop_id, a.vop, a.seller_tab, a.seller_fio, a.fact_sale_sum, a.fact_pair_kol, a.art, a.sh_sop, a.own_purchase, a.brand, a.cennik, d.*, a.fact_sale_sum * (bep_prem_get_combination_perc(extract(year from sysdate), extract(month from sysdate), '0001', d.id_combination)) / 100 as prem from(
    select 
     --AA."HOUR"
    to_number(to_char(AA."DATES", 'HH24')) "HOUR"
    ,AA."DATES"
    ,AA."SHOP_ID"
    ,AA."VOP"
    ,lpad(AA.SELLER_TAB, 8, '0') "SELLER_TAB"
    ,AA."SELLER_FIO"
    ,cast((AA."SUMP"-AA."SUMV") as number(18,2)) as fact_sale_sum
    ,case when c.mtart not in ('ZROH','ZHW3') then cast((KOLP-KOLV) as number(18)) else 0 end as FACT_PAIR_KOL
    ,AA."ART"
    , case when c.mtart not in ('ZROH','ZHW3') then 'Обувь' else 'Сопутка' end as "SH_SOP"
    , case when c.facture = ' ' and c.manufactor != 'СООО БЕЛВЕСТ' then 'Покупная'
           when c.facture = ' ' and c.manufactor = 'СООО БЕЛВЕСТ' then 'Собственная' 
           else c.facture 
      end as "OWN_PURCHASE"
    , c.manufactor as "BRAND"
    , case when "ACTION" = 'F' then 'Белый' else 'Оранжевый' end as "CENNIK" --CHANGE AFTER
    FROM (
        SELECT 
        extract(hour from A.SALE_DATE) AS HOUR
        ,A.SALE_DATE AS DATES
        ,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP 
        ,A.SHOP_ID
        ,A.SELLER_FIO
        ,A.SELLER_TAB
        ,B.ART
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
        ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
        ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP 
        ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
        ,B.ACTION
        FROM POS_SALE2 B
        INNER JOIN POS_SALE1 A ON A.ID_CHEK=B.ID_CHEK and a.shop_id = b.id_shop
        WHERE A.BIT_CLOSE = 'T'
        --and A.SHOP_ID = COALESCE('0008', A.SHOP_ID)
        --and trunc(a.sale_date) = trunc(sysdate-1)
        and to_char(a.sale_date, 'yyyymm') = to_char(sysdate, 'yyyymm')
    
        UNION ALL
    
        SELECT 
        0 as hour
        --extract(hour from COALESCE(C2.DATES, A2.dated)) as HOUR
        ,COALESCE(C2.DATES, A2.date_s) as DATES
        ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP
        ,A2.ID_SHOP
        ,B2.SELLER_FIO
        ,B2.SELLER_TAB
        ,B2.ART
        ,0 AS SUMP1
        ,CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 
        ,CAST(0.00 AS NUMBER(18,2) ) AS SUMP 
        ,CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV 
        ,0  AS KOLP 
        ,B2.KOL AS KOLV
        ,B2.ACTION
        FROM D_PRIXOD2 B2
        INNER JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID and a2.id_shop = b2.id_shop
        --INNER JOIN POS_SALE1 S2 ON TO_CHAR(S2.ID_CHEK) = A2.NDOC AND S2.ID_SHOP = A2.ID_SHOP --AND S2.SCAN = B2.SCAN
        LEFT JOIN (SELECT ID, ID_SHOP, MAX(DATES) AS DATES FROM (
                        SELECT p3.ID, p3.ID_SHOP, MAX(p3.DATE_S) AS DATES FROM D_PRIXOD3 p3 WHERE p3.ID_SHOP = p3.ID_SHOP GROUP BY p3.ID, p3.ID_SHOP
                        UNION ALL
                        SELECT ID_PRIXOD AS ID, ID_SHOP, MAX(DATE_S) AS DATES FROM POS_ORDER_RX where IDOSNOVANIE not in ('21','33','25','32')AND ID_SHOP =  ID_SHOP GROUP BY ID_PRIXOD, ID, ID_SHOP) WHERE ID != 0 GROUP BY ID, ID_SHOP
                ) C2 ON A2.ID_SHOP = C2.ID_SHOP AND A2.ID = C2.ID 
        LEFT JOIN (SELECT MAX(TIP) AS TIP, op1.IDOP, c.SHOPID FROM ST_OP op1 INNER JOIN CONFIG c ON c.SHOPID = c.ID_SHOP WHERE op1.NUMCONF IN (0, c.NUMCONF) GROUP BY op1.IDOP, c.SHOPID) op ON op.IDOP = A2.IDOP AND op.SHOPID = A2.ID_SHOP
        WHERE
        ((op.TIP IS NOT NULL AND op.TIP != 0) AND ((op.TIP != 1 AND A2.IDOP in ('03','14','19',/*'42',*/'45')) OR (op.TIP NOT IN (2, 3) AND A2.IDOP not in ('03','14','19',/*'42',*/'45'))) AND ((op.TIP = 1 AND A2.DATED IS NOT NULL) OR (op.TIP = 2 AND C2.DATES IS NOT NULL) OR (op.TIP = 3 AND COALESCE(C2.DATES, A2.DATED) IS NOT NULL)))
        AND A2.BIT_CLOSE='T' 
        --and A2.ID_SHOP = COALESCE('0008', A2.ID_SHOP)
        --and trunc( A2.dated) = trunc(sysdate-1)
        and to_char(A2.dated, 'yyyymm') = to_char(sysdate, 'yyyymm')
    
        UNION ALL
    
        select 
        --extract(hour from a.DATED) as HOUR
        0 as hour
        ,a.DATE_S as DATES
        ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
        ,b.id_shop
        ,b.SELLER_FIO
        ,b.seller_tab
        ,b.art ARTl
        ,0 AS SUMP1
        ,0 AS SUMV1 
        ,case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
        ,0 AS SUMV 
        ,b.kol AS KOLP
        ,0 AS KOLV
        ,b.ACTION
        from d_rasxod1 a
        inner join (
                select x.id, x.art, x.seller_fio, seller_tab, x.id_shop, x.action, nvl(y.sum,0) sum3, nvl(x.sum,0) sum2, nvl(x.kol,0) kol
                from (
                    select id, art, seller_fio, seller_tab, id_shop, action, sum(kol*cena3) sum, sum(kol) kol from d_rasxod2 group by id, art, seller_fio, seller_tab, id_shop, action
                ) x
                left join (select id, id_shop, sum(sum) sum from d_rasxod3 group by id, id_shop
                ) y on x.id = y.id and x.id_shop = y.id_shop
        ) b on a.id = b.id and a.id_shop = b.id_shop
        
        where a.bit_close = 'T' and a.idop in ('34','35',/*'37',*/'38','39','40', '44')
        --and b.id_shop = COALESCE('0008', b.id_shop)
        --and trunc(a.dated) = trunc(sysdate-1)
        and to_char(a.dated, 'yyyymm') = to_char(sysdate, 'yyyymm')
    ) AA
    left join s_art c on aa.art = c.art 
) a
--left join
inner join
(
    select gs.id_shop, cp.id_combination, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join BEP_PREM_S_CRITERION gc on cc.id_criterion = gc.id_criterion
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
    and cp.id_combination != 0
    group by gs.id_shop, cp.id_combination
) d on
    a.shop_id = d.id_shop 
    and
    case when d.ids_criterion like '%'||1||'%' then d.combination else a.SH_SOP end like '%'||a.SH_SOP||'%' 
    and case when d.ids_criterion like '%'||2||'%' then d.combination else a.CENNIK end like '%'||a.CENNIK||'%'  
    and case when d.ids_criterion like '%'||3||'%' then d.combination else a.OWN_PURCHASE end like '%'||a.OWN_PURCHASE||'%'  
    and case when d.ids_criterion like '%'||4||'%' then d.combination else a.BRAND end like '%'||a.BRAND||'%' 
order by shop_id, hour


;




MERGE INTO ST_DK d
USING (   SELECT '0000000000125' ID_DK, '+7 921 647 7439' AS PHONE_NUMBER, 'F' AS BIT_BLOCK FROM DUAL) t
ON (t.ID_DK = d.ID_DK)
WHEN MATCHED THEN UPDATE SET d.PHONE_NUMBER = t.PHONE_NUMBER, d.BIT_BLOCK = t.BIT_BLOCK
WHEN NOT MATCHED THEN INSERT (ID_DK, PHONE_NUMBER, BIT_BLOCK)
VALUES (t.ID_DK, t.PHONE_NUMBER, t.BIT_BLOCK);
commit;

SELECT * FROM ST_DK_PHONE;
