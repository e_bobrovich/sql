select rownum rn, x.* from (select a.tabel,nvl(a.fio,b.seller_fio) fio,A.DOLJNOST,
case when a.doljnost is null then 0 when upper(a.doljnost) like '%ПРОДАВЕЦ%' then 2 else 1 end code,
nvl(b.sumr,0) sumr
from pos_users a
full join (
SELECT SELLER_FIO
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  FROM POS_SALEA_V_PREM
 WHERE DATED>=to_date('01.08.2018','dd.MM.yyyy') 
 AND DATED<=to_date('31.08.2018','dd.MM.yyyy') AND BIT_CLOSE='T'   
 GROUP BY SELLER_FIO) b on a.fio = b.seller_fio
 where upper(a.doljnost) not like '%КАССИР%'
 order by a.tabel) x;
 
 select sum(sumr) from (
 select rownum rn, x.* from (select a.tabel,nvl(a.fio,b.seller_fio) fio,A.DOLJNOST,
case when a.doljnost is null then 0 when upper(a.doljnost) like '%ПРОДАВЕЦ%' then 2 else 1 end code,
nvl(b.sumr,0) sumr
from pos_users a
full join (
SELECT SELLER_FIO
,CAST(SUM(SUMP) AS NUMBER(18,2)) AS SUMP 
,CAST(SUM(SUMV) AS NUMBER(18,2)) AS SUMV 
,CAST(SUM(SUMP-SUMV) AS NUMBER(18,2) ) AS SUMR 
,CAST(SUM(SUMP1-SUMV1) AS NUMBER(18,2) ) AS SUM1 
,SUM(KOLP) AS KOLP 
,SUM(KOLV) AS KOLV 
,SUM(KOLP-KOLV) AS KOLR 
  FROM POS_SALEA_V_PREM
 WHERE DATED>=to_date('01.02.2018','dd.MM.yyyy') 
 AND DATED<to_date('01.03.2018','dd.MM.yyyy') AND BIT_CLOSE='T'   
 GROUP BY SELLER_FIO) b on a.fio = b.seller_fio
 where upper(a.doljnost) not like '%КАССИР%'
 order by a.tabel) x
 );
 
SELECT AA."DATED",AA."VOP",AA."KOEF",AA."ID_CHEK",AA."SALE_DATE",AA."ID_DK",AA."INF1",AA."SHOP_ID",AA."KASSIR_ID",AA."KASSIR_FIO",AA."SELLER_ID",AA."SELLER_FIO",AA."PRICE_SUM"
,AA."DISCOUNT_SUM",AA."SALE_SUM",AA."INF2",AA."INF3",AA."BIT_CLOSE",AA."BIT_VOZVR",AA."DISCOUNT_INF",AA."ID_SALE",AA."ID_CHEK_VOZVR",AA."ID_SALE_VOZVR",AA."PARTNO",AA."ART"
,AA."ASIZE",AA."PROCENT",AA."EAN",AA."SCAN",AA."ASSORT",AA."KOL",AA."CENA1",AA."CENA2",AA."CENA3",AA."PORTION",AA."NEI",AA."DISCOUNT",AA."SUMP1",AA."SUMV1",AA."SUMP",AA."SUMV",AA."KOLP"
,AA."KOLV",GRUP1(AA.ART,1) AS GRUP,NVL(C.MANUFACTOR,' ') AS MANUFACTOR,NVL(C.SEASON,' ') AS SEASON ,NVL(C.GROUPMW,' ') AS GROUPMW
,AA.KOP,AA.KASSA_ID FROM ( -- 12/07/2016

    SELECT TO_DATE(TO_CHAR(CAST(A.SALE_DATE AS DATE))) AS DATED,CASE WHEN A.BIT_VOZVR='F' THEN 'ПРОДАЖА' ELSE 'ВОЗВРАТ' END AS VOP ,CASE WHEN A.BIT_VOZVR='F' THEN 1 ELSE 2 END AS KOP
    ,CASE WHEN A.BIT_VOZVR='F' THEN  1 ELSE -1 END AS KOEF ,A.ID_CHEK,A.SALE_DATE,A.ID_DK,A.INF1,A.SHOP_ID,A.KASSIR_ID,A.KASSIR_FIO,A.SELLER_ID,A.SELLER_FIO,A.PRICE_SUM,A.DISCOUNT_SUM,A.SALE_SUM
    ,A.INF2,A.INF3 ,A.BIT_CLOSE,A.BIT_VOZVR ,B.DISCOUNT_INF,B.ID_SALE,B.ID_CHEK_VOZVR,B.ID_SALE_VOZVR ,B.PARTNO,B.ART,B.ASIZE,B.PROCENT,B.EAN,B.SCAN,B.ASSORT,B.KOL ,B.CENA1,B.CENA2,B.CENA3
    ,B.PORTION,B.NEI,B.DISCOUNT
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END AS SUMP1
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA1*B.KOL AS NUMBER(18,2) ) END AS SUMV1
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) ELSE CAST(0.00 AS NUMBER(18,2)) END  AS SUMP
    ,CASE WHEN A.BIT_VOZVR='F' THEN  CAST(0.00 AS NUMBER(18,2)) ELSE CAST(B.CENA3*B.KOL AS NUMBER(18,2) ) END AS SUMV
    ,CASE WHEN A.BIT_VOZVR='F' THEN  B.KOL ELSE 0 END AS KOLP ,CASE WHEN A.BIT_VOZVR='F' THEN  0 ELSE B.KOL END AS KOLV
    ,A.KASSA_ID
    FROM POS_SALE2 B  
    
    LEFT JOIN (
            select x.ID_CHEK,x.SALE_DATE,x.ID_DK,x.INF1,x.SHOP_ID,x.KASSIR_ID,x.KASSIR_FIO,x.SELLER_ID,x.SELLER_FIO,x.PRICE_SUM,x.DISCOUNT_SUM,nvl(c.sum,0) sale_sum,x.INF2,x.INF3,x.BIT_CLOSE,x.BIT_VOZVR,x.KASSA_ID,x.FR_ID_CHEK,x.SELLER_TAB,x.DATE_CLOSE,x.BIT_ANNUL
            from pos_sale1 x
            left join (select id_chek,sum(sum) sum from pos_sale3 where vid_op_id != 3 group by id_chek) C on x.id_chek = c.id_chek
    ) A ON A.ID_CHEK=B.ID_CHEK 
    
    WHERE A.BIT_CLOSE='T'
   
UNION ALL

-- SELECT (  CASE WHEN (A2.IDOP=(SELECT MAX(IDOP14) FROM CONFIG) OR A2.IDOP=(SELECT MAX(IDOP16) FROM CONFIG) )   AND NOT A3.ID IS NULL THEN A3.DATED   ELSE A2.DATED END) AS DATED
    SELECT GET_VOZVRAT(A2.ID) AS DATED
    ,'ВОЗВРАТ ДОКУМЕНТАЛЬНЫЙ' AS VOP,3 AS KOP,-1 AS KOEF,A2.ID AS ID_CHEK,to_date(to_char(A2.dated,'yyyyMMdd')||nvl(to_char(A2.timed,'HH24miss'),'000000'),'yyyyMMddHH24miss') AS SALE_DATE,C.ID_DK,C.INF1,C.SHOP_ID,C.KASSIR_ID,C.KASSIR_FIO,B2.SELLER_ID,B2.SELLER_FIO
    ,NVL(C.PRICE_SUM,B2.KOL*B2.CENA2) AS PRICE_SUM,NVL(C.DISCOUNT_SUM,B2.KOL*(B2.CENA2-B2.CENA3)) AS DISCOUNT_SUM,NVL(C.SALE_SUM,B2.KOL*B2.CENA3) AS SALE_SUM
    ,C.INF2,C.INF3 ,A2.BIT_CLOSE,C.BIT_VOZVR ,' ' AS DISCOUNT_INF
    ,0 AS ID_SALE,0 AS ID_CHEK_VOZVR,0 AS ID_SALE_VOZVR ,B2.PARTNO,B2.ART,B2.ASIZE,B2.PROCENT,B2.EAN,B2.SCAN,B2.ASSORT,B2.KOL ,B2.CENA1,B2.CENA2,B2.CENA3,B2.PORTION,B2.NEI,0 AS DISCOUNT , 0 AS SUMP1
    , CAST(B2.CENA1*B2.KOL AS NUMBER(18,2) ) AS SUMV1 , CAST(0.00 AS NUMBER(18,2) ) AS SUMP , CAST(B2.CENA3*B2.KOL AS NUMBER(18,2) ) AS SUMV , 0  AS KOLP , B2.KOL AS KOLV
    ,'0' AS KASSA_ID
    FROM D_PRIXOD2 B2
    LEFT JOIN D_PRIXOD1 A2 ON A2.ID=B2.ID
    LEFT JOIN POS_SALE1 C ON A2.NDOC=TO_CHAR(C.ID_CHEK) 
    WHERE
    --   ((A2.IDOP=(SELECT MAX(IDOP) FROM CONFIG) OR A2.IDOP=(SELECT MAX(IDOP14) FROM CONFIG)  AND NOT A3.ID IS NULL) OR A2.IDOP=(SELECT MAX(IDOP16) FROM CONFIG) ) AND A2.BIT_CLOSE='T')
    ((GET_VOZVRAT(A2.ID,3) IS not NULL and A2.IDOP in ('03','14','19','42')) or (GET_VOZVRAT(A2.ID,1) IS not NULL and A2.IDOP not in ('03','14','19','42')))  
    AND A2.BIT_CLOSE='T' 
    
    UNION ALL
    
    select a.DATED
    ,case when a.idop in ('37') then 'РАСХОД ПО СЕРТИФИКАТУ' else 'БЕЗНАЛ' end AS VOP
    ,4 AS KOP,1 AS KOEF,a.ID AS ID_CHEK
    ,to_date(to_char(a.dated,'yyyyMMdd')||nvl(to_char(a.timed,'HH24miss'),'000000'),'yyyyMMddHH24miss') AS SALE_DATE
    ,' ' ID_DK,' ' INF1,' ' SHOP_ID,0 KASSIR_ID,' ' KASSIR_FIO,0 SELLER_ID
    ,b.SELLER_FIO
    ,0 AS PRICE_SUM,
    0 AS DISCOUNT_SUM,
    0 AS SALE_SUM
    ,' ' INF2,' ' INF3 ,A.BIT_CLOSE,'F' BIT_VOZVR ,' ' AS DISCOUNT_INF
    ,0 AS ID_SALE,0 AS ID_CHEK_VOZVR,0 AS ID_SALE_VOZVR ,' ' PARTNO,' ' ART,0 ASIZE,0 PROCENT,' ' EAN,' ' SCAN,' ' ASSORT,0 KOL ,0 CENA1,0 CENA2,0 CENA3,0 PORTION,' ' NEI,0 AS DISCOUNT , 0 AS SUMP1
    ,0 AS SUMV1 , 
    case when a.idop in ('37') then b.sum3 else b.sum2 end AS SUMP 
    ,0 AS SUMV , 0  AS KOLP , 0 AS KOLV
    ,'0' AS KASSA_ID
    from d_rasxod1 a
    
    inner join (
            select x.id,x.seller_fio,nvl(y.sum,0) sum3,nvl(x.sum,0) sum2
            from (
                select id,seller_fio,sum(kol*cena3) sum from d_rasxod2 group by id,seller_fio
            ) x
            
            left join (select id,sum(sum) sum from d_rasxod3 group by id
            ) y on x.id = y.id
    ) b on a.id = b.id
    
    where a.bit_close = 'T' and a.idop in ('34','35','37','38','39','40')
)
AA
LEFT JOIN S_ART C ON AA.ART=C.ART
ORDER BY DATED DESC
; 
 
 
 select x.ID_CHEK,x.SALE_DATE,x.ID_DK,x.INF1,x.SHOP_ID,x.KASSIR_ID,x.KASSIR_FIO,x.SELLER_ID,x.SELLER_FIO,x.PRICE_SUM,x.DISCOUNT_SUM,nvl(c.sum,0) sale_sum,x.INF2,x.INF3,x.BIT_CLOSE,x.BIT_VOZVR,x.KASSA_ID,x.FR_ID_CHEK,x.SELLER_TAB,x.DATE_CLOSE,x.BIT_ANNUL
            from pos_sale1 x
            left join (select id_chek,sum(sum) sum from pos_sale3 where vid_op_id != 3 group by id_chek) C on x.id_chek = c.id_chek;
select * from pos_sale1;



