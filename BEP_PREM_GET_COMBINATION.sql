select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
from bep_prem_combination_percent cp
inner join bep_prem_s_group sg on cp.id_group = sg.id_group
inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
--and sg.dateb is not null and sg.datee is not null
and cp.id_combination != 0
and gs.id_shop = '0008'
group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent;


select a.* from (
    select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent,  listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
    and sg.dateb is not null and sg.datee is not null
    and cp.id_combination != 0
    and gs.id_shop = '0008'
    group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
) a
where
to_date('19.12.2018','dd.mm.yyyy') >= a.dateb and to_date('19.12.2018','dd.mm.yyyy') <= a.datee
and case when a.ids_criterion like '%'||1||'%' then a.combination else 'Сопутка' end like '%'||'Сопутка'||'%' 
and case when a.ids_criterion like '%'||2||'%' then a.combination else 'Белый' end like '%'||'Белый'||'%'  
and case when a.ids_criterion like '%'||3||'%' then a.combination else 'Собственная' end like '%'||'Собственная'||'%'  
and case when a.ids_criterion like '%'||4||'%' then a.combination else 'СООО БЕЛВЕСТ' end like '%'||'СООО БЕЛВЕСТ'||'%' 
;

select * from (
    select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
    and sg.dateb is null and sg.datee is  null
    and cp.id_combination != 0
    and gs.id_shop = '0008'
    group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, cp.percent
) a
where
    case when a.ids_criterion like '%'||1||'%' then a.combination else 'Сопутка' end like '%'||'Сопутка'||'%' 
and case when a.ids_criterion like '%'||2||'%' then a.combination else 'Белый' end like '%'||'Белый'||'%'  
and case when a.ids_criterion like '%'||3||'%' then a.combination else 'Собственная' end like '%'||'Собственная'||'%'  
and case when a.ids_criterion like '%'||4||'%' then a.combination else 'СООО БЕЛВЕСТ' end like '%'||'СООО БЕЛВЕСТ'||'%';



-----------------------------------------------------------------------------------



select * from
(
    select 1 sale, sysdate dates from dual
    union
    select 2 sale,sysdate-10 dates from dual
) a
left join
(
    select gs.id_shop, cp.id_combination, sg.dateb, sg.datee, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join BEP_PREM_S_CRITERION gc on cc.id_criterion = gc.id_criterion
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
    and cp.id_combination != 0
    and gs.id_shop = '0008'
    group by gs.id_shop, cp.id_combination, sg.dateb, sg.datee
) b
on 
1 = 1

;

--КОМБИНАЦИИ С ПЕРИОДАМИ
select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
from bep_prem_combination_percent cp
inner join bep_prem_s_group sg on cp.id_group = sg.id_group
inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
and sg.dateb is not null and sg.datee is not null
and cp.id_combination != 0
and gs.id_shop = '0008'
group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee
;

--КОМБИНАЦИИ БЕЗ ПЕРИОДОВ
select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
from bep_prem_combination_percent cp
inner join bep_prem_s_group sg on cp.id_group = sg.id_group
inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
and sg.dateb is null and sg.datee is null
and cp.id_combination != 0
and gs.id_shop = '0008'
group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee
;

select 
a.id_group id_group_spec, b.id_group id_group_def, 
a.dateb, a.datee, 
a.id_combination id_combination_spec, a.combination combination_spec,a.ids_criterion ids_criterion_spec,
b.id_combination id_combination_def, b.combination combination_def, b.ids_criterion ids_criterion_spec
from
(
    select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
    and sg.dateb is not null and sg.datee is not null
    and cp.id_combination != 0
    and gs.id_shop = '0008'
    group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee
) a
full join 
(
    select gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee, listagg(cc.value,'/') within group(order by cc.id_criterion) combination, listagg(cc.id_criterion,'/') within group(order by cc.id_criterion) ids_criterion
    from bep_prem_combination_percent cp
    inner join bep_prem_s_group sg on cp.id_group = sg.id_group
    inner join bep_prem_group_shop gs on cp.id_group = gs.id_group 
    left join bep_prem_combination_criterion cc on cp.id_combination = cc.id_combination and cp.id_group = cc.id_group
    left join bep_prem_s_criterion gc on cc.id_criterion = gc.id_criterion
    where sg.year = extract(year from sysdate) and sg.month = extract(month from sysdate)
    and sg.dateb is null and sg.datee is null
    and cp.id_combination != 0
    and gs.id_shop = '0008'
    group by gs.id_group, gs.id_shop, cp.id_combination, sg.dateb, sg.datee
) b on 1=1;