select 
case when c.id_dk_from is null then '' else 'С '||c.id_dk_from end ||
case when c.id_dk_from is not null and b.id_dk_to is not null then '/' else '' end ||
case when b.id_dk_to is null then '' else 'На '||b.id_dk_to end
as replace_state 
from (select * from st_dk where id_dk = '0000002728100') a
left join (
    select id_dk_from, listagg(id_dk_to, ', ') within group (order by id_dk_to desc) id_dk_to
    from (select distinct id_dk_from, id_dk_to from pos_dk_copy_logs where id_dk_from = '0000002728100')
    group by id_dk_from
) b on a.id_dk = b.id_dk_from
left join (
    select id_dk_to, listagg(id_dk_from, ', ') within group (order by id_dk_from desc) id_dk_from
    from (select distinct id_dk_to, id_dk_from from pos_dk_copy_logs where id_dk_to = '0000002728100')
    group by id_dk_to
) c on a.id_dk = c.id_dk_to
;


select id_dk_from, listagg(id_dk_to, ', ') within group (order by id_dk_to desc) id_dk_to
from (select distinct id_dk_from, id_dk_to from pos_dk_copy_logs where id_dk_from = '0000002728100')
group by id_dk_from;


select id_dk_to, count(*) from pos_dk_copy_logs group by id_dk_to having count(*) > 1;
select * from pos_dk_copy_logs where id_dk_to = '0000019252834';

select * from pos_dk_copy_logs where id_dk_from = '0000002728100' or id_dk_to = '0000002728100';

select id_dk_from, id_dk_to from pos_dk_copy_logs where id_dk_from = '0000002728100' or id_dk_to = '0000002728100' group by id_dk_from, id_dk_to;

select id_dk_to from pos_dk_copy_logs where id_dk_from = '0000002728100' group by id_dk_to;

select id_dk_from, listagg(id_dk_to, ', ') within group (order by id_dk_to desc) id_dk_to
from (select distinct id_dk_from, id_dk_to from pos_dk_copy_logs where id_dk_from = '0000002728100')
group by id_dk_from;



select id_dk_from from pos_dk_copy_logs where id_dk_to = '0000002728100' group by id_dk_from;

select id_dk_to, listagg(id_dk_from, ', ') within group (order by id_dk_from desc) id_dk_from
from (select distinct id_dk_to, id_dk_from from pos_dk_copy_logs where id_dk_to = '0000002728100')
group by id_dk_to;



select id_dk_from, id_dk_to, count(*) from pos_dk_copy_logs group by id_dk_from, id_dk_to;

select * from
(select distinct id_dk_from from pos_dk_copy_logs) a
inner join (select distinct id_dk_to from pos_dk_copy_logs) b on a.id_dk_from = b.id_dk_to
;

select * from pos_dk_copy_logs where id_dk_from = '0000002728100' or id_dk_to = '0000002728100';
select * from pos_dk_copy_logs where id_dk_from = '0000019689041' or id_dk_to = '0000019689041';